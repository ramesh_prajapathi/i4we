'use strict';

angular.module('secondarySalesApp')
	.controller('groupmeetingeventCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/groupmeetingevents/create' || $location.path() === '/groupmeetingevents/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/groupmeetingevents/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/groupmeetingevents/create' || $location.path() === '/groupmeetingevents/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/groupmeetingevents/create' || $location.path() === '/groupmeetingevents/' + $routeParams.id;
			return visible;
		};


		/*********/

		//  $scope.groupmeetingevents = Restangular.all('groupmeetingevents').getList().$object;

		if ($routeParams.id) {
			Restangular.one('groupmeetingevents', $routeParams.id).get().then(function (groupmeetingevent) {
				$scope.original = groupmeetingevent;
				$scope.groupmeetingevent = Restangular.copy($scope.original);
			});
		}
		$scope.searchgroupmeetingevent = $scope.name;

		/********************************* INDEX *******************************************/
		$scope.zn = Restangular.all('groupmeetingevents').getList().then(function (zn) {
			$scope.groupmeetingevents = zn;
			angular.forEach($scope.groupmeetingevents, function (member, index) {
				member.index = index + 1;
			});
		});


		$scope.getTopic = function (topicId) {
			return Restangular.one('groupmeetingtopics', topicId).get().$object;
		};
		/*************************** SAVE *******************************************/
		$scope.validatestring = '';
		$scope.Save = function () {

			$scope.groupmeetingevents.post($scope.groupmeetingevent).then(function () {
				console.log('groupmeetingevent Saved');
				window.location = '/groupmeetingevents';
			});
		};
		/*************************************************************************** UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			if ($scope.groupmeetingevent.name == '' || $scope.groupmeetingevent.name == null) {
				$scope.groupmeetingevent.name = null;
				$scope.validatestring = $scope.validatestring + 'Plese enter your groupmeetingevent name';
				document.getElementById('name').style.border = "1px solid #ff0000";

			}
			if ($scope.validatestring != '') {
				alert($scope.validatestring);
				$scope.validatestring = '';
			} else {
				$scope.groupmeetingevents.customPUT($scope.groupmeetingevent).then(function () {
					console.log('groupmeetingevent Saved');
					window.location = '/groupmeetingevents';
				});
			}


		};
		/************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			if (confirm("Are you sure want to delete..!") == true) {
				Restangular.one('groupmeetingevents/' + id).remove($scope.groupmeetingevent).then(function () {
					$route.reload();
				});

			} else {

			}

		}

	});
