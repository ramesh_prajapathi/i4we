'use strict';

angular.module('secondarySalesApp')
	.controller('BulkUpCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
		/*********/

		$scope.addedtodos = [];

		$scope.showForm = function () {
			var visible = $location.path() === '/bulkupdates/create' || $location.path() === '/bulkupdates/' + $routeParams.id;
			return visible;
		};
		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/bulkupdates/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/bulkupdates/create' || $location.path() === '/bulkupdates/' + $routeParams.id;
			return visible;
		};
		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/bulkupdates/create' || $location.path() === '/bulkupdates/' + $routeParams.id;
			return visible;
		};
		/*************************************************************************************/



		$scope.bulkupdate = {
			state: $window.sessionStorage.zoneId,
			district: $window.sessionStorage.salesAreaId,
			facility: $window.sessionStorage.coorgId,
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			lastmodifiedtime: new Date(),
			deleteflag: false
		};

		$scope.auditlog = {
			description: 'Bulk Update Create',
			modifiedbyroleid: $window.sessionStorage.roleId,
			modifiedby: $window.sessionStorage.UserEmployeeId,
			lastmodifiedtime: new Date(),
			entityroleid: 43,
			state: $window.sessionStorage.zoneId,
			district: $window.sessionStorage.salesAreaId,
			facility: $window.sessionStorage.coorgId,
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
		};

		$scope.reportincident = {};
		$scope.event = {};
		$scope.submitbulkupdates = Restangular.all('bulkupdates').getList().$object;
		$scope.bulkupdatefollowups = Restangular.all('bulkupdatefollowups').getList().$object;
		$scope.submittodos = Restangular.all('todos');
		$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;
		$scope.applicastages = Restangular.all('applicationstages').getList().then(function (appstage) {
			$scope.applicationstages = appstage;
			angular.forEach($scope.applicationstages, function (member, index) {
				member.index = index + 1;
				member.enabled = false;
			});
		});

		$scope.getOrganisedby = function (partnerId) {
			return Restangular.one('partners', partnerId).get().$object;
		};

		$scope.getMemberName = function (partnerId) {
			return Restangular.all('beneficiaries?filter={"where":{"id":{"inq":[' + partnerId + ']}}}').getList().$object;
		};

		$scope.getFollwUp = function (followUpId) {
			return Restangular.all('bulkupdatefollowups?filter={"where":{"id":{"inq":[' + followUpId + ']}}}').getList().$object;
		};



		$scope.bulkupdates = Restangular.all('bulkupdates').getList().$object;

		/************************************* Current& Future Date********************************/
		var today = new Date();

		$scope.bulkupdate.followupstatus = 'open';
		var newdate = new Date();

		$scope.today = function () {
			newdate.setDate(newdate.getDate() + 7, 'dd-MMMM-yyyy');

			var dd = newdate.getDate();
			var mm = newdate.getMonth() + 1;
			var y = newdate.getFullYear();

			var sevendays = new Date();
			sevendays.setDate(sevendays.getDate() + 7);
			$scope.dtmin = sevendays;
			/*$scope.newtodo = {
				datetime: sevendays
			};*/
		};
		$scope.today();
		// $scope.presenttoday = new Date();
		// $scope.dtmax = new Date();

		$scope.showWeeks = true;
		$scope.toggleWeeks = function () {
			$scope.showWeeks = !$scope.showWeeks;
		};

		$scope.clear = function () {
			$scope.dt = null;
		};

		$scope.toggleMin = function () {
			$scope.minDate = ($scope.minDate) ? null : new Date();
		};
		$scope.toggleMin();
		$scope.picker = {};
		$scope.followdt = {};



		$scope.opendob = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();

			$timeout(function () {
				$('#datepickerdob' + index).focus();
			});
			$scope.picker.dobopened = true;
		};

		$scope.open = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();

			$timeout(function () {
				$('#datepickerfollowup' + index).focus();
			});
			//$scope.follow.followupopened = true;
			$scope.followdt.followupopened = true;

		};



		$scope.dateOptions = {
			'year-format': 'yy',
			'starting-day': 1
		};

		$scope.monthOptions = {
			formatYear: 'yyyy',
			startingDay: 1,
			minMode: 'month'
		};
		$scope.mode = 'month';



		$scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
		$scope.monthformats = ['MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
		$scope.format = $scope.formats[0];
		$scope.monthformat = $scope.monthformats[0];

		//Datepicker settings end

		/************************************************ EVENT END *******************************/

		if ($routeParams.id) {
			Restangular.one('bulkupdates', $routeParams.id).get().then(function (event) {
				$scope.original = event;
				$scope.event = Restangular.copy($scope.original);
				$scope.event.organised = event.organisedby;
				$scope.event.attendees = event.attendeeslist.split(",");

				var res = $scope.event.eventsdiscussed.split(",");
				$scope.applicastages = Restangular.all('applicationstages').getList().then(function (appstage) {
					$scope.applicationstages = appstage;
					angular.forEach($scope.applicationstages, function (member, index) {
						if ((res.indexOf(member.name) > -1)) {
							member.enabled = true;
						} else {
							member.enabled = false;
						}
					});
				});
			});
		}
		$scope.searchbulkupdate = $scope.name;

		/************************************************* INDEX *******************************************/
		$scope.zn = Restangular.all('bulkupdates?filter[where][deleteflag]=false' + '&filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (zn) {
			$scope.Printbulkupdates = zn;
			$scope.modalInstanceLoad.close();
			angular.forEach($scope.Printbulkupdates, function (member, index) {
				member.index = index + 1;
				member.memberlist = $scope.getMemberName(member.memberlist);
				member.followup = $scope.getFollwUp(member.followup);
			});
		});

		/********************************************** SAVE *******************************************/

		$scope.bulkupdatedataModal = false;
		$scope.message = 'Bulk Update has been created!';
		$scope.modalTitle = 'Thank You';

		$scope.bulkupdate.follow = [];
		$scope.validatestring = '';
		$scope.Savebulkupdate = function () {
			$scope.bulkupdatedataModal = !$scope.bulkupdatedataModal;
			$scope.bulkupdate.followup = null;
			$scope.bulkupdate.memberlist = null;

			var eventcheckedValue = null;
			var inputElements = document.getElementsByClassName('eventCheckbox');
			//var inputElements = document.getElementsById('mytable');
			/*for (var i = 0; inputElements[i]; ++i) {
				if (inputElements[i].checked) {
					if (checkedValue == null) {
						checkedValue = inputElements[i].value;
					} else {
						checkedValue = checkedValue + ',' + inputElements[i].value;
					}
				}
			}*/
			for (var i = 0; inputElements[i]; ++i) {
				if (inputElements[i].checked) {
					if (eventcheckedValue == null) {
						eventcheckedValue = inputElements[i].value;
					} else {
						eventcheckedValue = eventcheckedValue + ',' + inputElements[i].value;
					}
				}
			}

			$scope.bulkupdate.memberlist = eventcheckedValue;
			console.log('$scope.bulkupdate.memberlist', $scope.bulkupdate.memberlist);

			for (var i = 0; i < $scope.bulkupdate.follow.length; i++) {
				if (i == 0) {
					$scope.bulkupdate.followup = $scope.bulkupdate.follow[i];
				} else {
					$scope.bulkupdate.followup = $scope.bulkupdate.followup + ',' + $scope.bulkupdate.follow[i];
				}
			}

			if (eventcheckedValue === null) {
				$scope.validatestring = $scope.validatestring + 'Please Add Member';
			} else if ($scope.bulkupdate.follow == '' || $scope.bulkupdate.follow == null) {
				$scope.validatestring = $scope.validatestring + 'Please Sellect Follow Up';
			}

			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
				$scope.bulkupdatedataModal = !$scope.bulkupdatedataModal;
			} else {
				$scope.submitbulkupdates.post($scope.bulkupdate).then(function (resp) {
					$scope.auditlog.entityid = resp.id;
					$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
						console.log('responseaudit', responseaudit);

						console.log('bulkupdate Saved', $scope.bulkupdate);
						$scope.SaveToDo();
						//window.location = '/bulkupdates';
						console.log('Save ToDo');
					});
				});
			}
		};

		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};


		/************************************************* MOdal ******************************/





		$scope.showfollowupModal = false;
		$scope.$watch('bulkupdate.follow', function (newValue, oldValue) {
			$scope.newtodo = {};
			$scope.newtodo.status = 1;
			var sevendays = new Date();
			sevendays.setDate(sevendays.getDate() + 7);
			$scope.dtmin = new Date();
			$scope.newtodo.datetime = sevendays;

			$scope.oldFollowUp = oldValue;
			$scope.newFollow = newValue;
			if (newValue === oldValue) {
				return;
			} else {
				$scope.SavetodoFollow = function () {
					$scope.newtodo.todotype = 33;
					$scope.newtodo.state = $window.sessionStorage.zoneId;
					$scope.newtodo.district = $window.sessionStorage.salesAreaId;
					$scope.newtodo.facility = $window.sessionStorage.coorgId;
					$scope.newtodo.facilityId = $scope.getfacilityId;
					$scope.newtodo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
					$scope.newtodo.lastmodifiedtime = new Date();
					$scope.newtodo.purpose = $scope.purposeid;
					$scope.newtodo.roleId = $window.sessionStorage.roleId;
					console.log('$scope.newtodo.purpose', $scope.newtodo.purpose);

					$scope.bulkupdate.datetime = $scope.newtodo.datetime;
					console.log('$scope.bulkupdate.datetime', $scope.bulkupdate.datetime);

					$scope.addedtodos.push($scope.newtodo);
					console.log('$scope.addedtodos', $scope.addedtodos);
					$scope.showfollowupModal = !$scope.showfollowupModal;
				};
				var array3 = newValue.filter(function (obj) {
					return oldValue.indexOf(obj) == -1;
				});
				if (array3.length > 0) {
					console.log('unique', array3);
					for (var i = 0; i < $scope.bulkupdatefollowups.length; i++) {
						if ($scope.bulkupdatefollowups[i].id == array3[0]) {
							$scope.printpurpose = $scope.bulkupdatefollowups[i].name;
							$scope.purposeid = $scope.bulkupdatefollowups[i].id;
						}
					}
				}

				if (oldValue != undefined) {
					if (oldValue.length < newValue.length) {
						$scope.showfollowupModal = !$scope.showfollowupModal;
					}
				} else {
					$scope.showfollowupModal = !$scope.showfollowupModal;
				}
			};
		});


		$scope.CancelFollow = function () {
			$scope.bulkupdate.follow = $scope.oldFollowUp;
			console.log('$scope.bulkupdate.follow', $scope.bulkupdate.follow);
			$scope.showfollowupModal = !$scope.showfollowupModal;
		};




		$scope.todocount = 0;
		$scope.SaveToDo = function () {
			$scope.addedtodocount = 0;
			for (var i = 0; i < $scope.addedtodos.length; i++) {
				$scope.addedtodos[i].beneficiaryid = $scope.attendeestodo[$scope.todocount];
				$scope.submittodos.post($scope.addedtodos[i]).then(function (resp) {
					$scope.addedtodocount++;
					if ($scope.addedtodocount >= $scope.addedtodos.length) {
						$scope.todocount++;
						if ($scope.todocount < $scope.attendeestodo.length) {
							$scope.SaveToDo();
						} else if ($scope.SaveToDo.length <= $scope.addedtodos.length) {
							window.location = '/bulkupdates';
						}
					}
				});
			}
		};


		/**************************************************** UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Updatebulkupdate = function () {
			document.getElementById('name').style.border = "";
			if ($scope.bulkupdate.name == '' || $scope.bulkupdate.name == null) {
				$scope.bulkupdate.name = null;
				$scope.validatestring = $scope.validatestring + 'Please enter your bulkupdate name';
				document.getElementById('name').style.border = "1px solid #ff0000";

			}
			if ($scope.validatestring != '') {
				alert($scope.validatestring);
				$scope.validatestring = '';
			} else {
				$scope.submitbulkupdates.customPUT($scope.bulkupdate).then(function () {
					//  console.log('bulkupdate Saved');
					window.location = '/bulkupdates';
				});
			}


		};
		/******************************************* DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: 'yes'
            }]
			Restangular.one('bulkupdates/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}


		$scope.zipmasters = Restangular.all('pillars').getList().then(function (zipmaster) {
			// console.log('zipmaster', zipmaster);
			$scope.zipmasters = zipmaster;
			angular.forEach($scope.zipmasters, function (member, index) {

				member.total = null;
				member.loader = null;
			});
		});



		$scope.RemoveMember = function (index) {
			$scope.bulkpartners.splice(index, 1);
			$scope.$watch('partner.enabled', function (newValue, oldValue) {
				console.log('partner.enabled', newValue);
				if (newValue === oldValue) {
					$scope.partners[index].enabled = false;
					return;
				} else {
					for (var i = 0; i < $scope.bulkpartners.length; i++) {
						$scope.partners[i].enabled = false;
					}
				}
			});
		};
		/******************************************** MOdal For FollowUp***************************/
		/* $scope.prtnrs = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=null').getList().then(function (partner) {
		     $scope.partners = partner;
		     angular.forEach($scope.partners, function (member, index) {
		         member.index = index;
		         member.enabled = false;
		     });
		 });*/

		

		if ($window.sessionStorage.roleId == 5) {
			Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
				$scope.comemberid = comember.id;
				$scope.bulkupdate.facilityId = comember.id;
				$scope.auditlog.facilityId = comember.id;
				$scope.getfacilityId = comember.id;

				$scope.part = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (partner) {
					$scope.partners = partner;
					angular.forEach($scope.partners, function (member, index) {
						member.index = index;
						member.enabled = false;
					});
				});

			});
		} else {
			Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
				$scope.getsiteid = fw.id;
				Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
					$scope.comemberid = comember.id;
					$scope.bulkupdate.facilityId = comember.id;
					$scope.auditlog.facilityId = comember.id;
					$scope.getfacilityId = comember.id;
				});
				//});
				Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (partner) {
					$scope.partners = partner;
					angular.forEach($scope.partners, function (member, index) {
						member.index = index;
						member.enabled = false;
					});
				});
			});
		}

		$scope.showModal1 = false;
		$scope.toggleModal1 = function () {

			$scope.SaveScheme = function () {
				$scope.newArray = [];
				$scope.bulkpartners = [];
				$scope.partnerid = [];

				$scope.bulkupdate.attendees = [];
				for (var i = 0; i < $scope.partners.length; i++) {
					if ($scope.partners[i].enabled == true) {
						$scope.newArray.push($scope.partners[i]);
						$scope.partnerid.push($scope.partners[i].id);
						//$scope.attendeestodo.push($scope.partners[i].id);
					} else if ($scope.partners[i].enabled == false) {
						$scope.partners[i].enabled = false;

					}
				}
				$scope.attendeestodo = $scope.partnerid;
				console.log('$scope.attendeestodo', $scope.attendeeattendeestodo);


				$scope.bulkpartners = $scope.newArray;
				$scope.showModal1 = !$scope.showModal1;
			};
			$scope.showModal1 = !$scope.showModal1;
		};



		$scope.CancelReport = function () {
			$scope.showModal1 = !$scope.showModal1;
		};

		$scope.$watch('partner.enabled', function (newValue, oldValue) {
			console.log('partner.enabled', newValue);
		});


		$("#Search").keyup(function () {
			var data = this.value.toUpperCase().split(" ");
			var js = $("#bnbody").find("tr");
			if (this.value == "") {
				js.show();
				return;
			}
			js.hide();
			js.filter(function (i, v) {
				var $t = $(this);
				for (var d = 0; d < data.length; ++d) {
					if ($t.text().toUpperCase().indexOf(data[d]) > -1) {
						return true;
					}
				}
				return false;
			}).show();
		});

	/************************ Language ****************************************/
	$scope.languages = Restangular.all('languages').getList().$object;
		$scope.multiLang = Restangular.one('multilanguages?filter[where][id]='+$window.sessionStorage.language).get().then(function (langResponse) {
			console.log('langResponse', langResponse);

			$scope.bulkupdateheader = langResponse[0].bulkupdateheader;
			$scope.memberfullname = langResponse[0].memberfullname;
			$scope.memberdetails = langResponse[0].memberdetails;
			$scope.action = langResponse[0].action;
			$scope.bulkupdateheader = langResponse[0].bulkupdateheader;
			$scope.followuprequired = langResponse[0].followuprequired;
			$scope.membername = langResponse[0].membername;
            $scope.removebutton = langResponse[0].removebutton;
			$scope.status = langResponse[0].status;
			$scope.followupdate = langResponse[0].followupdate;
			$scope.create = langResponse[0].create;
			$scope.update = langResponse[0].update;
			$scope.cancel = langResponse[0].cancel;
			$scope.addnew = langResponse[0].addnew;
			$scope.addmember = langResponse[0].addmember;
			$scope.followup = langResponse[0].followup;
			$scope.select = langResponse[0].select;
			$scope.show = langResponse[0].show;
			$scope.entry = langResponse[0].entry;
			$scope.searchfor = langResponse[0].searchfor;
            $scope.savebutton = langResponse[0].savebutton;
            $scope.searchmember = langResponse[0].searchmember;    
		
		});

	});
