'use strict';

angular.module('secondarySalesApp')
    .controller('LangLHSCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/


        $scope.HideCreateButton = true;
        $scope.langdisable = false;

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        $scope.$watch('lhs.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                 Restangular.one('lhsLanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.lhs = Restangular.copy($scope.original);
                    });
                
                
                Restangular.all('lhsLanguages?filter[where][language]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                        $scope.HideCreateButton = true;
                    } else {
                        //$scope.LangId = response[0].id;
                       // $scope.HideCreateButton = false;
                      //  $scope.langdisable = true;

                        //  $scope.lhs = response[0];
                        //  console.log('$scope.lhs', $scope.lhs);
                        $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';
                    }
                });

            }
        });

    /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/LangLHSlist';

        };
    
    
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/
    
    
        $scope.lhs = {
            deleteFlag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.UserEmployeeId
        };




        $scope.Save = function () {
            Restangular.all('lhsLanguages').post($scope.lhs).then(function (docResponse) {
                console.log('docResponse', docResponse);
                window.location = '/LangLHSlist';
            });

        };

        $scope.Update = function () {
            Restangular.one('lhsLanguages', $routeParams.id).customPUT($scope.lhs).then(function (docResponse) {
                console.log('docResponse', docResponse);
                window.location = '/LangLHSlist';
            });
        };

        if ($routeParams.id) {

            $scope.HideCreateButton = false;
            $scope.langdisable = true;
            Restangular.one('lhsLanguages', $routeParams.id).get().then(function (lhs) {
                $scope.original = lhs;
                $scope.lhs = Restangular.copy($scope.original);
            });
        }


    });
