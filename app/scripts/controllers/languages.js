'use strict';

angular.module('secondarySalesApp')
    .controller('LanguagesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
        /*********/

        $scope.someFocusVariable = true;

        $scope.showForm = function () {
            var visible = $location.path() === '/language/create' || $location.path() === '/language/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/language/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/language/create' || $location.path() === '/language/edit/' + $routeParams.id;
            return visible;
        };

        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/language/create' || $location.path() === '/language/edit/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/language") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /****************************************************************************************INDEX****************************/

        $scope.searchCon = $scope.name;

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (lang) {
            $scope.languageslist = lang;
            $scope.languageId = $window.sessionStorage.sales_languageId;

            $scope.languageArray = [];

            angular.forEach($scope.languageslist, function (member, index) {
                member.index = index + 1;

                $scope.languageArray.push({
                    name: member.name,
                    id: member.id
                });

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        $('#name').keypress(function (evt) {
            if (/^[a-zA-Z ]*$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $scope.language = {
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.UserEmployeeId,
            deleteFlag: false
        };

        if ($window.sessionStorage.roleId == 1) {
            $scope.hideAddBtn = true;
        } else {
            $scope.hideAddBtn = false;
        }
        /****************************************************************************CREATE************************************/
        $scope.validatestring = '';
        $scope.Save = function () {
            document.getElementById('name').style.border = "";

            if ($scope.language.name == '' || $scope.language.name == null) {
                $scope.language.name = null;
                $scope.validatestring = $scope.validatestring + 'Please enter your language name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                Restangular.one('languages/findOne?filter[where][name]=' + $scope.language.name).get().then(function (ctry) {
                    // console.log('ctry', ctry);
                    alert('Existing Language, Please Change Language Name');
                    $scope.language.name = '';
                }, function (response) {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

                    $scope.languages.post($scope.language).then(function () {
                        // console.log('$scope.language', $scope.language);
                        window.location = '/language-list';
                    });
                });
            }
        };

        /***********************************************************************************UPDATE****************************/

        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.language.name == '' || $scope.language.name == null) {
                $scope.language.name = null;
                $scope.validatestring = $scope.validatestring + 'Please enter your language name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

                $scope.languages.customPUT($scope.language).then(function () {
                    // console.log('$scope.language', $scope.language);
                    window.location = '/language-list';
                });
            }
        };

        $scope.modalTitle = 'Thank You';

        if ($routeParams.id) {
            $scope.message = 'Language has been Updated!';
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('languages', $routeParams.id).get().then(function (language) {
                $scope.original = language;
                $scope.language = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Language has been created!';
        }

        /**********************************************************************************************DELETE*************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('languages/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /**************************Export data to excel sheet ***************/

        $scope.exportData = function () {
            // console.log('$scope.householdsArray', $scope.householdsArray);
            if ($scope.languageArray.length == 0) {
                alert('No data found');
            } else {
                alasql('SELECT * INTO XLSX("languages.xlsx",{headers:true}) FROM ?', [$scope.languageArray]);
            }
        };


        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

    });
