'use strict';

angular.module('secondarySalesApp')
    .controller('DistrictsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter) {

        /***********************************************SHOW AND HIDE BUTTON**************************************/

        $scope.showForm1 = false;

        $scope.someFocusVariable = true;

        $scope.showForm = function () {
            var visible = $location.path() === '/district/create' || $location.path() === '/district/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/district/create';
                return visible;
                //	$scope.statedisable = false;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/district/create' || $location.path() === '/district/edit/' + $routeParams.id;
            return visible;

        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/district/create' || $location.path() === '/district/edit/' + $routeParams.id;
            return visible;
        };

        $scope.searchSalesArea = $scope.name;

        //		if ($window.sessionStorage.roleId != 1) {
        //			window.location = "/";
        //		}

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.sales_zoneId == null
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.zoneId = $window.sessionStorage.myRoute;
        }

        if ($window.sessionStorage.prviousLocation != "partials/district") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
        /*....................................................................................*/

        Restangular.all('countries?filter[where][deleteFlag]=false').getList().then(function (znes) {
            $scope.countries = znes;
            $scope.countryId = znes[0].id;
        });

        $scope.statedsply = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;

        // $scope.countryId = '';
        $scope.stateId = '';

        $scope.$watch('countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {

                $scope.districtArray = [];

                $window.sessionStorage.myRoute = newValue;
                $window.sessionStorage.currentRoute = newValue;

                Restangular.all('states?filter[where][deleteFlag]=false' + '&filter[where][countryId]=' + newValue).getList().then(function (state) {
                    $scope.states = state;
                });

                //$scope.zoneId = $window.sessionStorage.myRoute;
                //console.log('Inside Value:', $window.sessionStorage.myRoute);
                Restangular.all('districts?filter[where][countryId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (sal) {
                    $scope.DisplayDistricts = sal;
                    // console.log('DisplaysalesAreas', sal);
                    angular.forEach($scope.DisplayDistricts, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.countries.length; a++) {
                            if (member.countryId == $scope.countries[a].id) {
                                member.countryname = $scope.countries[a].name;
                                break;
                            }
                        }
                        for (var b = 0; b < $scope.statedsply.length; b++) {
                            if (member.stateId == $scope.statedsply[b].id) {
                                member.statename = $scope.statedsply[b].name;
                                break;
                            }
                        }
                    });
                });
            }
            $scope.zonalid = newValue;
        });

        $scope.$watch('stateId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {

                $scope.districtArray = [];

                $window.sessionStorage.myRoute = newValue;
                $window.sessionStorage.currentRoute = newValue;

                //$scope.zoneId = $window.sessionStorage.myRoute;
                //console.log('Inside Value:', $window.sessionStorage.myRoute);
                Restangular.all('districts?filter[where][countryId]=' + $scope.zonalid + '&filter[where][stateId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (sal) {
                    $scope.DisplayDistricts = sal;
                    // console.log('DisplaysalesAreas', sal);
                    angular.forEach($scope.DisplayDistricts, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.countries.length; a++) {
                            if (member.countryId == $scope.countries[a].id) {
                                member.countryname = $scope.countries[a].name;
                                break;
                            }
                        }
                        for (var b = 0; b < $scope.statedsply.length; b++) {
                            if (member.stateId == $scope.statedsply[b].id) {
                                member.statename = $scope.statedsply[b].name;
                                break;
                            }
                        }
                    });
                });
            }
        });

        $scope.getCountry = function (countryId) {
            return Restangular.one('countries', countryId).get().$object;
        };

        $scope.getState = function (stateId) {
            return Restangular.one('states', stateId).get().$object;
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        /******************************************************************************************/
        $scope.getZone = function (zoneId) {
            return Restangular.one('zones', zoneId).get().$object;
        };

        $scope.DisplayDistricts = [];

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('districts/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.district = {
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.UserEmployeeId,
            deleteFlag: false
        }

        $scope.submitDisable = false;
        $scope.validatestring = '';

        $scope.Save = function () {
            document.getElementById('name').style.border = "";

            if ($scope.district.name == '' || $scope.district.name == null) {
                document.getElementById('name').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please Enter District';
            } else if ($scope.district.countryId == '' || $scope.district.countryId == null) {
                $scope.district.countryId = '';
                $scope.validatestring = $scope.validatestring + 'Please Select Country';
            } else if ($scope.district.stateId == '' || $scope.district.stateId == null) {
                $scope.district.stateId = '';
                $scope.validatestring = $scope.validatestring + 'Please Select State';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.all('districts').post($scope.district).then(function () {
                    //$location.path('/sales-areas');
                    window.location = '/district-list';
                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


        ///////////////////////////////////////////////////////////UPDATE DELETE///////////////////////////////    

        //$scope.zones = Restangular.all('zones').getList().$object;
        $scope.modalTitle = 'Thank You';
        if ($routeParams.id) {
            $scope.showForm1 = true;
            $scope.message = 'District has been Updated!';
            Restangular.one('districts', $routeParams.id).get().then(function (district) {
                $scope.original = district;
                $scope.district = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'District has been Created!';
        }

        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            if ($scope.district.name == '' || $scope.district.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter District';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.one('districts', $routeParams.id).customPUT($scope.district).then(function () {
                    // console.log('Sales-Area Saved', $scope.district);
                    window.location = '/district-list';
                    //$location.path('/sales-areas');
                });
            }
        };

        $scope.district.countryId = '';

        $scope.$watch('district.countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                Restangular.all('states?filter[where][deleteFlag]=false' + '&filter[where][countryId]=' + newValue).getList().then(function (state) {
                    $scope.mystates = state;
                });
            }
        });

        /**************************Export data to excel sheet ***************/

        $scope.valDtCount = 0;

        $scope.exportData = function () {
            $scope.valDtCount = 0;
            //  console.log('$scope.DisplayDistricts', $scope.DisplayDistricts);

            if ($scope.DisplayDistricts.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.DisplayDistricts.length; c++) {
                    $scope.districtArray.push({
                        'DISTRICT': $scope.DisplayDistricts[c].name,
                        'STATE': $scope.DisplayDistricts[c].statename,
                        'COUNTRY': $scope.DisplayDistricts[c].countryname,
                        'DELETE FLAG': $scope.DisplayDistricts[c].deleteFlag
                    });

                    $scope.valDtCount++;

                    if ($scope.DisplayDistricts.length == $scope.valDtCount) {
                        alasql('SELECT * INTO XLSX("districts.xlsx",{headers:true}) FROM ?', [$scope.districtArray]);
                    }
                }
                // alasql('SELECT * INTO XLSX("districts.xlsx",{headers:true}) FROM ?', [$scope.districtArray]);
            }
        };


        /////////////////////////////////////////CALL CITY ACCORDING TO ZONE//////////////////


    });