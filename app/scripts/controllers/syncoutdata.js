'use strict';

angular.module('secondarySalesApp')
    .controller('SyncOutDataCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter) {

        $scope.firstClickCount = 0;
        $scope.secondClickCount = 0;
        $scope.thirdClickCount = 0;

        $scope.HfLanguage = {};

        Restangular.one('hflanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.HfLanguage = langResponse[0];
            $scope.hfHeading = langResponse[0].hfCreate;
        });

        Restangular.all('users?filter[where][id]=' + $routeParams.id).getList().then(function (user) {
            $scope.selectedUser = user[0];
            // console.log('$scope.selectedFW',$scope.selectedFW);
        });

        Restangular.all('userlogindetails?filter[where][platform]=mobile&filter[where][operationflag]=syncout&filter[where][userid]=' + $routeParams.id).getList().then(function (logindetails) {
            $scope.userlogindetails = logindetails;
            if ($scope.userlogindetails.length >= 4) {
                $scope.thirdSyncout = $scope.userlogindetails[$scope.userlogindetails.length - 1];
                $scope.secondSyncout = $scope.userlogindetails[$scope.userlogindetails.length - 2];
                $scope.firstSyncout = $scope.userlogindetails[$scope.userlogindetails.length - 3];
            } else if ($scope.userlogindetails.length == 3) {
                $scope.thirdSyncout = $scope.userlogindetails[2];
                $scope.secondSyncout = $scope.userlogindetails[1];
                $scope.firstSyncout = $scope.userlogindetails[0];
            } else if ($scope.userlogindetails.length == 2) {
                $scope.thirdSyncout = $scope.userlogindetails[1];
                $scope.secondSyncout = $scope.userlogindetails[0];
            } else if ($scope.userlogindetails.length == 1) {
                $scope.thirdSyncout = $scope.userlogindetails[0];
            }


            Restangular.all('members?filter[where][associatedHF]=' + $routeParams.id).getList().then(function (resPartner1) {
                $scope.partners = resPartner1;
                angular.forEach($scope.partners, function (member, index) {
                    member.index = index + 1;
                });
                $scope.onCLickThirdSyncOut($scope.thirdSyncout.datetime);

            });
        });

        $scope.getassignedSites = function (siteids) {
            if (siteids != null || siteids != '' || siteids != 'undefined') {
                return Restangular.all('sites?filter={"where":{"id":{"inq":[' + siteids + ']}}}').getList().$object;
            }
        };


        $scope.thirdSyncMembers = "";
        $scope.thirdSyncAnswers = "";
        $scope.thirdSyncSchemes = "";
        $scope.thirdSyncIncidents = "";

        $scope.thirdArraySyncMembers = [];
        $scope.thirdArraySyncAnswers = [];
        $scope.thirdArraySyncSchemes = [];
        $scope.thirdArraySyncIncidents = [];

        $scope.onCLickThirdSyncOut = function (syncouttime) {

            if (syncouttime != null && syncouttime != '' && syncouttime != undefined) {
                if ($scope.thirdClickCount == 0) {
                    $scope.thirdClickCount++;

                    Restangular.all('members?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().then(function (bens) {
                        $scope.beneficiaries = bens;

                        angular.forEach($scope.beneficiaries, function (member, index) {
                            if ($scope.thirdArraySyncMembers.indexOf(member.name) == -1) {
                                $scope.thirdArraySyncMembers.push(member.name);
                                if ($scope.thirdSyncMembers != "") {
                                    $scope.thirdSyncMembers = $scope.thirdSyncMembers + "," + member.name;
                                } else {
                                    $scope.thirdSyncMembers = $scope.thirdSyncMembers + member.name;
                                }
                            }
                        });
                    });

                    //                    Restangular.all('reportincidents?filter[where][syncouttime]=' + syncouttime).getList().then(function (reports) {
                    //                        //console.log("reports",reports);
                    //                        $scope.reportincidents = reports;
                    //                        angular.forEach($scope.reportincidents, function (member, index) {
                    //                            member.index = index + 1;
                    //
                    //                            for (var m = 0; m < $scope.partners.length; m++) {
                    //                                if (member.beneficiaryid == $scope.partners[m].id) {
                    //                                    member.Membername = $scope.partners[m].fullname;
                    //                                    break;
                    //                                }
                    //                            }
                    //                            if ($scope.thirdArraySyncIncidents.indexOf(member.Membername) == -1) {
                    //                                $scope.thirdArraySyncIncidents.push(member.Membername);
                    //                                if ($scope.thirdSyncIncidents != "") {
                    //                                    $scope.thirdSyncIncidents = $scope.thirdSyncIncidents + "," + member.Membername;
                    //                                } else {
                    //                                    $scope.thirdSyncIncidents = $scope.thirdSyncIncidents + member.Membername;
                    //                                }
                    //                            }
                    //                        });
                    //                    });


                    Restangular.all('schememasters?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().then(function (schmasters) {
                        $scope.schememasters = schmasters;
                        // console.log("schmasters",schmasters);
                        angular.forEach($scope.schememasters, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.memberId == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].name;
                                    break;
                                }
                            }
                            if ($scope.thirdArraySyncSchemes.indexOf(member.Membername) == -1) {
                                $scope.thirdArraySyncSchemes.push(member.Membername);
                                if ($scope.thirdSyncSchemes != "") {
                                    $scope.thirdSyncSchemes = $scope.thirdSyncSchemes + "," + member.Membername;
                                } else {
                                    $scope.thirdSyncSchemes = $scope.thirdSyncSchemes + member.Membername;
                                }
                            }
                        });
                    });

                    $scope.thirdgroupmeetings = Restangular.all('shgmeetingheaders?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().$object;
                    $scope.thirdconditions = Restangular.all('conditionheaders?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().$object;
                }
            }
        };



        $scope.secondSyncMembers = "";
        $scope.secondSyncAnswers = "";
        $scope.secondSyncSchemes = "";
        $scope.secondSyncIncidents = "";
        $scope.secondArraySyncMembers = [];
        $scope.secondArraySyncAnswers = [];
        $scope.secondArraySyncSchemes = [];
        $scope.secondArraySyncIncidents = [];

        $scope.onCLickSecondSyncOut = function (syncouttime) {
            if (syncouttime != null && syncouttime != '' && syncouttime != undefined) {
                if ($scope.secondClickCount == 0) {
                    $scope.secondClickCount++;
                    Restangular.all('members?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().then(function (bens) {
                        $scope.beneficiaries = bens;
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            if ($scope.secondArraySyncMembers.indexOf(member.name) == -1) {
                                $scope.secondArraySyncMembers.push(member.name);
                                if ($scope.secondSyncMembers != "") {
                                    $scope.secondSyncMembers = $scope.secondSyncMembers + "," + member.name;
                                } else {
                                    $scope.secondSyncMembers = $scope.secondSyncMembers + member.name;
                                }
                            }
                        });
                    });

                    //                    Restangular.all('reportincidents?filter[where][syncouttime]=' + syncouttime).getList().then(function (reports) {
                    //                        $scope.reportincidents = reports;
                    //                        angular.forEach($scope.reportincidents, function (member, index) {
                    //                            member.index = index + 1;
                    //
                    //                            for (var m = 0; m < $scope.partners.length; m++) {
                    //                                if (member.beneficiaryid == $scope.partners[m].id) {
                    //                                    member.Membername = $scope.partners[m].fullname;
                    //                                    break;
                    //                                }
                    //                            }
                    //                            if ($scope.secondArraySyncIncidents.indexOf(member.Membername) == -1) {
                    //                                $scope.secondArraySyncIncidents.push(member.Membername);
                    //                                if ($scope.secondSyncIncidents != "") {
                    //                                    $scope.secondSyncIncidents = $scope.secondSyncIncidents + "," + member.Membername;
                    //                                } else {
                    //                                    $scope.secondSyncIncidents = $scope.secondSyncIncidents + member.Membername;
                    //                                }
                    //                            }
                    //                        });
                    //                    });


                    Restangular.all('schememasters?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().then(function (schmasters) {
                        $scope.schememasters = schmasters;
                        angular.forEach($scope.schememasters, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.memberId == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].name;
                                    break;
                                }
                            }
                            if ($scope.secondArraySyncSchemes.indexOf(member.Membername) == -1) {
                                $scope.secondArraySyncSchemes.push(member.Membername);
                                if ($scope.secondSyncSchemes != "") {
                                    $scope.secondSyncSchemes = $scope.secondSyncSchemes + "," + member.Membername;
                                } else {
                                    $scope.secondSyncSchemes = $scope.secondSyncSchemes + member.Membername;
                                }
                            }
                        });
                    });

                    $scope.secondgroupmeetings = Restangular.all('shgmeetingheaders?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().$object;
                    $scope.secondconditions = Restangular.all('conditionheaders?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().$object;
                }
            }
        };


        $scope.firstSyncMembers = "";
        $scope.firstSyncAnswers = "";
        $scope.firstSyncSchemes = "";
        $scope.firstSyncIncidents = "";
        $scope.firstArraySyncMembers = [];
        $scope.firstArraySyncAnswers = [];
        $scope.firstArraySyncSchemes = [];
        $scope.firstArraySyncIncidents = [];
        $scope.onCLickFirstSyncOut = function (syncouttime) {
            if (syncouttime != null && syncouttime != '' && syncouttime != undefined) {
                if ($scope.firstClickCount == 0) {
                    $scope.firstClickCount++;
                    Restangular.all('members?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().then(function (bens) {
                        $scope.beneficiaries = bens;
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            if ($scope.firstArraySyncMembers.indexOf(member.name) == -1) {
                                $scope.firstArraySyncMembers.push(member.name);
                                if ($scope.firstSyncMembers != "") {
                                    $scope.firstSyncMembers = $scope.firstSyncMembers + "," + member.name;
                                } else {
                                    $scope.firstSyncMembers = $scope.firstSyncMembers + member.name;
                                }
                            }
                        });
                    });

                    //                    Restangular.all('reportincidents?filter[where][syncouttime]=' + syncouttime).getList().then(function (reports) {
                    //                        $scope.reportincidents = reports;
                    //                        angular.forEach($scope.reportincidents, function (member, index) {
                    //                            member.index = index + 1;
                    //
                    //                            for (var m = 0; m < $scope.partners.length; m++) {
                    //                                if (member.beneficiaryid == $scope.partners[m].id) {
                    //                                    member.Membername = $scope.partners[m].fullname;
                    //                                    break;
                    //                                }
                    //                            }
                    //                            if ($scope.firstArraySyncIncidents.indexOf(member.Membername) == -1) {
                    //                                $scope.firstArraySyncIncidents.push(member.Membername);
                    //                                if ($scope.firstSyncIncidents != "") {
                    //                                    $scope.firstSyncIncidents = $scope.firstSyncIncidents + "," + member.Membername;
                    //                                } else {
                    //                                    $scope.firstSyncIncidents = $scope.firstSyncIncidents + member.Membername;
                    //                                }
                    //                            }
                    //                        });
                    //                    });


                    Restangular.all('schememasters?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().then(function (schmasters) {
                        $scope.schememasters = schmasters;
                        angular.forEach($scope.schememasters, function (member, index) {
                            member.index = index + 1;

                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.memberId == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m].name;
                                    break;
                                }
                            }
                            if ($scope.firstArraySyncSchemes.indexOf(member.Membername) == -1) {
                                $scope.firstArraySyncSchemes.push(member.Membername);
                                if ($scope.firstSyncSchemes != "") {
                                    $scope.firstSyncSchemes = $scope.firstSyncSchemes + "," + member.Membername;
                                } else {
                                    $scope.firstSyncSchemes = $scope.firstSyncSchemes + member.Membername;
                                }
                            }
                        });
                    });

                    $scope.firstgroupmeetings = Restangular.all('shgmeetingheaders?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().$object;
                    $scope.firstconditions = Restangular.all('conditionheaders?filter[where][syncouttime]=' + syncouttime + '&filter[where][associatedHF]=' + $routeParams.id).getList().$object;
                }
            }
        };

    });
