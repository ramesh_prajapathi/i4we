'use strict';

angular.module('secondarySalesApp')
  .controller('EmotionCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams,$timeout,baseUrl, $route, $window) {
    /*********/

    $scope.showForm = function(){
      var visible = $location.path() === '/emotionalstates/create' || $location.path() === '/emotionalstates/' + $routeParams.id;
      return visible;
    };
    
    $scope.isCreateView = function(){
      if($scope.showForm()){
        var visible = $location.path() === '/emotionalstates/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function (){
        var visible = $location.path() === '/emotionalstates/create'|| $location.path() === '/emotionalstates/' + $routeParams.id;
        return visible;
      };

    
    $scope.hideSearchFilter = function (){
        var visible = $location.path() === '/emotionalstates/create'|| $location.path() === '/emotionalstates/' + $routeParams.id;
        return visible;
      };

    
    /*********/

  //  $scope.emotionalstates = Restangular.all('emotionalstates').getList().$object;
    
    if($routeParams.id){
      Restangular.one('emotionalstates', $routeParams.id).get().then(function(emotionalstate){
				$scope.original = emotionalstate;
				$scope.emotionalstate = Restangular.copy($scope.original);
      });
    }
    $scope.Search = $scope.name;
    
/************************************************************************** INDEX *******************************************/
       $scope.zn = Restangular.all('emotionalstates').getList().then(function (zn) {
        $scope.emotionalstates = zn;
        angular.forEach($scope.emotionalstates, function (member, index) {
            member.index = index + 1;
        });
    });
    
/*************************************************************************** SAVE *******************************************/
     $scope.validatestring = '';
        $scope.Save = function () {       
                    $scope.emotionalstates.post($scope.emotionalstate).then(function () {
                        console.log('emotionalstates Saved');
                        window.location = '/emotionalstates';
                    });
        };
/*************************************************************************** UPDATE *******************************************/ 
     $scope.validatestring = '';
        $scope.Update = function () {
                document.getElementById('name').style.border = "";
          if ($scope.emotionalstate.name == '' || $scope.emotionalstate.name == null) {
                $scope.emotionalstate.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your emotional status ';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } 
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.emotionalstates.customPUT($scope.emotionalstate).then(function () {
                        console.log('emotional status Saved');
                        window.location = '/emotionalstates';
                    });
                }


        };
/*************************************************************************** DELETE *******************************************/ 
   $scope.Delete = function (id) {
        if (confirm("Are you sure want to delete..!") == true) {
            Restangular.one('emotionalstates/' + id).remove($scope.emotionalstate).then(function () {
                $route.reload();
            });

        } else {

        }

    }
    
  });


