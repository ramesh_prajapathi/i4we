'use strict';

angular.module('secondarySalesApp')
	.controller('followupreportincidentCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/followupreportincident/create' || $location.path() === '/followupreportincident/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/followupreportincident/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/followupreportincident/create' || $location.path() === '/followupreportincident/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/followupreportincident/create' || $location.path() === '/followupreportincident/' + $routeParams.id;
			return visible;
		};

		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
		}

		
		if($window.sessionStorage.prviousLocation != "partials/followupreportincident") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}
		
		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};
	
		/*********/

		$scope.submitreportincidentfollowups = Restangular.all('reportincidentfollowups').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'Report Incident Follow Up has been Updated!';
			Restangular.one('reportincidentfollowups', $routeParams.id).get().then(function (reportincidentfollowup) {
				$scope.original = reportincidentfollowup;
				$scope.reportincidentfollowup = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Report Incident Follow Up has been Created!';
		}
		$scope.searchreportincidentfollowups = $scope.name;

		/********************************************************* INDEX *******************************************/
		$scope.zn = Restangular.all('reportincidentfollowups?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.reportincidentfollowups = zn;
			angular.forEach($scope.reportincidentfollowups, function (member, index) {
				member.index = index + 1;
			});
		});

		/************************************ SAVE *******************************************/
		$scope.reportincidentfollowup = {
			name: '',
			deleteflag: false
		};
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Savereportincidentfollowup = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.reportincidentfollowup.name == '' || $scope.reportincidentfollowup.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.reportincidentfollowup.hnname == '' || $scope.reportincidentfollowup.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.reportincidentfollowup.knname == '' || $scope.reportincidentfollowup.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.reportincidentfollowup.taname == '' || $scope.reportincidentfollowup.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.reportincidentfollowup.tename == '' || $scope.reportincidentfollowup.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.reportincidentfollowup.mrname == '' || $scope.reportincidentfollowup.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitreportincidentfollowups.post($scope.reportincidentfollowup).then(function () {
					console.log('reportincidentfollowups Saved');
					window.location = '/followupreportincident';
				});
			};
		};
		/********************************* UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Updatereportincidentfollowup = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.reportincidentfollowup.name == '' || $scope.reportincidentfollowup.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.reportincidentfollowup.hnname == '' || $scope.reportincidentfollowup.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.reportincidentfollowup.knname == '' || $scope.reportincidentfollowup.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.reportincidentfollowup.taname == '' || $scope.reportincidentfollowup.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.reportincidentfollowup.tename == '' || $scope.reportincidentfollowup.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.reportincidentfollowup.mrname == '' || $scope.reportincidentfollowup.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitreportincidentfollowups.customPUT($scope.reportincidentfollowup).then(function () {
					console.log('reportincidentfollowups Saved');
					window.location = '/followupreportincident';
				});
			}
		};
		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/********************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('reportincidentfollowups/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}
	});
