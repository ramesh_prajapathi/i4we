'use strict';

angular.module('secondarySalesApp')
    .controller('ReportIncidentListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {

        $rootScope.clickFW();
        /************************ Language ************************/

        Restangular.one('riLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.RILanguage = langResponse[0];
            $scope.headingName = $scope.RILanguage.houseHoldCreate;
            $scope.modalTitle = $scope.RILanguage.thankYou;
            $scope.message = $scope.RILanguage.thankYouCreated;
        });

        /************* Focus ***************************************/

        $scope.someFocusVariable = true;
        $scope.FocusMe = true;

        /***************** Get data ***********************************/

        $scope.status = 'active';

        $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;

        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;

        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;

        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;

        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;

        $scope.roledsply = Restangular.all('roles').getList().$object;

        $scope.severityofincidentsdsply = Restangular.all('severityofincidents?filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.reportincidentfollowupsdsply = Restangular.all('reportincidentfollowups?filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.typeofincidentsdsplyNew = Restangular.all('typeofincidents?filter[where][deleteFlag]=false').getList().$object;

        $scope.severityofincidentsdsplyNew = Restangular.all('severityofincidents?filter[where][deleteFlag]=false').getList().$object;

        $scope.currentstatusofcasesdsplyNew = Restangular.all('currentstatusofcases?filter[where][deleteFlag]=false').getList().$object;

        $scope.reportincidentfollowupsdsplyNew = Restangular.all('reportincidentfollowups?filter[where][deleteFlag]=false').getList().$object;

        var key;

        $scope.filterFields = ['multipleHHId', 'multiplemobile', 'multiplemembersname'];
        $scope.searchInput = '';

        Restangular.all('typeofincidents?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (tinci) {

            $scope.typeofincidentsdsply = tinci;

            Restangular.all('currentstatusofcases?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (curr) {
                $scope.currentstatusofcasesdsply = curr;

                $scope.typeofincidentsdsply = tinci;


                
                    if ($window.sessionStorage.roleId + "" === "3") {
                        $scope.filterCall = 'reportincidents?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                        
                         $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}}]}}';
                    } else {
                        $scope.filterCall = 'reportincidents?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                        
                        $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}}]}}';
                    }
                    
                     Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
                        
                    $scope.finalMembers = mems;
                    
                    Restangular.all($scope.filterCall).getList().then(function (rincident) {
                        $scope.reportincidents = rincident;

                        $scope.RIArray = [];
                        
                        $scope.modalInstanceLoad.close();

                        angular.forEach($scope.reportincidents, function (member, index) {
                            member.index = index + 1;

                            member.datedisply = $filter('date')(member.incidentdate, 'dd-MMM-yyyy');
                            member.datedisplyone = $filter('date')(member.followupdate, 'dd-MMM-yyyy');
                            member.incidentdate = $filter('date')(member.incidentdate, 'dd/MM/yyyy');
                            member.dateofclosure = $filter('date')(member.dateofclosure, 'dd/MM/yyyy');
                            member.followupdate = $filter('date')(member.followupdate, 'dd/MM/yyyy');
                            member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                            member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                            if (member.dateofclosure == null) {
                                member.dateofclosure = '';
                            }
                            if (member.lastModifiedDate == null) {
                                member.lastModifiedDate = '';
                            }

                            for (var a = 0; a < $scope.typeofincidentsdsply.length; a++) {
                                if ($window.sessionStorage.language == 1) {
                                    if (member.incidenttype == $scope.typeofincidentsdsply[a].id) {
                                        member.incidenttypename = $scope.typeofincidentsdsply[a].name;
                                        break;
                                    }
                                } else if ($window.sessionStorage.language != 1) {
                                    if (member.incidenttype == $scope.typeofincidentsdsply[a].parentId) {
                                        member.incidenttypename = $scope.typeofincidentsdsply[a].name;
                                        break;
                                    }
                                }
                            }

                            for (var a = 0; a < $scope.typeofincidentsdsplyNew.length; a++) {
                                if (member.incidenttype == $scope.typeofincidentsdsplyNew[a].id) {
                                    member.incidenttypeEngname = $scope.typeofincidentsdsplyNew[a].name;
                                    break;
                                }
                            }

                            for (var b = 0; b < $scope.currentstatusofcasesdsply.length; b++) {
                                if ($window.sessionStorage.language == 1) {
                                    if (member.currentstatus == $scope.currentstatusofcasesdsply[b].id) {
                                        member.currentstatusname = $scope.currentstatusofcasesdsply[b].name;
                                        break;
                                    }
                                } else if ($window.sessionStorage.language != 1) {
                                    if (member.currentstatus == $scope.currentstatusofcasesdsply[b].parentId) {
                                        member.currentstatusname = $scope.currentstatusofcasesdsply[b].name;
                                        break;
                                    }
                                }
                            }

                            for (var b = 0; b < $scope.currentstatusofcasesdsplyNew.length; b++) {
                                if (member.currentstatus == $scope.currentstatusofcasesdsplyNew[b].id) {
                                    member.currentstatusEngname = $scope.currentstatusofcasesdsplyNew[b].name;
                                    break;
                                }
                            }

                            for (var d = 0; d < $scope.reportincidentfollowupsdsply.length; d++) {
                                if ($window.sessionStorage.language == 1) {
                                    if (member.followupneeded == $scope.reportincidentfollowupsdsply[d].id) {
                                        member.followupneededname = $scope.reportincidentfollowupsdsply[d].name;
                                        break;
                                    }
                                } else if ($window.sessionStorage.language != 1) {
                                    if (member.followupneeded == $scope.reportincidentfollowupsdsply[d].parentId) {
                                        member.followupneededname = $scope.reportincidentfollowupsdsply[d].name;
                                        break;
                                    }
                                }
                            }

                            for (var d = 0; d < $scope.reportincidentfollowupsdsplyNew.length; d++) {
                                if (member.followupneeded == $scope.reportincidentfollowupsdsplyNew[d].id) {
                                    member.followupneededEngname = $scope.reportincidentfollowupsdsplyNew[d].name;
                                    break;
                                }
                            }

                            for (var dp = 0; dp < $scope.usersdisplay.length; dp++) {
                                if (member.associatedHF == $scope.usersdisplay[dp].id) {
                                    member.assignedTo = $scope.usersdisplay[dp].name;
                                    break;
                                }
                            }

                            for (var p = 0; p < $scope.countrydisplay.length; p++) {
                                if (member.countryId == $scope.countrydisplay[p].id) {
                                    member.countryname = $scope.countrydisplay[p].name;
                                    break;
                                }
                            }
                            for (var q = 0; q < $scope.statedisplay.length; q++) {
                                if (member.stateId == $scope.statedisplay[q].id) {
                                    member.statename = $scope.statedisplay[q].name;
                                    break;
                                }
                            }
                            for (var r = 0; r < $scope.districtdsply.length; r++) {
                                if (member.districtId == $scope.districtdsply[r].id) {
                                    member.districtname = $scope.districtdsply[r].name;
                                    break;
                                }
                            }
                            for (var s = 0; s < $scope.sitedsply.length; s++) {
                                if (member.siteId == $scope.sitedsply[s].id) {
                                    member.sitename = $scope.sitedsply[s].name;
                                    break;
                                }
                            }
                            for (var t = 0; t < $scope.usersdisplay.length; t++) {
                                if (member.createdBy == $scope.usersdisplay[t].id) {
                                    member.createdByname = $scope.usersdisplay[t].username;
                                    break;
                                }
                            }
                            for (var u = 0; u < $scope.usersdisplay.length; u++) {
                                if (member.lastModifiedBy == $scope.usersdisplay[u].id) {
                                    member.lastModifiedByname = $scope.usersdisplay[u].username;
                                    break;
                                }
                            }
                            for (var v = 0; v < $scope.roledsply.length; v++) {
                                if (member.lastModifiedByRole == $scope.roledsply[v].id) {
                                    member.lastModifiedByRolename = $scope.roledsply[v].name;
                                    break;
                                }
                            }
                            for (var w = 0; w < $scope.roledsply.length; w++) {
                                if (member.createdByRole == $scope.roledsply[w].id) {
                                    member.createdByRolename = $scope.roledsply[w].name;
                                    break;
                                }
                            }
                            for (var x = 0; x < $scope.severityofincidentsdsply.length; x++) {
                                if ($window.sessionStorage.language == 1) {
                                    if (member.severity == $scope.severityofincidentsdsply[x].id) {
                                        member.severityname = $scope.severityofincidentsdsply[x].name;
                                        break;
                                    }
                                } else if ($window.sessionStorage.language != 1) {
                                    if (member.severity == $scope.severityofincidentsdsply[x].parentId) {
                                        member.severityEngname = $scope.severityofincidentsdsply[x].name;
                                        break;
                                    }
                                }
                            }

                            for (var x = 0; x < $scope.severityofincidentsdsply.length; x++) {
                                if (member.severity == $scope.severityofincidentsdsplyNew[x].id) {
                                    member.severityEngname = $scope.severityofincidentsdsplyNew[x].name;
                                    break;
                                }
                            }

                            if (member.multiplemembers != null) {
                                member.multiplemembersLength = member.multiplemembers.split(',').length;
                                member.arraymultiplemembers = member.multiplemembers.split(",");
                                member.multiplemembersname = "";

                                for (var i = 0; i < member.arraymultiplemembers.length; i++) {
                                    for (var j = 0; j < $scope.finalMembers.length; j++) {
                                        if ($scope.finalMembers[j].id + "" === member.arraymultiplemembers[i] + "") {
                                            if (member.multiplemembersname === "") {
                                                member.multiplemembersname = $scope.finalMembers[j].name;
                                                member.multiplemobile = $scope.finalMembers[j].mobile;
                                                member.multipleHHId = $scope.finalMembers[j].HHId;
                                            } else {
                                                member.multiplemembersname = member.multiplemembersname + "," + $scope.finalMembers[j].name;
                                                member.multiplemobile = member.multiplemobile + "," + $scope.finalMembers[j].mobile;
                                                member.multipleHHId = member.multipleHHId + "," + $scope.finalMembers[j].HHId;
                                            }

                                            break;
                                        }
                                    }
                                }
                            } else {
                                member.multiplemembersname = "";
                                member.membersInGroupLength = 0;
                            }

                            for (key in member) {

                                if (member[key] === true) {
                                    member[key] = 'Yes';
                                } else if (member[key] === false) {
                                    member[key] = 'No';
                                }
                                // console.log(member[key]);
                            }

                            $scope.TotalData = [];
                            $scope.TotalData.push(member);
                        });
                    });
                });
            });
        });

        $scope.$watch('status', function (newValue, oldValue) {
            if (newValue === oldValue || newValue === '' || newValue === null) {
                return;
            } else {
                Restangular.all('typeofincidents').getList().then(function (tinci) {

                    $scope.typeofincidentsdsply = tinci;

                    Restangular.all('currentstatusofcases').getList().then(function (curr) {
                        $scope.currentstatusofcasesdsply = curr;

                        $scope.typeofincidentsdsply = tinci;


                        Restangular.all('members').getList().then(function (mem) {
                            $scope.finalMembers = mem;
                            if ($window.sessionStorage.roleId + "" === "3") {
                                $scope.filterCall = 'reportincidents?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                            } else {
                                $scope.filterCall = 'reportincidents?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                            }
                            Restangular.all($scope.filterCall).getList().then(function (rincident) {
                                $scope.reportincidents = rincident;

                                $scope.RIArray = [];

                                angular.forEach($scope.reportincidents, function (member, index) {
                                    member.index = index + 1;

                                    member.incidentdate = $filter('date')(member.incidentdate, 'dd/MM/yyyy');
                                    member.dateofclosure = $filter('date')(member.dateofclosure, 'dd/MM/yyyy');
                                    member.followupdate = $filter('date')(member.followupdate, 'dd/MM/yyyy');
                                    member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                                    member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                                    for (var a = 0; a < $scope.typeofincidentsdsply.length; a++) {
                                        if ($window.sessionStorage.language == 1) {
                                            if (member.incidenttype == $scope.typeofincidentsdsply[a].id) {
                                                member.incidenttypename = $scope.typeofincidentsdsply[a].name;
                                                break;
                                            }
                                        } else if ($window.sessionStorage.language != 1) {
                                            if (member.incidenttype == $scope.typeofincidentsdsply[a].parentId) {
                                                member.incidenttypename = $scope.typeofincidentsdsply[a].name;
                                                break;
                                            }
                                        }
                                    }

                                    for (var a = 0; a < $scope.typeofincidentsdsplyNew.length; a++) {
                                        if (member.incidenttype == $scope.typeofincidentsdsplyNew[a].id) {
                                            member.incidenttypeEngname = $scope.typeofincidentsdsplyNew[a].name;
                                            break;
                                        }
                                    }

                                    for (var b = 0; b < $scope.currentstatusofcasesdsply.length; b++) {
                                        if ($window.sessionStorage.language == 1) {
                                            if (member.currentstatus == $scope.currentstatusofcasesdsply[b].id) {
                                                member.currentstatusname = $scope.currentstatusofcasesdsply[b].name;
                                                break;
                                            }
                                        } else if ($window.sessionStorage.language != 1) {
                                            if (member.currentstatus == $scope.currentstatusofcasesdsply[b].parentId) {
                                                member.currentstatusname = $scope.currentstatusofcasesdsply[b].name;
                                                break;
                                            }
                                        }
                                    }

                                    for (var b = 0; b < $scope.currentstatusofcasesdsplyNew.length; b++) {
                                        if (member.currentstatus == $scope.currentstatusofcasesdsplyNew[b].id) {
                                            member.currentstatusEngname = $scope.currentstatusofcasesdsplyNew[b].name;
                                            break;
                                        }
                                    }

                                    for (var d = 0; d < $scope.reportincidentfollowupsdsply.length; d++) {
                                        if ($window.sessionStorage.language == 1) {
                                            if (member.followupneeded == $scope.reportincidentfollowupsdsply[d].id) {
                                                member.followupneededname = $scope.reportincidentfollowupsdsply[d].name;
                                                break;
                                            }
                                        } else if ($window.sessionStorage.language != 1) {
                                            if (member.followupneeded == $scope.reportincidentfollowupsdsply[d].parentId) {
                                                member.followupneededname = $scope.reportincidentfollowupsdsply[d].name;
                                                break;
                                            }
                                        }
                                    }

                                    for (var d = 0; d < $scope.reportincidentfollowupsdsplyNew.length; d++) {
                                        if (member.followupneeded == $scope.reportincidentfollowupsdsplyNew[d].id) {
                                            member.followupneededEngname = $scope.reportincidentfollowupsdsplyNew[d].name;
                                            break;
                                        }
                                    }

                                    for (var dp = 0; dp < $scope.usersdisplay.length; dp++) {
                                        if (member.associatedHF == $scope.usersdisplay[dp].id) {
                                            member.assignedTo = $scope.usersdisplay[dp].name;
                                            break;
                                        }
                                    }

                                    for (var p = 0; p < $scope.countrydisplay.length; p++) {
                                        if (member.countryId == $scope.countrydisplay[p].id) {
                                            member.countryname = $scope.countrydisplay[p].name;
                                            break;
                                        }
                                    }

                                    for (var q = 0; q < $scope.statedisplay.length; q++) {
                                        if (member.stateId == $scope.statedisplay[q].id) {
                                            member.statename = $scope.statedisplay[q].name;
                                            break;
                                        }
                                    }
                                    for (var r = 0; r < $scope.districtdsply.length; r++) {
                                        if (member.districtId == $scope.districtdsply[r].id) {
                                            member.districtname = $scope.districtdsply[r].name;
                                            break;
                                        }
                                    }
                                    for (var s = 0; s < $scope.sitedsply.length; s++) {
                                        if (member.siteId == $scope.sitedsply[s].id) {
                                            member.sitename = $scope.sitedsply[s].name;
                                            break;
                                        }
                                    }
                                    for (var t = 0; t < $scope.usersdisplay.length; t++) {
                                        if (member.createdBy == $scope.usersdisplay[t].id) {
                                            member.createdByname = $scope.usersdisplay[t].username;
                                            break;
                                        }
                                    }
                                    for (var u = 0; u < $scope.usersdisplay.length; u++) {
                                        if (member.lastModifiedBy == $scope.usersdisplay[u].id) {
                                            member.lastModifiedByname = $scope.usersdisplay[u].username;
                                            break;
                                        }
                                    }
                                    for (var v = 0; v < $scope.roledsply.length; v++) {
                                        if (member.lastModifiedByRole == $scope.roledsply[v].id) {
                                            member.lastModifiedByRolename = $scope.roledsply[v].name;
                                            break;
                                        }
                                    }
                                    for (var w = 0; w < $scope.roledsply.length; w++) {
                                        if (member.createdByRole == $scope.roledsply[w].id) {
                                            member.createdByRolename = $scope.roledsply[w].name;
                                            break;
                                        }
                                    }
                                    for (var x = 0; x < $scope.severityofincidentsdsply.length; x++) {
                                        if ($window.sessionStorage.language == 1) {
                                            if (member.severity == $scope.severityofincidentsdsply[x].id) {
                                                member.severityname = $scope.severityofincidentsdsply[x].name;
                                                break;
                                            }
                                        } else if ($window.sessionStorage.language != 1) {
                                            if (member.severity == $scope.severityofincidentsdsply[x].parentId) {
                                                member.severityEngname = $scope.severityofincidentsdsply[x].name;
                                                break;
                                            }
                                        }
                                    }

                                    for (var x = 0; x < $scope.severityofincidentsdsply.length; x++) {
                                        if (member.severity == $scope.severityofincidentsdsplyNew[x].id) {
                                            member.severityEngname = $scope.severityofincidentsdsplyNew[x].name;
                                            break;
                                        }
                                    }
                                    if (member.multiplemembers != null) {
                                        member.multiplemembersLength = member.multiplemembers.split(',').length;
                                        member.arraymultiplemembers = member.multiplemembers.split(",");
                                        member.multiplemembersname = "";

                                        for (var i = 0; i < member.arraymultiplemembers.length; i++) {
                                            for (var j = 0; j < $scope.finalMembers.length; j++) {
                                                if ($scope.finalMembers[j].id + "" === member.arraymultiplemembers[i] + "") {
                                                    if (member.multiplemembersname === "") {
                                                        member.multiplemembersname = $scope.finalMembers[j].name;
                                                        member.multiplemobile = $scope.finalMembers[j].mobile;
                                                        member.multipleHHId = $scope.finalMembers[j].HHId;
                                                    } else {
                                                        member.multiplemembersname = member.multiplemembersname + "," + $scope.finalMembers[j].name;
                                                        member.multiplemobile = member.multiplemobile + "," + $scope.finalMembers[j].mobile;
                                                        member.multipleHHId = member.multipleHHId + "," + $scope.finalMembers[j].HHId;
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    } else {
                                        member.multiplemembersname = "";
                                        member.membersInGroupLength = 0;
                                    }

                                    for (key in member) {

                                        if (member[key] === true) {
                                            member[key] = 'Yes';
                                        } else if (member[key] === false) {
                                            member[key] = 'No';
                                        }
                                        // console.log(member[key]);
                                    }

                                    $scope.TotalData = [];
                                    $scope.TotalData.push(member);
                                });
                            });
                        });
                    });
                });
            }
        });

        /**************************Export data to excel sheet ***************/

        $scope.DisableExport = false;
        $scope.valRiCount = 0;

        $scope.exportData = function () {
            $scope.RIArray = [];
            $scope.valRiCount = 0;
            if ($scope.reportincidents.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.reportincidents.length; c++) {
                    $scope.RIArray.push({
                        'Sr No': $scope.reportincidents[c].index,
                        'COUNTRY': $scope.reportincidents[c].countryname,
                        'STATE': $scope.reportincidents[c].statename,
                        'DISTRICT': $scope.reportincidents[c].districtname,
                        'SITE': $scope.reportincidents[c].sitename,
                        'INCIDENT DATE': $scope.reportincidents[c].incidentdate,
                        'INCIDENT TYPE': $scope.reportincidents[c].incidenttypeEngname,
                        'MULTIPLE PEOPLE AFFECTED': $scope.reportincidents[c].co,
                        'MEMBERS': $scope.reportincidents[c].multiplemembersname,
                        'ASSIGNED TO': $scope.reportincidents[c].assignedTo,
                        'PHYSICAL': $scope.reportincidents[c].physical,
                        'SEXUAL': $scope.reportincidents[c].sexual,
                        'CHILD RELATED': $scope.reportincidents[c].childrelated,
                        'EMOTIONAL': $scope.reportincidents[c].emotional,
                        'PROPERTY RELATED': $scope.reportincidents[c].propertyrelated,
                        'MENTAL': $scope.reportincidents[c].mental,
                        'WOUND': $scope.reportincidents[c].wound,
                        'CUT': $scope.reportincidents[c].cut,
                        'SEVERE PAIN': $scope.reportincidents[c].severepain,
                        'BLEEDING': $scope.reportincidents[c].bleeding,
                        'IMMOBILE': $scope.reportincidents[c].immobile,
                        'PASSING OUT': $scope.reportincidents[c].passingout,
                        'UNCOVEREDFOOD': $scope.reportincidents[c].uncoveredfood,
                        'WATER STAGNATION': $scope.reportincidents[c].waterstagnation,
                        'UNCLEARED GARBAGE': $scope.reportincidents[c].unclearedgarbage,
                        'SEVERITY OF INCIDENT': $scope.reportincidents[c].severityEngname,
                        'POLICE': $scope.reportincidents[c].police,
                        'FATHER IN LAW': $scope.reportincidents[c].fatherinlaw,
                        'MOTHER IN LAW': $scope.reportincidents[c].motherinlaw,
                        'SISTER/BROTHER IN LAW': $scope.reportincidents[c].sisbroinlaw,
                        'SON/DAUGHTER': $scope.reportincidents[c].sondaughter,
                        'OTHERS': $scope.reportincidents[c].othermem,
                        'HUSBANDs FRIENDS': $scope.reportincidents[c].husfriends,
                        'GOONS': $scope.reportincidents[c].goons,
                        'PARTNER/BOYFRIEND': $scope.reportincidents[c].partners,
                        'HUSBAND': $scope.reportincidents[c].husband,
                        'ANYOTHER FAMILY MEMBERS': $scope.reportincidents[c].anyfamilymember,
                        'DAUGHTER IN LAW': $scope.reportincidents[c].daughterinlaw,
                        'LANDLOARDS': $scope.reportincidents[c].landlord,
                        'FAMILY MEMBERS': $scope.reportincidents[c].familymember,
                        'REPORTED': $scope.reportincidents[c].reported,
                        'REPORTED TO POLICE': $scope.reportincidents[c].reportedpolice,
                        'REPORTED TO NGOs': $scope.reportincidents[c].reportedngos,
                        'REPORTED TO FRIENDS': $scope.reportincidents[c].reportedfriends,
                        'REPORTED TO PLV': $scope.reportincidents[c].reportedplv,
                        'REPORTED TO SG/SHG': $scope.reportincidents[c].shshg,
                        'REPORTED TO HFs': $scope.reportincidents[c].hfs,
                        'REPORTED TO DOCTOR/NURSE': $scope.reportincidents[c].doctornurse,
                        'REPORTED TO LEGAL AID CLINIC': $scope.reportincidents[c].reportedlegalaid,
                        'REPORTED TO GRAM PANCHAYAT': $scope.reportincidents[c].grampanchayat,
                        'TIME TO RESPOND': $scope.reportincidents[c].timetorespond,
                        'REFERRED TO HFs': $scope.reportincidents[c].referredhf,
                        'REFERRED FOR MEDICAL CARE': $scope.reportincidents[c].referredmedicalcare,
                        'REFERRED TO CO MANAGER': $scope.reportincidents[c].referredcomanager,
                        'REFERRED FOR HEAD OF GRAM PANCHAYAT': $scope.reportincidents[c].referredheadofgrampanchayat,
                        'REFERRED TO PLV': $scope.reportincidents[c].referredplv,
                        'REFERRED TO LEGAL AID CLINIC': $scope.reportincidents[c].referredlegalaid,
                        'REFERRED TO POLICE': $scope.reportincidents[c].referredpolice,
                        'REFERRED TO SG/SHG': $scope.reportincidents[c].referredsgshg,
                        'EMERGENCY ACTION TAKEN AT THE CLINIC': $scope.reportincidents[c].emergencyactionclinic,
                        'REFERRED TO HOSPITAL': $scope.reportincidents[c].referredhospital,
                        'COMPLAINT MADE IN SOFTWARE': $scope.reportincidents[c].complaintmade,
                        'COMMUNITY MOBILIZED': $scope.reportincidents[c].communitymobilized,
                        'PRIORITIZED FLOR SAWF ACTION': $scope.reportincidents[c].prioritizedflorsawfaction,
                        'CURRENT STATUS OF CASE': $scope.reportincidents[c].currentstatusEngname,
                        'FOLLOW UP REQUIRED': $scope.reportincidents[c].followupneededEngname,
                        'FOLLOW UP DATE': $scope.reportincidents[c].followupdate,
                        'DATE OF CLOSURE': $scope.reportincidents[c].dateofclosure,
                        'CREATED BY': $scope.reportincidents[c].createdByname,
                        'CREATED DATE': $scope.reportincidents[c].createdDate,
                        'CREATED ROLE': $scope.reportincidents[c].createdByRolename,
                        'LAST MODIFIED BY': $scope.reportincidents[c].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.reportincidents[c].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.reportincidents[c].lastModifiedByRolename,
                        'DELETE FLAG': $scope.reportincidents[c].deleteFlag
                    });

                    $scope.valRiCount++;
                    if ($scope.reportincidents.length == $scope.valRiCount) {
                        alasql('SELECT * INTO XLSX("reportincidents.xlsx",{headers:true}) FROM ?', [$scope.RIArray]);
                    }
                }
            }
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/reportincident-list") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            // console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************** Delete **********/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('reportincidents/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

    });
