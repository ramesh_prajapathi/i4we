'use strict';

angular.module('secondarySalesApp')
    .controller('DataMismatchCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        $scope.mismatchedSHG = [];
        $scope.mismatchedMember = [];
        $scope.mismatchedData = {};

        Restangular.all('shgs?filter[where][deleteFlag]=false').getList().then(function (allshgs) {
            var allmembers = [];
            Restangular.all('members?filter[where][deleteFlag]=false&filter[where][id][lte]=4000').getList().then(function (firstmembers) {
                //allmembers.push(firstmembers);
                Restangular.all('members?filter[where][deleteFlag]=false&filter[where][id][gt]=4000').getList().then(function (secondmembers) {
                    allmembers = firstmembers.concat(secondmembers);
                    console.log("allmembers",allmembers.length);
                    for (var i = 0; i < allshgs.length; i++) {
                        for (var j = 0; j < allmembers.length; j++) {
                            if (allmembers[j].shgId + '' === allshgs[i].id + '') {
                                if ((allshgs[i].membersInGroup.split(",")).indexOf(allmembers[j].id + '') == -1) {
                                    //                                $scope.mismatchedSHG.push(allshgs[i]);
                                    //                                $scope.mismatchedMember.push(allmembers[j]);

                                    $scope.mismatchedData = {};

                                    $scope.mismatchedData.GroupName = allshgs[i].groupName;
                                    $scope.mismatchedData.GroupId = allshgs[i].id;
                                    $scope.mismatchedData.MembersInGroup = allshgs[i].membersInGroup;

                                    $scope.mismatchedData.HHId = allmembers[j].HHId;
                                    $scope.mismatchedData.IndividualId = allmembers[j].individualId;
                                    $scope.mismatchedData.MemberName = allmembers[j].name;
                                    $scope.mismatchedData.MemberId = allmembers[j].id;
                                    $scope.mismatchedData.SHGId = allmembers[j].shgId;
                                    $scope.mismatchedMember.push($scope.mismatchedData);
                                }
                            }
                        }



                        if (allshgs[i].membersInGroup != null) {
                            $scope.memberArray = allshgs[i].membersInGroup.split(",");
                        } else {
                            $scope.memberArray = [];
                        }

                        for (var k = 0; k < $scope.memberArray.length; k++) {
                            for (var l = 0; l < allmembers.length; l++) {
                                if ($scope.memberArray[k] + '' === allmembers[l].id + '') {
                                    if (allmembers[l].shgId + '' != allshgs[i].id) {
                                        $scope.mismatchedData = {};

                                        $scope.mismatchedData.GroupName = allshgs[i].groupName;
                                        $scope.mismatchedData.GroupId = allshgs[i].id;
                                        $scope.mismatchedData.MembersInGroup = allshgs[i].membersInGroup;

                                        $scope.mismatchedData.HHId = allmembers[l].HHId;
                                        $scope.mismatchedData.IndividualId = allmembers[l].individualId;
                                        $scope.mismatchedData.MemberName = allmembers[l].name;
                                        $scope.mismatchedData.MemberId = allmembers[l].id;
                                        $scope.mismatchedData.SHGId = allmembers[l].shgId;
                                        $scope.mismatchedMember.push($scope.mismatchedData);
                                    }
                                }
                            }
                        }

                    }
                    //console.log('$scope.mismatchedSHG',$scope.mismatchedSHG);
                    console.log('$scope.mismatchedMember', $scope.mismatchedMember);
                });
            });
        });
    });