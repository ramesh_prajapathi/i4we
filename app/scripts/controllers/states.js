'use strict';

angular.module('secondarySalesApp')
    .controller('StatesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        $scope.modalTitle = 'Thank You';

        $scope.someFocusVariable = true;

        $scope.showForm = function () {
            var visible = $location.path() === '/state/create' || $location.path() === '/state/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/state/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/state/create' || $location.path() === '/state/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/state/create' || $location.path() === '/state/edit/' + $routeParams.id;
            return visible;
        };
        //		if ($window.sessionStorage.roleId != 1) {
        //			window.location = "/";
        //		}


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/state") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/

        //        Restangular.all('states?filter[where][deleteflag]=false').getList().then(function (state) {
        //            $scope.states = state;
        //            $scope.zoneId = $window.sessionStorage.sales_zoneId;
        //            angular.forEach($scope.states, function (member, index) {
        //                member.index = index + 1;
        //
        //                $scope.TotalTodos = [];
        //                $scope.TotalTodos.push(member);
        //            });
        //        });

        Restangular.all('countries?filter[where][deleteFlag]=false').getList().then(function (znes) {
            $scope.countries = znes;
            $scope.countryId = znes[0].id;
        });

        $scope.getCountry = function (countryId) {
            return Restangular.one('countries', countryId).get().$object;
        };

        /*-------------------------------------------------------------------------------------*/
        $scope.state = {
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            deleteFlag: false
        };

        $scope.statecodeDisable = false;
        $scope.membercountDisable = false;

        $scope.Displaystates = [];

        $scope.$watch('countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {

                $scope.stateArray = [];

                $window.sessionStorage.myRoute = newValue;
                Restangular.all('states?filter[where][countryId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (state) {
                    $scope.Displaystates = state;
                    // console.log('DisplaysalesAreas', sal);
                    angular.forEach($scope.Displaystates, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.countries.length; a++) {
                            if (member.countryId == $scope.countries[a].id) {
                                member.countryname = $scope.countries[a].name;
                                break;
                            }
                        }

                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        });

        if ($routeParams.id) {
            $scope.message = 'State has been Updated!';
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('states', $routeParams.id).get().then(function (state) {
                $scope.original = state;
                $scope.state = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'State has been created!';
        }
        /************* SAVE *******************************************/
        $scope.validatestring = '';

        $scope.submitDisable = false;

        $scope.SaveZone = function (clicked) {
            document.getElementById('name').style.border = "";
            document.getElementById('code').style.border = "";

            if ($scope.state.name == '' || $scope.state.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter State Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.state.code == '' || $scope.state.code == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter State Code';
                document.getElementById('code').style.borderColor = "#FF0000";

            } else if ($scope.state.sequence == '' || $scope.state.sequence == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Sequence';
                document.getElementById('sequence').style.borderColor = "#FF0000";

            } else if ($scope.state.countryId == '' || $scope.state.countryId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Country';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                //toaster.pop('success', "State has been created", null, null, 'trustedHtml');
                Restangular.all('states').post($scope.state).then(function (znResponse) {
                    //  console.log('znResponse', znResponse);
                    window.location = '/state-list';
                });
            }
        };

        /***************************** UPDATE *******************************************/
        $scope.UpdateZone = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('code').style.border = "";

            if ($scope.state.name == '' || $scope.state.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter State Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.state.code == '' || $scope.state.code == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter State Code';
                document.getElementById('code').style.borderColor = "#FF0000";

            } else if ($scope.state.countryId == '' || $scope.state.countryId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Country';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.one('states', $routeParams.id).customPUT($scope.state).then(function () {
                    //$location.path('/zones');
                    window.location = '/state-list';
                });
            }
        };

        /**************************Export data to excel sheet ***************/

        $scope.valSteCount = 0;

        $scope.exportData = function () {
            $scope.valSteCount = 0;
            // console.log('$scope.householdsArray', $scope.householdsArray);
            if ($scope.Displaystates.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.Displaystates.length; c++) {
                    $scope.stateArray.push({
                        'STATE': $scope.Displaystates[c].name,
                        'STATE CODE': $scope.Displaystates[c].code,
                        'COUNTRY': $scope.Displaystates[c].countryname,
                        'DELETE FLAG': $scope.Displaystates[c].deleteFlag
                    });

                    $scope.valSteCount++;
                    if ($scope.Displaystates.length == $scope.valSteCount) {
                        alasql('SELECT * INTO XLSX("states.xlsx",{headers:true}) FROM ?', [$scope.stateArray]);
                    }
                }
                // alasql('SELECT * INTO XLSX("states.xlsx",{headers:true}) FROM ?', [$scope.stateArray]);
            }
        };


        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        /*---------------------------Delete---------------------------------------------------*/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('states/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

    });

/********************************* Not In Use ************************


		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		
		$scope.validatestring = '';
		/*$scope.SaveZone = function () {
			document.getElementById('name').style.border = "";
			if ($scope.zone.name == '' || $scope.zone.name == null) {
				//$scope.zone.name = null;
				$scope.validatestring = $scope.validatestring + 'Please enter state name';
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			//	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				Restangular.all('zones').post($scope.zone).then(function (res) {
					console.log('Zone Saved', res);
					window.location = '/zones';
				});
			}
		};*/


/****************************************************************/