'use strict';

angular.module('secondarySalesApp')
    .controller('InterventionsListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/
        $rootScope.clickFW();
        $scope.InterventionLanguage = {};

        Restangular.one('interventionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.InterventionLanguage = langResponse[0];
            // $scope.message = langResponse[0].InterventionSaved;
        });

        $scope.UserLanguage = $window.sessionStorage.language;

        $scope.venues = Restangular.all('venues?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.types = Restangular.all('typeofinterventions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.venuesNew = Restangular.all('venues?filter[where][deleteFlag]=false').getList().$object;
        $scope.typesNew = Restangular.all('typeofinterventions?filter[where][deleteFlag]=false').getList().$object;

        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;
        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;
        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;
        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;
        $scope.roledsply = Restangular.all('roles').getList().$object;

        Restangular.all('users').getList().then(function (usrs) {

            $scope.finalUsers = usrs;

            if ($window.sessionStorage.roleId + "" === "3") {
                $scope.filterCall = 'interventions?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            } else {
                $scope.filterCall = 'interventions?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            }

            Restangular.all($scope.filterCall).getList().then(function (intr) {
                $scope.interventions = intr;

                $scope.displayInterventions = [];
                
                $scope.modalInstanceLoad.close();
                
                angular.forEach($scope.interventions, function (data, index) {
                    data.index = index + 1;

                    $scope.currentDate = new Date();
                    $scope.createdDate = new Date(data.createdDate);
                    //                    console.log($scope.createdDate);
                    //                    console.log($scope.currentDate);

                    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                    $scope.diffDays = Math.round(Math.abs(($scope.createdDate.getTime() - $scope.currentDate.getTime()) / (oneDay)));
                    //   console.log($scope.diffDays);

                    if ($scope.diffDays < 1) {
                        data.disableEdit = true;
                    } else {
                        data.disableEdit = false;
                    }

                    data.datedisply = $filter('date')(data.date, 'dd-MMM-yyyy');
                    data.date = $filter('date')(data.date, 'dd/MM/yyyy');
                    data.createdDate = $filter('date')(data.createdDate, 'dd/MM/yyyy');
                    data.lastModifiedDate = $filter('date')(data.lastModifiedDate, 'dd/MM/yyyy');
                    data.totalMembersCovered = data.maleMemberCovered + data.femaleMemberCovered;

                    for (var a = 0; a < $scope.finalUsers.length; a++) {
                        if (data.associatedHF == $scope.finalUsers[a].id) {
                            data.assignedTo = $scope.finalUsers[a].name;
                        }
                    }

                    for (var b = 0; b < $scope.venues.length; b++) {
                        if ($window.sessionStorage.language == 1) {
                            if ($scope.venues[b].id + "" === data.venue + "") {
                                data.venueName = $scope.venues[b].name;
                                break;
                            }
                        } else if ($window.sessionStorage.language != 1) {
                            if ($scope.venues[b].parentId + "" === data.venue + "") {
                                data.venueName = $scope.venues[b].name;
                                break;
                            }
                        }
                    }

                    for (var bb = 0; bb < $scope.venuesNew.length; bb++) {
                        if (data.venue == $scope.venuesNew[bb].id) {
                            data.venueEngName = $scope.venuesNew[bb].name;
                        }
                    }

                    for (var c = 0; c < $scope.types.length; c++) {
                        if ($window.sessionStorage.language == 1) {
                            if ($scope.types[c].id + "" === data.typeOfIntervention + "") {
                                data.typeName = $scope.types[c].name;
                                break;
                            }
                        } else if ($window.sessionStorage.language != 1) {
                            if ($scope.types[c].parentId + "" === data.typeOfIntervention + "") {
                                data.typeName = $scope.types[c].name;
                                break;
                            }
                        }
                    }

                    for (var cc = 0; cc < $scope.typesNew.length; cc++) {
                        if (data.typeOfIntervention == $scope.typesNew[cc].id) {
                            data.typeEngName = $scope.typesNew[cc].name;
                        }
                    }

                    for (var p = 0; p < $scope.countrydisplay.length; p++) {
                        if (data.countryId == $scope.countrydisplay[p].id) {
                            data.countryname = $scope.countrydisplay[p].name;
                            break;
                        }
                    }

                    for (var q = 0; q < $scope.statedisplay.length; q++) {
                        if (data.stateId == $scope.statedisplay[q].id) {
                            data.statename = $scope.statedisplay[q].name;
                            break;
                        }
                    }

                    for (var r = 0; r < $scope.districtdsply.length; r++) {
                        if (data.districtId == $scope.districtdsply[r].id) {
                            data.districtname = $scope.districtdsply[r].name;
                            break;
                        }
                    }

                    for (var s = 0; s < $scope.sitedsply.length; s++) {
                        if (data.siteId == $scope.sitedsply[s].id) {
                            data.sitename = $scope.sitedsply[s].name;
                            break;
                        }
                    }

                    for (var t = 0; t < $scope.finalUsers.length; t++) {
                        if (data.createdBy == $scope.finalUsers[t].id) {
                            data.createdByname = $scope.finalUsers[t].username;
                            break;
                        }
                    }
                    for (var u = 0; u < $scope.finalUsers.length; u++) {
                        if (data.lastModifiedBy == $scope.finalUsers[u].id) {
                            data.lastModifiedByname = $scope.finalUsers[u].username;
                            break;
                        }
                    }

                    for (var v = 0; v < $scope.roledsply.length; v++) {
                        if (data.lastModifiedByRole == $scope.roledsply[v].id) {
                            data.lastModifiedByRolename = $scope.roledsply[v].name;
                            break;
                        }
                    }

                    for (var w = 0; w < $scope.roledsply.length; w++) {
                        if (data.createdByRole == $scope.roledsply[w].id) {
                            data.createdByRolename = $scope.roledsply[w].name;
                            break;
                        }
                    }

                    $scope.displayInterventions.push(data);

                });
            });

        });

        /************** Watch Function **************************************/

        $scope.$watch('type', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '' || newValue == null) {
                return;
            } else {

                $scope.displayInterventions = [];

                Restangular.all('users').getList().then(function (usrs) {

                    $scope.finalUsers = usrs;

                    if ($window.sessionStorage.roleId + "" === "3") {
                        $scope.filterCall = 'interventions?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"typeOfIntervention":{"inq":[' + newValue + ']}}]}}';
                    } else {
                        $scope.filterCall = 'interventions?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"typeOfIntervention":{"inq":[' + newValue + ']}}]}}';
                    }

                    Restangular.all($scope.filterCall).getList().then(function (intr) {
                        $scope.interventions = intr;

                        $scope.displayInterventions = [];

                        angular.forEach($scope.interventions, function (data, index) {
                            data.index = index + 1;

                            $scope.currentDate = new Date();
                            $scope.createdDate = new Date(data.createdDate);

                            var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                            $scope.diffDays = Math.round(Math.abs(($scope.createdDate.getTime() - $scope.currentDate.getTime()) / (oneDay)));

                            if ($scope.diffDays < 1) {
                                data.disableEdit = true;
                            } else {
                                data.disableEdit = false;
                            }

                            data.datedisply = $filter('date')(data.date, 'dd-MMM-yyyy');
                            data.date = $filter('date')(data.date, 'dd/MM/yyyy');
                            data.createdDate = $filter('date')(data.createdDate, 'dd/MM/yyyy');
                            data.lastModifiedDate = $filter('date')(data.lastModifiedDate, 'dd/MM/yyyy');
                            data.totalMembersCovered = data.maleMemberCovered + data.femaleMemberCovered;

                            for (var a = 0; a < $scope.finalUsers.length; a++) {
                                if (data.associatedHF == $scope.finalUsers[a].id) {
                                    data.assignedTo = $scope.finalUsers[a].name;
                                }
                            }

                            for (var b = 0; b < $scope.venues.length; b++) {
                                if ($window.sessionStorage.language == 1) {
                                    if ($scope.venues[b].id + "" === data.venue + "") {
                                        data.venueName = $scope.venues[b].name;
                                        break;
                                    }
                                } else if ($window.sessionStorage.language != 1) {
                                    if ($scope.venues[b].parentId + "" === data.venue + "") {
                                        data.venueName = $scope.venues[b].name;
                                        break;
                                    }
                                }
                            }

                            for (var bb = 0; bb < $scope.venuesNew.length; bb++) {
                                if (data.venue == $scope.venuesNew[bb].id) {
                                    data.venueEngName = $scope.venuesNew[bb].name;
                                }
                            }

                            for (var c = 0; c < $scope.types.length; c++) {
                                if ($window.sessionStorage.language == 1) {
                                    if ($scope.types[c].id + "" === data.typeOfIntervention + "") {
                                        data.typeName = $scope.types[c].name;
                                        break;
                                    }
                                } else if ($window.sessionStorage.language != 1) {
                                    if ($scope.types[c].parentId + "" === data.typeOfIntervention + "") {
                                        data.typeName = $scope.types[c].name;
                                        break;
                                    }
                                }
                            }

                            for (var cc = 0; cc < $scope.typesNew.length; cc++) {
                                if (data.typeOfIntervention == $scope.typesNew[cc].id) {
                                    data.typeEngName = $scope.typesNew[cc].name;
                                }
                            }

                            for (var p = 0; p < $scope.countrydisplay.length; p++) {
                                if (data.countryId == $scope.countrydisplay[p].id) {
                                    data.countryname = $scope.countrydisplay[p].name;
                                    break;
                                }
                            }

                            for (var q = 0; q < $scope.statedisplay.length; q++) {
                                if (data.stateId == $scope.statedisplay[q].id) {
                                    data.statename = $scope.statedisplay[q].name;
                                    break;
                                }
                            }

                            for (var r = 0; r < $scope.districtdsply.length; r++) {
                                if (data.districtId == $scope.districtdsply[r].id) {
                                    data.districtname = $scope.districtdsply[r].name;
                                    break;
                                }
                            }

                            for (var s = 0; s < $scope.sitedsply.length; s++) {
                                if (data.siteId == $scope.sitedsply[s].id) {
                                    data.sitename = $scope.sitedsply[s].name;
                                    break;
                                }
                            }

                            for (var t = 0; t < $scope.finalUsers.length; t++) {
                                if (data.createdBy == $scope.finalUsers[t].id) {
                                    data.createdByname = $scope.finalUsers[t].username;
                                    break;
                                }
                            }
                            for (var u = 0; u < $scope.finalUsers.length; u++) {
                                if (data.lastModifiedBy == $scope.finalUsers[u].id) {
                                    data.lastModifiedByname = $scope.finalUsers[u].username;
                                    break;
                                }
                            }

                            for (var v = 0; v < $scope.roledsply.length; v++) {
                                if (data.lastModifiedByRole == $scope.roledsply[v].id) {
                                    data.lastModifiedByRolename = $scope.roledsply[v].name;
                                    break;
                                }
                            }

                            for (var w = 0; w < $scope.roledsply.length; w++) {
                                if (data.createdByRole == $scope.roledsply[w].id) {
                                    data.createdByRolename = $scope.roledsply[w].name;
                                    break;
                                }
                            }

                            $scope.displayInterventions.push(data);

                        });
                    });

                });

            }
        });

        /**************************Export data to excel sheet ***************/

        $scope.DisableExport = false;
        $scope.valIntCount = 0;

        $scope.exportData = function () {

            $scope.InterventionArray = [];

            $scope.valIntCount = 0;

            if ($scope.displayInterventions.length == 0) {
                alert('No data found');
            } else {

                for (var arr = 0; arr < $scope.displayInterventions.length; arr++) {

                    $scope.InterventionArray.push({
                        'Sr No': $scope.displayInterventions[arr].index,
                        'COUNTRY': $scope.displayInterventions[arr].countryname,
                        'STATE': $scope.displayInterventions[arr].statename,
                        'DISTRICT': $scope.displayInterventions[arr].districtname,
                        'SITE': $scope.displayInterventions[arr].sitename,
                        'DATE': $scope.displayInterventions[arr].date,
                        'ASSIGNED TO': $scope.displayInterventions[arr].assignedTo,
                        'VENUE': $scope.displayInterventions[arr].venueEngName,
                        'TYPE OF INTERVENTION': $scope.displayInterventions[arr].typeEngName,
                        'NO OF MALE MEMBERS COVERED': $scope.displayInterventions[arr].maleMemberCovered,
                        'NO OF MALE FEMEMBERS COVERED': $scope.displayInterventions[arr].femaleMemberCovered,
                        'TOTAL MEMBERS COVERED': $scope.displayInterventions[arr].totalMembersCovered,
                        'CREATED BY': $scope.displayInterventions[arr].createdByname,
                        'CREATED DATE': $scope.displayInterventions[arr].createdDate,
                        'CREATED ROLE': $scope.displayInterventions[arr].createdByRolename,
                        'LAST MODIFIED BY': $scope.displayInterventions[arr].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.displayInterventions[arr].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.displayInterventions[arr].lastModifiedByRolename,
                        'DELETE FLAG': $scope.displayInterventions[arr].deleteFlag
                    });

                    $scope.valIntCount++;

                    if ($scope.displayInterventions.length == $scope.valIntCount) {
                        alasql('SELECT * INTO XLSX("interventions.xlsx",{headers:true}) FROM ?', [$scope.InterventionArray]);
                    }
                }
            }
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

    });