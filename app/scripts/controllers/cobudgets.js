'use strict';
angular.module('secondarySalesApp').controller('cobudgetsCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $modal) {
    console.log('$window.sessionStorage.UserEmployeeId',$window.sessionStorage.UserEmployeeId);
    
    $scope.spmbudgetyears = Restangular.all('spmbudgetyears?filter[where][deleteflag]=false').getList().$object;
    $scope.Total_CO_HumanResource = 0;
    $scope.Total_CO_HumanResource_April = 0;
    
    Restangular.all('cobudgets?filter[where][facility]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=false').getList().then(function (AllbudgetResponse) {
        for (var i = 0; i < AllbudgetResponse.length; i++) {
           // console.log('AllbudgetResponse', AllbudgetResponse[i]);
            if(AllbudgetResponse[i].month == 4) {
            $scope.Total_CO_HumanResource_April = AllbudgetResponse[i].totalamount;
            }
            if(AllbudgetResponse[i].month == 5) {
            $scope.Total_CO_HumanResource_May = AllbudgetResponse[i].totalamount;
            }
            if(AllbudgetResponse[i].month == 6) {
            $scope.Total_CO_HumanResource_June = AllbudgetResponse[i].totalamount;
            }
            if(AllbudgetResponse[i].month == 7) {
            $scope.Total_CO_HumanResource_July = AllbudgetResponse[i].totalamount;
            }
            if(AllbudgetResponse[i].month == 8) {
            $scope.Total_CO_HumanResource_August = AllbudgetResponse[i].totalamount;
            }
            if(AllbudgetResponse[i].month == 9) {
            $scope.Total_CO_HumanResource_September = AllbudgetResponse[i].totalamount;
            }
            if(AllbudgetResponse[i].month == 10) {
            $scope.Total_CO_HumanResource_October = AllbudgetResponse[i].totalamount;
            }
            if(AllbudgetResponse[i].month == 11) {
            $scope.Total_CO_HumanResource_Nov = AllbudgetResponse[i].totalamount;
            }
            if(AllbudgetResponse[i].month == 12) {
            $scope.Total_CO_HumanResource_Dec = AllbudgetResponse[i].totalamount;
            }
            if(AllbudgetResponse[i].month == 1) {
            $scope.Total_CO_HumanResource_Jan = AllbudgetResponse[i].totalamount;
            }
            if(AllbudgetResponse[i].month == 2) {
            $scope.Total_CO_HumanResource_Feb = AllbudgetResponse[i].totalamount;
            }
            if(AllbudgetResponse[i].month == 3) {
            $scope.Total_CO_HumanResource_March = AllbudgetResponse[i].totalamount;
            }
            $scope.Total_CO_HumanResource = $scope.Total_CO_HumanResource + AllbudgetResponse[i].totalamount;
        }
        
        
        console.log('$scope.Total_CO_HumanResource', $scope.Total_CO_HumanResource);
    });
    console.log('UserEmp', $window.sessionStorage.UserEmployeeId);
    $scope.YearFunction = function (selyr) {
        console.log('selyr', selyr)
        Restangular.all('spmbudgets?filter[where][facility]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][year]=' + selyr + '&filter[where][deleteflag]=false').getList().then(function (res) {
           
            //$scope.balanceamount_humanresource = res[0].humanresources;
            $scope.spmBudget = res[0].totalamount;
            $scope.Balance_amount = $scope.spmBudget - $scope.Total_CO_HumanResource; 
             console.log('$scope.Balance_amount', $scope.Balance_amount)
        });
    }
    $scope.HumanresourceTotal = function () {};
    $scope.HumanresourceTotal1 = function () {
        $scope.cobudget.humanresources = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.cohumanresources);
    }
    $scope.TravelandCommunication = function () {}
    $scope.TravelandCommunication1 = function () {
        $scope.cobudget.travelandcommunication = parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.cotravelandcommunication);
    }
    $scope.Genericactivity = function () {}
    $scope.Genericactivity1 = function () {
        $scope.cobudget.genericactivity = parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.cogenericactivity);
    }
    $scope.Specificactivity = function () {}
    $scope.Specificactivity1 = function () {
        $scope.cobudget.specificactivity = parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.cospecificactivity);
    }
    $scope.Msgtgactivity = function () {}
    $scope.Msgtgactivity1 = function () {
        $scope.cobudget.msgtgactivity = parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.comsgtgactivity);
    }
    $scope.Administrativeexpense = function () {}
    $scope.Administrativeexpense1 = function () {
        $scope.cobudget.administrativeexpense = parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.coadministrativeexpense);
    }
    $scope.Monitoringevaluation = function () {}
    $scope.Monitoringevaluation1 = function () {
        $scope.cobudget.monitoringevaluation = parseInt($scope.cobudget.swastimonitoringevaluation) + parseInt($scope.cobudget.comonitoringevaluation);
    }
    $scope.cobudget = {
        deleteflag: false
        , facility: $window.sessionStorage.UserEmployeeId
        , lastmodifiedby: $window.sessionStorage.UserEmployeeId
        , lastmodifiedtime: new Date()
        , lastmodifiedbyrole: $window.sessionStorage.roleId
        
    };
    $scope.BudgetView = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            //$scope.callModal1();
            //alert("Cell index is: " + x);
            var x = document.getElementsByTagName("td");
            //console.log('this', x)
            // console.log('test',document.getElementById('test').innerHTML);
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[0].cellIndex;
            //console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.cobudget.month = 4;
            $scope.DisplayMonth = 'APRIL';
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    };
    $scope.BudgetView2 = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[1].cellIndex;
            console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.DisplayMonth = 'MAY';
            $scope.cobudget.month = 5;
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    };
    $scope.BudgetView3 = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[1].cellIndex;
            console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.DisplayMonth = 'JUNE';
            $scope.cobudget.month = 6;
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    }
    $scope.BudgetView4 = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[1].cellIndex;
            console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.DisplayMonth = 'JULY';
            $scope.cobudget.month = 7;
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    }
    $scope.BudgetView5 = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[1].cellIndex;
            console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.DisplayMonth = 'AUGUST';
            $scope.cobudget.month = 8;
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    }
    $scope.BudgetView6 = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[1].cellIndex;
            console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.DisplayMonth = 'SEPTEMBER';
            $scope.cobudget.month = 9;
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    }
    $scope.BudgetView7 = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[1].cellIndex;
            console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.DisplayMonth = 'OCTOBER';
            $scope.cobudget.month = 10;
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    }
    $scope.BudgetView8 = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[1].cellIndex;
            console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.DisplayMonth = 'NOVEMBER';
            $scope.cobudget.month = 11;
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    }
    $scope.BudgetView9 = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[1].cellIndex;
            console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.DisplayMonth = 'DECEMBER';
            $scope.cobudget.month = 12;
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    }
    $scope.BudgetView10 = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[1].cellIndex;
            console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.DisplayMonth = 'JANURARY';
            $scope.cobudget.month = 1;
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    }
    $scope.BudgetView11 = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[1].cellIndex;
            console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.DisplayMonth = 'FEBRUARY';
            $scope.cobudget.month = 2;
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    }
    $scope.BudgetView12 = function () {
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            var tdElem = document.getElementsByTagName("td");
            var tdText = tdElem[1].cellIndex;
            console.log('tdText', tdText);
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
            $scope.DisplayMonth = 'MARCH';
            $scope.cobudget.month = 3;
            Restangular.one('spmbudgetyears', $scope.cobudget.year).get().then(function (yr) {
                $scope.Current_Year = yr.name;
            });
        }
    }
    $scope.Cancel = function () {
            $scope.modalAW.close();
        }
        /*  $(document).ready(function () {
              colSum();
             // console.log('Readyy');
              //console.log('$scope.callSwastiSum()')
              // $scope.callSwastiSum();
          });

          function colSum() {
              var sum = 0;
              //iterate through each input and add to sum
              $('td').each(function () {
                  sum += parseInt($(this).text());
                  
              });
              //change value of total
              $('#mySum').html(sum);
              console.log('sum',sum);
              console.log('Mysum',$('#mySum').html(sum));
              console.log('Readyy call');
             // $scope.Swasti_Total = parseInt($scope.cobudget.swastimonitoringevaluation) + parseInt($scope.cobudget.swastitravelandcommunication);
          }*/
    $scope.Swasti_Total = '';
    $scope.SwastiSum1 = function () {
        // console.log('$scope.cobudget.swastihumanresources',$scope.cobudget.swastihumanresources);
        /* if($scope.cobudget.swastihumanresources == '' || $scope.cobudget.swastitravelandcommunication || $scope.cobudget.swastigenericactivity == '' ||$scope.cobudget.swastispecificactivity == '' || $scope.cobudget.swastimsgtgactivity == '' || $scope.cobudget.swastiadministrativeexpense == '' || $scope.cobudget.swastimonitoringevaluation == ''){
            $scope.Swasti_Total = 0
            } 
         if($scope.cobudget.swastitravelandcommunication == '' || $scope.cobudget.swastigenericactivity == '' ||$scope.cobudget.swastispecificactivity == '' || $scope.cobudget.swastimsgtgactivity == '' || $scope.cobudget.swastiadministrativeexpense == '' || $scope.cobudget.swastimonitoringevaluation == ''){
                $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources);
                
            } 
         if($scope.cobudget.swastigenericactivity == '' ||$scope.cobudget.swastispecificactivity == '' || $scope.cobudget.swastimsgtgactivity == '' || $scope.cobudget.swastiadministrativeexpense == '' || $scope.cobudget.swastimonitoringevaluation == ''){
                $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication);
            }*/
        /*if($scope.cobudget.swastihumanresources == '' || $scope.cobudget.swastihumanresources == null || $scope.cobudget.swastihumanresources == undefined){
            $scope.cobudget.swastihumanresources = 0;
          $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) +parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        } 
        else if($scope.cobudget.swastitravelandcommunication == '' || $scope.cobudget.swastitravelandcommunication == null || $scope.cobudget.swastitravelandcommunication == undefined){
            $scope.cobudget.swastitravelandcommunication = 0;
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) +parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        }
        else if($scope.cobudget.swastigenericactivity == '' || $scope.cobudget.swastigenericactivity == null || $scope.cobudget.swastigenericactivity == undefined){
            $scope.cobudget.swastigenericactivity = 0;
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) +parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        } else {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) +parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        }*/
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
        
    };
    $scope.SwastiSum2 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.SwastiSum3 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.SwastiSum4 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.SwastiSum5 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.SwastiSum6 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.SwastiSum7 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    }
    $scope.COSum1 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum2 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum3 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum4 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum5 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum6 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum7 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.validatestring = '';
    $scope.Save = function () {
        document.getElementById('swastihumanresources').style.borderColor = "";
        document.getElementById('cohumanresources').style.borderColor = "";
        document.getElementById('swastitravelandcommunication').style.borderColor = "";
        document.getElementById('cotravelandcommunication').style.borderColor = "";
        document.getElementById('swastigenericactivity').style.borderColor = "";
        document.getElementById('cogenericactivity').style.borderColor = "";
        document.getElementById('swastispecificactivity').style.borderColor = "";
        document.getElementById('cospecificactivity').style.borderColor = "";
        document.getElementById('swastimsgtgactivity').style.borderColor = "";
        document.getElementById('comsgtgactivity').style.borderColor = "";
        document.getElementById('swastiadministrativeexpense').style.borderColor = "";
        document.getElementById('coadministrativeexpense').style.borderColor = "";
        document.getElementById('swastimonitoringevaluation').style.borderColor = "";
        document.getElementById('comonitoringevaluation').style.borderColor = "";
        if ($scope.cobudget.swastihumanresources == '' || $scope.cobudget.swastihumanresources == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount By Swasti';
            document.getElementById('swastihumanresources').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.cohumanresources == '' || $scope.cobudget.cohumanresources == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount By CO';
            document.getElementById('cohumanresources').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastitravelandcommunication == '' || $scope.cobudget.swastitravelandcommunication == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount By Swasti';
            document.getElementById('swastitravelandcommunication').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.cotravelandcommunication == '' || $scope.cobudget.cotravelandcommunication == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount By CO';
            document.getElementById('cotravelandcommunication').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastigenericactivity == '' || $scope.cobudget.swastigenericactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount By Swasti';
            document.getElementById('swastigenericactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.cogenericactivity == '' || $scope.cobudget.cogenericactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount By CO';
            document.getElementById('cogenericactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastispecificactivity == '' || $scope.cobudget.swastispecificactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount By Swasti';
            document.getElementById('swastispecificactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.cospecificactivity == '' || $scope.cobudget.cospecificactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount By CO';
            document.getElementById('cospecificactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastimsgtgactivity == '' || $scope.cobudget.swastimsgtgactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter MSM/TG Activity Amount By Swasti';
            document.getElementById('swastimsgtgactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.comsgtgactivity == '' || $scope.cobudget.comsgtgactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter MSM/TG Activity Amount By CO';
            document.getElementById('comsgtgactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastiadministrativeexpense == '' || $scope.cobudget.swastiadministrativeexpense == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount By Swasti';
            document.getElementById('swastiadministrativeexpense').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.coadministrativeexpense == '' || $scope.cobudget.coadministrativeexpense == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount By CO';
            document.getElementById('coadministrativeexpense').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastimonitoringevaluation == '' || $scope.cobudget.swastimonitoringevaluation == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount By Swasti';
            document.getElementById('swastimonitoringevaluation').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.comonitoringevaluation == '' || $scope.cobudget.comonitoringevaluation == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount By CO';
            document.getElementById('comonitoringevaluation').style.borderColor = "#FF0000";
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            console.log('SAVE', $scope.cobudget);
            $scope.cobudget.balancetilldate =  $scope.spmBudget - $scope.cobudget.totalamount;
            Restangular.all('cobudgets').post($scope.cobudget).then(function (cobudgetsRes) {
                console.log('cobudgetsRes', cobudgetsRes);
                $scope.modalAW.close();
                $route.reload();
            });
        }
    }
    $scope.modalTitle = 'Thank You';
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.modalInstance1 = $modal.open({
            animation: true
            , templateUrl: 'template/validation.html'
            , scope: $scope
            , backdrop: 'static'
            , size: 'sm'
        });
    };
    $scope.OkValidation = function () {
        $scope.modalInstance1.close();
    };
    
   
   /* $scope.budgetDisable = false;
    $scope.Budget = function () {
        document.getElementById('name').style.borderColor = "";
        if ($scope.spmbudgetyear.name == '' || $scope.spmbudgetyear.name == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Budget Year';
            document.getElementById('name').style.borderColor = "#FF0000";
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            $scope.budgetDisable = true;
            Restangular.all('spmbudgetyears').post($scope.spmbudgetyear).then(function (bugdet) {
                console.log('budget', bugdet);
                $scope.budgetDisable = false;
                // $scope.modalInstance1.close();
                $route.reload();
            });
        }
    }*/
});