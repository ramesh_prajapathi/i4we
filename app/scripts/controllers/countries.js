'use strict';

angular.module('secondarySalesApp')
    .controller('CountriesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
        /*********/
    
        $scope.someFocusVariable = true;

        $scope.showForm = function () {
            var visible = $location.path() === '/country/create' || $location.path() === '/country/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/country/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/country/create' || $location.path() === '/country/edit/' + $routeParams.id;
            return visible;
        };

        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/country/create' || $location.path() === '/country/edit/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/country") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /****************************************************************************************INDEX****************************/

        $scope.searchCon = $scope.name;

        Restangular.all('countries?filter[where][deleteFlag]=false').getList().then(function (part) {
            $scope.countries = part;
            $scope.countryId = $window.sessionStorage.sales_countryId;

            $scope.countryArray = [];

            angular.forEach($scope.countries, function (member, index) {
                member.index = index + 1;

                $scope.countryArray.push({
                    'COUNTRY': member.name,
                    'DELETE FLAG': member.deleteFlag
                });

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        $('#name').keypress(function (evt) {
            if (/^[a-zA-Z ]*$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $scope.country = {
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteFlag: false
        };

        /****************************************************************************CREATE************************************/
        $scope.validatestring = '';
        $scope.Save = function () {
            document.getElementById('name').style.border = "";

            if ($scope.country.name == '' || $scope.country.name == null) {
                $scope.country.name = null;
                $scope.validatestring = $scope.validatestring + 'Please enter your country name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                Restangular.one('countries/findOne?filter[where][name]=' + $scope.country.name).get().then(function (ctry) {
                    // console.log('ctry', ctry);
                    alert('Existing Country, Please Change Country Name');
                    $scope.country.name = '';
                }, function (response) {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

                    $scope.countries.post($scope.country).then(function () {
                        // console.log('$scope.country', $scope.country);
                        window.location = '/country-list';
                    });
                });
            }
        };

        /***********************************************************************************UPDATE****************************/

        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.country.name == '' || $scope.country.name == null) {
                $scope.country.name = null;
                $scope.validatestring = $scope.validatestring + 'Please enter your country name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

                $scope.countries.customPUT($scope.country).then(function () {
                    // console.log('$scope.country', $scope.country);
                    window.location = '/country-list';
                });
            }
        };

        $scope.modalTitle = 'Thank You';

        if ($routeParams.id) {
            $scope.message = 'Country has been Updated!';
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('countries', $routeParams.id).get().then(function (country) {
                $scope.original = country;
                $scope.country = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Country has been created!';
        }

        /**********************************************************************************************DELETE*************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('countries/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /**************************Export data to excel sheet ***************/

        $scope.exportData = function () {
            // console.log('$scope.householdsArray', $scope.householdsArray);
            if ($scope.countryArray.length == 0) {
                alert('No data found');
            } else {
                alasql('SELECT * INTO XLSX("countries.xlsx",{headers:true}) FROM ?', [$scope.countryArray]);
            }
        };


        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

    });