'use strict';

angular.module('secondarySalesApp')
    .controller('COOrgEditCtrl', function ($scope, Restangular, $routeParams, $filter, $timeout, $window, $route, AnalyticsRestangular) {
        if ($window.sessionStorage.roleId != 5) {
            window.location = "/";
        }
        $scope.isCreateView = false;
        $scope.nameDisable = true;
        $scope.stateDisable = true;
        $scope.districDisable = true;
        $scope.heading = 'Facility/CO';

        $scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + $window.sessionStorage.zoneId).getList().$object;
        $scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
        /******************************************* Not In Use ***************************
		$scope.cocategories = Restangular.all('cocategories').getList().$object;
		$scope.submitpartners = Restangular.all('comembers').getList().$object;
		$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;
		$scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
		$scope.salesAreas = Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().$object;
	
		$scope.PillarChanged = function (state) {
			console.log(state);
			$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + state).getList().$object;
		};
	
	
		$scope.documenthide = true;
		$scope.$watch('partner.nooftis', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				if (newValue < 0 || newValue > 5) {
					alert('Invalid Entry');
					$scope.partner.nooftis = null;

					if (newValue == 0) {
						$scope.partner.tionerating = null;
						$scope.partner.titworating = null;
						$scope.partner.tithreerating = null;
						$scope.partner.tifourrating = null;
						$scope.partner.tifiverating = null;
					}

					if (newValue == 1) {
						$scope.partner.titworating = null;
						$scope.partner.tithreerating = null;
						$scope.partner.tifourrating = null;
						$scope.partner.tifiverating = null;
					}

					if (newValue == 2) {
						$scope.partner.tithreerating = null;
						$scope.partner.tifourrating = null;
						$scope.partner.tifiverating = null;
					}

					if (newValue == 3) {
						$scope.partner.tifourrating = null;
						$scope.partner.tifiverating = null;
					}

					if (newValue == 4) {
						$scope.partner.tifiverating = null;
					}

				}
			}
		});


		//Datepicker settings start
		$scope.today = function () {
			$scope.dt = $filter('date')(new Date(), 'y-MM-dd');
		};
		$scope.today();

		$scope.showWeeks = true;
		$scope.toggleWeeks = function () {
			$scope.showWeeks = !$scope.showWeeks;
		};

		$scope.clear = function () {
			$scope.dt = null;
		};

		// Disable weekend selection
	
		$scope.toggleMin = function () {
			$scope.minDate = ($scope.minDate) ? null : new Date();
		};
		$scope.toggleMin();

		$scope.open = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();

			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.opened = true;
		};

		$scope.open2 = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();

			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.opened2 = true;
		};

		$scope.open3 = function ($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened3 = true;
		};

		$scope.open4 = function ($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened4 = true;
		};

		$scope.dateOptions = {
			'year-format': 'yy',
			'starting-day': 1
		};

		$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
		$scope.format = $scope.formats[0];
		//Datepicker settings end

	/****************************************************************************/
        /*$scope.user = {
			flag: true,
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			deleteflag: false,
			roleId: 5,
			status: 'active',
			salesAreaId: $window.sessionStorage.salesAreaId,
			coorgId: $window.sessionStorage.coorgId,
			zoneId: $window.sessionStorage.zoneId
		};*/
        $scope.partner = {};
        $scope.auditlog = {
            description: 'Manager/HDF Profile Update',
            modifiedbyroleid: $window.sessionStorage.roleId,
            modifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            entityroleid: 53,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            facility: $window.sessionStorage.coorgId,
            facilityId: $window.sessionStorage.UserEmployeeId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId
        };



        Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (fwResponse) {
            $scope.facilityName = fwResponse.name;
            $scope.partner.facility = fwResponse.id;
        });

        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (partner) {
            $scope.original = partner;
            $scope.partner = Restangular.copy($scope.original);
            $scope.modalInstanceLoad.close();

        });

        /************************************ Update *******************************************/
        Restangular.all('users?filter[where][employeeid]=' + $routeParams.id + '&filter[where][roleId]=5').getList().then(function (submituser) {
            $scope.getUserId = submituser[0].id;
            $scope.original = submituser[0];
            $scope.user = Restangular.copy($scope.original);
            console.log('$scope.getUserId', $scope.getUserId);
        });

        $scope.createDisabled = false;
        $scope.usr = {};
        $scope.Update = function () {
            $scope.createDisabled = true;
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).customPUT($scope.partner).then(function (submitfs) {
                $scope.auditlog.entityid = submitfs.id;
                var usrnme = submitfs.name;
                usrnme = usrnme.toLowerCase().replace(' ', '');
                if ($scope.usr.password != null) {
                    $scope.user.password = $scope.usr.password;
                }
                //$scope.user.username = usrnme;
                //$scope.user.mobile = submitfs.helpline;
                //$scope.user.employeeid = submitfs.id;
                //$scope.user.email = submitfs.email;
                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                    //console.log('$scope.user',$scope.user);
                    Restangular.one('users/' + $scope.getUserId).customPUT($scope.user).then(function (submituser) {
                        //console.log('submituser', submituser);
                        AnalyticsRestangular.one('users/' + $scope.getUserId).customPUT($scope.user).then(function (analyticssubResponse) {
                            window.location = '/';
                        });
                    });
                });
            });
        };


        $scope.partner = {
            address: ''
        }



        /*
			$scope.Update = function () {
				if ($scope.fieldworker.firstname == '' || $scope.fieldworker.firstname == null) {
					$scope.validatestring = $scope.validatestring + 'Plese Enter User Name';
				} else if ($scope.fieldworker.lastname == '' || $scope.fieldworker.lastname == null) {
					$scope.validatestring = $scope.validatestring + 'Plese Enter Last Name';
				} else if ($scope.fieldworker.mobile == '' || $scope.fieldworker.mobile == null) {
					$scope.validatestring = $scope.validatestring + 'Plese Enter Mobile Number';
				} else if ($scope.fieldworker.email == '' || $scope.fieldworker.email == null) {
					$scope.validatestring = $scope.validatestring + 'Plese Enter Email';
				}
				if ($scope.validatestring != '') {
					$scope.toggleValidation();
					$scope.validatestring1 = $scope.validatestring;
					$scope.validatestring = '';
					$scope.dataModal = !$scope.dataModal;
				} else {
					$scope.UpdateClicked = true;
					$scope.createDisabled = true;
					Restangular.one('fieldworkers', $routeParams.id).customPUT($scope.fieldworker).then(function (submitfw) {

						$scope.auditlog.entityid = submitfw.id;
						$scope.user.username = submitfw.firstname;
						$scope.user.mobile = submitfw.mobile;
						$scope.user.employeeid = submitfw.id;
						$scope.user.email = submitfw.email;

						Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
							$scope.UpdateComember.fwcount = +($scope.fieldworker.fwcode) + 1;
							
							Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).customPUT($scope.UpdateComember).then(function (comember) {
							});
						
							Restangular.one('users/' + $scope.getUserId).customPUT($scope.user).then(function (submituser) {
								//console.log('submituser', submituser);
								$scope.dataModal = !$scope.dataModal;
							});
						});
					}, function (error) {
						$scope.UpdateClicked = false;
						//console.log('fw error', error);
						if (error.data.error.constraint == 'unique_firstname') {
							$scope.toggleValidation();
							$scope.validatestring1 = "Username already exits please refresh the page and try again";
						} else if (error.data.error.constraint == 'unique_email') {
							$scope.toggleValidation();
							$scope.validatestring1 = "email id already exits";
						}
					});
				}*/


        ///////////////////////////////////////////////////////MAP//////////////////////////

        $scope.showMapModal = false;
        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                $scope.partner.latitude = latLng.lat() + ',' + latLng.lng();
                $scope.partner.longitude = latLng.lng();

                document.getElementById('info').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
            }
            var map;

            function initialize() {

                $scope.address = $scope.partner.address;
                // console.log('$scope.address', $scope.address);
                $scope.latitude = 21.0000;
                $scope.longitude = 78.0000;
                if ($scope.address.length > 0) {
                    var addressgeocoder = new google.maps.Geocoder();
                    addressgeocoder.geocode({
                        'address': $scope.address
                    }, function (results, status) {

                        if (status == google.maps.GeocoderStatus.OK) {
                            $scope.latitude = parseInt(results[0].geometry.location.lat());
                            $scope.longitude = parseInt(results[0].geometry.location.lng());
                            //console.log($scope.latitude, $scope.longitude);

                            var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                            map = new google.maps.Map(document.getElementById('mapCanvas'), {
                                zoom: 4,
                                center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            });
                            var marker = new google.maps.Marker({
                                position: latLng,
                                title: 'Point A',
                                map: map,
                                draggable: true
                            });

                            // Update current position info.
                            updateMarkerPosition(latLng);
                            geocodePosition(latLng);

                            // Add dragging event listeners.
                            google.maps.event.addListener(marker, 'dragstart', function () {
                                updateMarkerAddress('Dragging...');
                            });

                            google.maps.event.addListener(marker, 'drag', function () {
                                updateMarkerStatus('Dragging...');
                                updateMarkerPosition(marker.getPosition());
                            });

                            google.maps.event.addListener(marker, 'dragend', function () {
                                updateMarkerStatus('Drag ended');
                                geocodePosition(marker.getPosition());
                            });
                        }
                    });
                } else {
                    $scope.latitude = 21.0000;
                    $scope.longitude = 78.0000;

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 4,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });

                }


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(4);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };

        $scope.CancelMap = function () {
            if ($scope.mapcount == 0) {
                $scope.showMapModal = !$scope.showMapModal;
                $scope.mapcount++;
            }
        };

        /******************** Language *****************************/
        $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.facilityprofileheader = langResponse.facilityprofileheader;
            $scope.addnew = langResponse.addnew;
            $scope.date = langResponse.date;
            $scope.eventname = langResponse.eventname;
            $scope.show = langResponse.show;
            $scope.entry = langResponse.entry;
            $scope.searchfor = langResponse.searchfor;
            $scope.action = langResponse.action;
            $scope.create = langResponse.create;
            $scope.update = langResponse.update;
            $scope.cancel = langResponse.cancel;

            $scope.name = langResponse.name;
            $scope.printmobile = langResponse.mobile;
            $scope.anotherhelplineno = langResponse.anotherhelplineno;
            $scope.address = langResponse.address;
            $scope.comanagerhdfprofileheader = langResponse.comanagerhdfprofileheader;
            $scope.comanagerhdfid = langResponse.comanagerhdfid;
            $scope.latlong = langResponse.latlong;
            $scope.facilityid = langResponse.facilityid;
            $scope.state = langResponse.state;
            $scope.district = langResponse.district;
            $scope.divbasicinfo = langResponse.divbasicinfo;
            $scope.divuserinfo = langResponse.divuserinfo;
            $scope.username = langResponse.username;
            $scope.password = langResponse.password;
        });



    });