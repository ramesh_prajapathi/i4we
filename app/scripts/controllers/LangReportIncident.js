'use strict';

angular.module('secondarySalesApp')
    .controller('LangRICtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/



        $scope.HideSubmitButton = true;
        $scope.langdisable = false;

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        $scope.$watch('reportincident.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                
                  Restangular.one('riLanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.reportincident = Restangular.copy($scope.original);
                    });
                
                Restangular.all('riLanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                        $scope.HideSubmitButton = true;
                    } else {
                        // $scope.LangId = response[0].id;
                       // $scope.HideSubmitButton = false;
                       // $scope.langdisable = true;

                        //$scope.reportincident = response[0];
                        // console.log('$scope.reportincident', $scope.reportincident);
                        $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';

                    }
                });

            }
        });
    
        /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/LangReportincidentlist';

        };
    
    
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/

        $scope.reportincident = {
            deleteFlag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.UserEmployeeId
        };




        $scope.Save = function () {
            Restangular.all('riLanguages').post($scope.reportincident).then(function (riResponse) {
                console.log('riResponse', riResponse);
                window.location = '/LangReportincidentlist';
            });

        };

        $scope.Update = function () {
            Restangular.one('riLanguages', $routeParams.id).customPUT($scope.reportincident).then(function (riResponse) {
                console.log('riResponse', riResponse);
                window.location = '/LangReportincidentlist';
            });
        };

        if ($routeParams.id) {

            $scope.HideSubmitButton = false;
            $scope.langdisable = true;
            Restangular.one('riLanguages', $routeParams.id).get().then(function (reportincident) {
                $scope.original = reportincident;
                $scope.reportincident = Restangular.copy($scope.original);
            });
        }
    });
