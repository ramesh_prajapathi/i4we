'use strict';

angular.module('secondarySalesApp')
    .controller('FWDashboardCtrl', function ($scope, Restangular,$window) {

        /*=============================================================================================*/
        /*Field worker Analytics*/
        /*=============================================================================================*/

        var dtgFormat = d3.time.format("%Y-%m-%d");
        var tmestmFormat = d3.time.format("%H:%M:%S.%LZ");
        var iso = d3.time.format.utc("%Y-%m-%dT%H:%M:%S.%LZ");
        var hrformat = d3.time.format("%H:%M %p");

        var dtgFormat1 = d3.time.format("%Y-%m-%d:%H:%M:%S.%LZ");

        //dtgFormat1 = d3.time.format("%Y-%m-%dT%H:%M:%S");

        //dtgFormat1 = d3.time.format.utc("%Y-%m-%dT%H:%M:%SZ");


        var fwNameLogin = getUrlVars()["myparams"];
        var fwRollLogin = getUrlVars()["myroll"];
        var fwuserid = $window.sessionStorage.UserEmployeeId;
        $scope.currentUserName = $window.sessionStorage.userName;
        $scope.RoleName = 'FW';

        //console.log(fwRollLogin);

        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                vars[key] = value;
            });
            return vars;
        }



        //var val1 = 'testing';
        document.getElementById("cookie1").innerHTML = fwNameLogin;
        document.getElementById("cookie2").innerHTML = fwRollLogin;

        var today = new Date();
        //console.log(today);
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        var hrs = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = dd + '/' + mm + '/' + yyyy + ':' + hrs + ':' + min + ':' + sec;

        //document.write(today);
        //console.log(today);
        document.getElementById("todaydatefw").innerHTML = today;

        var http = new XMLHttpRequest();
        var url = "http://swastianalyticstest.herokuapp.com/api/v1/users/login";
        var params = "email=demo@demo.com&password=1";
        http.open("POST", url, true);

        //Send the proper header information along with the request
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.setRequestHeader("Content-length", params.length);
        http.setRequestHeader("Connection", "close");

        http.send(params); // Returned data, e.g., an HTML document.
        http.onload = function () {
            var status = http.status; // HTTP response status, e.g., 200 for "200 OK"
            var data = http.responseText;
            var obj = JSON.parse(data);
            var accesstoken = obj.id;

            //var dataurl = "https://swastianalyticstest.herokuapp.com/api/v1/site_f?access_token="+accesstoken;
            //var ben_dataurl = "https://swastianalyticstest.herokuapp.com/api/v1/beneficiary_f?access_token="+accesstoken;
            var mytab_name
            var dataurl = "https://swastianalyticstest.herokuapp.com/api/v1/site_f?filter[where][fieldworker_id]=" + fwuserid + "&access_token=" + accesstoken;

            d3.json(dataurl, function (error, alldata) {


                //console.log(alldata);


                alldata.forEach(function (d) {

                });

                var nwx = crossfilter(alldata);

                var etl_job = "Target_job";
                var etl_message_type = "end";
                var etl_message = "success";

                d3.json("https://swastianalyticstest.herokuapp.com/api/v1/etl_job_history?filter[where][job]=" + etl_job + "&[message_type]=" + etl_message_type + "&[message]=" + etl_message + "&access_token=" + accesstoken, function (error, etldata) {
                    dtgFormat = d3.time.format("%Y-%m-%d");
                    etldata.forEach(function (d) {


                        d.etllastdatetime = iso.parse(d.moment);

                        var etlvalues = d.moment.split('T');

                        var etlconvdate = etlvalues[0];
                        var etlstreet = etlvalues[1];
                        console.log(etlstreet);

                        var mintsec = tmestmFormat.parse(etlstreet);
                        console.log(mintsec);



                        var format = d3.format('02d');
                        var datefrmt = d3.format("%d");

                        var mydateformat = format((d.etllastdatetime.getDate())) + '/' + format(d.etllastdatetime.getMonth() + 1) + '/' + d.etllastdatetime.getFullYear() + ':' + mintsec.getHours() + ":" + mintsec.getMinutes() + ":" + mintsec.getSeconds();
                        /*var mytimeformat =  d.etllastdatetime.getHours() + ":"  
                        +  d.etllastdatetime.getMinutes() + ":" 
                        +  d.etllastdatetime.getSeconds();*/
                        //console.log(mydateformat);
                        // console.log(mytimeformat);

                        document.getElementById("etljobdate").innerHTML = mydateformat;

                    });

                    d3.json("https://swastianalyticstest.herokuapp.com/api/v1/beneficiary_f?filter[where][fieldworker_id]=" + fwuserid + "&access_token=" + accesstoken, function (error, benficiarydata) {
                        dtgFormat = d3.time.format("%Y-%m-%d");
                        benficiarydata.forEach(function (d) {

                            var vulcountvalues = d.last_modified_time.split('T');

                            var vulcountfirstdate = vulcountvalues[0];
                            var vulcountlastdate = vulcountvalues[1];
                            //console.log(convdate);
                            // console.log( d.follow_up_date);
                            d.vulcountdate = dtgFormat.parse(vulcountfirstdate);

                        });
                        //console.log(benficiarydata);
                        var fwx = crossfilter(benficiarydata);

                        d3.json("https://swastianalyticstest.herokuapp.com/api/v1/beneficiary_f?filter[where][fieldworker_id]=" + fwuserid + "&access_token=" + accesstoken, function (error, benficiarydata1) {
                            dtgFormat = d3.time.format("%Y-%m-%d");
                            benficiarydata1.forEach(function (d) {

                                var memcountvalues = d.last_modified_time.split('T');

                                var memcountfirstdate = memcountvalues[0];
                                var memcountlastdate = memcountvalues[1];
                                //console.log(convdate);
                                // console.log( d.follow_up_date);
                                d.memcountdate = dtgFormat.parse(memcountfirstdate);
                                // console.log( d.dateoperation1);

                            });
                            //console.log(benficiarydata);
                            var awx = crossfilter(benficiarydata1);

                            d3.json("https://swastianalyticstest.herokuapp.com/api/v1/todo_f?filter[where][fieldworker_id]=" + fwuserid + "&access_token=" + accesstoken, function (error, todo_fdata) {
                                todo_fdata.forEach(function (d) {

                                    var values = d.follow_up_date.split('T');

                                    var convdate = values[0];
                                    var street = values[1];
                                    //console.log(convdate);
                                    // console.log( d.follow_up_date);
                                    d.date = dtgFormat.parse(convdate);
                                    //    d.monthdate = dtgFormat1.parse(d.follow_up_date);
                                    // d.date_posted = dateFormat.parse(d.follow_up_date);
                                    //d.day = d3.time.day(d.date);
                                    //d.date1 = dtgFormat1.parse(d.follow_up_date);
                                    //d.Year = d.date.getFullYear();
                                    // d.todate = formatDate(parseDate(d.follow_up_date));


                                    // console.log( d.date);



                                    // console.log( d.dateoperation1);

                                });
                                //console.log(benficiarydata);
                                var twx = crossfilter(todo_fdata);



                                /*=============================================================================================*/
                                /*Barchart for Todo*/
                                /*=============================================================================================*/

                                window.onresize = function (event) {
                                    var fwTodoSiteStackedBaNewWidth = document.getElementById('fwTodoSiteStackedBarChartDisplay').offsetWidth;
                                    fwTodoSiteStackedBarChart.width(fwTodoSiteStackedBaNewWidth)
                                    fwTodoSiteStackedBarChart.rescale();
                                    fwTodoSiteStackedBarChart.render();

                                    var fwPiechartwidth = document.getElementById('fwVulnaerablePieChart').offsetWidth;
                                    var fwPiechartwidth1 = fwPiechartwidth - 30;
                                    fwVulnaerablePieChart.width(fwPiechartwidth1);
                                    fwVulnaerablePieChart.render();




                                };

                                var fwTodoSiteStackedBarWidth = document.getElementById('fwTodoSiteStackedBarChartDisplay').offsetWidth;

                                var fwTodoSiteTypeDim = twx.dimension(function (d) {
                                    return d.site_name;
                                });

                                var fwTodoSiteTypeDueGroupOverDue = fwTodoSiteTypeDim.group().reduceSum(function (d) {
                                    return d.overdue_count;
                                });

                                var fwTodoSiteTypeDueGroupDue = fwTodoSiteTypeDim.group().reduceSum(function (d) {
                                    return d.due_count;
                                });
                                //var sidewaysGroup1 = regroup(dim, ['overdue_count','due_count']);


                                function getTops(source_group) {
                                    return {
                                        all: function () {
                                            return source_group.top(30);
                                        }
                                    };
                                }
                                var opfakeGroup = getTops(fwTodoSiteTypeDueGroupDue);
                                var heightOfContainer = 50,
                                    legendHeight = 0,
                                    legendY = heightOfContainer - legendHeight;


                                var fwTodoSiteStackedBarChart = dc.barChart('#fwTodoSiteStackedBarChartDisplay');
                                fwTodoSiteStackedBarChart.width(fwTodoSiteStackedBarWidth) /* dc.barChart('#monthly-volume-chart', 'chartGroup'); */
                                    .height(300)
                                    //.transitionDuration(1000)
                                    // .margins({top: 10, right: 50, bottom: 30, left: 50})
                                    // .margins({
                                    //   top: 10,
                                    //    right: 10,
                                    //    left: 30,
                                    //    bottom: 60
                                    //})

                                .dimension(fwTodoSiteTypeDim)
                                    .group(fwTodoSiteTypeDueGroupDue, "Due")
                                    .stack(fwTodoSiteTypeDueGroupOverDue, "Over Due")
                                    .ordinalColors(["#3399ff", "#ff5c33"])
                                    .centerBar(false)
                                    .gap(30)
                                    // .elasticX(true)
                                    // .elasticY(true)
                                    //.x(d3.scale.ordinal().domain(["Member Count", "Positive Member Count", "ART Eligible Member Count", "ART Member Count", "LTFU Membe Count"]))
                                    //.x(d3.scale.ordinal(["Member Count", "Positive Member Count", "ART Eligible Member Count", "ART Member Count", "LTFU Membe Count"]))
                                    .x(d3.scale.ordinal([fwTodoSiteTypeDim]))
                                    .xUnits(dc.units.ordinal)
                                    .renderHorizontalGridLines(true)
                                    //.renderlet(function(c) {
                                    //  c.svg().select('g').attr("transform","rotate(90 200,200)");
                                    // })

                                .elasticY(true)
                                    //.xAxisLabel("Reason for Variance ")
                                    .yAxisLabel("Count")
                                    .renderLabel(true)
                                    .title(function (d) {
                                        //console.log("Myvalue",d);
                                        return "Site Name: " + d.key + " Count: " + d.value;

                                    })
                                    //.renderVerticalGridLines(true)
                                    .ordering(function (d) {
                                        return -d.value;
                                    })
                                    //.y(d3.scale().domain([0, 400]))
                                    .renderLabel(true)
                                    // .legend(dc.legend().x(200).y(0).itemHeight(13).gap(2))
                                    .xAxisPadding(legendY + 50)
                                    .legend(dc.legend().x(0).y(270))
                                    //.legend(dc.legend().x(370).y(10).itemHeight(12).gap(5).horizontal(1).legendWidth(90).itemWidth(95))
                                    .xAxisLabel("Sites")
                                    .barPadding(0.30)
                                    .yAxis().tickFormat(d3.format("s"));
                                // .renderlet(function (chart) {
                                fwTodoSiteStackedBarChart.on('renderlet.barclicker', function (chart, filter) {
                                    chart.selectAll('.x text')
                                        .style('font-weight', 'bold')
                                        .style('font-size', '10px')
                                        .style('font-family', 'sans-serif')

                                    // .attr('transform', "rotate(-40)")

                                    //.select('text')
                                    //.style('text-anchor', 'start')
                                    //    .attr('transform', 'rotate(20 -10 40)');

                                    /* var colors = d3.scale.ordinal().domain(reason_var_dim)
                                         .range(["steelblue", "brown", "red", "green", "yellow", "grey", "blue", "black"]);
                                     chart.selectAll('rect.bar').each(function (d) {
                                         d3.select(this).attr("style", "fill: " + colors(d.data.key));
                                     });*/

                                    var gLabels = chart.select(".labels");
                                    if (gLabels.empty()) {
                                        gLabels = chart.select(".chart-body").append('g').classed('labels', true);
                                    }

                                    var gLabelsData = gLabels.selectAll("text").data(chart.selectAll(".bar")[0]);

                                    gLabelsData.exit().remove(); //Remove unused elements



                                    gLabelsData.enter().append("text") //Add new elements

                                    gLabelsData
                                        .attr('text-anchor', 'middle')
                                        .attr('fill', 'white')
                                        // .text("font-size","10px")
                                        // .attr("font-size", "2px")
                                        .text(function (d) {
                                            return d3.select(d).data()[0].data.value
                                        })
                                        .attr('x', function (d) {
                                            return +d.getAttribute('x') + (d.getAttribute('width') / 2);
                                        })
                                        .attr('y', function (d) {
                                            return +d.getAttribute('y') + 15;
                                        })
                                        .attr('style', function (d) {
                                            if (+d.getAttribute('height') < 18) return "display:none";
                                        });

                                    chart.selectAll('.y text')

                                    .style('font-weight', 'bold')
                                        .style('font-size', '10px')
                                        .style('font-family', 'sans-serif')

                                    chart.selectAll('legend')
                                        .style('font-size', '5px')
                                        .style('font-family', 'sans-serif')


                                    chart.selectAll('rect.bar').on('click.custom', function (d) {
                                        // console.log("click!", d.layer);

                                        var site_name = d.x;
                                        var layer_data = d.layer;
                                        var y_data = d.y;
                                        // console.log(site_name)
                                        var fwTodoSiteTable = dc.dataTable('#fwTodoSiteTableChart');
                                        var fwTodoSiteTableChartDim1 = twx.dimension(function (d)
                                            //{if(d.site_name=='Site1') {return d.fw_name;}});
                                            {
                                                //return d.fullname;
                                                // if (d.site_name == d.x)
                                                //return d.site_name=='Site4' && d.fw_name=='Jhon';
                                                // console.log(site_name)
                                                //console.log(d.due_flag)
                                                if (layer_data == 'Due') {
                                                    return d.site_name == site_name && d.due_flag == 'yes';
                                                } else {
                                                    return d.site_name == site_name && d.overdue_flag == 'yes';
                                                }

                                            });


                                        fwTodoSiteTableChartDim1.filter(true);
                                        // });
                                        //  console.log("myfilter!", myfilter);
                                        var fwTodoSiteTableChartGroup = fwTodoSiteTableChartDim1.group().reduceSum(function (d) {
                                            return d.vulnerabilityindex;
                                        });


                                        var ofs = 0,
                                            pag = 5;
                                        update();

                                        fwTodoSiteTable /* dc.dataTable('.dc-data-table', 'chartGroup') */
                                            .dimension(fwTodoSiteTableChartDim1)
                                            /* .group(function (d) {
                                                         var format = d3.format('02d');
                                                         var datefrmt = d3.format("%d")
                                                         return (d.dateoperation1.getFullYear()) + '/' + format((d.dateoperation1.getMonth() + 1)) + '/' + format((d.dateoperation1.getDate()))
                                                     })*/
                                            .group(function (d) {
                                                return "";
                                            })

                                        // .group(fwVulnaerableMemberNameTableChartGroup)
                                        .columns([function (d) {
                                                    return d.beneficiary_name;
                            },
              function (d) {
                                                    return d.site_name;
                            },
             function (d) {
                                                    return d.todo_action_type;
                            },
                              function (d) {
                                                    var format = d3.format('02d');
                                                    var datefrmt = d3.format("%d");
                                                    var months = new Array(12);
                                                    months[0] = "Jan";
                                                    months[1] = "Feb";
                                                    months[2] = "Mar";
                                                    months[3] = "Apr";
                                                    months[4] = "May";
                                                    months[5] = "Jun";
                                                    months[6] = "Jul";
                                                    months[7] = "Aug";
                                                    months[8] = "Sep";
                                                    months[9] = "Oct";
                                                    months[10] = "Nov";
                                                    months[11] = "Dec";
                                                    //return d.date.getFullYear() + '/' + format((d.date.getMonth() + 1)) + '/' + format((d.date.getDate()))
                                                    return format((d.date.getDate())) + '-' + months[d.date.getMonth()] + '-' + d.date.getFullYear();
                                                    //return d.follow_up_date;
                            }

           ])
                                            .sortBy(function (d) {
                                                return d.date;
                                            })
                                            .order(d3.descending)
                                            //.size(10);
                                            //fwTodoSiteStackedBarChart.render();
                                            //fwTodoSiteTable.redraw();
                                        dc.renderAll();

                                        function display() {
                                            d3.select('#begin')
                                                .text(ofs);
                                            d3.select('#end')
                                                .text(ofs + pag - 1);
                                            d3.select('#last')
                                                .attr('disabled', ofs - pag < 0 ? 'true' : null);
                                            d3.select('#next')
                                                .attr('disabled', ofs + pag >= y_data ? 'true' : null);
                                            d3.select('#size').text(y_data);
                                        }

                                        function update() {
                                            fwTodoSiteTable.beginSlice(ofs);
                                            fwTodoSiteTable.endSlice(ofs + pag);
                                            display();
                                        }
                                        $scope.next = function () {
                                            ofs += pag;
                                            update();
                                            fwTodoSiteTable.redraw();
                                        }
                                        $scope.last = function () {
                                            ofs -= pag;
                                            update();
                                            fwTodoSiteTable.redraw();
                                        }


                                        // return onChartClick.call(this, d);
                                    });

                                })


                                // .colors(["#67e667", "#ff0000", "#ff4040", "#ff7373", "#67e667", "#39e639", "#00cc00"])	   


                                //fwTodoSiteStackedBarChart.margins().bottom = legendY;

                                //fwTodoSiteStackedBarChart.filter = function() {};

                                /*   fwTodoSiteStackedBarChart.on('renderlet.barclicker', function (chart, filter) {
                                      
                                   });*/

                                //  var onChartClick = function () {
                                //   console.log("hi");
                                //  console.log("click!", d);               

                                // }
                                // reason_var_barChart.filter = function () {};

                                /*=============================================================================================*/
                                /*Select Chart for Memebr*/
                                /*=============================================================================================*/

                                var fwMembernameDim = nwx.dimension(function (d) {
                                    return d.fw_name;
                                });
                                var fwMemberCountGroup = fwMembernameDim.group();
                                var selectField = dc.selectMenu('#fwMemberSelectMenuChart')
                                    .dimension(fwMembernameDim)
                                    .title(function (d) {
                                        return d.key;
                                    })
                                    .group(fwMemberCountGroup);


                                /*=============================================================================================*/
                                /*Pie Chart for Valnurable Memebr*/
                                /*=============================================================================================*/

                                var fwVulnaerablePieChart = dc.pieChart("#fwVulnaerablePieChart");
                                var fwVulnaerablePieChartDim = fwx.dimension(function (d) {
                                    return d.category;
                                });
                                var fwVulnaerablePieChartGroup = fwVulnaerablePieChartDim.group().reduceSum(function (d) {
                                    return d.vulnerability_count;
                                });

                                /********************** addition ********************/
                                var fwVulnarableWidth = document.getElementById('fwVulnaerablePieChart').offsetWidth;

                                var fwVulnarableWidth1 = fwVulnarableWidth - 30;
                                /********************** addition ********************/

                                fwVulnaerablePieChart
                                    .width(fwVulnarableWidth1).height(300)
                                    .dimension(fwVulnaerablePieChartDim)
                                    .group(fwVulnaerablePieChartGroup)

                                .innerRadius(45)
                                    .externalRadiusPadding(40)
                                    .ordinalColors(["#ffc266 ", "#ff6666", "#66ccff", "#80ffaa"])
                                    .title(function (d) {
                                        // console.log('p', d.value);
                                        //return " Count: " + d.value;
                                        if (d.key == 'Category1') {
                                            return 'Vulnerable index >20 and Count:' + d.value;
                                        }
                                        if (d.key == 'Category2') {
                                            return 'Vulnerable index between >10 and <=20 and Count:' + d.value;
                                        }
                                        if (d.key == 'Category3') {
                                            return 'Vulnerable index between >5 and <=10 and Count:' + d.value;
                                        }
                                        if (d.key == 'Category4') {
                                            return 'Vulnerable index <5 and Count:' + d.value;
                                        };
                                    })
                                    .label(function (d) {
                                        if (d.key == 'Category1') {
                                            return +d.value;
                                        }
                                        if (d.key == 'Category2') {
                                            return +d.value;
                                        }
                                        if (d.key == 'Category3') {
                                            return +d.value;
                                        }
                                        if (d.key == 'Category4') {
                                            return +d.value;
                                        };
                                    })
                                    //.legend(dc.legend().x(140).y(0).gap(5))

                                .legend(dc.legend().x(0).y(5).itemHeight(5).gap(5).horizontal(1).legendWidth(90).itemWidth(95).legendText(function (d) {
                                    //console.log(d)
                                    if (d.name == 'Category1') {
                                        return 'Vulnerable index >20 ';
                                    }
                                    if (d.name == 'Category2') {
                                        return 'Vulnerable index between >10 and <=20';
                                    }
                                    if (d.name == 'Category3') {
                                        return 'Vulnerable index between >5 and <=10 ';
                                    }
                                    if (d.name == 'Category4') {
                                        return 'Vulnerable index <=5 ';
                                    };
                                }));
                                // .renderlet(function (chart) {
                                fwVulnaerablePieChart.on('renderlet.barclicker', function (chart, filter) {

                                    chart.selectAll('.pie-slice').on('click.custom', function (r) {


                                        var pie_click_data = r.data.value;
                                        var memberTable = dc.dataTable('#fwVulnaerableMemberTableChart');

                                        var fwVulnaerableMemberNameTableChartDim = fwx.dimension(function (d) {

                                            // if (d.fw_name == fwNameLogin)
                                            return d.vulnerabilityindex > 0;
                                            // else
                                            //  return;
                                        });
                                        fwVulnaerableMemberNameTableChartDim.filter(true);
                                        //return d.fullname;
                                        // });

                                        var fwVulnaerableMemberNameTableChartGroup = fwVulnaerableMemberNameTableChartDim.group().reduceSum(function (d) {
                                            return d.vulnerabilityindex;
                                        });

                                        var vulofs = 0,
                                            vulpag = 5;
                                        vulupdate();
                                        //memberTable = dc.dataTable('#fwVulnaerableMemberTableChart');
                                        memberTable /* dc.dataTable('.dc-data-table', 'chartGroup') */
                                            .dimension(fwVulnaerableMemberNameTableChartDim)
                                            /* .group(function (d) {
                                                         var format = d3.format('02d');
                                                         var datefrmt = d3.format("%d")
                                                         return (d.dateoperation1.getFullYear()) + '/' + format((d.dateoperation1.getMonth() + 1)) + '/' + format((d.dateoperation1.getDate()))
                                                     })*/
                                            .group(function (d) {
                                                return "";
                                            })

                                        // .group(fwVulnaerableMemberNameTableChartGroup)
                                        .columns([function (d) {
                                                    return d.beneficiary_name;
                        },
              function (d) {
                                                    return d.vulnerabilityindex;
                        },
                              function (d) {
                                                    var format = d3.format('02d');
                                                    var datefrmt = d3.format("%d");
                                                    var months = new Array(12);
                                                    months[0] = "Jan";
                                                    months[1] = "Feb";
                                                    months[2] = "Mar";
                                                    months[3] = "Apr";
                                                    months[4] = "May";
                                                    months[5] = "Jun";
                                                    months[6] = "Jul";
                                                    months[7] = "Aug";
                                                    months[8] = "Sep";
                                                    months[9] = "Oct";
                                                    months[10] = "Nov";
                                                    months[11] = "Dec";


                                                    //return d.date.getFullYear() + '/' + format((d.date.getMonth() + 1)) + '/' + format((d.date.getDate()))

                                                    return format((d.vulcountdate.getDate())) + '-' + months[d.vulcountdate.getMonth()] + '-' + d.vulcountdate.getFullYear();
                                                    //return d.follow_up_date;
                            }
           ])
                                            .sortBy(function (d) {
                                                return +d.vulnerabilityindex;
                                            })
                                            .order(d3.descending);
                                        //.size(10);
                                        // memberTable.render();
                                        dc.renderAll();
                                        // dc.redraw();
                                        memberTable.filter = function () {};


                                        function display() {
                                            d3.select('#pieTabBegin')
                                                .text(vulofs);
                                            d3.select('#pieTabEnd')
                                                .text(vulofs + vulpag - 1);
                                            d3.select('#pieTabLast')
                                                .attr('disabled', vulofs - vulpag < 0 ? 'true' : null);
                                            d3.select('#pieTabNext')
                                                .attr('disabled', vulofs + vulpag >= pie_click_data ? 'true' : null);
                                            d3.select('#pieTabSize').text(pie_click_data);
                                        }

                                        function vulupdate() {
                                            memberTable.beginSlice(vulofs);
                                            memberTable.endSlice(vulofs + vulpag);
                                            display();
                                        }
                                        $scope.vulnext = function () {
                                            vulofs += vulpag;
                                            vulupdate();
                                            memberTable.redraw();
                                        }
                                        $scope.vullast = function () {
                                            vulofs -= vulpag;
                                            vulupdate();
                                            memberTable.redraw();
                                        }

                                    });
                                });



                                /*=============================================================================================*/
                                /*Number Display Chart for Member not met */
                                /*=============================================================================================*/
                                var fwNumberDisplayDim = awx.dimension(function (d) {
                                    return d.fieldworker_id;
                                });

                                //var fwVulnaerableMemberNameNumberDisplayGroup = fwx.groupAll();
                                /*  var fwNumberDisplayGroup = fwNumberDisplayDim.group().reduceSum(function (d) {
                   //  console.log("valnera",d.mem_not_met_count);  
                  var bg_color_memnotmet = document.getElementById('fwNumberDisplayChart');
                  if(d.mem_not_met_count >= 10){
                //g_color_memnotmet.style.backgroundColor = 'red';
                    bg_color_memnotmet.style.color = 'red';
  } else {
    //_color_memnotmet.style.backgroundColor = 'green';
       bg_color_memnotmet.style.color = 'green';
  }
                 console.log(d.mem_not_met_count);
                 return d.mem_not_met_count;
                    });*/

                                var fwNumberDisplayGroup = fwNumberDisplayDim.group().reduceSum(function (d) {
                                    //console.log("valnera",d.mem_not_met_count); 
                                    var bg_color_memnotmet = document.getElementById('fwNumberDisplayChart');
                                    //var changeColor = function(obj){

                                    if (d.mem_not_met_count >= 12) {
                                        //g_color_memnotmet.style.backgroundColor = 'red';
                                        bg_color_memnotmet.style.color = 'red';
                                    } else {
                                        //_color_memnotmet.style.backgroundColor = 'green';
                                        bg_color_memnotmet.style.color = 'green';
                                    }
                                    return d.mem_not_met_count;
                                });



                                var member_not_met_count;

                                var fwNumberDisplay = dc.numberDisplay('#fwNumberDisplayChart');

                                fwNumberDisplay.group(fwNumberDisplayGroup)
                                    .formatNumber(d3.format(".g"))
                                    //.valueAccessor(function(p) { 
                                    // console.log('p',p);
                                    //return p.value.count > 0 ? p.value.sum / p.value.count : 0; });
                                    .valueAccessor(function (p) {
                                        //  console.log("p", p);

                                        member_not_met_count = p.value;
                                        return p.value;
                                    });

                                /*=============================================================================================*/
                                /*Table Chart for Member Count*/
                                /*=============================================================================================*/



                                $scope.member_count = function () {
                                    //fwHealthRowchart.filter("positive_members");
                                    // alert("hi")
                                    console.log("Inside");


                                    var fwMemberNotMet = dc.dataTable('#fwMemberNotMetTableChart');
                                    var fwTodoSiteTableChartDim = awx.dimension(function (d) {

                                        //if (d.fw_name == fwNameLogin)
                                        return d.mem_not_met_flag == 'yes';

                                    });
                                    fwTodoSiteTableChartDim.filter(true);
                                    //return d.fullname;
                                    // });

                                    var fwTodoSiteTableChartGroup = fwTodoSiteTableChartDim.group().reduceSum(function (d) {
                                        return d.mem_not_met_flag;
                                    });

                                    var memofs = 0,
                                        mempag = 5;
                                    memupdate();

                                    fwMemberNotMet /* dc.dataTable('.dc-data-table', 'chartGroup') */
                                        .dimension(fwTodoSiteTableChartDim)
                                        .group(function (d) {
                                            return "";
                                        })

                                    // .group(fwVulnaerableMemberNameTableChartGroup)
                                    .columns([function (d) {
                                                return d.beneficiary_name;
                        },
                              function (d) {
                                                var format = d3.format('02d');
                                                var datefrmt = d3.format("%d");
                                                var months = new Array(12);
                                                months[0] = "Jan";
                                                months[1] = "Feb";
                                                months[2] = "Mar";
                                                months[3] = "Apr";
                                                months[4] = "May";
                                                months[5] = "Jun";
                                                months[6] = "Jul";
                                                months[7] = "Aug";
                                                months[8] = "Sep";
                                                months[9] = "Oct";
                                                months[10] = "Nov";
                                                months[11] = "Dec";
                                                //return d.date.getFullYear() + '/' + format((d.date.getMonth() + 1)) + '/' + format((d.date.getDate()))
                                                //console.log( format((d.memcountdate.getDate())) + '-' + format((d.memcountdate.getMonth() + 1)) + '-' + d.memcountdate.getFullYear());
                                                return format((d.memcountdate.getDate())) + '-' + months[d.memcountdate.getMonth()] + '-' + d.memcountdate.getFullYear();
                                                //return d.follow_up_date;
                            }
           ])
                                        .sortBy(function (d) {
                                            return d.last_modified_time;
                                        })
                                        .order(d3.descending)
                                        //.size(10);
                                        //fwMemberNotMet.render();
                                    dc.renderAll();
                                    // dc.redraw();
                                    fwMemberNotMet.filter = function () {};

                                    function display() {
                                        d3.select('#numTabBegin')
                                            .text(memofs);
                                        d3.select('#numTabEnd')
                                            .text(memofs + mempag - 1);
                                        d3.select('#numTabLast')
                                            .attr('disabled', memofs - mempag < 0 ? 'true' : null);
                                        d3.select('#numTabNext')
                                            .attr('disabled', memofs + mempag >= member_not_met_count ? 'true' : null);
                                        d3.select('#numTabSize').text(member_not_met_count);
                                    }

                                    function memupdate() {
                                        fwMemberNotMet.beginSlice(memofs);
                                        fwMemberNotMet.endSlice(memofs + mempag);
                                        display();
                                    }
                                    $scope.memnext = function () {
                                        memofs += mempag;
                                        memupdate();
                                        fwMemberNotMet.redraw();
                                    }
                                    $scope.memlast = function () {
                                        memofs -= mempag;
                                        memupdate();
                                        fwMemberNotMet.redraw();
                                    }


                                }



                                //fwHealthRowchart.filter = function () {};
                                dc.renderAll();




                                function AddXAxis(chartToUpdate, displayText) {
                                    chartToUpdate.svg()
                                        .append("text")
                                        .attr("class", "x-axis-label")
                                        .attr("text-anchor", "middle")
                                        .attr("x", chartToUpdate.width() / 2)
                                        .attr("y", chartToUpdate.height() / 10)
                                        .text(displayText);
                                }


                            });
                        });
                    });

                });
            });
        }

    });