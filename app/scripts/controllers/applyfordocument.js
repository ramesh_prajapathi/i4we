'use strict';
angular.module('secondarySalesApp').controller('ApplyForDocumentCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $timeout, $modal) {
    //    if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
    //        window.location = "/";
    //    }

    $scope.UserLanguage = $window.sessionStorage.language;

    $scope.DocumentLanguage = {};

    Restangular.one('documentlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.DocumentLanguage = langResponse[0];
        $scope.AFDHeading = $scope.DocumentLanguage.afdCreate;
    });

    if ($window.sessionStorage.language == 1) {
        $scope.modalTitle = 'Thank You';
    } else if ($window.sessionStorage.language == 2) {
        $scope.modalTitle = 'धन्यवाद';
    } else if ($window.sessionStorage.language == 3) {
        $scope.modalTitle = 'ಧನ್ಯವಾದ';
    } else if ($window.sessionStorage.language == 4) {
        $scope.modalTitle = 'நன்றி';
    }

    $scope.HideCreateButton = false;
    /*****************************************************************************/
    $scope.documentmaster = {
        documentflag: 'yes',
        createdDate: new Date(),
        createdBy: $window.sessionStorage.userId,
        createdByRole: $window.sessionStorage.roleId,
        lastModifiedDate: new Date(),
        lastModifiedBy: $window.sessionStorage.userId,
        lastModifiedByRole: $window.sessionStorage.roleId,
        countryId: $window.sessionStorage.countryId,
        stateId: $window.sessionStorage.stateId,
        districtId: $window.sessionStorage.districtId,
        siteId: $window.sessionStorage.siteId.split(",")[0],
        deleteFlag: false,
        servicefee: 0,
        memberId: []
    };

    $scope.documentmaster.memberId.push($window.sessionStorage.fullName);

    $scope.$watch('documentmaster.memberId', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            Restangular.one('members/findOne?filter[where][id]=' + newValue).get().then(function (mem) {
                $scope.selectedMember = mem;
            });
        }
    });

    if ($window.sessionStorage.roleId + "" === "3") {

        $scope.hideAssigned = true;

        Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
            $scope.users = urs;
            $scope.documentmaster.associatedHF = $window.sessionStorage.userId;

        });
    } else {

        $scope.hideAssigned = false;

        Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
            $scope.users = urs;
            $scope.documentmaster.associatedHF = urs[0].id;

        });

    }

    $scope.auditlog = {
        action: 'Insert',
        module: 'Apply for Document',
        owner: $window.sessionStorage.userId,
        datetime: new Date(),
        details: 'Apply for Document Inserted',
        countryId: $window.sessionStorage.countryId,
        stateId: $window.sessionStorage.stateId,
        districtId: $window.sessionStorage.districtId,
        siteId: $window.sessionStorage.siteId.split(",")[0]
    };

    /***********************************************************************/
    $scope.UserLanguage = $window.sessionStorage.language;
    $scope.responcedreceived = Restangular.all('responsereceived?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
    $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
    $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
    $scope.schemestages = Restangular.all('schemestages?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
    $scope.printschemes = Restangular.all('documenttypes?filter[where][language]=' + $window.sessionStorage.language).getList().$object;
    $scope.submitsurveyanswers = Restangular.all('surveyanswers');
    /**************************************** Member *******************************************/
    //   $scope.beneficiaries = Restangular.all('members').getList().$object;
    if ($window.sessionStorage.roleId + "" === "3") {
        $scope.memberFilterCall = 'members?filter[where][deleteFlag]=false' + '&filter[where][associatedHF]=' + $window.sessionStorage.userId;
    } else {
        $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
    }
    Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
        $scope.beneficiaries = mems;

        angular.forEach($scope.beneficiaries, function (member, index) {
            member.index = index;
        });
    });

    /***********************************************************************/
    //  $scope.message = $scope.documentadded; //'Document has been applied!';
    // $scope.modalTitle = $scope.thankyou;
    $scope.applyfordocumentdataModal = false;
    $scope.validatestring = '';
    $scope.documentdataModal = false;
    $scope.CreateClicked = false;
    $scope.documentdisabled = true;

    $scope.surveyanswer = {};

    $scope.Save = function () {

        if ($scope.documentmaster.memberId == '' || $scope.documentmaster.memberId == null) {
            $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectMember; //'Please Select a Member';

        } else if ($scope.documentmaster.associatedHF == '' || $scope.documentmaster.associatedHF == null) {
            $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectAssignedTo;

        } else if ($scope.documentmaster.schemeId == '' || $scope.documentmaster.schemeId == null) {
            $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectDocument;

        } else if ($scope.documentmaster.stage == '' || $scope.documentmaster.stage == null) {
            $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectStage;

        } else if ($scope.documentmaster.datetime == '' || $scope.documentmaster.datetime == null) {
            $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectFollowUp;

        } else if ($scope.documentmaster.stage == 4) {
            if ($scope.documentmaster.responserecieve == '' || $scope.documentmaster.responserecieve == null) {
                $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectResponseReceived;
            } else if ($scope.documentmaster.responserecieve == 2) {
                if ($scope.documentmaster.rejection == '' || $scope.documentmaster.rejection == null) {
                    $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectReasonForRejection;
                }
            }

        } else if ($scope.documentmaster.stage == 5) {
            if ($scope.documentmaster.delay == '' || $scope.documentmaster.delay == null) {
                $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectReasonForDelay;
            }
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            $scope.toggleLoading();
            $scope.message = $scope.DocumentLanguage.thankYouCreated;
            //   $scope.documentmaster.associatedHF = $scope.selectedMember.associatedHF;
            Restangular.all('schememasters').post($scope.documentmaster).then(function (resp) {

                $scope.auditlog.rowId = resp.id;
                $scope.auditlog.details = $scope.selectedMember.individualId;
                $scope.auditlog.memberId = $scope.selectedMember.id;
                $scope.auditlog.conditionId = resp.schemeId;
                $scope.auditlog.associatedHF = resp.associatedHF;

                Restangular.all('audittrials').post($scope.auditlog).then(function (respAudit) {

                    $scope.modalInstanceLoad.close();
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('reloading...');

                    setInterval(function () {
                        window.location = '/applyfordocuments';
                    }, 1500);
                });
            }, function (error) {
                console.log("error", error);
                if (error.data.error.constraint === "unique_constraint_memberid_schemeid") {
                    $scope.modalInstanceLoad.close();
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.DocumentLanguage.documentAlreadyAdded;
                }
            });
        }
    };

    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    //Datepicker settings start
    var sevendays = new Date();
   // sevendays.setDate(sevendays.getDate() + 7);
    $scope.documentmaster.datetime = sevendays;
    $scope.today = function () {};
    $scope.today();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.dtmin = new Date();
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.open = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.opened = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };
    $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end
    $scope.datetimehide = false;
    $scope.reponserec = true;
    $scope.delaydis = true;
    $scope.collectedrequired = true;

    $scope.$watch('documentmaster.stage', function (newValue, oldValue) {
        var fifteendays = new Date();
        fifteendays.setDate(fifteendays.getDate() + 15);
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        if (newValue === oldValue) {
            return;
        } else if (newValue === '4') {
            $scope.reponserec = false;
            $scope.delaydis = true;
            $scope.datetimehide = false;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
        } else if (newValue === '5') {
            $scope.delaydis = false;
            $scope.reponserec = true;
            $scope.datetimehide = false;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.datetime = fifteendays;
            $scope.documentmaster.responserecieve = null;
        } else if (newValue === '2') {
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.datetimehide = false;
            $scope.collectedrequired = false;
            $scope.rejectdis = true;
            $scope.documentmaster.datetime = fifteendays;
            $scope.documentmaster.responserecieve = null;
        } else if (newValue === '3') {
            $scope.documentmaster.datetime = fifteendays;
            $scope.datetimehide = false;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
            $scope.delaydis = true;
            $scope.reponserec = true;
        } else if (newValue === '7') {
            var sixmonth = new Date();
            sixmonth.setDate(sixmonth.getDate() + 180);
            $scope.documentmaster.datetime = sixmonth;
            $scope.datetimehide = false;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
            $scope.delaydis = true;
            $scope.reponserec = true;
        } else if (newValue === '1') {
            $scope.documentmaster.datetime = fifteendays;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
        } else if (newValue === '8') {
            $scope.documentmaster.datetime = sevendays;
            $scope.datetimehide = false;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
        } else if (newValue === '6') {
            console.log('newValuedf', newValue);
            $scope.datetimehide = true;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
        } else if (newValue === '9') {
            $scope.datetimehide = true;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
            $scope.schememaster.datetime = new Date();
        } else {
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.documentmaster.responserecieve = null;
            $scope.documentmaster.delay = null;
            //$scope.documentmaster.datetime = '';
            $scope.datetimehide = false;
            $scope.rejectdis = true;
            $scope.documentmaster.responserecieve = null;
        }
    });

    $scope.rejectdis = true;

    $scope.$watch('documentmaster.responserecieve', function (newValue, oldValue) {
        var sixmonths = new Date();
        sixmonths.setDate(sixmonths.getDate() + 180);
        if (newValue === oldValue || newValue === null) {
            return;
        } else if (newValue === '1') {
            $scope.documentmaster.datetime = sixmonths;
            $scope.rejectdis = true;
        } else if (newValue === '2') {
            $scope.rejectdis = false;
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            $scope.documentmaster.datetime = sevendays;
        } else {
            // $scope.documentmaster.datetime = '';
            $scope.rejectdis = true;
            $scope.documentmaster.rejection = null;
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.documentmaster.responserecieve = null;
            $scope.documentmaster.delay = null;
            //$scope.documentmaster.datetime = '';
            $scope.datetimehide = false;
        }
    });
    /****************************** Language **********************/


});