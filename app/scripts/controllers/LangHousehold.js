'use strict';

angular.module('secondarySalesApp')
    .controller('LangHouseholdCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/
    
      /*********/

        $scope.isCreateSave = true;
        $scope.langdisable = false;

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        $scope.$watch('hh.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                
                 Restangular.one('householdLanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.hh = Restangular.copy($scope.original);
                    });
                
                Restangular.all('householdLanguages?filter[where][language]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                        $scope.isCreateSave = true;
                    } else {
                       // $scope.LangId = response[0].id;
                      //  $scope.isCreateSave = false;
                      //  $scope.langdisable = true;

                       //$scope.hh = response[0];
                       // console.log('$scope.hh', $scope.hh);
                        $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';

                    }
                });

            }
        });
    
     /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/LangHhList';

        };
    
    
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/
    

        $scope.hh = {
            deleteFlag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.UserEmployeeId
        };



       
        $scope.Save = function () {
            Restangular.all('householdLanguages').post($scope.hh).then(function (hhResponse) {
                console.log('hhResponse', hhResponse);
                window.location = '/LangHhList';
            });
           
        };

        $scope.Update = function () {
            Restangular.one('householdLanguages', $routeParams.id).customPUT($scope.hh).then(function (hhResponse) {
                console.log('hhResponse', hhResponse);
                window.location = '/LangHhList';
            });
        };
    
     if ($routeParams.id) {
          
             $scope.isCreateSave = false;
                $scope.langdisable = true;
            Restangular.one('householdLanguages', $routeParams.id).get().then(function (hh) {
                $scope.original = hh;
                $scope.hh = Restangular.copy($scope.original);
            });
      }
    
});