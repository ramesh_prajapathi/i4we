'use strict';

angular.module('secondarySalesApp')
    .controller('WashInfraListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/

        $scope.InterventionLanguage = {};

        Restangular.one('washLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.washLanguage = langResponse[0];
            // $scope.message = langResponse[0].InterventionSaved;
        });

        $scope.UserLanguage = $window.sessionStorage.language;

        $scope.typeofinfrastructures = Restangular.all('typeofinfrastructures?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.typeofinfrastructuresNew = Restangular.all('typeofinfrastructures?filter[where][deleteFlag]=false').getList().$object;

        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;
        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;
        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;
        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;
        $scope.areadsply = Restangular.all('areas?filter[where][deleteFlag]=false').getList().$object;
        $scope.roledsply = Restangular.all('roles').getList().$object;

        Restangular.all('users').getList().then(function (usrs) {

            $scope.finalUsers = usrs;

            if ($window.sessionStorage.roleId + "" === "3") {
                $scope.filterCall = 'infrastructures?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            } else {
                $scope.filterCall = 'infrastructures?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            }

            Restangular.all($scope.filterCall).getList().then(function (infr) {
                $scope.infrastructures = infr;

                $scope.displayInfrastructures = [];

                $scope.modalInstanceLoad.close();

                angular.forEach($scope.infrastructures, function (data, index) {
                    data.index = index + 1;

                    data.dateOfMapping = $filter('date')(data.dateOfMapping, 'dd-MMM-yyyy');
                    data.dateOfMappingExp = $filter('date')(data.dateOfMapping, 'dd/MM/yyyy');
                    data.createdDate = $filter('date')(data.createdDate, 'dd/MM/yyyy');
                    data.lastModifiedDate = $filter('date')(data.lastModifiedDate, 'dd/MM/yyyy');
                    data.locationname = data.latitude + ', ' + data.longitude;

                    if (data.isNew == true) {
                        data.isNewExp = 'yes';
                    } else {
                        data.isNewExp = 'no'
                    }

                    var data2 = $scope.finalUsers.filter(function (arr) {
                        return arr.id == data.associatedHF
                    })[0];

                    if (data2 != undefined) {
                        data.assignedTo = data2.name;
                    }

                    if ($window.sessionStorage.language == 1) {

                        var data3 = $scope.typeofinfrastructures.filter(function (arr) {
                            return arr.id == data.typeOfInfrastructure
                        })[0];

                        if (data3 != undefined) {
                            data.typeOfInfrastructureName = data3.name;
                        }

                    } else if ($window.sessionStorage.language != 1) {

                        var data3 = $scope.typeofinfrastructures.filter(function (arr) {
                            return arr.parentId == data.venue
                        })[0];

                        if (data3 != undefined) {
                            data.typeOfInfrastructureName = data3.name;
                        }
                    }

                    var data4 = $scope.typeofinfrastructuresNew.filter(function (arr) {
                            return arr.id == data.typeOfInfrastructure
                        })[0];

                        if (data4 != undefined) {
                            data.typeOfInfrastructureEngName = data4.name;
                        }
                    
                     var data5 = $scope.areadsply.filter(function (arr) {
                            return arr.id == data.areaId
                        })[0];

                        if (data5 != undefined) {
                            data.areaname = data5.name;
                        }
                    
                    var data6 = $scope.countrydisplay.filter(function (arr) {
                            return arr.id == data.countryId
                        })[0];

                        if (data6 != undefined) {
                            data.countryname = data6.name;
                        }
                    
                    var data7 = $scope.statedisplay.filter(function (arr) {
                            return arr.id == data.stateId
                        })[0];

                        if (data7 != undefined) {
                            data.statename = data7.name;
                        }
                    
                     var data8 = $scope.districtdsply.filter(function (arr) {
                            return arr.id == data.districtId
                        })[0];

                        if (data8 != undefined) {
                            data.districtname = data8.name;
                        }
                    
                    var data9 = $scope.sitedsply.filter(function (arr) {
                            return arr.id == data.siteId
                        })[0];

                        if (data9 != undefined) {
                            data.sitename = data9.name;
                        }
                    
                     var data10 = $scope.finalUsers.filter(function (arr) {
                            return arr.id == data.createdBy
                        })[0];

                        if (data10 != undefined) {
                            data.createdByname = data10.username;
                        }
                    
                    var data11 = $scope.finalUsers.filter(function (arr) {
                            return arr.id == data.lastModifiedBy
                        })[0];

                        if (data11 != undefined) {
                            data.lastModifiedByname = data11.username;
                        }
                    
                     var data12 = $scope.roledsply.filter(function (arr) {
                            return arr.id == data.lastModifiedByRole
                        })[0];

                        if (data12 != undefined) {
                            data.lastModifiedByRolename = data12.name;
                        }
                    
                    var data13 = $scope.roledsply.filter(function (arr) {
                            return arr.id == data.createdByRole
                        })[0];

                        if (data13 != undefined) {
                            data.createdByRolename = data13.name;
                        }



                    $scope.displayInfrastructures.push(data);

                });
            });

        });

        /**************************Export data to excel sheet ***************/

        $scope.DisableExport = false;
        $scope.valWashCount = 0;

        $scope.exportData = function () {

            $scope.washArray = [];

            $scope.valWashCount = 0;

            if ($scope.displayInfrastructures.length == 0) {
                alert('No data found');
            } else {

                for (var arr = 0; arr < $scope.displayInfrastructures.length; arr++) {

                    $scope.washArray.push({
                        'Sr No': $scope.displayInfrastructures[arr].index,
                        'COUNTRY': $scope.displayInfrastructures[arr].countryname,
                        'STATE': $scope.displayInfrastructures[arr].statename,
                        'DISTRICT': $scope.displayInfrastructures[arr].districtname,
                        'SITE': $scope.displayInfrastructures[arr].sitename,
                        'DATE OF MAPPING': $scope.displayInfrastructures[arr].dateOfMappingExp,
                        'ASSIGNED TO': $scope.displayInfrastructures[arr].assignedTo,
                        'AREA NAME': $scope.displayInfrastructures[arr].areaname,
                        'LOCATION': $scope.displayInfrastructures[arr].locationname,
                        'TYPE OF INFRASTRUCTURE': $scope.displayInfrastructures[arr].typeOfInfrastructureEngName,
                        'NO OF INFRASTRUCTURE': $scope.displayInfrastructures[arr].noOfInfrastructure,
                        'NEW': $scope.displayInfrastructures[arr].isNewExp,
                        'CREATED BY': $scope.displayInfrastructures[arr].createdByname,
                        'CREATED DATE': $scope.displayInfrastructures[arr].createdDate,
                        'CREATED ROLE': $scope.displayInfrastructures[arr].createdByRolename,
                        'LAST MODIFIED BY': $scope.displayInfrastructures[arr].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.displayInfrastructures[arr].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.displayInfrastructures[arr].lastModifiedByRolename,
                        'DELETE FLAG': $scope.displayInfrastructures[arr].deleteFlag
                    });

                    $scope.valWashCount++;

                    if ($scope.displayInfrastructures.length == $scope.valWashCount) {
                        alasql('SELECT * INTO XLSX("washinfrastructures.xlsx",{headers:true}) FROM ?', [$scope.washArray]);
                    }
                }
            }
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

    });
