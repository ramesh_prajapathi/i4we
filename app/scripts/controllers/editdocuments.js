'use strict';

angular.module('secondarySalesApp')
    .controller('EditDocumentCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $timeout) {

        $scope.DocumentLanguage = {};

        Restangular.one('documentlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.DocumentLanguage = langResponse[0];
            $scope.AFDHeading = $scope.DocumentLanguage.afdEdit;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        /* $scope.showForm = function () {
		     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
		     return visible;
		 };

		 $scope.isCreateView = function () {
		     if ($scope.showForm()) {
		         var visible = $location.path() === '/applyforschemes/create';
		         return visible;
		     }
		 };
		 $scope.hideCreateButton = function () {
		     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
		     return visible;
		 };

		 $scope.hideSearchFilter = function () {
		     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
		     return visible;
		 };*/
        $scope.HideCreateButton = true;

        $scope.auditlog = {
            action: 'Update',
            module: 'Apply for Document',
            owner: $window.sessionStorage.userId,
            datetime: new Date(),
            details: 'Apply for Document Updated',
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        };

        /*******************************************************************************************************************/
        $scope.documentmaster = {
            attendees: [],
            documentflag: 'yes'
        };
        //        $scope.schemestages = Restangular.all('schemestages?filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.states = Restangular.all('zones').getList().$object;
        $scope.genders = Restangular.all('genders?filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.educations = Restangular.all('educations?filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.submitdocumentmasters = Restangular.all('schememasters').getList().$object;
        $scope.submittodos = Restangular.all('todos').getList().$object;
        //   $scope.beneficiaries = Restangular.all('members').getList().$object;

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.hideAssigned = true;

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.documentmaster.associatedHF = $window.sessionStorage.userId;

            });
        } else {

            $scope.hideAssigned = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.documentmaster.associatedHF = $scope.schememaster.associatedHF;

            });
        }

        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.memberFilterCall = 'members?filter[where][deleteFlag]=false' + '&filter[where][associatedHF]=' + $window.sessionStorage.userId;
        } else {
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        }
        Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
            $scope.beneficiaries = mems;

            angular.forEach($scope.beneficiaries, function (member, index) {
                member.index = index;
            });
        });

        $scope.$watch('documentmaster.memberId', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                Restangular.one('members/findOne?filter[where][id]=' + newValue).get().then(function (mem) {
                    $scope.selectedMember = mem;
                });
            }
        });

        // $scope.modaldocumentmasters = Restangular.all('schemes?filter[where][deleteflag]=null').getList().$object;
        $scope.responcedreceived = Restangular.all('responsereceived?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.sm = Restangular.all('documenttypes?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (scheme) {
            $scope.printschemes = scheme;
            //  $scope.groupmeeting.attend = partner.id;
            // console.log('$scope.groupmeeting.attendees.id', $scope.groupmeeting.attendees);
            angular.forEach($scope.printschemes, function (member, index) {
                member.index = index;
                member.enabled = false;
            });
        });


        $scope.getGender = function (genderId) {
            return Restangular.one('genders', genderId).get().$object;
        };

        if ($routeParams.id) {
            Restangular.one('schememasters', $routeParams.id).get().then(function (schememaster) {
                $scope.original = schememaster;
                $scope.documentmaster = Restangular.copy($scope.original);

                if (schememaster.stage == 4) {
                    $scope.reponserec = false;
                }

                if (schememaster.stage == 5) {
                    $scope.delaydis = false;
                }

                $scope.schemstage = Restangular.all('schemestages?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (schemstage) {
                    $scope.schemestages = schemstage;
                    angular.forEach($scope.schemestages, function (member, index) {
                        member.index = index;
                        if (schememaster.stage == member.id || schememaster.stage == member.parentId) {
                            $scope.stageLevel = member.level;
                        }
                    });
                    if (schememaster.stage == 4 && schememaster.responserecieve === '2') {
                        $scope.reponserec = false;
                        $scope.rejectdis = false;
                        angular.forEach($scope.schemestages, function (member, index) {
                            member.index = index;
                            if (member.id == 4 || member.parentId == 4 || member.id == 8 || member.parentId == 8) {
                                member.enabled = false;
                            } else {
                                member.enabled = true
                            }
                        });
                    } else {
                        angular.forEach($scope.schemestages, function (member, index) {
                            member.index = index;
                            if ($scope.stageLevel < member.level) {
                                member.enabled = false;
                            } else if ((member.id == schememaster.stage || member.parentId == schememaster.stage) && $scope.stageLevel == member.level) {
                                member.enabled = false;
                            } else {
                                member.enabled = true
                            }
                        });
                    }
                });

                // $scope.schememaster.datetime = $filter('date')(schememaster.datetime, 'y-MM-dd');
                // console.log('$scope.documentmaster', $scope.documentmaster);
                //$scope.schememaster.categ = schememaster.category.split(",");
                //$scope.schememaster.agegrp = schememaster.agegroup.split(",");
            });
        }

        $scope.DeleteFlag = function (id) {
            if (confirm("Are you sure want to delete..!") == true) {
                Restangular.one('schemes/' + id).remove($scope.documentmasters).then(function () {
                    $route.reload();
                });

            } else {

            }

        }

        $scope.validatestring = '';

        $scope.Update = function () {
            if ($scope.documentmaster.memberId == '' || $scope.documentmaster.memberId == null) {
                $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectMember; //'Please Select a Member';

            } else if ($scope.documentmaster.schemeId == '' || $scope.documentmaster.schemeId == null) {
                $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectScheme;

            } else if ($scope.documentmaster.stage == '' || $scope.documentmaster.stage == null) {
                $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectStage;

            } else if ($scope.documentmaster.datetime == '' || $scope.documentmaster.datetime == null) {
                $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectFollowUp;

            } else if ($scope.documentmaster.stage == 4) {
                if ($scope.documentmaster.responserecieve == '' || $scope.documentmaster.responserecieve == null) {
                    $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectResponseReceived;
                } else if ($scope.documentmaster.responserecieve == 2) {
                    if ($scope.documentmaster.rejection == '' || $scope.documentmaster.rejection == null) {
                        $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectReasonForRejection;
                    }
                }

            } else if ($scope.documentmaster.stage == 5) {
                if ($scope.documentmaster.delay == '' || $scope.documentmaster.delay == null) {
                    $scope.validatestring = $scope.validatestring + $scope.DocumentLanguage.selectReasonForDelay;
                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.toggleLoading();

                $scope.documentmaster.lastModifiedDate = new Date();
                $scope.documentmaster.lastModifiedBy = $window.sessionStorage.userId;
                $scope.documentmaster.lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.documentmaster.associatedHF = $scope.selectedMember.associatedHF;
                $scope.message = $scope.DocumentLanguage.thankYouUpdated;

                $scope.submitdocumentmasters.customPUT($scope.documentmaster).then(function (resp) {
                    //  console.log('$scope.documentmasters', $scope.documentmaster);

                    $scope.auditlog.rowId = resp.id;
                    $scope.auditlog.details = $scope.selectedMember.individualId;
                    $scope.auditlog.memberId = $scope.selectedMember.id;
                    $scope.auditlog.conditionId = resp.schemeId;
                    $scope.auditlog.associatedHF = $scope.selectedMember.associatedHF;

                    Restangular.all('audittrials').post($scope.auditlog).then(function (respAudit) {
                        $scope.modalInstanceLoad.close();
                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        console.log('reloading...');

                        setInterval(function () {
                            window.location = '/applyfordocuments';
                        }, 1500);

                        ///window.location = '/applyforschemes';
                    });
                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /* $scope.Update = function () {
             $scope.submitdocumentmasters.customPUT($scope.documentmaster).then(function () {
                 console.log('$scope.documentmasters', $scope.documentmasters);
                 window.location = '/applyforschemes';
             });
         };*/


        //Datepicker settings start
        $scope.today = function () {
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            // $scope.documentmaster.datetime = $filter('date')(sevendays, 'y-MM-dd')
        };
        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dtmin = new Date();

        // Disable weekend selection
        /*  $scope.disabled = function (date, mode) {
		      return (mode === 'day' && (date.getDay() === 0));
		  };*/

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings en

        $scope.reponserec = true;
        $scope.delaydis = true;

        $scope.$watch('documentmaster.stage', function (newValue, oldValue) {
            var fifteendays = new Date();
            fifteendays.setDate(fifteendays.getDate() + 15);
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            if (newValue === oldValue) {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                $scope.documentmaster.datetime = $scope.documentmaster.datetime;
            } else if (newValue === '4') {
                $scope.reponserec = false;
                $scope.delaydis = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
            } else if (newValue === '5') {
                $scope.delaydis = false;
                $scope.reponserec = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.documentmaster.datetime = fifteendays;
                $scope.documentmaster.responserecieve = null;
            } else if (newValue === '2') {
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.documentmaster.datetime = fifteendays;
                $scope.documentmaster.responserecieve = null;
            } else if (newValue === '3') {
                $scope.documentmaster.datetime = fifteendays;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.documentmaster.responserecieve = null;
                $scope.delaydis = true;
                $scope.reponserec = true;
            } else if (newValue === '7') {
                var sixmonth = new Date();
                sixmonth.setDate(sixmonth.getDate() + 180);
                $scope.documentmaster.datetime = sixmonth;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.documentmaster.responserecieve = null;
                $scope.delaydis = true;
                $scope.reponserec = true;
            } else if (newValue === '1') {
                $scope.documentmaster.datetime = fifteendays;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.documentmaster.responserecieve = null;
            } else if (newValue === '8') {
                $scope.documentmaster.datetime = sevendays;
                $scope.datetimehide = false;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.documentmaster.responserecieve = null;
            } else if (newValue === '6') {
                console.log('newValuedf', newValue);
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.documentmaster.responserecieve = null;
            } else if (newValue === '9') {
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.documentmaster.responserecieve = null;
                $scope.schememaster.datetime = new Date();
            } else {
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.documentmaster.responserecieve = null;
                $scope.documentmaster.delay = null;
                //$scope.documentmaster.datetime = '';
                $scope.datetimehide = false;
                $scope.rejectdis = true;
                $scope.documentmaster.responserecieve = null;
            }
        });

        $scope.rejectdis = true;

        $scope.$watch('documentmaster.responserecieve', function (newValue, oldValue) {
            var sixmonths = new Date();
            sixmonths.setDate(sixmonths.getDate() + 180);
            if (newValue === oldValue || newValue === null) {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                $scope.documentmaster.datetime = $scope.documentmaster.datetime;
            } else if (newValue === '1') {
                $scope.documentmaster.datetime = sixmonths;
                $scope.rejectdis = true;
            } else if (newValue === '2') {
                $scope.rejectdis = false;
                var sevendays = new Date();
                sevendays.setDate(sevendays.getDate() + 7);
                $scope.documentmaster.datetime = sevendays;
            } else {
                // $scope.documentmaster.datetime = '';
                $scope.rejectdis = true;
                $scope.documentmaster.rejection = null;
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.documentmaster.responserecieve = null;
                $scope.documentmaster.delay = null;
                //$scope.documentmaster.datetime = '';
                $scope.datetimehide = false;
            }
        });

        /************************************************ Search On Modal *************************/

        $scope.schemename = $scope.name;
        $scope.$watch('documentmaster.agegrp', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][agegroup]=' + newValue).getList().then(function (scheme) {
                    $scope.schemes = scheme;

                    angular.forEach($scope.schemes, function (member, index) {
                        member.index = index;
                        member.enabled = false;
                    });
                });
            }
        });

        $scope.$watch('documentmaster.gender', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][gender]=' + newValue).getList().then(function (scheme) {
                    $scope.schemes = scheme;

                    angular.forEach($scope.schemes, function (member, index) {
                        member.index = index;
                        member.enabled = false;
                    });
                });
            }
        });

        $scope.$watch('documentmaster.stateid', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][state]=' + newValue).getList().then(function (scheme) {
                    $scope.schemes = scheme;

                    angular.forEach($scope.schemes, function (member, index) {
                        member.index = index;
                        member.enabled = false;
                    });
                });
            }
        });

        $scope.showschemesModal = false;
        $scope.toggleschemesModal = function () {
            $scope.SaveScheme = function () {
                $scope.newArray = [];
                $scope.documentmaster.attendees = [];
                for (var i = 0; i < $scope.printschemes.length; i++) {
                    if ($scope.printschemes[i].enabled == true) {
                        $scope.newArray.push($scope.printschemes[i].index);
                    }
                }
                $scope.documentmaster.attendees = $scope.newArray
                $scope.showschemesModal = !$scope.showschemesModal;
            };


            $scope.showschemesModal = !$scope.showschemesModal;
        };

        $scope.CancelScheme = function () {
            $scope.showschemesModal = !$scope.showschemesModal;
        };


        /*  $scope.$watch('documentmaster.attendees', function (newValue, oldValue) {
              //console.log('watch.attendees', newValue);

              if (newValue === oldValue) {
                  return;
              } else {
                  if (newValue.length > oldValue.length) {
                      // something was added
                      var Array1 = newValue;
                      var Array2 = oldValue;

                      for (var i = 0; i < Array2.length; i++) {
                          var arrlen = Array1.length;
                          for (var j = 0; j < arrlen; j++) {
                              if (Array2[i] == Array1[j]) {
                                  Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));

                              }
                          }
                      }
                      $scope.printschemes[Array1[0]].enabled = true;

                      // do stuff
                  } else if (newValue.length < oldValue.length) {
                      // something was removed
                      var Array1 = oldValue;
                      var Array2 = newValue;

                      for (var i = 0; i < Array2.length; i++) {
                          var arrlen = Array1.length;
                          for (var j = 0; j < arrlen; j++) {
                              if (Array2[i] == Array1[j]) {
                                  Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                              }
                          }
                      }
                      $scope.printschemes[Array1[0]].enabled = false;

                  }
              }
          });*/


    });
