'use strict';

angular.module('secondarySalesApp')
    .controller('groupmeetingtopicCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }
        $scope.showForm = function () {
            var visible = $location.path() === '/groupmeetingtopics/create' || $location.path() === '/groupmeetingtopics/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/groupmeetingtopics/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/groupmeetingtopics/create' || $location.path() === '/groupmeetingtopics/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/groupmeetingtopics/create' || $location.path() === '/groupmeetingtopics/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.pillarId = $window.sessionStorage.myRoute;
        }

        if ($window.sessionStorage.prviousLocation != "partials/groupmeetingtopic") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }


        //$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };



        /*********/
        $scope.searchgroupmeetingtopic = $scope.name;

        /****************************** INDEX *******************************************/
        $scope.zn = Restangular.all('pillars?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.pillars = zn;
            $scope.pillarId = $window.sessionStorage.myRoute;
        });

        $scope.pillarId = '';
        $scope.zonalid = '';
        $scope.$watch('pillarId', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $window.sessionStorage.myRoute = newValue;
                $scope.pillarId = $window.sessionStorage.myRoute;
                $scope.sal = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (sal) {
                    $scope.groupmeetingtopics = sal;
                    angular.forEach($scope.groupmeetingtopics, function (member, index) {
                        member.index = index + 1;
                    });
                });
            }
        });

        /********************* SAVE *******************************************/
        $scope.groupmeetingtopic = {
            "name": '',
            "deleteflag": false
        };
        $scope.validatestring = '';
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.groupmeetingtopic.name == '' || $scope.groupmeetingtopic.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.groupmeetingtopic.pillarid == '' || $scope.groupmeetingtopic.pillarid == null) {
                $scope.validatestring = $scope.validatestring + 'Please select a pillar';

            } else if ($scope.groupmeetingtopic.hnname == '' || $scope.groupmeetingtopic.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name in hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.groupmeetingtopic.knname == '' || $scope.groupmeetingtopic.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name in kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.groupmeetingtopic.taname == '' || $scope.groupmeetingtopic.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name in tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.groupmeetingtopic.tename == '' || $scope.groupmeetingtopic.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name in telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.groupmeetingtopic.mrname == '' || $scope.groupmeetingtopic.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name in marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.all('groupmeetingtopics').post($scope.groupmeetingtopic).then(function (poRes) {
                    //console.log('groupmeetingtopic Saved',poRes);
                    window.location = '/groupmeetingtopics';
                });
            }
        };
        /************************************* UPDATE *******************************************/

        if ($routeParams.id) {
            $scope.message = 'Group Meeting topic has been Updated!';
            Restangular.one('groupmeetingtopics', $routeParams.id).get().then(function (groupmeetingtopic) {
                $scope.original = groupmeetingtopic;
                $scope.groupmeetingtopic = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Group Meeting topic has been Created!';
        }

        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.groupmeetingtopic.name == '' || $scope.groupmeetingtopic.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.groupmeetingtopic.pillarid == '' || $scope.groupmeetingtopic.pillarid == null) {
                $scope.validatestring = $scope.validatestring + 'Please select a pillar';

            } else if ($scope.groupmeetingtopic.hnname == '' || $scope.groupmeetingtopic.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name in hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.groupmeetingtopic.knname == '' || $scope.groupmeetingtopic.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name in kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.groupmeetingtopic.taname == '' || $scope.groupmeetingtopic.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name in tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.groupmeetingtopic.tename == '' || $scope.groupmeetingtopic.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name in telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.groupmeetingtopic.mrname == '' || $scope.groupmeetingtopic.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter topic name in marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                //$scope.submitgroupmeetingtopics.customPUT($scope.groupmeetingtopic).then(function () {
                Restangular.one('groupmeetingtopics', $routeParams.id).customPUT($scope.groupmeetingtopic).then(function () {
                    console.log('groupmeetingtopic Saved');
                    window.location = '/groupmeetingtopics';
                });
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /*********************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('groupmeetingtopics/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        $scope.getPillar = function (pillarId) {
            return Restangular.one('pillars', pillarId).get().$object;
        };


    });
