'use strict';

angular.module('secondarySalesApp')
    .controller('LangPatientRecordCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
    
    
    $scope.HideCreateButton = true;
        $scope.langdisable = false;

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        $scope.$watch('precord.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                
                 Restangular.one('prlanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.precord = Restangular.copy($scope.original);
                    });
                
                Restangular.all('prlanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                        $scope.HideSubmitButton = true;
                    } else {
                        // $scope.LangId = response[0].id;
                        //$scope.HideSubmitButton = false;
                        //$scope.langdisable = true;

                        //$scope.reportincident = response[0];
                        // console.log('$scope.reportincident', $scope.reportincident);
                         $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';


                    }
                });

            }
        });
    
        
         /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/LangPatientRecordlist';

        };
    
    
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/


        $scope.precord = {
            deleteFlag: false,
            lastModifiedDate: new Date()
        };




        $scope.Save = function () {
            Restangular.all('prlanguages').post($scope.precord).then(function (prResponse) {
                console.log('prResponse', prResponse);
                window.location = '/LangPatientRecordlist';
            });

        };

        $scope.Update = function () {
            Restangular.one('prlanguages', $routeParams.id).customPUT($scope.precord).then(function (prResponse) {
                console.log('prResponse', prResponse);
                window.location = '/LangPatientRecordlist';
            });
        };

        if ($routeParams.id) {

            $scope.HideCreateButton = false;
            $scope.langdisable = true;
            Restangular.one('prlanguages', $routeParams.id).get().then(function (precord) {
                $scope.original = precord;
                $scope.precord = Restangular.copy($scope.original);
            });
        }
    
});