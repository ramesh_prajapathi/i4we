'use strict';
angular.module('secondarySalesApp').controller('cobudgetsViewNewCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $modal) {
    $scope.isCreateView = true;

    Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=false').getList().then(function (Allspmres) {
        $scope.yearArray = [];
        for (var b = 0; b < Allspmres.length; b++) {
            $scope.yearArray.push(Allspmres[b].year);
        }
        //console.log('$scope.yearArray',$scope.yearArray)
        Restangular.all('spmbudgetyears?filter={"where":{"id":{"inq":[' + $scope.yearArray + ']}}}').getList().then(function (cobyearRes) {
            $scope.spmbudgetyears = cobyearRes;

        });
    });

    $scope.YearFunction = function (selyr) {
        //console.log("selyr", selyr);
        if (selyr === undefined || selyr === null || selyr === '') {
            return;
        } else {
            $scope.current_YEAR = selyr;
            console.log('selyr', selyr);
            //console.log('$window.sessionStorage.UserEmployeeId', $window.sessionStorage.UserEmployeeId)
            Restangular.one('spmbudgetyears', selyr).get().then(function (yearResa) {
                $scope.Current_Year = yearResa.name;
            });
            Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][year]=' + selyr).getList().then(function (res) {
                $scope.spmBudget = res[0].totalamount;
                $scope.spmBudgetAmount = res[0].balancetilldate;
                $scope.spmBudgetId = res[0].id;
                $scope.Balance_amount = $scope.spmBudget - $scope.Total_CO_HumanResource;
                //console.log('$scope.spmBudget', res[0].humanresources);

                $scope.spmhumanresources = res[0].humanresources;
                $scope.spmtravelandcommunication = res[0].travelandcommunication;
                $scope.spmgenericactivity = res[0].genericactivity;
                $scope.spmspecificactivity = res[0].specificactivity;
                $scope.spmmsgtgactivity = res[0].msgtgactivity;
                $scope.spmadministrativeexpense = res[0].administrativeexpense;
                $scope.spmmonitoringevaluation = res[0].monitoringevaluation;
                $scope.spmbudget_total = res[0].humanresources + res[0].travelandcommunication + res[0].genericactivity + res[0].specificactivity + res[0].msgtgactivity + res[0].administrativeexpense + res[0].monitoringevaluation;


            });
            // console.log('$scope.cobudgetsArray',$scope.cobudgetsArray)
            //$scope.cobudgetsArray = $scope.cobudgetsArray;
            angular.forEach($scope.cobudgetsArray, function (member, index) {
                member.index = index + 1;
                member.totalamounts = $scope.getTotalAmount(member.id, selyr);
                //        $scope.TotalData = [];
                //        $scope.TotalData.push(member);
            });
            //console.log("$scope.cobudgetsArray", $scope.cobudgetsArray);
        };
    };
    $scope.getTotalAmount = function (id, selyr) {
        return Restangular.all('cobudgets?filter[where][month]=' + id + '&filter[where][year]=' + selyr + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().$object;
    };
    $scope.cobudgetsArray = [];
    $scope.cobudgetsArray.push({
        mymonth: 'APRIL',
        id: 4
    }, {
        mymonth: 'MAY',
        id: 5
    }, {
        mymonth: 'JUNE',
        id: 6
    }, {
        mymonth: 'JULY',
        id: 7
    }, {
        mymonth: 'AUGUST',
        id: 8
    }, {
        mymonth: 'SEPTEMBER',
        id: 9
    }, {
        mymonth: 'OCTOBER',
        id: 10
    }, {
        mymonth: 'NOVEMBER',
        id: 11
    }, {
        mymonth: 'DECEMBER',
        id: 12
    }, {
        mymonth: 'JANUARY',
        id: 1
    }, {
        mymonth: 'FEBRUARY',
        id: 2
    }, {
        mymonth: 'MARCH',
        id: 3
    });


    /******************************EDIT **************************/
    $scope.BudgetEdit = function (budID, year) {
        $scope.currentBudgetId = null;
        $scope.original = null;
        $scope.cobudget = {
            year: $scope.current_YEAR,
            deleteflag: false,
            comember: $window.sessionStorage.UserEmployeeId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            lastmodifiedbyrole: $window.sessionStorage.roleId,
            facility: $window.sessionStorage.coorgId,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId
        };
        $scope.Displaymonth = null;
        $scope.Current_Year = null;
        $scope.Swasti_Total = null;
        $scope.Swasti_Total = null;
        $scope.Total_Sum = null;
        $scope.CO_Total = null;
        $scope.Remaining_Sum = null;


        $scope.isCreateView = false;
        //console.log('BudgetId', budID);
        Restangular.one('cobudgets?filter[where][month]=' + budID + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).get().then(function (resp) {
            //console.log('cobudgets', resp)
            //$scope.remainingbudget = resp;
            $scope.currentBudgetId = resp[0].id;
            $scope.currentBalAmount = resp[0].totalamount;
            //console.log('$scope.currentBudgetId', $scope.currentBudgetId);

            Restangular.one('cobudgets', $scope.currentBudgetId).get().then(function (aprl) {
                //console.log('cobudgets', aprl);
                $scope.original = aprl;
                $scope.cobudget = Restangular.copy($scope.original);
                //$scope.Displaymonth = $scope.original.month;
                for (var i = 0; i < $scope.cobudgetsArray.length; i++) {
                    //console.log('scope.cobudgetsArray.length',$scope.cobudgetsArray.length)
                    if ($scope.cobudgetsArray[i].id == $scope.original.month) {
                        // console.log('$scope.cobudgetsArray[i].id',$scope.cobudgetsArray[i].mymonth)
                        $scope.Displaymonth = $scope.cobudgetsArray[i].mymonth;
                    }
                }
                Restangular.one('spmbudgetyears', $scope.original.year).get().then(function (yr) {
                    $scope.Current_Year = yr.name;
                });
                $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
                $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
                $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);


                /*
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.cobudget.humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.cobudget.genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.cobudget.travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.cobudget.specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.cobudget.msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.cobudget.administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.cobudget.monitoringevaluation;
                
                 $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                */
            });
        });

        Restangular.all('cobudgets?filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
            //console.log('cobudgets', resp)
            $scope.remainingbudget = resp;
            $scope.totalremainig_humanresource = 0;
            $scope.totalremainig_genericactivity = 0;
            $scope.totalremainig_travelandcommunication = 0;
            $scope.totalremainig_specificactivity = 0;
            $scope.totalremainig_msgtgactivity = 0;
            $scope.totalremainig_administrativeexpense = 0;
            $scope.totalremainig_monitoringevaluation = 0;
            for (var i = 0; i < $scope.remainingbudget.length; i++) {
                $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
            }
            $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
            $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
            $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
            $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
            $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
            $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
            $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

            $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            //console.log('$scope.remaininghumanresources', $scope.remaininghumanresources);
            //console.log('$scope.totalremainig_humanresource', $scope.totalremainig_humanresource);
            //console.log('$scope.totalremainig_travelandcommunication', $scope.totalremainig_travelandcommunication);

        });


        $scope.BudgetEditModal1();
    }

    $scope.spmbudgetupdate = {};

    $scope.Update = function () {
        document.getElementById('swastihumanresources').style.borderColor = "";
        document.getElementById('cohumanresources').style.borderColor = "";
        document.getElementById('swastitravelandcommunication').style.borderColor = "";
        document.getElementById('cotravelandcommunication').style.borderColor = "";
        document.getElementById('swastigenericactivity').style.borderColor = "";
        document.getElementById('cogenericactivity').style.borderColor = "";
        document.getElementById('swastispecificactivity').style.borderColor = "";
        document.getElementById('cospecificactivity').style.borderColor = "";
        document.getElementById('swastimsgtgactivity').style.borderColor = "";
        document.getElementById('comsgtgactivity').style.borderColor = "";
        document.getElementById('swastiadministrativeexpense').style.borderColor = "";
        document.getElementById('coadministrativeexpense').style.borderColor = "";
        document.getElementById('swastimonitoringevaluation').style.borderColor = "";
        document.getElementById('comonitoringevaluation').style.borderColor = "";
        if ($scope.cobudget.swastihumanresources === '' || $scope.cobudget.swastihumanresources === null) {
            $scope.validatestring = $scope.validatestring + $scope.enterhumanressw;
            document.getElementById('swastihumanresources').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cohumanresources === '' || $scope.cobudget.cohumanresources === null) {
            $scope.validatestring = $scope.validatestring + $scope.enterhumanresco;
            document.getElementById('cohumanresources').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastitravelandcommunication === '' || $scope.cobudget.swastitravelandcommunication === null) {
            $scope.validatestring = $scope.validatestring + $scope.entertravelsw;
            document.getElementById('swastitravelandcommunication').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cotravelandcommunication === '' || $scope.cobudget.cotravelandcommunication === null) {
            $scope.validatestring = $scope.validatestring + $scope.entertravelco;
            document.getElementById('cotravelandcommunication').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastigenericactivity === '' || $scope.cobudget.swastigenericactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.entergenericsw;
            document.getElementById('swastigenericactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cogenericactivity === '' || $scope.cobudget.cogenericactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.entergenericco;
            document.getElementById('cogenericactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastispecificactivity === '' || $scope.cobudget.swastispecificactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.enterspecificsw;
            document.getElementById('swastispecificactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cospecificactivity === '' || $scope.cobudget.cospecificactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.enterspecificco;
            document.getElementById('cospecificactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastimsgtgactivity === '' || $scope.cobudget.swastimsgtgactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.entermsmsw;
            document.getElementById('swastimsgtgactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.comsgtgactivity === '' || $scope.cobudget.comsgtgactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.entermsmco;;
            document.getElementById('comsgtgactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastiadministrativeexpense === '' || $scope.cobudget.swastiadministrativeexpense === null) {
            $scope.validatestring = $scope.validatestring + $scope.enteradministrativesw;
            document.getElementById('swastiadministrativeexpense').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.coadministrativeexpense === '' || $scope.cobudget.coadministrativeexpense === null) {
            $scope.validatestring = $scope.validatestring + $scope.enteradministrativeco;
            document.getElementById('coadministrativeexpense').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastimonitoringevaluation === '' || $scope.cobudget.swastimonitoringevaluation === null) {
            $scope.validatestring = $scope.validatestring + $scope.entermonitoringsw;
            document.getElementById('swastimonitoringevaluation').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.comonitoringevaluation === '' || $scope.cobudget.comonitoringevaluation === null) {
            $scope.validatestring = $scope.validatestring + $scope.entermonitoringco;
            document.getElementById('comonitoringevaluation').style.borderColor = "#FF0000";
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            //console.log('SAVE', $scope.cobudget);
            // Restangular.one('spmbudgets', $scope.cobudget.year).get().then(function (res) {
            Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][year]=' + $scope.cobudget.year).getList().then(function (res) {
                $scope.spmBudget = res[0].totalamount;
                $scope.spmBudgetAmount = res[0].balancetilldate;

                //console.log('$scope.spmBudget',$scope.spmBudget)
                //   $scope.cobudget.balancetilldate = $scope.spmBudget - $scope.cobudget.totalamount;


                //     $scope.cobudget.balancetilldate = $scope.spmBudgetAmount - $scope.cobudget.totalamount;

                $scope.cobudget.balancetilldate = $scope.spmBudgetAmount + ($scope.currentBalAmount - $scope.cobudget.totalamount);
                $scope.spmbudgetupdate.balancetilldate = $scope.spmBudgetAmount + ($scope.currentBalAmount - $scope.cobudget.totalamount);

                $scope.cobudget.remaininghumanresources = $scope.remaininghumanresources;
                $scope.cobudget.remaininggenericactivity = $scope.remaininggenericactivity;
                $scope.cobudget.remainingtravelandcommunication = $scope.remainingtravelandcommunication;
                $scope.cobudget.remainingspecificactivity = $scope.remainingspecificactivity;
                $scope.cobudget.remainingmsgtgactivity = $scope.remainingmsgtgactivity;
                $scope.cobudget.remainingadministrativeexpense = $scope.remainingadministrativeexpense;
                $scope.cobudget.remainingmonitoringevaluation = $scope.remainingmonitoringevaluation;
                $scope.cobudget.remainingtotal = $scope.Remaining_Sum;

                //    console.log('$scope.spmbudgetupdate.balancetilldate', $scope.spmbudgetupdate.balancetilldate);                 
                //    console.log('$scope.cobudget.balancetilldate', $scope.cobudget.balancetilldate);

                //console.log('$scope.currentBudgetId', $scope.currentBudgetId);

                Restangular.one('cobudgets', $scope.currentBudgetId).customPUT($scope.cobudget).then(function (updateResponse) {
                    // console.log('updateResponse', updateResponse);
                    Restangular.one('spmbudgets', $scope.spmBudgetId).customPUT($scope.spmbudgetupdate).then(function (updateResponse) {
                        $scope.modalAW.close();
                        //location.reload();



                        Restangular.one('spmbudgetyears', $scope.current_YEAR).get().then(function (yearResa) {
                            $scope.Current_Year = yearResa.name;
                        });
                        Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][year]=' + $scope.current_YEAR).getList().then(function (res) {
                            $scope.spmBudget = res[0].totalamount;
                            $scope.spmBudgetAmount = res[0].balancetilldate;
                            $scope.spmBudgetId = res[0].id;
                            $scope.Balance_amount = $scope.spmBudget - $scope.Total_CO_HumanResource;
                            //console.log('$scope.spmBudget', $scope.spmBudget)
                            /* for(var b=0; b<res.length;b++){
                                 $
                             } */

                        });
                        // console.log('$scope.cobudgetsArray',$scope.cobudgetsArray)
                        //$scope.cobudgetsArray = $scope.cobudgetsArray;
                        angular.forEach($scope.cobudgetsArray, function (member, index) {
                            member.index = index + 1;
                            member.totalamounts = $scope.getTotalAmount(member.id, $scope.current_YEAR);
                            //        $scope.TotalData = [];
                            //        $scope.TotalData.push(member);
                        });
                    });
                });
            });
        }
    }
    $scope.BudgetEditModal1 = function () {
        $scope.modalAW = $modal.open({
            animation: true,
            templateUrl: 'template/spmbudgetView.html',
            scope: $scope,
            backdrop: 'static',
            size: 'lg'
        });
    };
    $scope.Cancel = function () {
        $scope.modalAW.close();
        //location.reload();
    }
    /****************ADD BUDGET*******************/
    $scope.cobudget = {
        deleteflag: false,
        comember: $window.sessionStorage.UserEmployeeId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        lastmodifiedbyrole: $window.sessionStorage.roleId,
        facility: $window.sessionStorage.coorgId,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId
    };
    $scope.callMsg = function () {
        alert('You have already created budget for this month');
    }

    //console.log(' $window.sessionStorage.UserEmployeeId', $window.sessionStorage.UserEmployeeId);
    $scope.validatestring = ''
    $scope.AddBudget = function (nam, monthid, year) {



        $scope.original = null;
        console.log('AddBudget');
        $scope.isCreateView = true;
        $scope.cobudget = {
            year: $scope.current_YEAR,
            deleteflag: false,
            comember: $window.sessionStorage.UserEmployeeId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            lastmodifiedbyrole: $window.sessionStorage.roleId,
            facility: $window.sessionStorage.coorgId,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId
        };
        $scope.Swasti_Total = 0;
        $scope.CO_Total = 0;
        $scope.Total_Sum = 0;
        //$scope.Remaining_Sum = 0;
        $scope.remaininghumanresources = "";
        $scope.remaininggenericactivity = null;
        $scope.remainingtravelandcommunication = null;
        $scope.remainingspecificactivity = null;
        $scope.remainingmsgtgactivity = null;
        $scope.remainingadministrativeexpense = null;
        $scope.remainingmonitoringevaluation = null;
        $scope.Remaining_Sum = 0;
        //console.log('name', nam)
        $scope.AlreadyCreated = '';
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        } else {
            //Restangular.one('cobudgets?filter[where][year]=' + $scope.cobudget.year + '&filter[where][month]=' + monthid + '&filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.UserEmployeeId).get().then(function (coRes) {
            Restangular.one('cobudgets?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.UserEmployeeId).get().then(function (coRes) {
                //$scope.AlreadyCreated = coRes.month;
                //console.log('CoRes',coRes);
                $scope.AlreadyCreated = coRes.length;
                //console.log('coRes.length', $scope.AlreadyCreated)
                // console.log('coRes',coRes)
                if ($scope.AlreadyCreated == 1) {
                    $scope.modalAW.close();
                    console.log('Shi h')
                    $scope.toggleValidation2();
                    //$scope.validatestring = $scope.validatestring + 'You have already created budget for this month';
                }
                /* else {
                     console.log('Shi NHI h')
                 }*/
            });

            Restangular.all('cobudgets?filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('cobudgets', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);

                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                //console.log('$scope.remaininghumanresources', $scope.remaininghumanresources);
                //console.log('$scope.totalremainig_travelandcommunication', $scope.totalremainig_travelandcommunication);

            });
        }



        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            $scope.Displaymonth = nam;
            $scope.cobudget.month = monthid;
            $scope.modalAW = $modal.open({
                animation: true,
                templateUrl: 'template/spmbudgetView.html',
                scope: $scope,
                backdrop: 'static',
                size: 'lg'
            });
        }
    }
    $scope.modalTitle = 'Thank You';
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.modalInstance1 = $modal.open({
            animation: true,
            templateUrl: 'template/validation.html',
            scope: $scope,
            backdrop: 'static',
            size: 'sm'
        });
    };
    $scope.toggleValidation2 = function () {
        $scope.modalInstance1 = $modal.open({
            animation: true,
            templateUrl: 'template/validation2.html',
            scope: $scope,
            backdrop: 'static',
            size: 'sm'
        });
    };
    $scope.OkValidation = function () {
        $scope.modalInstance1.close();
    };

    $scope.Save = function () {

        document.getElementById('swastihumanresources').style.borderColor = "";
        document.getElementById('cohumanresources').style.borderColor = "";
        document.getElementById('swastitravelandcommunication').style.borderColor = "";
        document.getElementById('cotravelandcommunication').style.borderColor = "";
        document.getElementById('swastigenericactivity').style.borderColor = "";
        document.getElementById('cogenericactivity').style.borderColor = "";
        document.getElementById('swastispecificactivity').style.borderColor = "";
        document.getElementById('cospecificactivity').style.borderColor = "";
        document.getElementById('swastimsgtgactivity').style.borderColor = "";
        document.getElementById('comsgtgactivity').style.borderColor = "";
        document.getElementById('swastiadministrativeexpense').style.borderColor = "";
        document.getElementById('coadministrativeexpense').style.borderColor = "";
        document.getElementById('swastimonitoringevaluation').style.borderColor = "";
        document.getElementById('comonitoringevaluation').style.borderColor = "";
        if ($scope.cobudget.swastihumanresources == '' || $scope.cobudget.swastihumanresources == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount By Swasti';
            document.getElementById('swastihumanresources').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cohumanresources == '' || $scope.cobudget.cohumanresources == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount By CO';
            document.getElementById('cohumanresources').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastitravelandcommunication == '' || $scope.cobudget.swastitravelandcommunication == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount By Swasti';
            document.getElementById('swastitravelandcommunication').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cotravelandcommunication == '' || $scope.cobudget.cotravelandcommunication == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount By CO';
            document.getElementById('cotravelandcommunication').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastigenericactivity == '' || $scope.cobudget.swastigenericactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount By Swasti';
            document.getElementById('swastigenericactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cogenericactivity == '' || $scope.cobudget.cogenericactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount By CO';
            document.getElementById('cogenericactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastispecificactivity == '' || $scope.cobudget.swastispecificactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount By Swasti';
            document.getElementById('swastispecificactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cospecificactivity == '' || $scope.cobudget.cospecificactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount By CO';
            document.getElementById('cospecificactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastimsgtgactivity == '' || $scope.cobudget.swastimsgtgactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter MSM/TG Activity Amount By Swasti';
            document.getElementById('swastimsgtgactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.comsgtgactivity == '' || $scope.cobudget.comsgtgactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter MSM/TG Activity Amount By CO';
            document.getElementById('comsgtgactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastiadministrativeexpense == '' || $scope.cobudget.swastiadministrativeexpense == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount By Swasti';
            document.getElementById('swastiadministrativeexpense').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.coadministrativeexpense == '' || $scope.cobudget.coadministrativeexpense == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount By CO';
            document.getElementById('coadministrativeexpense').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastimonitoringevaluation == '' || $scope.cobudget.swastimonitoringevaluation == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount By Swasti';
            document.getElementById('swastimonitoringevaluation').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.comonitoringevaluation == '' || $scope.cobudget.comonitoringevaluation == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount By CO';
            document.getElementById('comonitoringevaluation').style.borderColor = "#FF0000";

        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            console.log('SAVE', $scope.cobudget);

            $scope.spmbudgetupdate.balancetilldate = $scope.spmBudgetAmount - $scope.cobudget.totalamount;
            $scope.cobudget.balancetilldate = $scope.spmBudgetAmount - $scope.cobudget.totalamount;

            $scope.cobudget.remaininghumanresources = $scope.remaininghumanresources;
            $scope.cobudget.remaininggenericactivity = $scope.remaininggenericactivity;
            $scope.cobudget.remainingtravelandcommunication = $scope.remainingtravelandcommunication;
            $scope.cobudget.remainingspecificactivity = $scope.remainingspecificactivity;
            $scope.cobudget.remainingmsgtgactivity = $scope.remainingmsgtgactivity;
            $scope.cobudget.remainingadministrativeexpense = $scope.remainingadministrativeexpense;
            $scope.cobudget.remainingmonitoringevaluation = $scope.remainingmonitoringevaluation;
            $scope.cobudget.remainingtotal = $scope.Remaining_Sum;
            // console.log('$scope.spmbudgetupdate.balancetilldate', $scope.spmbudgetupdate.balancetilldate);
            //console.log('$scope.cobudget.balancetilldate', $scope.cobudget.balancetilldate);

            Restangular.all('cobudgets').post($scope.cobudget).then(function (cobudgetsRes) {

                Restangular.one('spmbudgets', $scope.spmBudgetId).customPUT($scope.spmbudgetupdate).then(function (updateResponse) {
                    // console.log('cobudgetsRes', cobudgetsRes);
                    $scope.cobudget.year = $scope.current_YEAR;
                    $scope.modalAW.close();
                    //$route.reload();

                    Restangular.one('spmbudgetyears', $scope.current_YEAR).get().then(function (yearResa) {
                        $scope.Current_Year = yearResa.name;
                    });
                    Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][year]=' + $scope.current_YEAR).getList().then(function (res) {
                        $scope.spmBudget = res[0].totalamount;
                        $scope.spmBudgetAmount = res[0].balancetilldate;
                        $scope.spmBudgetId = res[0].id;
                        $scope.Balance_amount = $scope.spmBudget - $scope.Total_CO_HumanResource;
                        //console.log('$scope.spmBudget', $scope.spmBudget)
                        /* for(var b=0; b<res.length;b++){
                             $
                         } */

                    });
                    // console.log('$scope.cobudgetsArray',$scope.cobudgetsArray)
                    //$scope.cobudgetsArray = $scope.cobudgetsArray;
                    angular.forEach($scope.cobudgetsArray, function (member, index) {
                        member.index = index + 1;
                        member.totalamounts = $scope.getTotalAmount(member.id, $scope.current_YEAR);
                        //        $scope.TotalData = [];
                        //        $scope.TotalData.push(member);
                    });
                });
            });
        }
    }
    /************************************GET TOTAL *******************************/
    $scope.HumanresourceTotal = function () {};
    $scope.HumanresourceTotal1 = function () {

        $scope.cobudget.humanresources = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.cohumanresources);
        //$scope.remaininghumanresources = $scope.totalremainig_humanresource + $scope.cobudget.humanresources;
        //init = 100,entered=200,remaining=500,actualremain=400
        if ($scope.original != undefined && $scope.original != null) {
            $scope.initialswastiamount = +$scope.original.swastihumanresources;
            $scope.enteredswastiamount = +$scope.cobudget.swastihumanresources;
            $scope.initialcoamount = +$scope.original.cohumanresources;
            $scope.enteredcoamount = +$scope.cobudget.cohumanresources;
            $scope.presentremaining = +$scope.totalremainig_humanresource;
            console.log('$scope.totalremainig_humanresource bef', $scope.totalremainig_humanresource);
            $scope.actualremaining = $scope.presentremaining + $scope.initialswastiamount + $scope.initialcoamount - $scope.enteredswastiamount - $scope.enteredcoamount;
            $scope.remaininghumanresources = $scope.actualremaining;
        } else {
            //$scope.remaininghumanresources = $scope.totalremainig_humanresource;
            $scope.remaininghumanresources = (+$scope.totalremainig_humanresource) - ((+$scope.cobudget.swastihumanresources) + (+$scope.cobudget.cohumanresources));
        }

        //        $scope.remaininghumanresources = ($scope.totalremainig_humanresource) - (parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.cohumanresources));
        //        $scope.remaininghumanresources1 = $scope.totalremainig_humanresource - $scope.cobudget.humanresources;
        //        $scope.remaininghumanresources2 = $scope.spmhumanresources - $scope.totalremainig_humanresource + $scope.cobudget.humanresources;
        //        console.log('$scope.cobudget.humanresources', $scope.cobudget.humanresources);
        //        console.log('$scope.spmhumanresources', $scope.spmhumanresources);
    }
    $scope.TravelandCommunication = function () {}
    $scope.TravelandCommunication1 = function () {
        $scope.cobudget.travelandcommunication = parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.cotravelandcommunication);
        if ($scope.original != undefined && $scope.original != null) {
            $scope.initialswastiamount2 = +$scope.original.swastitravelandcommunication;
            $scope.enteredswastiamount2 = +$scope.cobudget.swastitravelandcommunication;
            $scope.initialcoamount2 = +$scope.original.cotravelandcommunication;
            $scope.enteredcoamount2 = +$scope.cobudget.cotravelandcommunication;
            //$scope.presentremaining2 = +$scope.totalremainig_travelandcommunication;//+$scope.remainingtravelandcommunication; 
            $scope.presentremaining2 = $scope.remainingtravelandcommunication;



            console.log('$scope.initialswastiamount2 bef', $scope.initialswastiamount2);
            console.log('$scope.enteredswastiamount2 bef', $scope.enteredswastiamount2);
            console.log('$scope.initialcoamount2 bef', $scope.initialcoamount2);
            console.log('$scope.enteredcoamount2 bef', $scope.enteredcoamount2);
            console.log('$scope.presentremaining2 bef', $scope.presentremaining2);
            console.log('$scope.totalremainig_travelandcommunication bef', $scope.totalremainig_travelandcommunication);
            console.log('$scope.remainingtravelandcommunication bef', $scope.remainingtravelandcommunication);

            $scope.actualremaining2 = $scope.presentremaining2 + $scope.initialswastiamount2 + $scope.initialcoamount2 - $scope.enteredswastiamount2 - $scope.enteredcoamount2;
            $scope.remainingtravelandcommunication = $scope.actualremaining2;
            console.log('$scope.remainingtravelandcommunication aft', $scope.remainingtravelandcommunication);
        } else {
            $scope.remainingtravelandcommunication = (+$scope.totalremainig_travelandcommunication) - ((+$scope.cobudget.swastitravelandcommunication) + (+$scope.cobudget.cotravelandcommunication));
            console.log('$scope.remainingtravelandcommunication2', $scope.remainingtravelandcommunication);
        }
    }
    $scope.Genericactivity = function () {}
    $scope.Genericactivity1 = function () {
        $scope.cobudget.genericactivity = parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.cogenericactivity);
        /* if ($scope.original != undefined && $scope.original != null) {
            $scope.initialswastiamount = +$scope.original.swastigenericactivity;
            $scope.enteredswastiamount = +$scope.cobudget.swastigenericactivity;
            $scope.initialcoamount = +$scope.original.cogenericactivity;
            $scope.enteredcoamount = +$scope.cobudget.cogenericactivity;
            $scope.presentremaining = +$scope.totalremainig_genericactivity;

            $scope.actualremaining = $scope.presentremaining + $scope.initialswastiamount + $scope.initialcoamount - $scope.enteredswastiamount - $scope.enteredcoamount;
            $scope.remaininggenericactivity = $scope.actualremaining;
        } else {
            //$scope.remaininghumanresources = $scope.totalremainig_humanresource;
            $scope.remaininggenericactivity = (+$scope.totalremainig_genericactivity) - ((+$scope.cobudget.swastigenericactivity) + (+$scope.cobudget.cogenericactivity));
        }*/
    }
    $scope.Specificactivity = function () {}
    $scope.Specificactivity1 = function () {
        $scope.cobudget.specificactivity = parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.cospecificactivity);
        $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.cobudget.specificactivity;
    }
    $scope.Msgtgactivity = function () {}
    $scope.Msgtgactivity1 = function () {
        $scope.cobudget.msgtgactivity = parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.comsgtgactivity);
        $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.cobudget.msgtgactivity;
    }
    $scope.Administrativeexpense = function () {}
    $scope.Administrativeexpense1 = function () {
        $scope.cobudget.administrativeexpense = parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.coadministrativeexpense);
        $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.cobudget.administrativeexpense;
    }
    $scope.Monitoringevaluation = function () {}
    $scope.Monitoringevaluation1 = function () {
        $scope.cobudget.monitoringevaluation = parseInt($scope.cobudget.swastimonitoringevaluation) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.cobudget.monitoringevaluation;
    }
    $scope.SwastiSum1 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum2 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum3 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum4 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum5 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum6 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum7 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    }
    $scope.COSum1 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum2 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum3 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum4 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum5 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum6 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum7 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    /***************************************CO SUM *********************************
    $scope.COSum1 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum2 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum3 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum4 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum5 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum6 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum7 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    
    /******************************** Remaining Sum****************************************
    
    $scope.Remaining1 = function() {
        $scope.Remaining_Sum = $scope.remaininghumanresources;
        console.log('$scope.Remaining_Sum',$scope.Remaining_Sum);
    }
        
     $scope.Remaining2 = function() {
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remainingtravelandcommunication;
         console.log('$scope.Remaining_Sum2',$scope.Remaining_Sum);
    }    
    /************************************************************************************/

    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.selyear
        $scope.enterhumanressw = langResponse.enterhumanressw;
        $scope.enterhumanresco = langResponse.enterhumanresco;
        $scope.entertravelsw = langResponse.entertravelsw;
        $scope.entertravelco = langResponse.entertravelco;
        $scope.entergenericsw = langResponse.entergenericsw;
        $scope.entergenericco = langResponse.entergenericco;
        $scope.enterspecificsw = langResponse.enterspecificsw;
        $scope.enterspecificco = langResponse.enterspecificco;
        $scope.entermsmsw = langResponse.entermsmsw;
        $scope.entermsmco = langResponse.entermsmco;
        $scope.enteradministrativesw = langResponse.enteradministrativesw;
        $scope.enteradministrativeco = langResponse.enteradministrativeco;
        $scope.entermonitoringsw = langResponse.entermonitoringsw;
        $scope.entermonitoringco = langResponse.entermonitoringco;
    });

});
