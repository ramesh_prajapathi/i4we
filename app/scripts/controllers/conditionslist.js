'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/
        $rootScope.clickFW();
        $scope.ConditionLanguage = {};
        $scope.ConditionList = [];

        Restangular.one('conditionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.ConditionLanguage = langResponse[0];
        });

        $scope.UserLanguage = $window.sessionStorage.language;

        $scope.someFocusVariable = true;
        $scope.FocusMe = true;

        $scope.conditionsdsply = Restangular.all('conditions?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][enabledFor]=true' + '&filter[where][deleteFlag]=false').getList().$object;

        $scope.conditionsteps = Restangular.all('conditionsteps?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().$object;

        $scope.conditionstatuses = Restangular.all('conditionstatuses?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().$object;

        /*********************** List call *****************************************/

        $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;
        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;
        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;
        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;
        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;
        $scope.roledsply = Restangular.all('roles').getList().$object;
        $scope.conditiondsplyNew = Restangular.all('conditions?filter[where][deleteFlag]=false').getList().$object;
        $scope.stepsdsplyNew = Restangular.all('conditionsteps?filter[where][deleteFlag]=false').getList().$object;
        $scope.statusesdsplyNew = Restangular.all('conditionstatuses?filter[where][deleteFlag]=false').getList().$object;
        $scope.followupsdsply = Restangular.all('conditionfollowups?filter[where][deleteFlag]=false').getList().$object;
        $scope.reasonsdsply = Restangular.all('reasonforcancellations?filter[where][deleteFlag]=false').getList().$object;

        $scope.conditiondsply = Restangular.all('conditions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.stepsdsply = Restangular.all('conditionsteps?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.statusesdsply = Restangular.all('conditionstatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.conchecksdsply = Restangular.all('conditionchecklists?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.diagnosisdsply = Restangular.all('conditiondiagnosis?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.mydiagheaders = Restangular.all('diagnosisheaders?filter[where][deleteFlag]=false&filter[where][associatedHF]=' + $window.sessionStorage.userId).getList().$object;

        $scope.mychckheaders = Restangular.all('checklistheaders?filter[where][deleteFlag]=false&filter[where][associatedHF]=' + $window.sessionStorage.userId).getList().$object;


        $scope.filterFields = ['HHheadname', 'HHId', 'mobile', 'membername'];
        $scope.searchInput = '';

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.filterCall = 'conditionheaderviews?filter[where][deleteFlag]=false&filter[where][associatedhf]=' + $window.sessionStorage.userId + '&filter[where][language]=' + $window.sessionStorage.language;

            $scope.myTrailerCall = 'conditiontrailers?filter[where][deleteFlag]=false&filter[where][associatedHF]=' + $window.sessionStorage.userId;

        } else {

            $scope.filterCall = 'conditionheaderviews?filter={"where":{"and":[{"siteid":{"inq":[' + $window.sessionStorage.siteId + ']}},{"language":{"inq":[' + $window.sessionStorage.language + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.myTrailerCall = 'conditiontrailers?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        }

        Restangular.all($scope.myTrailerCall).getList().then(function (ctns) {

            //$scope.contrailerdisplay = ctns;
            $scope.mycontrailers = ctns;

            Restangular.all($scope.filterCall).getList().then(function (cns) {

                $scope.conditionheaders = cns;

                $scope.ConditionArray = [];

                $scope.ConditionList = [];

                $scope.exportArray = [];
                $scope.modalInstanceLoad.close();

                angular.forEach($scope.conditionheaders, function (value, index) {

                    $scope.checkdisplay = [];

                    $scope.diagdisplay = [];

                    $scope.contrailerdisplay = [];

                    for (var os = 0; os < $scope.mychckheaders.length; os++) {
                        if (value.id == $scope.mychckheaders[os].conditionHeaderId) {
                            $scope.checkdisplay.push($scope.mychckheaders[os]);
                        }
                    }

                    for (var mi = 0; mi < $scope.mydiagheaders.length; mi++) {
                        if (value.id == $scope.mydiagheaders[mi].conditionHeaderId) {
                            $scope.diagdisplay.push($scope.mydiagheaders[mi]);
                        }
                    }

                    for (var vd = 0; vd < $scope.mycontrailers.length; vd++) {
                        if (value.id == $scope.mycontrailers[vd].conditionHeaderId) {
                            $scope.contrailerdisplay.push($scope.mycontrailers[vd]);
                            // console.log($scope.contrailerdisplay);
                        }
                    }

                    value.index = index + 1;

                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                    if (value.syncouttime == null) {
                        value.syncouttime = '';
                    }

                    if (value.conditionfollowupname == null) {
                        value.conditionfollowupname = '';
                    }

                    if (value.followupdateDsply == null) {
                        value.followupdateDsply = '';
                    }

                    if (value.reasonforcancellationname == null) {
                        value.reasonforcancellationname = '';
                    }

                    if (value.screenedbyname == null) {
                        value.screenedbyname = '';
                    }

                    value.checkListName = "";

                    for (var i = 0; i < $scope.checkdisplay.length; i++) {
                        value.status = $scope.checkdisplay[i].status;
                        for (var j = 0; j < $scope.conchecksdsply.length; j++) {
                            if ($scope.conchecksdsply[j].id + "" === $scope.checkdisplay[i].checklist + "") {
                                if (value.checkListName === "") {
                                    value.checkListName = $scope.conchecksdsply[j].name + ' - ' + value.status;
                                } else {
                                    value.checkListName = value.checkListName + ' , ' + $scope.conchecksdsply[j].name + ' - ' + value.status;
                                }
                                break;
                            }
                        }
                    }

                    value.diagnosisName = "";

                    for (var i = 0; i < $scope.diagdisplay.length; i++) {
                        for (var j = 0; j < $scope.diagnosisdsply.length; j++) {
                            if ($window.sessionStorage.language == 1) {
                                if ($scope.diagnosisdsply[j].id + "" === $scope.diagdisplay[i].diagnosis + "") {
                                    if (value.diagnosisName === "") {
                                        value.diagnosisName = $scope.diagnosisdsply[j].name + ' - ' + $scope.diagdisplay[i].value;
                                       // console.log(value.diagnosisName);
                                    } else {
                                        value.diagnosisName = value.diagnosisName + ' , ' + $scope.diagnosisdsply[j].name + ' - ' + $scope.diagdisplay[i].value;
                                       // console.log(value.diagnosisName);
                                    }
                                    break;
                                }
                            } else {
                                if ($scope.diagnosisdsply[j].parentId + "" === $scope.diagdisplay[i].diagnosis + "") {
                                    if (value.diagnosisName === "") {
                                        value.diagnosisName = $scope.diagnosisdsply[j].name + ' - ' + $scope.diagdisplay[i].value;
                                      //  console.log(value.diagnosisName);
                                    } else {
                                        value.diagnosisName = value.diagnosisName + ' , ' + $scope.diagnosisdsply[j].name + ' - ' + $scope.diagdisplay[i].value;
                                      //  console.log(value.diagnosisName);
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    value.trailerDetails = "";
                    value.trailerDetails1 = "";
                    value.trailerDetails2 = "";
                    value.trailerDetails3 = "";
                    value.trailerDetails4 = "";
                    value.trailerDetails5 = "";
                    $scope.myArray = [];

                    angular.forEach($scope.contrailerdisplay, function (mydata, index) {
                        if (mydata.status > 0) {

                            mydata.createdDate = $filter('date')(mydata.createdDate, 'dd/MM/yyyy');

                            for (var eee = 0; eee < $scope.statusesdsplyNew.length; eee++) {

                                if ($scope.statusesdsplyNew[eee].id + "" === mydata.status + "") {

                                    for (var ddd = 0; ddd < $scope.stepsdsplyNew.length; ddd++) {

                                        if ($scope.stepsdsplyNew[ddd].id + "" === mydata.step + "") {

                                            value.trailerDetails = $scope.statusesdsplyNew[eee].name + ' - ' + mydata.createdDate;

                                            $scope.myArray.push(value.trailerDetails);

                                            value.trailerDetails1 = $scope.myArray[0];
                                            value.trailerDetails2 = $scope.myArray[1];
                                            value.trailerDetails3 = $scope.myArray[2];
                                            value.trailerDetails4 = $scope.myArray[3];
                                            value.trailerDetails5 = $scope.myArray[4];

                                            //console.log(value.trailerDetails1);
                                        }
                                    }
                                }
                            }
                        }

                    });

                    $scope.ConditionList.push(value);

                    $scope.exportArray.push(value);
                });
            });
        });

        /************************** Condition Filter ******************************/

        $scope.$watch('condition', function (newValue, oldValue) {
            if (newValue == '' || newValue == oldValue) {
                return;
            } else {

                $scope.stepId = '';
                $scope.statusId = '';

                $scope.ConditionList = [];

                if ($window.sessionStorage.roleId + "" === "3") {

                    $scope.filterCall = 'conditionheaderviews?filter[where][deleteFlag]=false&filter[where][associatedhf]=' + $window.sessionStorage.userId + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][conditionid]=' + newValue;
                } else {

                    $scope.filterCall = 'conditionheaderviews?filter={"where":{"and":[{"siteid":{"inq":[' + $window.sessionStorage.siteId + ']}},{"language":{"inq":[' + $window.sessionStorage.language + ']}},{"deleteFlag":{"inq":[false]}},{"conditionid":{"inq":[' + newValue + ']}}]}}';

                }

                Restangular.all($scope.filterCall).getList().then(function (cns) {
                    $scope.conditionheaders = cns;

                    angular.forEach($scope.conditionheaders, function (value, index) {

                        value.index = index + 1;

                        value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                        value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                        value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                        if (value.syncouttime == null) {
                            value.syncouttime = '';
                        }

                        $scope.ConditionList.push(value);
                    });
                });
            }
        });

        /************************** Step Filter ******************************/

        $scope.$watch('stepId', function (newValue, oldValue) {
            if (newValue == '' || newValue == oldValue) {
                return;
            } else {

                $scope.statusId = '';

                $scope.ConditionList = [];

                if ($window.sessionStorage.roleId + "" === "3") {

                    $scope.filterCall = 'conditionheaderviews?filter[where][deleteFlag]=false&filter[where][associatedhf]=' + $window.sessionStorage.userId + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][conditionid]=' + $scope.condition + '&filter[where][conditionstepid]=' + newValue;

                } else {

                    $scope.filterCall = 'conditionheaderviews?filter={"where":{"and":[{"siteid":{"inq":[' + $window.sessionStorage.siteId + ']}},{"language":{"inq":[' + $window.sessionStorage.language + ']}},{"deleteFlag":{"inq":[false]}},{"conditionid":{"inq":[' + $scope.condition + ']}},{"conditionstepid":{"inq":[' + newValue + ']}}]}}';

                }

                Restangular.all($scope.filterCall).getList().then(function (cns) {
                    $scope.conditionheaders = cns;

                    angular.forEach($scope.conditionheaders, function (value, index) {

                        value.index = index + 1;

                        value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                        value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                        value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                        if (value.syncouttime == null) {
                            value.syncouttime = '';
                        }

                        $scope.ConditionList.push(value);
                    });
                });
            }
        });

        /************************** Status Filter ******************************/

        $scope.$watch('statusId', function (newValue, oldValue) {
            if (newValue == '' || newValue == oldValue) {
                return;
            } else {

                $scope.ConditionList = [];

                if ($window.sessionStorage.roleId + "" === "3") {

                    $scope.filterCall = 'conditionheaderviews?filter[where][deleteFlag]=false&filter[where][associatedhf]=' + $window.sessionStorage.userId + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][conditionid]=' + $scope.condition + '&filter[where][conditionstepid]=' + $scope.stepId + '&filter[where][conditionstatusid]=' + newValue;

                } else {

                    $scope.filterCall = 'conditionheaderviews?filter={"where":{"and":[{"siteid":{"inq":[' + $window.sessionStorage.siteId + ']}},{"language":{"inq":[' + $window.sessionStorage.language + ']}},{"deleteFlag":{"inq":[false]}},{"conditionid":{"inq":[' + $scope.condition + ']}},{"conditionstepid":{"inq":[' + $scope.stepId + ']}},{"conditionstatusid":{"inq":[' + newValue + ']}}]}}';
                }

                Restangular.all($scope.filterCall).getList().then(function (cns) {
                    $scope.conditionheaders = cns;

                    angular.forEach($scope.conditionheaders, function (value, index) {

                        value.index = index + 1;

                        value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                        value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                        value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                        if (value.syncouttime == null) {
                            value.syncouttime = '';
                        }

                        $scope.ConditionList.push(value);
                    });
                });
            }
        });

        /**************************Export data to excel sheet ***************/

        $scope.DisableExport = false;
        $scope.valConCount = 0;

        $scope.exportData = function () {
            $scope.ConditionArray = [];
            $scope.valConCount = 0;
            if ($scope.exportArray.length == 0) {
                alert('No data found');
            } else {
                for (var arr = 0; arr < $scope.exportArray.length; arr++) {
                    $scope.ConditionArray.push({
                        'Sr No': $scope.exportArray[arr].index,
                        'COUNTRY': $scope.exportArray[arr].countryname,
                        'STATE': $scope.exportArray[arr].statename,
                        'DISTRICT': $scope.exportArray[arr].districtname,
                        'SITE': $scope.exportArray[arr].sitename,
                        'MEMBER NAME': $scope.exportArray[arr].membername,
                        'HEAD OF THE HOUSEHOLD': $scope.exportArray[arr].householdname,
                        'INDIVIDUAL ID': $scope.exportArray[arr].individualId,
                        'ASSIGNED TO': $scope.exportArray[arr].associatedhfname,
                        'SCREEN AND TREAT': $scope.exportArray[arr].conditionname,
                        'STEP': $scope.exportArray[arr].conditionstepname,
                        'STATUS': $scope.exportArray[arr].conditionstatusname,
                        'CHECK LIST': $scope.exportArray[arr].checkListName,
                        'CASE CLOSED': $scope.exportArray[arr].caseClosed,
                        'DIAGNOSIS': $scope.exportArray[arr].diagnosisName,
                        'RISK ASSESSMENT': $scope.exportArray[arr].trailerDetails1,
                        'SCREENING': $scope.exportArray[arr].trailerDetails2,
                        'TESTING': $scope.exportArray[arr].trailerDetails3,
                        'TREATMENT': $scope.exportArray[arr].trailerDetails4,
                        'RETESTING': $scope.exportArray[arr].trailerDetails5,
                        'SCREENED BY': $scope.exportArray[arr].screenedbyname,
                        'FOLLOW UP': $scope.exportArray[arr].conditionfollowupname,
                        'FOLLOW UP DATE': $scope.exportArray[arr].followupdateDsply,
                        'REASON FOR CANCELLING': $scope.exportArray[arr].reasonforcancellationname,
                        'CREATED BY': $scope.exportArray[arr].createdbyname,
                        'CREATED DATE': $scope.exportArray[arr].createdDate,
                        'CREATED ROLE': $scope.exportArray[arr].createdbyrolename,
                        'LAST MODIFIED BY': $scope.exportArray[arr].lastmodifiedbyname,
                        'LAST MODIFIED DATE': $scope.exportArray[arr].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.exportArray[arr].lastmodifiedbyrolename,
                        'DELETE FLAG': $scope.conditionheaders[arr].deleteFlag
                    });

                    $scope.valConCount++;
                    if ($scope.exportArray.length == $scope.valConCount) {
                        alasql('SELECT * INTO XLSX("screenandtreats.xlsx",{headers:true}) FROM ?', [$scope.ConditionArray]);
                    }
                }
            }
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

    });
