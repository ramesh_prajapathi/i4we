'use strict';

angular.module('secondarySalesApp')
    .controller('EduCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 9) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/education/create' || $location.path() === '/education/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/education/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/education/create' || $location.path() === '/education/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/education/create' || $location.path() === '/education/edit/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        //console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/education-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        /*********/

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        //  $scope.educations = Restangular.all('educations').getList().$object;

        if ($routeParams.id) {

            $scope.message = 'Education has been Updated!';

            Restangular.one('educations', $routeParams.id).get().then(function (education) {
                $scope.original = education;
                $scope.education = Restangular.copy($scope.original);
            });

            Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablangs) {
                $scope.tablanguages1 = tablangs;
                $scope.tablanguages1.splice(0, 1);
                $scope.currValue = 0;

                Restangular.all('educations?filter[where][deleteFlag]=false&filter[where][parentId]=' + $routeParams.id + '&filter[where][parentFlag]=false').getList().then(function (intvls1) {
                    $scope.mtlangs1 = intvls1;

                    angular.forEach($scope.tablanguages1, function (member, index) {
                        member.index = index;

                        var data = $scope.mtlangs1.filter(function (arr) {
                            return arr.language == member.id
                        })[0];

                        if (data == undefined) {
                            $scope.rowArray.push({
                                language: member.id,
                                id: 0
                            });
                        } else {
                            $scope.rowArray.push(data);
                        }

                    });
                });
            });


        } else {
            $scope.message = 'Education has been Created!';
        }

        $scope.Search = $scope.name;

        /********************** Watch Fucntion ********************/

        $scope.showenglishLang = true;

        $scope.$watch('education.name', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id) {
                return;
            } else {
                angular.forEach($scope.rowArray, function (member, index) {
                    member.index = index;
                    member.name = newValue;
                });
            }
        });

        $scope.$watch('education.language', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                } else {
                    $scope.showenglishLang = true;
                }
            }
        });

        /*************************** INDEX *******************************************/

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (lng) {
            $scope.languages = lng;
        });

        Restangular.all('educations?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.educations = zn;
            angular.forEach($scope.educations, function (member, index) {
                member.index = index + 1;

                for (var a = 0; a < $scope.languages.length; a++) {
                    if (member.language == $scope.languages[a].id) {
                        member.languagename = $scope.languages[a].name;
                        break;
                    }
                }
            });
        });

        Restangular.all('educations?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (edu) {
            $scope.englisheducations = edu;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        $scope.rowArray = [];

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablang) {
            $scope.tablanguages = tablang;
            $scope.tablanguages.splice(0, 1);
            $scope.currValue = 0;

            angular.forEach($scope.tablanguages, function (member, index) {
                member.index = index;

                if (!$routeParams.id) {
                    $scope.rowArray.push({
                        language: member.id,
                        name: ''
                    });
                }

                if (member.index == 0) {
                    member.active = true;
                    member.visible = true;
                } else {
                    member.active = false;
                    member.visible = false;
                }
            });
        });

        $scope.tabClick = function (row) {
            $scope.tablanguages[row.index].visible = true;
            if (row.index != 0) {
                $scope.tablanguages[0].visible = false;
            }
            $scope.currValue = row.index;
        };

        /*************************** SAVE *******************************************/

        $scope.education = {
            "name": '',
            "deleteFlag": false,
            "language": 1
        };

        $scope.validatestring = '';

        $scope.Save = function () {

            document.getElementById('name').style.border = "";

            if ($scope.education.name == '' || $scope.education.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Education';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.education.parentId === '') {
                    delete $scope.education['parentId'];
                }

                $scope.submitDisable = true;
                $scope.education.parentFlag = $scope.showenglishLang;

                Restangular.all('educations').post($scope.education).then(function (response) {
                    $scope.langFunc(response.id);
                }, function (error) {
                    if (error.data.error.constraint === 'education_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.langcount = 0;

        $scope.langFunc = function (intervalId) {

            if ($scope.langcount < $scope.rowArray.length) {

                $scope.rowArray[$scope.langcount].parentFlag = false;
                $scope.rowArray[$scope.langcount].parentId = intervalId;
                $scope.rowArray[$scope.langcount].deleteFlag = false;

                Restangular.all('educations').post($scope.rowArray[$scope.langcount]).then(function () {
                    $scope.langcount++;
                    $scope.langFunc(intervalId);
                });

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/education-list';
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /********************** UPDATE *******************************************/

        $scope.validatestring = '';

        $scope.Update = function () {

            document.getElementById('name').style.border = "";

            if ($scope.education.name == '' || $scope.education.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Education';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.education.parentId === '') {
                    delete $scope.education['parentId'];
                }
                $scope.submitDisable = true;

                Restangular.one('educations', $routeParams.id).customPUT($scope.education).then(function (resp) {
                    $scope.langUpdateFunc(resp.id);
                }, function (error) {
                    if (error.data.error.constraint === 'education_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        $scope.langUpdatecount = 0;

        $scope.langUpdateFunc = function (intervalId) {

            if ($scope.langUpdatecount < $scope.rowArray.length) {

                if ($scope.rowArray[$scope.langUpdatecount].id == 0 || $scope.rowArray[$scope.langUpdatecount].id == "0") {
                    $scope.rowArray[$scope.langUpdatecount].parentFlag = false;
                    $scope.rowArray[$scope.langUpdatecount].parentId = intervalId;
                    $scope.rowArray[$scope.langUpdatecount].deleteFlag = false;
                    delete $scope.rowArray[$scope.langUpdatecount]['id'];

                    Restangular.all('educations').post($scope.rowArray[$scope.langUpdatecount]).then(function () {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId);
                    });

                } else {

                    Restangular.one('educations', $scope.rowArray[$scope.langUpdatecount].id).customPUT($scope.rowArray[$scope.langUpdatecount]).then(function () {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId);
                    });
                }

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/education-list';
            }
        };


        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        
/******************************************************** DELETE *******************************************/
        
        $scope.Delete = function (id) {
            
            $scope.item = [{
                deleteFlag: true
            }]

            Restangular.one('educations/' + id).customPUT($scope.item[0]).then(function () {
                Restangular.all('educations?filter[where][parentId]=' + id).getList().then(function (stypes) {
                    $scope.deleteRows = stypes;
                    $scope.DeleteFunc();
                });
            });
        };

        $scope.updateFlag = {
            deleteFlag: true
        };

        $scope.DeleteCount = 0;

        $scope.DeleteFunc = function (id) {

            if ($scope.DeleteCount < $scope.deleteRows.length) {

                Restangular.one('educations/' + $scope.deleteRows[$scope.DeleteCount].id).customPUT($scope.updateFlag).then(function () {
                    $scope.DeleteCount++;
                    $scope.DeleteFunc();
                });

            } else {
                $route.reload();
            }
        };

    });
