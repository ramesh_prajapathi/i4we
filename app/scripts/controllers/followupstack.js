'use strict';

angular.module('secondarySalesApp')
	.controller('followupstacksCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/followupstacks/create' || $location.path() === '/followupstacks/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/followupstacks/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/followupstacks/create' || $location.path() === '/followupstacks/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/followupstacks/create' || $location.path() === '/followupstacks/' + $routeParams.id;
			return visible;
		};

		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}  else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
		}


		
		if ($window.sessionStorage.prviousLocation != "partials/followupstack") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}
		
		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};
		/*********/

		$scope.submitfollowupstacks = Restangular.all('followupstacks').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'Stake-Holder Follow Up has been Updated!';
			Restangular.one('followupstacks', $routeParams.id).get().then(function (followupstack) {
				$scope.original = followupstack;
				$scope.followupstack = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Stake-Holder Follow Up has been Created!';
		}

		$scope.searchfollowupstack = $scope.name;

		/********************************************************* INDEX *******************************************/
		$scope.zn = Restangular.all('followupstacks?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.followupstacks = zn;
			angular.forEach($scope.followupstacks, function (member, index) {
				member.index = index + 1;
			});
		});

		/************************************ SAVE *******************************************/
		$scope.followupstack = {
			name: '',
			deleteflag: false
		};
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Savefollowupstack = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.followupstack.name == '' || $scope.followupstack.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.followupstack.hnname == '' || $scope.followupstack.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.followupstack.knname == '' || $scope.followupstack.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.followupstack.taname == '' || $scope.followupstack.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.followupstack.tename == '' || $scope.followupstack.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.followupstack.mrname == '' || $scope.followupstack.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitfollowupstacks.post($scope.followupstack).then(function () {
					console.log('followupstack Saved');
					window.location = '/followupstacks';
				});
			};
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/********************************* UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Updatefollowupstack = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.followupstack.name == '' || $scope.followupstack.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.followupstack.hnname == '' || $scope.followupstack.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.followupstack.knname == '' || $scope.followupstack.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.followupstack.taname == '' || $scope.followupstack.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.followupstack.tename == '' || $scope.followupstack.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.followupstack.mrname == '' || $scope.followupstack.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitfollowupstacks.customPUT($scope.followupstack).then(function () {
					console.log('followupstack Saved');
					window.location = '/followupstacks';
				});
			}


		};
		/********************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('followupstacks/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

	});
