'use strict';

angular.module('secondarySalesApp')
    .controller('ImportHouseHoldCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $http, $window, $route, $fileUploader) {

        $scope.entriesdisplay = [];

        $scope.householdDis = [];
        $scope.hideSubmit = true;
        $scope.DisableValidate = true;
        $scope.modalTitle = 'Wait';
        $scope.ValidationmessageTitle = 'Thank You';
        $scope.message = 'Households Uploading...';
        $scope.savemessage = 'Uploaded Sucessfully...';
        $scope.validatestring1 = 'Some Of your data is not in proper format. Click on "Download Error Text" to download the error log.';
        $scope.validatestringxls = 'Your xls is not in specified format.';
        $scope.showFileUpload = true;
        $scope.savepartialdataModal = false;
        $scope.showFileText = true;

        /************************************* Watch Function **************************/

        $scope.countryId = '';
        $scope.stateId = '';
        $scope.districtId = '';

        Restangular.all('countries?filter[where][deleteFlag]=false').getList().then(function (ct) {
            $scope.countries = ct;
            $scope.countryId = ct[0].id;
        });

        $scope.$watch('countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                Restangular.all('states?filter[where][countryId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (responceSt) {
                    $scope.displaystates = responceSt;
                });
                $scope.countiesid = +newValue;
            }
        });

        $scope.$watch('stateId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                Restangular.all('districts?filter[where][deleteFlag]=false' + '&filter[where][stateId]=' + newValue).getList().then(function (dt) {
                    $scope.displaydistricts = dt;
                });
            }
            $scope.statesid = +newValue;
        });

        $scope.$watch('districtId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {

                Restangular.all('sites?filter[where][districtId]=' + newValue + '&filter[where][stateId]=' + $scope.statesid + '&filter[where][countryId]=' + $scope.countiesid + '&filter[where][deleteFlag]=false').getList().then(function (ctyRes) {
                    $scope.displaysites = ctyRes;
                });
            }
        });

        $scope.member = [];

        $scope.household = [];

        $scope.stateupdate = {};

        $scope.$watch('siteId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $scope.showFileUpload = false;

                Restangular.one('states', $scope.stateId).get().then(function (ste) {

                    var num = ste.sequence,
                        str = num.toString(),
                        len = str.length;

                    if (len == 1) {
                        $scope.seqSize = '0000';
                    } else if (len == 2) {
                        $scope.seqSize = '000'
                    } else if (len == 3) {
                        $scope.seqSize = '00'
                    } else if (len == 4) {
                        $scope.seqSize = '0'
                    } else if (len == 5) {
                        $scope.seqSize = '';
                    }

                    Restangular.one('sites', newValue).get().then(function (site) {
                        $scope.hhId = ste.code + '' + site.code + '' + $scope.seqSize;
                        $scope.hhIdSeq = ste.sequence;
                    });
                });
            }
        });

        /******************** End ****************************************/

        var X = XLSX;
        var XW = {
            /* worker message */
            msg: 'xlsx',
            /* worker scripts */
            rABS: 'scripts/services/xlsxworker2.js',
            norABS: 'scripts/services/xlsxworker1.js',
            noxfer: 'scripts/services/xlsxworker.js'
        };


        var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
        if (!rABS) {
            document.getElementsByName("userabs")[0].disabled = true;
            document.getElementsByName("userabs")[0].checked = false;
        }

        var use_worker = typeof Worker !== 'undefined';
        if (!use_worker) {
            document.getElementsByName("useworker")[0].disabled = true;
            document.getElementsByName("useworker")[0].checked = false;
        }

        function xw_noxfer(data, cb) {
            var worker = new Worker(XW.noxfer);
            worker.onmessage = function (e) {
                switch (e.data.t) {
                    case 'ready':
                        break;
                    case 'e':
                        console.error(e.data.d);
                        break;
                    case XW.msg:
                        cb(JSON.parse(e.data.d));
                        break;
                }
            };
            var arr = rABS ? data : btoa(fixdata(data));
            worker.postMessage({
                d: arr,
                b: rABS
            });
        }

        function xw_xfer(data, cb) {
            var worker = new Worker(rABS ? XW.rABS : XW.norABS);
            worker.onmessage = function (e) {
                switch (e.data.t) {
                    case 'ready':
                        break;
                    case 'e':
                        console.error(e.data.d);
                        break;
                    default:
                        var xx = ab2str(e.data).replace(/\n/g, "\\n").replace(/\r/g, "\\r");
                        console.log("done");
                        $scope.DisableValidate = false;
                        $scope.showFileText = false;
                        cb(JSON.parse(xx));
                        break;
                }
            };
            if (rABS) {
                var val = s2ab(data);
                worker.postMessage(val[1], [val[1]]);
            } else {
                worker.postMessage(data, [data]);
            }
        }

        function xw(data, cb) {
            //transferable = document.getElementsByName("xferable")[0].checked;
            transferable = true;
            if (transferable) xw_xfer(data, cb);
            else xw_noxfer(data, cb);
        }

        var transferable = use_worker;
        if (!transferable) {
            document.getElementsByName("xferable")[0].disabled = true;
            document.getElementsByName("xferable")[0].checked = false;
        }

        function s2ab(s) {
            var b = new ArrayBuffer(s.length * 2),
                v = new Uint16Array(b);
            for (var i = 0; i != s.length; ++i) v[i] = s.charCodeAt(i);
            return [v, b];
        }

        function ab2str(data) {
            var o = "",
                l = 0,
                w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w)));
            return o;
        }

        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }

        function to_csv(workbook) {
            var result = [];
            workbook.SheetNames.forEach(function (sheetName) {
                var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
                if (csv.length > 0) {
                    result.push("SHEET: " + sheetName);
                    result.push("");
                    result.push(csv);
                }
            });
            return result.join("\n");
        }

        function to_formulae(workbook) {
            var result = [];
            workbook.SheetNames.forEach(function (sheetName) {
                var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
                if (formulae.length > 0) {
                    result.push("SHEET: " + sheetName);
                    result.push("");
                    result.push(formulae.join("\n"));
                }
            });
            return result.join("\n");
        }


        /*   $scope.xlsentries = {
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			deleteflag: false
		}; 
    
    console.log('$window.sessionStorage.UserEmployeeId',$window.sessionStorage.UserEmployeeId); */

        function process_wb(wb) {
            var output = "";
            switch (get_radio_value("format")) {
                case "csv":
                    output = to_csv(wb);
                    break;
                case "form":
                    output = to_formulae(wb);
                    break;
                default:
                    output = JSON.stringify(to_json(wb), 2, 2);
                    $scope.entries = to_json(wb);
                    $scope.xlsentries = $scope.entries.Sheet1;

                    angular.forEach($scope.xlsentries, function (member, index) {
                        //Just add the index to your item
                        member.index = index;
                        $scope.entriesdisplay.push(member);
                    });

                    // console.log('output', $scope.entries.Sheet1);
            }
            /*if (out.innerText === undefined) out.textContent = output;
            else out.innerText = output;*/
            if (typeof console !== 'undefined') console.log("output", new Date());
        }

        function get_radio_value(radioName) {
            var radios = document.getElementsByName(radioName);
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].checked || radios.length === 1) {
                    return radios[i].value;
                }
            }
        }

        var xlf = document.getElementById('xlf');

        var xlf1 = document.getElementById('xlf1');
        //console.log('Event Start');

        function handleFile(e) {
            $scope.hideSubmit = true;
            $scope.hideValidate = false;
            //console.log('Event');
            //rABS = document.getElementsByName("userabs")[0].checked;
            // use_worker = document.getElementsByName("useworker")[0].checked;
            rABS = true;
            use_worker = true;
            var files = e.target.files;
            var f = files[0]; {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = function (e) {
                    if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                    var data = e.target.result;
                    if (use_worker) {
                        xw(data, process_wb);
                    } else {
                        var wb;
                        if (rABS) {
                            wb = X.read(data, {
                                type: 'binary'
                            });
                        } else {
                            var arr = fixdata(data);
                            wb = X.read(btoa(arr), {
                                type: 'base64'
                            });
                        }
                        process_wb(wb);
                    }
                };
                if (rABS) reader.readAsBinaryString(f);
                else reader.readAsArrayBuffer(f);
            }
        }

        function validateFileType(e) {
            $scope.DisableSubmit = true;
            $scope.xlsentries = [];
            // $scope.offermanagement.imagename = this.value.split(/[\/\\]/).pop();
            // console.log('fileInput',this.value);

            var _validVideoFileExtensions = [".xlsx", ".xls"];
            var sFileName = this.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validVideoFileExtensions.length; j++) {
                    var sCurExtension = _validVideoFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        $scope.CorrectFormat = true;
                        handleFile(e);
                        break;
                    }
                }

                if (!blnValid) {
                    alert("Sorry, " + sFileName.split(/[\/\\]/).pop() + " is invalid, allowed extensions are: " + _validVideoFileExtensions.join(", "));
                    xlf.value = null;
                    //$scope.uploader.queue[0].remove();
                    // console.log('$scope.uploader',$scope.uploader.queue[0]);
                    $scope.CorrectFormat = false;
                }
            }

            /* if (item.file.type != 'video/mp4') {
                 alert('Accepts Only .mp4 files');
                 item.remove();
             }*/
        };

        xlf.addEventListener('change', validateFileType, false);

        xlf1.addEventListener('change', validateFileType, false);

        /* var link = document.getElementById('download');
        link.href = 'http://flipkart-client.herokuapp.com/xlssheets/xlszone.xlsx'; */

        /* $scope.downloadfile = function () {
            $http({
                method: 'GET',
                url: '/xlssheets/xlszone.xlsx'
            }).
            success(function (data, status, headers, config) {
                var anchor = angular.element('<a/>');
                anchor.attr({
                    href: '/xlssheets/xlszone.xlsx',
                    target: '_blank',
                    download: '/xlssheets/xlszone.xlsx'
                })[0].click();

            }).
            error(function (data, status, headers, config) {
                // if there's an error you should see it here
            });

        } */

        $scope.UploadAgain = function () {
            angular.element("input[type='file']").val(null);
            $scope.xlsentries = [];
            $scope.entriesdisplay = [];
            $scope.hideValidate = false;
            $scope.DisableValidate = true;
            $scope.showFileText = true;
        };

        $scope.partners = Restangular.all('states').getList().$object;

        $scope.Save = function () {
            $scope.sheetValidation = false;
            $scope.count = 0;
            $scope.zonedataModal = true;
            $scope.item = $scope.entries.Sheet1;
            $scope.saveData();
            $scope.showDownload = true;
        };

        $scope.Save1 = function () {
            $scope.sheetValidation = false;
            $scope.count = 0;
            $scope.zonedataModal = true;
            $scope.item = $scope.entries.Sheet1;
            $scope.saveDataMemData();
            $scope.showDownload = false;
        };

        $scope.showHead = true;
        $scope.showMem = false;

        $scope.headUpload = function () {
            $scope.showHead = true;
            $scope.showMem = false;
        };

        $scope.memUpload = function () {
            $scope.showHead = false;
            $scope.showMem = true;
        };

        var saveCount = 0;

        var seqCount = 0;

        $scope.exportArray = [];

        $scope.saveData = function () {

            if (saveCount < $scope.item.length) {

                $scope.datatext = '';

                $scope.hhIdSeqValue = $scope.hhIdSeq + seqCount;

                var ageval = $scope.item[saveCount].age;
                var todaydate = new Date();
                var newdate = new Date(todaydate);
                newdate.setFullYear(newdate.getFullYear() - ageval);
                var nd = new Date(newdate);

                $scope.household[saveCount].HHId = $scope.hhId + $scope.hhIdSeqValue;
                $scope.household[saveCount].associatedHF = $scope.item[saveCount].associatedHF;
                $scope.household[saveCount].ssgThisHHBelongs = $scope.item[saveCount].ssgThisHHBelongs;
                $scope.household[saveCount].countryId = $scope.countryId;
                $scope.household[saveCount].stateId = $scope.stateId;
                $scope.household[saveCount].districtId = $scope.districtId;
                $scope.household[saveCount].siteId = $scope.siteId
                $scope.household[saveCount].createdDate = new Date();
                $scope.household[saveCount].createdBy = $window.sessionStorage.userId;
                $scope.household[saveCount].createdByRole = $window.sessionStorage.roleId;
                $scope.household[saveCount].lastModifiedDate = new Date();
                $scope.household[saveCount].lastModifiedBy = $window.sessionStorage.userId;
                $scope.household[saveCount].lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.household[saveCount].deleteFlag = false;

                $scope.member[saveCount].HHId = $scope.hhId + $scope.hhIdSeqValue;
                $scope.member[saveCount].individualId = $scope.member[saveCount].HHId + '-' + '1';
                $scope.member[saveCount].name = $scope.item[saveCount].nameOfHeadInHH;
                $scope.member[saveCount].associatedHF = $scope.item[saveCount].associatedHF;
                $scope.member[saveCount].dob = nd;
                $scope.member[saveCount].age = $scope.item[saveCount].age;
                $scope.member[saveCount].gender = $scope.item[saveCount].gender;
                $scope.member[saveCount].mobile = $scope.item[saveCount].mobile;
                $scope.member[saveCount].occupation = $scope.item[saveCount].occupation;
                $scope.member[saveCount].address = $scope.item[saveCount].address;
                $scope.member[saveCount].caste = $scope.item[saveCount].caste;
                $scope.member[saveCount].religion = $scope.item[saveCount].religion;
                $scope.member[saveCount].migrant = $scope.item[saveCount].migrant;
                $scope.member[saveCount].bplStatus = $scope.item[saveCount].bplStatus;
                $scope.member[saveCount].latitude = $scope.item[saveCount].latitude;
                $scope.member[saveCount].longitude = $scope.item[saveCount].longitude;
                $scope.member[saveCount].email = $scope.item[saveCount].email;
                $scope.member[saveCount].aadharNumber = $scope.item[saveCount].aadharNumber;
                $scope.member[saveCount].esiNumber = $scope.item[saveCount].esiNumber;
                $scope.member[saveCount].countryId = $scope.countryId;
                $scope.member[saveCount].stateId = $scope.stateId;
                $scope.member[saveCount].districtId = $scope.districtId;
                $scope.member[saveCount].siteId = $scope.siteId
                $scope.member[saveCount].createdDate = new Date();
                $scope.member[saveCount].createdBy = $window.sessionStorage.userId;
                $scope.member[saveCount].createdByRole = $window.sessionStorage.roleId;
                $scope.member[saveCount].lastModifiedDate = new Date();
                $scope.member[saveCount].lastModifiedBy = $window.sessionStorage.userId;
                $scope.member[saveCount].lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.member[saveCount].isHeadOfFamily = true;
                $scope.member[saveCount].deleteFlag = false;
                $scope.member[saveCount].healthyDays = 0;

                if ($scope.member[saveCount].migrant == 1) {
                    $scope.member[saveCount].migrantGoesHome = true;
                } else {
                    $scope.member[saveCount].migrantGoesHome = false;
                }

                Restangular.all('households').post($scope.household[saveCount]).then(function (xlsresp) {

                    $scope.member[saveCount].parentId = xlsresp.id;

                    $scope.exportArray.push(xlsresp);

                    Restangular.all('members').post($scope.member[saveCount]).then(function (memResp) {

                        $scope.stateupdate.sequence = $scope.hhIdSeqValue + 1;

                        Restangular.one('states', $scope.stateId).customPUT($scope.stateupdate).then(function (res) {

                            $scope.datatext = $scope.datatext + JSON.stringify($scope.item[saveCount].index + 2);
                            saveCount++;
                            seqCount++;
                            $scope.saveData();
                        });
                    }, function (response) {
                        console.log("Error with status code", response.status);
                        $scope.zonedataModal = false;
                        $scope.savepartialdataModal = true;
                    });

                }, function (response) {
                    console.log("Error with status code", response.error);
                    $scope.zonedataModal = false;
                    $scope.savepartialdataModal = true;
                });
            } else {

                $scope.zonedataModal = false;
                Restangular.all('households?filter[where][deleteFlag]=false').getList().then(function (HhResp) {
                    // console.log('HhResp',HhResp);
                    angular.forEach(HhResp, function (member, index) {
                        $scope.householdDis.push(member.id);

                    });
                });

                setTimeout(function () {
                    $scope.savedataModal = true;
                    angular.element("input[type='file']").val(null);
                    $scope.xlsentries = [];
                    $scope.entriesdisplay = [];
                    $scope.hideValidate = false;
                    $scope.DisableValidate = true;
                }, 200);
            }
        };

        var saveCountMem = 0;

        $scope.saveDataMemData = function () {

            if (saveCountMem < $scope.item.length) {

                $scope.datatext = '';

                var agevalQ = $scope.item[saveCountMem].age;
                var todaydateQ = new Date();
                var newdateQ = new Date(todaydateQ);
                newdateQ.setFullYear(newdateQ.getFullYear() - agevalQ);
                var ndQ = new Date(newdateQ);

                $scope.memberone[saveCountMem].HHId = $scope.item[saveCountMem].HHId;
                $scope.memberone[saveCountMem].individualId = $scope.item[saveCountMem].individualId;
                $scope.memberone[saveCountMem].name = $scope.item[saveCountMem].name;
                $scope.memberone[saveCountMem].associatedHF = $scope.item[saveCountMem].associatedHF;
                $scope.memberone[saveCountMem].relation = $scope.item[saveCountMem].relation;
                $scope.memberone[saveCountMem].dob = ndQ;
                $scope.memberone[saveCountMem].age = $scope.item[saveCountMem].age;
                $scope.memberone[saveCountMem].gender = $scope.item[saveCountMem].gender;
                $scope.memberone[saveCountMem].occupation = $scope.item[saveCountMem].occupation;
                $scope.memberone[saveCountMem].aadharNumber = $scope.item[saveCountMem].aadharNumber;
                $scope.memberone[saveCountMem].esiNumber = $scope.item[saveCountMem].esiNumber;
                $scope.memberone[saveCountMem].mobile = $scope.item[saveCountMem].mobile;
                $scope.memberone[saveCountMem].countryId = $scope.countryId;
                $scope.memberone[saveCountMem].stateId = $scope.stateId;
                $scope.memberone[saveCountMem].districtId = $scope.districtId;
                $scope.memberone[saveCountMem].siteId = $scope.siteId
                $scope.memberone[saveCountMem].createdDate = new Date();
                $scope.memberone[saveCountMem].createdBy = $window.sessionStorage.userId;
                $scope.memberone[saveCountMem].createdByRole = $window.sessionStorage.roleId;
                $scope.memberone[saveCountMem].lastModifiedDate = new Date();
                $scope.memberone[saveCountMem].lastModifiedBy = $window.sessionStorage.userId;
                $scope.memberone[saveCountMem].lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.memberone[saveCountMem].isHeadOfFamily = false;
                $scope.memberone[saveCountMem].deleteFlag = false;
                $scope.memberone[saveCountMem].parentId = $scope.item[saveCountMem].parentId;
                $scope.memberone[saveCountMem].literacyLevel = $scope.item[saveCountMem].literacyLevel;
                $scope.memberone[saveCountMem].nameOfSchool = $scope.item[saveCountMem].nameOfSchool;
                $scope.memberone[saveCountMem].occupation = $scope.item[saveCountMem].occupation;
                $scope.memberone[saveCountMem].nameOfFactory = $scope.item[saveCountMem].nameOfFactory;
                $scope.memberone[saveCountMem].shgId = $scope.item[saveCountMem].shgId;

                Restangular.all('members?filter[where][parentId]=' + $scope.item[saveCountMem].parentId + '&filter[where][isHeadOfFamily]=true').getList().then(function (repn) {

                    // $scope.memberone[saveCountMem].mobile = repn[0].mobile;
                    $scope.memberone[saveCountMem].email = repn[0].email;
                    $scope.memberone[saveCountMem].address = repn[0].address;
                    $scope.memberone[saveCountMem].caste = repn[0].caste;
                    $scope.memberone[saveCountMem].religion = repn[0].religion;
                    $scope.memberone[saveCountMem].migrant = repn[0].migrant;
                    $scope.memberone[saveCountMem].migrantGoesHome = repn[0].migrantGoesHome;
                    $scope.memberone[saveCountMem].bplStatus = repn[0].bplStatus;
                    $scope.memberone[saveCountMem].latitude = repn[0].latitude;
                    $scope.memberone[saveCountMem].longitude = repn[0].longitude;
                    $scope.memberone[saveCountMem].healthyDays = 0;

                    Restangular.all('members').post($scope.memberone[saveCountMem]).then(function (memRespData) {
                        $scope.datatext = $scope.datatext + JSON.stringify($scope.item[saveCountMem].index + 2);
                        saveCountMem++;
                        $scope.saveDataMemData();

                    }, function (response) {
                        console.log("Error with status code", response.status);
                        $scope.zonedataModal = false;
                        $scope.savepartialdataModal = true;
                    });
                });

            } else {

                $scope.zonedataModal = false;

                setTimeout(function () {
                    $scope.savedataModal = true;
                    angular.element("input[type='file']").val(null);
                    $scope.xlsentries = [];
                    $scope.entriesdisplay = [];
                    $scope.hideValidate = false;
                    $scope.DisableValidate = true;
                }, 200);
            }
        };

        $scope.reloadPage = function () {
            window.location = "/import";
        };

        /***********************NEW CHANGES FOR INVAILD ENTRY*********************/
        $scope.userDisply = [];
        $scope.genderDis = [];
        $scope.occupationDis = [];
        $scope.casteDis = [];
        $scope.religionDis = [];
        $scope.migrantDis = [];
        $scope.bplstatusDis = [];
        $scope.literacyDis = [];
        $scope.relationDis = [];


        Restangular.all('users?filter[where][deleteFlag]=false').getList().then(function (userResp) {
            angular.forEach(userResp, function (member, index) {
                $scope.userDisply.push(member.id);

            });
        });

        Restangular.all('genders?filter[where][deleteFlag]=false').getList().then(function (genderResp) {
            angular.forEach(genderResp, function (member, index) {
                $scope.genderDis.push(member.id);

            });
        });

        Restangular.all('occupations?filter[where][deleteFlag]=false').getList().then(function (occResp) {
            angular.forEach(occResp, function (member, index) {
                $scope.occupationDis.push(member.id);

            });
        });

        Restangular.all('castes?filter[where][deleteFlag]=false').getList().then(function (casteResp) {
            angular.forEach(casteResp, function (member, index) {
                $scope.casteDis.push(member.id);

            });
        });

        Restangular.all('religions?filter[where][deleteFlag]=false').getList().then(function (regResp) {
            angular.forEach(regResp, function (member, index) {
                $scope.religionDis.push(member.id);

            });
        });

        Restangular.all('migrants?filter[where][deleteFlag]=false').getList().then(function (migResp) {
            angular.forEach(migResp, function (member, index) {
                $scope.migrantDis.push(member.id);

            });
        });

        Restangular.all('bplstatuses?filter[where][deleteFlag]=false').getList().then(function (bplResp) {
            angular.forEach(bplResp, function (member, index) {
                $scope.bplstatusDis.push(member.id);

            });
        });

        Restangular.all('literacylevels?filter[where][deleteFlag]=false').getList().then(function (literacyResp) {
            angular.forEach(literacyResp, function (member, index) {
                $scope.literacyDis.push(member.id);

            });
        });

        Restangular.all('relationWithHHs?filter[where][deleteFlag]=false').getList().then(function (relationResp) {
            angular.forEach(relationResp, function (member, index) {
                $scope.relationDis.push(member.id);

            });
        });


        Restangular.all('households?filter[where][deleteFlag]=false').getList().then(function (HhResp) {
            angular.forEach(HhResp, function (member, index) {
                $scope.householdDis.push(member.id);

            });

        });


        /****************************************************************/

        $scope.Validate = function () {

            $scope.errortext = '';

            var re = /^-?[0-9]\d*(\.\d+)?$/;
            var ne = /^-?[0-9]\d*(\.\d+)?$/; /* number only  validation regular expression */
            var se = /^[a-zA-Z ]+$/; /* string only validation regular expression */
            var de = /^(0|[1-9]\d*)$/;

            $scope.household = [];
            $scope.member = [];
            $scope.xlslength = $scope.xlsentries.length;

            if ($scope.xlsentries[0]['nameOfHeadInHH'] && $scope.xlsentries[0]['associatedHF'] && $scope.xlsentries[0]['age'] && $scope.xlsentries[0]['gender'] && $scope.xlsentries[0]['mobile'] && $scope.xlsentries[0]['occupation'] && $scope.xlsentries[0]['address'] && $scope.xlsentries[0]['caste'] && $scope.xlsentries[0]['religion'] && $scope.xlsentries[0]['migrant'] && $scope.xlsentries[0]['bplStatus'] && $scope.xlsentries[0]['latitude'] && $scope.xlsentries[0]['longitude']) {
                console.log('presence');
                $scope.hideValidate = true;

                for (var i = 0; i < $scope.xlsentries.length; i++) {

                    if ($scope.xlsentries[i].nameOfHeadInHH == '' || $scope.xlsentries[i].nameOfHeadInHH == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column nameOfHeadInHH does not exist' + '\r\n';

                    }
                    if ($scope.xlsentries[i].associatedHF == '' || $scope.xlsentries[i].associatedHF == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column associatedHF does not exist' + '\r\n';

                    }


                    if (!de.test($scope.xlsentries[i].associatedHF) || $scope.xlsentries[i].associatedHF == 0) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column associatedHF does not follow format' + '\r\n';

                    }
                    /******************new changes*******************/
                    function userchecked(test) {
                        return test == $scope.xlsentries[i].associatedHF;
                    }

                    var obj = $scope.userDisply.find(userchecked);
                    // console.log('obj', obj);
                    if (obj == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column associatedHF does not exist' + '\r\n';
                    }

                    function genderchecked(gender) {
                        return gender == $scope.xlsentries[i].gender;
                    }

                    var genderobj = $scope.genderDis.find(genderchecked);
                    //  console.log('genderobj', genderobj);
                    if (genderobj == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column gender does not exist' + '\r\n';

                    }

                    function occupationchecked(occupation) {
                        return occupation == $scope.xlsentries[i].occupation;
                    }

                    var occupationobj = $scope.occupationDis.find(occupationchecked);
                    //console.log('occupationobj', occupationobj);
                    if (occupationobj == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column occupation does not exist' + '\r\n';

                    }

                    function castechecked(caste) {
                        return caste == $scope.xlsentries[i].caste;
                    }

                    var casteobj = $scope.casteDis.find(castechecked);
                    //console.log('casteobj', casteobj);
                    if (casteobj == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column caste does not exist' + '\r\n';

                    }

                    function religionchecked(religion) {
                        return religion == $scope.xlsentries[i].religion;
                    }

                    var religionobj = $scope.religionDis.find(religionchecked);
                    //console.log('religionobj', religionobj);
                    if (religionobj == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column religion does not exist' + '\r\n';

                    }

                    function migrantchecked(migrant) {
                        return migrant == $scope.xlsentries[i].migrant;
                    }

                    var migrantobj = $scope.migrantDis.find(migrantchecked);
                    //console.log('migrantobj', migrantobj);
                    if (migrantobj == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column migrant does not exist' + '\r\n';

                    }

                    function bplStatuschecked(bplStatus) {
                        return bplStatus == $scope.xlsentries[i].bplStatus;
                    }

                    var bplStatusobj = $scope.bplstatusDis.find(bplStatuschecked);
                    //console.log('bplStatusobj', bplStatusobj);
                    if (bplStatusobj == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column bplStatus does not exist' + '\r\n';

                    }

                    if ($scope.xlsentries[i].age == '' || $scope.xlsentries[i].age == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column age does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].age) || $scope.xlsentries[i].age == 0 || $scope.xlsentries[i].age > 99) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column age does not follow format' + '\r\n';

                    }
                    if ($scope.xlsentries[i].gender == '' || $scope.xlsentries[i].gender == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column gender does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].gender) || $scope.xlsentries[i].gender == 0) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column gender does not follow format' + '\r\n';

                    }

                    if ($scope.xlsentries[i].mobile == '' || $scope.xlsentries[i].mobile == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column mobile does not exist' + '\r\n';

                    }


                    if ($scope.xlsentries[i].mobile.length != 10) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column mobile does not follow format' + '\r\n';

                    }
                    if ($scope.xlsentries[i].occupation == '' || $scope.xlsentries[i].occupation == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column occupation does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].occupation) || $scope.xlsentries[i].occupation == 0) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column occupation does not follow format' + '\r\n';

                    }

                    if ($scope.xlsentries[i].address == '' || $scope.xlsentries[i].address == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column address does not exist' + '\r\n';

                    }
                    if ($scope.xlsentries[i].caste == '' || $scope.xlsentries[i].caste == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column caste does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].caste) || $scope.xlsentries[i].caste == 0) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column caste does not follow format' + '\r\n';

                    }

                    if ($scope.xlsentries[i].religion == '' || $scope.xlsentries[i].religion == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column religion does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].religion) || $scope.xlsentries[i].religion == 0) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column religion does not follow format' + '\r\n';

                    }



                    if ($scope.xlsentries[i].migrant == '' || $scope.xlsentries[i].migrant == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column migrant does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].migrant) || $scope.xlsentries[i].migrant == 0) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column migrant does not follow format' + '\r\n';

                    }



                    if ($scope.xlsentries[i].bplStatus == '' || $scope.xlsentries[i].bplStatus == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column bplStatus does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].bplStatus) || $scope.xlsentries[i].bplStatus == 0) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column bplStatus does not follow format' + '\r\n';

                    }

                    if ($scope.xlsentries[i].latitude == '' || $scope.xlsentries[i].latitude == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column latitude does not exist' + '\r\n';

                    }
                    if ($scope.xlsentries[i].longitude == '' || $scope.xlsentries[i].longitude == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column longitude does not exist' + '\r\n';
                    }

                    $scope.household.push({
                        nameOfHeadInHH: $scope.xlsentries[i].nameOfHeadInHH
                    });

                    $scope.member.push({
                        age: $scope.xlsentries[i].age
                    });
                }

                var textFile = null,
                    makeTextFile = function (text) {
                        var data = new Blob([text], {
                            type: 'text/plain'
                        });
                        // If we are replacing a previously generated file we need to
                        // manually revoke the object URL to avoid memory leaks.
                        if (textFile !== null) {
                            window.URL.revokeObjectURL(textFile);
                        }
                        textFile = window.URL.createObjectURL(data);
                        return textFile;
                    };

                if ($scope.errortext != '') {
                    $scope.showValidation = !$scope.showValidation;
                    var link = document.getElementById('downloadlink');
                    link.href = makeTextFile($scope.errortext);
                    link.style.display = 'block';
                } else {
                    $scope.sheetValidation = !$scope.sheetValidation;
                    $scope.hideSubmit = false;
                }
            } else {

                //   $scope.xlsValidation = !$scope.xlsValidation;

                if (!$scope.xlsentries[0]['nameOfHeadInHH']) {
                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column nameOfHeadInHH does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column nameOfHeadInHH does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['associatedHF']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column associatedHF does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column associatedHF does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['age']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column age does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column age does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['gender']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column gender does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column gender does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['mobile']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column mobile does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column mobile does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['occupation']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column occupation does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column occupation does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['address']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column address does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column address does not exist' + '\r\n';

                } else if (!$scope.xlsentries[0]['caste']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column caste does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column caste does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['religion']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column religion does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column religion does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['migrant']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column migrant does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column migrant does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['bplStatus']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column bplStatus does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column bplStatus does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['latitude']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column latitude does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column latitude does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['longitude']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column longitude does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column longitude does not exist' + '\r\n';

                }

                var textFile = null,
                    makeTextFile = function (text) {
                        var data = new Blob([text], {
                            type: 'text/plain'
                        });
                        // If we are replacing a previously generated file we need to
                        // manually revoke the object URL to avoid memory leaks.
                        if (textFile !== null) {
                            window.URL.revokeObjectURL(textFile);
                        }
                        textFile = window.URL.createObjectURL(data);
                        return textFile;
                    };

                if ($scope.errortext != '') {
                    $scope.showValidation = !$scope.showValidation;
                    var link = document.getElementById('downloadlink');
                    link.href = makeTextFile($scope.errortext);
                    link.style.display = 'block';
                } else {
                    $scope.sheetValidation = !$scope.sheetValidation;
                    $scope.hideSubmit = false;
                }
            }
        };

        $scope.Validate1 = function () {

            // $scope.callHH();
            Restangular.all('households?filter[where][deleteFlag]=false').getList().then(function (HhResp) {
                angular.forEach(HhResp, function (member, index) {
                    $scope.householdDis.push(member.id);

                });

            });

            $scope.errortext = '';

            var re = /^-?[0-9]\d*(\.\d+)?$/;
            var ne = /^-?[0-9]\d*(\.\d+)?$/; /* number only  validation regular expression */
            var se = /^[a-zA-Z ]+$/; /* string only validation regular expression */
            var de = /^(0|[1-9]\d*)$/;

            $scope.memberone = [];

            $scope.xlslength = $scope.xlsentries.length;

            if ($scope.xlsentries[0]['name'] && $scope.xlsentries[0]['associatedHF'] && $scope.xlsentries[0]['relation'] && $scope.xlsentries[0]['age'] && $scope.xlsentries[0]['literacyLevel'] && $scope.xlsentries[0]['gender'] && $scope.xlsentries[0]['individualId'] && $scope.xlsentries[0]['HHId'] && $scope.xlsentries[0]['parentId']) {
                console.log('presence');
                $scope.hideValidate = true;

                for (var i = 0; i < $scope.xlsentries.length; i++) {

                    if ($scope.xlsentries[i].name == '' || $scope.xlsentries[i].name == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column Name does not exist' + '\r\n';

                    }
                    if ($scope.xlsentries[i].associatedHF == '' || $scope.xlsentries[i].associatedHF == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column associatedHF does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].associatedHF) || $scope.xlsentries[i].associatedHF == 0) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column associatedHF does not follow format' + '\r\n';

                    }

                    function userchecked1(user) {
                        return user == $scope.xlsentries[i].associatedHF;
                    }

                    var userobj = $scope.userDisply.find(userchecked1);
                    //console.log('userobj', userobj);
                    if (userobj == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column associatedHF does not exist' + '\r\n';

                    }

                    function genderchecked1(gender) {
                        return gender == $scope.xlsentries[i].gender;
                    }

                    var genderobj1 = $scope.genderDis.find(genderchecked1);
                    // console.log('genderobj1', genderobj1);
                    if (genderobj1 == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column gender does not exist' + '\r\n';

                    }

                    function relationchecked(relation) {
                        return relation == $scope.xlsentries[i].relation;
                    }

                    var relationobj = $scope.relationDis.find(relationchecked);
                    //console.log('relationobj', relationobj);
                    if (relationobj == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column relation does not exist' + '\r\n';

                    }

                    function literacychecked(literacy) {
                        return literacy == $scope.xlsentries[i].literacyLevel;
                    }

                    var literacyobj = $scope.literacyDis.find(literacychecked);
                    // console.log('literacyobj', literacyobj);
                    if (literacyobj == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column literacy does not exist' + '\r\n';

                    }

                    function householdschecked(household) {
                        return household == $scope.xlsentries[i].parentId;
                    }

                    var householdobj = $scope.householdDis.find(householdschecked);
                  //  console.log('householdobj', householdobj);
                    if (householdobj == undefined) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column parentId does not exist' + '\r\n';

                    }
                    // var duplicate = _.findWhere($scope.householdDis, {id: $scope.xlsentries[i].parentId});
                    // console.log('duplicate', duplicate);

                    if ($scope.xlsentries[i].age == '' || $scope.xlsentries[i].age == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column age does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].age) || $scope.xlsentries[i].age == 0 || $scope.xlsentries[i].age > 99) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column age does not follow format' + '\r\n';

                    }
                    if ($scope.xlsentries[i].literacyLevel == '' || $scope.xlsentries[i].literacyLevel == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column literacy level does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].literacyLevel) || $scope.xlsentries[i].literacyLevel == 0) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column literacy level does not follow format' + '\r\n';

                    }
                    if ($scope.xlsentries[i].gender == '' || $scope.xlsentries[i].gender == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column gender does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].gender) || $scope.xlsentries[i].gender == 0) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column gender does not follow format' + '\r\n';

                    }
                    if ($scope.xlsentries[i].individualId == '' || $scope.xlsentries[i].individualId == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column individualId does not exist' + '\r\n';

                    }
                    if ($scope.xlsentries[i].HHId == '' || $scope.xlsentries[i].HHId == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column HHId does not exist' + '\r\n';

                    }
                    if ($scope.xlsentries[i].parentId == '' || $scope.xlsentries[i].parentId == null) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column parentId does not exist' + '\r\n';

                    }
                    if (!de.test($scope.xlsentries[i].parentId) || $scope.xlsentries[i].parentId == 0) {
                        $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column parentId does not follow format' + '\r\n';

                    }
                    if ($scope.xlsentries[i].mobile == '' || $scope.xlsentries[i].mobile == null) {
                        console.log('no mobile no');
                    } else {
                        if (!de.test($scope.xlsentries[i].mobile) || $scope.xlsentries[i].mobile.length != 10) {
                            $scope.errortext = $scope.errortext + 'Row ' + JSON.stringify($scope.xlsentries[i].index + 2) + ' / Column mobile does not follow format' + '\r\n';
                        }
                    }

                    $scope.memberone.push({
                        name: $scope.xlsentries[i].name
                    });
                }

                var textFile = null,
                    makeTextFile = function (text) {
                        var data = new Blob([text], {
                            type: 'text/plain'
                        });
                        // If we are replacing a previously generated file we need to
                        // manually revoke the object URL to avoid memory leaks.
                        if (textFile !== null) {
                            window.URL.revokeObjectURL(textFile);
                        }
                        textFile = window.URL.createObjectURL(data);
                        return textFile;
                    };

                if ($scope.errortext != '') {
                    $scope.showValidation = !$scope.showValidation;
                    var link = document.getElementById('downloadlink');
                    link.href = makeTextFile($scope.errortext);
                    link.style.display = 'block';
                } else {
                    $scope.sheetValidation = !$scope.sheetValidation;
                    $scope.hideSubmit = false;
                }
            } else {

                //  $scope.xlsValidation = !$scope.xlsValidation;

                if (!$scope.xlsentries[0]['name']) {
                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column name does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column name does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['associatedHF']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column associatedHF does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column associatedHF does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['relation']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column relation does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column relation does not exist' + '\r\n';

                } else if (!$scope.xlsentries[0]['age']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column age does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column age does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['literacyLevel']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column literacyLevel does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column literacyLevel does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['gender']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column gender does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column gender does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['individualId']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column individualId does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column individualId does not exist' + '\r\n';

                }
                if (!$scope.xlsentries[0]['HHId']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column HHId does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column HHId does not exist' + '\r\n';

                } else if (!$scope.xlsentries[0]['parentId']) {

                    $scope.errortext = $scope.errortext + 'Row ' + '1' + ' / Column parentId does not exist' + ' (or) ' + 'Row ' + '2' + ' / Column parentId does not exist' + '\r\n';

                }

                var textFile = null,
                    makeTextFile = function (text) {
                        var data = new Blob([text], {
                            type: 'text/plain'
                        });
                        // If we are replacing a previously generated file we need to
                        // manually revoke the object URL to avoid memory leaks.
                        if (textFile !== null) {
                            window.URL.revokeObjectURL(textFile);
                        }
                        textFile = window.URL.createObjectURL(data);
                        return textFile;
                    };

                if ($scope.errortext != '') {
                    $scope.showValidation = !$scope.showValidation;
                    var link = document.getElementById('downloadlink');
                    link.href = makeTextFile($scope.errortext);
                    link.style.display = 'block';
                } else {
                    $scope.sheetValidation = !$scope.sheetValidation;
                    $scope.hideSubmit = false;
                }

            }
        };

        $scope.DisableExport = false;
        $scope.valCount = 0;

        $scope.DownLoad = function () {

            $scope.expArray = [];
            $scope.valCount = 0;

            if ($scope.exportArray.length == 0) {
                alert('No data found');
            } else {

                for (var c = 0; c < $scope.exportArray.length; c++) {

                    $scope.expArray.push({
                        nameOfHeadInHH: $scope.exportArray[c].nameOfHeadInHH,
                        HHId: $scope.exportArray[c].HHId,
                        associatedHF: $scope.exportArray[c].associatedHF,
                        ssgThisHHBelongs: $scope.exportArray[c].ssgThisHHBelongs,
                        individualId: $scope.exportArray[c].HHId + '-' + '1',
                        parentId: $scope.exportArray[c].id,
                        isHeadOfFamily: true,
                        countryId: $scope.countryId,
                        stateId: $scope.stateId,
                        districtId: $scope.districtId,
                        siteId: $scope.siteId,
                        createdDate: new Date(),
                        createdBy: $window.sessionStorage.userId,
                        createdByRole: $window.sessionStorage.roleId,
                        lastModifiedDate: new Date(),
                        lastModifiedBy: $window.sessionStorage.userId,
                        lastModifiedByRole: $window.sessionStorage.roleId,
                        deleteFlag: false
                    });

                    $scope.valCount++;

                    if ($scope.exportArray.length == $scope.valCount) {
                        alasql('SELECT * INTO XLSX("households.xlsx",{headers:true}) FROM ?', [$scope.expArray]);
                    }
                }
            }
        };

        $scope.cancelBtn = function () {
            angular.element("input[type='file']").val(null);
            $scope.xlsentries = [];
            $scope.entriesdisplay = [];
            $scope.hideValidate = false;
            $scope.DisableValidate = true;
            $scope.showFileText = true;
            $scope.stateId = '';
            $scope.districtId = '';
            $scope.siteId = '';
            $scope.showFileUpload = true;
        };

    });
