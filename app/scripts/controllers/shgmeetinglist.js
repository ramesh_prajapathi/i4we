'use strict';

angular.module('secondarySalesApp')
    .controller('SHGMeetingsListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {

        $scope.SHGmeetingLanguage = {};
        $rootScope.clickFW();

        Restangular.one('shgmeetingLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.SHGmeetingLanguage = langResponse[0];

            // $scope.SHGmeetingheading = langResponse[0].shgCreate;
            //  $scope.modalTitle = langResponse[0].thankYou;

        });

        $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;
        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;
        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;
        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;
        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;
        $scope.roledsply = Restangular.all('roles').getList().$object;
    
        $scope.reasonforloansdsply = Restangular.all('reasonforloans?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.symptomsdsply = Restangular.all('symptoms?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.illnessesdsply = Restangular.all('illnesses?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            // console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        $scope.status = 'active';

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        Restangular.all('shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (shg) {
            $scope.shgs = shg;

            if ($window.sessionStorage.roleId + "" === "3") {

                $scope.filterCall = 'shgmeetingheaders?filter[where][deleteFlag]=false&filter[where][associatedHF]=' + $window.sessionStorage.userId;

                $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}}]}}';

                $scope.loanHeaderCall = 'loanheaders?filter[where][deleteFlag]=false&filter[where][associatedHF]=' + $window.sessionStorage.userId;

                $scope.shgtrailerfilterCall = 'shgmeetingtrailers?filter[where][deleteFlag]=false&filter[where][associatedHF]=' + $window.sessionStorage.userId;

                $scope.healthyCall = 'healthydays?filter[where][deleteFlag]=false&filter[where][associatedHF]=' + $window.sessionStorage.userId;

            } else {

                $scope.filterCall = 'shgmeetingheaders?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}}]}}';

                $scope.loanHeaderCall = 'loanheaders?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                $scope.shgtrailerfilterCall = 'shgmeetingtrailers?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                $scope.healthyCall = 'healthydays?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            }

            Restangular.all($scope.filterCall).getList().then(function (shgmeetingheader) {

                // Restangular.all('members?filter={"where":{"and":[{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (mems) {

                Restangular.all($scope.memberFilterCall).getList().then(function (memResp) {

                    //Restangular.all('loanheaders?filter[where][deleteFlag]=false').getList().then(function (loanhdr) {

                    Restangular.all($scope.loanHeaderCall).getList().then(function (loanhdr) {

                        $scope.shgmeetingheaders = shgmeetingheader;

                        $scope.loanheadersdisplay = loanhdr;

                        $scope.membersdisplay = memResp;

                        $scope.noOfMembersAttendedDisply = 0;
                        $scope.noOfMembersSavedDisply = 0;

                        $scope.modalInstanceLoad.close();

                        angular.forEach($scope.shgmeetingheaders, function (member, index) {
                            member.index = index + 1;

                            $scope.currentDate = new Date();
                            $scope.createdDate = new Date(member.createdDate);

                            var oneDay = 24 * 60 * 60 * 1000;
                            $scope.diffDays = Math.round(Math.abs(($scope.createdDate.getTime() - $scope.currentDate.getTime()) / (oneDay)));

                            if ($scope.diffDays < 1) {
                                member.disableEdit = true;
                            } else {
                                member.disableEdit = false;
                            }

                            member.amountSaved = +member.amountSaved;
                            for (var m = 0; m < $scope.shgs.length; m++) {
                                if (member.groupId == $scope.shgs[m].id) {
                                    member.groupName = $scope.shgs[m].groupName;
                                    if ($scope.shgs[m].membersInGroup != null) {
                                        member.membersInGroupLength = $scope.shgs[m].membersInGroup.split(',').length;
                                        break;
                                    }
                                }
                            }

                            member.lastMeetingDateDsply = $filter('date')(member.lastMeetingDate, 'dd-MMM-yyyy');
                            member.lastMeetingDateExport = $filter('date')(member.lastMeetingDate, 'dd/MM/yyyy');
                            member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                            member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                            var data2 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.associatedHF
                            })[0];

                            if (data2 != undefined) {
                                member.assignedTo = data2.name;
                            }
                            var data5 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data5 != undefined) {
                                member.countryname = data5.name;
                            }

                            var data6 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == member.stateId
                            })[0];

                            if (data6 != undefined) {
                                member.statename = data6.name;
                            }

                            var data7 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == member.districtId
                            })[0];

                            if (data7 != undefined) {
                                member.districtname = data7.name;
                            }
                            var data8 = $scope.sitedsply.filter(function (arr) {
                                return arr.id == member.siteId
                            })[0];

                            if (data8 != undefined) {
                                member.sitename = data8.name;
                            }

                            var data9 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.createdBy
                            })[0];

                            if (data9 != undefined) {
                                member.createdByname = data9.username;
                            }

                            var data10 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.lastModifiedBy
                            })[0];

                            if (data10 != undefined) {
                                member.lastModifiedByname = data10.username;
                            }

                            var data11 = $scope.roledsply.filter(function (arr) {
                                return arr.id == member.lastModifiedByRole
                            })[0];

                            if (data11 != undefined) {
                                member.lastModifiedByRolename = data11.name;
                            }

                            var data12 = $scope.roledsply.filter(function (arr) {
                                return arr.id == member.createdByRole
                            })[0];

                            if (data12 != undefined) {
                                member.createdByRolename = data12.name;
                            }


                            member.amountOfLoan = "";

                            for (var ew = 0; ew < $scope.loanheadersdisplay.length; ew++) {
                                for (var vs = 0; vs < $scope.membersdisplay.length; vs++) {
                                    if (member.shgMeetingId == $scope.loanheadersdisplay[ew].shgmeetingId) {
                                        if ($scope.loanheadersdisplay[ew].memberId == $scope.membersdisplay[vs].id) {
                                            if (member.loantaken == "") {
                                                member.loantaken = 'Member Name - ' + $scope.membersdisplay[vs].name + ' Loan Taken - ' + $scope.loanheadersdisplay[ew].amountOfLoan + ' Loan Repaid - ' + $scope.loanheadersdisplay[ew].installmentRepaid;
                                            } else {
                                                member.loantaken = member.loantaken + ', ' + 'Member Name - ' + $scope.membersdisplay[vs].name + ' Loan Taken - ' + $scope.loanheadersdisplay[ew].amountOfLoan + ' Loan Repaid - ' + $scope.loanheadersdisplay[ew].installmentRepaid;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        });
                    });
                });
            });

            Restangular.all($scope.shgtrailerfilterCall).getList().then(function (shgmeetingtrailer) {

                Restangular.all($scope.memberFilterCall).getList().then(function (memResp) {

                    Restangular.all($scope.loanHeaderCall).getList().then(function (loanhdr) {

                        Restangular.all($scope.filterCall).getList().then(function (shgmeetingheaderdsply) {

                            Restangular.all($scope.healthyCall).getList().then(function (healthy) {

                                $scope.shgmeetingtrailers = shgmeetingtrailer;

                                $scope.loanheadersdisplay1 = loanhdr;

                                $scope.membersdisplay1 = memResp;

                                $scope.shgmeetingheaderdsply = shgmeetingheaderdsply;

                                $scope.healthydays = healthy;

                                angular.forEach($scope.shgmeetingtrailers, function (member, index) {
                                    member.index = index + 1;

                                    var data01 = $scope.membersdisplay1.filter(function (arr) {
                                        return arr.id == member.memberId
                                    })[0];

                                    if (data01 != undefined) {
                                        member.memberName = data01.name;
                                        member.individualId = data01.individualId;
                                        member.hhId = data01.HHId;
                                    }

                                    var data02 = $scope.shgs.filter(function (arr) {
                                        return arr.id == member.groupId
                                    })[0];

                                    if (data02 != undefined) {
                                        member.shgName = data02.groupName;
                                    }

                                    var data03 = $scope.shgmeetingheaderdsply.filter(function (arr) {
                                        return arr.id == member.groupId
                                    })[0];

                                    if (data03 != undefined) {
                                        member.meetingDate = data03.groupName;
                                    }

                                    var data2 = $scope.usersdisplay.filter(function (arr) {
                                        return arr.id == member.associatedHF
                                    })[0];

                                    if (data2 != undefined) {
                                        member.assignedTo = data2.name;
                                    }

                                    var data5 = $scope.countrydisplay.filter(function (arr) {
                                        return arr.id == member.countryId
                                    })[0];

                                    if (data5 != undefined) {
                                        member.countryname = data5.name;
                                    }

                                    var data6 = $scope.statedisplay.filter(function (arr) {
                                        return arr.id == member.stateId
                                    })[0];

                                    if (data6 != undefined) {
                                        member.statename = data6.name;
                                    }

                                    var data7 = $scope.districtdsply.filter(function (arr) {
                                        return arr.id == member.districtId
                                    })[0];

                                    if (data7 != undefined) {
                                        member.districtname = data7.name;
                                    }
                                    var data8 = $scope.sitedsply.filter(function (arr) {
                                        return arr.id == member.siteId
                                    })[0];

                                    if (data8 != undefined) {
                                        member.sitename = data8.name;
                                    }

                                    var data9 = $scope.usersdisplay.filter(function (arr) {
                                        return arr.id == member.createdBy
                                    })[0];

                                    if (data9 != undefined) {
                                        member.createdByname = data9.username;
                                    }

                                    var data10 = $scope.usersdisplay.filter(function (arr) {
                                        return arr.id == member.lastModifiedBy
                                    })[0];

                                    if (data10 != undefined) {
                                        member.lastModifiedByname = data10.username;
                                    }

                                    var data11 = $scope.roledsply.filter(function (arr) {
                                        return arr.id == member.lastModifiedByRole
                                    })[0];

                                    if (data11 != undefined) {
                                        member.lastModifiedByRolename = data11.name;
                                    }

                                    var data12 = $scope.roledsply.filter(function (arr) {
                                        return arr.id == member.createdByRole
                                    })[0];

                                    if (data12 != undefined) {
                                        member.createdByRolename = data12.name;
                                    }

                                    member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                                    member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                                    if (member.createdDate == null) {
                                        member.createdDate = '';
                                    }

                                    if (member.amountSaved == null) {
                                        member.amountSaved = 0;
                                    }

                                    if (member.lastModifiedDate == null) {
                                        member.lastModifiedDate = '';
                                    }

                                    if (member.attended == true) {
                                        member.attended = 'Yes';
                                    } else {
                                        member.attended = 'No';
                                    }

                                    if (member.saved == true) {
                                        member.saved = 'Yes';
                                    } else {
                                        member.saved = 'No';
                                    }

                                    for (var ew = 0; ew < $scope.loanheadersdisplay1.length; ew++) {
                                        if (member.shgMeetingId == $scope.loanheadersdisplay1[ew].shgmeetingId) {
                                            if (member.memberId == $scope.loanheadersdisplay1[ew].memberId) {
                                                console.log("member.memberId",member.memberId);
                                                console.log("$scope.loanheadersdisplay1[ew].memberId",$scope.loanheadersdisplay1[ew].memberId);

                                                member.amountOfLoan = $scope.loanheadersdisplay1[ew].amountOfLoan;
                                                member.noOfInstallmentsToBePaid = $scope.loanheadersdisplay1[ew].noOfInstallmentsToBePaid;
                                                member.amountOfInstallmentsToBePaid = $scope.loanheadersdisplay1[ew].amountOfInstallmentsToBePaid;
                                                member.installmentRepaid = $scope.loanheadersdisplay1[ew].installmentRepaid;
                                                member.loanOutStanding = $scope.loanheadersdisplay1[ew].loanOutStanding;

                                                var datas2 = $scope.reasonforloansdsply.filter(function (arr) {
                                                    return arr.id == $scope.loanheadersdisplay1[ew].reason
                                                })[0];

                                                if (datas2 != undefined) {
                                                    member.reasonfortakingloan = datas2.name;
                                                }
                                                break;
                                            }
                                        }
                                    }

                                    for (var eh = 0; eh < $scope.healthydays.length; eh++) {
                                        if (member.shgMeetingId == $scope.healthydays[eh].shgMeetingId) {
                                            if (member.memberId == $scope.healthydays[eh].memberId) {

                                                member.daysUnwell = $scope.healthydays[eh].daysUnwell;

                                                var datas12 = $scope.symptomsdsply.filter(function (arr) {
                                                    return arr.id == $scope.healthydays[eh].primarySymptom
                                                })[0];

                                                if (datas12 != undefined) {
                                                    member.primarysymptom = datas12.name;
                                                }

                                                var datas22 = $scope.symptomsdsply.filter(function (arr) {
                                                    return arr.id == $scope.healthydays[eh].secondarySymptom
                                                })[0];

                                                if (datas22 != undefined) {
                                                    member.secondarysymptom = datas22.name;
                                                }

                                                var datas32 = $scope.illnessesdsply.filter(function (arr) {
                                                    return arr.id == $scope.healthydays[eh].illness
                                                })[0];

                                                if (datas32 != undefined) {
                                                    member.illness = datas32.name;
                                                }
                                                
                                                break;
                                            }
                                        }
                                    }
                                });
                            });
                        });
                    });
                });
            });
        });

        $scope.SHGMeetingsArray = [];

        $scope.exportData = function () {
            $scope.SHGMeetingsArray = [];
            $scope.valASCount = 0;

            if ($scope.shgmeetingtrailers.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.shgmeetingtrailers.length; c++) {
                    $scope.SHGMeetingsArray.push({
                        'Sr No': $scope.shgmeetingtrailers[c].index,
                        'COUNTRY': $scope.shgmeetingtrailers[c].countryname,
                        'STATE': $scope.shgmeetingtrailers[c].statename,
                        'DISTRICT': $scope.shgmeetingtrailers[c].districtname,
                        'SITE': $scope.shgmeetingtrailers[c].sitename,
                        'ASSIGNED TO': $scope.shgmeetingtrailers[c].assignedTo,
                        'MEMBER NAME': $scope.shgmeetingtrailers[c].memberName,
                        'INDIVIDUAL ID': $scope.shgmeetingtrailers[c].individualId,
                        'HH ID': $scope.shgmeetingtrailers[c].hhId,
                        'SHG NAME': $scope.shgmeetingtrailers[c].shgName,
                        'MEETING DATE': $scope.shgmeetingtrailers[c].createdDate,
                        'ATTENDED': $scope.shgmeetingtrailers[c].attended,
                        'SAVED': $scope.shgmeetingtrailers[c].saved,
                        'SAVING AMOUNT': $scope.shgmeetingtrailers[c].amountSaved,
                        'AMOUNT OF LOAN': $scope.shgmeetingtrailers[c].amountOfLoan,
                        'REASON FOR TAKING LOAN': $scope.shgmeetingtrailers[c].reasonfortakingloan,
                        'NO OF INSTALLMENT': $scope.shgmeetingtrailers[c].noOfInstallmentsToBePaid,
                        'AMOUNT OF INSTALLMENT': $scope.shgmeetingtrailers[c].amountOfInstallmentsToBePaid,
                        'INSTALLMENTS REPAID': $scope.shgmeetingtrailers[c].installmentRepaid,
                        'LOAN OUTSTANDING': $scope.shgmeetingtrailers[c].loanOutStanding,
                        'UNHEALTHY DAYS': $scope.shgmeetingtrailers[c].daysUnwell,
                        'PRIMARY SYMPTOM': $scope.shgmeetingtrailers[c].primarysymptom,
                        'SECONDARY SYMPTOM': $scope.shgmeetingtrailers[c].secondarysymptom,
                        'ILLNESS': $scope.shgmeetingtrailers[c].illness,
                        'CREATED BY': $scope.shgmeetingtrailers[c].createdByname,
                        'CREATED DATE': $scope.shgmeetingtrailers[c].createdDate,
                        'CREATED ROLE': $scope.shgmeetingtrailers[c].createdByRolename,
                        'LAST MODIFIED BY': $scope.shgmeetingtrailers[c].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.shgmeetingtrailers[c].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.shgmeetingtrailers[c].lastModifiedByRolename,
                        'DELETE FLAG': $scope.shgmeetingtrailers[c].deleteFlag
                    });

                    $scope.valASCount++;

                    if ($scope.shgmeetingtrailers.length == $scope.valASCount) {
                        alasql('SELECT * INTO XLSX("shgmeetings.xlsx",{headers:true}) FROM ?', [$scope.SHGMeetingsArray]);
                    }
                }
            }
        };
    });
