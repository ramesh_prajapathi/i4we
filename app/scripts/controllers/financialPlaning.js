'use strict';
angular.module('secondarySalesApp').controller('fPlanCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter, $modal) {
    /*********/
    $scope.calculatebuttonhide = false;
    $scope.savebuttonhide = true;
    $scope.cancelbuttonhide = false;
    $scope.editbuttonhide = true;
    $scope.productinstall = true;
    $scope.DisableFinancialGoal = false;
    $scope.DisableProductChosen = false;
    $scope.financialplaning = {
        memberid: $window.sessionStorage.fullName
        , lastmodifiedtime: new Date()
        , lastmodifiedby: $window.sessionStorage.UserEmployeeId
        , facility: $window.sessionStorage.coorgId
        , lastmodifiedbyrole: $window.sessionStorage.roleId
        , deleteflag: false
    };
    //$scope.timeinyears = Restangular.all('timeinyears').getList().$object;
    $scope.financialproducts = Restangular.all('financialproducts?filter[where][deleteflag]=false').getList().$object;
    $scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][id]=' + $window.sessionStorage.fullName).getList().then(function (part1) {
        $scope.beneficiaries = part1;
        $scope.memberName = part1[0].fullname;
        $scope.Age = part1[0].age;
    });
    Restangular.all('timeinyears').getList().then(function (response) {
        $scope.timeinyears = response[0];
        var str = response[0].name;
        var arr = str.split('-');
        var a1 = parseInt(arr[0]);
        var a2 = parseInt(arr[1]);
        $scope.newntimesinyears = [];
            for (var i = a1; i <= a2; i++) {
                $scope.newntimesinyears.push(i);
               // console.log(' $scope.newntimesinyears', $scope.newntimesinyears);
            }
      
    });
 
    Restangular.all('financialgoals').getList().then(function (glresponse) {
        $scope.financialgoals = glresponse;
        Restangular.all('financialplannings?filter[where][memberid]=' + $window.sessionStorage.fullName + '&filter[where][deleteflag]=false').getList().then(function (response) {
            $scope.financialplannings = response;
            angular.forEach($scope.financialplannings, function (member, index) {
                member.index = index + 1;
                for (var n = 0; n < $scope.financialgoals.length; n++) {
                    if (member.financialgoal == $scope.financialgoals[n].id) {
                        member.FinancialgoalName = $scope.financialgoals[n];
                        break;
                    }
                }
            });
        });
    });
    /*	$scope.getFGoal = function (id) {
			return Restangular.all('financialgoals?filter={"where":{"id":{"inq":[' + id + ']}}}').getList().$object;
		};

		console.log('$window.sessionStorage.fullName', $window.sessionStorage.fullName);
*/
    $scope.openAdd = function () {
        //console.log('data openAdd', data);
        $scope.modalINFO = $modal.open({
            animation: true
            , templateUrl: 'template/FinancialGoal.html'
            , scope: $scope
            , backdrop: 'static'
        });
    };
    $scope.validatestring = ''
    $scope.AlertMessage = '';
    $scope.CalculateButton = function () {
        console.log('Calculate');
        if ($scope.financialplaning.financialgoal == '' || $scope.financialplaning.financialgoal == null) {
            $scope.AlertMessage = $scope.selfinancialgoal; //'Please Select Financial Goal';
            $scope.modalInstance1 = $modal.open({
                animation: true
                , templateUrl: 'template/validation.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'sm'
            });
        }
        else if ($scope.financialplaning.timeinyears == '' || $scope.financialplaning.timeinyears == null) {
            $scope.AlertMessage = $scope.seltimeinyr;
            $scope.modalInstance1 = $modal.open({
                animation: true
                , templateUrl: 'template/validation.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'sm'
            });
        }
        else if ($scope.financialplaning.targetamount == '' || $scope.financialplaning.targetamount == null) {
            $scope.AlertMessage = $scope.entertargetamount;
            $scope.modalInstance1 = $modal.open({
                animation: true
                , templateUrl: 'template/validation.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'sm'
            });
        }
        else {
            $scope.CalculateDetails();
            $scope.modalInstance1 = $modal.open({
                animation: true
                , templateUrl: 'template/option.html'
                , scope: $scope
                , backdrop: 'static'
            });
        }
    };
    $scope.OkValidation = function () {
        $scope.modalInstance1.close();
    }
    $scope.Cancel = function () {
        //$scope.modalINFO.close();
        location.reload();
    }
    $scope.okOption = function (optId, inst) {
            $scope.financialplaning.productchosen = optId;
            $scope.financialplaning.installment = inst;
            $scope.modalInstance1.close();
            $scope.calculatebuttonhide = true;
            //$scope.cancelbuttonhide = false;
            $scope.productinstall = false;
            //console.log('data okOption', data);
            if ($scope.editoperation == true) {
                $scope.savebuttonhide = true;
                $scope.editbuttonhide = false;
            }
            else {
                $scope.savebuttonhide = false;
            }
        }
        /*$scope.TimeYear = function(yr){
			console.log('year',yr);
			$scope.TimeinYear = yr;
		}*/
    $scope.CalculateDetails = function () {
        //console.log('$scope.Age',$scope.Age);
        $scope.TotalOptions = [];
        Restangular.all('financialproducts?filter[where][deleteflag]=false').getList().then(function (fproduct) {
            //console.log('fproduct', fproduct);
            $scope.financialproducts = fproduct;
            angular.forEach($scope.financialproducts, function (member, index) {
                member.index = index + 1;
                $scope.target = $scope.financialplaning.targetamount;
                if (index == 0) {
                    $scope.aone = member.rateofinterest;
                    $scope.savings = Math.round($scope.target * (member.rateofinterest / 1200) / (Math.pow(((1 + member.rateofinterest / 1200)), ($scope.financialplaning.timeinyears * 12)) - 1));
                }
                else if (index == 1) {
                    $scope.rd = Math.round($scope.target * (member.rateofinterest / 1200) / (Math.pow(((1 + member.rateofinterest / 1200)), ($scope.financialplaning.timeinyears * 12)) - 1));
                }
                else if (index == 2) {
                    $scope.fd = Math.round($scope.target / Math.pow((1 + member.rateofinterest / 100), ($scope.financialplaning.timeinyears)));
                }
                else if (index == 3) {
                    $scope.insurance = Math.round($scope.target / ($scope.financialplaning.timeinyears * 12));
                }
                else if (index == 4) {
                    $scope.pension = Math.round($scope.target * (member.rateofinterest / 1200) / (Math.pow((1 + (member.rateofinterest / 1200)), ((60 - $scope.Age) * 12)) - 1));
                }
                else if (index == 5) {
                    $scope.nsc = Math.round($scope.target / Math.pow((1 + member.rateofinterest / 100), ($scope.financialplaning.timeinyears)));
                }
                else if (index == 6) {
                    $scope.ppf = Math.round($scope.target * (member.rateofinterest / 1200) / (Math.pow(((1 + member.rateofinterest / 1200)), ($scope.financialplaning.timeinyears * 12)) - 1));
                }
                else if (index == 7) {
                    $scope.investment = 0;
                    //console.log('investment', $scope.investment);
                }
            });
            $scope.optArray = [
                {
                    name: 'Savings'
                    , ammount: $scope.savings
                    , id: 1
  }
                , {
                    name: 'RD'
                    , ammount: $scope.rd
                    , id: 2
  }, {
                    name: 'FD'
                    , ammount: $scope.fd
                    , id: 3
  }, {
                    name: 'Insurance'
                    , ammount: $scope.insurance
                    , id: 4
  }, {
                    name: 'Pension'
                    , ammount: $scope.pension
                    , id: 5
  }, {
                    name: 'NSC'
                    , ammount: $scope.nsc
                    , id: 6
  }, {
                    name: 'PPF'
                    , ammount: $scope.ppf
                    , id: 7
  }, {
                    name: 'Investment'
                    , ammount: $scope.investment
                    , id: 8
  }

  ]
        });
    }
    $scope.Save = function () {
        //console.log('financialplaning.financialgoal', $scope.financialplaning.financialgoal);
        $scope.Currfinancialgoals = Restangular.all('financialgoals?filter[where][id]=' + $scope.financialplaning.financialgoal).getList().$object;
        $scope.modalINFO.close();
        Restangular.all('financialplannings').post($scope.financialplaning).then(function (response) {
            console.log('financialplannings', response);
            location.reload();
        });
    }
    $scope.openEdit = function () {
        $scope.modalINFO = $modal.open({
            animation: true
            , templateUrl: 'template/FinancialGoal.html'
            , scope: $scope
            , backdrop: 'static'
        });
    }
    $scope.editoperation = false;
    $scope.EditFinancialPlaning = function (FpId, data) {
        $scope.productinstall = false;
        $scope.DisableFinancialGoal = true;
        $scope.editoperation = true;
        $scope.DisableProductChosen = true;
        console.log('FpId', FpId);
        //console.log('financial', data);
        //$scope.financialgoals = Restangular.all('financialgoals').getList().$object;
        /*Restangular.one('financialplannings', FpId).get().then(function (fplan) {
				$scope.original = fplan;
				$scope.financialplanning = Restangular.copy($scope.original);
				console.log('fplan', fplan);*/
        //$scope.financialgoals = Restangular.all('financialgoals').getList().then(function (financialgoals) {
        //	$scope.financialgoals = financialgoals;
        //$scope.timeinyearss = Restangular.all('timeinyearss').getList().then(function (timeinyearss) {
        //$scope.timeinyearss = timeinyearss;
        //});
        //});
        //});
        $scope.financialplaning = data;
        $scope.openEdit();
        //$scope.Update(FpId);			
    }
    $scope.Update = function () {
        //$scope.productinstall = false;
        //console.log('$scope.financialplaning', $scope.financialplaning);
        //console.log('$scope.financialplaning', $scope.financialplaning.id);
        Restangular.one('financialplannings', $scope.financialplaning.id).customPUT($scope.financialplaning).then(function (response) {
            console.log('update financialplaning', response);
            location.reload();
        });
        //$scope.modalINFO.close();
    }
    $scope.removeFP = function (DelId) {
            //var item = $scope.Currfinancialgoals[index];
            //$scope.Currfinancialgoals.splice(index, 1);
            $scope.item = {
                deleteflag: true
            }
            Restangular.one('financialplannings', DelId).customPUT($scope.item).then(function (response) {
                console.log('Delete financialplaning', response);
                location.reload();
            });
        }
        //Datepicker settings start
        //$scope.datetimedisplay = $filter('date')(new Date(), 'y-MM-dd');
    $scope.financialplaning.datetime = new Date(); //$filter('date')(new Date(), 'y-MM-dd');
    $scope.today = function () {};
    $scope.today();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.dtmin = new Date();
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.open = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.opened = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy'
        , 'starting-day': 1
    };
    $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end
    $scope.stakeholderdataModal = false;
    $scope.SaveButton = function () {
        $scope.modalTitleShow1 = $scope.finhascreated;//'Financial planing Has Been Created';
        $scope.modalTitleShow = 'Thank You';
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        window.location = '/';
    };
    /****************************** Language **********************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.member = langResponse.member;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.okbutton = langResponse.ok;
        $scope.printdate = langResponse.date;
        $scope.printAdd = langResponse.addbutton;
        $scope.printSave = langResponse.savebutton;
        $scope.printEdit = langResponse.update;
        $scope.financialplanning = langResponse.financialplanning;
        $scope.Financial_Goal = langResponse.financialgoal;
        $scope.ACTION = langResponse.action;
        $scope.FinancialGoalDetails = langResponse.financialgoaldetails;
        $scope.Timeinyears = langResponse.timeinyear;
        $scope.Target_Amount = langResponse.targetamount;
        $scope.Product_Chosen = langResponse.productchosen;
        $scope.Installment = langResponse.installment;
        $scope.Calculate = langResponse.calculate;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
        $scope.modalTitle = langResponse.createdetails;
        $scope.finhascreated = langResponse.finhascreated;
        $scope.selfinancialgoal = langResponse.selfinancialgoal;
        $scope.seltimeinyr = langResponse.seltimeinyr;
        $scope.entertargetamount = langResponse.entertargetamount;
    });
   
});