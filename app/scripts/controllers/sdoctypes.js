'use strict';

angular.module('secondarySalesApp')
	.controller('SDocTypCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/sdocumenttypes/create' || $location.path() === '/sdocumenttypes/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/sdocumenttypes/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/sdocumenttypes/create' || $location.path() === '/sdocumenttypes/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/sdocumenttypes/create' || $location.path() === '/sdocumenttypes/' + $routeParams.id;
			return visible;
		};

		$scope.documenttype = {
			multipleflag: true,
			deleteflag: false
		}

		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			//console.log('$scope.countryId From Landing', $scope.pageSize);
		}

		if ($window.sessionStorage.prviousLocation != "partials/sdoctypes") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} 
	
		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};
	
		
/**********************/
		//  $scope.documenttypes = Restangular.all('documenttypes').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'Document Type has been Updated!';
			Restangular.one('documenttypes', $routeParams.id).get().then(function (documenttype) {
				$scope.original = documenttype;
				$scope.documenttype = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Document Type has been Created!';
		}
		$scope.Search = $scope.name;

		/**************************** INDEX *******************************************/
		$scope.zn = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.documenttypes = zn;
			angular.forEach($scope.documenttypes, function (member, index) {
				member.index = index + 1;
			});
		});

		/******************************* SAVE *******************************************/
		$scope.validatestring = '';
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.documenttype.name == '' || $scope.documenttype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.documenttype.hnname == '' || $scope.documenttype.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.documenttype.knname == '' || $scope.documenttype.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.documenttype.taname == '' || $scope.documenttype.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.documenttype.tename == '' || $scope.documenttype.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.documenttype.mrname == '' || $scope.documenttype.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.documenttypes.post($scope.documenttype).then(function () {
					console.log('document Type Saved');
					window.location = '/sdocumenttypes';
				});
			};
		};
		/***************************** UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.documenttype.name == '' || $scope.documenttype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.documenttype.hnname == '' || $scope.documenttype.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.documenttype.knname == '' || $scope.documenttype.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.documenttype.taname == '' || $scope.documenttype.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.documenttype.tename == '' || $scope.documenttype.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.documenttype.mrname == '' || $scope.documenttype.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter documenttype in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.documenttypes.customPUT($scope.documenttype).then(function () {
					console.log('documenttype Saved');
					window.location = '/sdocumenttypes';
				});
			};
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/************************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('documenttypes/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}
	});
