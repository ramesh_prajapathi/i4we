'use strict';
angular.module('secondarySalesApp').controller('EditGroupMeetingsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter, $modal) {
    /*********/
    if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        window.location = "/";
    }
    $scope.addedtodos = [];
    $scope.newtodo = {};
    //$scope.event = {};
    $scope.showForm = function () {
        var visible = $location.path() === '/groupmeetings/create' || $location.path() === '/groupmeetings/' + $routeParams.id;
        return visible;
    };
    $scope.isCreateView = function () {
        if ($scope.showForm()) {
            var visible = $location.path() === '/groupmeetings/create';
            return visible;
        }
    };
    $scope.hideCreateButton = function () {
        var visible = $location.path() === '/groupmeetings/create' || $location.path() === '/groupmeetings/' + $routeParams.id;
        return visible;
    };
    $scope.hideSearchFilter = function () {
        var visible = $location.path() === '/groupmeetings/create' || $location.path() === '/groupmeetings/' + $routeParams.id;
        return visible;
    };
    $scope.DisableFollowup = true;
    $scope.auditlog = {
        modifiedbyroleid: $window.sessionStorage.roleId
        , modifiedby: $window.sessionStorage.UserEmployeeId
        , lastmodifiedtime: new Date()
        , entityroleid: 37
        , state: $window.sessionStorage.zoneId
        , district: $window.sessionStorage.salesAreaId
        , facility: $window.sessionStorage.coorgId
        , lastmodifiedby: $window.sessionStorage.UserEmployeeId
    };
    $scope.groupmeeting = {
        attendees: []
        , follow: []
        , state: $window.sessionStorage.zoneId
        , district: $window.sessionStorage.salesAreaId
        , facility: $window.sessionStorage.coorgId
        , lastmodifiedby: $window.sessionStorage.UserEmployeeId
        , lastmodifiedtime: new Date()
        , deleteflag: false
    };
    $scope.UserLanguage = $window.sessionStorage.language;
    /****************************************** Not In Use **********************************
		
		$scope.followupgroups = Restangular.all('followupgroups').getList().$object;
		$scope.submitgroupmeetings = Restangular.all('groupmeetings').getList().$object;
		//$scope.fieldworkers = Restangular.all('fieldworkers').getList().$object;
		$scope.comembers = Restangular.all('comembers').getList().$object;
		$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;

	
	
		$scope.getFW = function (partnerId) {
			return Restangular.one('fieldworkers', partnerId).get().$object;
		};

		$scope.getCO = function (partnerId) {
			return Restangular.one('fieldworkers', partnerId).get().$object;
		};

		$scope.getGroupMeetingTopics = function (partnerId) {
			return Restangular.all('groupmeetingtopics?filter={"where":{"id":{"inq":[' + partnerId + ']}}}').getList().$object;
		};

		$scope.getPilar = function (id) {
			return Restangular.all('pillars?filter={"where":{"id":{"inq":[' + id + ']}}}').getList().$object;
		};
		
			$scope.groupcreate = false;
		var booleanValue = true;
		$scope.organisedbychange = true;
		$scope.Change = function () {
			if (booleanValue == false) {
				$scope.organisedbychange = true;
				$scope.groupcreate = true;
			} else {
				$scope.organisedbychange = false;
				$scope.groupcreate = true;
			}
		};


	/************************************************* INDEX *******************************************
		$scope.grp = Restangular.all('groupmeetings?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (grp) {
			$scope.groupmeetings = grp;
			angular.forEach($scope.groupmeetings, function (member, index) {
				member.index = index + 1;
				member.pillars = $scope.getPilar(member.pillarid);
				member.meetingtopics = $scope.getGroupMeetingTopics(member.event);
			});
		});


//$scope.follow = {};
		$scope.submittodos = Restangular.all('todos').getList().$object;
	//$scope.groupmeetings = Restangular.all('groupmeetings').getList().$object;

	/*************************************************************************************

		$scope.zipmasters = Restangular.all('pillars').getList().then(function (zipmaster) {
			$scope.zipmasters = zipmaster;
			angular.forEach($scope.zipmasters, function (member, index) {
				member.total = null;
				member.loader = null;
			});
		});

		$scope.checkbox = [];
		$scope.eventcheckbox = [];
		$scope.mobilenumbers = [];
	
		$scope.GetMobNumbers = function (index, zipcode, event) {
			//console.log($scope.zipmasters[index]);
			if ($scope.zipmasters[index].enabled == true) {
				$scope.zipmasters[index].loader = 'images/loginloader.gif';
				//   console.log($scope.checkbox[index], zipcode);
				$scope.surveyquestions = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + $scope.zipmasters[index].id).getList().then(function (surveyquestions) {
					$scope.prospects = surveyquestions;
					$scope.zipmasters[index].total = surveyquestions.length;
					$scope.zipmasters[index].loader = null;
					for (var i = 0; i < surveyquestions.length; i++) {
						surveyquestions[i].enabled = false;
						$scope.mobilenumbers.push(surveyquestions[i]);
					};
				});
			} else {
				$scope.zipmasters[index].total = null;
				$scope.zipmasters[index].loader = 'images/loginloader.gif';
				$scope.surveyquestions = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + $scope.zipmasters[index].id).getList().then(function (surveyquestions) {
					$scope.zipmasters[index].loader = null;
					for (var i = 0; i < surveyquestions.length; i++) {
						$scope.mobilenumbers.splice($scope.mobilenumbers.indexOf(surveyquestions[i].name), 1);
					};
				});
			}
		}



		$scope.Organisedcomembers = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + $window.sessionStorage.UserEmployeeId + ',41]}}}').getList().$object;
		console.log('$scope.Organisedcomembers', $scope.Organisedcomembers);

	/*********************************************************************/
    //$scope.showfollowupModal = !$scope.showfollowupModal;
    /***********************************************************************************************/
    $scope.documenthide = true;
    $scope.documenthide = true;
    $scope.$watch('groupmeeting.organisedby', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        }
        else {
            if (newValue == 41) {
                $scope.documenthide = false;
            }
            else {
                $scope.documenthide = true;
            }
        }
    });
    /****************************************************************************************/
    $scope.$watch('groupmeeting.attendees', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        }
        else {
            $scope.attendeestodo = newValue;
            if (newValue.length > oldValue.length) {
                // something was added
                var Array1 = newValue;
                var Array2 = oldValue;
                for (var i = 0; i < Array2.length; i++) {
                    var arrlen = Array1.length;
                    for (var j = 0; j < arrlen; j++) {
                        if (Array2[i] == Array1[j]) {
                            Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                        }
                    }
                }
                for (var i = 0; i < $scope.partners.length; i++) {
                    if ($scope.partners[i].id == Array1[0]) {
                        $scope.partners[i].enabled = true;
                    }
                }
                // do stuff
            }
            else if (newValue.length < oldValue.length) {
                // something was removed
                var Array1 = oldValue;
                var Array2 = newValue;
                for (var i = 0; i < Array2.length; i++) {
                    var arrlen = Array1.length;
                    for (var j = 0; j < arrlen; j++) {
                        if (Array2[i] == Array1[j]) {
                            Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                        }
                    }
                }
                for (var i = 0; i < $scope.partners.length; i++) {
                    if ($scope.partners[i].id == Array1[0]) {
                        $scope.partners[i].enabled = false;
                    }
                }
                //$scope.partners[Array1[0]].enabled = false;
            }
        }
    });
    /************************************* Current& Future Date**********************************/
    var today = new Date();
    $scope.groupmeeting.datetime = new Date();
    $scope.newtodo.datetime = new Date();
    //$scope.todo.status = 1;
    var newdate = new Date();
    $scope.today = function () {};
    $scope.today();
    $scope.dtmax = new Date();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.opendob = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerdob' + index).focus();
        });
        $scope.picker.dobopened = true;
        //$scope.follow.followupopened = true;
    };
    $scope.open = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerfollowup' + index).focus();
        });
        $scope.follow.followupopened = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy'
        , 'starting-day': 1
    };
    $scope.monthOptions = {
        formatYear: 'yyyy'
        , startingDay: 1
        , minMode: 'month'
    };
    $scope.mode = 'month';
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.monthformat = $scope.monthformats[0];
    //Datepicker settings end
    $scope.searchgroupmeeting = $scope.name;
    /************************************************* MOdal ******************************/
    $scope.showschemesModal = false;
    $scope.toggleschemesModal = function () {
        $scope.SaveScheme = function () {
            $scope.newArray = [];
            $scope.groupmeeting.attendees = [];
            for (var i = 0; i < $scope.partners.length; i++) {
                if ($scope.partners[i].enabled == true) {
                    $scope.newArray.push($scope.partners[i].id);
                }
            };
            $scope.attendeestodo = $scope.groupmeeting.attendees;
            $scope.groupmeeting.attendees = $scope.newArray;
            $scope.showschemesModal = !$scope.showschemesModal;
        };
        $scope.showschemesModal = !$scope.showschemesModal;
    };
    $scope.CancelScheme = function () {
        $scope.showschemesModal = !$scope.showschemesModal;
    };
    /*********************************************************************************/
    $scope.newtodo.status = 1;
    $scope.newtodo.todotype = 32;
    $scope.newtodo.state = $window.sessionStorage.zoneId;
    $scope.newtodo.district = $window.sessionStorage.salesAreaId;
    $scope.newtodo.facility = $window.sessionStorage.coorgId;
    $scope.newtodo.facilityId = $scope.getfacilityId;
    $scope.newtodo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
    $scope.newtodo.lastmodifiedtime = new Date();
    $scope.showfollowupModal = false;
    $scope.CancelFollow = function () {
        $scope.showfollowupModal = !$scope.showfollowupModal;
    };
    $scope.CancelModal = function () {
            $scope.showschemesModal = !$scope.showschemesModal;
        }
        /********************************************** SAVE *******************************************/
    $scope.todocount = 0;
    $scope.todocount1 = 0;
    $scope.SaveToDo = function () {
        $scope.addedtodocount = 0;
        $scope.addedtodocount1 = 0;
        for (var i = 0; i < $scope.addedtodos.length; i++) {
            $scope.addedtodos[i].beneficiaryid = $scope.attendeestodo[$scope.todocount];
            $scope.addedtodos[i].purpose = $scope.attendeespurpose[$scope.todocount1];
            $scope.submittodos.post($scope.addedtodos[i]).then(function (resp) {
                $scope.addedtodocount++;
                $scope.addedtodocount1++;
                if ($scope.addedtodocount >= $scope.addedtodos.length && $scope.addedtodocount1 >= $scope.addedtodos.length) {
                    $scope.todocount++;
                    $scope.todocount1++;
                    if ($scope.todocount < $scope.attendeestodo.length && $scope.todocount1 < $scope.attendeespurpose.length) {
                        $scope.SaveToDo();
                    }
                    else if ($scope.SaveToDo.length <= $scope.addedtodos.length) {
                        window.location = '/groupmeetings';
                    }
                }
            });
        }
    };
    $scope.groupmeetingdataModal = false;
    /*********************************************************************************************/
    $scope.validatestring = '';
    $scope.Updategroupmeeting = function () {
        //$scope.groupmeetingdataModal = !$scope.groupmeetingdataModal;
        
        $scope.groupmeeting.attendeeslist = null;
        $scope.groupmeeting.followup = null;
        $scope.groupmeeting.pillarid = null;
        $scope.groupmeeting.topicsdiscussed = null;
        var checkedValue = null;
        var inputElements1 = document.getElementsByClassName('messageCheckbox');
        for (var i = 0; inputElements1[i]; ++i) {
            if (inputElements1[i].checked) {
                if (checkedValue == null) {
                    checkedValue = inputElements1[i].value;
                }
                else {
                    checkedValue = checkedValue + ',' + inputElements1[i].value;
                }
            }
        };
        $scope.groupmeeting.pillarid = checkedValue;
        var eventcheckedValue = null;
        var inputElements2 = document.getElementsByClassName('eventCheckbox');
        for (var i = 0; inputElements2[i]; ++i) {
            if (inputElements2[i].checked) {
                if (eventcheckedValue == null) {
                    eventcheckedValue = inputElements2[i].value;
                }
                else {
                    eventcheckedValue = eventcheckedValue + ',' + inputElements2[i].value;
                }
            }
        }
        $scope.groupmeeting.topicsdiscussed = eventcheckedValue;
        var checkedValue1 = null;
        var inputElements3 = document.getElementsByClassName('attendcheckbox');
        for (var i = 0; inputElements3[i]; ++i) {
            if (inputElements3[i].checked) {
                if (checkedValue1 == null) {
                    checkedValue1 = inputElements3[i].value;
                }
                else {
                    checkedValue1 = checkedValue1 + ',' + inputElements3[i].value;
                }
            }
        }
        $scope.groupmeeting.attendeeslist = checkedValue1;
        if ($scope.groupmeeting.follow != 'None') {
            for (var i = 0; i < $scope.groupmeeting.follow.length; i++) {
                if (i == 0) {
                    $scope.groupmeeting.followup = $scope.groupmeeting.follow[i];
                }
                else {
                    $scope.groupmeeting.followup = $scope.groupmeeting.followup + ',' + $scope.groupmeeting.follow[i];
                }
            }
        }
        else {
            $scope.groupmeeting.followup = null;
        }
        if ($scope.groupmeeting.datetime == '' || $scope.groupmeeting.datetime == null) {
            $scope.validatestring = $scope.validatestring + $scope.seldate;
        }
        else if ($scope.groupmeeting.name == '' || $scope.groupmeeting.name == null) {
            $scope.validatestring = $scope.validatestring + $scope.entername;
        }
        else if (checkedValue === null) {
            $scope.validatestring = $scope.validatestring + $scope.selpillar;
        }
        else if (eventcheckedValue === null) {
            $scope.validatestring = $scope.validatestring + $scope.seltopic;
        }
        else if ($scope.groupmeeting.organisedby == '' || $scope.groupmeeting.organisedby == null) {
            $scope.validatestring = $scope.validatestring + $scope.selorgby;
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
           // $scope.groupmeetingdataModal = !$scope.groupmeetingdataModal;
        }
        else {
             $scope.openOneAlert();
            //$scope.submitgroupmeetings.customPUT($scope.groupmeeting).then(function (resp) {
            Restangular.one('groupmeetings', $routeParams.id).customPUT($scope.groupmeeting).then(function (resp) {
                $scope.updatemember();
                $scope.auditlog.entityid = resp.id;
                var respdate = $filter('date')(resp.datetime, 'dd-MMMM-yyyy');
                $scope.auditlog.description = 'Group Meeting Updated With Following Details:' + 'Name-' + resp.name + ' , ' + 'MemberId-' + resp.attendees + ' , ' + 'PillarId-' + resp.pillarid + ' , ' + 'TopicId-' + resp.topicsdiscussed + ' , ' + 'Organized By -' + resp.organisedby + ' , ' + 'Follow Up -' + resp.follow + ' , ' + 'Date -' + respdate;
                //$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                    console.log('responseaudit', responseaudit);
                    console.log('groupmeeting Saved', resp);
                    
                    window.location = '/groupmeetings';
                    console.log('$scope.SaveToDo');
                });
            });
        };
    };
    $scope.updatemember = function () {
        $scope.memberupdate = {
            lastmeetingdate: new Date()
        }
        for (var m = 0; m < $scope.groupmeeting.attendees.length; m++) {
            console.log('memlist[m]', $scope.groupmeeting.attendees[m]);
            Restangular.all('beneficiaries/' + $scope.groupmeeting.attendees[m]).customPUT($scope.memberupdate).then(function (responsemember) {
                console.log('member update', responsemember);
            });
        }
    }
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    $scope.followupgroups = Restangular.all('followupgroups').getList().$object;
    $scope.GetMobNumbers = function (index, zipcode, event) {
        if ($scope.zipmasters[index].enabled == true) {
            $scope.zipmasters[index].loader = 'images/loginloader.gif';
            $scope.surveyquestions = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + $scope.zipmasters[index].id).getList().then(function (surveyquestions) {
                $scope.prospects = surveyquestions;
                $scope.zipmasters[index].total = surveyquestions.length;
                $scope.zipmasters[index].loader = null;
                for (var i = 0; i < surveyquestions.length; i++) {
                    surveyquestions[i].enabled = false;
                    $scope.mobilenumbers.push(surveyquestions[i]);
                };
            });
        }
        else {
            $scope.zipmasters[index].total = null;
            $scope.zipmasters[index].loader = 'images/loginloader.gif';
            $scope.surveyquestions = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + $scope.zipmasters[index].id).getList().then(function (surveyquestions) {
                $scope.zipmasters[index].loader = null;
                for (var i = 0; i < surveyquestions.length; i++) {
                    $scope.mobilenumbers.splice($scope.mobilenumbers.indexOf(surveyquestions[i].name), 1);
                };
            });
        }
    }
    $scope.checkbox = [];
    $scope.eventcheckbox = [];
    $scope.mobilenumbers = [];
    if ($routeParams.id) {
        Restangular.one('groupmeetings', $routeParams.id).get().then(function (grp) {
            $scope.original = grp;
            $scope.groupmeeting = Restangular.copy($scope.original);
            if (grp.attendeeslist != null) {
                $scope.groupmeeting.attendees = grp.attendeeslist.split(",");
            }
            else {
                $scope.groupmeeting.attendees = [];
            }
            if (grp.followup != null) {
                $scope.groupmeeting.follow = grp.followup.split(",");
            }
            else {
                $scope.groupmeeting.follow = [];
            }
            if (grp.pillarid != null) {
                $scope.selectedpillars = grp.pillarid.split(",");
            }
            else {
                $scope.selectedpillars = [];
                $scope.modalInstanceLoad.close();
            }
            if (grp.topicsdiscussed != null) {
                $scope.selectedtopicsdiscussed = grp.topicsdiscussed.split(",");
            }
            else {
                $scope.selectedtopicsdiscussed = [];
                $scope.modalInstanceLoad.close();
            }
            if (grp.attendeeslist != null) {
                $scope.selectedattendeeslist = grp.attendeeslist.split(",");
            }
            else {
                $scope.selectedattendeeslist = [];
            }
            $scope.zipmasters = Restangular.all('pillars').getList().then(function (zipmaster) {
                $scope.zipmasters = zipmaster;
                angular.forEach($scope.zipmasters, function (member, index) {
                    member.total = null;
                    member.loader = null;
                    if ($scope.selectedpillars.indexOf(member.id.toString()) == -1) {
                        member.enabled = false;
                    }
                    else {
                        member.enabled = true;
                        member.loader = 'images/loginloader.gif';
                        $scope.surveyquestions = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + member.id).getList().then(function (surveyquestions) {
                            $scope.prospects = surveyquestions;
                            for (var i = 0; i < surveyquestions.length; i++) {
                                surveyquestions[i].enabled = false;
                                if ($scope.selectedtopicsdiscussed.indexOf(surveyquestions[i].id.toString()) == -1) {
                                    surveyquestions[i].enabled = false;
                                }
                                else {
                                    surveyquestions[i].enabled = true;
                                }
                                $scope.mobilenumbers.push(surveyquestions[i]);
                                member.loader = null;
                                $scope.modalInstanceLoad.close();
                            };
                        });
                    }
                });
            });
            /**************************************** Member *******************************************/
            if ($window.sessionStorage.roleId == 5) {
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
                    $scope.groupmeeting.facilityId = comember.id;
                    $scope.auditlog.facilityId = comember.id;
                    Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fwrResponse) {
                        $scope.fieldworkers = fwrResponse;
                    });
                });
                $scope.Organisedcomembers = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + $window.sessionStorage.UserEmployeeId + ',41]}}}').getList().$object;
                $scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (part1) {
                    $scope.partners = part1;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;
                        if ($scope.selectedattendeeslist.indexOf(member.id.toString()) == -1) {
                            member.enabled = false;
                        }
                        else {
                            member.enabled = true;
                        }
                    });
                });
            }
            else {
                Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                        $scope.partners = part2;
                        angular.forEach($scope.partners, function (member, index) {
                            member.index = index + 1;
                            if ($scope.selectedattendeeslist.indexOf(member.id.toString()) == -1) {
                                member.enabled = false;
                            }
                            else {
                                member.enabled = true;
                            }
                        });
                    });
                    Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                        $scope.groupmeeting.facilityId = comember.id;
                        $scope.auditlog.facilityId = comember.id;
                    });
                    $scope.Organisedcomembers2 = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + fw.lastmodifiedby + ',41]}}}').getList().then(function (responseorg) {
                        $scope.Organisedcomembers = responseorg;
                    });
                });
            }
        });
    }
   
    /******************************* Language ********************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        console.log('langualge',langResponse.thankyou);
         console.log('Thank',$scope.thankyou)
        $scope.mandatoryfield1 = langResponse.mandatoryfield;
        $scope.date = langResponse.date;
        $scope.printname = langResponse.name;
        $scope.action = langResponse.action;
        $scope.searchmember = langResponse.searchmember;
        $scope.addnew = langResponse.addnew;
        $scope.show = langResponse.show;
        $scope.entry = langResponse.entry;
        $scope.headergroupmeeting = langResponse.headergroupmeeting;
        $scope.memberattendmeeting = langResponse.memberattendmeeting;
        $scope.organizedby = langResponse.organizedby;
        $scope.pillar = langResponse.pillar;
        $scope.searchfor = langResponse.searchfor;
        $scope.topics = langResponse.topics;
        $scope.followuprequired = langResponse.followuprequired;
        $scope.membername = langResponse.membername;
        $scope.select = langResponse.select;
        $scope.printstatus = langResponse.status;
        $scope.followupdate = langResponse.followupdate;
        $scope.topicdiscussed = langResponse.topicdiscussed;
        $scope.dateofclosure = langResponse.dateofclosure;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.savebutton = langResponse.savebutton;
        $scope.printothers = langResponse.others;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
        $scope.thankyou = langResponse.thankyou;
        //$scope.groupmhasupdated = langResponse.groupmhasupdated;
        $scope.seldate = langResponse.seldate;
        $scope.entername = langResponse.entername;
        $scope.selpillar = langResponse.selpillar;
        $scope.seltopic = langResponse.seltopic;
        $scope.selorgby = langResponse.selorgby;
        $scope.message = langResponse.groupmhasupdated; //'Group Meeting has been updated!';
        $scope.modalTitle = langResponse.thankyou; //'Thank You';
      
    });
    
       $scope.openOneAlert = function () {
         console.log('Me Calling')
         $scope.modalOneAlert = $modal.open({
             animation: true
             , templateUrl: 'template/AlertModal.html'
             , scope: $scope
             , backdrop: 'static'
             , keyboard: false
             , size: 'sm'
             , windowClass: 'modal-sucess '
         });
     };
    //  console.log('$scope.modalTitle',$scope.seltopic);
    if ($window.sessionStorage.language == 1) {
        $scope.searchmemberTitle = 'Search Member';
        $scope.followupmemberTitle = 'Follow Up Member';
    }
    else if ($window.sessionStorage.language == 2) {
        $scope.searchmemberTitle = 'मेम्बर खोजें';
        $scope.followupmemberTitle = 'ऊपर का सदस्य का पालन करें';
    }
    else if ($window.sessionStorage.language == 3) {
        $scope.searchmemberTitle = 'ಮೆಂಬರ್ ರನ್ನು ಶೋಧಿಸಿ';
        $scope.followupmemberTitle = 'ಮೆಂಬರ್  ಅನುಸರಣ';
    }
    else if ($window.sessionStorage.language == 4) {
        $scope.searchmemberTitle = 'மெம்பர் தேடல';
        $scope.followupmemberTitle = 'மெம்பர் பின்தொடர்';
    }
    else if ($window.sessionStorage.language == 5) {
        $scope.searchmemberTitle = 'శోధించబడుతున్న మెంబర';
        $scope.followupmemberTitle = 'సభ్యుని అనుసరించండ';
    }
    else if ($window.sessionStorage.language == 6) {
        $scope.searchmemberTitle = 'सदस्य शोधा';
        $scope.followupmemberTitle = 'प्रतिनिधी सदस्य अनुसरण करा';
    }
});