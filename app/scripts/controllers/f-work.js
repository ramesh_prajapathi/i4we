'use strict';

angular.module('secondarySalesApp')
    .controller('FWCtrl', function ($scope, Restangular, $route, $window, $modal, $http, $filter) {
        /**************************/

        $scope.HfLanguage = {};

        Restangular.one('hflanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.HfLanguage = langResponse[0];
        });

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined || $window.sessionStorage.sales_zoneId == null || $window.sessionStorage.sales_zoneId == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.sales_zoneId == null
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/f-work-form") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*****************************************************************************/

        $scope.auditlog = {
            modifiedbyroleid: $window.sessionStorage.roleId,
            modifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            entityroleid: 51,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId
        };

        $scope.toggleLoading = function () {
            $scope.modalInstanceLoad = $modal.open({
                animation: true,
                templateUrl: 'template/LodingModal.html',
                scope: $scope,
                backdrop: 'static',
                size: 'sm',
                keyboard: false

            });
        };
    
    $scope.someFocusVariable = true;
        $scope.FocusMe = true;
    
    $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;
    
    $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;

        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;

        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;

        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;
    
    $scope.roledsply = Restangular.all('roles').getList().$object;


        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
            $scope.auditlog.facilityId = comember.id;
        });


        $scope.getassignedSites = function (siteids) {
            if (siteids != null || siteids != '' || siteids != 'undefined') {
                return Restangular.all('distribution-routes?filter={"where":{"id":{"inq":[' + siteids + ']}}}').getList().$object;
            }
        };

        $scope.partners = [];

        Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[' + 3 + ']}}]}}').getList().then(function (zn) {
            $scope.partners = zn;
            //  console.log($scope.partners);

            Restangular.all('sites').getList().then(function (sits) {

                $scope.sites = sits;

                $scope.modalInstanceLoad.close();

                angular.forEach($scope.partners, function (member, index) {
                    member.index = index + 1;

                    member.arraysitebelongs = member.siteId.split(",");
                    member.sitename = "";

                    for (var i = 0; i < member.arraysitebelongs.length; i++) {
                        for (var j = 0; j < $scope.sites.length; j++) {
                            if ($scope.sites[j].id == member.arraysitebelongs[i]) {
                                if (member.sitename == "") {
                                    member.sitename = $scope.sites[j].name;
                                } else {
                                    member.sitename = member.sitename + ", " + $scope.sites[j].name;
                                }
                                break;
                            }
                        }
                    }

                    var data13 = $scope.countrydisplay.filter(function (arr) {
                        return arr.id == member.countryId
                    })[0];

                    if (data13 != undefined) {
                        member.countryname = data13.name;
                    }

                    var data14 = $scope.statedisplay.filter(function (arr) {
                        return arr.id == member.stateId
                    })[0];

                    if (data14 != undefined) {
                        member.statename = data14.name;
                    }

                    var data15 = $scope.districtdsply.filter(function (arr) {
                        return arr.id == member.districtId
                    })[0];

                    if (data15 != undefined) {
                        member.districtname = data15.name;
                    }

                    var data16 = $scope.sitedsply.filter(function (arr) {
                        return arr.id == member.siteId
                    })[0];

                    if (data16 != undefined) {
                        member.sitename = data16.name;
                    }

                    var data17 = $scope.usersdisplay.filter(function (arr) {
                        return arr.id == member.associatedHF
                    })[0];

                    if (data17 != undefined) {
                        member.assignedTo = data17.name;
                    }

                    var data18 = $scope.usersdisplay.filter(function (arr) {
                        return arr.id == member.lastModifiedBy
                    })[0];

                    if (data18 != undefined) {
                        member.lastModifiedByname = data18.username;
                    }

                    var data19 = $scope.roledsply.filter(function (arr) {
                        return arr.id == member.lastModifiedByRole
                    })[0];

                    if (data19 != undefined) {
                        member.lastModifiedByRolename = data19.name;
                    }
                    var data20 = $scope.roledsply.filter(function (arr) {
                        return arr.id == member.createdByRole
                    })[0];

                    if (data20 != undefined) {
                        member.createdByRolename = data20.name;
                    }

                    member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                    member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');
                    member.getdatatime = $filter('date')(member.syncintime, 'dd-MMM-yyyy / hh:mm:ss a');
                    member.pushdatatime = $filter('date')(member.syncouttime, 'dd-MMM-yyyy / hh:mm:ss a');
                    
                    if (member.getdatatime == null) {
                        member.getdatatime = '';
                    }
                    
                    if (member.pushdatatime == null) {
                        member.pushdatatime = '';
                    }

                    $scope.TotalData = [];
                    $scope.TotalData.push(member);

                });
            });
        });

        /**************************/

        /**************************Export data to excel sheet ***************/

        $scope.DisableExport = false;
        $scope.valptrCount = 0;

        $scope.exportData = function () {
            $scope.hfArray = [];
            $scope.valptrCount = 0;
            if ($scope.partners.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.partners.length; c++) {
                    $scope.hfArray.push({
                        'Sr No': $scope.partners[c].index,
                        'COUNTRY': $scope.partners[c].countryname,
                        'STATE': $scope.partners[c].statename,
                        'DISTRICT': $scope.partners[c].districtname,
                        'SITE': $scope.partners[c].sitename,
                        'USER NAME': $scope.partners[c].username,
                        'GET DATA': $scope.partners[c].getdatatime,
                        'PUSH DATA': $scope.partners[c].pushdatatime,
                        'FULL NAME': $scope.partners[c].name,
                        'MOBILE': $scope.partners[c].mobile,
                        'EMAIL': $scope.partners[c].email,
                        'CREATED BY': $scope.partners[c].createdByname,
                        'CREATED DATE': $scope.partners[c].createdDate,
                        'CREATED ROLE': $scope.partners[c].createdByRolename,
                        'LAST MODIFIED BY': $scope.partners[c].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.partners[c].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.partners[c].lastModifiedByRolename,
                        'DELETE FLAG': $scope.partners[c].deleteFlag
                    });

                    $scope.valptrCount++;
                    if ($scope.partners.length == $scope.valptrCount) {
                        alasql('SELECT * INTO XLSX("healthfacilitators.xlsx",{headers:true}) FROM ?', [$scope.hfArray]);
                    }
                }
            }
        };

        /**************************Sorting **********************************/

        var regex = /^([a-z]*)(\d*)/i;
        $scope.boolAsc = false;
        $scope.boolAscOne = false;

        $scope.changeSorting = function (col) {
            if (col == 'firstname') {
                if ($scope.boolAsc) {
                    // console.log('i am in 1 if');
                    $scope.ascendingSorting(col)
                } else {
                    // console.log('i am in 1 else');
                    $scope.descendingSorting(col)
                }
            } else {
                if ($scope.boolAscOne) {
                    // console.log('i am in 2 if');
                    $scope.ascendingSorting(col)
                } else {
                    //  console.log('i am in 2 else');
                    $scope.descendingSorting(col)
                }
            }

        }

        /**********Icon***********/
        $scope.getIcon = function (column) {

            if ($scope.boolAsc) {
                return 'fa-sort-up';

            } else {
                return 'fa-sort-desc';
            }
        }
        $scope.getIconOne = function (colnew) {

            if ($scope.boolAscOne) {
                return 'fa-sort-up';

            } else {
                return 'fa-sort-desc';
            }
        }

        /*******************/
        $scope.ascendingSorting = function (column) {
            var x = column;

            function sortFn(a, b) {
                var _a = a[x].match(regex);
                var _b = b[x].match(regex);

                // if the alphabetic part of a is less than that of b => -1
                if (_a[1] < _b[1]) return -1;
                // if the alphabetic part of a is greater than that of b => 1
                if (_a[1] > _b[1]) return 1;

                // if the alphabetic parts are equal, check the number parts
                var _n = parseInt(_a[2]) - parseInt(_b[2]);
                if (_n == 0) // if the number parts are equal start a recursive test on the rest
                    return sortFn(a[x](_a[0].length), b[x](_b[0].length));
                // else, just sort using the numbers parts
                return _n;
            }
            // $scope.boolAsc = !$scope.boolAsc;
            // $scope.partners.sort(sortFn);
            if (x == 'firstname') {
                //console.log(' i m in boolAsc');
                $scope.boolAsc = !$scope.boolAsc;
                $scope.partners.sort(sortFn);
            } else {
                // console.log(' i m in boolAscOne');
                $scope.boolAscOne = !$scope.boolAscOne;
                $scope.partners.sort(sortFn);
            }
        };

        $scope.descendingSorting = function (column) {
            var y = column;


            function sortFn(a, b) {
                var _a = a[y].match(regex);
                var _b = b[y].match(regex);

                // if the alphabetic part of a is less than that of b => -1
                if (_a[1] < _b[1]) return 1;
                // if the alphabetic part of a is greater than that of b => 1
                if (_a[1] > _b[1]) return -1;

                // if the alphabetic parts are equal, check the number parts
                var _n = parseInt(_b[2]) - parseInt(_a[2]);
                if (_n == 0) // if the number parts are equal start a recursive test on the rest
                    return sortFn(a[y](_a[0].length), b[y](_b[0].length));
                // else, just sort using the numbers parts
                return _n;
            }
            if (y == 'firstname') {
                // console.log(' i m in boolAsc');
                $scope.boolAsc = !$scope.boolAsc;
                $scope.partners.sort(sortFn);
            } else {
                // console.log(' i m in boolAscOne');
                $scope.boolAscOne = !$scope.boolAscOne;
                $scope.partners.sort(sortFn);
            }
            // $scope.boolAsc = !$scope.boolAsc;
            // $scope.partners.sort(sortFn);
        };

        /************************** DELETE *******************************************/

        $scope.Delete = function (id) {
            Restangular.one('fieldworkers/' + id).get().then(function (Responsefw) {
                console.log('Responsefw', Responsefw);
                $scope.getSiteassigned = Responsefw.sitesassigned;
                console.log('$scope.getSiteassigned', $scope.getSiteassigned);
                if ($scope.getSiteassigned != null) {
                    alert('This FW having site, You can not delete');
                } else {
                    $scope.auditlog.entityid = id;
                    var respdate = $filter('date')(new Date(), 'dd-MMMM-yyyy');
                    $scope.auditlog.description = 'Field Worker Deleted With Following Details: ' + 'Deleted By UserId - ' + $window.sessionStorage.userId + ', ' + 'Field Worker Id - ' + id + ', ' + 'RoleId - ' + $window.sessionStorage.roleId + ', ' + 'Date - ' + respdate;

                    Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {

                        $scope.item = [{
                            deleteflag: true,
                            lastmodifiedtime: new Date(),
                            modifiedby: $window.sessionStorage.UserEmployeeId
               }]
                        Restangular.one('fieldworkers/' + id).customPUT($scope.item[0]).then(function () {
                            $route.reload();
                        });
                    });
                }
            });
        }

        $scope.getUser = function (employeeid) {
            return Restangular.one('users/findOne?filter[where][employeeid]=' + employeeid + '&filter[where][roleId]=6').get().$object;
        };

    });
