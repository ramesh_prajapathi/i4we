'use strict';

angular.module('secondarySalesApp')
    .controller('WorkFlowCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/workflows/create' || $location.path() === '/workflows/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/workflows/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/workflows/create' || $location.path() === '/workflows/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/workflows/create' || $location.path() === '/workflows/' + $routeParams.id;
            return visible;
        };


        /*********/

        //  $scope.workflows = Restangular.all('workflows').getList().$object;

        if ($routeParams.id) {
            Restangular.one('workflows', $routeParams.id).get().then(function (workflow) {
                $scope.original = workflow;
                $scope.workflow = Restangular.copy($scope.original);
            });
        }
        $scope.searchWorkFlow = $scope.name;
        $scope.pillars = Restangular.all('pillars').getList().$object;

        /************************************************************************** INDEX *******************************************/
        $scope.zn = Restangular.all('workflows').getList().then(function (zn) {
            $scope.workflows = zn;
            angular.forEach($scope.workflows, function (member, index) {
                member.index = index + 1;
            });
        });

        /*************************************************************************** SAVE *******************************************/
        $scope.validatestring = '';
        $scope.SaveWorkFlow = function () {

            $scope.workflows.post($scope.workflow).then(function () {
                console.log('WorkFlow Saved');
                window.location = '/workflows';
            });
        };
        /*************************************************************************** UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.UpdateWorkFlow = function () {
            document.getElementById('name').style.border = "";
            if ($scope.workflow.name == '' || $scope.workflow.name == null) {
                $scope.workflow.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your workflow name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.workflows.customPUT($scope.workflow).then(function () {
                    console.log('WorkFlow Saved');
                    window.location = '/workflows';
                });
            }


        };
        /*************************************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            if (confirm("Are you sure want to delete..!") == true) {
                Restangular.one('workflows/' + id).remove($scope.workflow).then(function () {
                    $route.reload();
                });

            } else {

            }

        }


        $scope.getPillar = function (pillarId) {
            return Restangular.one('pillars', pillarId).get().$object;
        };

    });