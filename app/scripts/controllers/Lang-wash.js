'use strict';

angular.module('secondarySalesApp')
    .controller('LangWashCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/


        $scope.HideCreateButton = true;
        $scope.langdisable = false;

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        $scope.$watch('wash.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                
                 Restangular.one('washLanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.wash = Restangular.copy($scope.original);
                    });
                Restangular.all('washLanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                        $scope.HideCreateButton = true;
                    } else {
                        //$scope.LangId = response[0].id;
                        // $scope.HideCreateButton = false;
                        //  $scope.langdisable = true;

                        //  $scope.lhs = response[0];
                        //  console.log('$scope.lhs', $scope.lhs);
                         $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';


                    }
                });

            }
        });
    
                /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/LangWash-list';

        };
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/

        $scope.wash = {
            deleteFlag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId
        };

        $scope.Save = function () {
            Restangular.all('washLanguages').post($scope.wash).then(function (intResponse) {
               // console.log('intResponse', intResponse);
                window.location = '/LangWash-list';
            });

        };

        $scope.Update = function () {
            Restangular.one('washLanguages', $routeParams.id).customPUT($scope.wash).then(function (intResponse) {
              //  console.log('intResponse', intResponse);
                window.location = '/LangWash-list';
            });
        };

        if ($routeParams.id) {
            $scope.HideCreateButton = false;
            $scope.langdisable = true;
            Restangular.one('washLanguages', $routeParams.id).get().then(function (wash) {
                $scope.original = wash;
                $scope.wash = Restangular.copy($scope.original);
            });
        }
    });