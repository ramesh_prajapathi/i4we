'use strict';

angular.module('secondarySalesApp')
    .controller('SHGMeetingsEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {

        $scope.SHGmeetingLanguage = {};

        Restangular.one('shgmeetingLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.SHGmeetingLanguage = langResponse[0];
            $scope.message = langResponse[0].shgMeetingSaved
            $scope.ShgMeetingHeading = langResponse[0].updateMeeting;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        $scope.showCreate = true;
        $scope.disableCreate = false;
        $scope.updateCreate = true;
        $scope.hideCreate = false;
        $scope.HideAdd = true;

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.hideAssigned = true;

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.shgmeetingheader.associatedHF = $window.sessionStorage.userId;

            });

            $scope.shgFilterCall = 'shgs?filter[where][deleteFlag]=false&filter[where][associatedHF]=' + $window.sessionStorage.userId;

        } else {

            $scope.hideAssigned = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.shgmeetingheader.associatedHF = $scope.shgmeetingheader.associatedHF;
            });

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        }

        Restangular.all($scope.shgFilterCall).getList().then(function (shg) {
            $scope.shgs = shg;
        });

        $scope.auditlog = {
            action: 'Update',
            module: 'SHG Meeting',
            owner: $window.sessionStorage.userId,
            datetime: new Date(),
            details: 'SHG Meeting Updated',
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        };

        $scope.savedChange = function () {
            $scope.totalSavedInthisMeeting = 0;
            $scope.totalSavedInthisMeetingOne = 'Rs. ' + $scope.totalSavedInthisMeeting;

            angular.forEach($scope.shgMeetingTrailers, function (member, index) {
                $scope.totalSavedInthisMeeting = (+$scope.totalSavedInthisMeeting) + (+member.amount);
                $scope.totalSavedInthisMeetingOne = 'Rs. ' + $scope.totalSavedInthisMeeting;
                console.log($scope.totalSavedInthisMeeting);
            });
        };

        $scope.changeAmountVal = function (amount, index) {
            if (amount > $scope.amountToSave) {
                $scope.shgMeetingTrailers[index].amount = $scope.amountToSave;
                $scope.savedChange();
            } else {
                $scope.shgMeetingTrailers[index].amount = amount;
                $scope.savedChange();
            }
        };

        $scope.shgmeetingheaders = Restangular.all('shgmeetingheaders');
        $scope.shgmeetingtrailers = Restangular.all('shgmeetingtrailers');

        $scope.confirmModel = false;

        $scope.validatestring = '';

        $scope.noOfMembersAttendedDisply = 0;
        $scope.noOfMembersSavedDisply = 0;

        $scope.ConfirmPopup = function () {

            $scope.noOfMembersAttendedDisply = 0;
            $scope.noOfMembersSavedDisply = 0;

            if ($scope.shgmeetingheader.groupId == '' || $scope.shgmeetingheader.groupId == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGmeetingLanguage.pleaseSelectShg;

            } else if ($scope.shgmeetingheader.lastMeetingDate == '' || $scope.shgmeetingheader.lastMeetingDate == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGmeetingLanguage.pleaseSelectDateOfMeeting;

            } else if ($scope.shgmeetingheader.associatedHF == '' || $scope.shgmeetingheader.associatedHF == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGmeetingLanguage.selectAssignedTo;

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                angular.forEach($scope.shgMeetingTrailers, function (member, index) {
                    if (member.attended == true) {
                        $scope.noOfMembersAttendedDisply++;
                    }
                    if (member.saved == true) {
                        $scope.noOfMembersSavedDisply++;
                    }
                });

                $scope.confirmModel = true;
            }
        };

        $scope.trailerCount = 0;

        $scope.SaveHeader = function () {

            $scope.confirmModel = false;

            $scope.toggleLoading();

            $scope.updatedSHG = {
                totalAmountSaved: (+$scope.totalAmountSaved) + (+$scope.totalSavedInthisMeeting)
            };

            $scope.shgmeetingheader.noOfMembers = $scope.shgMeetingTrailers.length;
            $scope.shgmeetingheader.amountSaved = $scope.totalSavedInthisMeeting;
            $scope.shgmeetingheader.noOfMembersAttended = $scope.noOfMembersAttendedDisply;
            $scope.shgmeetingheader.noOfMembersSaved = $scope.noOfMembersSavedDisply;
            $scope.shgmeetingheader.lastModifiedDate = new Date();
            $scope.shgmeetingheader.lastModifiedBy = $window.sessionStorage.userId;
            $scope.shgmeetingheader.lastModifiedByRole = $window.sessionStorage.roleId;

            $scope.shgmeetingheaders.customPUT($scope.shgmeetingheader).then(function (PostResponce) {
                $scope.shgMeetingId = PostResponce.id;
                Restangular.one('shgs', PostResponce.groupId).customPUT($scope.updatedSHG).then(function (memResp) {

                    $scope.auditlog.rowId = PostResponce.id;

                    Restangular.all('audittrials').post($scope.auditlog).then(function (respAudit) {
                        $scope.saveTrailer();
                    });
                });
            });
        }

        $scope.saveTrailer = function () {
            $scope.shgMeetingTrailers[$scope.trailerCount].shgMeetingId = $scope.shgMeetingId;
            $scope.shgMeetingTrailers[$scope.trailerCount].associatedHF = $scope.shgmeetingheader.associatedHF;
            $scope.shgMeetingTrailers[$scope.trailerCount].amountSaved = $scope.shgMeetingTrailers[$scope.trailerCount].amount;

            $scope.shgmeetingtrailers.customPUT($scope.shgMeetingTrailers[$scope.trailerCount]).then(function (PostResponce) {
                $scope.trailerCount++;
                if ($scope.trailerCount < $scope.shgMeetingTrailers.length) {
                    $scope.saveTrailer();
                } else {
                    $scope.saveHealthyDaysFunc($scope.shgMeetingId);
                }
            });
        }

        var healthCount = 0;

        $scope.healthyDaysPost = {};

        $scope.saveHealthyDaysFunc = function (shgMeetingId) {

            if (healthCount < $scope.familymembersArray.length) {

                $scope.healthyDaysPost.shgMeetingId = shgMeetingId;
                $scope.healthyDaysPost.memberId = $scope.familymembersArray[healthCount].id;
                $scope.healthyDaysPost.isHeadOfHH = $scope.familymembersArray[healthCount].isHeadOfFamily;
                $scope.healthyDaysPost.parentId = $scope.familymembersArray[healthCount].parentId;
                $scope.healthyDaysPost.dateOfMeeting = $scope.shgmeetingheader.lastMeetingDate;
                $scope.healthyDaysPost.lastMeetingDate = $scope.lastMeetingDate;
                $scope.healthyDaysPost.daysUnwell = $scope.familymembersArray[healthCount].daysUnwell;
                $scope.healthyDaysPost.primarySymptom = $scope.familymembersArray[healthCount].primarySymptom;
                $scope.healthyDaysPost.secondarySymptom = $scope.familymembersArray[healthCount].secondarySymptom;
                $scope.healthyDaysPost.illness = $scope.familymembersArray[healthCount].illness;
                $scope.healthyDaysPost.associatedHF = $scope.shgmeetingheader.associatedHF;
                $scope.healthyDaysPost.countryId = $window.sessionStorage.countryId;
                $scope.healthyDaysPost.stateId = $window.sessionStorage.stateId;
                $scope.healthyDaysPost.districtId = $window.sessionStorage.districtId;
                $scope.healthyDaysPost.siteId = $window.sessionStorage.siteId.split(",")[0];
                $scope.healthyDaysPost.createdDate = new Date();
                $scope.healthyDaysPost.createdBy = $window.sessionStorage.userId;
                $scope.healthyDaysPost.createdByRole = $window.sessionStorage.roleId;
                $scope.healthyDaysPost.lastModifiedDate = new Date();
                $scope.healthyDaysPost.lastModifiedBy = $window.sessionStorage.userId;
                $scope.healthyDaysPost.lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.healthyDaysPost.deleteFlag = false;
                $scope.healthyDaysPost.id = $scope.familymembersArray[healthCount].healthydayid;

                if ($scope.familymembersArray[healthCount].daysUnwell > 0) {
                    Restangular.all('healthydays').customPUT($scope.healthyDaysPost).then(function (PostResponce) {
                        healthCount++;
                        $scope.saveHealthyDaysFunc(shgMeetingId);
                    });
                } else if ($scope.familymembersArray[healthCount].daysUnwell == 0) {
                    healthCount++;
                    $scope.saveHealthyDaysFunc(shgMeetingId);
                }

            } else {
                $scope.updateHealthyDays();

            }
        }


        /*************************new Changes for update Healthy Day ***************/

        var healthyDayCount = 0;

        $scope.healthyDaysUpdate = {};

        $scope.updateHealthyDays = function () {

            if (healthyDayCount < $scope.familymembersArray.length) {

                Restangular.one('members', $scope.familymembersArray[healthyDayCount].id).get().then(function (memberResp) {

                    $scope.myCount = $scope.familymembersArray[healthyDayCount].daysUnwell - ($scope.familymembersArray[healthyDayCount].daysUnwellNew);

                    //  console.log($scope.myCount);

                    $scope.healthyDaysUpdate.healthyDays = memberResp.healthyDays + ($scope.myCount);

                    Restangular.one('members', $scope.familymembersArray[healthyDayCount].id).customPUT($scope.healthyDaysUpdate).then(function (Resp) {
                        //  console.log('Resp', Resp);
                        healthyDayCount++;
                        $scope.updateHealthyDays();
                    });
                });

            } else {
                $scope.updateLoan();
            }
        };

        /************************** update loan module *****************************/

        var loanCount = 0;

        $scope.loanUpdate = {};

        $scope.loanModelUpdate = {};

        $scope.updateLoan = function () {

            if (loanCount < $scope.loanArray.length) {

                if ($scope.loanArray[loanCount].existingLoan == false) {

                    $scope.loanArray[loanCount].shgmeetingId = $scope.shgMeetingId;

                    Restangular.all('loanheaders').post($scope.loanArray[loanCount]).then(function (loanResp) {
                        loanCount++;
                        $scope.updateLoan();
                    });

                } else if ($scope.loanArray[loanCount].existingLoan == true) {

                    if ($scope.loanArray[loanCount].loanOutStanding == 0 || $scope.loanArray[loanCount].loanOutStanding < 0) {
                        $scope.loanArray[loanCount].completedFlag = true;
                    }

                    Restangular.all('loanheaders', $scope.loanArray[loanCount].id).customPUT($scope.loanArray[loanCount]).then(function (loanResp) {

                        Restangular.all('loantrailers?filter[where][loanheaderId]=' + loanResp.id + '&filter[where][memberId]=' + loanResp.memberId + '&filter[where][shgmeetingId]=' + $routeParams.id).getList().then(function (myloantr) {

                            if (myloantr.length > 0) {

                                $scope.loanModelUpdate.installmentRepaid = loanResp.installmentRepaid;
                                $scope.loanModelUpdate.loanOutStanding = loanResp.loanOutStanding;

                                Restangular.one('loantrailers', myloantr[0].id).customPUT($scope.loanModelUpdate).then(function (Resps) {
                                    loanCount++;
                                    $scope.updateLoan();
                                });
                                
                            } else {

                                $scope.loanUpdate.shgmeetingId = loanResp.shgmeetingId;
                                $scope.loanUpdate.loanheaderId = loanResp.id;
                                $scope.loanUpdate.date = new Date();
                                $scope.loanUpdate.amount = loanResp.amountOfInstallmentsToBePaid;
                                $scope.loanUpdate.memberId = loanResp.memberId;
                                $scope.loanUpdate.associatedHF = loanResp.associatedHF;
                                $scope.loanUpdate.headOfHouseholdId = loanResp.headOfHouseholdId;
                                $scope.loanUpdate.shgId = loanResp.shgId;
                                $scope.loanUpdate.amountOfLoan = loanResp.amountOfLoan;
                                $scope.loanUpdate.reason = loanResp.reason;
                                $scope.loanUpdate.noOfInstallmentsToBePaid = loanResp.noOfInstallmentsToBePaid;
                                $scope.loanUpdate.paidInstallments = loanResp.paidInstallments;
                                $scope.loanUpdate.amountOfInstallmentsToBePaid = loanResp.amountOfInstallmentsToBePaid;
                                $scope.loanUpdate.installmentRepaid = loanResp.installmentRepaid;
                                $scope.loanUpdate.loanOutStanding = loanResp.loanOutStanding;
                                $scope.loanUpdate.createdDate = new Date();
                                $scope.loanUpdate.createdBy = $window.sessionStorage.userId;
                                $scope.loanUpdate.createdByRole = $window.sessionStorage.roleId;
                                $scope.loanUpdate.lastModifiedDate = new Date();
                                $scope.loanUpdate.lastModifiedBy = $window.sessionStorage.userId;
                                $scope.loanUpdate.lastModifiedByRole = $window.sessionStorage.roleId;
                                $scope.loanUpdate.countryId = $window.sessionStorage.countryId;
                                $scope.loanUpdate.stateId = $window.sessionStorage.stateId;
                                $scope.loanUpdate.districtId = $window.sessionStorage.districtId;
                                $scope.loanUpdate.siteId = $window.sessionStorage.siteId.split(",")[0];
                                $scope.loanUpdate.deleteFlag = false;

                                Restangular.all('loantrailers').post($scope.loanUpdate).then(function (Resps) {
                                    loanCount++;
                                    $scope.updateLoan();
                                });
                            }
                        });
                    });
                }

            } else {

                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setTimeout(function () {
                    window.location = "/shgmeetings-list";
                }, 1500);
            }
        };

        /*************************new Changes for update Healthy Day ***************/

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /************** Healthy Days *********************************/

        $scope.searchModel = false;

        Restangular.all('symptoms?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (symptms) {
            $scope.symptoms = $filter('orderBy')(symptms, 'orderNo');
        });

        Restangular.all('illnesses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (illns) {
            $scope.illnesses = $filter('orderBy')(illns, 'orderNo');
        });

        Restangular.all('symptoms?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (symptms) {
            $scope.symptomsSecondaryArray = $filter('orderBy')(symptms, 'orderNo');
        });

        $scope.changeSymptom = function (data, index) {

            angular.forEach($scope.familymembers[index].symptomsSecondaryArray, function (value, index) {
                if (value.id == data) {
                    value.disabled = true;
                } else {
                    value.disabled = false;
                }
            });
        };

        $scope.shgmeetingheader = {};

        $scope.openSearch = function (id) {

            $scope.searchModel = true;

            Restangular.one('members', id).get().then(function (membr) {

                Restangular.all('members?filter[where][parentId]=' + membr.parentId + '&filter[where][deleteFlag]=false').getList().then(function (fmembers) {

                    Restangular.all('healthydays?filter[where][parentId]=' + membr.parentId + '&filter[where][shgMeetingId]=' + $routeParams.id + '&filter[where][deleteFlag]=false').getList().then(function (hdays) {

                        $scope.hdays = hdays;

                        //  console.log($scope.hdays);

                        $scope.familymembers = $filter('orderBy')(fmembers, 'individualId');

                        //   console.log($scope.familymembers);

                        angular.forEach($scope.familymembers, function (data, index) {
                            data.index = index + 1;
                            data.symptoms = $scope.symptoms;
                            data.symptomsSecondaryArray = $scope.symptomsSecondaryArray;
                            data.illnesses = $scope.illnesses;
                            data.daysUnwell = 0;
                            data.daysUnwellNew = 0;
                            data.disabledField = true;

                            for (var i = 0; i < $scope.hdays.length; i++) {
                                if (data.id == hdays[i].memberId) {
                                    data.daysUnwell = hdays[i].daysUnwell;
                                    data.daysUnwellNew = hdays[i].daysUnwell;
                                    data.primarySymptom = hdays[i].primarySymptom;
                                    data.secondarySymptom = hdays[i].secondarySymptom;
                                    data.illness = hdays[i].illness;
                                    data.disabledField = false;
                                    data.disabledInput = false;
                                    data.healthydayid = hdays[i].id;
                                    break;
                                } else {
                                    data.daysUnwell = 0;
                                    data.daysUnwellNew = 0;
                                    data.disabledField = true;
                                    data.disabledInput = false;
                                    data.healthydayid = null;
                                }
                            }
                        });
                    });
                });

                Restangular.all('shgmeetingheaders?filter[where][groupId]=' + $scope.shgmeetingheader.groupId).getList().then(function (shgmeet) {
                    $scope.shgmeetings = shgmeet;

                    if ($scope.shgmeetings.length > 0) {
                        $scope.lastMeetingDate = $scope.shgmeetings[$scope.shgmeetings.length - 1].lastMeetingDate;
                    }
                });
            });
        };

        $scope.changeUnwell = function (index, value) {
            if (value < 1) {
                $scope.familymembers[index].disabledField = true;
            } else {
                $scope.familymembers[index].disabledField = false;
                $scope.familymembers[index].symptoms = $scope.symptoms;
                $scope.familymembers[index].symptomsSecondaryArray = $scope.symptomsSecondaryArray;
                $scope.familymembers[index].illnesses = $scope.illnesses;
            }
        };

        $scope.familymembersArray = [];

        $scope.SaveHelathyDays = function (index, value) {
            $scope.searchModel = false;

            angular.forEach($scope.familymembers, function (value, index) {
                value.index = index;
                $scope.familymembersArray.push(value);
            });
            //  console.log($scope.familymembersArray);
        };

        $scope.loanModel = false;

        $scope.loanheader = {};

        $scope.openLoan = function (id, index, Flag) {

            $scope.loanModel = true;
            
              if(Flag == true){
                $scope.showViewLoan = true;
                $scope.showAddLoan = false;
                
            }else{
                $scope.showAddLoan = true;
                $scope.showViewLoan = false;
                
            }

            if ($scope.loanArray.length > 0) {

                var dup = _.findWhere($scope.loanArray, {
                    mymemberId: id
                });

                if (dup == undefined) {
                    $scope.getFunc(id, index);
                    $scope.loanheader.installmentRepaid = '';
                } else {
                    $scope.loanheader = dup;
                }
            } else {
                $scope.getFunc(id, index);
                $scope.loanheader.installmentRepaid = '';
            }
        };

        $scope.getFunc = function (id, index) {
            Restangular.all('loanheaders?filter[where][memberId]=' + id + '&filter[where][completedFlag]=false').getList().then(function (loan) {

                $scope.loans = loan;

                if (loan.length > 0) {

                    Restangular.all('loantrailers?filter[where][loanheaderId]=' + loan[0].id).getList().then(function (loantr) {
                        $scope.loanheader.paidInstallments = loantr.length + 1;

                        if (loantr.length > 0) {

                            $scope.myloanid = loan[0].id;
                            $scope.myobject = {};
                            $scope.myobject1 = {};
                            $scope.myobject = loantr[loantr.length - 1];
                            $scope.myobject1 = loantr[loantr.length - 2];
                            $scope.loanheader.installmentRepaid = $scope.myobject.installmentRepaid;
                            $scope.loanheader.amountOfInstallmentsToBePaid = $scope.myobject.amountOfInstallmentsToBePaid;

                            var s = loan[0].noOfInstallmentsToBePaid;
                            var myVal = parseInt(loan[0].amountOfLoan) / s;
                            var myVal1 = parseInt($scope.myobject.amountOfInstallmentsToBePaid) - myVal;
                            $scope.loanheader.interest = myVal1;

                            $scope.loanheader.interest = Math.round(myVal1);
                            $scope.loanheader.myinstallmentRepaid = Math.round(repaidinst) - (-loan[0].installmentRepaid);
                           // $scope.loanheader.myloanoutstanding = parseInt($scope.myobject1.loanOutStanding);
                            $scope.loanheader.myloanoutstanding = parseInt(loan[0].loanOutStanding);

                        } else {
                            var s = loan[0].noOfInstallmentsToBePaid;
                            var lastdate = new Date(loan[0].dateOfMeeting);
                            var todate = new Date();
                            var oneDay = 24 * 60 * 60 * 1000;
                            var diffDays = Math.round(Math.abs((lastdate.getTime() - todate.getTime()) / (oneDay)));


                            var myVal = parseInt(loan[0].loanOutStanding) * diffDays * 24;
                            var myVal1 = 365 * 100;
                            var finalVal = myVal / myVal1;
                            $scope.loanheader.interest = Math.round(finalVal);

                            var instl = loan[0].amountOfLoan / s;
                            var finalinstl = instl + finalVal;

                            $scope.loanheader.amountOfInstallmentsToBePaid = Math.round(finalinstl);
                            var repaidinst = $scope.loanheader.amountOfInstallmentsToBePaid - (finalVal);
                            $scope.loanheader.myinstallmentRepaid = Math.round(repaidinst) - (-loan[0].installmentRepaid);
                            $scope.loanheader.myloanoutstanding = parseInt(loan[0].loanOutStanding);
                            $scope.loanheader.mymemberId = id;
                        }

                        $scope.disableLoanAmount = true;
                        $scope.disableReason = true;
                        $scope.disableInstallmentRepaid = false;

                        $scope.loanheader.memberId = id;
                        $scope.loanheader.mymemberId = id;
                        $scope.loanheader.amountOfLoan = loan[0].amountOfLoan;
                        $scope.loanheaderamountOfLoan = 'Rs. ' + loan[0].amountOfLoan;
                        $scope.loanheader.reason = loan[0].reason;
                        $scope.loanheader.noOfInstallmentsToBePaid = loan[0].noOfInstallmentsToBePaid;
                        $scope.loanheader.loanOutStanding = loan[0].loanOutStanding;
                        $scope.loanheader.id = loan[0].id;
                        $scope.loanheader.existingLoan = true; 
                        
                        if($window.sessionStorage.language == 1){
                             Restangular.one('reasonforloans?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][id]=' + loan[0].reason).get().then(function(respqq){
                            console.log('respqq', respqq);
                                 $scope.loanheaderreason = respqq[0].name;
                            
                        });
                            
                        } else {
                            
                             Restangular.one('reasonforloans?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][parentId]=' + loan[0].reason).get().then(function(respqq){
                            $scope.loanheaderreason = respqq[0].name;
                            
                        });
                        }
                    });

                } else {

                    $scope.loanheader = {
                        createdDate: new Date(),
                        createdBy: $window.sessionStorage.userId,
                        createdByRole: $window.sessionStorage.roleId,
                        lastModifiedDate: new Date(),
                        lastModifiedBy: $window.sessionStorage.userId,
                        lastModifiedByRole: $window.sessionStorage.roleId,
                        countryId: $window.sessionStorage.countryId,
                        stateId: $window.sessionStorage.stateId,
                        districtId: $window.sessionStorage.districtId,
                        siteId: $window.sessionStorage.siteId.split(",")[0],
                        deleteFlag: false,
                        completedFlag: false
                    };

                    $scope.disableLoanAmount = false;
                    $scope.disableReason = false;
                    $scope.loanheader.paidInstallments = 0;
                    $scope.loanheader.memberId = id;
                    $scope.loanheader.associatedHF = $scope.shgmeetingheader.associatedHF;
                    $scope.loanheader.headOfHouseholdId = $scope.headOfHouseholdId;
                    $scope.loanheader.shgId = $scope.shgmeetingheader.groupId;
                    $scope.loanheader.dateOfMeeting = $scope.shgmeetingheader.lastMeetingDate;
                    $scope.loanheader.mymemberId = id;
                    $scope.loanheader.existingLoan = false;
                    $scope.disableInstallmentRepaid = true;
                }
            });
        };

        $scope.reasonforloans = Restangular.all('reasonforloans?filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.changeAmount = function (amount, data) {
            if (amount == 0) {
                $scope.disableReason = true;
            } else if (amount > 0) {
                $scope.disableReason = false;
            }

            // $scope.changeDue(data);
        };

        $scope.changeDue = function (value) {

            $timeout(function () {

                var s = value;
                var instl = $scope.loanheader.amountOfLoan / s;
                $scope.loanheader.amountOfInstallmentsToBePaid = Math.round(instl);
                $scope.loanheader.installmentRepaid = 0;
                $scope.loanheader.loanOutStanding = $scope.loanheader.amountOfLoan;

            }, 750);
        };

        $scope.changeInstallmentRepaid = function (amnt) {

            $timeout(function () {

                var myDue = amnt - (Math.round($scope.loanheader.interest));
                $scope.loanheader.loanOutStanding = $scope.loanheader.myloanoutstanding - (myDue);

            }, 1000);

        };

        $scope.loanArray = [];

        $scope.SaveLoan = function () {

            if ($scope.loanArray.length > 0) {

                var duplicate = _.findWhere($scope.loanArray, {
                    mymemberId: $scope.loanheader.mymemberId
                });

                if (duplicate == undefined) {
                    $scope.loanArray.push($scope.loanheader);
                } else {
                    // $scope.loanArray[i($scope.loanheader);
                }

            } else {
                $scope.loanArray.push($scope.loanheader);
            }

            //  console.log($scope.loanArray);
            $scope.loanModel = false;
            $scope.loanheader = {};
        };

        $scope.installmentModel = false;

        $scope.openInsts = function () {
            $scope.installmentModel = true;

            Restangular.all('loantrailers?filter[where][loanheaderId]=' + $scope.myloanid).getList().then(function (loans) {
                $scope.loaninstallments = loans;

                angular.forEach($scope.loaninstallments, function (data, index) {
                    data.index = index + 1;
                });
            });
        };

        /*************************************************************/

        //Datepicker settings start
        var sevendays = new Date();

        $scope.shgmeetingheader.lastMeetingDate = new Date();

        sevendays.setDate(sevendays.getDate() + 7);

        $scope.today = function () {};

        $scope.today();

        $scope.showWeeks = true;

        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dtmin = new Date();

        $scope.dtmax = new Date();

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };

        $scope.toggleMin();

        $scope.picker = {};

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end
        $scope.datetimehide = false;
        $scope.reponserec = true;
        $scope.delaydis = true;
        $scope.collectedrequired = true;

        if ($routeParams.id) {

            $scope.hideAssigned = true;
            $scope.totalSavedInthisMeeting = 0;
            $scope.totalSavedInthisMeetingOne = 'Rs. ' + $scope.totalSavedInthisMeeting;

            Restangular.all('shgmeetingheaders?filter[where][id]=' + $routeParams.id).getList().then(function (shgmeethead) {
                $scope.shgmeetingheader = shgmeethead[0];
                $scope.totalSavedInthisMeeting = shgmeethead[0].amountSaved;
                $scope.totalSavedInthisMeetingOne = 'Rs. ' + $scope.totalSavedInthisMeeting;

                Restangular.one('shgs', shgmeethead[0].groupId).get().then(function (shg) {
                    $scope.associatedHF = shg.associatedHF;
                    $scope.amountToSave = shg.amountToSave;
                    $scope.totalAmountSaved = shg.totalAmountSaved;
                    $scope.shgNameDisply = shg.groupName;
                });
            });

            Restangular.all('shgmeetingtrailers?filter[where][shgMeetingId]=' + $routeParams.id).getList().then(function (shgmeettrail) {
                $scope.shgMeetingTrailers = shgmeettrail;
                $scope.noOfMembersShgDisply = shgmeettrail.length;
                Restangular.all('members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (mems) {
                    $scope.beneficiaries = mems;

                    angular.forEach($scope.shgMeetingTrailers, function (member, index) {
                        member.index = index + 1;
                         //member.imagedispaly = 'loan.png';
                        
                        /******* new Edit*******/
                        Restangular.all('loanheaders?filter[where][memberId]=' + member.memberId + '&filter[where][completedFlag]=false').getList().then(function (loan) {
                           // console.log('loan', loan);

                           if (loan.length > 0) {
                               // $scope.imagedispaly = "loan-green.png";
                                member.imagedispaly = 'loan-green.png';
                                member.loanFlag = true;
                                
                            } else {
                               // $scope.imagedispaly = "loan.png";
                                 member.imagedispaly = 'loan.png';
                                 member.loanFlag = false;
                            }
                        });
                        /******* new Edit*******/
                        
                        
                        for (var i = 0; i < $scope.beneficiaries.length; i++) {
                            if ($scope.beneficiaries[i].id == member.memberId) {
                                member.name = $scope.beneficiaries[i].name;
                                member.individualId = $scope.beneficiaries[i].individualId;
                                break;
                            }
                        }
                    });
                });
            });
        }
    });