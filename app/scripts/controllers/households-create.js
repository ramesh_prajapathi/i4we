'use strict';
angular.module('secondarySalesApp')
    .controller('HouseholdCreateCtrl', function ($scope, Restangular, $filter, $timeout, $window, $route, $modal, $location) {

        //  console.log('$location', $location.$$url);

        $scope.memberone = {
            aadharNumber: '',
            mobile: ''
        };

        $scope.memberlists = [];

        $scope.Idcount = 1;

        $scope.HHLanguage = {};


        $scope.member = {
            relation: null,
            literacyLevel: null,
            nameOfSchool: null,
            nameOfFactory: null,
            deleteFlag: false,
            isHeadOfFamily: true,
            shgId: null,
            age: '',
            name: '',
            mobile: '',
            healthyDays: 0,
            aadharNumber: '',
            quickaddFlag: false,
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        };

        $scope.household = {
            deleteFlag: false,
            createdDate: new Date(),
            quickaddFlag: false,
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        };



        Restangular.one('householdlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.HHLanguage = langResponse[0];
            $scope.headingName = $scope.HHLanguage.houseHoldCreate;
            // $scope.modalTitle = $scope.HHLanguage.thankYou;
            $scope.message = $scope.HHLanguage.thankYouCreated;
        });

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.disableAssigned = true;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.member.associatedHF = $window.sessionStorage.userId;

            });
        } else {

            $scope.disableAssigned = false;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.member.associatedHF = urs[0].id;

            });

        }

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        if ($location.$$url == '/shg/household/create') {
            $scope.HideSHG = true;
        } else {
            $scope.HideSHG = false;
        }

        $scope.HideAdd = false;
        $scope.HideUpdate = true;
        $scope.addClicked = false;
        $scope.updateClicked = false;
        $scope.disableSchool = true;
        $scope.migrantdataModal = false;
        $scope.SHGmodal = false;
        $scope.mapdataModal = false;

        if ($location.$$url === '/household/create') {
            $scope.showCancel = true;
        }

        $scope.CancelButton = function () {
            if ($window.sessionStorage.previous == '/shg/create') {
                $location.path('/shg/create');
            } else if ($window.sessionStorage.previous == '/shg/edit/:id') {
                $location.path('/shg/edit/' + $window.sessionStorage.previousRouteparamsId);
            } else {
                $location.path('/household-list');
            }
        };

        $scope.Validname = function ($event, value) {
            var arr = $scope.member.name.split(/\s+/);
            // console.log('arr.length', arr.length);
            if (arr.length == 0 || arr.length == 1) {
                setTimeout(function () {
                    alert($scope.HHLanguage.invalidName);
                }, 1000);
                document.getElementById('name').style.borderColor = "#FF0000";
                //                var name = $window.document.getElementById('name');
                //                name.focus();
            } else {
                document.getElementById('name').style.borderColor = "";
                $scope.duplicateHHFind(value);
            }
        };

        $('#latitude').keypress(function (evt) {
            if (/^[0-9.,]+$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $('#longitude').keypress(function (evt) {
            if (/^[0-9.,]+$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $scope.stateupdate = {};

        Restangular.one('users', $window.sessionStorage.userId).get().then(function (usr) {
            Restangular.one('states', usr.stateId).get().then(function (ste) {

                var num = ste.sequence,
                    str = num.toString(),
                    len = str.length;

                if (len == 1) {
                    $scope.seqSize = '0000';
                } else if (len == 2) {
                    $scope.seqSize = '000'
                } else if (len == 3) {
                    $scope.seqSize = '00'
                } else if (len == 4) {
                    $scope.seqSize = '0'
                } else if (len == 5) {
                    $scope.seqSize = '';
                }

                Restangular.one('sites', usr.siteId.split(",")[0]).get().then(function (site) {
                    //  $scope.member.HHId = ste.code + '' + site.code + '' + $scope.seqSize + ste.sequence;
                    //  $scope.member.individualId = $scope.member.HHId + '-' + '1';

                    $scope.household.countryId = usr.countryId;
                    $scope.household.stateId = usr.stateId;
                    $scope.household.districtId = usr.districtId;
                    $scope.household.siteId = usr.siteId.split(",")[0];
                    $scope.userstateId = ste.id;
                    $scope.stateupdate.sequence = ste.sequence + 1;
                });
            });
        });

        $scope.ValidAadhar = function ($event) {
            // console.log('$scope.member.aadharNumber', $scope.member.aadharNumber);
            Restangular.one('members/findOne?filter[where][aadharNumber]=' + $scope.member.aadharNumber).get().then(function (membr) {
                alert($scope.HHLanguage.existingAadhar);
                $scope.member.aadharNumber = '';
                document.getElementById('aadharno').style.borderColor = "#FF0000";
            }, function (response) {
                // console.log("Error with status code", response.status);
                document.getElementById('aadharno').style.borderColor = "";
            });
        };

        $scope.ValidMobile = function ($event) {
            if ($scope.member.mobile != '' || $scope.member.mobile != null) {
                if ($scope.member.mobile.length != 10) {
                    $scope.member.mobile = '';
                    alert($scope.HHLanguage.enterDigits);
                }
            }
        };

        $scope.ValidMobileOne = function ($event) {
            if ($scope.memberone.mobile != '' || $scope.memberone.mobile != null) {
                if ($scope.memberone.mobile.length != 10) {
                    $scope.memberone.mobile = '';
                    alert($scope.HHLanguage.enterDigits);
                }
            }
        };

        //        $scope.ValidEmail = function ($event) {
        //            var re = /\S+@\S+\.\S+/;
        //            if ($scope.member.email != '' || $scope.member.email != null) {
        //                if (!re.test($scope.member.email)) {
        //                    $scope.member.email = '';
        //                    alert($scope.HHLanguage.validEmail);
        //                }
        //            }
        //        };

        $scope.ValidAadharOne = function ($event) {
            // console.log('$scope.member.aadharNumber', $scope.member.aadharNumber);
            Restangular.one('members/findOne?filter[where][aadharNumber]=' + $scope.memberone.aadharNumber).get().then(function (membrone) {
                alert($scope.HHLanguage.existingAadhar);
                $scope.memberone.aadharNumber = '';
                document.getElementById('aadhar').style.borderColor = "#FF0000";
            }, function (response) {
                // console.log("Error with status code", response.status);
                document.getElementById('aadhar').style.borderColor = "";
            });
        };

        $scope.duplicateMemberHead = false;

        $scope.duplicateHHFind = function (value) {

            $scope.validatestring1 = '';

            if (value == '' || value == null) {
                return;
            } else {

                Restangular.all($scope.memberFilterCall).getList().then(function (memList) {
                    var resultObject = _.findWhere(memList, {
                        name: value
                    });
                    if (resultObject == undefined) {
                        document.getElementById('name').style.borderColor = "";
                        $scope.duplicateMemberHead = false;
                        //$scope.someMemberName = false;
                    } else {
                        $scope.toggleValidation();
                        $scope.validatestring1 = $scope.HHLanguage.existinMemberNamePleaseChange;
                        // $scope.someMemberName = true;
                        document.getElementById('name').style.borderColor = "#FF0000";
                        $scope.duplicateMemberHead = true;
                    }
                });
            }
        };

        $scope.duplicateMember = false;

        $scope.duplicateFind = function (value) {

            $scope.validatestring1 = '';

            if (value == '' || value == null) {
                return;
            } else {

                Restangular.all($scope.memberFilterCall).getList().then(function (memList) {

                    var resultObject = _.findWhere(memList, {
                        name: value
                    });

                    if (resultObject == undefined) {
                        document.getElementById('nameone').style.borderColor = "";
                        $scope.duplicateMember = false;
                        //$scope.someMemberName = false;
                    } else {
                        $scope.toggleValidation();
                        $scope.validatestring1 = $scope.HHLanguage.existinMemberNamePleaseChange;
                        //  $scope.memberone.name = '';
                        document.getElementById('nameone').style.borderColor = "#FF0000";
                        $scope.duplicateMember = true;
                    }
                });
            }
        };

        $('#phoneno').keypress(function (evt) {
            if (/^-?[0-9]\d*(\.\d+)?$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $scope.AddMember = function () {
            $scope.memberone = {};
            $scope.HideAdd = false;
            $scope.HideUpdate = true;
            $scope.addClicked = false;
            $scope.beneficiarydataModal = true;
            $scope.hideFactory = true;
            $scope.hideOccupation = true;
            $scope.disableSHG = true;
            $scope.dupCheckFlag = true;

            //  $scope.individualId = $scope.member.individualId.split('-');
            //  $scope.indivdId = parseInt($scope.individualId[1]) + $scope.Idcount;

            //   var myNumber = $scope.indivdId;
            //    var formattedNumber = ("0" + myNumber).slice(-2);
            // $scope.memberone.individualId = $scope.member.HHId + '-' + $scope.indivdId;
        };

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.validatestring = '';

        $scope.getRelation = function (relation) {
            return Restangular.one('relationWithHHs', relation).get().$object;
        };

        $scope.getGender = function (gender) {
            return Restangular.one('genders', gender).get().$object;
        };

        $scope.getSHG = function (shgId) {
            return Restangular.one('shgs', shgId).get().$object;
        };

        $scope.ShgbelongsId = [];

        $scope.SaveMember = function () {

            $scope.validatestring = '';

            $scope.Shgbelongs = [];

            $scope.ShgbelongsId = [];

            document.getElementById('nameone').style.border = "";
            document.getElementById('ageone').style.border = "";

            //            if ($scope.memberone.gender == $scope.mygenderfmale && $scope.memberone.age > 18) {
            //                for (var v = 0; v < $scope.memberlists.length; v++) {
            //                    // console.log($scope.ShgbelongsId[v]);
            //                    if ($scope.memberlists[v].shgId == $scope.memberone.shgId) {
            //                        // alert('Already This SHG Belongs to Some Family Member, Please Change SHG');
            //                        $scope.validatestring = $scope.validatestring + $scope.HHLanguage.alreadySHGBelongs;
            //                        $scope.memberone.shgId = '';
            //                    }
            //                }
            //            } else   

            if ($scope.memberone.name == '' || $scope.memberone.name == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterNameOfMember;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.duplicateMember == true) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.existinMemberNamePleaseChange;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.memberone.relation == '' || $scope.memberone.relation == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectRelationship;
                // document.getElementById('date').style.borderColor = "#FF0000";

            } else if ($scope.memberone.dob == '' || $scope.memberone.dob == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectDOB;

            } else if ($scope.memberone.age === '' || $scope.memberone.age === null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterAge;
                document.getElementById('ageone').style.borderColor = "#FF0000";

            } else if ($scope.memberone.literacyLevel == '' || $scope.memberone.literacyLevel == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterLiteracyLevel;
                // document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.memberone.age <= 18) {
                if ($scope.memberone.nameOfSchool === '' || $scope.memberone.nameOfSchool === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterSchoolName;
                }

            } else if ($scope.hideOccupation == false) {
                if ($scope.memberone.occupation === '' || $scope.memberone.occupation === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectOccupation;
                }

            } else if ($scope.memberone.gender == '' || $scope.memberone.gender == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectGender;
                // document.getElementById('age').style.borderColor = "#FF0000";

            }
            if ($scope.hideFactory == false) {
                if ($scope.memberone.nameOfFactory === '' || $scope.memberone.nameOfFactory === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectFactory;
                }

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.addClicked = true;

                $scope.memberlists.push({
                    name: $scope.memberone.name,
                    relation: $scope.memberone.relation,
                    dob: $scope.memberone.dob,
                    age: $scope.memberone.age,
                    literacyLevel: $scope.memberone.literacyLevel,
                    nameOfSchool: $scope.memberone.nameOfSchool,
                    occupation: $scope.memberone.occupation,
                    nameOfFactory: $scope.memberone.nameOfFactory,
                    aadharNumber: $scope.memberone.aadharNumber,
                    esiNumber: $scope.memberone.esiNumber,
                    shgId: $scope.memberone.shgId,
                    gender: $scope.memberone.gender,
                    mobile: $scope.memberone.mobile,
                    address: $scope.member.address,
                    caste: $scope.member.caste,
                    religion: $scope.member.religion,
                    migrant: $scope.member.migrant,
                    bplStatus: $scope.member.bplStatus,
                    latitude: $scope.member.latitude,
                    longitude: $scope.member.longitude,
                    email: $scope.member.email,
                    healthyDays: 0,
                    deleteFlag: false,
                    isHeadOfFamily: false,
                    createdDate: new Date(),
                    createdBy: $window.sessionStorage.userId,
                    createdByRole: $window.sessionStorage.roleId,
                    lastModifiedDate: new Date(),
                    lastModifiedBy: $window.sessionStorage.userId,
                    lastModifiedByRole: $window.sessionStorage.roleId,
                    countryId: $window.sessionStorage.countryId,
                    stateId: $window.sessionStorage.stateId,
                    districtId: $window.sessionStorage.districtId,
                    siteId: $window.sessionStorage.siteId.split(",")[0]
                });

                $scope.Idcount++;

                angular.forEach($scope.memberlists, function (member, index) {
                    member.index = index;

                    Restangular.one('shgs', member.shgId).get().then(function (sgs) {

                        if (sgs.groupName != undefined || sgs.groupName != undefined) {

                            $scope.Shgbelongs.push(sgs.groupName);
                            $scope.ShgbelongsId.push(sgs.id);

                            $scope.ssgThisHHBelongs = $scope.Shgbelongs.toString();
                            $scope.household.ssgThisHHBelongs = $scope.ShgbelongsId.toString();

                              console.log('$scope.household.ssgThisHHBelongs', $scope.household.ssgThisHHBelongs);
                              console.log('$scope.ssgThisHHBelongs', $scope.ssgThisHHBelongs);
                        }

                        member.shgname = sgs.groupName;
                    });

                    if ($window.sessionStorage.language == 1) {
                        var data = $scope.genders.filter(function (arr) {
                            return arr.id == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    } else {
                        var data = $scope.genders.filter(function (arr) {
                            return arr.parentId == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    }

                    if ($window.sessionStorage.language == 1) {
                        var data1 = $scope.relationWithHHs.filter(function (arr) {
                            return arr.id == member.relation
                        })[0];

                        if (data1 != undefined) {
                            member.relationname = data1.name;
                        }
                    } else {
                        var data1 = $scope.relationWithHHs.filter(function (arr) {
                            return arr.parentId == member.relation
                        })[0];

                        if (data1 != undefined) {
                            member.relationname = data1.name;
                        }
                    }
                });

                $scope.beneficiarydataModal = false;
                $scope.memberone = {};
                $scope.validatestring = '';
            }
        };

        var uniqueNames = [];

        $scope.EditMember = function (index) {
            $scope.memberone = {};
            $scope.HideAdd = true;
            $scope.HideUpdate = false;
            $scope.updateClicked = false;
            var uniqueNames = [];
            $scope.beneficiarydataModal = true;
            $scope.memberone = $scope.memberlists[index];
            $scope.tempMember = Restangular.copy($scope.memberlists);
            $scope.oldGroupid = $scope.memberone.shgId;
            $scope.oldMemberid = $scope.memberone.id;
            $scope.dupCheckFlag = false;
        };

        $scope.CLOSEMEMBUTTON = function () {
            $scope.memberlists = $scope.tempMember;
            $scope.beneficiarydataModal = false;
            $scope.memberone = {};
        };
        $scope.CloseAddFamily = function () {

            $scope.beneficiarydataModal = false;
            $scope.memberone = {};
        };

        $scope.RemoveMember = function (index) {
            $scope.memberlists.splice(index, 1);
            $scope.Idcount--;
        };

        $scope.UpdateMember = function () {

            $scope.validatestring = '';

            $scope.Shgbelongs = [];

            $scope.ShgbelongsId = [];

            document.getElementById('nameone').style.border = "";
            document.getElementById('ageone').style.border = "";

            //            if ($scope.memberone.gender == $scope.mygenderfmale && $scope.memberone.age > 18) {
            //                for (var v = 0; v < $scope.tempMember.length; v++) {
            //                    if ($scope.memberone.shgId === $scope.tempMember[v].shgId) {
            //                        console.log($scope.tempMember[v].shgId);
            //                        console.log($scope.memberone.shgId);
            //                        // alert('Already This SHG Belongs to Some Family Member, Please Change SHG');
            //                        $scope.validatestring = $scope.validatestring + $scope.HHLanguage.alreadySHGBelongs;
            //                        $scope.memberone.shgId = '';
            //                    }
            //                }
            //            } else 

            if ($scope.memberone.name == '' || $scope.memberone.name == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterNameOfMember;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.duplicateMember == true) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.existinMemberNamePleaseChange;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.memberone.relation == '' || $scope.memberone.relation == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectRelationship;
                // document.getElementById('date').style.borderColor = "#FF0000";

            } else if ($scope.memberone.dob == '' || $scope.memberone.dob == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectDOB;

            } else if ($scope.memberone.age === '' || $scope.memberone.age === null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterAge;
                document.getElementById('ageone').style.borderColor = "#FF0000";

            } else if ($scope.memberone.literacyLevel == '' || $scope.memberone.literacyLevel == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterLiteracyLevel;
                // document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.memberone.age <= 18) {
                if ($scope.memberone.nameOfSchool === '' || $scope.memberone.nameOfSchool === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterSchoolName;
                }

            } else if ($scope.hideOccupation == false) {
                if ($scope.memberone.occupation === '' || $scope.memberone.occupation === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectOccupation;
                }

            } else if ($scope.memberone.gender == '' || $scope.memberone.gender == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectGender;
                // document.getElementById('age').style.borderColor = "#FF0000";

            }
            if ($scope.hideFactory == false) {
                if ($scope.memberone.nameOfFactory === '' || $scope.memberone.nameOfFactory === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectFactory;
                }

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.updateClicked = true;
                $scope.memberlists[$scope.memberone.index] = $scope.memberone;
                $scope.beneficiarydataModal = false;

                angular.forEach($scope.memberlists, function (member, index) {
                    member.index = index;

                    Restangular.one('shgs', member.shgId).get().then(function (sgs) {
                        member.shgname = sgs.groupName;

                        if (sgs.groupName != undefined || sgs.groupName != undefined) {

                            $scope.Shgbelongs.push(sgs.groupName);
                            $scope.ShgbelongsId.push(sgs.id);

                            $scope.ssgThisHHBelongs = $scope.Shgbelongs.toString();
                            $scope.household.ssgThisHHBelongs = $scope.ShgbelongsId.toString();

                            // console.log('$scope.household.ssgThisHHBelongs', $scope.household.ssgThisHHBelongs);
                            // console.log('$scope.ssgThisHHBelongs', $scope.ssgThisHHBelongs);
                        }
                    });

                    if ($window.sessionStorage.language == 1) {
                        var data = $scope.genders.filter(function (arr) {
                            return arr.id == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    } else {
                        var data = $scope.genders.filter(function (arr) {
                            return arr.parentId == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    }

                    if ($window.sessionStorage.language == 1) {
                        var data1 = $scope.relationWithHHs.filter(function (arr) {
                            return arr.id == member.relation
                        })[0];

                        if (data1 != undefined) {
                            member.relationname = data1.name;
                        }
                    } else {
                        var data1 = $scope.relationWithHHs.filter(function (arr) {
                            return arr.parentId == member.relation
                        })[0];

                        if (data1 != undefined) {
                            member.relationname = data1.name;
                        }
                    }
                });

                $scope.memberone = {};
                $scope.validatestring = '';
            }
        };


        $scope.validatestring = '';
        $scope.confirmSave = false;
        $scope.hh = {};

        $scope.SaveProfile = function () {

            $scope.household.nameOfHeadInHH = $scope.member.name;
            //  $scope.household.HHId = $scope.member.HHId;
            $scope.household.noOfMembers = $scope.memberlists.length + 1;
            $scope.household.associatedHF = $scope.member.associatedHF;

            if ($scope.ShgbelongsId.length == 0) {
                $scope.household.ssgThisHHBelongs = '';
            } else {
                $scope.ShgbelongsId.sort(function (a, b) {
                    return a - b
                });

                var uniqueArrayNew = [$scope.ShgbelongsId[0]];

                for (var i = 1; i < $scope.ShgbelongsId.length; i++) {
                    if ($scope.ShgbelongsId[i - 1] !== $scope.ShgbelongsId[i])
                        uniqueArrayNew.push($scope.ShgbelongsId[i]);
                }

                $scope.household.ssgThisHHBelongs = uniqueArrayNew.toString();
            }

            var re = /\S+@\S+\.\S+/;

            document.getElementById('name').style.border = "";
            document.getElementById('age').style.border = "";
            document.getElementById('address').style.border = "";
            document.getElementById('phoneno').style.borderColor = "";
            document.getElementById('latitude').style.borderColor = "";
            document.getElementById('longitude').style.borderColor = "";

            if ($scope.member.name == '' || $scope.member.name == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.invalidName;
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.duplicateMemberHead == true) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.existinMemberNamePleaseChange;
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.member.dob == '' || $scope.member.dob == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectDOB;
                // document.getElementById('date').style.borderColor = "#FF0000";

            } else if ($scope.member.age === '' || $scope.member.age === null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterAge;
                document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.member.gender == '' || $scope.member.gender == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectGender;
                // document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.member.mobile == '' || $scope.member.mobile == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterDigits;
                document.getElementById('phoneno').style.borderColor = "#FF0000";

            } else if ($scope.member.occupation == '' || $scope.member.occupation == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectOccupation;
                // document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.member.address == '' || $scope.member.address == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectAddress;
                document.getElementById('address').style.borderColor = "#FF0000";

            } else if ($scope.member.caste == '' || $scope.member.caste == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectCaste;

            } else if ($scope.member.religion == '' || $scope.member.religion == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectReligion;

            } else if ($scope.member.migrant == '' || $scope.member.migrant == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectMigrant;

            } else if ($scope.member.bplStatus == '' || $scope.member.bplStatus == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectBPL;

            } else if ($scope.member.latitude == '' || $scope.member.latitude == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterLatitude;
                document.getElementById('latitude').style.borderColor = "#FF0000";

            } else if ($scope.member.longitude == '' || $scope.member.longitude == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterLongitude;
                document.getElementById('longitude').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.hh.displayDOB = $filter('date')($scope.member.dob, 'dd-MMM-yyyy');
                $scope.hh.displayFullNameOfHh = $scope.member.name;
                //  $scope.hh.displayHouseHoldId = $scope.member.HHId;
                //  $scope.hh.displayIndividualId = $scope.member.individualId;
                $scope.hh.displayAge = $scope.member.age;
                $scope.hh.displayPhoneNumber = $scope.member.mobile;
                $scope.hh.displayShgsToWhich = $scope.household.ssgThisHHBelongs;
                $scope.hh.displayAddress = $scope.member.address;
                $scope.hh.displaylatlong = $scope.member.latitude + ' / ' + $scope.member.longitude;
                $scope.confirmSave = true;

                if ($window.sessionStorage.language == 1) {


                    Restangular.one('users', $scope.member.associatedHF).get().then(function (udr) {
                        Restangular.one('genders', $scope.member.gender).get().then(function (vdr) {
                            Restangular.one('occupations', $scope.member.occupation).get().then(function (tdr) {
                                Restangular.one('castes', $scope.member.caste).get().then(function (cast) {
                                    Restangular.one('migrants', $scope.member.migrant).get().then(function (migrnt) {
                                        Restangular.one('religions', $scope.member.religion).get().then(function (reg) {
                                            Restangular.one('bplStatuses', $scope.member.bplStatus).get().then(function (bpls) {

                                                $scope.hh.displayAssociateHF = udr.name;
                                                $scope.hh.displayGender = vdr.name;
                                                $scope.hh.displayOccupation = tdr.name;
                                                $scope.hh.displayCaste = cast.name;
                                                $scope.hh.dispalyReligion = reg.name;
                                                $scope.hh.displayMigrant = migrnt.name;
                                                $scope.hh.displayBplStatus = bpls.name;
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });


                } else {

                    Restangular.one('users', $scope.member.associatedHF).get().then(function (udr) {
                        Restangular.one('genders/findOne?filter[where][parentId]=' + $scope.member.gender + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (vdr) {
                            Restangular.one('occupations/findOne?filter[where][parentId]=' + $scope.member.occupation + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (tdr) {
                                Restangular.one('castes/findOne?filter[where][parentId]=' + $scope.member.caste + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (cast) {
                                    Restangular.one('migrants/findOne?filter[where][parentId]=' + $scope.member.migrant + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (migrnt) {
                                        Restangular.one('religions/findOne?filter[where][parentId]=' + $scope.member.religion + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (reg) {
                                            Restangular.one('bplStatuses/findOne?filter[where][parentId]=' + $scope.member.bplStatus + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (bpls) {

                                                $scope.hh.displayAssociateHF = udr.name;
                                                $scope.hh.displayGender = vdr.name;
                                                $scope.hh.displayOccupation = tdr.name;
                                                $scope.hh.displayCaste = cast.name;
                                                $scope.hh.dispalyReligion = reg.name;
                                                $scope.hh.displayMigrant = migrnt.name;
                                                $scope.hh.displayBplStatus = bpls.name;
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                }
            }
        };

        /********************final save**************/
        $scope.SaveConfirmPopUp = function () {

            $scope.toggleLoading();

            $scope.createClicked = true;

            $scope.saveCount = 0;

            Restangular.all('households').post($scope.household).then(function (Response) {
                // console.log('Response', Response);
                $scope.member.parentId = Response.id;

                Restangular.one('households', Response.id).get().then(function (res) {
                   // console.log('res', res);

                    if ($scope.member.aadharNumber == '' || $scope.member.aadharNumber == null) {
                        console.log('blank aadhar');
                    } else {
                        var aadharNo = $scope.member.aadharNumber.toString();
                        var lastfour = aadharNo.substr(aadharNo.length - 4);
                        var newAadhar = "XXXXXXXX";
                        $scope.member.aadharNumber = newAadhar + '' + lastfour;
                    }

                    $scope.member.HHId = res.HHId;
                    $scope.member.individualId = res.HHId + '-' + '1';

                    Restangular.all('members').post($scope.member).then(function (Resp) {
                      //  console.log('Resp', Resp);

                        if ($scope.memberlists.length == 0) {

                            if ($location.$$url == '/shg/household/create') {
                                Restangular.all('members?filter[where][parentId]=' + Response.id + '&filter[where][gender]=' + 2 + '&filter[where][age][gt]=17').getList().then(function (memResp) {
                                    if (memResp.length == 0) {
                                        if ($window.sessionStorage.previous == '/shg/create') {
                                            $scope.modalInstanceLoad.close();
                                            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                            console.log('reloading...');

                                            setInterval(function () {
                                                $location.path($window.sessionStorage.previous);
                                                window.location = $window.sessionStorage.previous;
                                            }, 1500);

                                        } else if ($window.sessionStorage.previous == '/shg/edit/:id') {
                                            $scope.modalInstanceLoad.close();
                                            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                            console.log('reloading...');

                                            setInterval(function () {
                                                $location.path('/shg/edit/' + $window.sessionStorage.previousRouteparamsId);
                                                window.location = "/shg/edit/" + $window.sessionStorage.previousRouteparamsId;
                                            }, 1500);
                                        }
                                    } else if (memResp.length != 0) {
                                        if ($window.sessionStorage.previous == '/shg/create') {
                                            $window.sessionStorage.shgMemberId = memResp[0].id;
                                            $scope.modalInstanceLoad.close();
                                            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                            console.log('reloading...');

                                            setInterval(function () {
                                                $location.path($window.sessionStorage.previous);
                                                window.location = $window.sessionStorage.previous;
                                            }, 1500);

                                        } else if ($window.sessionStorage.previous == '/shg/edit/:id') {
                                            $window.sessionStorage.shgMemberId = memResp[0].id;
                                            $scope.modalInstanceLoad.close();
                                            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                            console.log('reloading...');

                                            setInterval(function () {
                                                $location.path('/shg/edit/' + $window.sessionStorage.previousRouteparamsId);
                                                window.location = "/shg/edit/" + $window.sessionStorage.previousRouteparamsId;
                                            }, 1500);
                                        }
                                    }
                                });
                            } else {
                                $scope.modalInstanceLoad.close();
                                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                console.log('reloading...');

                                setInterval(function () {
                                    window.location = "/household-list";
                                }, 1500);
                            }
                        } else {
                            $scope.createHH(Response.id, Resp.HHId, Resp.associatedHF);
                        }
                    });
                });
            });

        };
        /********************final save**************/

        $scope.createCount = 0;

        $scope.hhidcount = 2;

        $scope.createHH = function (parId, HHid, associatedHF) {

            if ($scope.createCount < $scope.memberlists.length) {
                $scope.memberlists[$scope.createCount].parentId = parId;
                // $scope.memberlists[$scope.createCount].mobile = $scope.member.mobile;
                $scope.memberlists[$scope.createCount].HHId = HHid;
                $scope.memberlists[$scope.createCount].individualId = HHid + '-' + $scope.hhidcount;
                $scope.memberlists[$scope.createCount].associatedHF = associatedHF;

                if ($scope.memberlists[$scope.createCount].aadharNumber == '' || $scope.memberlists[$scope.createCount].aadharNumber == null) {
                    console.log('blank aadhar');
                } else {
                    var aadharNo = $scope.memberlists[$scope.createCount].aadharNumber.toString();
                    var lastfour = aadharNo.substr(aadharNo.length - 4);
                    var newAadhar = "XXXXXXXX";
                    $scope.memberlists[$scope.createCount].aadharNumber = newAadhar + '' + lastfour;
                }

                Restangular.all('members').post($scope.memberlists[$scope.createCount]).then(function (memResp) {
                    // console.log('memResp', memResp);

                    if ($location.$$url == '/shg/household/create') {

                        $scope.createCount++;
                        $scope.hhidcount++;
                        $scope.createHH(parId, HHid, associatedHF);

                        if ($scope.createCount == $scope.memberlists.length) {

                            Restangular.all('members?filter[where][parentId]=' + parId + '&filter[where][gender]=' + 2 + '&filter[where][age][gt]=17').getList().then(function (memeResp) {

                                $scope.shgmembers = memeResp;

                                if (memeResp.length > 1) {
                                    $scope.modalInstanceLoad.close();
                                    $scope.SHGmodal = true;
                                } else {
                                    if ($window.sessionStorage.previous == '/shg/create') {
                                        $window.sessionStorage.shgMemberId = memResp.id;
                                        $scope.modalInstanceLoad.close();
                                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                        console.log('reloading...');

                                        setInterval(function () {
                                            $location.path($window.sessionStorage.previous);
                                            window.location = $window.sessionStorage.previous;
                                        }, 350);

                                    } else if ($window.sessionStorage.previous == '/shg/edit/:id') {
                                        $window.sessionStorage.shgMemberId = memResp.id;
                                        $scope.modalInstanceLoad.close();
                                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                        console.log('reloading...');

                                        setInterval(function () {
                                            $location.path('/shg/edit/' + $window.sessionStorage.previousRouteparamsId);
                                            window.location = "/shg/edit/" + $window.sessionStorage.previousRouteparamsId;
                                        }, 350);
                                    }
                                }
                            });
                        }

                    } else {

                        if (memResp.shgId == "" || memResp.shgId == null || memResp.shgId == 0 || memResp.shgId == '0') {

                            $scope.createCount++;
                            $scope.hhidcount++;
                            $scope.createHH(parId, HHid, associatedHF);

                            if ($scope.createCount == $scope.memberlists.length) {
                                $scope.modalInstanceLoad.close();
                                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                console.log('reloading...');

                                setInterval(function () {
                                    window.location = "/household-list";
                                }, 350);
                            }
                        } else {

                            Restangular.one('shgs', memResp.shgId).get().then(function (shg) {

                                $scope.shgbelongs = shg.membersInGroup;

                                if ($scope.shgbelongs === null || $scope.shgbelongs === 'null') {
                                    $scope.shgbelongs = "";
                                }

                                $scope.shgUpdate = {
                                    id: shg.id,
                                    membersInGroup: ""
                                }

                                if ($scope.shgbelongs === "") {
                                    $scope.shgUpdate.membersInGroup = memResp.id;
                                } else {
                                    $scope.shgUpdate.membersInGroup = shg.membersInGroup + "," + memResp.id;

                                    var array = $scope.shgUpdate.membersInGroup.split(",");

                                    array.sort(function (a, b) {
                                        return a - b
                                    });

                                    var uniqueArray = [array[0]];

                                    for (var i = 1; i < array.length; i++) {
                                        if (array[i - 1] !== array[i])
                                            uniqueArray.push(array[i]);
                                    }

                                    $scope.shgUpdate.membersInGroup = uniqueArray.toString();
                                }

                                Restangular.one('shgs', $scope.shgUpdate.id).customPUT($scope.shgUpdate).then(function (shgResp) {
                                    // console.log('shgResp', shgResp);

                                    $scope.createCount++;
                                    $scope.hhidcount++;
                                    $scope.createHH(parId, HHid, associatedHF);

                                    if ($scope.createCount == $scope.memberlists.length) {
                                        $scope.modalInstanceLoad.close();
                                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                        console.log('reloading...');

                                        setInterval(function () {
                                            window.location = "/household-list";
                                        }, 350);
                                    }
                                });
                            });
                        }
                    }
                });
            }
        };

        $scope.clickRadio = function (id, name) {
            $window.sessionStorage.shgMemberId = id;
        };

        $scope.ConfirmSHGMember = function () {
            $scope.toggleLoading();
            //  console.log('$window.sessionStorage.shgMemberId', $window.sessionStorage.shgMemberId);

            if ($window.sessionStorage.previous == '/shg/create') {
                $scope.SHGmodal = false;
                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setInterval(function () {
                    $location.path($window.sessionStorage.previous);
                    window.location = $window.sessionStorage.previous;
                }, 350);

            } else if ($window.sessionStorage.previous == '/shg/edit/:id') {
                $scope.SHGmodal = false;
                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setInterval(function () {
                    $location.path('/shg/edit/' + $window.sessionStorage.previousRouteparamsId);
                    window.location = "/shg/edit/" + $window.sessionStorage.previousRouteparamsId;
                }, 350);
            }
        };

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }

        $scope.validationStateId = $window.sessionStorage.zoneId;
        $scope.isCreateView = true;
        $scope.isCreateEdit = true;
        $scope.isCreateSave = true;
        $scope.hideremove = false;
        $scope.DisableSubmit = false;
        $scope.ShowText = false;
        $scope.ShowStart = true;
        $scope.disableapprove = true;
        $scope.hotspotprint = true;
        $scope.createClicked = false;
        $scope.detailsHide = false;
        $scope.createmodifyDisable = true;
        $scope.detailsHide2 = false;
        $scope.showMoreDetails2 = false;
        $scope.hideavahanid = true;
        $scope.actpaid = false;
        $scope.familydeletebuttonhide = true;
        $scope.paralegalproposed_yes = true;
        $scope.boardmemberhide = true;
        $scope.phonetype_disabled = false;
        $scope.beneficiary = {
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.userId,
            lastmodifiedtime: new Date(),
            createddatetime: new Date(),
            district: $window.sessionStorage.salesAreaId,
            deleteflag: false,
            phonetype: 1,
            nooftermsheld: 1
        };
        $scope.maritalstatuses = Restangular.all('maritalstatuses').getList().$object;
        $scope.typologies = Restangular.all('typologies').getList().$object;

        $scope.genders = Restangular.all('genders?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        Restangular.all('genders?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (gendone) {
            $scope.gendersone = gendone;
        });

        Restangular.all('occupations?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (occptn) {
            $scope.occupations = occptn;
        });

        Restangular.all('castes?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (cste) {
            $scope.castes = cste;
            if ($scope.UserLanguage == 1) {
                $scope.member.caste = cste[0].id;
            } else {
                $scope.member.caste = cste[0].parentId;
            }
        });

        Restangular.all('migrants?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mgnt) {
            $scope.migrants = mgnt;

            if ($scope.UserLanguage == 1) {
                $scope.member.migrant = mgnt[1].id;
            } else {
                var data = $scope.migrants.filter(function (arr) {
                    return arr.parentId == 2
                })[0];

                $scope.member.migrant = data.parentId;
                console.log('$scope.member.migrant', $scope.member.migrant);
            }
        });

        Restangular.all('religions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rlgn) {
            $scope.religions = rlgn;
            if ($scope.UserLanguage == 1) {
                $scope.member.religion = rlgn[0].id;
            } else {
                $scope.member.religion = rlgn[0].parentId;
            }
        });

        Restangular.all('bplStatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (bpl) {
            $scope.bplStatuses = bpl;
            if ($scope.UserLanguage == 1) {
                $scope.member.bplStatus = bpl[1].id;
            } else {
                $scope.member.bplStatus = bpl[1].parentId;
            }
        });

        Restangular.all('relationWithHHs?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rltion) {
            $scope.relationWithHHs = rltion;
        });

        $scope.literacyLevels = Restangular.all('literacyLevels?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        Restangular.all('nameOfSchools?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (nOfschls) {
            $scope.nameOfSchools = nOfschls;
        });

        Restangular.all('nameOfFactories?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (nOffacts) {
            $scope.nameOfFactories = nOffacts;
        });

        $scope.shgs = Restangular.all('shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().$object;

        //  $scope.shgs = Restangular.all('shgs?filter[where][deleteFlag]=false').getList().$object;

        $scope.studyingin = Restangular.all('educations').getList().$object;
        $scope.towns = Restangular.all('cities?filter[where][district]=' + $window.sessionStorage.salesAreaId).getList().$object;
        $scope.hotspots = Restangular.all('hotspots?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().$object;
        //$scope.noofterms = Restangular.all('noofterms').getList().$object;
        //$scope.phonetypes = Restangular.all('phonetypes').getList().$object;
        Restangular.all('noofterms').getList().then(function (response) {
            $scope.noofterms = response[0];
            $scope.nooftermsArray = [];
            $scope.nooftermsArray = response[0].name;
            //$scope.noofterms.splice(response[0], 1);
            //  console.log('$scope.noofterms', $scope.nooftermsArray);
            var str = response[0].name;
            var arr = str.split('-');
            //            console.log('Arr', arr)
            //            console.log('Arr', arr[0])
            //            console.log('Arr', arr[1])
            var a1 = parseInt(arr[0]);
            var a2 = parseInt(arr[1]);
            $scope.newnoofterms = [];
            for (var i = a1; i <= a2; i++) {
                $scope.newnoofterms.push(i);
                //console.log('$scope.newnoofterms',$scope.newnoofterms)
            }
        });
        Restangular.all('phonetypes').getList().then(function (response) {
            $scope.phonetypes = response;
            $scope.beneficiary.phonetype = 1;
        });
        $scope.auditlog = {
            description: 'Member Create',
            modifiedbyroleid: $window.sessionStorage.roleId,
            modifiedby: $window.sessionStorage.userId,
            lastmodifiedtime: new Date(),
            entityroleid: 55,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            facility: $window.sessionStorage.coorgId
        };
        $scope.MemCount = {};
        $scope.reportincident = {};
        $scope.beneficiary.dynamicmember = false;
        $scope.beneficiary.plhiv = false;
        $scope.beneficiary.paidmember = false;
        $scope.beneficiary.physicallydisabled = false;
        $scope.beneficiary.mentallydisabled = false;
        $scope.beneficiary.championapproved = false;
        $scope.beneficiary.championproposed = false;
        $scope.beneficiary.paralegalproposed = false;
        $scope.beneficiary.paralegaltrained = false;
        $scope.beneficiary.boardmember = false;
        $scope.beneficiary.paralegalhasplv = false;
        /*$scope.newArray = [{ 
                		age: '',
                		gender: '',
                		Stuyding: null,
                		dob: null,
                		beneficiaryid: 0,
                		deleteflag: false
                }];
        	
                $scope.addFamilyMember12345 = function () {
                	console.log('PUSHING.......');
                	$scope.newArray.push({
                		age: '',
                		gender: '',
                		Stuyding: null,
                		dob: null,
                		beneficiaryid: 0,
                		deleteflag: false
                		});
                	};*/
        /*
        		 $scope.showMoreDetails = true;
                $scope.callAPI = true;
                $scope.MoreDetails = function () {
                    $scope.showMoreDetails = !$scope.showMoreDetails;
                     if ($scope.showMoreDetails == false && $scope.callAPI == true) {
                        $scope.callAPI = false;
                        $scope.hotspots = Restangular.all('hotspots?filter[where][state]=' +$window.sessionStorage.zoneId +'&filter[where][district]=' +$window.sessionStorage.salesAreaId +'&filter[where][facility]=' +$window.sessionStorage.coorgId).getList().$object;
                    }
                };*/
        $scope.fwsitehide = true;
        $scope.cosite = true;
        Restangular.one('zones', $window.sessionStorage.zoneId).get().then(function (zone) {
            $scope.zoneName = zone.name;
            $scope.beneficiary.state = zone.id;
            $scope.stateId = zone.code;
            //$scope.SequenceNumber = zone.membercount;
            //$scope.MemCount.membercount = +(zone.membercount) + 1;
            //Restangular.one('zones', zone.id).customPUT($scope.MemCount).then(function () {});
            if ($window.sessionStorage.roleId == 5) {
                $scope.beneficiary.lastmodifiedbyrole = 5;
                $scope.disableapprove = false;
                $scope.fwsitehide = true;
                $scope.cosite = false;
                Restangular.one('comembers', $window.sessionStorage.userId).get().then(function (comember) {
                    $scope.beneficiary.facilityId = comember.id;
                    $scope.auditlog.facilityId = comember.id;
                    Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {
                        $scope.facilityName = employee.firstName;
                        //  $scope.beneficiary.avahanid = $scope.stateId + employee.salesCode + $scope.SequenceNumber;
                        /*Restangular.all('routelinks?filter[where][facility]=' + employee.id).getList().then(function (linkresp) {

                        	$scope.arrlink = [];
                        	for (var i = 0; i <= linkresp.length; i++) {
                        		$scope.arrlink.push(linkresp[i].partnerId);
                        		$scope.arrlink.toString();
                        		
                        	}
                        	$scope.sites13 = Restangular.all('distribution-routes?filter={"where":{"and":[{"id":{"inq":[' + $scope.arrlink + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function(rlinl){
                        	$scope.sites = rlinl;
                        		console.log('arrlink', rlinl);
                        });
                        });*/
                        $scope.sites = Restangular.all('distribution-routes?filter[where][partnerId]=' + employee.id).getList().$object;
                    });
                    $scope.$watch('beneficiary.site', function (newValue, oldValue) {
                        if (newValue === oldValue || newValue == '' || newValue == null) {
                            return;
                        } else Restangular.all('routelinks?filter[where][distributionRouteId]=' + newValue).getList().then(function (routeResponse) {
                            Restangular.one('fieldworkers', routeResponse[0].partnerId).get().then(function (fwget) {
                                $scope.beneficiary.fieldworkername = fwget.firstname;
                                $scope.beneficiary.fieldworker = fwget.id;
                            });
                        });
                    });
                    //$scope.modalInstanceLoad.close();
                });
            } else if ($window.sessionStorage.roleId == 15) {
                $scope.beneficiary.lastmodifiedbyrole = 15;
                $scope.fwsitehide = true;
                $scope.cosite = false;
                Restangular.one('comembers', $window.sessionStorage.userId).get().then(function (comember) {
                    $scope.beneficiary.facilityId = comember.id;
                    $scope.auditlog.facilityId = comember.id;
                    Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {
                        //$scope.beneficiary.avahanid = $scope.stateId + employee.salesCode + $scope.SequenceNumber;
                        $scope.facilityName = employee.firstName;
                    });
                    $scope.sites = Restangular.all('distribution-routes?filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().$object;
                    $scope.$watch('beneficiary.site', function (newValue, oldValue) {
                        if (newValue === oldValue || newValue == '' || newValue == null) {
                            return;
                        } else Restangular.all('routelinks?filter[where][distributionRouteId]=' + newValue).getList().then(function (routeResponse) {
                            Restangular.one('fieldworkers', routeResponse[0].partnerId).get().then(function (fwget) {
                                //console.log('fwget', fwget);
                                $scope.beneficiary.fieldworkername = fwget.firstname;
                                $scope.beneficiary.fieldworker = fwget.id;
                            });
                        });
                    });
                    $scope.modalInstanceLoad.close();
                });
            } else {
                $scope.beneficiary.lastmodifiedbyrole = 6;
                $scope.fwsitehide = false;
                $scope.cosite = true;
                $scope.disableapprove = true;
                Restangular.one('fieldworkers', $window.sessionStorage.userId).get().then(function (fw) {
                    $scope.getsiteid = fw.id;
                    $scope.beneficiary.fieldworkername = fw.firstname;
                    $scope.beneficiary.fieldworker = fw.id;
                    Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                        $scope.beneficiary.facilityId = comember.id;
                        $scope.auditlog.facilityId = comember.id;
                        Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {
                            $scope.facilityName = employee.firstName;
                            // $scope.beneficiary.avahanid = $scope.stateId + employee.salesCode + $scope.SequenceNumber;
                            Restangular.all('routelinks?filter[where][partnerId]=' + $scope.getsiteid).getList().then(function (routeResponse) {
                                $scope.routelinksget = routeResponse;
                                angular.forEach($scope.routelinksget, function (member, index) {
                                    member.index = index + 1;
                                    member.routeName = Restangular.one('distribution-routes', member.distributionRouteId).get().$object;
                                });
                            });
                        });
                        $scope.modalInstanceLoad.close();
                    });
                });
            }
        });
        /******************************* ADD FAMILY MEMBER *****************/

        $scope.DisableGender = false;

        $scope.Hideprintfamiliymember = true;
        $scope.reportincidents = [];
        $scope.healths = [];
        $scope.outreachactivities = [];
        $scope.socialprotections = [];
        $scope.finances = [];
        $scope.UpdateZone = {
            membercount: 0
        };
        $scope.beneficiarydataModal = false;
        $scope.submitcount = 0;
        $scope.validatestring = '';
        $scope.no = {};
        $scope.newhotspot = {};
        $scope.newhotspot.facility = $window.sessionStorage.coorgId;
        $scope.newhotspot.state = $window.sessionStorage.zoneId;
        $scope.newhotspot.district = $window.sessionStorage.salesAreaId;

        $scope.callModal = function () {
            $scope.beneficiarydataModal = !$scope.beneficiarydataModal;
        }

        $scope.OKBUTTON = function () {
            $scope.beneficiarydataModal = !$scope.beneficiarydataModal;
            //$scope.modalToDo.close();
            // window.location = '/members';
        };

        $scope.openOneAlert = function () {
            $scope.modalOneAlert = $modal.open({
                animation: true,
                templateUrl: 'template/AlertModal.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                size: 'sm',
                windowClass: 'modal-danger'
            });
        };

        $scope.okAlert = function () {
            $scope.modalOneAlert.close();
        };

        $scope.dobcount = 0;

        $scope.$watch('member.aadharNumberParsed', function (newValue, oldValue) {
            if (newValue.indexOf('x') == -1) {
                $scope.member.aadharNumber = angular.copy($scope.member.aadharNumberParsed);
            }
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else if (newValue.length < 12) {
                return;
            } else {
                $scope.member.aadharNumberParsed = new Array(newValue.length - 3).join('x') + newValue.substr(newValue.length - 4, 4);
            }

        });

        $scope.$watch('member.dob', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                $scope.dobcount++;
                $scope.today = new Date();
                $scope.birthyear = newValue;
                var ynew = $scope.today.getFullYear();
                var mnew = $scope.today.getMonth();
                var dnew = $scope.today.getDate();
                var yold = $scope.birthyear.getFullYear();
                var mold = $scope.birthyear.getMonth();
                var dold = $scope.birthyear.getDate();
                var diff = ynew - yold;
                if (mold > mnew) diff--;
                else {
                    if (mold == mnew) {
                        if (dold > dnew) diff--;
                    }
                }
                $scope.member.age = diff;
            }
        });

        $scope.$watch('member.age', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue >= 0 && newValue <= 99) {
                    document.getElementById('age').style.borderColor = "";
                    if ($scope.dobcount == 0) {
                        var todaydate = new Date();
                        var newdate = new Date(todaydate);
                        newdate.setFullYear(newdate.getFullYear() - newValue);
                        var nd = new Date(newdate);
                        $scope.member.dob = nd;
                    } else {
                        $scope.dobcount = 0;
                    }
                } else {
                    //alert('Invalid Age');
                    $scope.AlertMessage = $scope.invalid + ' ' + $scope.printage;
                    $scope.member.dob = null;
                    $scope.member.age = null;
                }
            }
        });

        $scope.$watch('member.migrant', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if (newValue == 1) {
                // $scope.MigrantFunc = 1;
                $scope.migrantdataModal = true;
            }
        });

        $scope.DisableMigrantBtn = true;

        $scope.newValue = function (MigrantFunc) {
            if (MigrantFunc == 'yes') {
                $scope.MigrantFunc = 1;
                $scope.DisableMigrantBtn = false;
            } else if (MigrantFunc == 'no') {
                $scope.MigrantFunc = 2;
                $scope.DisableMigrantBtn = false;
            }
        };

        $scope.ConfirmMigrant = function () {
            $scope.migrantdataModal = false;
            $scope.member.migrant = $scope.MigrantFunc;
        };

        $scope.CLOSEBUTTON = function () {
            $scope.migrantdataModal = false;
        };

        $scope.disableSHG = true;

        $scope.$watch('memberone.relation', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {

                if (newValue == 1 || newValue == 3 || newValue == 5 || newValue == 7 || newValue == 9 || newValue == 41) {
                    $scope.memberone.shgId = '';
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;

                } else if (newValue == 1 && $scope.memberone.age < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if (newValue == 3 && $scope.memberone.age < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if (newValue == 5 && $scope.memberone.age < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if (newValue == 7 && $scope.memberone.age < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if (newValue == 9 && $scope.memberone.age < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if (newValue == 41 && $scope.memberone.age < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else {
                    if ($location.$$url == '/shg/household/create') {
                        $scope.disableSHG = true;
                        $scope.memberone.shgId = '';
                    } else {
                        // $scope.disableSHG = false;
                        $scope.disableSHG = true;
                    }
                    // $scope.disableSHG = false;
                    $scope.memberone.gender = 2;
                }
            }
        });

        $scope.hideOccupation = true;
        $scope.hideFactory = true;

        $scope.$watch('memberone.age', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {

                if (newValue >= 0 && newValue <= 99) {
                    document.getElementById('ageone').style.borderColor = "";
                    if ($scope.dobcount == 0) {
                        var todaydate = new Date();
                        var newdate = new Date(todaydate);
                        newdate.setFullYear(newdate.getFullYear() - newValue);
                        var nd = new Date(newdate);
                        $scope.memberone.dob = nd;
                    } else {
                        $scope.dobcount = 0;
                    }
                } else {
                    //alert('Invalid Age');
                    $scope.AlertMessage = $scope.invalid + ' ' + $scope.printage;
                    $scope.memberone.dob = null;
                    $scope.memberone.age = null;
                }

                if ($scope.memberone.relation == 1 || $scope.memberone.relation == 3 || $scope.memberone.relation == 5 || $scope.memberone.relation == 7 || $scope.memberone.relation == 9 || $scope.memberone.relation == 41) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 1 && newValue < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 3 && newValue < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 5 && newValue < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 7 && newValue < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 9 && newValue < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 41 && newValue < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else {
                    if ($location.$$url == '/shg/household/create') {
                        $scope.disableSHG = true;
                        $scope.memberone.shgId = '';
                    } else {
                        //  $scope.disableSHG = false;
                        $scope.disableSHG = true;
                    }
                    // $scope.disableSHG = false;
                    $scope.memberone.gender = 2;
                }

                if (newValue >= 18) {
                    $scope.disableSchool = true;
                    $scope.memberone.nameOfSchool = '';
                } else {
                    $scope.disableSchool = false;
                }

                if (newValue >= 14) {
                    $scope.hideOccupation = false;
                } else {
                    $scope.hideOccupation = true;
                    $scope.memberone.occupation = '';
                    $scope.hideFactory = true;
                    $scope.memberone.nameOfFactory = '';
                }
            }
        });

        $scope.$watch('memberone.occupation', function (newValue, oldValue) {

            if (newValue == '' || newValue == null) {
                return;
            } else {

                if (newValue == 2) {
                    $scope.hideFactory = false;

                    if ($scope.UserLanguage == 1) {
                        $scope.memberone.nameOfFactory = $scope.nameOfFactories[0].id;
                    } else {
                        $scope.memberone.nameOfFactory = $scope.nameOfFactories[0].parentId;
                    }
                    // console.log('$scope.hideFactory', $scope.hideFactory);
                } else {
                    $scope.hideFactory = true;
                    $scope.memberone.nameOfFactory = '';
                }
                //console.log('$scope.hideFactory', $scope.hideFactory);
            }
        });

        $scope.dobcountone = 0;

        $scope.$watch('memberone.dob', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                $scope.dobcountone++;
                $scope.today = new Date();
                $scope.birthyear = newValue;
                var ynew = $scope.today.getFullYear();
                var mnew = $scope.today.getMonth();
                var dnew = $scope.today.getDate();
                var yold = $scope.birthyear.getFullYear();
                var mold = $scope.birthyear.getMonth();
                var dold = $scope.birthyear.getDate();
                var diff = ynew - yold;
                if (mold > mnew) diff--;
                else {
                    if (mold == mnew) {
                        if (dold > dnew) diff--;
                    }
                }
                $scope.memberone.age = diff;

                if ($scope.memberone.age <= 18) {
                    $scope.disableSchool = false;
                    $scope.disableSHG = true;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 1 || $scope.memberone.relation == 3 || $scope.memberone.relation == 5 || $scope.memberone.relation == 7 || $scope.memberone.relation == 9 || $scope.memberone.relation == 41) {
                    $scope.disableSHG = true;
                    $scope.memberone.shgId = '';
                } else {
                    $scope.disableSchool = true;
                    $scope.memberone.nameOfSchool = '';
                    // $scope.memberone.nameOfSchool = $scope.schoolVal;
                    // $scope.disableSHG = false;
                    if ($location.$$url == '/shg/household/create') {
                        $scope.disableSHG = true;
                        $scope.memberone.shgId = '';
                    } else {
                        //  $scope.disableSHG = false;
                        $scope.disableSHG = true;
                    }
                }

                if ($scope.memberone.age >= 14) {
                    $scope.hideOccupation = false;
                } else {
                    $scope.hideOccupation = true;
                    $scope.memberone.occupation = '';
                    $scope.hideFactory = true;
                    $scope.memberone.nameOfFactory = '';
                }
            }
        });

        $scope.dupCheckFlag = false;

        $scope.$watch('memberone.shgId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if ($scope.dupCheckFlag == true) {
                    for (var w = 0; w < $scope.memberlists.length; w++) {
                        if ($scope.memberlists[w].name == $scope.memberone.name && $scope.memberlists[w].age == $scope.memberone.age && $scope.memberlists[w].relation == $scope.memberone.relation) {
                            alert($scope.HHLanguage.sameMemMultipleSHG);
                            $scope.memberone = {};
                        }
                    }
                }
            }
        });

        /******************************google map****************************/
        $scope.LocateMe = function () {
            $scope.mapdataModal = true;

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 4,

                center: new google.maps.LatLng(12.9538477, 77.3507369),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            var marker, i;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(12.9538477, 77.3507369),
                map: map,
                html: ''
            });

            $scope.toggleMapModal();
        };

        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            ///////////////////////////////////////////////////////MAP//////////////////////////

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                //  console.log(latLng);
                $scope.member.latitude = latLng.lat();
                $scope.member.longitude = latLng.lng();

                //  console.log('$scope.updatepromotion', $scope.updatepromotion);

                document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
            }
            var map;

            function initialize() {

                $scope.latitude = 12.9538477;
                $scope.longitude = 77.3507369;
                navigator.geolocation.getCurrentPosition(function (location) {
                    //                    console.log(location.coords.latitude);
                    //                    console.log(location.coords.longitude);
                    //                    console.log(location.coords.accuracy);
                    $scope.latitude = location.coords.latitude;
                    $scope.longitude = location.coords.longitude;
                    //                });

                    // console.log('$scope.address', $scope.address);

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 4,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });
                });


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(10);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                //  console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };

        /********************************* FamilyMember Change **************/

        $scope.FamilyGenderChanges = false;
        $scope.CheckGender = function (scope, index, agee, famId) {
            $scope.FamilyGenderChanges = true;
            if (scope.familymember.gender >= 1 && scope.familymember.Age != undefined && scope.familymember.Age != '') {
                return;
            } else if (agee <= 0 || agee == '') {
                //$scope.AlertMessage = 'Enter Children Age';
                //$scope.openOneAlert();
            }
        }
        $scope.FamilyAgeChanges = false;
        $scope.ChilredAge = function (scope, index) {
            $scope.FamilyAgeChanges = true;
            if (scope.familymember.age >= 0 && scope.familymember.age <= 99) {
                if ($scope.dobcount == 0) {
                    var todaydate = new Date();
                    var newdate = new Date(todaydate);
                    newdate.setFullYear(newdate.getFullYear() - scope.familymember.age);
                    var nd = new Date(newdate);
                    $scope.familymembers[index].dob = nd;
                } else {
                    $scope.dobcount = 0;
                }
            } else {
                //alert('Invalid Age');
                $scope.AlertMessage = 'Invalid Age'; //'Select Date Of Birth';
                $scope.openOneAlert();
                $scope.familymember[index].age = null;
                $scope.familymember[index].dob = null;
            }
        };
        $scope.FamilyDOBChanges = false;
        $scope.ChilredDOB = function (scope, index) {
            //console.log('scope', scope);
            $scope.FamilyDOBChanges = true;
            $scope.dobcount++;
            $scope.today = new Date();
            $scope.birthyear = scope.familymember.dob;
            var ynew = $scope.today.getFullYear();
            var mnew = $scope.today.getMonth();
            var dnew = $scope.today.getDate();
            var yold = $scope.birthyear.getFullYear();
            var mold = $scope.birthyear.getMonth();
            var dold = $scope.birthyear.getDate();
            var diff = ynew - yold;
            if (mold > mnew) diff--;
            else {
                if (mold == mnew) {
                    if (dold > dnew) diff--;
                }
            }
            $scope.familymembers[index].age = diff;
        }
        $scope.FamilyStudyChanges = false;
        $scope.CheckStudyIn = function (idstd) {
            $scope.FamilyStudyChanges = true;
        }
        // console.log('$scope.familymembers', $scope.familymembers);
        $scope.familymembers = [];
        $scope.familymembers = [{
            age: '',
            gender: '',
            Stuyding: null,
            dob: null,
            beneficiaryid: 0,
            deleteflag: false
  }];
        //$scope.newArray = [];
        $scope.checkAddFamily = false;
        $scope.addFamilyMember = function () {
            $scope.checkAddFamily = true;
            $scope.familymembers.push({
                age: '',
                gender: '',
                Stuyding: null,
                dob: null,
                beneficiaryid: 0,
                deleteflag: false
            });
            $scope.totalfamilymembercount = $scope.familymembers.length;
        };
        $scope.totalfamilymembercount = $scope.familymembers.length;
        $scope.removeFamilyMember = function (index) {
            var item = $scope.familymembers[index];
            if (index == 0) {
                //alert('Can not Delete..!');
                $scope.AlertMessage = $scope.cannot + ' ' + $scope.deletebutton; //'Select Date Of Birth';
                $scope.openOneAlert();
            } else {
                $scope.familymembers.splice(index, 1);
            }
        };
        /**********************************************/

        $scope.HideMonthofDetection = true;
        $scope.$watch('beneficiary.plhiv', function (newValue, oldValue) {
            //console.log('newValue', newValue);
            if (newValue === oldValue) {
                return;
            } else if (newValue === true) {
                $scope.HideMonthofDetection = false;
            } else {
                $scope.HideMonthofDetection = true;
            }
        });
        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};
        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };
        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };
        $scope.openfamilydob = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#familydobopen' + index).focus();
            });
            item.familydobopened = true;
        };
        $scope.openmod = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickermod' + index).focus();
            });
            $scope.mod.openedmod = true;
        };
        $scope.openstart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };
        $scope.incidentopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerincident' + index).focus();
            });
            $scope.incident.incidentopened = true;
        };
        $scope.healthopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerhealth' + index).focus();
            });
            $scope.hlth.healthopened = true;
        };
        $scope.opendatestartedatart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdatestartedatart' + index).focus();
            });
            $scope.datestartedart.openeddatestartedatart = true;
        };
        $scope.lasttestdateopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.lasttest.lasttestdateopened = true;
        };
        $scope.termstartdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termstartdate.dobopened = true;
        };
        $scope.termenddate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termenddate.dobopened = true;
        };
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];
        //Datepicker settings end
        /****************************** Language ***************************************/
        $scope.UserLanguage = $window.sessionStorage.language;
        $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.divbasic = langResponse.divbasic;
            $scope.printfullname = langResponse.fullname;
            $scope.printnickname = langResponse.nickname;
            $scope.printage = langResponse.age;
            $scope.printdob = langResponse.dob;
            $scope.printmaritalstatus = langResponse.maritalstatus;
            $scope.printphonenumber = langResponse.phonenumber;
            $scope.anotherphonenumber = langResponse.anotherphonenumber;
            $scope.heading = langResponse.heading;
            $scope.mandatoryfield = langResponse.mandatoryfield;
            $scope.divextended = langResponse.divextended;
            $scope.printgender = langResponse.gender;
            $scope.printtypology = langResponse.typology;
            $scope.printmyplhiv = langResponse.plhivprint;
            $scope.printmonthofdetection = langResponse.monthofdetection;
            $scope.printstartsexwork = langResponse.startsexwork;
            $scope.printphysicaldisable = langResponse.physicaldisable;
            $scope.printmentaldisable = langResponse.mentaldisable;
            $scope.divfacility = langResponse.divfacility;
            $scope.printstate = langResponse.state;
            $scope.printfacility = langResponse.facility;
            $scope.printsite = langResponse.site;
            $scope.printfw = langResponse.fw;
            $scope.photspot = langResponse.hotspot;
            $scope.printtown = langResponse.town;
            $scope.printavahanid = langResponse.avahanid;
            $scope.printtiid = langResponse.tiid;
            $scope.divmembername = langResponse.divmembername;
            $scope.printmydynamicmember = langResponse.dynamicmember;
            $scope.printmypaidmember = langResponse.paidmember;
            $scope.printchampion = langResponse.champion;
            $scope.proposed = langResponse.proposed;
            $scope.approved = langResponse.approved;
            $scope.create = langResponse.create;
            $scope.update = langResponse.update;
            $scope.cancel = langResponse.cancel;
            $scope.deletebutton = langResponse.delete;
            $scope.printmemberlist = langResponse.memberlist;
            $scope.memberregistration = langResponse.memberregistration;
            $scope.yes = langResponse.yes;
            $scope.nomessage = langResponse.no;
            $scope.ok = langResponse.ok;
            $scope.printgroupname = langResponse.groupname;
            $scope.printfamiliymember = langResponse.familiymember;
            $scope.printname = langResponse.name;
            $scope.action = langResponse.action;
            $scope.addbutton = langResponse.addbutton;
            $scope.removebutton = langResponse.removebutton;
            $scope.printstudyingin = langResponse.studyingin;
            $scope.printphone = langResponse.phone;
            $scope.trained = langResponse.trained;
            $scope.printphone = langResponse.phone;
            $scope.trained = langResponse.trained;
            $scope.paralegalvolunteer = langResponse.paralegalvolunteer;
            $scope.boardmemberever = langResponse.boardmemberever;
            $scope.hasplvidcard = langResponse.hasplvidcard;
            $scope.startdate = langResponse.startdate;
            $scope.emddate = langResponse.emddate;
            $scope.nooftermsheld = langResponse.nooftermsheld;
            $scope.printdynamicmemberlabel = langResponse.dynamicmember;
            $scope.pchildren = langResponse.children;
            $scope.please = langResponse.please;
            $scope.enter = langResponse.enter;
            $scope.your = langResponse.your;
            $scope.invalid = langResponse.invalid;
            $scope.cannot = langResponse.cannot;
            $scope.difference = langResponse.difference;
            $scope.nameconsistword = langResponse.nameconsistword
            $scope.diffrentage = langResponse.difference;
            $scope.select = langResponse.select;
            $scope.onlytwono = langResponse.onlytwono
            $scope.nonumtoremove = langResponse.nonumtoremove;
            //$scope.title1 = langResponse.alert; = 
            //suman
            $scope.selage = langResponse.selage;
            $scope.peyfullname = langResponse.peyfullname;
            $scope.Seldob = langResponse.Seldob;
            $scope.selfacility = langResponse.selfacility;
            $scope.selmaritalstatus = langResponse.selmaritalstatus;
            $scope.selphonetype = langResponse.selphonetype;
            $scope.seltypology = langResponse.seltypology;
            $scope.selgender = langResponse.selgender;
            $scope.selstate = langResponse.selstate;
            $scope.selsite = langResponse.selsite;
            $scope.selfw = langResponse.selfw;
            $scope.enterhotspot = langResponse.enterhotspot;
            $scope.seltown = langResponse.seltown;
            $scope.enternoofterm = langResponse.enternoofterm;
            $scope.enterchildage = langResponse.enterchildage;
            $scope.selchilddob = langResponse.selchilddob;
            $scope.enterchildgender = langResponse.enterchildgender;
        });
    })
    /********************************************************/
    .directive('modal1', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-lg">' + '<div class="modal-content">' + '<div class="modal-header">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="modal-body" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    })
    /******ravi*******/
    .directive('benefmodal1', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static" data-keyboard="false">' + '<div class="modal-dialog modal-lg">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    })

    /******ravi*******/
    .directive('migrantmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-md">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    })
    /*******shg modal*********************/
    .directive('shgmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-md">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    })

    .directive('mapmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-lg">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });
