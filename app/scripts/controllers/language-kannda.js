'use strict';

angular.module('secondarySalesApp')
	.controller('kanndaCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
		$scope.isCreateSave = true;
		$scope.heading = 'Multi-Language';

		$scope.multilanguage = {
			languageId: 3
		};

		$scope.submitmultilanguages = Restangular.all('multilanguages').getList().$object;

		$scope.SaveLanguage = function () {
			$scope.submitmultilanguages.customPUT($scope.multilanguage).then(function (response) {
				console.log('Response', response);
				window.location = '/';
			});
		};

		Restangular.one('multilanguages', 3).get().then(function (zone) {
			$scope.original = zone;
			$scope.multilanguage = Restangular.copy($scope.original);
			$scope.modalInstanceLoad.close();
		});

		$scope.LanguageName = 'ಕನ್ನಡ';
		$scope.Language = 'ಭಾಷಾ';
	});
