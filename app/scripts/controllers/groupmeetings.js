'use strict';
angular.module('secondarySalesApp').controller('GroupMeetingsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter, $modal) {
    /*********/
    if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        window.location = "/";
    }
    $scope.addedtodos = [];
    $scope.groupcreate = true;
    $scope.showForm = function () {
        var visible = $location.path() === '/groupmeetings/create' || $location.path() === '/groupmeetings/' + $routeParams.id;
        return visible;
    };
    $scope.isCreateView = function () {
        if ($scope.showForm()) {
            var visible = $location.path() === '/groupmeetings/create';
            return visible;
        }
    };
    $scope.hideCreateButton = function () {
        var visible = $location.path() === '/groupmeetings/create' || $location.path() === '/groupmeetings/' + $routeParams.id;
        return visible;
    };
    $scope.hideSearchFilter = function () {
        var visible = $location.path() === '/groupmeetings/create' || $location.path() === '/groupmeetings/' + $routeParams.id;
        return visible;
    };
    /**********************************Pagination******************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    if ($window.sessionStorage.prviousLocation != "partials/groupmeetings") {
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
    };
    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    /************************************************************************/
    $scope.DisableFollowup = false;
    $scope.printnone = 'None';
    $scope.auditlog = {
        description: 'Group Meeting Create',
        modifiedbyroleid: $window.sessionStorage.roleId,
        modifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        entityroleid: 37,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId
    };
    $scope.groupmeeting = {
        attendees: [],
        follow: [],
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        deleteflag: false
    };
    /************************************************* INDEX *******************************************/
    /* $scope.GNameFilter = function () {
          $scope.backgroundCol1 = "blue";
          $scope.backgroundCol2 = "";
          $scope.fws = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fws) {
              $scope.fieldworkers = fws;
              //console.log('fieldworkers', fws.length);
              $scope.dr = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + $window.sessionStorage.UserEmployeeId + ',41]}}}').getList().then(function (dr) {
                  $scope.Organisedcomembers = dr;
                  $scope.zn = Restangular.all('groupmeetings?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=name%20ASC').getList().then(function (zn) {
                      $scope.groupmeetings = zn;
                      $scope.modalInstanceLoad.close();
                      angular.forEach($scope.groupmeetings, function (member, index) {
                          member.index = index + 1;
                          member.pillars = $scope.getPilar(member.pillarid);
                          member.meetingtopics = $scope.getGroupMeetingTopics(member.topicsdiscussed);
                          member.attendeeslist = $scope.getGroupMeetingMember(member.attendeeslist);
                          for (var m = 0; m < $scope.fieldworkers.length; m++) {
                              if (member.organisedby == $scope.fieldworkers[m].id) {
                                  member.Fieldworkername = $scope.fieldworkers[m];
                                  break;
                              }
                          }
                          for (var n = 0; n < $scope.Organisedcomembers.length; n++) {
                              if (member.organisedby == $scope.Organisedcomembers[n].id) {
                                  member.Coname = $scope.Organisedcomembers[n];
                                  break;
                              }
                          }
                          member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                          $scope.TotalData = [];
                          $scope.TotalData.push(member);
                      });
                  });
              });
          });
      }
      $scope.DateFilter = function () {
          $scope.backgroundCol1 = "";
          $scope.backgroundCol2 = "blue";
          $scope.fws = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fws) {
              $scope.fieldworkers = fws;
              //console.log('fieldworkers', fws.length);
              $scope.dr = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + $window.sessionStorage.UserEmployeeId + ',41]}}}').getList().then(function (dr) {
                  $scope.Organisedcomembers = dr;
                  $scope.zn = Restangular.all('groupmeetings?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=datetime%20ASC').getList().then(function (zn) {
                      $scope.groupmeetings = zn;
                      $scope.modalInstanceLoad.close();
                      angular.forEach($scope.groupmeetings, function (member, index) {
                          member.index = index + 1;
                          member.pillars = $scope.getPilar(member.pillarid);
                          member.meetingtopics = $scope.getGroupMeetingTopics(member.topicsdiscussed);
                          member.attendeeslist = $scope.getGroupMeetingMember(member.attendeeslist);
                          for (var m = 0; m < $scope.fieldworkers.length; m++) {
                              if (member.organisedby == $scope.fieldworkers[m].id) {
                                  member.Fieldworkername = $scope.fieldworkers[m];
                                  break;
                              }
                          }
                          for (var n = 0; n < $scope.Organisedcomembers.length; n++) {
                              if (member.organisedby == $scope.Organisedcomembers[n].id) {
                                  member.Coname = $scope.Organisedcomembers[n];
                                  break;
                              }
                          }
                          member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                          $scope.TotalData = [];
                          $scope.TotalData.push(member);
                      });
                  });
              });
          });
      }*/

    /********************/
    /*$scope.sortingOrder = sortingOrder;
    $scope.reverse = false;

    $scope.sort_by = function (newSortingOrder) {
        if ($scope.sortingOrder == newSortingOrder)
            $scope.reverse = !$scope.reverse;

        $scope.sortingOrder = newSortingOrder;
};*/
	
	 $scope.sort = {
        active: '',
        descending: undefined
    }

    $scope.changeSorting = function (column) {

        var sort = $scope.sort;

        if (sort.active == column) {
            sort.descending = !sort.descending;

        } else {
            sort.active = column;
            sort.descending = false;
        }
    };

    $scope.getIcon = function (column) {

        var sort = $scope.sort;

        if (sort.active == column) {
            return sort.descending ? 'fa-sort-up' : 'fa-sort-desc';
        }
    }

    /*****************/


    $scope.todostatuses1 = Restangular.all('todostatuses?filter={"where": {"id": {"inq": [1,3]}}}').getList().then(function (responseseservity) {
        $scope.todostatuses = responseseservity;
        $scope.newtodo.status = 1;
    });
    $scope.fws = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fws) {
        $scope.fieldworkers = fws;
        //console.log('fieldworkers', fws.length);
        $scope.dr = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + $window.sessionStorage.UserEmployeeId + ',41]}}}').getList().then(function (dr) {
            $scope.Organisedcomembers = dr;
            $scope.zn = Restangular.all('groupmeetings?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=datetime%20ASC').getList().then(function (zn) {
                $scope.groupmeetings = zn;
                $scope.modalInstanceLoad.close();
                angular.forEach($scope.groupmeetings, function (member, index) {
                    member.index = index + 1;
                    member.pillars = $scope.getPilar(member.pillarid);
                    member.meetingtopics = $scope.getGroupMeetingTopics(member.topicsdiscussed);
                    member.attendeeslist = $scope.getGroupMeetingMember(member.attendeeslist);
                    member.filterDate = new Date(member.datetime.split("T")[0]);
                    for (var m = 0; m < $scope.fieldworkers.length; m++) {
                        if (member.organisedby == $scope.fieldworkers[m].id) {
                            member.Fieldworkername = $scope.fieldworkers[m];
                            break;
                        }
                    }
                    for (var n = 0; n < $scope.Organisedcomembers.length; n++) {
                        if (member.organisedby == $scope.Organisedcomembers[n].id) {
                            member.Coname = $scope.Organisedcomembers[n];
                            
                            break;
                        }
                    }
                    member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                    $scope.TotalData = [];
                    $scope.TotalData.push(member);
                });
            });
        });
    });
    $scope.getGroupMeetingTopics = function (partnerId) {
        return Restangular.all('groupmeetingtopics?filter={"where":{"id":{"inq":[' + partnerId + ']}}}').getList().$object;
    };
    $scope.getPilar = function (id) {
        return Restangular.all('pillars?filter={"where":{"id":{"inq":[' + id + ']}}}').getList().$object;
    };
    $scope.getGroupMeetingMember = function (id) {
        return Restangular.all('beneficiaries?filter={"where":{"id":{"inq":[' + id + ']}}}').getList().$object;
    };
    /******************************************* GET *******************************************/
    //$scope.submittodos = Restangular.all('todos').getList().$object
    $scope.UserLanguage = $window.sessionStorage.language;
    $scope.followupgroups = Restangular.all('followupgroups').getList().$object;
    $scope.zipmasters = Restangular.all('pillars').getList().then(function (zipmaster) {
        $scope.zipmasters = zipmaster;
        angular.forEach($scope.zipmasters, function (member, index) {
            member.total = null;
            member.loader = null;
            member.enabled = false;
        });
    });
    $scope.checkbox = [];
    $scope.eventcheckbox = [];
    $scope.mobilenumbers = [];
    $scope.GetMobNumbers = function (index, zipcode, event) {
            if ($scope.zipmasters[index].enabled == true) {
                $scope.zipmasters[index].loader = 'images/loginloader.gif';
                document.getElementById(index).style.visibility = "visible";
                $scope.surveyquestions = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + $scope.zipmasters[index].id).getList().then(function (surveyquestions) {
                    $scope.prospects = surveyquestions;
                    $scope.zipmasters[index].total = surveyquestions.length;
                    $scope.zipmasters[index].loader = null;
                    document.getElementById(index).style.visibility = "hidden";
                    for (var i = 0; i < surveyquestions.length; i++) {
                        surveyquestions[i].enabled = false;
                        $scope.mobilenumbers.push(surveyquestions[i]);
                    };
                });
            } else {
                $scope.zipmasters[index].total = null;
                $scope.zipmasters[index].loader = 'images/loginloader.gif';
                document.getElementById(index).style.visibility = "visible";
                $scope.surveyquestions = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + $scope.zipmasters[index].id).getList().then(function (surveyquestions) {
                    $scope.zipmasters[index].loader = null;
                    document.getElementById(index).style.visibility = "hidden";
                    for (var i = 0; i < surveyquestions.length; i++) {
                        $scope.mobilenumbers.splice($scope.mobilenumbers.indexOf(surveyquestions[i].name), 1);
                    };
                });
            }
        }
        /**************************************** Member *******************************************/
    if ($window.sessionStorage.roleId == 5) {
        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
            $scope.getuempid = $window.sessionStorage.UserEmployeeId;
            //$scope.comemberid = comember.id;
            $scope.groupmeeting.facilityId = comember.id;
            $scope.auditlog.facilityId = comember.id;
            //$scope.getfacilityId = comember.id;
        });
        $scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {
            $scope.partners = part1;
            angular.forEach($scope.partners, function (member, index) {
                member.index = index + 1;
            });
        });
    } else {
        Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
            $scope.fieldworkers = fw;
            Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                //$scope.comemberid = comember.id;
                $scope.groupmeeting.facilityId = comember.id;
                $scope.auditlog.facilityId = comember.id;
                //	$scope.getfacilityId = comember.id;
            });
            $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                $scope.partners = part2;
                angular.forEach($scope.partners, function (member, index) {
                    member.index = index + 1;
                });
            });
        });
    }
    /************************************************* MOdal ******************************/
    $scope.showschemesModal = false;
    $scope.toggleschemesModal = function () {
        $scope.SaveScheme = function () {
            $scope.newArray = [];
            $scope.groupmeeting.attendees = [];
            for (var i = 0; i < $scope.partners.length; i++) {
                if ($scope.partners[i].enabled == true) {
                    $scope.newArray.push($scope.partners[i].id);
                }
            };
            $scope.attendeestodo = $scope.groupmeeting.attendees;
            //console.log('$scope.attendeestodo',$scope.attendeestodo);
            $scope.groupmeeting.attendees = $scope.newArray;
            $scope.showschemesModal = !$scope.showschemesModal;
        };
        $scope.showschemesModal = !$scope.showschemesModal;
    };
    $scope.CancelScheme = function () {
        $scope.showschemesModal = !$scope.showschemesModal;
    };
    $scope.documenthide = true;
    $scope.$watch('groupmeeting.organisedby', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            if (newValue == 41) {
                $scope.documenthide = false;
            } else {
                $scope.documenthide = true;
            }
        }
    });
    $scope.CancelModal = function () {
        $scope.showschemesModal = !$scope.showschemesModal;
    }
    $scope.$watch('groupmeeting.attendees', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            $scope.attendeestodo = newValue;
            if (newValue.length > oldValue.length) {
                var Array1 = newValue;
                var Array2 = oldValue;
                for (var i = 0; i < Array2.length; i++) {
                    var arrlen = Array1.length;
                    for (var j = 0; j < arrlen; j++) {
                        if (Array2[i] == Array1[j]) {
                            Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                        }
                    }
                }
                for (var i = 0; i < $scope.partners.length; i++) {
                    if ($scope.partners[i].id == Array1[0]) {
                        $scope.partners[i].enabled = true;
                    }
                }
                // do stuff
            } else if (newValue.length < oldValue.length) {
                // something was removed
                var Array1 = oldValue;
                var Array2 = newValue;
                for (var i = 0; i < Array2.length; i++) {
                    var arrlen = Array1.length;
                    for (var j = 0; j < arrlen; j++) {
                        if (Array2[i] == Array1[j]) {
                            Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                        }
                    }
                }
                for (var i = 0; i < $scope.partners.length; i++) {
                    if ($scope.partners[i].id == Array1[0]) {
                        $scope.partners[i].enabled = false;
                    }
                }
            }
        }
    });
    $scope.groupmeeting.follow.push('None');
    $scope.showfollowupModal = false;
    $scope.$watch('groupmeeting.follow', function (newValue, oldValue) {
        $scope.newtodo = {};
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        $scope.mindt = new Date();
        $scope.newtodo.datetime = sevendays;
        $scope.newtodo.status = 1;
        $scope.oldFollowUp = oldValue;
        if (newValue === oldValue) {
            return;
        } else {
            $scope.attendeespurpose = newValue;
            $scope.SavetodoFollow = function () {
                $scope.newtodo.todotype = 32;
                $scope.newtodo.state = $window.sessionStorage.zoneId;
                $scope.newtodo.district = $window.sessionStorage.salesAreaId;
                $scope.newtodo.facility = $window.sessionStorage.coorgId;
                $scope.newtodo.facilityId = $scope.getfacilityId;
                $scope.newtodo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.newtodo.lastmodifiedtime = new Date();
                $scope.newtodo.purpose = $scope.printpurposeid;
                $scope.newtodo.roleId = $window.sessionStorage.roleId;
                //console.log('$scope.addedtodos',$scope.addedtodos);
                $scope.addedtodos.push($scope.newtodo);
                $scope.showfollowupModal = !$scope.showfollowupModal;
                for (var index = 0; index < $scope.followupgroups.length; index++) {
                    if ($scope.groupmeeting.follow[index] === 'None') {
                        $scope.groupmeeting.follow.splice(index, 1);
                    }
                }
            };
            var array3 = newValue.filter(function (obj) {
                return oldValue.indexOf(obj) == -1;
            });
            if (array3.length > 0) {
                for (var i = 0; i < $scope.followupgroups.length; i++) {
                    if ($scope.followupgroups[i].id == array3[0]) {
                        //$scope.printpurpose = $scope.followupgroups[i].name;
                        $scope.printpurpose = $scope.followupgroups[i];
                        $scope.printpurposeid = $scope.followupgroups[i].id;
                    }
                }
            }
            if (oldValue != undefined) {
                if (oldValue.length < newValue.length) {
                    $scope.showfollowupModal = !$scope.showfollowupModal;
                }
            } else {
                $scope.showfollowupModal = !$scope.showfollowupModal;
            }
        };
    });
    $scope.CancelFollow = function () {
        $scope.groupmeeting.follow = $scope.oldFollowUp;
        $scope.showfollowupModal = !$scope.showfollowupModal;
    };
    $scope.todocount = 0;
    $scope.todocount1 = 0;
    if ($scope.groupmeeting.follow == 'None') {
        //console.log('$scope.groupmeeting.followNone', $scope.groupmeeting.follow);
        $scope.SaveToDo = function () {
            $scope.addedtodocount = 0;
            $scope.addedtodocount1 = 0;
            for (var i = 0; i < $scope.addedtodos.length; i++) {
                $scope.addedtodos[i].beneficiaryid = $scope.attendeestodo[$scope.todocount1];
                //$scope.addedtodos[i].purpose = $scope.attendeespurpose[$scope.todocount1];
                $scope.addedtodos[i].reportincidentid = $scope.GroupMeetingId;
                //$scope.submittodos.post($scope.addedtodos[i]).then(function (resp) {
                Restangular.all('todos').post($scope.addedtodos[i]).then(function (resp) {
                    //console.log('resp', resp);
                    $scope.addedtodocount1++;
                    if ($scope.addedtodocount1 >= $scope.addedtodos.length) {
                        $scope.todocount1++;
                        if ($scope.todocount1 < $scope.attendeestodo.length) {
                            $scope.SaveToDo();
                        } else {
                            window.location = '/groupmeetings';
                        }
                    }
                });
            }
        };
        //window.location = '/groupmeetings';
    } else {
        window.location = '/groupmeetings';
    }
    $scope.groupmeetingdataModal = false;
    $scope.message = $scope.groupmhascreated;//'Group Meeting has been created!';
    $scope.modalTitle = $scope.thankyou;//'Thank You';
    $scope.validatestring = '';
    $scope.Savegroupmeeting = function () {
        $scope.groupmeetingdataModal = !$scope.groupmeetingdataModal;
        $scope.groupmeeting.attendeeslist = null;
        $scope.groupmeeting.followup = null;
        $scope.groupmeeting.pillarid = null;
        $scope.groupmeeting.topicsdiscussed = null;
        var checkedValue = null;
        var inputElements1 = document.getElementsByClassName('messageCheckbox');
        for (var i = 0; inputElements1[i]; ++i) {
            if (inputElements1[i].checked) {
                if (checkedValue == null) {
                    checkedValue = inputElements1[i].value;
                } else {
                    checkedValue = checkedValue + ',' + inputElements1[i].value;
                }
            }
        };
        $scope.groupmeeting.pillarid = checkedValue;
        var eventcheckedValue = null;
        var inputElements2 = document.getElementsByClassName('eventCheckbox');
        for (var i = 0; inputElements2[i]; ++i) {
            if (inputElements2[i].checked) {
                if (eventcheckedValue == null) {
                    eventcheckedValue = inputElements2[i].value;
                } else {
                    eventcheckedValue = eventcheckedValue + ',' + inputElements2[i].value;
                }
            }
        }
        $scope.groupmeeting.topicsdiscussed = eventcheckedValue;
        var checkedValue1 = null;
        var inputElements3 = document.getElementsByClassName('attendcheckbox');
        for (var i = 0; inputElements3[i]; ++i) {
            if (inputElements3[i].checked) {
                if (checkedValue1 == null) {
                    checkedValue1 = inputElements3[i].value;
                } else {
                    checkedValue1 = checkedValue1 + ',' + inputElements3[i].value;
                }
            }
        }
        $scope.groupmeeting.attendeeslist = checkedValue1;
        if ($scope.groupmeeting.follow != 'None') {
            //	$scope.groupmeeting.followup = null;
            //}	else {
            for (var i = 0; i < $scope.groupmeeting.follow.length; i++) {
                if (i == 0) {
                    $scope.groupmeeting.followup = $scope.groupmeeting.follow[i];
                } else {
                    $scope.groupmeeting.followup = $scope.groupmeeting.followup + ',' + $scope.groupmeeting.follow[i];
                }
            }
        } else {
            $scope.groupmeeting.followup = null;
        }
        if ($scope.groupmeeting.datetime == '' || $scope.groupmeeting.datetime == null) {
            $scope.validatestring = $scope.validatestring + $scope.seldate;
        } else if ($scope.groupmeeting.name == '' || $scope.groupmeeting.name == null) {
            $scope.validatestring = $scope.validatestring + $scope.entername;
        } else if (checkedValue === null) {
            $scope.validatestring = $scope.validatestring + $scope.selpillar;
        } else if (eventcheckedValue === null) {
            $scope.validatestring = $scope.validatestring + $scope.seltopic;
        } else if ($scope.groupmeeting.organisedby == '' || $scope.groupmeeting.organisedby == null) {
            $scope.validatestring = $scope.validatestring + $scope.selorgby;
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
            //$scope.groupmeetingdataModal = !$scope.groupmeetingdataModal;
        } else {
             $scope.openOneAlert();
            //$scope.submitgroupmeetings.post($scope.groupmeeting).then(function (resp) {
            Restangular.all('groupmeetings').post($scope.groupmeeting).then(function (resp) {
                //console.log('resp',resp);
                $scope.updatemember();
                $scope.auditlog.entityid = resp.id;
                var respdate = $filter('date')(resp.datetime, 'dd-MMMM-yyyy');
                $scope.auditlog.description = 'Group Meeting Created With Following Details:' + 'Name-' + resp.name + ' , ' + 'MemberId-' + resp.attendees + ' , ' + 'PillarId-' + resp.pillarid + ' , ' + 'TopicId-' + resp.topicsdiscussed + ' , ' + 'Organized By -' + resp.organisedby + ' , ' + 'Follow Up -' + resp.follow + ' , ' + 'Date -' + respdate;
                //$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                    if ($scope.groupmeeting.follow == 'None') {
                        window.location = '/groupmeetings';
                    }
                    $scope.GroupMeetingId = resp.id;
                    $scope.SaveToDo();
                });
            });
        };
    };
    $scope.updatemember = function () {
        $scope.memberupdate = {
            lastmeetingdate: new Date()
        }
        for (var m = 0; m < $scope.groupmeeting.attendees.length; m++) {
            //console.log('memlist[m]',$scope.groupmeeting.attendees[m]);
            Restangular.all('beneficiaries/' + $scope.groupmeeting.attendees[m]).customPUT($scope.memberupdate).then(function (responsemember) {
                //console.log('member update', responsemember);
            });
        }
    }
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    /************************************* Current& Future Date**********************************/
    var today = new Date();
    $scope.groupmeeting.datetime = new Date();
    var newdate = new Date();
    $scope.mindt = new Date();
    $scope.today = function () {
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
    };
    $scope.today();
    $scope.dtmax = new Date();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.opendob = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerdob' + index).focus();
        });
        $scope.picker.dobopened = true;
    };
    $scope.open = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerfollowup' + index).focus();
        });
        $scope.picker.followupopened = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };
    $scope.monthOptions = {
        formatYear: 'yyyy',
        startingDay: 1,
        minMode: 'month'
    };
    $scope.mode = 'month';
    $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.monthformats = ['MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.monthformat = $scope.monthformats[0];
    //Datepicker settings end
    /******************************* Language ********************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $rootScope.style = {
            "font-size": langResponse.fontsize
        }
        $scope.mandatoryfield1 = langResponse.mandatoryfield;
        $scope.date = langResponse.date;
        $scope.printname = langResponse.name;
        $scope.action = langResponse.action;
        $scope.searchmember = langResponse.searchmember;
        $scope.addnew = langResponse.addnew;
        $scope.show = langResponse.show;
        $scope.entry = langResponse.entry;
        $scope.headergroupmeeting = langResponse.headergroupmeeting;
        $scope.memberattendmeeting = langResponse.memberattendmeeting;
        $scope.organizedby = langResponse.organizedby;
        $scope.pillar = langResponse.pillar;
        $scope.searchfor = langResponse.searchfor;
        $scope.topics = langResponse.topics;
        $scope.followuprequired = langResponse.followuprequired;
        $scope.membername = langResponse.membername;
        $scope.select = langResponse.select;
        $scope.printstatus = langResponse.status;
        $scope.followupdate = langResponse.followupdate;
        $scope.topicdiscussed = langResponse.topicdiscussed;
        $scope.dateofclosure = langResponse.dateofclosure;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.savebutton = langResponse.savebutton;
        $scope.printothers = langResponse.others;
        $scope.searchmember = langResponse.searchmember;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
        
        //$scope.thankyou = langResponse.thankyou;
        //$scope.groupmhascreated = langResponse.groupmhascreated;
        $scope.groupmhasupdated = langResponse.groupmhasupdated;
        $scope.seldate = langResponse.seldate;
        $scope.entername = langResponse.entername;
        $scope.selpillar = langResponse.selpillar;
        $scope.seltopic = langResponse.seltopic;
        $scope.selorgby = langResponse.selorgby;
        $scope.message = langResponse.groupmhascreated;//'Group Meeting has been created!';
        $scope.modalTitle = langResponse.thankyou; 
        //$scope.searchmemberTitle = langResponse.searchmember;
        //$scope.followupmemberTitle = langResponse.followupmember;
        
    });
    
      $scope.openOneAlert = function () {
         console.log('Me Calling')
         $scope.modalOneAlert = $modal.open({
             animation: true
             , templateUrl: 'template/AlertModal.html'
             , scope: $scope
             , backdrop: 'static'
             , keyboard: false
             , size: 'sm'
             , windowClass: 'modal-sucess '
         });
     };
    
    if ($window.sessionStorage.language == 1) {
        $scope.searchmemberTitle = 'Search Member';
        $scope.followupmemberTitle = 'Follow Up Member';
    } else if ($window.sessionStorage.language == 2) {
        $scope.searchmemberTitle = 'मेम्बर खोजें';
        $scope.followupmemberTitle = 'ऊपर का सदस्य का पालन करें';
    } else if ($window.sessionStorage.language == 3) {
        $scope.searchmemberTitle = 'ಮೆಂಬರ್ ರನ್ನು ಶೋಧಿಸಿ';
        $scope.followupmemberTitle = 'ಮೆಂಬರ್  ಅನುಸರಣ';
    } else if ($window.sessionStorage.language == 4) {
        $scope.searchmemberTitle = 'மெம்பர் தேடல';
        $scope.followupmemberTitle = 'மெம்பர் பின்தொடர்';
    } else if ($window.sessionStorage.language == 5) {
        $scope.searchmemberTitle = 'శోధించబడుతున్న మెంబర';
        $scope.followupmemberTitle = 'సభ్యుని అనుసరించండ';
    } else if ($window.sessionStorage.language == 6) {
        $scope.searchmemberTitle = 'सदस्य शोधा';
        $scope.followupmemberTitle = 'प्रतिनिधी सदस्य अनुसरण करा';
    }
});
/************************************ Not In Use ****************************************
			
			$scope.submitgroupmeetings = Restangular.all('groupmeetings').getList().$object;
			$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;
			$scope.comembers = Restangular.all('comembers').getList().$object;

			$scope.Organisedcomembers = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + $window.sessionStorage.UserEmployeeId + ',41]}}}').getList().$object;
			
			
			
			$("#Search").keyup(function () {
				var data = this.value.toUpperCase().split(" ");
				var js = $("#bnbody").find("tr");
				if (this.value == "") {
					js.show();
					return;
				}
				js.hide();
				js.filter(function (i, v) {
					var $t = $(this);
					for (var d = 0; d < data.length; ++d) {
						if ($t.text().toUpperCase().indexOf(data[d]) > -1) {
							return true;
						}
					}
					return false;
				}).show();
			});


	
			if ($window.sessionStorage.roleId == 5) {
				console.log('CO Organisedby');
				$scope.getFW = function (partnerId) {
					if (partnerId == 41 || partnerId == $window.sessionStorage.UserEmployeeId) {
						return Restangular.one('comembers',partnerId).get().$object;
					} else {
						return Restangular.one('fieldworkers', partnerId).get().$object; //$scope.Organisedcomembers = 
					};
				};
			} else {
				console.log('fw Organisedby');
				$scope.getFW = function (partnerId) {
					if (partnerId == 41 || partnerId == $window.sessionStorage.UserEmployeeId) {
						return Restangular.one('comembers',partnerId).get().$object;
					} else {
						return Restangular.one('fieldworkers', partnerId).get().$object; //$scope.Organisedcomembers = 
					};
				};
				
			}
			
			
			
				$scope.getFW = function (partnerId) {
				return Restangular.one('fieldworkers', partnerId).get().$object;
			};

			$scope.getCO = function (partnerId) {
				return Restangular.one('fieldworkers', partnerId).get().$object;
			};


			$scope.groupmeetingothers = 'Others';

			$scope.getCO = function (partnerId) {
				return Restangular.one('comembers', partnerId).get().$object;
			};
			
			
	
		/*************************************************** Organised By **************************
		if ($window.sessionStorage.roleId == 5) {
			Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
				$scope.getuempid = $window.sessionStorage.UserEmployeeId;
				$scope.comemberid = comember.id;
				$scope.groupmeeting.facilityId = comember.id;
				$scope.auditlog.facilityId = comember.id;
				$scope.getfacilityId = comember.id;

				//console.log('CO Login', comember);
				
				Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fwrResponse) {
					$scope.fieldworkers = fwrResponse;
					console.log('CO Login', fwrResponse);
				});
			});
			$scope.Organisedcomembers = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + $window.sessionStorage.UserEmployeeId + ',41]}}}').getList().$object;
		} else {
			Restangular.all('fieldworkers?filter[where][id]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (fwrResponse2) {
				$scope.fieldworkers = fwrResponse2;
					console.log('FW Login', fwrResponse2);
			});
			Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fwrResponse3) {
				Restangular.one('comembers', fwrResponse3.facilityId).get().then(function (comember) {
					$scope.comemberid = comember.id;
					$scope.groupmeeting.facilityId = comember.id;
					$scope.auditlog.facilityId = comember.id;
					$scope.getfacilityId = comember.id;
				});
				$scope.getcoid = fwrResponse3.lastmodifiedby;
			//	$scope.Organisedcomembers = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + fwrResponse3.lastmodifiedby + ',41]}}}').getList().$object;
				$scope.getuempid = fwrResponse3.lastmodifiedby;
			});
			
		}
		
	
		*************************************************************************************/
