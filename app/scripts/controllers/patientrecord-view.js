'use strict';

angular.module('secondarySalesApp')
    .controller('PatientRecordListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {


        /************************ Language ************************/

    $rootScope.clickFW();
    
        Restangular.one('prlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.PRLanguage = langResponse[0];
            $scope.headingName = $scope.PRLanguage.houseHoldCreate;
            $scope.modalTitle = $scope.PRLanguage.thankYou;
            $scope.message = $scope.PRLanguage.thankYouCreated;
        });

        /************* Focus ***************************************/


        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.disableAssigned = true;
            $scope.hideAddBtn = false;

            $scope.patientrecordFilterCall = 'patientrecords?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}}]}}';


        } else {

            $scope.disableAssigned = false;
            $scope.hideAddBtn = true;

            $scope.patientrecordFilterCall = 'patientrecords?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}}]}}';
        }
        /************* Focus ***************************************/

        $scope.someFocusVariable = true;
        $scope.FocusMe = true;

        $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;

        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;

        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;

        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;

        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;

        $scope.roledsply = Restangular.all('roles').getList().$object;
        $scope.genderdsply = Restangular.all('genders?filter[where][deleteFlag]=false').getList().$object;
        $scope.migrantdsply = Restangular.all('migrants?filter[where][deleteFlag]=false').getList().$object;
        $scope.seenbysdsply = Restangular.all('seenbys?filter[where][deleteFlag]=false').getList().$object;
        $scope.refertosdsply = Restangular.all('refertos?filter[where][deleteFlag]=false').getList().$object;
        $scope.medicinesdply = Restangular.all('medicines?filter[where][deleteFlag]=false').getList().$object;
        $scope.testnamedsply = Restangular.all('testnames?filter[where][deleteFlag]=false').getList().$object;

        $scope.vitalrecords = Restangular.all('vitalrecords?filter[where][deleteFlag]=false').getList().$object;


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/patientrecord-list") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            // console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************** List **********/
        $scope.filterFields = ['HHId', 'mobile', 'patientname'];
        $scope.searchInput = '';


        Restangular.all($scope.patientrecordFilterCall).getList().then(function (patientRespose) {

            // Restangular.all('members?filter[where][deleteFlag]=false').getList().then(function (member) {
            Restangular.all($scope.memberFilterCall).getList().then(function (member) {

                Restangular.all('symptoms?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (symptom) {

                    Restangular.all('illnesses?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (illness) {

                        Restangular.all('symptoms?filter[where][deleteFlag]=false&filter[where][language]=' + 1).getList().then(function (symptom1) {

                            Restangular.all('illnesses?filter[where][deleteFlag]=false&filter[where][language]=' + 1).getList().then(function (illness1) {

                                Restangular.all('testresults?filter[where][deleteFlag]=false').getList().then(function (test) {

                                    Restangular.all('medicinegivens?filter[where][deleteFlag]=false').getList().then(function (medicine) {

                                        Restangular.all('vaitalrecordtrailers?filter[where][deleteFlag]=false').getList().then(function (vital) {


                                            $scope.members = member;
                                            $scope.symptoms = symptom;
                                            $scope.illnesses = illness;
                                            $scope.symptoms1 = symptom1;
                                            $scope.illnesses1 = illness1;
                                            $scope.patientrecords = patientRespose;
                                            $scope.testresults = test;
                                            $scope.medicinegivens = medicine;
                                            $scope.vaitalrecordtrailer = vital;

                                            $scope.modalInstanceLoad.close();

                                            angular.forEach($scope.patientrecords, function (member, index) {
                                                // console.log('member', member);

                                                if (member.memberofShgFlag === true) {
                                                    member.memberofShgFlagDis = 'Yes';
                                                    //console.log('member.memberofShgFlagDis', member.memberofShgFlagDis);
                                                } else {
                                                    member.memberofShgFlagDis = 'No';
                                                }

                                                if (member.patientname != null || member.patientname != '') {
                                                    member.patientnameNON = member.patientname;
                                                    // console.log('member.patientnameNON', member.patientnameNON);
                                                }


                                                if (member.mobile == null || member.mobile == '') {
                                                    member.mobile = '';
                                                }
                                                if (member.patientidfromWHP == null) {
                                                    member.patientidfromWHP = '';
                                                }

                                                $scope.myvitalArray = [];

                                                for (var mi = 0; mi < $scope.vaitalrecordtrailer.length; mi++) {
                                                    if (member.id == $scope.vaitalrecordtrailer[mi].patientHeaderId) {
                                                        $scope.myvitalArray.push($scope.vaitalrecordtrailer[mi]);
                                                    }
                                                }

                                                for (var i = 0; i < $scope.members.length; i++) {
                                                    if (member.memberId == $scope.members[i].id) {
                                                        member.patientname = $scope.members[i].name;
                                                        member.patientnameExp = $scope.members[i].name;
                                                        member.individualId = $scope.members[i].individualId;
                                                        member.mobile = $scope.members[i].mobile;
                                                        member.HHId = $scope.members[i].HHId;
                                                        break;
                                                    }
                                                }


                                                if ($window.sessionStorage.language == 1) {

                                                    var data3 = $scope.illnesses.filter(function (arr) {
                                                        return arr.id == member.illness
                                                    })[0];

                                                    if (data3 != undefined) {
                                                        member.illnessName = data3.name;
                                                    }

                                                } else if ($window.sessionStorage.language != 1) {

                                                    var data3 = $scope.illnesses.filter(function (arr) {
                                                        return arr.parentId == member.illness
                                                    })[0];

                                                    if (data3 != undefined) {
                                                        member.illnessName = data3.name;
                                                    }
                                                }

                                                var data4 = $scope.illnesses1.filter(function (arr) {
                                                    return arr.id == member.illness
                                                })[0];

                                                if (data4 != undefined) {
                                                    member.illnessNameNEW = data4.name;
                                                }

                                                var data5 = $scope.usersdisplay.filter(function (arr) {
                                                    return arr.id == member.associatedHF
                                                })[0];

                                                if (data5 != undefined) {
                                                    member.associatedHFName = data5.name;
                                                }

                                                var data6 = $scope.genderdsply.filter(function (arr) {
                                                    return arr.id == member.gender
                                                })[0];

                                                if (data6 != undefined) {
                                                    member.genderName = data6.name;
                                                }

                                                var data7 = $scope.migrantdsply.filter(function (arr) {
                                                    return arr.id == member.migrant
                                                })[0];

                                                if (data7 != undefined) {
                                                    member.migrantName = data7.name;
                                                }

                                                var data7 = $scope.seenbysdsply.filter(function (arr) {
                                                    return arr.id == member.seenby
                                                })[0];

                                                if (data7 != undefined) {
                                                    member.seenbyName = data7.name;
                                                }
                                                var data8 = $scope.refertosdsply.filter(function (arr) {
                                                    return arr.id == member.referto
                                                })[0];

                                                if (data8 != undefined) {
                                                    member.refertoName = data8.name;
                                                }

                                                var data13 = $scope.countrydisplay.filter(function (arr) {
                                                    return arr.id == member.countryId
                                                })[0];

                                                if (data13 != undefined) {
                                                    member.countryname = data13.name;
                                                }

                                                var data14 = $scope.statedisplay.filter(function (arr) {
                                                    return arr.id == member.stateId
                                                })[0];

                                                if (data14 != undefined) {
                                                    member.statename = data14.name;
                                                }

                                                var data15 = $scope.districtdsply.filter(function (arr) {
                                                    return arr.id == member.districtId
                                                })[0];

                                                if (data15 != undefined) {
                                                    member.districtname = data15.name;
                                                }

                                                var data16 = $scope.sitedsply.filter(function (arr) {
                                                    return arr.id == member.siteId
                                                })[0];

                                                if (data16 != undefined) {
                                                    member.sitename = data16.name;
                                                }



                                                var data18 = $scope.usersdisplay.filter(function (arr) {
                                                    return arr.id == member.lastModifiedBy
                                                })[0];

                                                if (data18 != undefined) {
                                                    member.lastModifiedByname = data18.username;
                                                }

                                                var data19 = $scope.roledsply.filter(function (arr) {
                                                    return arr.id == member.lastModifiedByRole
                                                })[0];

                                                if (data19 != undefined) {
                                                    member.lastModifiedByRolename = data19.name;
                                                }
                                                var data20 = $scope.roledsply.filter(function (arr) {
                                                    return arr.id == member.createdByRole
                                                })[0];

                                                if (data20 != undefined) {
                                                    member.createdByRolename = data20.name;
                                                }

                                                var data21 = $scope.usersdisplay.filter(function (arr) {
                                                    return arr.id == member.createdBy
                                                })[0];

                                                if (data21 != undefined) {
                                                    member.createdByname = data21.username;
                                                }

                                                member.testName = "";
                                                member.test = "";
                                                member.arrayTestname = '';

                                                for (var c = 0; c < $scope.testresults.length; c++) {
                                                    if ($scope.testresults[c].patientId == member.id) {
                                                        member.arrayTestname = $scope.testresults[c].testnameId.split(",");

                                                        for (var d = 0; d < $scope.testnamedsply.length; d++) {
                                                            for (var e = 0; e < member.arrayTestname.length; e++) {
                                                                if (member.arrayTestname[e] == $scope.testnamedsply[d].id) {
                                                                    if (member.testName == "") {
                                                                        member.testName = $scope.testnamedsply[d].name + ' - ' + $scope.testresults[c].result;
                                                                        member.test = $scope.testnamedsply[d].name;
                                                                    } else {
                                                                        member.testName = member.testName + ' , ' + $scope.testnamedsply[d].name + ' - ' + $scope.testresults[c].result;
                                                                        member.test = member.test + ' , ' + $scope.testnamedsply[d].name;
                                                                    }
                                                                    break;

                                                                  //  console.log('member.testName', member.testName);
                                                                }
                                                            }
                                                        }

                                                    }
                                                }
                                                member.temp = "";
                                                member.pulse = "";
                                                member.bdd = "";
                                                member.bds = "";
                                                member.SPO = "";
                                                member.Hemo = "";
                                                member.Sugar = "";
                                                member.RR = "";
                                                member.Height1 = "";
                                                member.Weight1 = "";
                                                member.bmi1 = "";

                                                for (var aa = 0; aa < $scope.myvitalArray.length; aa++) {
                                                    if ($scope.myvitalArray[aa].vitalrecordId == 1) {
                                                        member.temp = $scope.myvitalArray[aa].vitalValue;
                                                        //  break;
                                                    } else if ($scope.myvitalArray[aa].vitalrecordId == 2) {
                                                        member.pulse = $scope.myvitalArray[aa].vitalValue;
                                                        //  break;
                                                    } else if ($scope.myvitalArray[aa].vitalrecordId == 3) {
                                                        member.bdd = $scope.myvitalArray[aa].vitalValue;
                                                        //  break;
                                                    } else if ($scope.myvitalArray[aa].vitalrecordId == 4) {
                                                        member.bds = $scope.myvitalArray[aa].vitalValue;
                                                        //  break;
                                                    } else if ($scope.myvitalArray[aa].vitalrecordId == 5) {
                                                        member.SPO = $scope.myvitalArray[aa].vitalValue;
                                                        //  break;
                                                    } else if ($scope.myvitalArray[aa].vitalrecordId == 6) {
                                                        member.Hemo = $scope.myvitalArray[aa].vitalValue;
                                                        //  break;
                                                    } else if ($scope.myvitalArray[aa].vitalrecordId == 7) {
                                                        member.Sugar = $scope.myvitalArray[aa].vitalValue;
                                                        //  break;
                                                    } else if ($scope.myvitalArray[aa].vitalrecordId == 8) {
                                                        member.RR = $scope.myvitalArray[aa].vitalValue;
                                                        //  break;
                                                    } else if ($scope.myvitalArray[aa].vitalrecordId == 9) {
                                                        member.Height1 = $scope.myvitalArray[aa].vitalValue;
                                                        //  break;
                                                    } else if ($scope.myvitalArray[aa].vitalrecordId == 10) {
                                                        member.Weight1 = $scope.myvitalArray[aa].vitalValue;
                                                        //  break;
                                                    } else if ($scope.myvitalArray[aa].vitalrecordId == 11) {
                                                        member.bmi1 = $scope.myvitalArray[aa].vitalValue;
                                                        //  break;
                                                    }
                                                    // break;
                                                }


                                                if (member.symptom != null) {
                                                    member.arraysymptom = member.symptom.split(",");
                                                    member.symptomName = "";

                                                    for (var i = 0; i < member.arraysymptom.length; i++) {
                                                        for (var j = 0; j < $scope.symptoms.length; j++) {


                                                            if ($window.sessionStorage.language == 1) {
                                                                if ($scope.symptoms[j].id + "" === member.arraysymptom[i] + "") {
                                                                    if (member.symptomName === "") {
                                                                        member.symptomName = $scope.symptoms[j].name;
                                                                    } else {
                                                                        member.symptomName = member.symptomName + ", " + $scope.symptoms[j].name;
                                                                    }
                                                                    break;
                                                                }
                                                            } else if ($window.sessionStorage.language != 1) {
                                                                if ($scope.symptoms[j].parentId + "" === member.arraysymptom[i] + "") {
                                                                    if (member.symptomName === "") {
                                                                        member.symptomName = $scope.symptoms[j].name;
                                                                    } else {
                                                                        member.symptomName = member.symptomName + ", " + $scope.symptoms[j].name;
                                                                    }
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    member.symptomName = "";
                                                }

                                                /*******************/

                                                if (member.symptom != null) {
                                                    member.arraysymptom = member.symptom.split(",");
                                                    member.symptomNameNEW = "";

                                                    for (var ii = 0; ii < member.arraysymptom.length; ii++) {
                                                        for (var m = 0; m < $scope.symptoms1.length; m++) {

                                                            if ($scope.symptoms1[m].id == member.arraysymptom[ii]) {
                                                                if (member.symptomNameNEW === "") {
                                                                    member.symptomNameNEW = $scope.symptoms1[m].name;
                                                                    //console.log(member.symptomNameNEW);
                                                                } else {
                                                                    member.symptomNameNEW = member.symptomNameNEW + ", " + $scope.symptoms1[m].name;
                                                                    //console.log(member.symptomNameNEW);
                                                                }
                                                                break;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    member.symptomNameNEW = "";
                                                }

                                                /*******************/

                                                member.medicineName = "";

                                                for (var x = 0; x < $scope.medicinegivens.length; x++) {

                                                    if ($scope.medicinegivens[x].patientrecordid == member.id) {

                                                        for (var z = 0; z < $scope.medicinesdply.length; z++) {
                                                            if ($scope.medicinesdply[z].id + "" === $scope.medicinegivens[x].medicineid + "") {
                                                                if (member.medicineName === "") {
                                                                    member.medicineName = $scope.medicinesdply[z].name + ' - ' + $scope.medicinegivens[x].noofmedicine;
                                                                } else {
                                                                    member.medicineName = member.medicineName + ", " + $scope.medicinesdply[z].name + ' - ' + $scope.medicinegivens[x].noofmedicine;
                                                                }
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }


                                                member.datedisply = $filter('date')(member.visitdate, 'dd-MMM-yyyy');
                                                member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                                                member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                                                if (member.memberId == '0' || member.memberId == null || member.memberId == '') {
                                                    member.patientnameExp = '';
                                                    member.individualId = '';
                                                }
                                                if (member.referto == null) {
                                                    member.refertoName = '';
                                                }



                                                member.index = index + 1;
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });

        /**************************Export data to excel sheet ***************/

        $scope.DisableExport = false;
        $scope.valPRCount = 0;

        $scope.exportData = function () {
            $scope.PRArray = [];
            $scope.valPRCount = 0;
            if ($scope.patientrecords.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.patientrecords.length; c++) {
                    $scope.PRArray.push({
                        'COUNTRY': $scope.patientrecords[c].countryname,
                        'STATE': $scope.patientrecords[c].statename,
                        'DISTRICT': $scope.patientrecords[c].districtname,
                        'SITE': $scope.patientrecords[c].sitename,
                        'INDIVIDUAL ID': $scope.patientrecords[c].individualId,
                        'MEMBER OF SHG': $scope.patientrecords[c].memberofShgFlagDis,
                        'NAME OF THE PATIENT (SHG)': $scope.patientrecords[c].patientnameExp,
                        'NAME OF THE PATIENT (NON SHG)': $scope.patientrecords[c].patientnameNON,
                        'AGE': $scope.patientrecords[c].age,
                        'GENDER': $scope.patientrecords[c].genderName,
                        'CONTACT NUMBER': $scope.patientrecords[c].mobile,
                        'MIGRANT': $scope.patientrecords[c].migrantName,
                        'ASSIGNED TO': $scope.patientrecords[c].associatedHFName,
                        'VISIT DATE': $scope.patientrecords[c].datedisply,
                        'SYMPTOM': $scope.patientrecords[c].symptomNameNEW,
                        'ILLNESS': $scope.patientrecords[c].illnessNameNEW,
                        //'PRESCRIPTION': $scope.patientrecords[c].severepain,
                        'NAME OF THE MEDICINE': $scope.patientrecords[c].medicineName,
                        'SEEN BY': $scope.patientrecords[c].seenbyName,
                        'AMOUNT CHARGED': $scope.patientrecords[c].amountChagred,
                        'PATIENT ID FROM WHP': $scope.patientrecords[c].patientidfromWHP,
                        'REFFER TO': $scope.patientrecords[c].refertoName,
                        'TESTS': $scope.patientrecords[c].test,
                        'TEST NAME AND RESULT': $scope.patientrecords[c].testName,
                        'TEMPERATURE': $scope.patientrecords[c].temp,
                        'PULSE RATE': $scope.patientrecords[c].pulse,
                        'BP (Diastolic)': $scope.patientrecords[c].bdd,
                        'BP (Systolic)': $scope.patientrecords[c].bds,
                        'SPO2': $scope.patientrecords[c].SPO,
                        'HEMOGLOBIN': $scope.patientrecords[c].Hemo,
                        'SUGER LEVEL': $scope.patientrecords[c].Sugar,
                        'RR': $scope.patientrecords[c].RR,
                        'HEIGHT': $scope.patientrecords[c].Height1,
                        'WEIGHT': $scope.patientrecords[c].Weight1,
                        'BMI': $scope.patientrecords[c].bmi1,
                        'CREATED BY': $scope.patientrecords[c].createdByname,
                        'CREATED DATE': $scope.patientrecords[c].createdDate,
                        'CREATED ROLE': $scope.patientrecords[c].createdByRolename,
                        'LAST MODIFIED BY': $scope.patientrecords[c].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.patientrecords[c].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.patientrecords[c].lastModifiedByRolename,
                        'DELETE FLAG': $scope.patientrecords[c].deleteFlag

                    });

                    $scope.valPRCount++;
                    if ($scope.patientrecords.length == $scope.valPRCount) {
                        alasql('SELECT * INTO XLSX("patientrecords.xlsx",{headers:true}) FROM ?', [$scope.PRArray]);
                    }
                }
            }
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

    });
