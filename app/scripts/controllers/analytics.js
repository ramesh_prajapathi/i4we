'use strict';
angular.module('secondarySalesApp')
    .controller('AnalyticsCtrl', function ($scope, $http, Restangular, $window, $timeout, $location, baseUrl, $rootScope, $idle, $modal, $filter, $route) {

        $scope.MainLanguage = {};

        $scope.UserLanguage = $window.sessionStorage.language;

        Restangular.one('lhsLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.MainLanguage = langResponse[0];
        });

        Restangular.one('conditionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.ConditionLanguage = langResponse[0];
        });

        Restangular.all('conditions?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][enabledFor]=true' + '&filter[where][deleteFlag]=false').getList().then(function (cons) {
            $scope.conditions = cons;
        });

        $scope.NoofHHsinwhichnomembershavebeenprescreened = 0;
        $scope.NoofShgsNotScreened = 0;
        $scope.NoofIndMemsNotScreened = 0;
        $scope.totalMemsPreScreened = 0;
        $scope.totalMemsPositiveAftrPreScreened = 0;
        $scope.totalMemsScreened = 0;
        $scope.totalMemsPstveAfterScreened = 0;
        $scope.totalMemsTested = 0;
        $scope.totalMemsPstveAfterTested = 0;
        $scope.totalMemsTreatment = 0;
        $scope.totalMemsCured = 0;

        $scope.disableState = true;
        $scope.disableDistrict = true;
        $scope.disableSite = true;
        $scope.disableHf = true;

        $scope.myArray1 = [];

        Restangular.all('states?filter[where][deleteFlag]=false').getList().then(function (st) {
            $scope.states = st;
            $scope.state = $window.sessionStorage.stateId.split(',');
        });

        $scope.$watch('state', function (newValue, oldValue) {
            if (newValue == undefined || newValue == null || newValue == '' || newValue == oldValue) {
                return;
            } else if (newValue[0] == null || newValue[0] == "null") {
                return;
            } else {

                Restangular.all('districts?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (dist) {
                    $scope.districts = dist;

                    if ($window.sessionStorage.roleId == 2) {
                        $scope.district = '';
                        $scope.site = '';
                        $scope.hfuser = ''
                    } else {
                        $scope.district = $window.sessionStorage.districtId.split(',');
                    }
                });

                if ($scope.disableState == false) {

                    $scope.NoofHHsinwhichnomembershavebeenprescreened = 0;
                    $scope.NoofShgsNotScreened = 0;
                    $scope.NoofIndMemsNotScreened = 0;
                    $scope.totalMemsPreScreened = 0;
                    $scope.totalMemsPositiveAftrPreScreened = 0;
                    $scope.totalMemsScreened = 0;
                    $scope.totalMemsPstveAfterScreened = 0;
                    $scope.totalMemsTested = 0;
                    $scope.totalMemsPstveAfterTested = 0;
                    $scope.totalMemsTreatment = 0;
                    $scope.totalMemsCured = 0;

                    $scope.conditionId = '';

                    $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.householdFilterCall = 'households?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.memberFilterCall = 'members?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.mainFunc();
                }
            }
        });

        $scope.$watch('district', function (newValue, oldValue) {
            if (newValue == undefined || newValue == null || newValue == '' || newValue == oldValue) {
                return;
            } else if (newValue[0] == null || newValue[0] == "null") {
                return;
            } else {
                Restangular.all('sites?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (site) {
                    $scope.sites = site;
                    $scope.site = $window.sessionStorage.siteId.split(',');
                    // console.log(newValue);
                });

                if ($scope.disableDistrict == false) {

                    $scope.NoofHHsinwhichnomembershavebeenprescreened = 0;
                    $scope.NoofShgsNotScreened = 0;
                    $scope.NoofIndMemsNotScreened = 0;
                    $scope.totalMemsPreScreened = 0;
                    $scope.totalMemsPositiveAftrPreScreened = 0;
                    $scope.totalMemsScreened = 0;
                    $scope.totalMemsPstveAfterScreened = 0;
                    $scope.totalMemsTested = 0;
                    $scope.totalMemsPstveAfterTested = 0;
                    $scope.totalMemsTreatment = 0;
                    $scope.totalMemsCured = 0;

                    $scope.conditionId = '';

                    $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.householdFilterCall = 'households?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.memberFilterCall = 'members?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.mainFunc();
                }
            }
        });

        $scope.$watch('site', function (newValue, oldValue) {
            if (newValue == undefined || newValue == null || newValue == '' || newValue == oldValue) {
                return;
            } else if (newValue[0] == null || newValue[0] == "null") {
                return;
            } else {

                // console.log(newValue[0]);

                Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                    $scope.hfusers = urs;

                    if ($window.sessionStorage.roleId == 3) {
                        $scope.hfuser = $window.sessionStorage.userId.split(',');
                    }

                    if ($scope.disableSite == false) {

                        $scope.NoofHHsinwhichnomembershavebeenprescreened = 0;
                        $scope.NoofShgsNotScreened = 0;
                        $scope.NoofIndMemsNotScreened = 0;
                        $scope.totalMemsPreScreened = 0;
                        $scope.totalMemsPositiveAftrPreScreened = 0;
                        $scope.totalMemsScreened = 0;
                        $scope.totalMemsPstveAfterScreened = 0;
                        $scope.totalMemsTested = 0;
                        $scope.totalMemsPstveAfterTested = 0;
                        $scope.totalMemsTreatment = 0;
                        $scope.totalMemsCured = 0;

                        $scope.conditionId = '';

                        $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                        $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                        $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                        $scope.mainFunc();
                    }
                });
            }
        });

        $scope.$watch('hfuser', function (newValue, oldValue) {
            if (newValue == undefined || newValue == null || newValue == '' || newValue == oldValue) {
                return;
            } else if (newValue[0] == null || newValue[0] == "null") {
                return;
            } else {

                if ($scope.disableHf == false) {

                    $scope.NoofHHsinwhichnomembershavebeenprescreened = 0;
                    $scope.NoofShgsNotScreened = 0;
                    $scope.NoofIndMemsNotScreened = 0;
                    $scope.totalMemsPreScreened = 0;
                    $scope.totalMemsPositiveAftrPreScreened = 0;
                    $scope.totalMemsScreened = 0;
                    $scope.totalMemsPstveAfterScreened = 0;
                    $scope.totalMemsTested = 0;
                    $scope.totalMemsPstveAfterTested = 0;
                    $scope.totalMemsTreatment = 0;
                    $scope.totalMemsCured = 0;

                    $scope.conditionId = '';

                    $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.householdFilterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.mainFunc();
                }
            }
        });

        $scope.mainFunc = function () {

            $scope.myArray1 = [];

            $scope.memeArray = [];

            $scope.memeArray1 = [];

            $scope.memeArray2 = [];

            $scope.memeArray3 = [];

            $scope.memeArray4 = [];

            $scope.memeArray5 = [];

            $scope.memeArray6 = [];

            Restangular.all($scope.householdFilterCall).getList().then(function (household) {
                $scope.noOfHh = household.length;

                Restangular.all($scope.memberFilterCall).getList().then(function (mrs) {
                    $scope.noOfIndMem = mrs.length;
                    $scope.members = mrs;

                    Restangular.all($scope.shgFilterCall).getList().then(function (shg) {
                        $scope.noOfShg = shg.length;
                        $scope.shgs = shg;

                        $scope.newArray = [];

                        $scope.modalInstanceLoad.close();

                        angular.forEach($scope.shgs, function (member, index) {
                            member.index = index + 1;

                            member.membersInGroupLength = member.membersInGroup.split(',').length;

                            var c = member.membersInGroup.split(',');

                            var d = _.uniq(c, function (w) {
                                return w;
                            })

                            $scope.newArray.push(d);
                        });

                        if ($scope.shgs.length == $scope.newArray.length) {
                            for (var n = 0; n < $scope.newArray.length; n++) {
                                $scope.noOfmemsInshGCount = $scope.noOfmemsInshGCount + $scope.newArray[n].length;
                                $scope.noOfmemsInshG = $scope.noOfmemsInshGCount;
                                for (var o = 0; o < $scope.newArray[n].length; o++) {
                                    $scope.myArray1.push($scope.newArray[n][o]);

                                    if ($scope.UserLanguage == 1) {
                                        $scope.conditionId = $scope.conditions[0].id;
                                    } else {
                                        $scope.conditionId = $scope.conditions[0].parentId;
                                    }
                                }
                            }
                        }
                    });
                });
            });
        };

        if ($window.sessionStorage.roleId + "" === "3") {
            //HF

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = true;
            $scope.disableHf = true;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else if ($window.sessionStorage.roleId + "" === "8") {
            //Site Manager

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = true;
            $scope.disableHf = false;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else if ($window.sessionStorage.roleId + "" === "2") {
            //National Director

            $scope.disableState = false;
            $scope.disableDistrict = false;
            $scope.disableSite = false;
            $scope.disableHf = false;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}},{"stateId":{"gte":21}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}},{"stateId":{"gte":21}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}},{"stateId":{"gte":21}}]}}';

        } else if ($window.sessionStorage.roleId + "" === "4") {
            //Doctor

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = true;
            $scope.disableHf = false;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else if ($window.sessionStorage.roleId + "" === "5") {
            //District Programme Manager

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = false;
            $scope.disableHf = false;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"districtId":{"inq":[' + $window.sessionStorage.districtId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"districtId":{"inq":[' + $window.sessionStorage.districtId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"districtId":{"inq":[' + $window.sessionStorage.districtId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else if ($window.sessionStorage.roleId + "" === "6") {
            //State Programme Manager

            $scope.disableState = true;
            $scope.disableDistrict = false;
            $scope.disableSite = false;
            $scope.disableHf = false;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"stateId":{"inq":[' + $window.sessionStorage.stateId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"stateId":{"inq":[' + $window.sessionStorage.stateId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"stateId":{"inq":[' + $window.sessionStorage.stateId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else if ($window.sessionStorage.roleId + "" === "7") {
            //Nurse

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = true;
            $scope.disableHf = false;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        } else {

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = true;
            $scope.disableHf = false;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        }

        Restangular.all($scope.householdFilterCall).getList().then(function (household) {
            $scope.noOfHh = household.length;

            Restangular.all($scope.memberFilterCall).getList().then(function (mrs) {
                $scope.noOfIndMem = mrs.length;
                $scope.members = mrs;

                Restangular.all($scope.shgFilterCall).getList().then(function (shg) {
                    $scope.noOfShg = shg.length;
                    $scope.shgs = shg;

                    $scope.newArray = [];

                    $scope.modalInstanceLoad.close();

                    angular.forEach($scope.shgs, function (member, index) {
                        member.index = index + 1;

                        member.membersInGroupLength = member.membersInGroup.split(',').length;

                        var c = member.membersInGroup.split(',');

                        var d = _.uniq(c, function (w) {
                            return w;
                        })

                        $scope.newArray.push(d);
                    });

                    if ($scope.shgs.length == $scope.newArray.length) {
                        for (var n = 0; n < $scope.newArray.length; n++) {
                            $scope.noOfmemsInshGCount = $scope.noOfmemsInshGCount + $scope.newArray[n].length;
                            $scope.noOfmemsInshG = $scope.noOfmemsInshGCount;
                            for (var o = 0; o < $scope.newArray[n].length; o++) {
                                $scope.myArray1.push($scope.newArray[n][o]);

                                if ($scope.UserLanguage == 1) {
                                    $scope.conditionId = $scope.conditions[0].id;
                                } else {
                                    $scope.conditionId = $scope.conditions[0].parentId;
                                }
                            }
                        }
                    }
                });
            });
        });

        $scope.memeArray = [];

        $scope.memeArray1 = [];

        $scope.memeArray2 = [];

        $scope.memeArray3 = [];

        $scope.memeArray4 = [];

        $scope.memeArray5 = [];

        $scope.memeArray6 = [];

        $scope.$watch('conditionId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {

                $scope.NoofShgsNotScreened = $scope.myArray1.length;
                $scope.NoofIndMemsNotScreened = $scope.members.length;
                $scope.totalMemsPreScreened = 0;
                $scope.totalMemsPositiveAftrPreScreened = 0;
                $scope.totalMemsScreened = 0;
                $scope.totalMemsPstveAfterScreened = 0;
                $scope.totalMemsTested = 0;
                $scope.totalMemsPstveAfterTested = 0;
                $scope.totalMemsTreatment = 0;
                $scope.totalMemsCured = 0;

                $scope.memeArray = [];

                $scope.memeArray1 = [];

                $scope.memeArray2 = [];

                $scope.memeArray3 = [];

                $scope.memeArray4 = [];

                $scope.memeArray5 = [];

                $scope.memeArray6 = [];

                if ($window.sessionStorage.roleId + "" === "3") {

                    $scope.householdFilterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.conditionFilterCall = 'conditionheaders?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}';

                } else {

                    if ($scope.disableState == false) {

                        if ($scope.state == '' || $scope.state == null || $scope.state == undefined || $scope.state[0] == null || $scope.state[0] == "null") {

                            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                            $scope.conditionFilterCall = 'conditionheaders?filter={"where":{"and":[{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}';

                        } else {

                            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"stateId":{"inq":[' + $scope.state + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                            $scope.conditionFilterCall = 'conditionheaders?filter={"where":{"and":[{"stateId":{"inq":[' + $scope.state + ']}},{"deleteFlag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}';

                        }

                    } else if ($scope.disableDistrict == false) {

                        if ($scope.district == '' || $scope.district == null || $scope.district == undefined || $scope.district[0] == null || $scope.district[0] == "null") {

                            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"stateId":{"inq":[' + $scope.state + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                            $scope.conditionFilterCall = 'conditionheaders?filter={"where":{"and":[{"stateId":{"inq":[' + $scope.state + ']}},{"deleteFlag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}';

                        } else {

                            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"districtId":{"inq":[' + $scope.district + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                            $scope.conditionFilterCall = 'conditionheaders?filter={"where":{"and":[{"districtId":{"inq":[' + $scope.district + ']}},{"deleteFlag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}';

                        }

                    } else if ($scope.disableSite == false) {

                        if ($scope.site == '' || $scope.site == null || $scope.site == undefined || $scope.site[0] == null || $scope.site[0] == "null") {
                            
                           // console.log('site', $scope.site);

                            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"districtId":{"inq":[' + $scope.district + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                            $scope.conditionFilterCall = 'conditionheaders?filter={"where":{"and":[{"districtId":{"inq":[' + $scope.district + ']}},{"deleteFlag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}';

                        } else {

                            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $scope.site + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                            $scope.conditionFilterCall = 'conditionheaders?filter={"where":{"and":[{"siteId":{"inq":[' + $scope.site + ']}},{"deleteFlag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}';

                        }

                    } else if ($scope.disableHf == false) {

                        if ($scope.hfuser == '' || $scope.hfuser == null || $scope.hfuser == undefined) {

                            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $scope.site + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                            $scope.conditionFilterCall = 'conditionheaders?filter={"where":{"and":[{"siteId":{"inq":[' + $scope.site + ']}},{"deleteFlag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}';

                        } else {

                            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + $scope.hfuser + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                            $scope.conditionFilterCall = 'conditionheaders?filter={"where":{"and":[{"associatedHF":{"inq":[' + $scope.hfuser + ']}},{"deleteFlag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}';
                        }
                    }
                }

                Restangular.all($scope.conditionFilterCall).getList().then(function (resp) {
                    $scope.totalconditionheaders = resp;
                    var arr = [];
                    $scope.conditionheaders = [];

                    $.each(resp, function (index, value) {
                        if ($.inArray(value.memberId, arr) == -1) {
                            arr.push(value.memberId);
                            $scope.conditionheaders.push(value);
                        }
                    });
                    // console.log($scope.conditionheaders);

                    Restangular.all($scope.householdFilterCall).getList().then(function (household) {

                        $scope.NoofHHsinwhichnomembershavebeenprescreened = household.length;

                        var uniqueHHID = [];
                        $scope.filteredconditionheaders = [];

                        $.each($scope.conditionheaders, function (index, value) {
                            if ($.inArray(value.headOfHouseholdId, uniqueHHID) == -1) {
                                uniqueHHID.push(value.headOfHouseholdId);
                                $scope.filteredconditionheaders.push(value);
                            }
                        });

                        for (var i = 0; i < household.length; i++) {
                            for (var j = 0; j < $scope.filteredconditionheaders.length; j++) {
                                if (household[i].id == $scope.filteredconditionheaders[j].headOfHouseholdId) {
                                    $scope.NoofHHsinwhichnomembershavebeenprescreened--;
                                    break;
                                }
                            }
                        }

                        for (var k = 0; k < $scope.myArray1.length; k++) {
                            for (var l = 0; l < $scope.conditionheaders.length; l++) {
                                if ($scope.myArray1[k] == $scope.conditionheaders[l].memberId) {
                                    $scope.NoofShgsNotScreened--;
                                    break;
                                }
                            }
                        }

                      //  console.log("$scope.members.length", $scope.members.length);
                      //  console.log("$scope.conditionheaders.length", $scope.conditionheaders.length);
                        $scope.NoofIndMemsNotScreened = ($scope.members.length) - ($scope.conditionheaders.length);
                        $scope.totalMemsPreScreened = $scope.conditionheaders.length;
                        /*for (var n = 0; n < $scope.conditionheaders.length; n++) {
                            for (var m = 0; m < $scope.members.length; m++) {
                                if ($scope.members[m].id === $scope.conditionheaders[n].memberId) {
                                    $scope.NoofIndMemsNotScreened--;
                                    $scope.totalMemsPreScreened++;
                                    break;
                                }
                            }
                        }*/

                        for (var p = 0; p < $scope.conditionheaders.length; p++) {
                            if ($scope.conditionheaders[p].step > 1 || ($scope.conditionheaders[p].step == 1 && $scope.conditionheaders[p].status == 4)) {
                                $scope.memeArray.push($scope.conditionheaders[p].memberId);
                            }

                            if ($scope.conditionheaders[p].step > 2 || $scope.conditionheaders[p].step == 2) {
                                $scope.memeArray1.push($scope.conditionheaders[p].memberId);
                            }

                            if ($scope.conditionheaders[p].step > 2 || $scope.conditionheaders[p].step == 2 && $scope.conditionheaders[p].status == 4) {
                                $scope.memeArray2.push($scope.conditionheaders[p].memberId);
                            }

                            if ($scope.conditionheaders[p].step > 3 || $scope.conditionheaders[p].step == 3) {
                                $scope.memeArray3.push($scope.conditionheaders[p].memberId);
                            }

                            if ($scope.conditionheaders[p].step > 3 || $scope.conditionheaders[p].step == 3 && $scope.conditionheaders[p].status == 4) {
                                $scope.memeArray4.push($scope.conditionheaders[p].memberId);
                            }

                            if ($scope.conditionheaders[p].step >= 4) {
                                $scope.memeArray5.push($scope.conditionheaders[p].memberId);
                            }
                        }
                        for (var z = 0; z < $scope.totalconditionheaders.length; z++) {
                            if ($scope.totalconditionheaders[z].step == 5 && $scope.totalconditionheaders[z].status == 5 && $scope.totalconditionheaders[z].caseClosed == true) {
                                $scope.memeArray6.push($scope.totalconditionheaders[z].memberId);
                            }
                        }

                        var removeDuplicate = function (arr) {
                            var uniques = {};
                            var output = [];
                            for (var i = 0, n = arr.length; i < n; i++) {
                                var arr_item = arr[i];

                                if (!uniques[arr_item]) {
                                    output.push(arr_item);
                                    uniques[arr_item] = true;
                                }
                            }
                            return output;
                        };

                        var rd1 = removeDuplicate($scope.memeArray);
                        var rd2 = removeDuplicate($scope.memeArray1);
                        var rd3 = removeDuplicate($scope.memeArray2);
                        var rd4 = removeDuplicate($scope.memeArray3);
                        var rd5 = removeDuplicate($scope.memeArray4);
                        var rd6 = removeDuplicate($scope.memeArray5);
                        var rd7 = ($scope.memeArray6);

                        $scope.totalMemsPositiveAftrPreScreened = rd1.length;
                        $scope.totalMemsScreened = rd2.length;
                        $scope.totalMemsPstveAfterScreened = rd3.length;
                        $scope.totalMemsTested = rd4.length;
                        $scope.totalMemsPstveAfterTested = rd5.length;
                        $scope.totalMemsTreatment = rd6.length;
                        $scope.totalMemsCured = rd7.length;
                    });
                });
            }
        });

    });
