'use strict';

angular.module('secondarySalesApp')
	.controller('ToDoStatusesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}
		$scope.showForm = function () {
			var visible = $location.path() === '/todostatuses/create' || $location.path() === '/todostatuses/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/todostatuses/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/todostatuses/create' || $location.path() === '/todostatuses/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/todostatuses/create' || $location.path() === '/todostatuses/' + $routeParams.id;
			return visible;
		};

		/*********************************** Pagination *******************************************/

		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			//console.log('$scope.countryId From Landing', $scope.pageSize);
		}

		
		if ($window.sessionStorage.prviousLocation != "partials/todostatuses") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}
	
		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};

		/*********/
		//  $scope.todostatuses = Restangular.all('todostatuses').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'Todo Status has been Updated!';
			Restangular.one('todostatuses', $routeParams.id).get().then(function (todostatus) {
				$scope.original = todostatus;
				$scope.todostatus = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Todo Status has been Created!';
		}
		$scope.searchToDoStatus = $scope.name;
		$scope.pillars = Restangular.all('pillars').getList().$object;

		/************************** INDEX *******************************************/
		$scope.zn = Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.todostatuses = zn;
			angular.forEach($scope.todostatuses, function (member, index) {
				member.index = index + 1;
			});
		});

		$scope.todostatus = {
			deleteflag: false,
			name: ''
		};
		/******************************************** SAVE *******************************************/
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.SaveToDoStatus = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.todostatus.name == '' || $scope.todostatus.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.todostatus.hnname == '' || $scope.todostatus.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.todostatus.knname == '' || $scope.todostatus.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.todostatus.taname == '' || $scope.todostatus.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.todostatus.tename == '' || $scope.todostatus.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.todostatus.mrname == '' || $scope.todostatus.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.todostatuses.post($scope.todostatus).then(function () {
					console.log('ToDoStatus Saved');
					window.location = '/todostatuses';
				});
			};
		};
		/*********************************** UPDATE *******************************************/
		$scope.UpdateToDoStatus = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.todostatus.name == '' || $scope.todostatus.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.todostatus.hnname == '' || $scope.todostatus.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.todostatus.knname == '' || $scope.todostatus.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.todostatus.taname == '' || $scope.todostatus.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.todostatus.tename == '' || $scope.todostatus.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.todostatus.mrname == '' || $scope.todostatus.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.todostatuses.customPUT($scope.todostatus).then(function () {
					console.log('ToDoStatus Saved');
					window.location = '/todostatuses';
				});
			}
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/*************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('applicationstages/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

		$scope.getPillar = function (pillarId) {
			return Restangular.one('pillars', pillarId).get().$object;
		};

	});
