'use strict';

angular.module('secondarySalesApp')
    .controller('InterventionsStatusCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {

        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/interventions-status/create' || $location.path() === '/interventions-status/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/interventions-status/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/interventions-status/create' || $location.path() === '/interventions-status/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/interventions-status/create' || $location.path() === '/interventions-status/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);

        if ($window.sessionStorage.prviousLocation != "partials/interventions-status") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.showenglishLang = true;
        $scope.OtherLang = true;

        $scope.$watch('status.language', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }
                //                Restangular.one('genders?filter[where][deleteFlag]=false&filter[where][language]='+newValue).get().then(function(gen){
                //                    if(gen.lenth>0){
                //                        alert("Already Exists");
                //                    }
                //                    
                //                })
            }
        });

        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Status has been Updated!';
            Restangular.one('interventionstatuses', $routeParams.id).get().then(function (status) {
                $scope.original = status;
                $scope.status = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'reason has been Created!';
        }

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];

        /******************************** INDEX *******************************************/
        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (zn) {
            $scope.languages = zn;
        });

        Restangular.all('interventionstatuses?filter[where][deleteFlag]=false').getList().then(function (ives) {
            $scope.interventionstatuses = ives;
            angular.forEach($scope.interventionstatuses, function (member, index) {
                member.index = index + 1;

                Restangular.one('languages', member.language).get().then(function (lng) {
                    member.langname = lng.name;
                });
            });
        });

        Restangular.all('interventionstatuses?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (iv) {
            $scope.englishinterventionstatuses = iv;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };
        /********************************************* SAVE *******************************************/
    
        $scope.status = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteFlag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";

            if ($scope.status.language == '' || $scope.status.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.status.name == '' || $scope.status.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Intervention Status';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.status.parentId === '') {
                    delete $scope.status['parentId'];
                }
                $scope.submitDisable = true;
                $scope.status.parentFlag = $scope.showenglishLang;
                $scope.interventionstatuses.post($scope.status).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    window.location = '/interventions-status-list';
                }, function (error) {
                    if (error.data.error.constraint === 'interventionstatus_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.status.language == '' || $scope.status.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.status.name == '' || $scope.status.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Intervention Status';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.status.parentId === '') {
                    delete $scope.status['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('interventionstatuses', $routeParams.id).customPUT($scope.status).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    // console.log('Step Saved');
                    window.location = '/interventions-status-list';
                }, function (error) {
                    if (error.data.error.constraint === 'interventionstatus_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

                var sort = $scope.sort;

                if (sort.active == column) {
                    return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
                }
            }
            /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('interventionstatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });