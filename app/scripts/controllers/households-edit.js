'use strict';
angular.module('secondarySalesApp')
    .controller('HouseholdEditCtrl', function ($scope, Restangular, $routeParams, $filter, $timeout, $window, AnalyticsRestangular, $modal, $route, $location) {

        $scope.HHLanguage = {};

        Restangular.one('householdlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.HHLanguage = langResponse[0];
            $scope.headingName = $scope.HHLanguage.houseHoldEdit;
            $scope.message = $scope.HHLanguage.thankYouUpdated;
            // $scope.modalTitle = $scope.HHLanguage.thankYou;
            // console.log('$scope.modalTitle', $scope.modalTitle);
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        $scope.maritalstatuses = Restangular.all('maritalstatuses').getList().$object;
        $scope.typologies = Restangular.all('typologies').getList().$object;

        $scope.genders = Restangular.all('genders?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        Restangular.all('genders?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (gendone) {
            $scope.gendersone = gendone;
        });

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.disableAssigned = true;
            $scope.hideDeleteFamilyMember = true;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.users = Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().$object;
        } else {

            $scope.disableAssigned = false;
            $scope.hideDeleteFamilyMember = false;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.users = Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().$object;
        }

        Restangular.all('occupations?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (occptn) {
            $scope.occupations = occptn;
        });

        $scope.castes = Restangular.all('castes?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.migrants = Restangular.all('migrants?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.religions = Restangular.all('religions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.bplStatuses = Restangular.all('bplStatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        Restangular.all('relationWithHHs?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rltion) {
            $scope.relationWithHHs = rltion;
        });

        $scope.literacyLevels = Restangular.all('literacyLevels?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        Restangular.all('nameOfSchools?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (nOfschls) {
            $scope.nameOfSchools = nOfschls;
        });

        Restangular.all('nameOfFactories?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (nOffacts) {
            $scope.nameOfFactories = nOffacts;
        });

        $scope.shgs = Restangular.all('shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().$object;

        //  $scope.shgs = Restangular.all('shgs?filter[where][deleteFlag]=false').getList().$object;

        $scope.memberlists = [];

        $('#latitude').keypress(function (evt) {
            if (/^[0-9.]+$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $('#longitude').keypress(function (evt) {
            if (/^[0-9.]+$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $scope.member = {
            relation: null,
            deleteFlag: false,
            isHeadOfFamily: true,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0],
            shgId: null,
            age: '',
            name: ''
        };
    
    

        $scope.$watch('member.aadharNumberParsed', function (newValue, oldValue) {
            console.log("newValue",newValue);
            if(newValue!=null&&newValue.indexOf('x')==-1){
            $scope.member.aadharNumber = angular.copy($scope.member.aadharNumberParsed);
            }
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else if (newValue.length < 12) {
                return;
            } else {
                $scope.member.aadharNumberParsed = new Array(newValue.length - 3).join('x') + newValue.substr(newValue.length - 4, 4);
            }

        });

        $scope.Shgbelongs = [];

        $scope.ShgbelongsId = [];

        if ($routeParams.id) {
            Restangular.one('households', $routeParams.id).get().then(function (household) {
                $scope.original = household;
                $scope.household = Restangular.copy($scope.original);

                if ($scope.household.ssgThisHHBelongs != null) {
                    $scope.ShgbelongsId = $scope.household.ssgThisHHBelongs.split(',');

                    var uniqueNames = [];
                    $.each($scope.ShgbelongsId, function (i, el) {
                        if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
                    });

                    for (var d = 0; d < uniqueNames.length; d++) {
                        //  console.log('$scope.ShgbelongsId[d]', $scope.ShgbelongsId[d]);

                        Restangular.one('shgs/findOne?filter[where][id]=' + uniqueNames[d]).get().then(function (shgid) {
                            // console.log('shgid', shgid);
                            $scope.Shgbelongs.push(shgid.groupName);
                            $scope.ssgThisHHBelongs = $scope.Shgbelongs.toString();
                        });
                    }
                }

                Restangular.all('genders?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (gendList) {
                    $scope.gendersList = gendList;

                    Restangular.all('relationWithHHs?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rltionlst) {
                        $scope.relationWithHHsLists = rltionlst;

                        Restangular.all('members?filter[where][parentId]=' + household.id + '&filter[where][deleteFlag]=false').getList().then(function (memeResp) {

                            $scope.memlists = memeResp;
                            $scope.memberlists = [];
                            $scope.memCount = 0;

                            angular.forEach($scope.memlists, function (member, index) {
                                if (member.isHeadOfFamily === true) {
                                    $scope.member = member;
                                    $scope.member.aadharNumberParsed = angular.copy(member.aadharNumber);
                                    $scope.memberid = member.id;
                                    $scope.memberFullName = member.name;
                                    $scope.memberlists.splice(0, $scope.memCount);
                                    //  console.log('$scope.memberlists if', $scope.memberlists);
                                } else {
                                    $scope.memberlists.push(member);
                                    // console.log('$scope.memberlists', $scope.memberlists);
                                }

                                $scope.memCount++;
                                member.index = index;

                                if ($window.sessionStorage.language == 1) {
                                    var data = $scope.gendersList.filter(function (arr) {
                                        return arr.id == member.gender
                                    })[0];

                                    if (data != undefined) {
                                        member.gendername = data.name;
                                    }
                                } else {
                                    var data = $scope.gendersList.filter(function (arr) {
                                        return arr.parentId == member.gender
                                    })[0];

                                    if (data != undefined) {
                                        member.gendername = data.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {
                                    var data1 = $scope.relationWithHHsLists.filter(function (arr) {
                                        return arr.id == member.relation
                                    })[0];

                                    if (data1 != undefined) {
                                        member.relationname = data1.name;
                                    }
                                } else {
                                    var data1 = $scope.relationWithHHsLists.filter(function (arr) {
                                        return arr.parentId == member.relation
                                    })[0];

                                    if (data1 != undefined) {
                                        member.relationname = data1.name;
                                    }
                                }

                                Restangular.one('shgs', member.shgId).get().then(function (sgs) {
                                    member.shgname = sgs.groupName;
                                });
                            });
                            $scope.Idcount = memeResp.length + 1;
                        });
                    });
                });
            });
        }

        //   console.log('$location', $location.$$url);

        if ($location.$$url == '/shg/household/create') {
            $scope.HideSHG = true;
        } else {
            $scope.HideSHG = false;
        }

        $scope.HideAdd = false;
        $scope.HideUpdate = true;
        $scope.addClicked = false;
        $scope.updateClicked = false;
        $scope.editClicked = false;
        $scope.disableSchool = true;
        $scope.migrantdataModal = false;
        $scope.SHGmodal = false;

        if ($location.$$url === '/household/create') {
            $scope.showCancel = true;
        }

        $scope.CancelButton = function () {
            if ($location.$$url == '/shg/household/create') {
                $location.path('/shg/create');
            } else {
                $location.path('/household-list');
            }
        };

        $scope.Validname = function ($event, value) {
            var arr = $scope.member.name.split(/\s+/);
            // console.log('arr.length', arr.length);
            if (arr.length == 0 || arr.length == 1) {
                setTimeout(function () {
                    alert($scope.HHLanguage.invalidName);
                }, 1000);
                document.getElementById('name').style.borderColor = "#FF0000";
                //                var name = $window.document.getElementById('name');
                //                name.focus();
            } else {
                document.getElementById('name').style.borderColor = "";
                $scope.duplicateHHFind(value);
            }
        };

        $scope.household = {
            deleteFlag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        };

        $scope.ValidAadhar = function ($event) {
            // console.log('$scope.member.aadharNumber', $scope.member.aadharNumber);
            Restangular.one('members/findOne?filter[where][aadharNumber]=' + $scope.member.aadharNumber).get().then(function (membr) {
                alert($scope.HHLanguage.existingAadhar);
                $scope.member.aadharNumber = '';
                document.getElementById('aadharno').style.borderColor = "#FF0000";
            }, function (response) {
                // console.log("Error with status code", response.status);
                document.getElementById('aadharno').style.borderColor = "";
            });
        };

        $scope.ValidAadharOne = function ($event) {
            // console.log('$scope.member.aadharNumber', $scope.member.aadharNumber);
            Restangular.one('members/findOne?filter[where][aadharNumber]=' + $scope.memberone.aadharNumber).get().then(function (membrone) {
                alert($scope.HHLanguage.existingAadhar);
                $scope.memberone.aadharNumber = '';
                document.getElementById('aadhar').style.borderColor = "#FF0000";
            }, function (response) {
                // console.log("Error with status code", response.status);
                document.getElementById('aadhar').style.borderColor = "";
            });
        };

        $scope.duplicateMemberHead = false;

        $scope.duplicateHHFind = function (value) {

            if (value == '' || value == null) {
                return;
            } else {

                Restangular.all($scope.memberFilterCall).getList().then(function (memList) {
                    var resultObject = _.findWhere(memList, {
                        name: value
                    });
                    if (resultObject == undefined) {
                        document.getElementById('name').style.borderColor = "";
                        $scope.duplicateMemberHead = false;
                        //$scope.someMemberName = false;
                    } else {
                        $scope.toggleValidation();
                        $scope.validatestring1 = $scope.HHLanguage.existinMemberNamePleaseChange;
                        // $scope.someMemberName = true;
                        document.getElementById('name').style.borderColor = "#FF0000";
                        $scope.duplicateMemberHead = true;
                    }
                });
            }
        };

        $scope.duplicateMember = false;

        $scope.duplicateFind = function (value) {

            if (value == '' || value == null) {
                return;
            } else {

                Restangular.all($scope.memberFilterCall).getList().then(function (memList) {

                    var resultObject = _.findWhere(memList, {
                        name: value
                    });

                    if (resultObject == undefined) {
                        document.getElementById('nameone').style.borderColor = "";
                        $scope.duplicateMember = false;
                        //$scope.someMemberName = false;
                    } else {
                        $scope.toggleValidation();
                        $scope.validatestring1 = $scope.HHLanguage.existinMemberNamePleaseChange;
                        //  $scope.memberone.name = '';
                        document.getElementById('nameone').style.borderColor = "#FF0000";
                        $scope.duplicateMember = true;
                    }
                });
            }
        };

        $scope.memberone = {};

        $scope.getRelation = function (relation) {
            return Restangular.one('relationWithHHs', relation).get().$object;
        };

        $scope.getGender = function (gender) {
            return Restangular.one('genders', gender).get().$object;
        };

        $scope.getSHG = function (shgId) {
            return Restangular.one('shgs', shgId).get().$object;
        };

        $scope.memberlists = [];

        var Shgbelongs = [];

        $scope.AddMember = function () {
            $scope.HideAdd = false;
            $scope.HideUpdate = true;
            $scope.addClicked = false;
            $scope.beneficiarydataModal = true;
            $scope.dupCheckFlag = true;

            //  $scope.individualId = $scope.member.individualId.split('-');

            //  $scope.indivdId = $scope.Idcount;

            var myNumber = $scope.Idcount;

            //   var formattedNumber = ("0" + myNumber).slice(-2);

            $scope.memberone.individualId = $scope.member.HHId + '-' + myNumber;
        };

        $scope.validatestring = '';

        $scope.SaveMember = function () {

            $scope.Shgbelongs = [];

            $scope.ShgbelongsId = [];

            document.getElementById('nameone').style.border = "";
            document.getElementById('ageone').style.border = "";

            //            if ($scope.memberone.gender == $scope.mygenderfmale && $scope.memberone.age > 18) {
            //                for (var v = 0; v < $scope.memberlists.length; v++) {
            //                    // console.log($scope.ShgbelongsId[v]);
            //                    if ($scope.memberlists[v].shgId == $scope.memberone.shgId) {
            //                        // alert('Already This SHG Belongs to Some Family Member, Please Change SHG');
            //                        $scope.validatestring = $scope.validatestring + $scope.HHLanguage.alreadySHGBelongs;
            //                        $scope.memberone.shgId = '';
            //                    }
            //                }
            //            } else

            if ($scope.memberone.name == '' || $scope.memberone.name == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterNameOfMember;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.duplicateMember == true) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.existinMemberNamePleaseChange;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.memberone.relation == '' || $scope.memberone.relation == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectRelationship;
                // document.getElementById('date').style.borderColor = "#FF0000";

            } else if ($scope.memberone.dob == '' || $scope.memberone.dob == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectDOB;

            } else if ($scope.memberone.age === '' || $scope.memberone.age === null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterAge;
                document.getElementById('ageone').style.borderColor = "#FF0000";

            } else if ($scope.memberone.literacyLevel == '' || $scope.memberone.literacyLevel == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterLiteracyLevel;
                // document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.memberone.age <= 18) {
                if ($scope.memberone.nameOfSchool === '' || $scope.memberone.nameOfSchool === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterSchoolName;
                }

            } else if ($scope.hideOccupation == false) {
                if ($scope.memberone.occupation === '' || $scope.memberone.occupation === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectOccupation;
                }

            } else if ($scope.memberone.gender == '' || $scope.memberone.gender == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectGender;
                // document.getElementById('age').style.borderColor = "#FF0000";

            }
            if ($scope.hideFactory == false) {
                if ($scope.memberone.nameOfFactory === '' || $scope.memberone.nameOfFactory === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectFactory;
                }

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.addClicked = true;
                //   $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

                $scope.memberlists.push({
                    name: $scope.memberone.name,
                    relation: $scope.memberone.relation,
                    dob: $scope.memberone.dob,
                    age: $scope.memberone.age,
                    literacyLevel: $scope.memberone.literacyLevel,
                    nameOfSchool: $scope.memberone.nameOfSchool,
                    occupation: $scope.memberone.occupation,
                    nameOfFactory: $scope.memberone.nameOfFactory,
                    aadharNumber: $scope.memberone.aadharNumber,
                    esiNumber: $scope.memberone.esiNumber,
                    shgId: $scope.memberone.shgId,
                    gender: $scope.memberone.gender,
                    individualId: $scope.memberone.individualId,
                    mobile: $scope.memberone.mobile,
                    address: $scope.member.address,
                    caste: $scope.member.caste,
                    religion: $scope.member.religion,
                    migrant: $scope.member.migrant,
                    bplStatus: $scope.member.bplStatus,
                    latitude: $scope.member.latitude,
                    longitude: $scope.member.longitude,
                    email: $scope.member.email,
                    healthyDays: 0,
                    deleteFlag: false,
                    isHeadOfFamily: false,
                    createdDate: new Date(),
                    createdBy: $window.sessionStorage.userId,
                    createdByRole: $window.sessionStorage.roleId,
                    countryId: $window.sessionStorage.countryId,
                    stateId: $window.sessionStorage.stateId,
                    districtId: $window.sessionStorage.districtId,
                    siteId: $window.sessionStorage.siteId.split(",")[0]
                });

                $scope.Idcount++;

                angular.forEach($scope.memberlists, function (member, index) {
                    member.index = index;

                    Restangular.one('shgs/findOne?filter[where][id]=' + member.shgId).get().then(function (sgs) {

                        if (sgs.groupName != undefined || sgs.groupName != undefined) {

                            $scope.Shgbelongs.push(sgs.groupName);
                            $scope.ShgbelongsId.push(sgs.id);

                            $scope.ssgThisHHBelongs = $scope.Shgbelongs.toString();
                            $scope.household.ssgThisHHBelongs = $scope.ShgbelongsId.toString();
                        }
                        member.shgname = sgs.groupName;
                    });

                    if ($window.sessionStorage.language == 1) {
                        var data = $scope.gendersList.filter(function (arr) {
                            return arr.id == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    } else {
                        var data = $scope.gendersList.filter(function (arr) {
                            return arr.parentId == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    }

                    if ($window.sessionStorage.language == 1) {
                        var data1 = $scope.relationWithHHsLists.filter(function (arr) {
                            return arr.id == member.relation
                        })[0];

                        if (data1 != undefined) {
                            member.relationname = data1.name;
                        }
                    } else {
                        var data1 = $scope.relationWithHHsLists.filter(function (arr) {
                            return arr.parentId == member.relation
                        })[0];

                        if (data1 != undefined) {
                            member.relationname = data1.name;
                        }
                    }
                });

                $scope.beneficiarydataModal = false;
                $scope.memberone = {};
                $scope.validatestring = '';
            }
        };

        var uniqueNames = [];

        $scope.shgDupeCheck = false;

        $scope.EditMember = function (index, id) {
            $scope.HideAdd = true;
            $scope.HideUpdate = false;
            $scope.updateClicked = false;
            var uniqueNames = [];
            $scope.beneficiarydataModal = true;
            $scope.memberone = $scope.memberlists[index];
            $scope.tempMember = Restangular.copy($scope.memberlists);
            $scope.oldGroupid = $scope.memberone.shgId;
            $scope.oldMemberid = $scope.memberone.id;
            $scope.dupCheckFlag = false;
            $scope.shgDupeCheck = false;
        };

        $scope.CLOSEMEMBUTTON = function () {
            $scope.memberlists = $scope.tempMember;
            $scope.beneficiarydataModal = false;
            $scope.memberone = {};
        };

        $scope.CloseAddFamily = function () {
            $scope.beneficiarydataModal = false;
            $scope.memberone = {};
        };

        $scope.memberRemoved = 0;

        $scope.RemoveMember = function (index) {

            $scope.ShgbelongsId = [];

            $scope.memberlists[index].deleteFlag = true;
            $scope.memberlists[index].shgId = null;

            if ($scope.member.shgId != null || $scope.member.shgId != '' || $scope.member.shgId != 0) {
                $scope.ShgbelongsId.push($scope.member.shgId);
                //console.log($scope.ShgbelongsId);
            }

            angular.forEach($scope.memberlists, function (member, index) {
                member.index = index;
                // console.log('member delete', member);

                if (member.deleteFlag == true) {
                    return;
                } else {

                    if (member.shgId == null || member.shgId == '') {
                        return;
                    } else {

                        Restangular.one('shgs/findOne?filter[where][id]=' + member.shgId).get().then(function (sgsid) {
                            $scope.ShgbelongsId.push(sgsid.id);
                            // console.log($scope.ShgbelongsId);
                        });
                    }
                }
            });

            //            var membersShgList = $scope.household.ssgThisHHBelongs.split(",");
            //
            //            for (var i = 0; i < membersShgList.length; i++) {
            //                if (membersShgList[i] == $scope.memberlists[index].shgId) {
            //                    membersShgList.splice(i, 1);
            //                    console.log(membersShgList);
            //                }
            //            }

            // console.log($scope.memberlists[index]);
            //  $scope.memberlists.splice(index, 1);
            $scope.memberRemoved++;
            $scope.Idcount--;
        };

        $scope.UpdateMember = function () {

            $scope.Shgbelongs = [];

            $scope.ShgbelongsId = [];

            $scope.meberArrayData = [];

            $scope.finalMemVal = [];

            document.getElementById('nameone').style.border = "";
            document.getElementById('ageone').style.border = "";

            if ($scope.memberone.name == '' || $scope.memberone.name == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterNameOfMember;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.duplicateMember == true) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.existinMemberNamePleaseChange;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.memberone.relation == '' || $scope.memberone.relation == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectRelationship;
                // document.getElementById('date').style.borderColor = "#FF0000";

            } else if ($scope.memberone.dob == '' || $scope.memberone.dob == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectDOB;

            } else if ($scope.memberone.age === '' || $scope.memberone.age === null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterAge;
                document.getElementById('ageone').style.borderColor = "#FF0000";

            } else if ($scope.memberone.literacyLevel == '' || $scope.memberone.literacyLevel == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterLiteracyLevel;
                // document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.memberone.age <= 18) {
                if ($scope.memberone.nameOfSchool === '' || $scope.memberone.nameOfSchool === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterSchoolName;
                }

            } else if ($scope.hideOccupation == false) {
                if ($scope.memberone.occupation === '' || $scope.memberone.occupation === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectOccupation;
                }

            } else if ($scope.memberone.gender == '' || $scope.memberone.gender == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectGender;
                // document.getElementById('age').style.borderColor = "#FF0000";

            }
            if ($scope.hideFactory == false) {
                if ($scope.memberone.nameOfFactory === '' || $scope.memberone.nameOfFactory === null) {
                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectFactory;
                }

            }
            //            if ($scope.memberone.shgId == '' || $scope.memberone.shgId == null) {
            //                if ($scope.memberone.gender == $scope.mygenderfmale && $scope.memberone.age > 18) {
            //                    $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectSHG;
            //                    $scope.memberone.shgId = '';
            //                }
            //            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.updateClicked = true;
                //  $scope.memberlists[$scope.memberone.index] = $scope.memberone;
                $scope.beneficiarydataModal = false;

                angular.forEach($scope.memberlists, function (member, index) {
                    // console.log('member11111', member);
                    member.index = index;

                    Restangular.one('shgs/findOne?filter[where][id]=' + member.shgId).get().then(function (sgs) {

                        member.shgname = sgs.groupName;

                        if (sgs.groupName != undefined || sgs.groupName != undefined) {

                            $scope.Shgbelongs.push(sgs.groupName);
                            $scope.ShgbelongsId.push(sgs.id);

                            $scope.ssgThisHHBelongs = $scope.Shgbelongs.toString();
                            $scope.household.ssgThisHHBelongs = $scope.ShgbelongsId.toString();
                        }
                    });

                    if ($window.sessionStorage.language == 1) {
                        var data = $scope.gendersList.filter(function (arr) {
                            return arr.id == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    } else {
                        var data = $scope.gendersList.filter(function (arr) {
                            return arr.parentId == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    }

                    if ($window.sessionStorage.language == 1) {
                        var data1 = $scope.relationWithHHsLists.filter(function (arr) {
                            return arr.id == member.relation
                        })[0];

                        if (data1 != undefined) {
                            member.relationname = data1.name;
                        }
                    } else {
                        var data1 = $scope.relationWithHHsLists.filter(function (arr) {
                            return arr.parentId == member.relation
                        })[0];

                        if (data1 != undefined) {
                            member.relationname = data1.name;
                        }
                    }
                });

                if ($scope.ShgbelongsId == 0) {
                    $scope.ssgThisHHBelongs = "";
                    $scope.household.ssgThisHHBelongs = "";
                }

                //    console.log('$scope.household.ssgThisHHBelongs', $scope.household.ssgThisHHBelongs);
                //    console.log('$scope.ssgThisHHBelongs', $scope.ssgThisHHBelongs);

                if ($scope.tempFlag = true) {
                    Restangular.one('shgs', $scope.oldGroupid).get().then(function (sgh) {
                        // console.log('sgh', sgh);
                        $scope.meberArrayData = sgh.membersInGroup.split(',');
                        // console.log('$scope.meberArrayData', $scope.meberArrayData);

                        for (var b = 0; b < $scope.meberArrayData.length; b++) {
                            if ($scope.meberArrayData[b] != $scope.oldMemberid) {
                                $scope.finalMemVal.push($scope.meberArrayData[b]);
                                $scope.shgGrpUpdate.membersInGroup = $scope.finalMemVal.toString();
                                // console.log('$scope.shgGrpUpdate', $scope.shgGrpUpdate);
                            }
                        }
                    });
                }

                $scope.memberone = {};
                $scope.validatestring = '';
            }
        };

        //  $scope.message = 'Household has been updated';

        $scope.shgGrpUpdate = {};

        $scope.validatestring = '';
        $scope.confirmSave = false;
        $scope.hh = {};

        $scope.UpdateProfile = function () {

            $scope.household.nameOfHeadInHH = $scope.member.name;
            $scope.household.HHId = $scope.member.HHId;
            $scope.noOfMems = $scope.memberlists.length + 1;
            $scope.household.noOfMembers = $scope.noOfMems - ($scope.memberRemoved);
            $scope.household.associatedHF = $scope.member.associatedHF;
            //  console.log('$scope.household', $scope.household);


            if ($scope.ShgbelongsId.length == 0) {
                $scope.household.ssgThisHHBelongs = '';
            } else {
                $scope.ShgbelongsId.sort(function (a, b) {
                    return a - b
                });

                var uniqueArrayNew = [$scope.ShgbelongsId[0]];

                for (var i = 1; i < $scope.ShgbelongsId.length; i++) {
                    if ($scope.ShgbelongsId[i - 1] !== $scope.ShgbelongsId[i])
                        uniqueArrayNew.push($scope.ShgbelongsId[i]);
                }

                $scope.household.ssgThisHHBelongs = uniqueArrayNew.toString();
            }

            //            console.log('$scope.household', $scope.household);
            //            console.log('$scope.ShgbelongsId', $scope.ShgbelongsId);

            var re = /\S+@\S+\.\S+/;

            document.getElementById('name').style.border = "";
            document.getElementById('age').style.border = "";
            document.getElementById('address').style.border = "";
            document.getElementById('phoneno').style.borderColor = "";
            document.getElementById('latitude').style.borderColor = "";
            document.getElementById('longitude').style.borderColor = "";

            if ($scope.member.name == '' || $scope.member.name == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.invalidName;
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.duplicateMemberHead == true) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.existinMemberNamePleaseChange;
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.member.dob == '' || $scope.member.dob == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectDOB;
                // document.getElementById('date').style.borderColor = "#FF0000";

            } else if ($scope.member.age === '' || $scope.member.age === null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterAge;
                document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.member.gender == '' || $scope.member.gender == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectGender;
                // document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.member.mobile == '' || $scope.member.mobile == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterDigits;
                document.getElementById('phoneno').style.borderColor = "#FF0000";

            } else if ($scope.member.occupation == '' || $scope.member.occupation == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectOccupation;
                // document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.member.address == '' || $scope.member.address == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectAddress;
                document.getElementById('address').style.borderColor = "#FF0000";

            } else if ($scope.member.religion == '' || $scope.member.religion == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectReligion;

            } else if ($scope.member.migrant == '' || $scope.member.migrant == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectMigrant;

            } else if ($scope.member.bplStatus == '' || $scope.member.bplStatus == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectBPL;

            } else if ($scope.member.latitude == '' || $scope.member.latitude == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterLatitude;
                document.getElementById('latitude').style.borderColor = "#FF0000";

            } else if ($scope.member.longitude == '' || $scope.member.longitude == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterLongitude;
                document.getElementById('longitude').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.hh.displayDOB = $filter('date')($scope.member.dob, 'dd-MMM-yyyy');
                $scope.hh.displayFullNameOfHh = $scope.member.name;
                $scope.hh.displayHouseHoldId = $scope.member.HHId;
                $scope.hh.displayIndividualId = $scope.member.individualId;
                $scope.hh.displayAge = $scope.member.age;
                $scope.hh.displayPhoneNumber = $scope.member.mobile;
                $scope.hh.displayShgsToWhich = $scope.household.ssgThisHHBelongs;
                $scope.hh.displayAddress = $scope.member.address;
                $scope.hh.displaylatlong = $scope.member.latitude + ' / ' + $scope.member.longitude;
                $scope.confirmSave = true;

                if ($window.sessionStorage.language == 1) {

                    Restangular.one('users', $scope.member.associatedHF).get().then(function (udr) {
                        Restangular.one('genders', $scope.member.gender).get().then(function (vdr) {
                            Restangular.one('occupations', $scope.member.occupation).get().then(function (tdr) {
                                Restangular.one('castes', $scope.member.caste).get().then(function (cast) {
                                    Restangular.one('migrants', $scope.member.migrant).get().then(function (migrnt) {
                                        Restangular.one('religions', $scope.member.religion).get().then(function (reg) {
                                            Restangular.one('bplStatuses', $scope.member.bplStatus).get().then(function (bpls) {

                                                $scope.hh.displayAssociateHF = udr.name;
                                                $scope.hh.displayGender = vdr.name;
                                                $scope.hh.displayOccupation = tdr.name;
                                                $scope.hh.displayCaste = cast.name;
                                                $scope.hh.dispalyReligion = reg.name;
                                                $scope.hh.displayMigrant = migrnt.name;
                                                $scope.hh.displayBplStatus = bpls.name;
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });


                } else {

                    Restangular.one('users', $scope.member.associatedHF).get().then(function (udr) {
                        Restangular.one('genders/findOne?filter[where][parentId]=' + $scope.member.gender + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (vdr) {
                            Restangular.one('occupations/findOne?filter[where][parentId]=' + $scope.member.occupation + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (tdr) {
                                Restangular.one('castes/findOne?filter[where][parentId]=' + $scope.member.caste + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (cast) {
                                    Restangular.one('migrants/findOne?filter[where][parentId]=' + $scope.member.migrant + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (migrnt) {
                                        Restangular.one('religions/findOne?filter[where][parentId]=' + $scope.member.religion + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (reg) {
                                            Restangular.one('bplStatuses/findOne?filter[where][parentId]=' + $scope.member.bplStatus + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (bpls) {

                                                $scope.hh.displayAssociateHF = udr.name;
                                                $scope.hh.displayGender = vdr.name;
                                                $scope.hh.displayOccupation = tdr.name;
                                                $scope.hh.displayCaste = cast.name;
                                                $scope.hh.dispalyReligion = reg.name;
                                                $scope.hh.displayMigrant = migrnt.name;
                                                $scope.hh.displayBplStatus = bpls.name;
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                }

            }
        };


        /********************final save**************/
        $scope.SaveConfirmPopUp = function () {

            $scope.toggleLoading();

            $scope.editClicked = true;

            //  $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

            $scope.saveCount = 0;

            if ($scope.tempFlag === true && $scope.oldGroupid != '') {
                Restangular.one('shgs', $scope.oldGroupid).customPUT($scope.shgGrpUpdate).then(function (shgGrp) {
                    // console.log('shgGrp', shgGrp);
                });
            }

            $scope.household.lastModifiedDate = new Date();
            $scope.household.lastModifiedBy = $window.sessionStorage.userId;
            $scope.household.lastModifiedByRole = $window.sessionStorage.roleId;

            Restangular.one('households', $routeParams.id).customPUT($scope.household).then(function (Response) {
                // console.log('Response', Response);
                $scope.member.parentId = Response.id;
                $scope.member.lastModifiedDate = new Date();
                $scope.member.lastModifiedBy = $window.sessionStorage.userId;
                $scope.member.lastModifiedByRole = $window.sessionStorage.roleId;

                if ($scope.member.aadharNumber == '' || $scope.member.aadharNumber == null) {
                    console.log('blank aadhar');
                } else {
                    var aadharNo = $scope.member.aadharNumber.toString();
                    var lastfour = aadharNo.substr(aadharNo.length - 4);
                    var newAadhar = "XXXXXXXX";
                    //$scope.member.aadharNumber = newAadhar + '' + lastfour;
                }

                Restangular.one('members', $scope.memberid).customPUT($scope.member).then(function (Resp) {
                    console.log('Resp', Resp);

                    if ($scope.memberlists.length == 0) {
                        $scope.modalInstanceLoad.close();
                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        console.log('reloading...');

                        setInterval(function () {
                            window.location = "/household-list";
                        }, 1500);
                    } else {
                        $scope.UpdateHH(Response.id, Resp.HHId, Resp.associatedHF);
                    }
                });
            });

        };
        /********************final save**************/

        $scope.updateCount = 0;

        $scope.UpdateHH = function (parId, HHid, associatedHF) {

            if ($scope.updateCount < $scope.memberlists.length) {
                $scope.memberlists[$scope.updateCount].parentId = parId;
                $scope.memberlists[$scope.updateCount].HHId = HHid;
                $scope.memberlists[$scope.updateCount].associatedHF = associatedHF;
                //   $scope.memberlists[$scope.updateCount].mobile = $scope.member.mobile;
                $scope.memberlists[$scope.updateCount].lastModifiedDate = new Date();
                $scope.memberlists[$scope.updateCount].lastModifiedBy = $window.sessionStorage.userId;
                $scope.memberlists[$scope.updateCount].lastModifiedByRole = $window.sessionStorage.roleId;

                // console.log('$scope.updateCount', $scope.updateCount);
                // console.log('$scope.memberlists', $scope.memberlists);

                if ($scope.memberlists[$scope.updateCount].aadharNumber == '' || $scope.memberlists[$scope.updateCount].aadharNumber == null) {
                    console.log('blank aadhar');
                } else {
                    var aadharNo = $scope.memberlists[$scope.updateCount].aadharNumber.toString();
                    var lastfour = aadharNo.substr(aadharNo.length - 4);
                    var newAadhar = "XXXXXXXX";
                    $scope.memberlists[$scope.updateCount].aadharNumber = newAadhar + '' + lastfour;
                }

                Restangular.all('members', $scope.memberlists[$scope.updateCount].id).customPUT($scope.memberlists[$scope.updateCount]).then(function (memResp) {

                    //  console.log('memResp', memResp);

                    if (memResp.shgId == "" || memResp.shgId == null || memResp.shgId == '0' || memResp.shgId == 0) {

                        $scope.updateCount++;
                        $scope.UpdateHH(parId, HHid, associatedHF);

                        if ($scope.updateCount == $scope.memberlists.length) {
                            $scope.modalInstanceLoad.close();
                            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                            console.log('reloading...');

                            setInterval(function () {
                                window.location = "/household-list";
                            }, 1500);
                        }

                    } else if (memResp.deleteFlag == true && (memResp.shgId != "" || memResp.shgId != null || memResp.shgId != '0' || memResp.shgId != 0)) {


                        Restangular.one('shgs', memResp.shgId).get().then(function (shg) {

                            $scope.shgMemUpdate = {
                                id: shg.id
                            }

                            var membersShg = shg.membersInGroup.split(",");

                            for (var i = 0; i < membersShg.length; i++) {
                                if (membersShg[i] == memResp.id) {
                                    membersShg.splice(i, 1);
                                    //console.log(array);
                                }
                            }

                            $scope.shgMemUpdate.membersInGroup = membersShg;

                            //   console.log($scope.shgMemUpdate);

                            Restangular.one('shgs', $scope.shgMemUpdate.id).customPUT($scope.shgMemUpdate).then(function (shgResp) {

                                $scope.updateCount++;
                                $scope.UpdateHH(parId, HHid, associatedHF);

                                if ($scope.updateCount == $scope.memberlists.length) {
                                    $scope.modalInstanceLoad.close();
                                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                    console.log('reloading...');

                                    setInterval(function () {
                                        window.location = "/household-list";
                                    }, 1500);
                                }
                            });
                        });

                    } else {

                        Restangular.one('shgs', memResp.shgId).get().then(function (shg) {

                            $scope.shgbelongs = shg.membersInGroup;

                            if ($scope.shgbelongs === null || $scope.shgbelongs === 'null') {
                                $scope.shgbelongs = "";
                            }

                            $scope.shgUpdate = {
                                id: shg.id,
                                membersInGroup: ""
                            }

                            if ($scope.shgbelongs === "") {
                                $scope.shgUpdate.membersInGroup = memResp.id;
                            } else {
                                $scope.shgUpdate.membersInGroup = shg.membersInGroup + "," + memResp.id;

                                var array = $scope.shgUpdate.membersInGroup.split(",");

                                array.sort(function (a, b) {
                                    return a - b
                                });

                                var uniqueArray = [array[0]];

                                for (var i = 1; i < array.length; i++) {
                                    if (array[i - 1] !== array[i])
                                        uniqueArray.push(array[i]);
                                }

                                $scope.shgUpdate.membersInGroup = uniqueArray.toString();
                            }

                            Restangular.one('shgs', $scope.shgUpdate.id).customPUT($scope.shgUpdate).then(function (shgResp) {
                                // console.log('shgResp', shgResp);

                                $scope.updateCount++;
                                $scope.UpdateHH(parId, HHid, associatedHF);

                                if ($scope.updateCount == $scope.memberlists.length) {
                                    $scope.modalInstanceLoad.close();
                                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                    console.log('reloading...');

                                    setInterval(function () {
                                        window.location = "/household-list";
                                    }, 1500);
                                }
                            });
                        });
                    }
                });
            }
        };

        $scope.clickRadio = function (id, name) {
            $window.sessionStorage.shgMemberId = id;
        };

        $scope.ConfirmSHGMember = function () {
            // console.log('$window.sessionStorage.shgMemberId', $window.sessionStorage.shgMemberId);
            $scope.SHGmodal = false;
            $scope.modalInstanceLoad.close();
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            console.log('reloading...');

            setInterval(function () {
                window.location = $window.sessionStorage.previous;
            }, 1500);
        };

        $('#phoneno').keypress(function (evt) {
            if (/^-?[0-9]\d*(\.\d+)?$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $scope.validationStateId = $window.sessionStorage.zoneId;
        $scope.isCreateView = true;
        $scope.isCreateEdit = true;
        $scope.isCreateSave = false;
        $scope.hideremove = false;
        $scope.DisableSubmit = false;
        $scope.ShowText = false;
        $scope.ShowStart = true;
        $scope.disableapprove = true;
        $scope.hotspotprint = true;
        $scope.createClicked = false;
        $scope.detailsHide = false;
        $scope.createmodifyDisable = true;
        $scope.detailsHide2 = false;
        $scope.showMoreDetails2 = false;
        $scope.hideavahanid = true;
        $scope.actpaid = false;
        $scope.familydeletebuttonhide = true;
        $scope.paralegalproposed_yes = true;
        $scope.boardmemberhide = true;
        $scope.phonetype_disabled = false;

        $scope.auditlog = {
            description: 'Member Create',
            modifiedbyroleid: $window.sessionStorage.roleId,
            modifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            entityroleid: 55,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            facility: $window.sessionStorage.coorgId
        };

        $scope.MemCount = {};
        $scope.reportincident = {};
        /*$scope.newArray = [{ 
                		age: '',
                		gender: '',
                		Stuyding: null,
                		dob: null,
                		beneficiaryid: 0,
                		deleteflag: false
                }];
        	
                $scope.addFamilyMember12345 = function () {
                	console.log('PUSHING.......');
                	$scope.newArray.push({
                		age: '',
                		gender: '',
                		Stuyding: null,
                		dob: null,
                		beneficiaryid: 0,
                		deleteflag: false
                		});
                	};*/
        /*
        		 $scope.showMoreDetails = true;
                $scope.callAPI = true;
                $scope.MoreDetails = function () {
                    $scope.showMoreDetails = !$scope.showMoreDetails;
                     if ($scope.showMoreDetails == false && $scope.callAPI == true) {
                        $scope.callAPI = false;
                        $scope.hotspots = Restangular.all('hotspots?filter[where][state]=' +$window.sessionStorage.zoneId +'&filter[where][district]=' +$window.sessionStorage.salesAreaId +'&filter[where][facility]=' +$window.sessionStorage.coorgId).getList().$object;
                    }
                };*/
        $scope.fwsitehide = true;
        $scope.cosite = true;
        Restangular.one('zones', $window.sessionStorage.zoneId).get().then(function (zone) {
            $scope.zoneName = zone.name;
            $scope.beneficiary.state = zone.id;
            $scope.stateId = zone.code;
            //$scope.SequenceNumber = zone.membercount;
            //$scope.MemCount.membercount = +(zone.membercount) + 1;
            //Restangular.one('zones', zone.id).customPUT($scope.MemCount).then(function () {});
            if ($window.sessionStorage.roleId == 5) {
                $scope.beneficiary.lastmodifiedbyrole = 5;
                $scope.disableapprove = false;
                $scope.fwsitehide = true;
                $scope.cosite = false;
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
                    $scope.beneficiary.facilityId = comember.id;
                    $scope.auditlog.facilityId = comember.id;
                    Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {
                        $scope.facilityName = employee.firstName;
                        //  $scope.beneficiary.avahanid = $scope.stateId + employee.salesCode + $scope.SequenceNumber;
                        /*Restangular.all('routelinks?filter[where][facility]=' + employee.id).getList().then(function (linkresp) {

                        	$scope.arrlink = [];
                        	for (var i = 0; i <= linkresp.length; i++) {
                        		$scope.arrlink.push(linkresp[i].partnerId);
                        		$scope.arrlink.toString();
                        		
                        	}
                        	$scope.sites13 = Restangular.all('distribution-routes?filter={"where":{"and":[{"id":{"inq":[' + $scope.arrlink + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function(rlinl){
                        	$scope.sites = rlinl;
                        		console.log('arrlink', rlinl);
                        });
                        });*/
                        $scope.sites = Restangular.all('distribution-routes?filter[where][partnerId]=' + employee.id).getList().$object;
                    });
                    $scope.$watch('beneficiary.site', function (newValue, oldValue) {
                        if (newValue === oldValue || newValue == '' || newValue == null) {
                            return;
                        } else Restangular.all('routelinks?filter[where][distributionRouteId]=' + newValue).getList().then(function (routeResponse) {
                            Restangular.one('fieldworkers', routeResponse[0].partnerId).get().then(function (fwget) {
                                $scope.beneficiary.fieldworkername = fwget.firstname;
                                $scope.beneficiary.fieldworker = fwget.id;
                            });
                        });
                    });
                    //$scope.modalInstanceLoad.close();
                });
            } else if ($window.sessionStorage.roleId == 15) {
                $scope.beneficiary.lastmodifiedbyrole = 15;
                $scope.fwsitehide = true;
                $scope.cosite = false;
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
                    $scope.beneficiary.facilityId = comember.id;
                    $scope.auditlog.facilityId = comember.id;
                    Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {
                        //$scope.beneficiary.avahanid = $scope.stateId + employee.salesCode + $scope.SequenceNumber;
                        $scope.facilityName = employee.firstName;
                    });
                    $scope.sites = Restangular.all('distribution-routes?filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().$object;
                    $scope.$watch('beneficiary.site', function (newValue, oldValue) {
                        if (newValue === oldValue || newValue == '' || newValue == null) {
                            return;
                        } else Restangular.all('routelinks?filter[where][distributionRouteId]=' + newValue).getList().then(function (routeResponse) {
                            Restangular.one('fieldworkers', routeResponse[0].partnerId).get().then(function (fwget) {
                                //console.log('fwget', fwget);
                                $scope.beneficiary.fieldworkername = fwget.firstname;
                                $scope.beneficiary.fieldworker = fwget.id;
                            });
                        });
                    });
                    $scope.modalInstanceLoad.close();
                });
            } else {
                $scope.beneficiary.lastmodifiedbyrole = 6;
                $scope.fwsitehide = false;
                $scope.cosite = true;
                $scope.disableapprove = true;
                Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    $scope.getsiteid = fw.id;
                    $scope.beneficiary.fieldworkername = fw.firstname;
                    $scope.beneficiary.fieldworker = fw.id;
                    Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                        $scope.beneficiary.facilityId = comember.id;
                        $scope.auditlog.facilityId = comember.id;
                        Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {
                            $scope.facilityName = employee.firstName;
                            // $scope.beneficiary.avahanid = $scope.stateId + employee.salesCode + $scope.SequenceNumber;
                            Restangular.all('routelinks?filter[where][partnerId]=' + $scope.getsiteid).getList().then(function (routeResponse) {
                                $scope.routelinksget = routeResponse;
                                angular.forEach($scope.routelinksget, function (member, index) {
                                    member.index = index + 1;
                                    member.routeName = Restangular.one('distribution-routes', member.distributionRouteId).get().$object;
                                });
                            });
                        });
                        $scope.modalInstanceLoad.close();
                    });
                });
            }
        });
        /******************************* ADD FAMILY MEMBER *****************/

        $scope.DisableGender = false;

        $scope.Hideprintfamiliymember = true;
        $scope.reportincidents = [];
        $scope.healths = [];
        $scope.outreachactivities = [];
        $scope.socialprotections = [];
        $scope.finances = [];
        $scope.UpdateZone = {
            membercount: 0
        };
        $scope.beneficiarydataModal = false;
        $scope.submitcount = 0;
        $scope.validatestring = '';
        $scope.no = {};
        $scope.newhotspot = {};
        $scope.newhotspot.facility = $window.sessionStorage.coorgId;
        $scope.newhotspot.state = $window.sessionStorage.zoneId;
        $scope.newhotspot.district = $window.sessionStorage.salesAreaId;

        $scope.callModal = function () {
            $scope.beneficiarydataModal = !$scope.beneficiarydataModal;
        }
        $scope.OKBUTTON = function () {
            $scope.beneficiarydataModal = !$scope.beneficiarydataModal;
            //$scope.modalToDo.close();
            // window.location = '/members';
        };
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };
        $scope.openOneAlert = function () {
            $scope.modalOneAlert = $modal.open({
                animation: true,
                templateUrl: 'template/AlertModal.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                size: 'sm',
                windowClass: 'modal-danger'
            });
        };

        $scope.okAlert = function () {
            $scope.modalOneAlert.close();
        };

        $scope.dobcount = 0;

        $scope.$watch('member.dob', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                $scope.dobcount++;
                $scope.today = new Date();
                $scope.birthyear = newValue;
                var ynew = $scope.today.getFullYear();
                var mnew = $scope.today.getMonth();
                var dnew = $scope.today.getDate();
                var yold = $scope.birthyear.getFullYear();
                var mold = $scope.birthyear.getMonth();
                var dold = $scope.birthyear.getDate();
                var diff = ynew - yold;
                if (mold > mnew) diff--;
                else {
                    if (mold == mnew) {
                        if (dold > dnew) diff--;
                    }
                }
                $scope.member.age = diff;
            }
        });

        $scope.$watch('member.age', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue >= 0 && newValue <= 99) {
                    document.getElementById('age').style.borderColor = "";
                    if ($scope.dobcount == 0) {
                        var todaydate = new Date();
                        var newdate = new Date(todaydate);
                        newdate.setFullYear(newdate.getFullYear() - newValue);
                        var nd = new Date(newdate);
                        $scope.member.dob = nd;
                    } else {
                        $scope.dobcount = 0;
                    }
                } else {
                    //alert('Invalid Age');
                    $scope.AlertMessage = $scope.invalid + ' ' + $scope.printage;
                    $scope.member.dob = null;
                    $scope.member.age = null;
                }

            }
        });

        $scope.$watch('member.migrant', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue == 1) {
                    $scope.migrantdataModal = false;
                }
            }
        });

        $scope.newValue = function (MigrantFunc) {
            //  console.log(MigrantFunc);
            if (MigrantFunc == 'yes') {
                $scope.member.migrant = 1;
            } else if (MigrantFunc == 'no') {
                $scope.member.migrant = 2;
            }
        };

        $scope.ConfirmMigrant = function () {
            // console.log('$scope.member.migrant', $scope.member.migrant);
            $scope.migrantdataModal = false;
        };

        $scope.CLOSEBUTTON = function () {
            $scope.migrantdataModal = false;
        };

        $scope.disableSHG = true;

        $scope.tempFlag = false;

        $scope.$watch('memberone.relation', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id && (newValue == 1 || newValue == 3 || newValue == 5 || newValue == 7 || newValue == 9)) {

                if ($scope.oldGroupid == null || $scope.oldGroupid == '' || $scope.oldGroupid == undefined) {
                    $scope.tempFlag = false;
                    //  console.log('$scope.oldGroupid', $scope.oldGroupid);
                    //  console.log('$scope.tempFlag', $scope.tempFlag);
                } else {
                    $scope.tempFlag = true;
                    //  console.log('$scope.oldGroupid', $scope.oldGroupid);
                    // console.log('$scope.tempFlag', $scope.tempFlag);
                }

                $scope.disableSHG = true;
                $scope.memberone.shgId = '';
                $scope.memberone.gender = 1;
            } else {
                if (newValue == 1 && $scope.memberone.age < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if (newValue == 3 && $scope.memberone.age < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if (newValue == 5 && $scope.memberone.age < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if (newValue == 7 && $scope.memberone.age < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if (newValue == 9 && $scope.memberone.age < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';
                } else {
                    //$scope.disableSHG = false;
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 2;
                }
            }
        });

        $scope.hideOccupation = true;
        $scope.hideFactory = true;

        $scope.$watch('memberone.age', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {

                if (newValue >= 0 && newValue <= 99) {
                    document.getElementById('ageone').style.borderColor = "";
                    if ($scope.dobcount == 0) {
                        var todaydate = new Date();
                        var newdate = new Date(todaydate);
                        newdate.setFullYear(newdate.getFullYear() - newValue);
                        var nd = new Date(newdate);
                        $scope.memberone.dob = nd;
                    } else {
                        $scope.dobcount = 0;
                    }
                } else {
                    //alert('Invalid Age');
                    $scope.AlertMessage = $scope.invalid + ' ' + $scope.printage;
                    $scope.memberone.dob = null;
                    $scope.memberone.age = null;
                }

                if ($scope.memberone.relation == 1 || $scope.memberone.relation == 3 || $scope.memberone.relation == 5 || $scope.memberone.relation == 7 || $scope.memberone.relation == 9) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 1 && newValue < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 3 && newValue < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 5 && newValue < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 7 && newValue < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 9 && newValue < 18) {
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 1;
                    $scope.memberone.shgId = '';

                } else {
                    // $scope.disableSHG = false;
                    $scope.disableSHG = true;
                    $scope.memberone.gender = 2;
                }

                if (newValue >= 18) {
                    $scope.disableSchool = true;
                    // $scope.memberone.nameOfSchool = '';
                } else {
                    $scope.disableSchool = false;
                }

                if (newValue >= 14) {
                    $scope.hideOccupation = false;
                } else {
                    $scope.hideOccupation = true;
                    $scope.memberone.occupation = '';
                    $scope.hideFactory = true;
                    $scope.memberone.nameOfFactory = '';
                }
            }
        });

        $scope.$watch('memberone.occupation', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {

                if (newValue == 2) {
                    $scope.hideFactory = false;
                    // console.log('$scope.hideFactory', $scope.hideFactory);
                    if ($scope.UserLanguage == 1) {
                        $scope.memberone.nameOfFactory = $scope.nameOfFactories[0].id;
                    } else {
                        $scope.memberone.nameOfFactory = $scope.nameOfFactories[0].parentId;
                    }
                } else {
                    $scope.hideFactory = true;
                    $scope.memberone.nameOfFactory = '';
                }
                //console.log('$scope.hideFactory', $scope.hideFactory);
            }
        });

        $scope.dobcountone = 0;

        $scope.$watch('memberone.dob', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                $scope.dobcountone++;
                $scope.today = new Date();
                $scope.birthyear = newValue;
                var ynew = $scope.today.getFullYear();
                var mnew = $scope.today.getMonth();
                var dnew = $scope.today.getDate();
                var yold = $scope.birthyear.getFullYear();
                var mold = $scope.birthyear.getMonth();
                var dold = $scope.birthyear.getDate();
                var diff = ynew - yold;
                if (mold > mnew) diff--;
                else {
                    if (mold == mnew) {
                        if (dold > dnew) diff--;
                    }
                }
                $scope.memberone.age = diff;

                if ($scope.memberone.age <= 18) {
                    $scope.disableSchool = false;
                    $scope.disableSHG = true;
                    $scope.memberone.shgId = '';

                } else if ($scope.memberone.relation == 1 || $scope.memberone.relation == 3 || $scope.memberone.relation == 5 || $scope.memberone.relation == 7 || $scope.memberone.relation == 9) {
                    $scope.disableSHG = true;
                    $scope.memberone.shgId = '';
                }

                if ($scope.memberone.age >= 14) {
                    $scope.hideOccupation = false;
                } else {
                    $scope.hideOccupation = true;
                    $scope.memberone.occupation = '';
                    $scope.hideFactory = true;
                    $scope.memberone.nameOfFactory = '';
                }
            }
        });

        $scope.countnew = 0;

        $scope.dupCheckFlag == false;

        $scope.$watch('memberone.shgId', function (newValue, oldValue) {
            if (newValue == null || newValue == '' || newValue == oldValue) {
                $scope.countnew++;
                return;
            } else if ($routeParams.id && ($scope.memberone.relation == 2 || $scope.memberone.relation == 4 || $scope.memberone.relation == 6 || $scope.memberone.relation == 8 || $scope.memberone.relation == 10)) {
                $scope.tempFlag = true;
                $scope.countnew++;

                if ($scope.dupCheckFlag == true) {
                    for (var w = 0; w < $scope.memberlists.length; w++) {
                        if ($scope.memberlists[w].name == $scope.memberone.name && $scope.memberlists[w].age == $scope.memberone.age && $scope.memberlists[w].relation == $scope.memberone.relation) {
                            alert($scope.HHLanguage.sameMemMultipleSHG);
                            $scope.memberone = {};
                        }
                    }
                }
            }
        });

        /********************************* FamilyMember Change **************/

        $scope.FamilyGenderChanges = false;
        $scope.CheckGender = function (scope, index, agee, famId) {
            $scope.FamilyGenderChanges = true;
            if (scope.familymember.gender >= 1 && scope.familymember.Age != undefined && scope.familymember.Age != '') {
                return;
            } else if (agee <= 0 || agee == '') {
                //$scope.AlertMessage = 'Enter Children Age';
                //$scope.openOneAlert();
            }
        }
        $scope.FamilyAgeChanges = false;
        $scope.ChilredAge = function (scope, index) {
            $scope.FamilyAgeChanges = true;
            if (scope.familymember.age >= 0 && scope.familymember.age <= 99) {
                if ($scope.dobcount == 0) {
                    var todaydate = new Date();
                    var newdate = new Date(todaydate);
                    newdate.setFullYear(newdate.getFullYear() - scope.familymember.age);
                    var nd = new Date(newdate);
                    $scope.familymembers[index].dob = nd;
                } else {
                    $scope.dobcount = 0;
                }
            } else {
                //alert('Invalid Age');
                $scope.AlertMessage = 'Invalid Age'; //'Select Date Of Birth';
                $scope.openOneAlert();
                $scope.familymember[index].age = null;
                $scope.familymember[index].dob = null;
            }
        };
        $scope.FamilyDOBChanges = false;
        $scope.ChilredDOB = function (scope, index) {
            //console.log('scope', scope);
            $scope.FamilyDOBChanges = true;
            $scope.dobcount++;
            $scope.today = new Date();
            $scope.birthyear = scope.familymember.dob;
            var ynew = $scope.today.getFullYear();
            var mnew = $scope.today.getMonth();
            var dnew = $scope.today.getDate();
            var yold = $scope.birthyear.getFullYear();
            var mold = $scope.birthyear.getMonth();
            var dold = $scope.birthyear.getDate();
            var diff = ynew - yold;
            if (mold > mnew) diff--;
            else {
                if (mold == mnew) {
                    if (dold > dnew) diff--;
                }
            }
            $scope.familymembers[index].age = diff;
        }
        $scope.FamilyStudyChanges = false;
        $scope.CheckStudyIn = function (idstd) {
            $scope.FamilyStudyChanges = true;
        }
        console.log('$scope.familymembers', $scope.familymembers);
        $scope.familymembers = [];
        $scope.familymembers = [{
            age: '',
            gender: '',
            Stuyding: null,
            dob: null,
            beneficiaryid: 0,
            deleteflag: false
  }];
        //$scope.newArray = [];
        $scope.checkAddFamily = false;
        $scope.addFamilyMember = function () {
            $scope.checkAddFamily = true;
            $scope.familymembers.push({
                age: '',
                gender: '',
                Stuyding: null,
                dob: null,
                beneficiaryid: 0,
                deleteflag: false
            });
            $scope.totalfamilymembercount = $scope.familymembers.length;
        };
        $scope.totalfamilymembercount = $scope.familymembers.length;
        $scope.removeFamilyMember = function (index) {
            var item = $scope.familymembers[index];
            if (index == 0) {
                //alert('Can not Delete..!');
                $scope.AlertMessage = $scope.cannot + ' ' + $scope.deletebutton; //'Select Date Of Birth';
                $scope.openOneAlert();
            } else {
                $scope.familymembers.splice(index, 1);
            }
        };
        /**********************************************/
        $scope.$watch('beneficiary.age', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue >= 0 && newValue <= 99) {
                    if ($scope.dobcount == 0) {
                        var todaydate = new Date();
                        var newdate = new Date(todaydate);
                        newdate.setFullYear(newdate.getFullYear() - newValue);
                        var nd = new Date(newdate);
                        $scope.beneficiary.dob = nd;
                    } else {
                        $scope.dobcount = 0;
                    }
                } else {
                    //alert('Invalid Age');
                    $scope.AlertMessage = $scope.invalid + ' ' + $scope.printage;
                    $scope.beneficiary.dob = null;
                    $scope.beneficiary.age = null;
                }
            }
        });
        $scope.HideMonthofDetection = true;
        $scope.$watch('beneficiary.plhiv', function (newValue, oldValue) {
            //console.log('newValue', newValue);
            if (newValue === oldValue) {
                return;
            } else if (newValue === true) {
                $scope.HideMonthofDetection = false;
            } else {
                $scope.HideMonthofDetection = true;
            }
        });
        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};
        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };
        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };
        $scope.openfamilydob = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#familydobopen' + index).focus();
            });
            item.familydobopened = true;
        };
        $scope.openmod = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickermod' + index).focus();
            });
            $scope.mod.openedmod = true;
        };
        $scope.openstart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };
        $scope.incidentopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerincident' + index).focus();
            });
            $scope.incident.incidentopened = true;
        };
        $scope.healthopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerhealth' + index).focus();
            });
            $scope.hlth.healthopened = true;
        };
        $scope.opendatestartedatart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdatestartedatart' + index).focus();
            });
            $scope.datestartedart.openeddatestartedatart = true;
        };
        $scope.lasttestdateopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.lasttest.lasttestdateopened = true;
        };
        $scope.termstartdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termstartdate.dobopened = true;
        };
        $scope.termenddate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termenddate.dobopened = true;
        };
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];
        //Datepicker settings end
        /****************************** Language ***************************************/
        $scope.UserLanguage = $window.sessionStorage.language;
        $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.divbasic = langResponse.divbasic;
            $scope.printfullname = langResponse.fullname;
            $scope.printnickname = langResponse.nickname;
            $scope.printage = langResponse.age;
            $scope.printdob = langResponse.dob;
            $scope.printmaritalstatus = langResponse.maritalstatus;
            $scope.printphonenumber = langResponse.phonenumber;
            $scope.anotherphonenumber = langResponse.anotherphonenumber;
            $scope.heading = langResponse.heading;
            $scope.mandatoryfield = langResponse.mandatoryfield;
            $scope.divextended = langResponse.divextended;
            $scope.printgender = langResponse.gender;
            $scope.printtypology = langResponse.typology;
            $scope.printmyplhiv = langResponse.plhivprint;
            $scope.printmonthofdetection = langResponse.monthofdetection;
            $scope.printstartsexwork = langResponse.startsexwork;
            $scope.printphysicaldisable = langResponse.physicaldisable;
            $scope.printmentaldisable = langResponse.mentaldisable;
            $scope.divfacility = langResponse.divfacility;
            $scope.printstate = langResponse.state;
            $scope.printfacility = langResponse.facility;
            $scope.printsite = langResponse.site;
            $scope.printfw = langResponse.fw;
            $scope.photspot = langResponse.hotspot;
            $scope.printtown = langResponse.town;
            $scope.printavahanid = langResponse.avahanid;
            $scope.printtiid = langResponse.tiid;
            $scope.divmembername = langResponse.divmembername;
            $scope.printmydynamicmember = langResponse.dynamicmember;
            $scope.printmypaidmember = langResponse.paidmember;
            $scope.printchampion = langResponse.champion;
            $scope.proposed = langResponse.proposed;
            $scope.approved = langResponse.approved;
            $scope.create = langResponse.create;
            $scope.update = langResponse.update;
            $scope.cancel = langResponse.cancel;
            $scope.deletebutton = langResponse.delete;
            $scope.printmemberlist = langResponse.memberlist;
            $scope.memberregistration = langResponse.memberregistration;
            $scope.yes = langResponse.yes;
            $scope.nomessage = langResponse.no;
            $scope.ok = langResponse.ok;
            $scope.printgroupname = langResponse.groupname;
            $scope.printfamiliymember = langResponse.familiymember;
            $scope.printname = langResponse.name;
            $scope.action = langResponse.action;
            $scope.addbutton = langResponse.addbutton;
            $scope.removebutton = langResponse.removebutton;
            $scope.printstudyingin = langResponse.studyingin;
            $scope.printphone = langResponse.phone;
            $scope.trained = langResponse.trained;
            $scope.printphone = langResponse.phone;
            $scope.trained = langResponse.trained;
            $scope.paralegalvolunteer = langResponse.paralegalvolunteer;
            $scope.boardmemberever = langResponse.boardmemberever;
            $scope.hasplvidcard = langResponse.hasplvidcard;
            $scope.startdate = langResponse.startdate;
            $scope.emddate = langResponse.emddate;
            $scope.nooftermsheld = langResponse.nooftermsheld;
            $scope.printdynamicmemberlabel = langResponse.dynamicmember;
            $scope.pchildren = langResponse.children;
            $scope.please = langResponse.please;
            $scope.enter = langResponse.enter;
            $scope.your = langResponse.your;
            $scope.invalid = langResponse.invalid;
            $scope.cannot = langResponse.cannot;
            $scope.difference = langResponse.difference;
            $scope.nameconsistword = langResponse.nameconsistword
            $scope.diffrentage = langResponse.difference;
            $scope.select = langResponse.select;
            $scope.onlytwono = langResponse.onlytwono
            $scope.nonumtoremove = langResponse.nonumtoremove;
            //$scope.title1 = langResponse.alert; = 
            //suman
            $scope.selage = langResponse.selage;
            $scope.peyfullname = langResponse.peyfullname;
            $scope.Seldob = langResponse.Seldob;
            $scope.selfacility = langResponse.selfacility;
            $scope.selmaritalstatus = langResponse.selmaritalstatus;
            $scope.selphonetype = langResponse.selphonetype;
            $scope.seltypology = langResponse.seltypology;
            $scope.selgender = langResponse.selgender;
            $scope.selstate = langResponse.selstate;
            $scope.selsite = langResponse.selsite;
            $scope.selfw = langResponse.selfw;
            $scope.enterhotspot = langResponse.enterhotspot;
            $scope.seltown = langResponse.seltown;
            $scope.enternoofterm = langResponse.enternoofterm;
            $scope.enterchildage = langResponse.enterchildage;
            $scope.selchilddob = langResponse.selchilddob;
            $scope.enterchildgender = langResponse.enterchildgender;
        });
        //    if ($window.sessionStorage.language == 1) {
        //        $scope.modalTitle = 'Created With the Following Details';
        //    } else if ($window.sessionStorage.language == 2) {
        //        $scope.modalTitle = 'निम्न विवरण के साथ बनाया';
        //    } else if ($window.sessionStorage.language == 3) {
        //        $scope.modalTitle = 'ಕೆಳಗಿನ ವಿವರಗಳಿಂದ  ರಚಿಸಲಾಗಿದೆा';
        //    } else if ($window.sessionStorage.language == 4) {
        //        $scope.modalTitle = 'பின்வரும் விவரங்கள் உடன் உருவாக்கப்பட்டதுा';
        //    } else if ($window.sessionStorage.language == 5) {
        //        $scope.modalTitle = 'క్రింది వివరాలను తో రూపొందించబడింది';
        //    } else if ($window.sessionStorage.language == 6) {
        //        $scope.modalTitle = 'ह्या माहिती प्रमाणे करनायत आलेळे आहे';
        //    }



        /******************************google map****************************/
        $scope.LocateMe = function () {
            $scope.mapdataModal = true;

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 4,

                center: new google.maps.LatLng(12.9538477, 77.3507369),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            var marker, i;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(12.9538477, 77.3507369),
                map: map,
                html: ''
            });

            $scope.toggleMapModal();
        };

        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            ///////////////////////////////////////////////////////MAP//////////////////////////

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                //  console.log(latLng);
                $scope.member.latitude = latLng.lat();
                $scope.member.longitude = latLng.lng();

                //  console.log('$scope.updatepromotion', $scope.updatepromotion);

                document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
            }
            var map;

            function initialize() {

                $scope.latitude = 12.9538477;
                $scope.longitude = 77.3507369;
                navigator.geolocation.getCurrentPosition(function (location) {
                    //                    console.log(location.coords.latitude);
                    //                    console.log(location.coords.longitude);
                    //                    console.log(location.coords.accuracy);
                    $scope.latitude = location.coords.latitude;
                    $scope.longitude = location.coords.longitude;
                    //                });

                    // console.log('$scope.address', $scope.address);

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 4,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });
                });


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(10);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                //  console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };
    });
