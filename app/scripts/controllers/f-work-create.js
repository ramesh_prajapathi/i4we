'use strict';
angular.module('secondarySalesApp').controller('FWCreateCtrl', function ($scope, Restangular, $window, $filter, $modal, AnalyticsRestangular) {

    $scope.HfLanguage = {};

    Restangular.one('hflanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.HfLanguage = langResponse[0];
        $scope.hfHeading = langResponse[0].hfCreate;
    });

    Restangular.all('users').getList().then(function (user) {
        $scope.userslist = user;
    });

    $scope.isCreateView = true;
    $scope.heading = 'Field Worker Create';
    $scope.HideUsername = false;
    $scope.UpdateClicked = false;
    $scope.hideSitesAssign = true;

    $scope.hf = {
        status: 'active',
        typeOfDoctor: 0,
        emailVerified: false,
        roleId: 3,
        createdDate: new Date(),
        createdBy: $window.sessionStorage.userId,
        createdByRole: $window.sessionStorage.roleId,
        lastModifiedDate: new Date(),
        lastModifiedBy: $window.sessionStorage.userId,
        lastModifiedByRole: $window.sessionStorage.roleId,
        countryId: $window.sessionStorage.countryId,
        stateId: $window.sessionStorage.stateId,
        districtId: $window.sessionStorage.districtId,
        siteId: $window.sessionStorage.siteId.split(",")[0],
        deleteFlag: false
    };

    $scope.auditlog = {
        description: 'Health Facilitator Create',
        action: 'Insert',
        module: 'Health Facilitator',
        owner: $window.sessionStorage.userId,
        datetime: new Date(),
        modifiedbyroleid: $window.sessionStorage.roleId,
        modifiedby: $window.sessionStorage.userId,
        lastmodifiedtime: new Date(),
        entityroleid: 55,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        facility: $window.sessionStorage.coorgId
    };

    /********************************************************************/

    Restangular.one('countries', $window.sessionStorage.countryId).get().then(function (cn) {
        $scope.countryName = cn.name;
        $scope.hf.countryId = cn.id;
    });

    Restangular.one('states', $window.sessionStorage.stateId).get().then(function (st) {
        $scope.stateName = st.name;
        $scope.hf.stateId = st.id;
    });

    Restangular.one('districts', $window.sessionStorage.districtId).get().then(function (dt) {
        $scope.districtName = dt.name;
        $scope.hf.districtId = dt.id;
    });

    Restangular.one('sites', $window.sessionStorage.siteId.split(",")[0]).get().then(function (ste) {
        $scope.siteName = ste.name;
        $scope.hf.siteId = ste.id;
    });

    Restangular.one('users', $window.sessionStorage.userId).get().then(function (stm) {
        $scope.sitemgrName = stm.name;
        $scope.modalInstanceLoad.close();
    });


    $scope.duplicateusername = false;
    $scope.duplicateemail = false;

    $scope.checkuser = function (username) {

        var data = $scope.userslist.filter(function (arr) {
            return arr.username == username
        })[0];

        if (data != undefined) {
            $scope.duplicateusername = true;
        } else {
            $scope.duplicateusername = false;
        }
    };

    $scope.checkemail = function (email) {

        var data1 = $scope.userslist.filter(function (arr) {
            return arr.email == email
        })[0];

        if (data1 != undefined) {
            $scope.duplicateemail = true;
        } else {
            $scope.duplicateemail = false;
        }
    };

    /************************************ SAVE *******************************************/

    $scope.validatestring = '';

    $scope.createDisabled = false;

    $scope.dataModal = false;

    $scope.Save = function () {

        document.getElementById('firstname').style.border = "";
        document.getElementById('lastname').style.border = "";
        document.getElementById('email').style.border = "";
        document.getElementById('mobile').style.border = "";
        document.getElementById('password').style.border = "";

        if ($scope.hf.username == '' || $scope.hf.username == null) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.enterusername;
            document.getElementById('firstname').style.border = "1px solid #ff0000";

        } else if ($scope.duplicateusername == true) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.usernameexist;
            document.getElementById('firstname').style.border = "1px solid #ff0000";

        } else if ($scope.hf.name == '' || $scope.hf.name == null) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.enterfullname;
            document.getElementById('lastname').style.border = "1px solid #ff0000";

        } else if ($scope.hf.email == '' || $scope.hf.email == null) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.enteremail;
            document.getElementById('email').style.border = "1px solid #ff0000";

        } else if ($scope.duplicateemail == true) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.emailexist;
            document.getElementById('email').style.border = "1px solid #ff0000";

        } else if ($scope.hf.mobile == '' || $scope.hf.mobile == null) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.entermobile;
            document.getElementById('mobile').style.border = "1px solid #ff0000";

        } else if ($scope.hf.password == '' || $scope.hf.password == null) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.enterpassword;
            document.getElementById('password').style.border = "1px solid #ff0000";

        } else if ($scope.hf.languageId == '' || $scope.hf.languageId == null) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.selectLanguage;
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {

            $scope.toggleLoading();

            $scope.UpdateClicked = true;
            $scope.createDisabled = true;

            Restangular.all('users').post($scope.hf).then(function (memResp) {

                Restangular.all('audittrials').post($scope.auditlog).then(function (auditResp) {
                    
                    $scope.modalInstanceLoad.close();
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('reloading...');

                    setInterval(function () {
                        window.location = "/healthfacilitator-list";
                    }, 1500);
                });
            });
        }
    };

    $scope.showValidation = false;

    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };

    /********************************************* Map ******************************/
    $scope.showMapModal = false;
    $scope.toggleMapModal = function () {
        $scope.mapcount = 0;
        var geocoder = new google.maps.Geocoder();

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function (responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        }

        function updateMarkerStatus(str) {
            document.getElementById('markerStatus').innerHTML = str;
        }

        function updateMarkerPosition(latLng) {
            $scope.fieldworker.latitude = latLng.lat() + ',' + latLng.lng();
            $scope.fieldworker.longitude = latLng.lng();
            document.getElementById('info').innerHTML = [
    latLng.lat()
    , latLng.lng()
  ].join(', ');
        }

        function updateMarkerAddress(str) {
            document.getElementById('mapaddress').innerHTML = str;
        }
        var map;

        function initialize() {
            $scope.address = $scope.fieldworker.address;
            // console.log('$scope.address', $scope.address);
            $scope.latitude = 21.0000;
            $scope.longitude = 78.0000;
            if ($scope.address.length > 0) {
                var addressgeocoder = new google.maps.Geocoder();
                addressgeocoder.geocode({
                    'address': $scope.address
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $scope.latitude = parseInt(results[0].geometry.location.lat());
                        $scope.longitude = parseInt(results[0].geometry.location.lng());
                        //console.log($scope.latitude, $scope.longitude);
                        var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                        map = new google.maps.Map(document.getElementById('mapCanvas'), {
                            zoom: 4,
                            center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });
                        var marker = new google.maps.Marker({
                            position: latLng,
                            title: 'Point A',
                            map: map,
                            draggable: true
                        });
                        // Update current position info.
                        updateMarkerPosition(latLng);
                        geocodePosition(latLng);
                        // Add dragging event listeners.
                        google.maps.event.addListener(marker, 'dragstart', function () {
                            updateMarkerAddress('Dragging...');
                        });
                        google.maps.event.addListener(marker, 'drag', function () {
                            updateMarkerStatus('Dragging...');
                            updateMarkerPosition(marker.getPosition());
                        });
                        google.maps.event.addListener(marker, 'dragend', function () {
                            updateMarkerStatus('Drag ended');
                            geocodePosition(marker.getPosition());
                        });
                    }
                });
            } else {
                $scope.latitude = 21.0000;
                $scope.longitude = 78.0000;
                var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                map = new google.maps.Map(document.getElementById('mapCanvas'), {
                    zoom: 4,
                    center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var marker = new google.maps.Marker({
                    position: latLng,
                    title: 'Point A',
                    map: map,
                    draggable: true
                });
                // Update current position info.
                updateMarkerPosition(latLng);
                geocodePosition(latLng);
                // Add dragging event listeners.
                google.maps.event.addListener(marker, 'dragstart', function () {
                    updateMarkerAddress('Dragging...');
                });
                google.maps.event.addListener(marker, 'drag', function () {
                    updateMarkerStatus('Dragging...');
                    updateMarkerPosition(marker.getPosition());
                });
                google.maps.event.addListener(marker, 'dragend', function () {
                    updateMarkerStatus('Drag ended');
                    geocodePosition(marker.getPosition());
                });
            }
        }
        // Onload handler to fire off the app.
        //google.maps.event.addDomListener(window, 'load', initialize);
        initialize();
        window.setTimeout(function () {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
            map.setZoom(4);
        }, 1000);
        $scope.SaveMap = function () {
            $scope.showMapModal = !$scope.showMapModal;
            console.log($scope.reportincident);
        };
        //console.log('fdfd');
        $scope.showMapModal = !$scope.showMapModal;
    };

    $scope.CancelMap = function () {
        if ($scope.mapcount == 0) {
            $scope.showMapModal = !$scope.showMapModal;
            $scope.mapcount++;
        }
    };

});
