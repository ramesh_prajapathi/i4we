'use strict';

angular.module('secondarySalesApp')
    .controller('DynamicSchoolCheckListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 9) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/dynamicschoolchecklist/create' || $location.path() === '/dynamicschoolchecklist/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/dynamicschoolchecklist/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/dynamicschoolchecklist/create' || $location.path() === '/dynamicschoolchecklist/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/dynamicschoolchecklist/create' || $location.path() === '/dynamicschoolchecklist/edit/' + $routeParams.id;
            return visible;
        };


        /*********/

        if ($routeParams.id) {

            $scope.message = 'Dynamic Checklist has been Updated!';

            Restangular.one('dynamiclists', $routeParams.id).get().then(function (dynamiclist) {
                console.log('dynamiclist', dynamiclist);
                $scope.original = dynamiclist;
                $scope.dynamiclist = Restangular.copy($scope.original);

            });

            Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablangs) {
                $scope.tablanguages1 = tablangs;
                $scope.tablanguages1.splice(0, 1);
                $scope.currValue = 0;

                Restangular.all('dynamiclists?filter[where][deleteFlag]=false&filter[where][parentId]=' + $routeParams.id + '&filter[where][parentFlag]=false').getList().then(function (intvls1) {
                    $scope.mtlangs1 = intvls1;

                    angular.forEach($scope.tablanguages1, function (member, index) {
                        member.index = index;

                        var data = $scope.mtlangs1.filter(function (arr) {
                            return arr.language == member.id
                        })[0];

                        if (data == undefined) {
                            $scope.rowArray.push({
                                language: member.id,
                                id: 0
                            });
                        } else {
                            $scope.rowArray.push(data);
                        }
                    });
                });
            });

        } else {
            $scope.message = 'Dynamic Checklist has been Created!';
        }
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/dynamicschoolchecklist") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }


        $scope.showenglishLang = true;
        $scope.OtherLang = true;

        $scope.$watch('dynamiclist.question', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id) {
                return;
            } else {
                angular.forEach($scope.rowArray, function (member, index) {
                    member.index = index;
                    member.question = newValue;
                });
            }
        });

        $scope.$watch('dynamiclist.option', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id) {
                return;
            } else {
                angular.forEach($scope.rowArray, function (member, index) {
                    member.index = index;
                    member.option = newValue;
                });
            }
        });

        $scope.$watch('dynamiclist.language', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });

        $scope.$watch('dynamiclist.parentId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.one('dynamiclists', newValue).get().then(function (sts) {
                    $scope.dynamiclist.orderNo = sts.orderNo;
                    $scope.dynamiclist.mandatoryFlag = sts.mandatoryFlag;

                });
            }
        });
        /******************New Added ************************/

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];

        /******************************** INDEX *******************************************/

        Restangular.all('dynamiclists?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (dyn) {
            $scope.dynamiclists = dyn;
            angular.forEach($scope.dynamiclists, function (member, index) {
                member.index = index + 1;

                Restangular.one('languages', member.language).get().then(function (lng) {
                    member.langname = lng.name;
                });
            });
        });

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (sev) {
            $scope.dynlanguages = sev;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        $scope.rowArray = [];

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablang) {
            $scope.tablanguages = tablang;
            $scope.tablanguages.splice(0, 1);
            $scope.currValue = 0;

            angular.forEach($scope.tablanguages, function (member, index) {
                member.index = index;

                if (!$routeParams.id) {
                    $scope.rowArray.push({
                        language: member.id,
                        question: '',
                        option: ''
                    });
                }

                if (member.index == 0) {
                    member.active = true;
                    member.visible = true;
                } else {
                    member.active = false;
                    member.visible = false;
                }
            });
        });

        $scope.tabClick = function (row) {
            $scope.tablanguages[row.index].visible = true;
            if (row.index != 0) {
                $scope.tablanguages[0].visible = false;
            }
            $scope.currValue = row.index;
        };

        /********************************************* SAVE *******************************************/

        $scope.dynamiclist = {
            "name": '',
            "deleteFlag": false,
            "mandatoryFlag": false,
            "language": 1
        };

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {

            document.getElementById('question').style.border = "";
            document.getElementById('option').style.border = "";
            document.getElementById('seq').style.border = "";

            if ($scope.dynamiclist.question == '' || $scope.dynamiclist.question == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Question';
                document.getElementById('question').style.borderColor = "#FF0000";

            } else if ($scope.dynamiclist.option == '' || $scope.dynamiclist.option == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Option';
                document.getElementById('option').style.borderColor = "#FF0000";

            } else if ($scope.dynamiclist.orderNo == '' || $scope.dynamiclist.orderNo == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Sequence';
                document.getElementById('seq').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.dynamiclist.parentId === '') {
                    delete $scope.dynamiclist['parentId'];
                }

                $scope.submitDisable = true;
                $scope.dynamiclist.parentFlag = $scope.showenglishLang;

                Restangular.all('dynamiclists').post($scope.dynamiclist).then(function (response) {
                    $scope.langFunc(response.id, response.orderNo, response.mandatoryFlag);
                }, function (error) {
                    if (error.data.error.constraint === 'dynamiclist_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        $scope.langcount = 0;

        $scope.langFunc = function (intervalId, orderNo, mandatoryFlag) {

            if ($scope.langcount < $scope.rowArray.length) {

                $scope.rowArray[$scope.langcount].parentFlag = false;
                $scope.rowArray[$scope.langcount].parentId = intervalId;
                $scope.rowArray[$scope.langcount].orderNo = orderNo;
                $scope.rowArray[$scope.langcount].mandatoryFlag = mandatoryFlag;
                $scope.rowArray[$scope.langcount].deleteFlag = false;

                Restangular.all('dynamiclists').post($scope.rowArray[$scope.langcount]).then(function () {
                    $scope.langcount++;
                    $scope.langFunc(intervalId, orderNo, mandatoryFlag);
                });

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/dynamicschoolchecklist';
            }
        };

        /***************************************************** UPDATE *******************************************/

        $scope.Update = function () {

            document.getElementById('question').style.border = "";
            document.getElementById('option').style.border = "";
            document.getElementById('seq').style.border = "";

            if ($scope.dynamiclist.question == '' || $scope.dynamiclist.question == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Question';
                document.getElementById('question').style.borderColor = "#FF0000";

            } else if ($scope.dynamiclist.option == '' || $scope.dynamiclist.option == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Option';
                document.getElementById('option').style.borderColor = "#FF0000";

            } else if ($scope.dynamiclist.orderNo == '' || $scope.dynamiclist.orderNo == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Sequence';
                document.getElementById('seq').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.dynamiclist.parentId === '') {
                    delete $scope.dynamiclist['parentId'];
                }

                $scope.submitDisable = true;

                Restangular.all('dynamiclists', $routeParams.id).customPUT($scope.dynamiclist).then(function (resp) {
                    $scope.langUpdateFunc(resp.id, resp.orderNo, resp.mandatoryFlag);
                }, function (error) {
                    if (error.data.error.constraint === 'dynamiclist_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        $scope.langUpdatecount = 0;

        $scope.langUpdateFunc = function (intervalId, orderNo, mandatoryFlag) {

            if ($scope.langUpdatecount < $scope.rowArray.length) {

                if ($scope.rowArray[$scope.langUpdatecount].id == 0 || $scope.rowArray[$scope.langUpdatecount].id == "0") {

                    $scope.rowArray[$scope.langUpdatecount].parentFlag = false;
                    $scope.rowArray[$scope.langUpdatecount].parentId = intervalId;
                    $scope.rowArray[$scope.langUpdatecount].orderNo = orderNo;
                    $scope.rowArray[$scope.langUpdatecount].mandatoryFlag = mandatoryFlag;
                    $scope.rowArray[$scope.langUpdatecount].deleteFlag = false;
                    delete $scope.rowArray[$scope.langUpdatecount]['id'];

                    Restangular.all('dynamiclists').post($scope.rowArray[$scope.langUpdatecount]).then(function () {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId, orderNo, mandatoryFlag);
                    });

                } else {

                    $scope.rowArray[$scope.langUpdatecount].orderNo = orderNo;
                    $scope.rowArray[$scope.langUpdatecount].mandatoryFlag = mandatoryFlag;

                    Restangular.one('dynamiclists', $scope.rowArray[$scope.langUpdatecount].id).customPUT($scope.rowArray[$scope.langUpdatecount]).then(function () {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId, orderNo, mandatoryFlag);
                    });
                }

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/dynamicschoolchecklist';
            }
        };

        /**************************Sorting **********************************/
    
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
 
    
        /******************************************************** DELETE *******************************************/

        $scope.Delete = function (id) {
            
            $scope.item = [{
                deleteFlag: true
            }]

            Restangular.one('dynamiclists/' + id).customPUT($scope.item[0]).then(function () {
                Restangular.all('dynamiclists?filter[where][parentId]=' + id).getList().then(function (stypes) {
                    $scope.deleteRows = stypes;
                    $scope.DeleteFunc();
                });
            });
        };

        $scope.updateFlag = {
            deleteFlag: true
        };

        $scope.DeleteCount = 0;

        $scope.DeleteFunc = function (id) {

            if ($scope.DeleteCount < $scope.deleteRows.length) {

                Restangular.one('dynamiclists/' + $scope.deleteRows[$scope.DeleteCount].id).customPUT($scope.updateFlag).then(function () {
                    $scope.DeleteCount++;
                    $scope.DeleteFunc();
                });

            } else {
                $route.reload();
            }
        };

    });
