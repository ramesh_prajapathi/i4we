'use strict';

angular.module('secondarySalesApp')
    .controller('AuditTrailCtrl', function ($scope, Restangular, $http, $window, $route, $filter, $timeout) {

        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.someFocusVariable = true;

        //Datepicker settings start/////

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };

        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };

        $scope.toggleMin();
        $scope.picker = {};
        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];

        //Datepicker settings end///

        $scope.selectedDate = new Date();
        $scope.loaderModal = true;

        /******************** Calling Function **************/

        $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;
        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;
        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;
        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;
        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;
        $scope.conditiondsply = Restangular.all('conditions?filter[where][deleteFlag]=false').getList().$object;
        $scope.schemedsply = Restangular.all('schemes?filter[where][deleteFlag]=false').getList().$object;
        $scope.documentdsply = Restangular.all('documenttypes?filter[where][deleteFlag]=false').getList().$object;

        /*************** Watch Function *********************/

        $scope.$watch('selectedDate', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                console.log('newValue', newValue);
                var myDate = $filter('date')(newValue, 'yyyy-MM');
                
                //$scope.loaderModal = false;
                
                Restangular.all('audittrials?filter[order]=datetime%20DESC&filter[where][datetime][like]=' + myDate + '%').getList().then(function (audit) {
                    $scope.loaderModal = false;
                    $scope.audittrials = audit;

                    $scope.audiTrialArray = [];

                    angular.forEach($scope.audittrials, function (member, index) {
                        member.index = index + 1;

                        member.datetime = $filter('date')(member.datetime, 'dd-MMM-yyyy hh:mm a');

                        for (var m = 0; m < $scope.usersdisplay.length; m++) {
                            if (member.owner == $scope.usersdisplay[m].id) {
                                member.ownername = $scope.usersdisplay[m].username;
                                break;
                            }
                        }
                        for (var p = 0; p < $scope.countrydisplay.length; p++) {
                            if (member.countryId == $scope.countrydisplay[p].id) {
                                member.countryname = $scope.countrydisplay[p].name;
                                break;
                            }
                        }
                        for (var q = 0; q < $scope.statedisplay.length; q++) {
                            if (member.stateId == $scope.statedisplay[q].id) {
                                member.statename = $scope.statedisplay[q].name;
                                break;
                            }
                        }
                        for (var r = 0; r < $scope.districtdsply.length; r++) {
                            if (member.districtId == $scope.districtdsply[r].id) {
                                member.districtname = $scope.districtdsply[r].name;
                                break;
                            }
                        }
                        for (var s = 0; s < $scope.sitedsply.length; s++) {
                            if (member.siteId == $scope.sitedsply[s].id) {
                                member.sitename = $scope.sitedsply[s].name;
                                break;
                            }
                        }

                        if (member.module == 'Condition') {
                            for (var s = 0; s < $scope.conditiondsply.length; s++) {
                                if (member.conditionId == $scope.conditiondsply[s].id) {
                                    member.details = $scope.conditiondsply[s].name+"/"+member.details;
                                    break;
                                }
                            }
                        }

                        if (member.module == 'Apply for Scheme') {
                            for (var s = 0; s < $scope.schemedsply.length; s++) {
                                if (member.conditionId == $scope.schemedsply[s].id) {
                                    member.details = $scope.schemedsply[s].name+"/"+member.details;
                                    break;
                                }
                            }
                        }

                        if (member.module == 'Apply for Document') {
                            for (var s = 0; s < $scope.documentdsply.length; s++) {
                                if (member.conditionId == $scope.documentdsply[s].id) {
                                    member.details = $scope.documentdsply[s].name+"/"+member.details;
                                    break;
                                }
                            }
                        }

                    });
                });
            }
        });

        /******** Ng init function **************/

        $scope.getUser = function (owner) {
            return Restangular.one('users', owner).get().$object;
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        /**************************Export data to excel sheet ***************/

        $scope.valAuditCount = 0;

        $scope.exportData = function () {
            $scope.valAuditCount = 0;

            if ($scope.audittrials.length == 0) {
                $scope.loaderModal = false;
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.audittrials.length; c++) {
                    $scope.audiTrialArray.push({
                        'ACTION': $scope.audittrials[c].action,
                        'MODULE': $scope.audittrials[c].module,
                        'OWNER': $scope.audittrials[c].ownername,
                        'DATE / TIME': $scope.audittrials[c].datetime,
                        'DETAILS': $scope.audittrials[c].details,
                        'ROW ID': $scope.audittrials[c].rowId,
                        'COUNTRY': $scope.audittrials[c].countryname,
                        'STATE': $scope.audittrials[c].statename,
                        'DISTRICT': $scope.audittrials[c].districtname,
                        'SITE': $scope.audittrials[c].sitename
                    });

                    $scope.valAuditCount++;

                    if ($scope.audittrials.length == $scope.valAuditCount) {
                        alasql('SELECT * INTO XLSX("audittrail.xlsx",{headers:true}) FROM ?', [$scope.audiTrialArray]);
                    }
                }
            }
        };

    });
  /*.directive('loadingtodo', function () {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="loading" style="color:black;"><img src="images/piclinks/loaderpink_blue.gif" width="20" height="20"  class="loadingwidth" /> <br/><br/>LOADING. . .</div>',
            link: function (scope, element, attrs) {
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).show();
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).hide();
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
            }
        }
    });*/