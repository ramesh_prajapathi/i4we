'use strict';
angular.module('secondarySalesApp').controller('MainCtrl', function ($scope, $http, Restangular, $window, $timeout, $location, baseUrl, $rootScope, $idle, $modal, $filter, $route, dataShareService, anchorSmoothScroll) {

        if ($window.sessionStorage.roleId == 1) {
            window.location = "/audit-trail";
        }

        if ($window.sessionStorage.roleId == 9) {
            window.location = "/language-list";
        }

        $scope.disableState = true;
        $scope.disableDistrict = true;
        $scope.disableSite = true;
        $scope.disableHf = true;
        $scope.tabs = [{ "id": 1, "name": "HH", "children": []}, {"id": 2, "name": "SHG", "children": []}, {"id": 3, "name": "SHG meetings", "children": []},{ "id": 4, "name": "Applications", "children":[]}, {"id": 5, "name": "Screen and Treat", "children": []}, {"id": 6, "name": "incidents", "children": []}];
        $scope.currenttab = 1;
        $scope.changetab = function(tab){

            $scope.currenttab = tab.id;
        }
        Restangular.all('states?filter[where][deleteFlag]=false').getList().then(function (st) {
            $scope.states = st;
            $scope.state = $window.sessionStorage.stateId.split(',');
        });

        $scope.$watch('state', function (newValue, oldValue) {
            if (newValue == undefined || newValue == null || newValue == '' || newValue == oldValue) {
                return;
            } else if (newValue[0] == null || newValue[0] == "null") {
                return;
            } else {

                Restangular.all('districts?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (dist) {
                    $scope.districts = dist;

                    if ($window.sessionStorage.roleId == 2) {
                        $scope.district = '';
                        $scope.site = '';
                        $scope.hfuser = ''
                    } else {
                        $scope.district = $window.sessionStorage.districtId.split(',');
                    }
                });

                if ($scope.disableState == false) {

                    $scope.shgLen1 = 0;
                    $scope.shgLen2 = 0;
                    $scope.noOfmemsInshGCount = 0;
                    $scope.noOfHh = 0;
                    $scope.noOfIndMem = 0;
                    $scope.noOfShg = 0;
                    $scope.noOfshgGreaterThan15 = 0;
                    $scope.noOfshgLessThan5 = 0;
                    $scope.noOfmemsInshG = 0;
                    $scope.NoofPatientRecords = 0;
                    $scope.NoofApplyForSchemeCount = 0;
                    $scope.NoofApplyForDocumentCount = 0;
                    $scope.NoofInterventionCount = 0;
                    $scope.TotalSavingsInShgs = 0;

                    $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.householdFilterCall = 'households?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.memberFilterCall = 'members?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

                    $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

                    $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.mainFunc();
                }
            }
        });

        $scope.$watch('district', function (newValue, oldValue) {
            if (newValue == undefined || newValue == null || newValue == '' || newValue == oldValue) {
                return;
            } else if (newValue[0] == null || newValue[0] == "null") {
                return;
            } else {
                Restangular.all('sites?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (site) {
                    $scope.sites = site;
                    $scope.site = $window.sessionStorage.siteId.split(',');
                    // console.log(newValue);
                });

                if ($scope.disableDistrict == false) {

                    $scope.shgLen1 = 0;
                    $scope.shgLen2 = 0;
                    $scope.noOfmemsInshGCount = 0;
                    $scope.noOfHh = 0;
                    $scope.noOfIndMem = 0;
                    $scope.noOfShg = 0;
                    $scope.noOfshgGreaterThan15 = 0;
                    $scope.noOfshgLessThan5 = 0;
                    $scope.noOfmemsInshG = 0;
                    $scope.NoofPatientRecords = 0;
                    $scope.NoofApplyForSchemeCount = 0;
                    $scope.NoofApplyForDocumentCount = 0;
                    $scope.NoofInterventionCount = 0;
                    $scope.TotalSavingsInShgs = 0;

                    $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.householdFilterCall = 'households?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.memberFilterCall = 'members?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

                    $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

                    $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.mainFunc();
                }
            }
        });

        $scope.$watch('site', function (newValue, oldValue) {
            if (newValue == undefined || newValue == null || newValue == '' || newValue == oldValue) {
                return;
            } else if (newValue[0] == null || newValue[0] == "null") {
                return;
            } else {

                // console.log(newValue[0]);

                Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                    $scope.hfusers = urs;

                    if ($window.sessionStorage.roleId == 3) {
                        $scope.hfuser = $window.sessionStorage.userId.split(',');
                    }

                    if ($scope.disableSite == false) {

                        $scope.shgLen1 = 0;
                        $scope.shgLen2 = 0;
                        $scope.noOfmemsInshGCount = 0;
                        $scope.noOfHh = 0;
                        $scope.noOfIndMem = 0;
                        $scope.noOfShg = 0;
                        $scope.noOfshgGreaterThan15 = 0;
                        $scope.noOfshgLessThan5 = 0;
                        $scope.noOfmemsInshG = 0;
                        $scope.NoofPatientRecords = 0;
                        $scope.NoofApplyForSchemeCount = 0;
                        $scope.NoofApplyForDocumentCount = 0;
                        $scope.NoofInterventionCount = 0;
                        $scope.TotalSavingsInShgs = 0;

                        $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                        $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                        $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                        $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                        $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

                        $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

                        $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                        $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"siteId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                        $scope.mainFunc();

                    }
                });
            }
        });

        $scope.$watch('hfuser', function (newValue, oldValue) {
            if (newValue == undefined || newValue == null || newValue == '' || newValue == oldValue) {
                return;
            } else if (newValue[0] == null || newValue[0] == "null") {
                return;
            } else {

                if ($scope.disableHf == false) {

                    $scope.shgLen1 = 0;
                    $scope.shgLen2 = 0;
                    $scope.noOfmemsInshGCount = 0;
                    $scope.noOfHh = 0;
                    $scope.noOfIndMem = 0;
                    $scope.noOfShg = 0;
                    $scope.noOfshgGreaterThan15 = 0;
                    $scope.noOfshgLessThan5 = 0;
                    $scope.noOfmemsInshG = 0;
                    $scope.NoofPatientRecords = 0;
                    $scope.NoofApplyForSchemeCount = 0;
                    $scope.NoofApplyForDocumentCount = 0;
                    $scope.NoofInterventionCount = 0;
                    $scope.TotalSavingsInShgs = 0;

                    $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.householdFilterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

                    $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

                    $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                    $scope.mainFunc();
                }
            }
        });

        $scope.mainFunc = function () {

            Restangular.all($scope.householdFilterCall).getList().then(function (household) {
                $scope.noOfHh = household.length;
                $scope.houseHoldsDisplay = household;

                Restangular.all($scope.memberFilterCall).getList().then(function (mrs) {
                    $scope.noOfIndMem = mrs.length;

                    Restangular.all($scope.shgFilterCall).getList().then(function (shg) {
                        $scope.noOfShg = shg.length;
                        $scope.shgs = shg;

                        $scope.newArray = [];

                        angular.forEach($scope.shgs, function (member, index) {
                            member.index = index + 1;

                            member.membersInGroupLength = member.membersInGroup.split(',').length;

                            var c = member.membersInGroup.split(',');

                            var d = _.uniq(c, function (w) {
                                return w;
                            })

                            $scope.newArray.push(d);

                            if (member.membersInGroupLength < 5) {
                                $scope.shgLen1++;
                                $scope.noOfshgLessThan5 = $scope.shgLen1;
                            }
                            //                    else {
                            //                        $scope.noOfshgLessThan5 = 0;
                            //                    }

                            if (member.membersInGroupLength > 15) {
                                $scope.shgLen2++;
                                $scope.noOfshgGreaterThan15 = $scope.shgLen2;
                            }
                            //                    else {
                            //                        $scope.noOfshgGreaterThan15 = 0;
                            //                    }
                        });

                        if ($scope.shgs.length == $scope.newArray.length) {
                            for (var n = 0; n < $scope.newArray.length; n++) {
                                $scope.noOfmemsInshGCount = $scope.noOfmemsInshGCount + $scope.newArray[n].length;
                                $scope.noOfmemsInshG = $scope.noOfmemsInshGCount;
                            }
                        }
                    });
                });
            });

            Restangular.all($scope.patientRecordFilterCall).getList().then(function (precord) {
                $scope.NoofPatientRecords = precord.length;
            });

            Restangular.all($scope.applyforSchemeFilterCall).getList().then(function (applysch) {
                $scope.NoofApplyForSchemeCount = applysch.length;
            });

            Restangular.all($scope.applyforDocFilterCall).getList().then(function (applydoc) {
                $scope.NoofApplyForDocumentCount = applydoc.length;
            });

            Restangular.all($scope.interventionFilterCall).getList().then(function (intr) {
                $scope.NoofInterventionCount = intr.length;
            });

            Restangular.all($scope.totalamountFilterCall).getList().then(function (intr) {
                var scount = 0;
                var amount = 0;

                angular.forEach(intr, function (data, index) {
                    amount = amount - (-data.totalAmountSaved);
                    scount++;

                    if (intr.length == scount) {
                        $scope.TotalSavingsInShgs = amount;
                    }
                });
            });
        };

        $scope.confirmModal = false;
        $scope.currentPage = 1;

        $scope.MainLanguage = {};

        $scope.roleId = $window.sessionStorage.roleId;

        $scope.analyticsRedirect = function (path) {
            if ($scope.roleId == 3 || $scope.roleId == 7 || $scope.roleId == 8) {
                window.location = path;
            }
        };

        Restangular.one('lhsLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.MainLanguage = langResponse[0];
            //  $scope.typeId = 'condition';
        });

        Restangular.one('householdlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.HHLanguage = langResponse[0];
            $scope.headingName = $scope.HHLanguage.houseHoldCreate;
            // $scope.modalTitle = $scope.HHLanguage.thankYou;
            //$scope.message = $scope.HHLanguage.thankYouCreated;
        });


        $scope.shgLen1 = 0;
        $scope.shgLen2 = 0;
        $scope.noOfmemsInshGCount = 0;

        $scope.noOfHh = 0;
        $scope.noOfIndMem = 0;
        $scope.noOfShg = 0;
        $scope.noOfshgGreaterThan15 = 0;
        $scope.noOfshgLessThan5 = 0;
        $scope.noOfmemsInshG = 0;
        $scope.activeConditionsTracked = 0;
        $scope.shgMeetingsSavedByUser = 0;
        $scope.reportIncidentsByUser = 0;
        $scope.NoofOpenApplicationsMadeByUser = 0;
        $scope.NoofPatientRecords = 0;
        $scope.NoofApplyForSchemeCount = 0;
        $scope.NoofApplyForDocumentCount = 0;
        $scope.NoofInterventionCount = 0;
        $scope.TotalSavingsInShgs = 0;

        $scope.member = {
            relation: null,
            literacyLevel: null,
            nameOfSchool: null,
            nameOfFactory: null,
            deleteFlag: false,
            isHeadOfFamily: true,
            shgId: null,
            age: '',
            name: '',
            mobile: '',
            healthyDays: 0,
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        };

        $scope.household = {
            deleteFlag: false,
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        };


        Restangular.all('genders?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (gend) {
            $scope.genders = gend;

            if ($scope.UserLanguage == 1) {
                $scope.member.gender = gend[0].id;
                $scope.memgender = gend[0].id;
            } else {
                $scope.member.gender = gend[0].parentId;
                $scope.memgender = gend[0].parentId;
            }
        });

        Restangular.all('occupations?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (occptn) {
            // console.log('occptn', occptn);
            $scope.occupations = occptn;

            if ($scope.UserLanguage == 1) {
                $scope.member.occupation = occptn[6].id;
            } else {
                $scope.member.occupation = occptn[6].parentId;
            }
        });

        Restangular.all('castes?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (cste) {
            $scope.castes = cste;

            if ($scope.UserLanguage == 1) {
                $scope.member.caste = cste[4].id;
            } else {
                $scope.member.caste = cste[4].parentId;
            }
        });

        Restangular.all('migrants?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mgnt) {
            $scope.migrants = mgnt;

            if ($scope.UserLanguage == 1) {
                $scope.member.migrant = mgnt[2].id;
            } else {
                $scope.member.migrant = mgnt[2].parentId;
            }
        });

        Restangular.all('religions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rlgn) {
            $scope.religions = rlgn;
            // console.log('rlgn', rlgn);
            if ($scope.UserLanguage == 1) {
                $scope.member.religion = rlgn[5].id;
            } else {
                $scope.member.religion = rlgn[5].parentId;
            }
        });

        Restangular.all('bplStatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (bpl) {
            $scope.bplStatuses = bpl;
            //console.log('bpl', bpl);
            if ($scope.UserLanguage == 1) {
                $scope.member.bplStatus = bpl[4].id;
            } else {
                $scope.member.bplStatus = bpl[4].parentId;
            }
        });

        Restangular.all('conditions?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false' + '&filter[where][enabledFor]=true').getList().then(function (cn) {
            $scope.activeConditionsTracked = cn.length;
        });

        Restangular.all('shgmeetingheaders?filter[where][createdBy]=' + $window.sessionStorage.userId + '&filter[where][deleteFlag]=false').getList().then(function (shgmeetingheader) {
            $scope.shgMeetingsSavedByUser = shgmeetingheader.length;
        });

        Restangular.all('reportincidents?filter[where][createdBy]=' + $window.sessionStorage.userId + '&filter[where][deleteFlag]=false').getList().then(function (report) {
            $scope.reportIncidentsByUser = report.length;
        });

        Restangular.all('schememasters?filter[where][createdBy]=' + $window.sessionStorage.userId + '&filter[where][deleteFlag]=false').getList().then(function (condtion) {
            $scope.NoofOpenApplicationsMadeByUser = condtion.length;
        });

        $scope.hideTodo = false;

        if ($window.sessionStorage.roleId + "" === "3") {
            //HF

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = true;
            $scope.disableHf = true;


            $scope.hideTodo = true;
            $scope.disableAddPatient = true;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.disableAssigned = true;

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.member.associatedHF = $window.sessionStorage.userId;
                $scope.memhf = $window.sessionStorage.userId;
            });

            $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

            $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

            $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else if ($window.sessionStorage.roleId + "" === "8") {
            //Site Manager

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = true;
            $scope.disableHf = false;

            $scope.hideTodo = true;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.disableAssigned = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.member.associatedHF = urs[0].id;
                $scope.memhf = urs[0].id;
            });

            $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

            $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

            $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';


        } else if ($window.sessionStorage.roleId + "" === "2") {
            //National Director

            $scope.disableState = false;
            $scope.disableDistrict = false;
            $scope.disableSite = false;
            $scope.disableHf = false;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}},{"stateId":{"gte":21}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}},{"stateId":{"gte":21}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}},{"stateId":{"gte":21}}]}}';

            $scope.disableAssigned = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.member.associatedHF = urs[0].id;
            });

            $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

            $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

            $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"countryId":{"inq":[' + $window.sessionStorage.countryId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else if ($window.sessionStorage.roleId + "" === "4") {

            //Doctor

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = true;
            $scope.disableHf = false;


            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.disableAssigned = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.member.associatedHF = urs[0].id;
            });

            $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

            $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

            $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else if ($window.sessionStorage.roleId + "" === "5") {

            //District Programme Manager

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = false;
            $scope.disableHf = false;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"districtId":{"inq":[' + $window.sessionStorage.districtId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"districtId":{"inq":[' + $window.sessionStorage.districtId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"districtId":{"inq":[' + $window.sessionStorage.districtId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.disableAssigned = false;

            $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"districtId":{"inq":[' + $window.sessionStorage.districtId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"districtId":{"inq":[' + $window.sessionStorage.districtId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

            $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"districtId":{"inq":[' + $window.sessionStorage.districtId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

            $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"districtId":{"inq":[' + $window.sessionStorage.districtId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"districtId":{"inq":[' + $window.sessionStorage.districtId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else if ($window.sessionStorage.roleId + "" === "6") {
            //State Programme Manager

            $scope.disableState = true;
            $scope.disableDistrict = false;
            $scope.disableSite = false;
            $scope.disableHf = false;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"stateId":{"inq":[' + $window.sessionStorage.stateId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"stateId":{"inq":[' + $window.sessionStorage.stateId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"stateId":{"inq":[' + $window.sessionStorage.stateId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.disableAssigned = false;

            $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"stateId":{"inq":[' + $window.sessionStorage.stateId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"stateId":{"inq":[' + $window.sessionStorage.stateId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

            $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"stateId":{"inq":[' + $window.sessionStorage.stateId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

            $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"stateId":{"inq":[' + $window.sessionStorage.stateId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"stateId":{"inq":[' + $window.sessionStorage.stateId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else if ($window.sessionStorage.roleId + "" === "7") {
            //Nurse

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = true;
            $scope.disableHf = false;

            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.disableAssigned = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.member.associatedHF = urs[0].id;
            });

            $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

            $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

            $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else {

            $scope.disableState = true;
            $scope.disableDistrict = true;
            $scope.disableSite = true;
            $scope.disableHf = false;

            $scope.disableAddPatient = false;
            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.householdFilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.disableAssigned = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.member.associatedHF = urs[0].id;
            });

            $scope.patientRecordFilterCall = 'patientrecords?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.applyforSchemeFilterCall = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';

            $scope.applyforDocFilterCall = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';

            $scope.interventionFilterCall = 'interventions?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.totalamountFilterCall = 'shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        }

        Restangular.all($scope.householdFilterCall).getList().then(function (household) {
            $scope.noOfHh = household.length;
            $scope.houseHoldsDisplay = household;

            Restangular.all($scope.memberFilterCall).getList().then(function (mrs) {
                $scope.noOfIndMem = mrs.length;

                Restangular.all($scope.shgFilterCall).getList().then(function (shg) {
                    $scope.noOfShg = shg.length;
                    $scope.shgs = shg;

                    $scope.newArray = [];

                    angular.forEach($scope.shgs, function (member, index) {
                        member.index = index + 1;

                        member.membersInGroupLength = member.membersInGroup.split(',').length;

                        var c = member.membersInGroup.split(',');

                        var d = _.uniq(c, function (w) {
                            return w;
                        })

                        $scope.newArray.push(d);

                        if (member.membersInGroupLength < 5) {
                            $scope.shgLen1++;
                            $scope.noOfshgLessThan5 = $scope.shgLen1;
                        }
                        //                    else {
                        //                        $scope.noOfshgLessThan5 = 0;
                        //                    }

                        if (member.membersInGroupLength > 15) {
                            $scope.shgLen2++;
                            $scope.noOfshgGreaterThan15 = $scope.shgLen2;
                        }
                        //                    else {
                        //                        $scope.noOfshgGreaterThan15 = 0;
                        //                    }
                    });

                    if ($scope.shgs.length == $scope.newArray.length) {
                        for (var n = 0; n < $scope.newArray.length; n++) {
                            $scope.noOfmemsInshGCount = $scope.noOfmemsInshGCount + $scope.newArray[n].length;
                            $scope.noOfmemsInshG = $scope.noOfmemsInshGCount;
                        }
                    }
                });
            });
        });

        Restangular.all($scope.patientRecordFilterCall).getList().then(function (precord) {
            $scope.NoofPatientRecords = precord.length;
        });

        Restangular.all($scope.applyforSchemeFilterCall).getList().then(function (applysch) {
            $scope.NoofApplyForSchemeCount = applysch.length;
        });

        Restangular.all($scope.applyforDocFilterCall).getList().then(function (applydoc) {
            $scope.NoofApplyForDocumentCount = applydoc.length;
        });

        Restangular.all($scope.interventionFilterCall).getList().then(function (intr) {
            $scope.NoofInterventionCount = intr.length;
        });

        Restangular.all($scope.totalamountFilterCall).getList().then(function (intr) {
            var scount = 0;
            var amount = 0;

            angular.forEach(intr, function (data, index) {
                amount = amount - (-data.totalAmountSaved);
                scount++;

                if (intr.length == scount) {
                    $scope.TotalSavingsInShgs = amount;
                }
            });
        });

        $scope.openApplications = function () {
            $scope.ButtonDisable = true;
            if ($scope.roleId == 3 || $scope.roleId == 7 || $scope.roleId == 8) {
                $scope.confirm.value = '';
                $scope.confirmModal = true;
            }
        };

        $scope.confirm = {};
        $scope.ButtonDisable = true;
        $scope.clickButtonEnable = function () {
            $scope.ButtonDisable = false;
        }

        $scope.confirmRedirect = function () {
            if ($scope.confirm.value == 'scheme') {
                window.location = "/applyforschemes";
            } else if ($scope.confirm.value == 'document') {
                window.location = "/applyfordocuments";
            } else {
                return;
            }
        };

        $scope.todoArray = [];
        $scope.disableClick = true;
        $scope.loaderModal = true;

        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;
        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;
        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;
        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;
        $scope.roledsply = Restangular.all('roles').getList().$object;

        $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;

        /*************** Condition ************************/

        $scope.conditiondsply = Restangular.all('conditions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.stepsdsply = Restangular.all('conditionsteps?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.statusesdsply = Restangular.all('conditionstatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.conditiondsplyNew = Restangular.all('conditions?filter[where][deleteFlag]=false').getList().$object;
        $scope.stepsdsplyNew = Restangular.all('conditionsteps?filter[where][deleteFlag]=false').getList().$object;
        $scope.statusesdsplyNew = Restangular.all('conditionstatuses?filter[where][deleteFlag]=false').getList().$object;

        $scope.finalSchemes = Restangular.all('schemes?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().$object;

        $scope.finalSchemeStages = Restangular.all('schemestages?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().$object;

        $scope.finalDocs = Restangular.all('documenttypes?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().$object;

        $scope.finalDocStages = Restangular.all('schemestages?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().$object;

        $scope.typeofincidentsdsply = Restangular.all('typeofincidents?filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.currentstatusofcasesdsply = Restangular.all('currentstatusofcases?filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        /************************ End ***********************/

        /*************** Intervention ***********************/

        $scope.venues = Restangular.all('venues?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.types = Restangular.all('typeofinterventions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.venuesNew = Restangular.all('venues?filter[where][deleteFlag]=false').getList().$object;

        $scope.typesNew = Restangular.all('typeofinterventions?filter[where][deleteFlag]=false').getList().$object;

        /************************** End **********************/

        /*************** Scheme ******************************/

        $scope.responcedreceived = Restangular.all('responsereceived?filter[where][deleteFlag]=false').getList().$object;
        $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteFlag]=false').getList().$object;
        $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteFlag]=false').getList().$object;
        $scope.finalSchemesNew = Restangular.all('schemes?filter[where][deleteFlag]=false').getList().$object;
        $scope.finalSchemeStagesNew = Restangular.all('schemestages?filter[where][deleteFlag]=false').getList().$object;

        /************************** End **********************/

        /*************** Document ******************************/

        $scope.responcedreceived = Restangular.all('responsereceived?filter[where][deleteFlag]=false').getList().$object;
        $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteFlag]=false').getList().$object;
        $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteFlag]=false').getList().$object;
        $scope.finalDocsNew = Restangular.all('documenttypes?filter[where][deleteFlag]=false').getList().$object;
        $scope.finalSchemeNew = Restangular.all('schemestages?filter[where][deleteFlag]=false').getList().$object;

        /************************** End **********************/

        /*************** Incident ******************************/

        $scope.severityofincidentsdsply = Restangular.all('severityofincidents?filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.reportincidentfollowupsdsply = Restangular.all('reportincidentfollowups?filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        $scope.typeofincidentsdsplyNew = Restangular.all('typeofincidents?filter[where][deleteFlag]=false').getList().$object;

        $scope.severityofincidentsdsplyNew = Restangular.all('severityofincidents?filter[where][deleteFlag]=false').getList().$object;

        $scope.currentstatusofcasesdsplyNew = Restangular.all('currentstatusofcases?filter[where][deleteFlag]=false').getList().$object;

        $scope.reportincidentfollowupsdsplyNew = Restangular.all('reportincidentfollowups?filter[where][deleteFlag]=false').getList().$object;

        /************************** End **********************/

        /********************* Default Call ******************/

        $scope.myTodoArray = [];

        $scope.todoCount = 0;

        $scope.exportTodoData = [];

        $scope.DisableExport = true;
        $scope.DisableStatus = true;
        $scope.DisableType = true;

        Restangular.all($scope.memberFilterCall).getList().then(function (mems) {

            $scope.memsdisply = mems;

            if ($window.sessionStorage.roleId + "" === "3") {
                $scope.filterCallExpCondition = 'conditionheaders?filter[where][deleteFlag]=false&filter[where][associatedHF]=' + $window.sessionStorage.userId;
            } else {
                $scope.filterCallExpCondition = 'conditionheaders?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            }

            Restangular.all($scope.filterCallExpCondition).getList().then(function (cns) {

                var conCount = 0;

                $scope.todoExpConArray = $filter('orderBy')(cns, 'followupdate');

                if ($scope.todoExpConArray.length == 0) {
                    $scope.schemeFunc();
                } else {

                    angular.forEach($scope.todoExpConArray, function (member, index) {
                        member.index = index + 1;
                        member.type = 'Screen and Treat';
                        member.sortingDate = member.followupdate;
                        member.date = $filter('date')(member.followupdate, 'dd-MMM-yyyy');
                        member.dateExport = $filter('date')(member.followupdate, 'dd-MM-yyyy');
                        member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                        member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                        if (member.date == null) {
                            member.date = '';
                        }

                        var data = $scope.memsdisply.filter(function (arr) {
                            return arr.id == member.memberId
                        })[0];

                        if (data != undefined) {
                            member.membername = data.name;
                            member.individualId = data.individualId;
                            member.HHId = data.HHId;
                            member.mobile = data.mobile;

                            var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                return arr.id == data.parentId
                            })[0];

                            if (data1 != undefined) {
                                member.hhName = data1.nameOfHeadInHH;
                            }
                        }


                        if (member.status == 7 || member.status == 5) {
                            member.exp_status = 'Closed';
                        } else if (member.status != 5 || member.status != 7) {
                            member.exp_status = 'Opened';
                        }

                        if (member.step == 5 && member.status == 5 && member.caseClosed == false) {
                            member.exp_status = 'Opened';
                        } else if (member.step == 5 && member.status == 5 && member.caseClosed == true) {
                            member.exp_status = 'Closed';
                        }

                        var data2 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.associatedHF
                        })[0];

                        if (data2 != undefined) {
                            member.assignedTo = data2.name;
                        }

                        if ($window.sessionStorage.language == 1) {

                            var data3 = $scope.conditiondsply.filter(function (arr) {
                                return arr.id == member.condition
                            })[0];

                            if (data3 != undefined) {
                                member.detail = data3.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data3 = $scope.conditiondsply.filter(function (arr) {
                                return arr.parentId == member.condition
                            })[0];

                            if (data3 != undefined) {
                                member.detail = data3.name;
                            }
                        }

                        var data4 = $scope.conditiondsplyNew.filter(function (arr) {
                            return arr.id == member.condition
                        })[0];

                        if (data4 != undefined) {
                            member.detailEngName = data4.name;
                        }


                        if ($window.sessionStorage.language == 1) {

                            var data5 = $scope.stepsdsply.filter(function (arr) {
                                return arr.id == member.step
                            })[0];

                            if (data5 != undefined) {
                                member.stepname = data5.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data5 = $scope.stepsdsply.filter(function (arr) {
                                return arr.parentId == member.step
                            })[0];

                            if (data5 != undefined) {
                                member.stepname = data5.name;
                            }
                        }

                        var data6 = $scope.stepsdsplyNew.filter(function (arr) {
                            return arr.id == member.step
                        })[0];

                        if (data6 != undefined) {
                            member.stepEngName = data6.name;
                        }

                        if ($window.sessionStorage.language == 1) {

                            var data7 = $scope.statusesdsply.filter(function (arr) {
                                return arr.id == member.status
                            })[0];

                            if (data7 != undefined) {
                                member.statusname = data7.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data7 = $scope.statusesdsply.filter(function (arr) {
                                return arr.parentId == member.status
                            })[0];

                            if (data7 != undefined) {
                                member.statusname = data7.name;
                            }
                        }

                        var data8 = $scope.statusesdsplyNew.filter(function (arr) {
                            return arr.id == member.status
                        })[0];

                        if (data8 != undefined) {
                            member.statusEngName = data8.name;
                        }

                        var data9 = $scope.countrydisplay.filter(function (arr) {
                            return arr.id == member.countryId
                        })[0];

                        if (data9 != undefined) {
                            member.countryname = data9.name;
                        }

                        var data10 = $scope.statedisplay.filter(function (arr) {
                            return arr.id == member.stateId
                        })[0];

                        if (data10 != undefined) {
                            member.statename = data10.name;
                        }

                        var data11 = $scope.districtdsply.filter(function (arr) {
                            return arr.id == member.districtId
                        })[0];

                        if (data11 != undefined) {
                            member.districtname = data11.name;
                        }

                        var data12 = $scope.sitedsply.filter(function (arr) {
                            return arr.id == member.siteId
                        })[0];

                        if (data12 != undefined) {
                            member.sitename = data12.name;
                        }

                        member.statusStep = member.stepname + ' / ' + member.statusname;
                        member.statusStepEngName = member.stepEngName + ' / ' + member.statusEngName;

                        $scope.exportTodoData.push(member);
                        conCount++;

                        if (conCount == $scope.todoExpConArray.length) {
                            $scope.schemeFunc();
                        }
                    });
                }
            });
        });

        $scope.schemeFunc = function () {

            if ($window.sessionStorage.roleId + "" === "3") {
                $scope.filterCallExpSchemes = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';
            } else {
                $scope.filterCallExpSchemes = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';
            }

            Restangular.all($scope.filterCallExpSchemes).getList().then(function (schme) {

                $scope.todoExpSchmeArray = $filter('orderBy')(schme, 'datetime');

                var schemeCount = 0;

                if ($scope.todoExpSchmeArray.length == 0) {
                    $scope.documentFunc();
                } else {

                    angular.forEach($scope.todoExpSchmeArray, function (member, index) {
                        member.index = index + 1;
                        member.type = 'Scheme';

                        member.sortingDate = member.datetime;
                        member.date = $filter('date')(member.datetime, 'dd-MMM-yyyy');
                        member.dateExport = $filter('date')(member.datetime, 'dd-MM-yyyy');
                        member.datetime = $filter('date')(member.datetime, 'dd/MM/yyyy');
                        member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                        member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                        if (member.date == null) {
                            member.date = '';
                        }

                        if (member.datetime == null) {
                            member.datetime = '';
                        }
                        if (member.responserecieve == null) {
                            member.responserecieve = '';
                        }
                        if (member.lastModifiedDate == null) {
                            member.lastModifiedDate = '';
                        }
                        if (member.rejection == null) {
                            member.rejection = '';
                        }
                        if (member.delay == null) {
                            member.delay = '';
                        }
                        if (member.collectedrequired == null) {
                            member.collectedrequired = '';
                        }

                        if (member.stage == 7 || member.stage == 9) {
                            member.exp_status = 'Closed';
                        } else if (member.stage != 7 || member.stage != 9) {
                            member.exp_status = 'Opened';
                        }

                        var data = $scope.memsdisply.filter(function (arr) {
                            return arr.id == member.memberId
                        })[0];

                        if (data != undefined) {
                            member.membername = data.name;
                            member.individualId = data.individualId;
                            member.HHId = data.HHId;
                            member.mobile = data.mobile;

                            var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                return arr.id == data.parentId
                            })[0];

                            if (data1 != undefined) {
                                member.hhName = data1.nameOfHeadInHH;
                            }
                        }

                        if ($window.sessionStorage.language == 1) {

                            var data11 = $scope.finalSchemes.filter(function (arr) {
                                return arr.id == member.schemeId
                            })[0];

                            if (data11 != undefined) {
                                member.detail = data11.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data11 = $scope.finalSchemes.filter(function (arr) {
                                return arr.parentId == member.schemeId
                            })[0];

                            if (data11 != undefined) {
                                member.detail = data11.name;
                            }
                        }

                        var data2 = $scope.finalSchemesNew.filter(function (arr) {
                            return arr.id == member.schemeId
                        })[0];

                        if (data2 != undefined) {
                            member.detailEngName = data2.name;
                        }

                        if ($window.sessionStorage.language == 1) {

                            var data3 = $scope.finalSchemeStages.filter(function (arr) {
                                return arr.id == member.stage
                            })[0];

                            if (data3 != undefined) {
                                member.statusStep = data3.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data3 = $scope.finalSchemeStages.filter(function (arr) {
                                return arr.parentId == member.stage
                            })[0];

                            if (data3 != undefined) {
                                member.statusStep = data3.name;
                            }
                        }

                        var data4 = $scope.finalSchemeStagesNew.filter(function (arr) {
                            return arr.id == member.stage
                        })[0];

                        if (data4 != undefined) {
                            member.statusStepEngName = data4.name;
                        }

                        var data5 = $scope.responcedreceived.filter(function (arr) {
                            return arr.id == member.responserecieve
                        })[0];

                        if (data5 != undefined) {
                            member.responsereceivedName = data5.name;
                        }

                        var data6 = $scope.reasonforrejections.filter(function (arr) {
                            return arr.id == member.rejection
                        })[0];

                        if (data6 != undefined) {
                            member.rejectionName = data6.name;
                        }

                        var data7 = $scope.reasonfordelayed.filter(function (arr) {
                            return arr.id == member.delay
                        })[0];

                        if (data7 != undefined) {
                            member.delayName = data7.name;
                        }

                        var data8 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.associatedHF
                        })[0];

                        if (data8 != undefined) {
                            member.assignedTo = data8.name;
                        }

                        var data9 = $scope.countrydisplay.filter(function (arr) {
                            return arr.id == member.countryId
                        })[0];

                        if (data9 != undefined) {
                            member.countryname = data9.name;
                        }

                        var data10 = $scope.statedisplay.filter(function (arr) {
                            return arr.id == member.stateId
                        })[0];

                        if (data10 != undefined) {
                            member.statename = data10.name;
                        }

                        var data11 = $scope.districtdsply.filter(function (arr) {
                            return arr.id == member.districtId
                        })[0];

                        if (data11 != undefined) {
                            member.districtname = data11.name;
                        }

                        var data12 = $scope.sitedsply.filter(function (arr) {
                            return arr.id == member.siteId
                        })[0];

                        if (data12 != undefined) {
                            member.sitename = data12.name;
                        }

                        $scope.exportTodoData.push(member);
                        schemeCount++;

                        if (schemeCount == $scope.todoExpSchmeArray.length) {
                            $scope.documentFunc();
                        }
                    });
                }
            });
        };

        $scope.documentFunc = function () {

            if ($window.sessionStorage.roleId + "" === "3") {
                $scope.filterCallExpDocuments = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';
            } else {
                $scope.filterCallExpDocuments = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';
            }

            Restangular.all($scope.filterCallExpDocuments).getList().then(function (doc) {

                $scope.todoExpDocArray = $filter('orderBy')(doc, 'datetime');

                if ($scope.todoExpDocArray.length == 0) {
                    $scope.incidentFunc();
                } else {

                    var docCount = 0;

                    angular.forEach($scope.todoExpDocArray, function (member, index) {
                        member.index = index + 1;
                        member.type = 'Document';

                        member.sortingDate = member.datetime;
                        member.date = $filter('date')(member.datetime, 'dd-MMM-yyyy');
                        member.dateExport = $filter('date')(member.datetime, 'dd-MM-yyyy');
                        member.datetime = $filter('date')(member.datetime, 'dd/MM/yyyy');
                        member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                        member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                        if (member.date == null) {
                            member.date = '';
                        }

                        if (member.datetime == null) {
                            member.datetime = '';
                        }
                        if (member.responserecieve == null) {
                            member.responserecieve = '';
                        }
                        if (member.lastModifiedDate == null) {
                            member.lastModifiedDate = '';
                        }
                        if (member.rejection == null) {
                            member.rejection = '';
                        }
                        if (member.delay == null) {
                            member.delay = '';
                        }
                        if (member.collectedrequired == null) {
                            member.collectedrequired = '';
                        }

                        if (member.stage == 7 || member.stage == 9) {
                            member.exp_status = 'Closed';
                        } else if (member.stage != 7 || member.stage != 9) {
                            member.exp_status = 'Opened';
                        }

                        var data = $scope.memsdisply.filter(function (arr) {
                            return arr.id == member.memberId
                        })[0];

                        if (data != undefined) {
                            member.membername = data.name;
                            member.individualId = data.individualId;
                            member.HHId = data.HHId;
                            member.mobile = data.mobile;

                            var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                return arr.id == data.parentId
                            })[0];

                            if (data1 != undefined) {
                                member.hhName = data1.nameOfHeadInHH;
                            }
                        }

                        if ($window.sessionStorage.language == 1) {

                            var data2 = $scope.finalDocs.filter(function (arr) {
                                return arr.id == member.schemeId
                            })[0];

                            if (data2 != undefined) {
                                member.detail = data2.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data2 = $scope.finalDocs.filter(function (arr) {
                                return arr.parentId == member.schemeId
                            })[0];

                            if (data2 != undefined) {
                                member.detail = data2.name;
                            }
                        }

                        var data3 = $scope.finalDocsNew.filter(function (arr) {
                            return arr.id == member.schemeId
                        })[0];

                        if (data3 != undefined) {
                            member.detailEngName = data3.name;
                        }

                        if ($window.sessionStorage.language == 1) {

                            var data4 = $scope.finalDocStages.filter(function (arr) {
                                return arr.id == member.stage
                            })[0];

                            if (data4 != undefined) {
                                member.statusStep = data4.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data4 = $scope.finalDocStages.filter(function (arr) {
                                return arr.parentId == member.stage
                            })[0];

                            if (data4 != undefined) {
                                member.statusStep = data4.name;
                            }
                        }

                        var data5 = $scope.finalSchemeStagesNew.filter(function (arr) {
                            return arr.id == member.stage
                        })[0];

                        if (data5 != undefined) {
                            member.statusStepEngName = data5.name;
                        }

                        var data6 = $scope.responcedreceived.filter(function (arr) {
                            return arr.id == member.responserecieve
                        })[0];

                        if (data6 != undefined) {
                            member.responsereceivedName = data6.name;
                        }

                        var data7 = $scope.reasonforrejections.filter(function (arr) {
                            return arr.id == member.rejection
                        })[0];

                        if (data7 != undefined) {
                            member.rejectionName = data7.name;
                        }

                        var data8 = $scope.reasonfordelayed.filter(function (arr) {
                            return arr.id == member.delay
                        })[0];

                        if (data8 != undefined) {
                            member.delayName = data8.name;
                        }

                        var data9 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.associatedHF
                        })[0];

                        if (data9 != undefined) {
                            member.assignedTo = data9.name;
                        }

                        var data10 = $scope.countrydisplay.filter(function (arr) {
                            return arr.id == member.countryId
                        })[0];

                        if (data10 != undefined) {
                            member.countryname = data10.name;
                        }

                        var data11 = $scope.statedisplay.filter(function (arr) {
                            return arr.id == member.stateId
                        })[0];

                        if (data11 != undefined) {
                            member.statename = data11.name;
                        }

                        var data12 = $scope.districtdsply.filter(function (arr) {
                            return arr.id == member.districtId
                        })[0];

                        if (data12 != undefined) {
                            member.districtname = data12.name;
                        }

                        var data13 = $scope.sitedsply.filter(function (arr) {
                            return arr.id == member.siteId
                        })[0];

                        if (data13 != undefined) {
                            member.sitename = data13.name;
                        }

                        $scope.exportTodoData.push(member);
                        docCount++;

                        if (docCount == $scope.todoExpDocArray.length) {
                            $scope.incidentFunc();
                        }
                    });
                }
            });
        };

        $scope.incidentFunc = function () {

            var key;

            Restangular.all($scope.memberFilterCall).getList().then(function (mem) {

                $scope.finalMembers = mem;

                if ($window.sessionStorage.roleId + "" === "3") {
                    $scope.filterCallExpIncidents = 'reportincidents?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                } else {
                    $scope.filterCallExpIncidents = 'reportincidents?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                }

                Restangular.all($scope.filterCallExpIncidents).getList().then(function (rincident) {

                    $scope.todoExpIncArray = $filter('orderBy')(rincident, 'followupdate');
                    // console.log(' $scope.todoExpIncArray',  $scope.todoExpIncArray);

                    var incCount = 0;

                    if ($scope.todoExpIncArray.length == 0) {
                        $scope.DisableExport = false;
                        $scope.DisableStatus = true;
                        $scope.DisableType = false;
                        $scope.todoArray = $scope.exportTodoData;

                        $scope.loaderModal = false;
                        angular.forEach($scope.todoArray, function (member, index) {
                            member.index = index + 1;
                        });
                    } else {

                        angular.forEach($scope.todoExpIncArray, function (member, index) {
                            member.index = index + 1;
                            member.type = 'Incident';

                            member.sortingDate = member.followupdate;
                            member.date = $filter('date')(member.followupdate, 'dd-MMM-yyyy');
                            member.dateExport = $filter('date')(member.followupdate, 'dd-MM-yyyy');
                            member.incidentdate = $filter('date')(member.incidentdate, 'dd/MM/yyyy');
                            member.dateofclosure = $filter('date')(member.dateofclosure, 'dd/MM/yyyy');
                            member.followupdate = $filter('date')(member.followupdate, 'dd/MM/yyyy');
                            member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                            member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                            if (member.dateofclosure == null) {
                                member.dateofclosure = '';
                            }
                            if (member.lastModifiedDate == null) {
                                member.lastModifiedDate = '';
                            }
                            if (member.date == null) {
                                member.date = '';
                            }

                            if (member.currentstatus == 5 || member.currentstatus == 9) {
                                member.exp_status = 'Closed';
                            } else if (member.currentstatus != 5 || member.currentstatus != 9) {
                                member.exp_status = 'Opened';
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data = $scope.typeofincidentsdsply.filter(function (arr) {
                                    return arr.id == member.incidenttype
                                })[0];

                                if (data != undefined) {
                                    member.detail = data.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data = $scope.typeofincidentsdsply.filter(function (arr) {
                                    return arr.parentId == member.incidenttype
                                })[0];

                                if (data != undefined) {
                                    member.detail = data.name;
                                }
                            }

                            var data1 = $scope.typeofincidentsdsplyNew.filter(function (arr) {
                                return arr.id == member.incidenttype
                            })[0];

                            if (data1 != undefined) {
                                member.detailEngName = data1.name;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data2 = $scope.currentstatusofcasesdsply.filter(function (arr) {
                                    return arr.id == member.currentstatus
                                })[0];

                                if (data2 != undefined) {
                                    member.currentstatusname = data2.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data2 = $scope.currentstatusofcasesdsply.filter(function (arr) {
                                    return arr.parentId == member.currentstatus
                                })[0];

                                if (data2 != undefined) {
                                    member.currentstatusname = data2.name;
                                }
                            }

                            var data3 = $scope.currentstatusofcasesdsplyNew.filter(function (arr) {
                                return arr.id == member.currentstatus
                            })[0];

                            if (data3 != undefined) {
                                member.currentstatusEngname = data3.name;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data4 = $scope.reportincidentfollowupsdsply.filter(function (arr) {
                                    return arr.id == member.followupneeded
                                })[0];

                                if (data4 != undefined) {
                                    member.statusStep = data4.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data4 = $scope.reportincidentfollowupsdsply.filter(function (arr) {
                                    return arr.parentId == member.followupneeded
                                })[0];

                                if (data4 != undefined) {
                                    member.statusStep = data4.name;
                                }
                            }

                            var data5 = $scope.reportincidentfollowupsdsplyNew.filter(function (arr) {
                                return arr.id == member.followupneeded
                            })[0];

                            if (data5 != undefined) {
                                member.statusStepEngName = data5.name;
                            }

                            var data6 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.associatedHF
                            })[0];

                            if (data6 != undefined) {
                                member.assignedTo = data6.name;
                            }

                            var data7 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data7 != undefined) {
                                member.countryname = data7.name;
                            }

                            var data8 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == member.stateId
                            })[0];

                            if (data8 != undefined) {
                                member.statename = data8.name;
                            }

                            var data9 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == member.districtId
                            })[0];

                            if (data9 != undefined) {
                                member.districtname = data9.name;
                            }

                            var data10 = $scope.sitedsply.filter(function (arr) {
                                return arr.id == member.siteId
                            })[0];

                            if (data10 != undefined) {
                                member.sitename = data10.name;
                            }

                            if (member.multiplemembers != null) {
                                member.multiplemembersLength = member.multiplemembers.split(',').length;
                                member.arraymultiplemembers = member.multiplemembers.split(",");
                                member.membername = "";
                                member.individualId = "";
                                member.HHId = "";
                                member.mobile = "";
                                member.hhName = "";

                                for (var i = 0; i < member.arraymultiplemembers.length; i++) {
                                    for (var j = 0; j < $scope.finalMembers.length; j++) {
                                        if ($scope.finalMembers[j].id + "" === member.arraymultiplemembers[i] + "") {
                                            if (member.membername === "") {
                                                member.membername = $scope.finalMembers[j].name;
                                                member.individualId = $scope.finalMembers[j].individualId;
                                                member.HHId = $scope.finalMembers[j].HHId;
                                                member.mobile = $scope.finalMembers[j].mobile;

                                                var data11 = $scope.memsdisply.filter(function (arr) {
                                                    return arr.id == $scope.finalMembers[j].id
                                                })[0];

                                                if (data11 != undefined) {

                                                    var data12 = $scope.houseHoldsDisplay.filter(function (arr) {
                                                        return arr.id == data11.parentId
                                                    })[0];

                                                    if (data12 != undefined) {
                                                        member.hhName = data12.nameOfHeadInHH;
                                                    }
                                                }

                                            } else {
                                                member.membername = member.membername + "," + $scope.finalMembers[j].name;
                                                member.individualId = member.individualId + "," + $scope.finalMembers[j].individualId;
                                                member.HHId = member.HHId + "," + $scope.finalMembers[j].HHId;
                                                member.mobile = member.mobile + "," + $scope.finalMembers[j].mobile;

                                                var data11 = $scope.memsdisply.filter(function (arr) {
                                                    return arr.id == $scope.finalMembers[j].id
                                                })[0];

                                                if (data11 != undefined) {

                                                    var data12 = $scope.houseHoldsDisplay.filter(function (arr) {
                                                        return arr.id == data11.parentId
                                                    })[0];

                                                    if (data12 != undefined) {
                                                        member.hhName = member.hhName + "," + data12.nameOfHeadInHH;
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                            } else {
                                member.membername = "";
                                member.individualId = "";
                                member.HHId = "";
                                member.mobile = "";
                                member.membersInGroupLength = 0;
                            }

                            for (key in member) {

                                if (member[key] === true) {
                                    member[key] = 'Yes';
                                } else if (member[key] === false) {
                                    member[key] = 'No';
                                }
                                // console.log(member[key]);
                            }

                            $scope.exportTodoData.push(member);
                            incCount++;

                            if (incCount == $scope.todoExpIncArray.length) {
                                $scope.DisableExport = false;
                                $scope.loaderModal = false;
                                $scope.DisableStatus = true;
                                $scope.DisableType = false;
                                $scope.todoArray = $scope.exportTodoData;
                                $scope.disableClick = false;

                                angular.forEach($scope.todoArray, function (member, index) {
                                    member.index = index + 1;
                                });
                            }
                        });
                    }
                });
            });
        };

        $scope.filterFields = ['membername', 'HHId', 'mobile'];
        $scope.searchInput = '';

        /****************** End ******************************/

        $scope.$watch('typeId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {

                $scope.todoArray = [];

                var tdArray = [];

                var typecount = 0;

                $scope.status = '';

                console.log(newValue);

                $scope.DisableStatus = true;

                $scope.currentPage = 1;

                //  $scope.status = 'opened';

                if (newValue == 'condition') {

                    if ($window.sessionStorage.roleId + "" === "3") {
                        $scope.filterCallCondition = 'conditionheaders?filter[where][deleteFlag]=false&filter[where][associatedHF]=' + $window.sessionStorage.userId;
                    } else {
                        $scope.filterCallCondition = 'conditionheaders?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                    }

                    Restangular.all($scope.filterCallCondition).getList().then(function (cns) {

                        var tArray = $filter('orderBy')(cns, 'followupdate');

                        angular.forEach(tArray, function (member, index) {
                            member.index = index + 1;
                            member.type = 'Screen and Treat';
                            member.sortingDate = member.followupdate;
                            member.date = $filter('date')(member.followupdate, 'dd-MMM-yyyy');
                            member.dateExport = $filter('date')(member.followupdate, 'dd-MM-yyyy');
                            member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                            member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                            if (member.date == null) {
                                member.date = '';
                            }

                            var data = $scope.memsdisply.filter(function (arr) {
                                return arr.id == member.memberId
                            })[0];

                            if (data != undefined) {
                                member.membername = data.name;
                                member.individualId = data.individualId;
                                member.HHId = data.HHId;
                                member.mobile = data.mobile;

                                var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                    return arr.id == data.parentId
                                })[0];

                                if (data1 != undefined) {
                                    member.hhName = data1.nameOfHeadInHH;
                                }
                            }

                            if (member.status == 7 || member.status == 5) {
                                member.exp_status = 'Closed';
                            } else if (member.status != 5 || member.status != 7) {
                                member.exp_status = 'Opened';
                            }

                            if (member.step == 5 && member.status == 5 && member.caseClosed == false) {
                                member.exp_status = 'Opened';
                            } else if (member.step == 5 && member.status == 5 && member.caseClosed == true) {
                                member.exp_status = 'Closed';
                            }


                            var data2 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.associatedHF
                            })[0];

                            if (data2 != undefined) {
                                member.assignedTo = data2.name;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data3 = $scope.conditiondsply.filter(function (arr) {
                                    return arr.id == member.condition
                                })[0];

                                if (data3 != undefined) {
                                    member.detail = data3.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data3 = $scope.conditiondsply.filter(function (arr) {
                                    return arr.parentId == member.condition
                                })[0];

                                if (data3 != undefined) {
                                    member.detail = data3.name;
                                }
                            }

                            var data4 = $scope.conditiondsplyNew.filter(function (arr) {
                                return arr.id == member.condition
                            })[0];

                            if (data4 != undefined) {
                                member.detailEngName = data4.name;
                            }


                            if ($window.sessionStorage.language == 1) {

                                var data5 = $scope.stepsdsply.filter(function (arr) {
                                    return arr.id == member.step
                                })[0];

                                if (data5 != undefined) {
                                    member.stepname = data5.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data5 = $scope.stepsdsply.filter(function (arr) {
                                    return arr.parentId == member.step
                                })[0];

                                if (data5 != undefined) {
                                    member.stepname = data5.name;
                                }
                            }

                            var data6 = $scope.stepsdsplyNew.filter(function (arr) {
                                return arr.id == member.step
                            })[0];

                            if (data6 != undefined) {
                                member.stepEngName = data6.name;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data7 = $scope.statusesdsply.filter(function (arr) {
                                    return arr.id == member.status
                                })[0];

                                if (data7 != undefined) {
                                    member.statusname = data7.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data7 = $scope.statusesdsply.filter(function (arr) {
                                    return arr.parentId == member.status
                                })[0];

                                if (data7 != undefined) {
                                    member.statusname = data7.name;
                                }
                            }

                            var data8 = $scope.statusesdsplyNew.filter(function (arr) {
                                return arr.id == member.status
                            })[0];

                            if (data8 != undefined) {
                                member.statusEngName = data8.name;
                            }

                            var data9 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data9 != undefined) {
                                member.countryname = data9.name;
                            }

                            var data10 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == member.stateId
                            })[0];

                            if (data10 != undefined) {
                                member.statename = data10.name;
                            }

                            var data11 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == member.districtId
                            })[0];

                            if (data11 != undefined) {
                                member.districtname = data11.name;
                            }

                            var data12 = $scope.sitedsply.filter(function (arr) {
                                return arr.id == member.siteId
                            })[0];

                            if (data12 != undefined) {
                                member.sitename = data12.name;
                            }

                            member.statusStep = member.stepname + ' / ' + member.statusname;
                            member.statusStepEngName = member.stepEngName + ' / ' + member.statusEngName;

                            tdArray.push(member);
                            typecount++;

                            if (tArray.length == typecount) {
                                $scope.todoArray = tdArray;
                                $scope.DisableStatus = false;

                                angular.forEach($scope.todoArray, function (member, index) {
                                    member.index = index + 1;
                                });
                            }
                        });
                    });

                } else if (newValue == 'scheme') {

                    if ($window.sessionStorage.roleId + "" === "3") {
                        $scope.filterCallSchemes = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';
                    } else {
                        $scope.filterCallSchemes = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}}]}}';
                    }

                    Restangular.all($scope.filterCallSchemes).getList().then(function (schme) {

                        var tArray = $filter('orderBy')(schme, 'datetime');

                        angular.forEach(tArray, function (member, index) {
                            member.index = index + 1;
                            member.type = 'Scheme';

                            member.sortingDate = member.datetime;
                            member.date = $filter('date')(member.datetime, 'dd-MMM-yyyy');
                            member.dateExport = $filter('date')(member.datetime, 'dd/MMM/yyyy');
                            member.datetime = $filter('date')(member.datetime, 'dd/MM/yyyy');
                            member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                            member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                            if (member.date == null) {
                                member.date = '';
                            }

                            if (member.stage == 7 || member.stage == 9) {
                                member.exp_status = 'Closed';
                            } else if (member.stage != 7 || member.stage != 9) {
                                member.exp_status = 'Opened';
                            }

                            var data = $scope.memsdisply.filter(function (arr) {
                                return arr.id == member.memberId
                            })[0];

                            if (data != undefined) {
                                member.membername = data.name;
                                member.individualId = data.individualId;
                                member.HHId = data.HHId;
                                member.mobile = data.mobile;

                                var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                    return arr.id == data.parentId
                                })[0];

                                if (data1 != undefined) {
                                    member.hhName = data1.nameOfHeadInHH;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data11 = $scope.finalSchemes.filter(function (arr) {
                                    return arr.id == member.schemeId
                                })[0];

                                if (data11 != undefined) {
                                    member.detail = data11.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data11 = $scope.finalSchemes.filter(function (arr) {
                                    return arr.parentId == member.schemeId
                                })[0];

                                if (data11 != undefined) {
                                    member.detail = data11.name;
                                }
                            }

                            var data2 = $scope.finalSchemesNew.filter(function (arr) {
                                return arr.id == member.schemeId
                            })[0];

                            if (data2 != undefined) {
                                member.detailEngName = data2.name;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data3 = $scope.finalSchemeStages.filter(function (arr) {
                                    return arr.id == member.stage
                                })[0];

                                if (data3 != undefined) {
                                    member.statusStep = data3.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data3 = $scope.finalSchemeStages.filter(function (arr) {
                                    return arr.parentId == member.stage
                                })[0];

                                if (data3 != undefined) {
                                    member.statusStep = data3.name;
                                }
                            }

                            var data4 = $scope.finalSchemeStagesNew.filter(function (arr) {
                                return arr.id == member.stage
                            })[0];

                            if (data4 != undefined) {
                                member.statusStepEngName = data4.name;
                            }

                            var data5 = $scope.responcedreceived.filter(function (arr) {
                                return arr.id == member.responserecieve
                            })[0];

                            if (data5 != undefined) {
                                member.responsereceivedName = data5.name;
                            }

                            var data6 = $scope.reasonforrejections.filter(function (arr) {
                                return arr.id == member.rejection
                            })[0];

                            if (data6 != undefined) {
                                member.rejectionName = data6.name;
                            }

                            var data7 = $scope.reasonfordelayed.filter(function (arr) {
                                return arr.id == member.delay
                            })[0];

                            if (data7 != undefined) {
                                member.delayName = data7.name;
                            }

                            var data8 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.associatedHF
                            })[0];

                            if (data8 != undefined) {
                                member.assignedTo = data8.name;
                            }

                            var data9 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data9 != undefined) {
                                member.countryname = data9.name;
                            }

                            var data10 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == member.stateId
                            })[0];

                            if (data10 != undefined) {
                                member.statename = data10.name;
                            }

                            var data11 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == member.districtId
                            })[0];

                            if (data11 != undefined) {
                                member.districtname = data11.name;
                            }

                            var data12 = $scope.sitedsply.filter(function (arr) {
                                return arr.id == member.siteId
                            })[0];

                            if (data12 != undefined) {
                                member.sitename = data12.name;
                            }

                            tdArray.push(member);
                            typecount++;

                            if (tArray.length == typecount) {
                                $scope.todoArray = tdArray;
                                $scope.DisableStatus = false;

                                angular.forEach($scope.todoArray, function (member, index) {
                                    member.index = index + 1;
                                });
                            }
                        });
                    });

                } else if (newValue == 'document') {

                    if ($window.sessionStorage.roleId + "" === "3") {
                        $scope.filterCallDocuments = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';
                    } else {
                        $scope.filterCallDocuments = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';
                    }

                    Restangular.all($scope.filterCallDocuments).getList().then(function (doc) {

                        var tArray = $filter('orderBy')(doc, 'datetime');

                        angular.forEach(tArray, function (member, index) {
                            member.index = index + 1;
                            member.type = 'Document';

                            member.sortingDate = member.datetime;
                            member.date = $filter('date')(member.datetime, 'dd-MMM-yyyy');
                            member.dateExport = $filter('date')(member.datetime, 'dd/MMM/yyyy');
                            member.datetime = $filter('date')(member.datetime, 'dd/MM/yyyy');
                            member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                            member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                            if (member.date == null) {
                                member.date = '';
                            }

                            if (member.stage == 7 || member.stage == 9) {
                                member.exp_status = 'Closed';
                            } else if (member.stage != 7 || member.stage != 9) {
                                member.exp_status = 'Opened';
                            }

                            var data = $scope.memsdisply.filter(function (arr) {
                                return arr.id == member.memberId
                            })[0];

                            if (data != undefined) {
                                member.membername = data.name;
                                member.individualId = data.individualId;
                                member.HHId = data.HHId;
                                member.mobile = data.mobile;

                                var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                    return arr.id == data.parentId
                                })[0];

                                if (data1 != undefined) {
                                    member.hhName = data1.nameOfHeadInHH;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data2 = $scope.finalDocs.filter(function (arr) {
                                    return arr.id == member.schemeId
                                })[0];

                                if (data2 != undefined) {
                                    member.detail = data2.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data2 = $scope.finalDocs.filter(function (arr) {
                                    return arr.parentId == member.schemeId
                                })[0];

                                if (data2 != undefined) {
                                    member.detail = data2.name;
                                }
                            }

                            var data3 = $scope.finalDocsNew.filter(function (arr) {
                                return arr.id == member.schemeId
                            })[0];

                            if (data3 != undefined) {
                                member.detailEngName = data3.name;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data4 = $scope.finalDocStages.filter(function (arr) {
                                    return arr.id == member.stage
                                })[0];

                                if (data4 != undefined) {
                                    member.statusStep = data4.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data4 = $scope.finalDocStages.filter(function (arr) {
                                    return arr.parentId == member.stage
                                })[0];

                                if (data4 != undefined) {
                                    member.statusStep = data4.name;
                                }
                            }

                            var data5 = $scope.finalSchemeStagesNew.filter(function (arr) {
                                return arr.id == member.stage
                            })[0];

                            if (data5 != undefined) {
                                member.statusStepEngName = data5.name;
                            }

                            var data6 = $scope.responcedreceived.filter(function (arr) {
                                return arr.id == member.responserecieve
                            })[0];

                            if (data6 != undefined) {
                                member.responsereceivedName = data6.name;
                            }

                            var data7 = $scope.reasonforrejections.filter(function (arr) {
                                return arr.id == member.rejection
                            })[0];

                            if (data7 != undefined) {
                                member.rejectionName = data7.name;
                            }

                            var data8 = $scope.reasonfordelayed.filter(function (arr) {
                                return arr.id == member.delay
                            })[0];

                            if (data8 != undefined) {
                                member.delayName = data8.name;
                            }

                            var data9 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.associatedHF
                            })[0];

                            if (data9 != undefined) {
                                member.assignedTo = data9.name;
                            }

                            var data10 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data10 != undefined) {
                                member.countryname = data10.name;
                            }

                            var data11 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == member.stateId
                            })[0];

                            if (data11 != undefined) {
                                member.statename = data11.name;
                            }

                            var data12 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == member.districtId
                            })[0];

                            if (data12 != undefined) {
                                member.districtname = data12.name;
                            }

                            var data13 = $scope.sitedsply.filter(function (arr) {
                                return arr.id == member.siteId
                            })[0];

                            if (data13 != undefined) {
                                member.sitename = data13.name;
                            }

                            tdArray.push(member);
                            typecount++;

                            if (tArray.length == typecount) {
                                $scope.todoArray = tdArray;
                                $scope.DisableStatus = false;

                                angular.forEach($scope.todoArray, function (member, index) {
                                    member.index = index + 1;
                                });
                            }
                        });
                    });

                } else if (newValue == 'incident') {

                    var key;

                    if ($window.sessionStorage.roleId + "" === "3") {
                        $scope.filterCallIncidents = 'reportincidents?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                    } else {
                        $scope.filterCallIncidents = 'reportincidents?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                    }

                    Restangular.all($scope.filterCallIncidents).getList().then(function (rincident) {

                        var tArray = $filter('orderBy')(rincident, 'followupdate');

                        angular.forEach(tArray, function (member, index) {
                            member.index = index + 1;
                            member.type = 'Incident';

                            member.sortingDate = member.followupdate;
                            member.date = $filter('date')(member.followupdate, 'dd-MMM-yyyy');
                            member.dateExport = $filter('date')(member.followupdate, 'dd/MMM/yyyy');
                            member.incidentdate = $filter('date')(member.incidentdate, 'dd/MM/yyyy');
                            member.dateofclosure = $filter('date')(member.dateofclosure, 'dd/MM/yyyy');
                            member.followupdate = $filter('date')(member.followupdate, 'dd/MM/yyyy');
                            member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                            member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                            if (member.dateofclosure == null) {
                                member.dateofclosure = '';
                            }
                            if (member.lastModifiedDate == null) {
                                member.lastModifiedDate = '';
                            }
                            if (member.date == null) {
                                member.date = '';
                            }

                            if (member.currentstatus == 5 || member.currentstatus == 9) {
                                member.exp_status = 'Closed';
                            } else if (member.currentstatus != 5 || member.currentstatus != 9) {
                                member.exp_status = 'Opened';
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data = $scope.typeofincidentsdsply.filter(function (arr) {
                                    return arr.id == member.incidenttype
                                })[0];

                                if (data != undefined) {
                                    member.detail = data.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data = $scope.typeofincidentsdsply.filter(function (arr) {
                                    return arr.parentId == member.incidenttype
                                })[0];

                                if (data != undefined) {
                                    member.detail = data.name;
                                }
                            }

                            var data1 = $scope.typeofincidentsdsplyNew.filter(function (arr) {
                                return arr.id == member.incidenttype
                            })[0];

                            if (data1 != undefined) {
                                member.detailEngName = data1.name;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data2 = $scope.currentstatusofcasesdsply.filter(function (arr) {
                                    return arr.id == member.currentstatus
                                })[0];

                                if (data2 != undefined) {
                                    member.currentstatusname = data2.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data2 = $scope.currentstatusofcasesdsply.filter(function (arr) {
                                    return arr.parentId == member.currentstatus
                                })[0];

                                if (data2 != undefined) {
                                    member.currentstatusname = data2.name;
                                }
                            }

                            var data3 = $scope.currentstatusofcasesdsplyNew.filter(function (arr) {
                                return arr.id == member.currentstatus
                            })[0];

                            if (data3 != undefined) {
                                member.currentstatusEngname = data3.name;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data4 = $scope.reportincidentfollowupsdsply.filter(function (arr) {
                                    return arr.id == member.followupneeded
                                })[0];

                                if (data4 != undefined) {
                                    member.statusStep = data4.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data4 = $scope.reportincidentfollowupsdsply.filter(function (arr) {
                                    return arr.parentId == member.followupneeded
                                })[0];

                                if (data4 != undefined) {
                                    member.statusStep = data4.name;
                                }
                            }

                            var data5 = $scope.reportincidentfollowupsdsplyNew.filter(function (arr) {
                                return arr.id == member.followupneeded
                            })[0];

                            if (data5 != undefined) {
                                member.statusStepEngName = data5.name;
                            }

                            var data6 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.associatedHF
                            })[0];

                            if (data6 != undefined) {
                                member.assignedTo = data6.name;
                            }

                            var data7 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data7 != undefined) {
                                member.countryname = data7.name;
                            }

                            var data8 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == member.stateId
                            })[0];

                            if (data8 != undefined) {
                                member.statename = data8.name;
                            }

                            var data9 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == member.districtId
                            })[0];

                            if (data9 != undefined) {
                                member.districtname = data9.name;
                            }

                            var data10 = $scope.sitedsply.filter(function (arr) {
                                return arr.id == member.siteId
                            })[0];

                            if (data10 != undefined) {
                                member.sitename = data10.name;
                            }

                            if (member.multiplemembers != null) {
                                member.multiplemembersLength = member.multiplemembers.split(',').length;
                                member.arraymultiplemembers = member.multiplemembers.split(",");
                                member.membername = "";
                                member.individualId = "";
                                member.HHId = "";
                                member.mobile = "";
                                member.hhName = "";

                                for (var i = 0; i < member.arraymultiplemembers.length; i++) {
                                    for (var j = 0; j < $scope.finalMembers.length; j++) {
                                        if ($scope.finalMembers[j].id + "" === member.arraymultiplemembers[i] + "") {
                                            if (member.membername === "") {
                                                member.membername = $scope.finalMembers[j].name;
                                                member.individualId = $scope.finalMembers[j].individualId;
                                                member.HHId = $scope.finalMembers[j].HHId;
                                                member.mobile = $scope.finalMembers[j].mobile;

                                                var data11 = $scope.memsdisply.filter(function (arr) {
                                                    return arr.id == $scope.finalMembers[j].id
                                                })[0];

                                                if (data11 != undefined) {

                                                    var data12 = $scope.houseHoldsDisplay.filter(function (arr) {
                                                        return arr.id == data11.parentId
                                                    })[0];

                                                    if (data12 != undefined) {
                                                        member.hhName = data12.nameOfHeadInHH;
                                                    }
                                                }

                                            } else {
                                                member.membername = member.membername + "," + $scope.finalMembers[j].name;
                                                member.individualId = member.individualId + "," + $scope.finalMembers[j].individualId;
                                                member.HHId = member.HHId + "," + $scope.finalMembers[j].HHId;
                                                member.mobile = member.mobile + "," + $scope.finalMembers[j].mobile;

                                                var data11 = $scope.memsdisply.filter(function (arr) {
                                                    return arr.id == $scope.finalMembers[j].id
                                                })[0];

                                                if (data11 != undefined) {

                                                    var data12 = $scope.houseHoldsDisplay.filter(function (arr) {
                                                        return arr.id == data11.parentId
                                                    })[0];

                                                    if (data12 != undefined) {
                                                        member.hhName = member.hhName + "," + data12.nameOfHeadInHH;
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                            } else {
                                member.membername = "";
                                member.individualId = "";
                                member.HHId = "";
                                member.mobile = "";
                                member.membersInGroupLength = 0;
                            }

                            for (key in member) {

                                if (member[key] === true) {
                                    member[key] = 'Yes';
                                } else if (member[key] === false) {
                                    member[key] = 'No';
                                }
                                // console.log(member[key]);
                            }

                            tdArray.push(member);
                            typecount++;

                            if (tArray.length == typecount) {
                                $scope.todoArray = tdArray;
                                $scope.DisableStatus = false;

                                angular.forEach($scope.todoArray, function (member, index) {
                                    member.index = index + 1;
                                });
                            }

                            $scope.TotalData = [];
                            $scope.TotalData.push(member);
                        });
                    });
                }
            }
        });

        /********************************** Status Filter ******************************/

        $scope.$watch('status', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {

                $scope.todoArray = [];

                var tdArray = [];

                var typecount = 0;

                $scope.currentPage = 1;

                console.log(newValue);

                if (newValue == 'opened') {

                    if ($scope.typeId == 'condition') {

                        if ($window.sessionStorage.roleId + "" === "3") {
                            $scope.filterCallCondition = 'conditionheaders?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"status":{"nin":[7,5]}},{"caseClosed":{"inq":[false]}}]}}';
                        } else {
                            $scope.filterCallCondition = 'conditionheaders?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"status":{"nin":[7,5]}},{"caseClosed":{"inq":[false]}}]}}';
                        }

                        Restangular.all($scope.filterCallCondition).getList().then(function (cns) {

                            var tArray = $filter('orderBy')(cns, 'followupdate');

                            angular.forEach(tArray, function (member, index) {

                                member.index = index + 1;

                                member.type = 'Screen and Treat';
                                member.sortingDate = member.followupdate;
                                member.date = $filter('date')(member.followupdate, 'dd-MMM-yyyy');
                                member.dateExport = $filter('date')(member.followupdate, 'dd-MM-yyyy');

                                if (member.date == null) {
                                    member.date = '';
                                }

                                var data = $scope.memsdisply.filter(function (arr) {
                                    return arr.id == member.memberId
                                })[0];

                                if (data != undefined) {
                                    member.membername = data.name;
                                    member.individualId = data.individualId;
                                    member.HHId = data.HHId;
                                    member.mobile = data.mobile;

                                    var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                        return arr.id == data.parentId
                                    })[0];

                                    if (data1 != undefined) {
                                        member.hhName = data1.nameOfHeadInHH;
                                    }
                                }

                                var data2 = $scope.usersdisplay.filter(function (arr) {
                                    return arr.id == member.associatedHF
                                })[0];

                                if (data2 != undefined) {
                                    member.assignedTo = data2.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data3 = $scope.conditiondsply.filter(function (arr) {
                                        return arr.id == member.condition
                                    })[0];

                                    if (data3 != undefined) {
                                        member.detail = data3.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data3 = $scope.conditiondsply.filter(function (arr) {
                                        return arr.parentId == member.condition
                                    })[0];

                                    if (data3 != undefined) {
                                        member.detail = data3.name;
                                    }
                                }

                                var data4 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.id == member.condition
                                })[0];

                                if (data4 != undefined) {
                                    member.detailEngName = data4.name;
                                }


                                if ($window.sessionStorage.language == 1) {

                                    var data5 = $scope.stepsdsply.filter(function (arr) {
                                        return arr.id == member.step
                                    })[0];

                                    if (data5 != undefined) {
                                        member.stepname = data5.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data5 = $scope.stepsdsply.filter(function (arr) {
                                        return arr.parentId == member.step
                                    })[0];

                                    if (data5 != undefined) {
                                        member.stepname = data5.name;
                                    }
                                }

                                var data6 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.id == member.step
                                })[0];

                                if (data6 != undefined) {
                                    member.stepEngName = data6.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data7 = $scope.statusesdsply.filter(function (arr) {
                                        return arr.id == member.status
                                    })[0];

                                    if (data7 != undefined) {
                                        member.statusname = data7.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data7 = $scope.statusesdsply.filter(function (arr) {
                                        return arr.parentId == member.status
                                    })[0];

                                    if (data7 != undefined) {
                                        member.statusname = data7.name;
                                    }
                                }

                                var data8 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.id == member.status
                                })[0];

                                if (data8 != undefined) {
                                    member.statusEngName = data8.name;
                                }

                                var data9 = $scope.countrydisplay.filter(function (arr) {
                                    return arr.id == member.countryId
                                })[0];

                                if (data9 != undefined) {
                                    member.countryname = data9.name;
                                }

                                var data10 = $scope.statedisplay.filter(function (arr) {
                                    return arr.id == member.stateId
                                })[0];

                                if (data10 != undefined) {
                                    member.statename = data10.name;
                                }

                                var data11 = $scope.districtdsply.filter(function (arr) {
                                    return arr.id == member.districtId
                                })[0];

                                if (data11 != undefined) {
                                    member.districtname = data11.name;
                                }

                                var data12 = $scope.sitedsply.filter(function (arr) {
                                    return arr.id == member.siteId
                                })[0];

                                if (data12 != undefined) {
                                    member.sitename = data12.name;
                                }

                                member.statusStep = member.stepname + ' / ' + member.statusname;
                                member.statusStepEngName = member.stepEngName + ' / ' + member.statusEngName;

                                tdArray.push(member);
                                typecount++;

                                if (tArray.length == typecount) {
                                    $scope.todoArray = tdArray;

                                    angular.forEach($scope.todoArray, function (member, index) {
                                        member.index = index + 1;
                                    });
                                }
                            });
                        });

                    } else if ($scope.typeId == 'scheme') {

                        if ($window.sessionStorage.roleId + "" === "3") {
                            $scope.filterCallSchemes = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}},{"stage":{"nin":[7,9]}}]}}';
                        } else {
                            $scope.filterCallSchemes = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}},{"stage":{"nin":[7,9]}}]}}';
                        }

                        Restangular.all($scope.filterCallSchemes).getList().then(function (schme) {

                            var tArray = $filter('orderBy')(schme, 'datetime');

                            angular.forEach(tArray, function (member, index) {
                                member.index = index + 1;
                                member.type = 'Scheme';

                                member.sortingDate = member.datetime;
                                member.date = $filter('date')(member.datetime, 'dd-MMM-yyyy');
                                member.dateExport = $filter('date')(member.datetime, 'dd/MMM/yyyy');
                                member.datetime = $filter('date')(member.datetime, 'dd/MM/yyyy');

                                if (member.date == null) {
                                    member.date = '';
                                }

                                if (member.stage == 7 || member.stage == 9) {
                                    member.exp_status = 'Closed';
                                } else if (member.stage != 7 || member.stage != 9) {
                                    member.exp_status = 'Opened';
                                }

                                var data = $scope.memsdisply.filter(function (arr) {
                                    return arr.id == member.memberId
                                })[0];

                                if (data != undefined) {
                                    member.membername = data.name;
                                    member.individualId = data.individualId;
                                    member.HHId = data.HHId;
                                    member.mobile = data.mobile;

                                    var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                        return arr.id == data.parentId
                                    })[0];

                                    if (data1 != undefined) {
                                        member.hhName = data1.nameOfHeadInHH;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data11 = $scope.finalSchemes.filter(function (arr) {
                                        return arr.id == member.schemeId
                                    })[0];

                                    if (data11 != undefined) {
                                        member.detail = data11.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data11 = $scope.finalSchemes.filter(function (arr) {
                                        return arr.parentId == member.schemeId
                                    })[0];

                                    if (data11 != undefined) {
                                        member.detail = data11.name;
                                    }
                                }

                                var data2 = $scope.finalSchemesNew.filter(function (arr) {
                                    return arr.id == member.schemeId
                                })[0];

                                if (data2 != undefined) {
                                    member.detailEngName = data2.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data3 = $scope.finalSchemeStages.filter(function (arr) {
                                        return arr.id == member.stage
                                    })[0];

                                    if (data3 != undefined) {
                                        member.statusStep = data3.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data3 = $scope.finalSchemeStages.filter(function (arr) {
                                        return arr.parentId == member.stage
                                    })[0];

                                    if (data3 != undefined) {
                                        member.statusStep = data3.name;
                                    }
                                }

                                var data4 = $scope.finalSchemeStagesNew.filter(function (arr) {
                                    return arr.id == member.stage
                                })[0];

                                if (data4 != undefined) {
                                    member.statusStepEngName = data4.name;
                                }

                                var data5 = $scope.responcedreceived.filter(function (arr) {
                                    return arr.id == member.responserecieve
                                })[0];

                                if (data5 != undefined) {
                                    member.responsereceivedName = data5.name;
                                }

                                var data6 = $scope.reasonforrejections.filter(function (arr) {
                                    return arr.id == member.rejection
                                })[0];

                                if (data6 != undefined) {
                                    member.rejectionName = data6.name;
                                }

                                var data7 = $scope.reasonfordelayed.filter(function (arr) {
                                    return arr.id == member.delay
                                })[0];

                                if (data7 != undefined) {
                                    member.delayName = data7.name;
                                }

                                var data8 = $scope.usersdisplay.filter(function (arr) {
                                    return arr.id == member.associatedHF
                                })[0];

                                if (data8 != undefined) {
                                    member.assignedTo = data8.name;
                                }

                                var data9 = $scope.countrydisplay.filter(function (arr) {
                                    return arr.id == member.countryId
                                })[0];

                                if (data9 != undefined) {
                                    member.countryname = data9.name;
                                }

                                var data10 = $scope.statedisplay.filter(function (arr) {
                                    return arr.id == member.stateId
                                })[0];

                                if (data10 != undefined) {
                                    member.statename = data10.name;
                                }

                                var data11 = $scope.districtdsply.filter(function (arr) {
                                    return arr.id == member.districtId
                                })[0];

                                if (data11 != undefined) {
                                    member.districtname = data11.name;
                                }

                                var data12 = $scope.sitedsply.filter(function (arr) {
                                    return arr.id == member.siteId
                                })[0];

                                if (data12 != undefined) {
                                    member.sitename = data12.name;
                                }

                                tdArray.push(member);
                                typecount++;

                                if (tArray.length == typecount) {
                                    $scope.todoArray = tdArray;

                                    angular.forEach($scope.todoArray, function (member, index) {
                                        member.index = index + 1;
                                    });
                                }
                            });
                        });

                    } else if ($scope.typeId == 'document') {

                        if ($window.sessionStorage.roleId + "" === "3") {
                            $scope.filterCallDocuments = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}},{"stage":{"nin":[7,9]}}]}}';
                        } else {
                            $scope.filterCallDocuments = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}},{"stage":{"nin":[7,9]}}]}}';
                        }

                        Restangular.all($scope.filterCallDocuments).getList().then(function (doc) {

                            var tArray = $filter('orderBy')(doc, 'datetime');

                            angular.forEach(tArray, function (member, index) {
                                member.index = index + 1;
                                member.type = 'Document';

                                member.sortingDate = member.datetime;
                                member.date = $filter('date')(member.datetime, 'dd-MMM-yyyy');
                                member.dateExport = $filter('date')(member.datetime, 'dd/MMM/yyyy');
                                member.datetime = $filter('date')(member.datetime, 'dd/MM/yyyy');
                                member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                                member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                                if (member.date == null) {
                                    member.date = '';
                                }

                                if (member.stage == 7 || member.stage == 9) {
                                    member.exp_status = 'Closed';
                                } else if (member.stage != 7 || member.stage != 9) {
                                    member.exp_status = 'Opened';
                                }

                                var data = $scope.memsdisply.filter(function (arr) {
                                    return arr.id == member.memberId
                                })[0];

                                if (data != undefined) {
                                    member.membername = data.name;
                                    member.individualId = data.individualId;
                                    member.HHId = data.HHId;
                                    member.mobile = data.mobile;

                                    var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                        return arr.id == data.parentId
                                    })[0];

                                    if (data1 != undefined) {
                                        member.hhName = data1.nameOfHeadInHH;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data2 = $scope.finalDocs.filter(function (arr) {
                                        return arr.id == member.schemeId
                                    })[0];

                                    if (data2 != undefined) {
                                        member.detail = data2.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data2 = $scope.finalDocs.filter(function (arr) {
                                        return arr.parentId == member.schemeId
                                    })[0];

                                    if (data2 != undefined) {
                                        member.detail = data2.name;
                                    }
                                }

                                var data3 = $scope.finalDocsNew.filter(function (arr) {
                                    return arr.id == member.schemeId
                                })[0];

                                if (data3 != undefined) {
                                    member.detailEngName = data3.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data4 = $scope.finalDocStages.filter(function (arr) {
                                        return arr.id == member.stage
                                    })[0];

                                    if (data4 != undefined) {
                                        member.statusStep = data4.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data4 = $scope.finalDocStages.filter(function (arr) {
                                        return arr.parentId == member.stage
                                    })[0];

                                    if (data4 != undefined) {
                                        member.statusStep = data4.name;
                                    }
                                }

                                var data5 = $scope.finalSchemeStagesNew.filter(function (arr) {
                                    return arr.id == member.stage
                                })[0];

                                if (data5 != undefined) {
                                    member.statusStepEngName = data5.name;
                                }

                                var data6 = $scope.responcedreceived.filter(function (arr) {
                                    return arr.id == member.responserecieve
                                })[0];

                                if (data6 != undefined) {
                                    member.responsereceivedName = data6.name;
                                }

                                var data7 = $scope.reasonforrejections.filter(function (arr) {
                                    return arr.id == member.rejection
                                })[0];

                                if (data7 != undefined) {
                                    member.rejectionName = data7.name;
                                }

                                var data8 = $scope.reasonfordelayed.filter(function (arr) {
                                    return arr.id == member.delay
                                })[0];

                                if (data8 != undefined) {
                                    member.delayName = data8.name;
                                }

                                var data9 = $scope.usersdisplay.filter(function (arr) {
                                    return arr.id == member.associatedHF
                                })[0];

                                if (data9 != undefined) {
                                    member.assignedTo = data9.name;
                                }

                                var data10 = $scope.countrydisplay.filter(function (arr) {
                                    return arr.id == member.countryId
                                })[0];

                                if (data10 != undefined) {
                                    member.countryname = data10.name;
                                }

                                var data11 = $scope.statedisplay.filter(function (arr) {
                                    return arr.id == member.stateId
                                })[0];

                                if (data11 != undefined) {
                                    member.statename = data11.name;
                                }

                                var data12 = $scope.districtdsply.filter(function (arr) {
                                    return arr.id == member.districtId
                                })[0];

                                if (data12 != undefined) {
                                    member.districtname = data12.name;
                                }

                                var data13 = $scope.sitedsply.filter(function (arr) {
                                    return arr.id == member.siteId
                                })[0];

                                if (data13 != undefined) {
                                    member.sitename = data13.name;
                                }

                                tdArray.push(member);
                                typecount++;

                                if (tArray.length == typecount) {
                                    $scope.todoArray = tdArray;

                                    angular.forEach($scope.todoArray, function (member, index) {
                                        member.index = index + 1;
                                    });
                                }
                            });
                        });

                    } else if ($scope.typeId == 'incident') {

                        var key;

                        if ($window.sessionStorage.roleId + "" === "3") {
                            $scope.filterCallIncidents = 'reportincidents?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"currentstatus":{"nin":[5,9]}}]}}';
                        } else {
                            $scope.filterCallIncidents = 'reportincidents?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"currentstatus":{"nin":[5,9]}}]}}';
                        }

                        Restangular.all($scope.filterCallIncidents).getList().then(function (rincident) {

                            var tArray = $filter('orderBy')(rincident, 'followupdate');

                            angular.forEach(tArray, function (member, index) {
                                member.index = index + 1;
                                member.type = 'Incident';

                                member.sortingDate = member.followupdate;
                                member.date = $filter('date')(member.followupdate, 'dd-MMM-yyyy');
                                member.dateExport = $filter('date')(member.followupdate, 'dd/MMM/yyyy');
                                member.incidentdate = $filter('date')(member.incidentdate, 'dd/MM/yyyy');
                                member.dateofclosure = $filter('date')(member.dateofclosure, 'dd/MM/yyyy');
                                member.followupdate = $filter('date')(member.followupdate, 'dd/MM/yyyy');

                                if (member.dateofclosure == null) {
                                    member.dateofclosure = '';
                                }
                                if (member.lastModifiedDate == null) {
                                    member.lastModifiedDate = '';
                                }
                                if (member.date == null) {
                                    member.date = '';
                                }

                                if (member.currentstatus == 5 || member.currentstatus == 9) {
                                    member.exp_status = 'Closed';
                                } else if (member.currentstatus != 5 || member.currentstatus != 9) {
                                    member.exp_status = 'Opened';
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data = $scope.typeofincidentsdsply.filter(function (arr) {
                                        return arr.id == member.incidenttype
                                    })[0];

                                    if (data != undefined) {
                                        member.detail = data.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data = $scope.typeofincidentsdsply.filter(function (arr) {
                                        return arr.parentId == member.incidenttype
                                    })[0];

                                    if (data != undefined) {
                                        member.detail = data.name;
                                    }
                                }

                                var data1 = $scope.typeofincidentsdsplyNew.filter(function (arr) {
                                    return arr.id == member.incidenttype
                                })[0];

                                if (data1 != undefined) {
                                    member.detailEngName = data1.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data2 = $scope.currentstatusofcasesdsply.filter(function (arr) {
                                        return arr.id == member.currentstatus
                                    })[0];

                                    if (data2 != undefined) {
                                        member.currentstatusname = data2.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data2 = $scope.currentstatusofcasesdsply.filter(function (arr) {
                                        return arr.parentId == member.currentstatus
                                    })[0];

                                    if (data2 != undefined) {
                                        member.currentstatusname = data2.name;
                                    }
                                }

                                var data3 = $scope.currentstatusofcasesdsplyNew.filter(function (arr) {
                                    return arr.id == member.currentstatus
                                })[0];

                                if (data3 != undefined) {
                                    member.currentstatusEngname = data3.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data4 = $scope.reportincidentfollowupsdsply.filter(function (arr) {
                                        return arr.id == member.followupneeded
                                    })[0];

                                    if (data4 != undefined) {
                                        member.statusStep = data4.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data4 = $scope.reportincidentfollowupsdsply.filter(function (arr) {
                                        return arr.parentId == member.followupneeded
                                    })[0];

                                    if (data4 != undefined) {
                                        member.statusStep = data4.name;
                                    }
                                }

                                var data5 = $scope.reportincidentfollowupsdsplyNew.filter(function (arr) {
                                    return arr.id == member.followupneeded
                                })[0];

                                if (data5 != undefined) {
                                    member.statusStepEngName = data5.name;
                                }

                                var data6 = $scope.usersdisplay.filter(function (arr) {
                                    return arr.id == member.associatedHF
                                })[0];

                                if (data6 != undefined) {
                                    member.assignedTo = data6.name;
                                }

                                var data7 = $scope.countrydisplay.filter(function (arr) {
                                    return arr.id == member.countryId
                                })[0];

                                if (data7 != undefined) {
                                    member.countryname = data7.name;
                                }

                                var data8 = $scope.statedisplay.filter(function (arr) {
                                    return arr.id == member.stateId
                                })[0];

                                if (data8 != undefined) {
                                    member.statename = data8.name;
                                }

                                var data9 = $scope.districtdsply.filter(function (arr) {
                                    return arr.id == member.districtId
                                })[0];

                                if (data9 != undefined) {
                                    member.districtname = data9.name;
                                }

                                var data10 = $scope.sitedsply.filter(function (arr) {
                                    return arr.id == member.siteId
                                })[0];

                                if (data10 != undefined) {
                                    member.sitename = data10.name;
                                }

                                if (member.multiplemembers != null) {
                                    member.multiplemembersLength = member.multiplemembers.split(',').length;
                                    member.arraymultiplemembers = member.multiplemembers.split(",");
                                    member.membername = "";
                                    member.individualId = "";
                                    member.HHId = "";
                                    member.mobile = "";
                                    member.hhName = "";

                                    for (var i = 0; i < member.arraymultiplemembers.length; i++) {
                                        for (var j = 0; j < $scope.finalMembers.length; j++) {
                                            if ($scope.finalMembers[j].id + "" === member.arraymultiplemembers[i] + "") {
                                                if (member.membername === "") {
                                                    member.membername = $scope.finalMembers[j].name;
                                                    member.individualId = $scope.finalMembers[j].individualId;
                                                    member.HHId = $scope.finalMembers[j].HHId;
                                                    member.mobile = $scope.finalMembers[j].mobile;

                                                    var data11 = $scope.memsdisply.filter(function (arr) {
                                                        return arr.id == $scope.finalMembers[j].id
                                                    })[0];

                                                    if (data11 != undefined) {

                                                        var data12 = $scope.houseHoldsDisplay.filter(function (arr) {
                                                            return arr.id == data11.parentId
                                                        })[0];

                                                        if (data12 != undefined) {
                                                            member.hhName = data12.nameOfHeadInHH;
                                                        }
                                                    }

                                                } else {
                                                    member.membername = member.membername + "," + $scope.finalMembers[j].name;
                                                    member.individualId = member.individualId + "," + $scope.finalMembers[j].individualId;
                                                    member.HHId = member.HHId + "," + $scope.finalMembers[j].HHId;
                                                    member.mobile = member.mobile + "," + $scope.finalMembers[j].mobile;

                                                    var data11 = $scope.memsdisply.filter(function (arr) {
                                                        return arr.id == $scope.finalMembers[j].id
                                                    })[0];

                                                    if (data11 != undefined) {

                                                        var data12 = $scope.houseHoldsDisplay.filter(function (arr) {
                                                            return arr.id == data11.parentId
                                                        })[0];

                                                        if (data12 != undefined) {
                                                            member.hhName = member.hhName + "," + data12.nameOfHeadInHH;
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    member.membername = "";
                                    member.individualId = "";
                                    member.HHId = "";
                                    member.mobile = "";
                                    member.membersInGroupLength = 0;
                                }

                                for (key in member) {

                                    if (member[key] === true) {
                                        member[key] = 'Yes';
                                    } else if (member[key] === false) {
                                        member[key] = 'No';
                                    }
                                    // console.log(member[key]);
                                }

                                tdArray.push(member);
                                typecount++;

                                if (tArray.length == typecount) {
                                    $scope.todoArray = tdArray;

                                    angular.forEach($scope.todoArray, function (member, index) {
                                        member.index = index + 1;
                                    });
                                }

                                $scope.TotalData = [];
                                $scope.TotalData.push(member);
                            });
                        });

                    }
                } else if (newValue == 'closed') {

                    if ($scope.typeId == 'condition') {

                        if ($window.sessionStorage.roleId + "" === "3") {

                            $scope.filterCallCondition = 'conditionheaders?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"status":{"inq":[5,7]}}]}}';
                            //,{"caseClosed":{"inq":[true]}}
                        } else {
                            $scope.filterCallCondition = 'conditionheaders?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"status":{"inq":[5,7]}}]}}';
                            // {"caseClosed":{"inq":[true]}},
                        }

                        Restangular.all($scope.filterCallCondition).getList().then(function (cns) {

                            var tArray = $filter('orderBy')(cns, 'followupdate');

                            angular.forEach(tArray, function (member, index) {
                                member.index = index + 1;

                                if (member.step == 5 && member.status == 5 && member.caseClosed == false) {
                                    $scope.todoArray.splice(member.index, 1);
                                }

                                member.type = 'Screen and Treat';
                                member.sortingDate = member.followupdate;
                                member.date = $filter('date')(member.followupdate, 'dd-MMM-yyyy');
                                member.dateExport = $filter('date')(member.followupdate, 'dd-MM-yyyy');

                                if (member.date == null) {
                                    member.date = '';
                                }

                                var data = $scope.memsdisply.filter(function (arr) {
                                    return arr.id == member.memberId
                                })[0];

                                if (data != undefined) {
                                    member.membername = data.name;
                                    member.individualId = data.individualId;
                                    member.HHId = data.HHId;
                                    member.mobile = data.mobile;

                                    var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                        return arr.id == data.parentId
                                    })[0];

                                    if (data1 != undefined) {
                                        member.hhName = data1.nameOfHeadInHH;
                                    }
                                }

                                var data2 = $scope.usersdisplay.filter(function (arr) {
                                    return arr.id == member.associatedHF
                                })[0];

                                if (data2 != undefined) {
                                    member.assignedTo = data2.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data3 = $scope.conditiondsply.filter(function (arr) {
                                        return arr.id == member.condition
                                    })[0];

                                    if (data3 != undefined) {
                                        member.detail = data3.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data3 = $scope.conditiondsply.filter(function (arr) {
                                        return arr.parentId == member.condition
                                    })[0];

                                    if (data3 != undefined) {
                                        member.detail = data3.name;
                                    }
                                }

                                var data4 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.id == member.condition
                                })[0];

                                if (data4 != undefined) {
                                    member.detailEngName = data4.name;
                                }


                                if ($window.sessionStorage.language == 1) {

                                    var data5 = $scope.stepsdsply.filter(function (arr) {
                                        return arr.id == member.step
                                    })[0];

                                    if (data5 != undefined) {
                                        member.stepname = data5.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data5 = $scope.stepsdsply.filter(function (arr) {
                                        return arr.parentId == member.step
                                    })[0];

                                    if (data5 != undefined) {
                                        member.stepname = data5.name;
                                    }
                                }

                                var data6 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.id == member.step
                                })[0];

                                if (data6 != undefined) {
                                    member.stepEngName = data6.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data7 = $scope.statusesdsply.filter(function (arr) {
                                        return arr.id == member.status
                                    })[0];

                                    if (data7 != undefined) {
                                        member.statusname = data7.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data7 = $scope.statusesdsply.filter(function (arr) {
                                        return arr.parentId == member.status
                                    })[0];

                                    if (data7 != undefined) {
                                        member.statusname = data7.name;
                                    }
                                }

                                var data8 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.id == member.status
                                })[0];

                                if (data8 != undefined) {
                                    member.statusEngName = data8.name;
                                }

                                var data9 = $scope.countrydisplay.filter(function (arr) {
                                    return arr.id == member.countryId
                                })[0];

                                if (data9 != undefined) {
                                    member.countryname = data9.name;
                                }

                                var data10 = $scope.statedisplay.filter(function (arr) {
                                    return arr.id == member.stateId
                                })[0];

                                if (data10 != undefined) {
                                    member.statename = data10.name;
                                }

                                var data11 = $scope.districtdsply.filter(function (arr) {
                                    return arr.id == member.districtId
                                })[0];

                                if (data11 != undefined) {
                                    member.districtname = data11.name;
                                }

                                var data12 = $scope.sitedsply.filter(function (arr) {
                                    return arr.id == member.siteId
                                })[0];

                                if (data12 != undefined) {
                                    member.sitename = data12.name;
                                }

                                member.statusStep = member.stepname + ' / ' + member.statusname;
                                member.statusStepEngName = member.stepEngName + ' / ' + member.statusEngName;

                                tdArray.push(member);
                                typecount++;

                                if (tArray.length == typecount) {
                                    $scope.todoArray = tdArray;

                                    angular.forEach($scope.todoArray, function (member, index) {
                                        member.index = index + 1;
                                    });
                                }
                            });
                        });

                    } else if ($scope.typeId == 'scheme') {

                        if ($window.sessionStorage.roleId + "" === "3") {
                            $scope.filterCallSchemes = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}},{"stage":{"inq":[7,9]}}]}}';
                        } else {
                            $scope.filterCallSchemes = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}},{"stage":{"inq":[7,9]}}]}}';
                        }

                        Restangular.all($scope.filterCallSchemes).getList().then(function (schme) {

                            var tArray = $filter('orderBy')(schme, 'datetime');

                            angular.forEach(tArray, function (member, index) {
                                member.index = index + 1;
                                member.type = 'Scheme';

                                member.sortingDate = member.datetime;
                                member.date = $filter('date')(member.datetime, 'dd-MMM-yyyy');
                                member.dateExport = $filter('date')(member.datetime, 'dd/MMM/yyyy');
                                member.datetime = $filter('date')(member.datetime, 'dd/MM/yyyy');

                                if (member.date == null) {
                                    member.date = '';
                                }

                                if (member.stage == 7 || member.stage == 9) {
                                    member.exp_status = 'Closed';
                                } else if (member.stage != 7 || member.stage != 9) {
                                    member.exp_status = 'Opened';
                                }

                                var data = $scope.memsdisply.filter(function (arr) {
                                    return arr.id == member.memberId
                                })[0];

                                if (data != undefined) {
                                    member.membername = data.name;
                                    member.individualId = data.individualId;
                                    member.HHId = data.HHId;
                                    member.mobile = data.mobile;

                                    var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                        return arr.id == data.parentId
                                    })[0];

                                    if (data1 != undefined) {
                                        member.hhName = data1.nameOfHeadInHH;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data11 = $scope.finalSchemes.filter(function (arr) {
                                        return arr.id == member.schemeId
                                    })[0];

                                    if (data11 != undefined) {
                                        member.detail = data11.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data11 = $scope.finalSchemes.filter(function (arr) {
                                        return arr.parentId == member.schemeId
                                    })[0];

                                    if (data11 != undefined) {
                                        member.detail = data11.name;
                                    }
                                }

                                var data2 = $scope.finalSchemesNew.filter(function (arr) {
                                    return arr.id == member.schemeId
                                })[0];

                                if (data2 != undefined) {
                                    member.detailEngName = data2.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data3 = $scope.finalSchemeStages.filter(function (arr) {
                                        return arr.id == member.stage
                                    })[0];

                                    if (data3 != undefined) {
                                        member.statusStep = data3.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data3 = $scope.finalSchemeStages.filter(function (arr) {
                                        return arr.parentId == member.stage
                                    })[0];

                                    if (data3 != undefined) {
                                        member.statusStep = data3.name;
                                    }
                                }

                                var data4 = $scope.finalSchemeStagesNew.filter(function (arr) {
                                    return arr.id == member.stage
                                })[0];

                                if (data4 != undefined) {
                                    member.statusStepEngName = data4.name;
                                }

                                var data5 = $scope.responcedreceived.filter(function (arr) {
                                    return arr.id == member.responserecieve
                                })[0];

                                if (data5 != undefined) {
                                    member.responsereceivedName = data5.name;
                                }

                                var data6 = $scope.reasonforrejections.filter(function (arr) {
                                    return arr.id == member.rejection
                                })[0];

                                if (data6 != undefined) {
                                    member.rejectionName = data6.name;
                                }

                                var data7 = $scope.reasonfordelayed.filter(function (arr) {
                                    return arr.id == member.delay
                                })[0];

                                if (data7 != undefined) {
                                    member.delayName = data7.name;
                                }

                                var data8 = $scope.usersdisplay.filter(function (arr) {
                                    return arr.id == member.associatedHF
                                })[0];

                                if (data8 != undefined) {
                                    member.assignedTo = data8.name;
                                }

                                var data9 = $scope.countrydisplay.filter(function (arr) {
                                    return arr.id == member.countryId
                                })[0];

                                if (data9 != undefined) {
                                    member.countryname = data9.name;
                                }

                                var data10 = $scope.statedisplay.filter(function (arr) {
                                    return arr.id == member.stateId
                                })[0];

                                if (data10 != undefined) {
                                    member.statename = data10.name;
                                }

                                var data11 = $scope.districtdsply.filter(function (arr) {
                                    return arr.id == member.districtId
                                })[0];

                                if (data11 != undefined) {
                                    member.districtname = data11.name;
                                }

                                var data12 = $scope.sitedsply.filter(function (arr) {
                                    return arr.id == member.siteId
                                })[0];

                                if (data12 != undefined) {
                                    member.sitename = data12.name;
                                }

                                tdArray.push(member);
                                typecount++;

                                if (tArray.length == typecount) {
                                    $scope.todoArray = tdArray;

                                    angular.forEach($scope.todoArray, function (member, index) {
                                        member.index = index + 1;
                                    });
                                }
                            });
                        });

                    } else if ($scope.typeId == 'document') {

                        if ($window.sessionStorage.roleId + "" === "3") {
                            $scope.filterCallDocuments = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}},{"stage":{"inq":[7,9]}}]}}';
                        } else {
                            $scope.filterCallDocuments = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}},{"stage":{"inq":[7,9]}}]}}';
                        }

                        Restangular.all($scope.filterCallDocuments).getList().then(function (doc) {

                            var tArray = $filter('orderBy')(doc, 'datetime');

                            angular.forEach(tArray, function (member, index) {
                                member.index = index + 1;
                                member.type = 'Document';

                                member.sortingDate = member.datetime;
                                member.date = $filter('date')(member.datetime, 'dd-MMM-yyyy');
                                member.dateExport = $filter('date')(member.datetime, 'dd/MMM/yyyy');
                                member.datetime = $filter('date')(member.datetime, 'dd/MM/yyyy');
                                member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                                member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                                if (member.date == null) {
                                    member.date = '';
                                }

                                if (member.stage == 7 || member.stage == 9) {
                                    member.exp_status = 'Closed';
                                } else if (member.stage != 7 || member.stage != 9) {
                                    member.exp_status = 'Opened';
                                }

                                var data = $scope.memsdisply.filter(function (arr) {
                                    return arr.id == member.memberId
                                })[0];

                                if (data != undefined) {
                                    member.membername = data.name;
                                    member.individualId = data.individualId;
                                    member.HHId = data.HHId;
                                    member.mobile = data.mobile;

                                    var data1 = $scope.houseHoldsDisplay.filter(function (arr) {
                                        return arr.id == data.parentId
                                    })[0];

                                    if (data1 != undefined) {
                                        member.hhName = data1.nameOfHeadInHH;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data2 = $scope.finalDocs.filter(function (arr) {
                                        return arr.id == member.schemeId
                                    })[0];

                                    if (data2 != undefined) {
                                        member.detail = data2.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data2 = $scope.finalDocs.filter(function (arr) {
                                        return arr.parentId == member.schemeId
                                    })[0];

                                    if (data2 != undefined) {
                                        member.detail = data2.name;
                                    }
                                }

                                var data3 = $scope.finalDocsNew.filter(function (arr) {
                                    return arr.id == member.schemeId
                                })[0];

                                if (data3 != undefined) {
                                    member.detailEngName = data3.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data4 = $scope.finalDocStages.filter(function (arr) {
                                        return arr.id == member.stage
                                    })[0];

                                    if (data4 != undefined) {
                                        member.statusStep = data4.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data4 = $scope.finalDocStages.filter(function (arr) {
                                        return arr.parentId == member.stage
                                    })[0];

                                    if (data4 != undefined) {
                                        member.statusStep = data4.name;
                                    }
                                }

                                var data5 = $scope.finalSchemeStagesNew.filter(function (arr) {
                                    return arr.id == member.stage
                                })[0];

                                if (data5 != undefined) {
                                    member.statusStepEngName = data5.name;
                                }

                                var data6 = $scope.responcedreceived.filter(function (arr) {
                                    return arr.id == member.responserecieve
                                })[0];

                                if (data6 != undefined) {
                                    member.responsereceivedName = data6.name;
                                }

                                var data7 = $scope.reasonforrejections.filter(function (arr) {
                                    return arr.id == member.rejection
                                })[0];

                                if (data7 != undefined) {
                                    member.rejectionName = data7.name;
                                }

                                var data8 = $scope.reasonfordelayed.filter(function (arr) {
                                    return arr.id == member.delay
                                })[0];

                                if (data8 != undefined) {
                                    member.delayName = data8.name;
                                }

                                var data9 = $scope.usersdisplay.filter(function (arr) {
                                    return arr.id == member.associatedHF
                                })[0];

                                if (data9 != undefined) {
                                    member.assignedTo = data9.name;
                                }

                                var data10 = $scope.countrydisplay.filter(function (arr) {
                                    return arr.id == member.countryId
                                })[0];

                                if (data10 != undefined) {
                                    member.countryname = data10.name;
                                }

                                var data11 = $scope.statedisplay.filter(function (arr) {
                                    return arr.id == member.stateId
                                })[0];

                                if (data11 != undefined) {
                                    member.statename = data11.name;
                                }

                                var data12 = $scope.districtdsply.filter(function (arr) {
                                    return arr.id == member.districtId
                                })[0];

                                if (data12 != undefined) {
                                    member.districtname = data12.name;
                                }

                                var data13 = $scope.sitedsply.filter(function (arr) {
                                    return arr.id == member.siteId
                                })[0];

                                if (data13 != undefined) {
                                    member.sitename = data13.name;
                                }

                                tdArray.push(member);
                                typecount++;

                                if (tArray.length == typecount) {
                                    $scope.todoArray = tdArray;

                                    angular.forEach($scope.todoArray, function (member, index) {
                                        member.index = index + 1;
                                    });
                                }
                            });
                        });

                    } else if ($scope.typeId == 'incident') {

                        var key;

                        if ($window.sessionStorage.roleId + "" === "3") {
                            $scope.filterCallIncidents = 'reportincidents?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"currentstatus":{"inq":[5,9]}}]}}';
                        } else {
                            $scope.filterCallIncidents = 'reportincidents?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"currentstatus":{"inq":[5,9]}}]}}';
                        }

                        Restangular.all($scope.filterCallIncidents).getList().then(function (rincident) {

                            var tArray = $filter('orderBy')(rincident, 'followupdate');

                            angular.forEach(tArray, function (member, index) {
                                member.index = index + 1;
                                member.type = 'Incident';

                                member.sortingDate = member.followupdate;
                                member.date = $filter('date')(member.followupdate, 'dd-MMM-yyyy');
                                member.dateExport = $filter('date')(member.followupdate, 'dd/MMM/yyyy');
                                member.incidentdate = $filter('date')(member.incidentdate, 'dd/MM/yyyy');
                                member.dateofclosure = $filter('date')(member.dateofclosure, 'dd/MM/yyyy');
                                member.followupdate = $filter('date')(member.followupdate, 'dd/MM/yyyy');

                                if (member.dateofclosure == null) {
                                    member.dateofclosure = '';
                                }
                                if (member.lastModifiedDate == null) {
                                    member.lastModifiedDate = '';
                                }
                                if (member.date == null) {
                                    member.date = '';
                                }

                                if (member.currentstatus == 5 || member.currentstatus == 9) {
                                    member.exp_status = 'Closed';
                                } else if (member.currentstatus != 5 || member.currentstatus != 9) {
                                    member.exp_status = 'Opened';
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data = $scope.typeofincidentsdsply.filter(function (arr) {
                                        return arr.id == member.incidenttype
                                    })[0];

                                    if (data != undefined) {
                                        member.detail = data.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data = $scope.typeofincidentsdsply.filter(function (arr) {
                                        return arr.parentId == member.incidenttype
                                    })[0];

                                    if (data != undefined) {
                                        member.detail = data.name;
                                    }
                                }

                                var data1 = $scope.typeofincidentsdsplyNew.filter(function (arr) {
                                    return arr.id == member.incidenttype
                                })[0];

                                if (data1 != undefined) {
                                    member.detailEngName = data1.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data2 = $scope.currentstatusofcasesdsply.filter(function (arr) {
                                        return arr.id == member.currentstatus
                                    })[0];

                                    if (data2 != undefined) {
                                        member.currentstatusname = data2.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data2 = $scope.currentstatusofcasesdsply.filter(function (arr) {
                                        return arr.parentId == member.currentstatus
                                    })[0];

                                    if (data2 != undefined) {
                                        member.currentstatusname = data2.name;
                                    }
                                }

                                var data3 = $scope.currentstatusofcasesdsplyNew.filter(function (arr) {
                                    return arr.id == member.currentstatus
                                })[0];

                                if (data3 != undefined) {
                                    member.currentstatusEngname = data3.name;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data4 = $scope.reportincidentfollowupsdsply.filter(function (arr) {
                                        return arr.id == member.followupneeded
                                    })[0];

                                    if (data4 != undefined) {
                                        member.statusStep = data4.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data4 = $scope.reportincidentfollowupsdsply.filter(function (arr) {
                                        return arr.parentId == member.followupneeded
                                    })[0];

                                    if (data4 != undefined) {
                                        member.statusStep = data4.name;
                                    }
                                }

                                var data5 = $scope.reportincidentfollowupsdsplyNew.filter(function (arr) {
                                    return arr.id == member.followupneeded
                                })[0];

                                if (data5 != undefined) {
                                    member.statusStepEngName = data5.name;
                                }

                                var data6 = $scope.usersdisplay.filter(function (arr) {
                                    return arr.id == member.associatedHF
                                })[0];

                                if (data6 != undefined) {
                                    member.assignedTo = data6.name;
                                }

                                var data7 = $scope.countrydisplay.filter(function (arr) {
                                    return arr.id == member.countryId
                                })[0];

                                if (data7 != undefined) {
                                    member.countryname = data7.name;
                                }

                                var data8 = $scope.statedisplay.filter(function (arr) {
                                    return arr.id == member.stateId
                                })[0];

                                if (data8 != undefined) {
                                    member.statename = data8.name;
                                }

                                var data9 = $scope.districtdsply.filter(function (arr) {
                                    return arr.id == member.districtId
                                })[0];

                                if (data9 != undefined) {
                                    member.districtname = data9.name;
                                }

                                var data10 = $scope.sitedsply.filter(function (arr) {
                                    return arr.id == member.siteId
                                })[0];

                                if (data10 != undefined) {
                                    member.sitename = data10.name;
                                }

                                if (member.multiplemembers != null) {
                                    member.multiplemembersLength = member.multiplemembers.split(',').length;
                                    member.arraymultiplemembers = member.multiplemembers.split(",");
                                    member.membername = "";
                                    member.individualId = "";
                                    member.HHId = "";
                                    member.mobile = "";
                                    member.hhName = "";

                                    for (var i = 0; i < member.arraymultiplemembers.length; i++) {
                                        for (var j = 0; j < $scope.finalMembers.length; j++) {
                                            if ($scope.finalMembers[j].id + "" === member.arraymultiplemembers[i] + "") {
                                                if (member.membername === "") {
                                                    member.membername = $scope.finalMembers[j].name;
                                                    member.individualId = $scope.finalMembers[j].individualId;
                                                    member.HHId = $scope.finalMembers[j].HHId;
                                                    member.mobile = $scope.finalMembers[j].mobile;

                                                    var data11 = $scope.memsdisply.filter(function (arr) {
                                                        return arr.id == $scope.finalMembers[j].id
                                                    })[0];

                                                    if (data11 != undefined) {

                                                        var data12 = $scope.houseHoldsDisplay.filter(function (arr) {
                                                            return arr.id == data11.parentId
                                                        })[0];

                                                        if (data12 != undefined) {
                                                            member.hhName = data12.nameOfHeadInHH;
                                                        }
                                                    }

                                                } else {
                                                    member.membername = member.membername + "," + $scope.finalMembers[j].name;
                                                    member.individualId = member.individualId + "," + $scope.finalMembers[j].individualId;
                                                    member.HHId = member.HHId + "," + $scope.finalMembers[j].HHId;
                                                    member.mobile = member.mobile + "," + $scope.finalMembers[j].mobile;

                                                    var data11 = $scope.memsdisply.filter(function (arr) {
                                                        return arr.id == $scope.finalMembers[j].id
                                                    })[0];

                                                    if (data11 != undefined) {

                                                        var data12 = $scope.houseHoldsDisplay.filter(function (arr) {
                                                            return arr.id == data11.parentId
                                                        })[0];

                                                        if (data12 != undefined) {
                                                            member.hhName = member.hhName + "," + data12.nameOfHeadInHH;
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    member.membername = "";
                                    member.individualId = "";
                                    member.HHId = "";
                                    member.mobile = "";
                                    member.membersInGroupLength = 0;
                                }

                                for (key in member) {

                                    if (member[key] === true) {
                                        member[key] = 'Yes';
                                    } else if (member[key] === false) {
                                        member[key] = 'No';
                                    }
                                    // console.log(member[key]);
                                }

                                tdArray.push(member);
                                typecount++;

                                if (tArray.length == typecount) {
                                    $scope.todoArray = tdArray;

                                    angular.forEach($scope.todoArray, function (member, index) {
                                        member.index = index + 1;
                                    });
                                }

                                $scope.TotalData = [];
                                $scope.TotalData.push(member);
                            });
                        });
                    }
                }
            }
        });

        /*********************************** end ***************************************/

        $scope.confirmTodoModal = false;

        $scope.todoconfirm = {};

        $scope.confirmTodo = function () {
            $scope.disabledTodo = true;
            if ($scope.roleId == 3 || $scope.roleId == 7 || $scope.roleId == 8) {
                $scope.todoconfirm.value = '';
                $scope.confirmTodoModal = true;
            }
        };

        $scope.disabledTodo = true;
        $scope.clickAddPopUp = function () {
            $scope.disabledTodo = false;
        };

        $scope.confirmTodoPopup = function () {
            if ($scope.todoconfirm.value == 'condition') {
                $scope.confirmTodoModal = false;
                window.location = "/screentreat/create";
            } else if ($scope.todoconfirm.value == 'shgmeeting') {
                $scope.confirmTodoModal = false;
                window.location = "/shgmeeting/create";
            } else if ($scope.todoconfirm.value == 'intervention') {
                $scope.confirmTodoModal = false;
                window.location = "/intervention/create";
            } else {
                return;
            }
        };

        /**************************Export data to excel sheet ***************/

        $scope.valTodoCount = 0;

        $scope.exportData = function () {

            //  console.log($scope.exportTodoData);

            $scope.todoExportArray = [];

            $scope.valTodoCount = 0;

            if ($scope.exportTodoData.length == 0) {
                alert('No data found');
            } else {

                for (var arr = 0; arr < $scope.exportTodoData.length; arr++) {

                    $scope.todoExportArray.push({
                        'Sr No': arr + 1,
                        'COUNTRY': $scope.exportTodoData[arr].countryname,
                        'STATE': $scope.exportTodoData[arr].statename,
                        'DISTRICT': $scope.exportTodoData[arr].districtname,
                        'SITE': $scope.exportTodoData[arr].sitename,
                        'NAME': $scope.exportTodoData[arr].membername,
                        'ID': $scope.exportTodoData[arr].individualId,
                        'ASSIGNED TO': $scope.exportTodoData[arr].assignedTo,
                        'TYPE': $scope.exportTodoData[arr].type,
                        'DETAILS': $scope.exportTodoData[arr].detailEngName,
                        'STATUS / STEP': $scope.exportTodoData[arr].statusStepEngName,
                        'OPENED / CLOSED': $scope.exportTodoData[arr].exp_status,
                        'DATE': $scope.exportTodoData[arr].dateExport,
                        'HEAD OF HOUSEHOLD': $scope.exportTodoData[arr].hhName,
                        'CREATED DATE': $scope.exportTodoData[arr].createdDate,
                        'LAST MODIFIED DATE': $scope.exportTodoData[arr].lastModifiedDate,
                    });

                    $scope.valTodoCount++;

                    if ($scope.exportTodoData.length == $scope.valTodoCount) {
                        alasql('SELECT * INTO XLSX("todos.xlsx",{headers:true}) FROM ?', [$scope.todoExportArray]);
                    }
                }
            }
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            //  console.log(column);

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        $scope.todoDown = function () {
            anchorSmoothScroll.scrollTo('middle');
        };

        if ($window.sessionStorage.todoClick == 'clicked') {
            $scope.todoDown();
        }

        $scope.$on('toDo', function (event, value) {
            // console.log(value);
            anchorSmoothScroll.scrollTo(value);
        });

        $scope.quickadddataModal = false;

        if ($window.sessionStorage.quickAddClick == 'clicked') {
            $timeout(function () {
                $scope.quickadddataModal = true;
                $window.sessionStorage.quickAddClick = '';
            }, 500);

        }

        $scope.$on('quickAdd', function (event, value) {
            $scope.quickadddataModal = value;
        });


        /************************* End ************************************/

        /********************** Quick Add *********************************/

        $scope.stateupdate = {};

        Restangular.one('users', $window.sessionStorage.userId).get().then(function (usr) {
            Restangular.one('states', usr.stateId).get().then(function (ste) {

                var num = ste.sequence,
                    str = num.toString(),
                    len = str.length;

                if (len == 1) {
                    $scope.seqSize = '0000';
                } else if (len == 2) {
                    $scope.seqSize = '000'
                } else if (len == 3) {
                    $scope.seqSize = '00'
                } else if (len == 4) {
                    $scope.seqSize = '0'
                } else if (len == 5) {
                    $scope.seqSize = '';
                }

                Restangular.one('sites', usr.siteId.split(",")[0]).get().then(function (site) {
                    $scope.member.HHId = ste.code + '' + site.code + '' + $scope.seqSize + ste.sequence;
                    $scope.member.individualId = $scope.member.HHId + '-' + '1';

                    $scope.userstateId = ste.id;
                    $scope.stateupdate.sequence = ste.sequence + 1;
                });
            });
        });

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $('#phoneno').keypress(function (evt) {
            if (/^-?[0-9]\d*(\.\d+)?$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $scope.Validname = function ($event, value) {
            var arr = $scope.member.name.split(/\s+/);
            // console.log('arr.length', arr.length);
            if (arr.length == 0 || arr.length == 1) {
                setTimeout(function () {
                    alert($scope.HHLanguage.invalidName);
                }, 1000);
                document.getElementById('name').style.borderColor = "#FF0000";
                // var name = $window.document.getElementById('name');
                // name.focus();
            } else {
                document.getElementById('name').style.borderColor = "";
                $scope.duplicateHHFind(value);
            }
        };

        $scope.Validname1 = function ($event, value) {
            var arr = $scope.memberone.name.split(/\s+/);
            // console.log('arr.length', arr.length);
            if (arr.length == 0 || arr.length == 1) {
                setTimeout(function () {
                    alert($scope.HHLanguage.invalidName);
                }, 1000);
                document.getElementById('nameone').style.borderColor = "#FF0000";
                // var name = $window.document.getElementById('name');
                // name.focus();
            } else {
                document.getElementById('nameone').style.borderColor = "";
                $scope.duplicateHHFind(value);
            }
        };

        $scope.duplicateMemberHead = false;

        $scope.duplicateHHFind = function (value) {

            $scope.validatestring1 = '';

            if (value == '' || value == null) {
                return;
            } else {

                Restangular.all($scope.memberFilterCall).getList().then(function (memList) {
                    var resultObject = _.findWhere(memList, {
                        name: value
                    });
                    if (resultObject == undefined) {
                        document.getElementById('name').style.borderColor = "";
                        $scope.duplicateMemberHead = false;
                        //$scope.someMemberName = false;
                    } else {
                        $scope.toggleValidation();
                        $scope.validatestring1 = $scope.HHLanguage.existinMemberNamePleaseChange;
                        // $scope.someMemberName = true;
                        document.getElementById('name').style.borderColor = "#FF0000";
                        $scope.duplicateMemberHead = true;
                    }
                });
            }
        };

        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();

        $scope.picker = {};

        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };

        $scope.openfamilydob = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#familydobopen' + index).focus();
            });
            $scope.picker.familydobopened = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];
        //Datepicker settings end

        $scope.dobcount = 0;

        $scope.$watch('member.dob', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                $scope.dobcount++;
                $scope.today = new Date();
                $scope.birthyear = newValue;
                var ynew = $scope.today.getFullYear();
                var mnew = $scope.today.getMonth();
                var dnew = $scope.today.getDate();
                var yold = $scope.birthyear.getFullYear();
                var mold = $scope.birthyear.getMonth();
                var dold = $scope.birthyear.getDate();
                var diff = ynew - yold;
                if (mold > mnew) diff--;
                else {
                    if (mold == mnew) {
                        if (dold > dnew) diff--;
                    }
                }
                $scope.member.age = diff;
            }
        });

        $scope.$watch('member.age', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue >= 0 && newValue <= 99) {
                    document.getElementById('age').style.borderColor = "";
                    if ($scope.dobcount == 0) {
                        var todaydate = new Date();
                        var newdate = new Date(todaydate);
                        newdate.setFullYear(newdate.getFullYear() - newValue);
                        var nd = new Date(newdate);
                        $scope.member.dob = nd;
                    } else {
                        $scope.dobcount = 0;
                    }
                } else {
                    //alert('Invalid Age');
                    $scope.AlertMessage = $scope.invalid + ' ' + $scope.printage;
                    $scope.member.dob = null;
                    $scope.member.age = null;
                }
            }
        });

        $scope.ValidMobile = function ($event) {
            if ($scope.member1.mobile != '' || $scope.member1.mobile != null) {
                if ($scope.member1.mobile.length != 10) {
                    $scope.member1.mobile = '';
                    alert('Enter 10 digits');
                }
            }
        };


        $scope.$watch('member.migrant', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if (newValue == 1) {
                // $scope.MigrantFunc = 1;
                $scope.migrantdataModal = true;
            }
        });

        $scope.DisableMigrantBtn = true;

        $scope.newValue = function (MigrantFunc) {
            if (MigrantFunc == 'yes') {
                $scope.MigrantFunc = 1;
                $scope.DisableMigrantBtn = false;
            } else if (MigrantFunc == 'no') {
                $scope.MigrantFunc = 2;
                $scope.DisableMigrantBtn = false;
            }
        };

        $scope.ConfirmMigrant = function () {
            $scope.migrantdataModal = false;
            $scope.member.migrant = $scope.MigrantFunc;
        };

        $scope.CLOSEBUTTON = function () {
            $scope.migrantdataModal = false;
        };

        $scope.viewdataModal = false;

        $scope.validatestring = '';

        $scope.member1 = {
            mobile: '9999999999'
        };
        //        $scope.member.latitude = '28.613939';
        //        $scope.member.longitude = '77.209021';
        $scope.member.latitude = '00.00000';
        $scope.member.longitude = '00.00000';

        $scope.member.address = 'AddressEmpty';


        $scope.validateHH = function () {

            $scope.member.quickaddFlag = true;

            $scope.household.nameOfHeadInHH = $scope.member.name;
            $scope.household.HHId = $scope.member.HHId;
            $scope.household.noOfMembers = $scope.memberlists.length + 1;
            $scope.household.gender = $scope.member.gender;
            $scope.household.associatedHF = $scope.member.associatedHF;
            $scope.household.ssgThisHHBelongs = "";
            $scope.household.quickaddFlag = true;

            var re = /\S+@\S+\.\S+/;

            document.getElementById('name').style.border = "";
            document.getElementById('age').style.border = "";
            // document.getElementById('address').style.border = "";
            // document.getElementById('phoneno').style.borderColor = "";
            // document.getElementById('latitude').style.borderColor = "";
            // document.getElementById('longitude').style.borderColor = "";

            if ($scope.member.name == '' || $scope.member.name == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.invalidName;
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.duplicateMemberHead == true) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.existinMemberNamePleaseChange;
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.member.dob == '' || $scope.member.dob == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectDOB;
                // document.getElementById('date').style.borderColor = "#FF0000";

            } else if ($scope.member.age === '' || $scope.member.age === null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterAge;
                document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.member.gender == '' || $scope.member.gender == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectGender;
                // document.getElementById('age').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.viewdataModal = true;
            }
        };

        $scope.memberone = {
            aadharNumber: '',
            mobile: ''
        };

        Restangular.all('genders?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (gendone) {
            $scope.gendersone = gendone;

            if ($scope.UserLanguage == 1) {
                $scope.memberone.gender = gendone[0].id;
                $scope.memgender = gendone[0].id;
            } else {
                $scope.memberone.gender = gendone[0].parentId;
                $scope.memgender = gendone[0].parentId;
            }
        });

        Restangular.all('relationWithHHs?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rltion) {
            $scope.relationWithHHs = rltion;
        });

        Restangular.all('literacyLevels?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (litlevel) {
            $scope.literacyLevels = litlevel;
        });

        Restangular.all('nameOfSchools?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (nOfschls) {
            $scope.nameOfSchools = nOfschls;
        });

        $scope.ValidAadhar = function ($event) {
            // console.log('$scope.member.aadharNumber', $scope.member.aadharNumber);
            Restangular.one('members/findOne?filter[where][aadharNumber]=' + $scope.member.aadharNumber).get().then(function (membr) {
                alert($scope.HHLanguage.existingAadhar);
                $scope.member.aadharNumber = '';
                document.getElementById('aadharno').style.borderColor = "#FF0000";
            }, function (response) {
                // console.log("Error with status code", response.status);
                document.getElementById('aadharno').style.borderColor = "";
            });
        };

        $scope.ValidAadharOne = function ($event) {
            // console.log('$scope.member.aadharNumber', $scope.member.aadharNumber);
            Restangular.one('members/findOne?filter[where][aadharNumber]=' + $scope.memberone.aadharNumber).get().then(function (membrone) {
                alert($scope.HHLanguage.existingAadhar);
                $scope.memberone.aadharNumber = '';
                document.getElementById('aadhar').style.borderColor = "#FF0000";
            }, function (response) {
                // console.log("Error with status code", response.status);
                document.getElementById('aadhar').style.borderColor = "";
            });
        };


        $scope.watLikeTodoModel = false;
        $scope.addClicked = false;

        $scope.memberlists = [];

        $scope.Idcount = 1;

        $scope.AddMember = function () {
            $scope.memberone = {
                gender: $scope.memgender,
                aadharNumber: '',
                mobile: '',
                relation: 83,
                literacyLevel: 110,
                nameOfSchool: 34
            };

            $scope.HideAdd = false;
            $scope.HideUpdate = true;
            //  $scope.addClicked = false;
            $scope.beneficiarydataModal = true;
            $scope.hideFactory = true;
            $scope.hideOccupation = true;
            $scope.disableSHG = true;
            $scope.dupCheckFlag = true;

            $scope.individualId = $scope.member.individualId.split('-');
            $scope.indivdId = parseInt($scope.individualId[1]) + $scope.Idcount;
            $scope.memberone.individualId = $scope.member.HHId + '-' + $scope.indivdId;
        };

        $scope.$watch('memberone.age', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {

                if (newValue >= 0 && newValue <= 99) {
                    document.getElementById('ageone').style.borderColor = "";
                    if ($scope.dobcount == 0) {
                        var todaydate = new Date();
                        var newdate = new Date(todaydate);
                        newdate.setFullYear(newdate.getFullYear() - newValue);
                        var nd = new Date(newdate);
                        $scope.memberone.dob = nd;
                    } else {
                        $scope.dobcount = 0;
                    }
                } else {
                    //alert('Invalid Age');
                    $scope.AlertMessage = $scope.invalid + ' ' + $scope.printage;
                    $scope.memberone.dob = null;
                    $scope.memberone.age = null;
                }

                if (newValue >= 18) {
                    $scope.memberone.nameOfSchool = '';
                } else {
                    $scope.memberone.nameOfSchool = 34;
                }
            }
        });

        $scope.$watch('memberone.dob', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                $scope.dobcountone++;
                $scope.today = new Date();
                $scope.birthyear = newValue;
                var ynew = $scope.today.getFullYear();
                var mnew = $scope.today.getMonth();
                var dnew = $scope.today.getDate();
                var yold = $scope.birthyear.getFullYear();
                var mold = $scope.birthyear.getMonth();
                var dold = $scope.birthyear.getDate();
                var diff = ynew - yold;
                if (mold > mnew) diff--;
                else {
                    if (mold == mnew) {
                        if (dold > dnew) diff--;
                    }
                }
                $scope.memberone.age = diff;

                if ($scope.memberone.age <= 18) {
                    $scope.memberone.nameOfSchool = 34;
                } else {
                    $scope.memberone.nameOfSchool = '';
                }
            }
        });

        $scope.SaveMember = function () {

            $scope.validatestring = '';

            document.getElementById('nameone').style.border = "";
            document.getElementById('ageone').style.border = "";
            document.getElementById('phonenoone').style.border = "";

            if ($scope.memberone.name == '' || $scope.memberone.name == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterNameOfMember;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.duplicateMember == true) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.existinMemberNamePleaseChange;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.memberone.gender == '' || $scope.memberone.gender == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectGender;
                // document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.memberone.dob == '' || $scope.memberone.dob == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectDOB;

            } else if ($scope.memberone.age === '' || $scope.memberone.age === null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterAge;
                document.getElementById('ageone').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                // $scope.addClicked = true;

                $scope.memberlists.push({
                    name: $scope.memberone.name,
                    associatedHF: $scope.member.associatedHF,
                    relation: $scope.memberone.relation,
                    dob: $scope.memberone.dob,
                    age: $scope.memberone.age,
                    literacyLevel: $scope.memberone.literacyLevel,
                    nameOfSchool: $scope.memberone.nameOfSchool,
                    occupation: null,
                    nameOfFactory: null,
                    aadharNumber: $scope.memberone.aadharNumber,
                    esiNumber: $scope.memberone.esiNumber,
                    shgId: '',
                    gender: $scope.memberone.gender,
                    individualId: $scope.memberone.individualId,
                    mobile: $scope.memberone.mobile,
                    address: null,
                    caste: null,
                    religion: null,
                    migrant: null,
                    bplStatus: null,
                    latitude: null,
                    longitude: null,
                    email: null,
                    healthyDays: 0,
                    deleteFlag: false,
                    isHeadOfFamily: false,
                    quickaddFlag: true,
                    createdDate: new Date(),
                    createdBy: $window.sessionStorage.userId,
                    createdByRole: $window.sessionStorage.roleId,
                    lastModifiedDate: new Date(),
                    lastModifiedBy: $window.sessionStorage.userId,
                    lastModifiedByRole: $window.sessionStorage.roleId,
                    countryId: $window.sessionStorage.countryId,
                    stateId: $window.sessionStorage.stateId,
                    districtId: $window.sessionStorage.districtId,
                    siteId: $window.sessionStorage.siteId.split(",")[0]
                });

                $scope.Idcount++;

                angular.forEach($scope.memberlists, function (member, index) {
                    member.index = index;

                    if ($window.sessionStorage.language == 1) {
                        var data = $scope.genders.filter(function (arr) {
                            return arr.id == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    } else {
                        var data = $scope.genders.filter(function (arr) {
                            return arr.parentId == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    }

                    $scope.beneficiarydataModal = false;
                    $scope.memberone = {};
                    $scope.validatestring = '';
                });
            }
        };

        var uniqueNames = [];

        $scope.EditMember = function (index) {
            $scope.memberone = {};
            $scope.HideAdd = true;
            $scope.HideUpdate = false;
            $scope.updateClicked = false;
            var uniqueNames = [];
            $scope.beneficiarydataModal = true;
            $scope.memberone = $scope.memberlists[index];
            $scope.tempMember = Restangular.copy($scope.memberlists);
            $scope.oldGroupid = $scope.memberone.shgId;
            $scope.oldMemberid = $scope.memberone.id;
            $scope.dupCheckFlag = false;
        };

        $scope.CLOSEMEMBUTTON = function () {
            $scope.memberlists = $scope.tempMember;
            $scope.beneficiarydataModal = false;
            $scope.memberone = {};
        };

        $scope.CloseAddFamily = function () {
            $scope.beneficiarydataModal = false;
            $scope.memberone = {};
        };

        $scope.RemoveMember = function (index) {
            $scope.memberlists.splice(index, 1);
            $scope.Idcount--;
        };

        $scope.UpdateMember = function () {

            $scope.validatestring = '';

            document.getElementById('nameone').style.border = "";
            document.getElementById('ageone').style.border = "";

            if ($scope.memberone.name == '' || $scope.memberone.name == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterNameOfMember;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.duplicateMember == true) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.existinMemberNamePleaseChange;
                document.getElementById('nameone').style.borderColor = "#FF0000";

            } else if ($scope.memberone.gender == '' || $scope.memberone.gender == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectGender;
                // document.getElementById('age').style.borderColor = "#FF0000";

            } else if ($scope.memberone.dob == '' || $scope.memberone.dob == null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.selectDOB;

            } else if ($scope.memberone.age === '' || $scope.memberone.age === null) {
                $scope.validatestring = $scope.validatestring + $scope.HHLanguage.enterAge;
                document.getElementById('ageone').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.updateClicked = true;
                $scope.memberlists[$scope.memberone.index] = $scope.memberone;
                $scope.beneficiarydataModal = false;

                angular.forEach($scope.memberlists, function (member, index) {
                    member.index = index;

                    if ($window.sessionStorage.language == 1) {
                        var data = $scope.genders.filter(function (arr) {
                            return arr.id == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    } else {
                        var data = $scope.genders.filter(function (arr) {
                            return arr.parentId == member.gender
                        })[0];

                        if (data != undefined) {
                            member.gendername = data.name;
                        }
                    }
                });

                $scope.memberone = {};
                $scope.validatestring = '';
            }
        };

        $scope.saveHouseHold = function () {

            $scope.quickadddataModal = false;
            $scope.addClicked = true;

            Restangular.all('households').post($scope.household).then(function (Response) {
                $scope.member.parentId = Response.id;
                // console.log(Response);

                Restangular.one('states', $scope.userstateId).customPUT($scope.stateupdate).then(function (res) {

                    if ($scope.member.aadharNumber == '' || $scope.member.aadharNumber == null) {
                        console.log('blank aadhar');
                    } else {
                        var aadharNo = $scope.member.aadharNumber.toString();
                        var lastfour = aadharNo.substr(aadharNo.length - 4);
                        var newAadhar = "XXXXXXXX";
                        $scope.member.aadharNumber = newAadhar + '' + lastfour;
                    }

                    Restangular.all('members').post($scope.member).then(function (Resp) {
                        $window.sessionStorage.MemberId = Resp.id;
                        $scope.saveMemberList(Response.id);
                        // console.log(Resp);
                    });
                });
            });
        };

        var savememCount = 0;

        $scope.saveMemberList = function (hhid) {

            if ($scope.memberlists.length > savememCount) {

                //console.log($scope.memberlists);

                $scope.memberlists[savememCount].parentId = hhid;

                if ($scope.memberlists[savememCount].aadharNumber == '' || $scope.memberlists[savememCount].aadharNumber == null) {
                    console.log('blank aadhar');
                } else {
                    var aadharNo = $scope.memberlists[savememCount].aadharNumber.toString();
                    var lastfour = aadharNo.substr(aadharNo.length - 4);
                    var newAadhar = "XXXXXXXX";
                    $scope.memberlists[savememCount].aadharNumber = newAadhar + '' + lastfour;
                }

                Restangular.all('members').post($scope.memberlists[savememCount]).then(function (Resp1) {
                    // console.log(Resp1);
                    savememCount++;
                    $scope.saveMemberList(hhid);
                });

            } else {
                $scope.member = {};
                $scope.viewdataModal = false;
                $scope.watLikeTodoModel = true;
                $scope.QucikAddButtonDisable = true;
            }
        };

        /*****************QuickaddPopupcancle**************/

        $scope.cancleQucikAdd = function () {
           // $scope.member = {};
            $scope.memberone = {};
            $scope.memberlists = [];
            $scope.member.name = '';
            $scope.member.dob = '';
            $scope.member.age = '';
            $scope.quickadddataModal = false;
        };

        /*****************************last model****************************/
        $scope.confirmQuickRedirect = function () {

            if ($scope.confirm.value === 'condition') {
                window.location = "/screentreat/create";
                $scope.watLikeTodoModel = false;
            } else if ($scope.confirm.value === 'patientrecord') {
                window.location = "/patientrecord/create";
                $scope.watLikeTodoModel = false;
            } else {

                $scope.watLikeTodoModel = false;
                $window.sessionStorage.MemberId = '';
                $window.sessionStorage.ConditionMemberId = '';
                window.location = "/";
            }
        };

        $scope.QucikAddButtonDisable = true;

        $scope.clickQucikAddButton = function () {
            $scope.QucikAddButtonDisable = false;
        }
        /******************************google map****************************/

        $scope.mapdataModal = false;

        $scope.LocateMe = function () {
            $scope.mapdataModal = true;

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 2,

                // center: new google.maps.LatLng(12.9538477, 77.3507369),
                //center: new google.maps.LatLng(28.613939, 77.209021),
                center: new google.maps.LatLng(21.7679, 78.8718),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            var marker, i;

            marker = new google.maps.Marker({
                // position: new google.maps.LatLng(12.9538477, 77.3507369),
                position: new google.maps.LatLng(21.7679, 78.8718),
                map: map,
                html: ''
            });

            $scope.toggleMapModal();
        };

        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            ///////////////////////////////////////////////////////MAP//////////////////////////

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                //  console.log(latLng);
                $scope.latLng1 = latLng.lat();
                $scope.latLng2 = latLng.lng();

                //  console.log('$scope.updatepromotion', $scope.updatepromotion);

                document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
            }
            var map;

            function initialize() {

                $scope.latitude = 21.7679;
                $scope.longitude = 78.8718;
                navigator.geolocation.getCurrentPosition(function (location) {
                    //                    console.log(loca21.7679, 78.8718tion.coords.latitude);
                    //                    console.log(location.coords.longitude);
                    //                    console.log(location.coords.accuracy);
                    ///  $scope.latitude = location.coords.latitude; // current location
                    ///        $scope.longitude = location.coords.longitude; //current location
                    //                });

                    // console.log('$scope.address', $scope.address);

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 2,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });
                });


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(5);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                //  console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };


        /*************Map ok Button ******/
        $scope.OkBUTTON = function () {
            $scope.member.latitude = $scope.latLng1;
            $scope.member.longitude = $scope.latLng2;
            $scope.mapdataModal = false;
        };
        /**************************** End ******************************************/
    })

    .factory('dataShareService', function () {
        return {
            count: ""
        };
    })

    .service('anchorSmoothScroll', function () {

        this.scrollTo = function (eID) {

            // This scrolling function 
            // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

            var startY = currentYPosition();
            var stopY = elmYPosition(eID);
            var distance = stopY > startY ? stopY - startY : startY - stopY;
            if (distance < 100) {
                scrollTo(0, stopY);
                return;
            }
            var speed = Math.round(distance / 100);
            if (speed >= 20) speed = 20;
            var step = Math.round(distance / 25);
            var leapY = stopY > startY ? startY + step : startY - step;
            var timer = 0;
            if (stopY > startY) {
                for (var i = startY; i < stopY; i += step) {
                    setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                    leapY += step;
                    if (leapY > stopY) leapY = stopY;
                    timer++;
                }
                return;
            }
            for (var i = startY; i > stopY; i -= step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY -= step;
                if (leapY < stopY) leapY = stopY;
                timer++;
            }

            function currentYPosition() {
                // Firefox, Chrome, Opera, Safari
                if (self.pageYOffset) return self.pageYOffset;
                // Internet Explorer 6 - standards mode
                if (document.documentElement && document.documentElement.scrollTop)
                    return document.documentElement.scrollTop;
                // Internet Explorer 6, 7 and 8
                if (document.body.scrollTop) return document.body.scrollTop;
                return 0;
            }

            function elmYPosition(eID) {
                var elm = document.getElementById(eID);
                var y = elm.offsetTop;
                var node = elm;
                while (node.offsetParent && node.offsetParent != document.body) {
                    node = node.offsetParent;
                    y += node.offsetTop;
                }
                return y;
            }

        };

    })

    .directive('quickaddmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static" data-keyboard="false">' + '<div class="modal-dialog modal-lg">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    })

    .directive('quickviewmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static" data-keyboard="false">' + '<div class="modal-dialog modal-lg">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    })
    .directive('quickfinalmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static" data-keyboard="false">' + '<div class="modal-dialog modal-md">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    })
    .directive('quickfinalviewmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static" data-keyboard="false">' + '<div class="modal-dialog modal-md">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    })

    .directive('loadingtodo', function () {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="loading" style="color:black;"><img src="images/piclinks/loaderpink_blue.gif" width="20" height="20"  class="loadingwidth" /> <br/><br/>LOADING. . .</div>',
            link: function (scope, element, attrs) {
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).show();
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).hide();
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
            }
        }
    });
