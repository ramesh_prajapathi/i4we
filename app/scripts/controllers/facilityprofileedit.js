'use strict';
angular.module('secondarySalesApp').controller('FacilityProfileEditCtrl', function ($scope, Restangular, $routeParams, $window, $filter, $timeout, $location, $modal, $route) {
    /*if ($window.sessionStorage.roleId != 1) {
    	window.location = "/";
    }*/
    $scope.heading = 'Facility Update';
    $scope.Created = true;
    $scope.Updated = false;
    $scope.fwcodedisable = true;
    $scope.fwnamedisable = true;
    $scope.fwstatedisable = true;
    $scope.fwdistrictdisable = true;
    $scope.tiRatingDisable = true;
    $scope.tiNonRatingDisable = true;
    $scope.tideletebuttonhide = true;
    $scope.nooftis_disable = true;
    $scope.noofnontis_disable = true;
    $scope.employee = {
        //lastmodifiedtime: new Date(),

        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        deleteflag: false
    };
    $scope.typologies = Restangular.all('typologies?filter[where][deleteflag]=false').getList().$object;
    $scope.areaofoperations = Restangular.all('areaofoperations?filter[where][deleteflag]=false').getList().$object;
    $scope.employeestatuses = Restangular.all('employeestatuses?filter[where][deleteflag]=false').getList().$object;
    $scope.facilitytypes = Restangular.all('facilitytypes?filter[where][deleteflag]=false').getList().$object;
    $scope.tiratings = Restangular.all('tiratings').getList().$object;
    $scope.states = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comemberResponse) {
        $scope.getFacilityId = comemberResponse.facility;
        Restangular.one('employees', $scope.getFacilityId).get().then(function (employee) {
            $scope.original = employee;
            //console.log('employee', employee.id);
            $scope.salearea = Restangular.all('sales-areas?filter[where][zoneId]=' + $scope.original.stateId + '&filter[where][deleteflag]=false').getList().then(function (salearea) {
                $scope.districts = salearea;
                $scope.employee = Restangular.copy($scope.original);
                //$scope.oldValueTIRating = parseInt(employee.nooftis);
                //$scope.empnonto = $scope.employee.noofnonti;
                if (employee.nooftis == null || employee.nooftis == '' || employee.nooftis == undefined) {
                    $scope.oldValueTIRating = 0;
                    $scope.nooftis_disable = false;
                } else {
                    $scope.nooftis_disable = true;
                    $scope.oldValueTIRating = parseInt(employee.nooftis);
                }
                if (employee.noofnonti == null || employee.noofnonti == '' || employee.noofnonti == undefined) {
                    $scope.oldValueNonTIRating = 0;
                    $scope.noofnontis_disable = false;
                } else {
                    $scope.oldValueNonTIRating = parseInt(employee.noofnonti);
                }
                if (employee.typology != null) {
                    $scope.employee.typolog = employee.typology.split(",");
                } else {
                    $scope.employee.typolog = [];
                }
                Restangular.one('tifacilities?filter[where][facilityid]=' + $scope.getFacilityId + '&filter[where][deleteflag]=false').get().then(function (tt) {
                    $scope.tifacilities = tt;
                    //console.log('$scope.tifacilities', $scope.tifacilities);
                    //$scope.currentTI = tt.length;
                    if (tt.length == 0) {
                        $scope.nooftis_disable = false;
                    }
                });
                Restangular.one('nontifacilities?filter[where][facilityid]=' + $scope.getFacilityId + '&filter[where][deleteflag]=false').get().then(function (ttt) {
                    $scope.nontifacilities = ttt;
                    $scope.currentNONTI = ttt.length;
                    if (ttt.length == 0) {
                        $scope.noofnontis_disable = false;
                    }
                });
            });
        });
    });
    //console.log('$scope.getFacilityId', $scope.getFacilityId);
    //console.log('$scope.employee.noofnonti',$scope.employee.noofnonti);		
    $scope.tifacilitiesAdd = false;
    $scope.Rating = function (no) {

        $scope.tifacilitiesAdd = true;
        $scope.tifacilities = [];
        for (var i = 0; i < no; i++) {
            $scope.tifacilities.push({
                rating: '',
                facilityid: '',
                state: '',
                district: '',
                deleteflag: false
            });
        }
        $scope.CurrentRating = no;
        //console.log('Rating', $scope.tifacilities);
    }
    $scope.nontifacilitiesAdd = false;
    $scope.currentChangenonti = 0;
    $scope.ProjectImp = function (noo) {
        $scope.nontifacilitiesAdd = true;
        $scope.nontifacilities = [];
        for (var i = 0; i < noo; i++) {
            $scope.nontifacilities.push({
                rating: '',
                facilityid: '',
                state: '',
                district: '',
                deleteflag: false
            });
        }
    }

    $scope.removeTIRating = function (index) {
        $scope.tifacilities.splice(index, 1);
        $scope.employee.nooftis--;
    };
    $scope.addTIRatingCount = 0;
    $scope.addTIRatingSave = false;
    $scope.addTIRating = function () {
        $scope.addTIRatingCount++;
        $scope.tifacilitiesAdd = true;
        $scope.addTIRatingSave = true;
        $scope.tifacilities.push({
            rating: '',
            facilityid: '',
            state: '',
            district: '',
            deleteflag: false
        });
        $scope.employee.nooftis++;
        //console.log('$scope.tifacilities', $scope.tifacilities);
        //        for (var i = 0; i < $scope.tifacilities.length; i++) {
        //            console.log('index' + i, $scope.tifacilities[i])
        //        }
    };
    $scope.addNONTIRating = function () {
        $scope.addNONTIRatingCount++;
        $scope.nontifacilitiesAdd = true;
        $scope.addNONTIRatingSave = true;
        $scope.nontifacilities.push({
            rating: '',
            facilityid: '',
            state: '',
            district: '',
            deleteflag: false
        });
        $scope.employee.noofnonti++;
    }
    $scope.Update = function () {
        //console.log('$scope.empnonto', $scope.empnonto);
        //console.log('$scope.getFacilityId',$scope.getFacilityId);
        //console.log('$scope.oldValueNonTIRating', parseInt($scope.oldValueNonTIRating));
        //console.log('$scope.nontifacilitiesAdd', $scope.nontifacilitiesAdd);
        //console.log('$scope.employee.nooftis',$scope.employee.nooftis);
        //console.log('$scope.totalnooftis',$scope.totalnoofnontis);
        //$scope.totalnooftis = $scope.oldValueTIRating + parseInt($scope.employee.nooftis)
        //$scope.totalnoofnontis = $scope.oldValueNonTIRating + parseInt($scope.employee.noofnonti)
        /* if ($scope.tifacilitiesAdd == true) {
             $scope.employee.nooftis = $scope.oldValueTIRating + parseInt($scope.employee.nooftis)
         }
         if ($scope.nontifacilitiesAdd == true) {
             $scope.employee.noofnonti = $scope.oldValueNonTIRating + parseInt($scope.employee.noofnonti)
         }*/
        //console.log('$scope.employee.noofnonti', $scope.employee.noofnonti);
        $scope.TICounter = 0;
        $scope.NonTICounter = 0;
        if ($scope.employee.salesCode == '' || $scope.employee.salesCode == null) {
            $scope.employee.salesCode = null;
            $scope.validatestring = $scope.validatestring + $scope.please + ' ' + $scope.enter + ' ' + $scope.facilitycode;
            //document.getElementById('salesCode').style.border = "1px solid #ff0000";
        } else if ($scope.employee.firstName == '' || $scope.employee.firstName == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterfirstname;
            $scope.TabName = $scope.BASIC;
        } else if ($scope.employee.facilitytype == '' || $scope.employee.facilitytype == null) {
            $scope.validatestring = $scope.validatestring + $scope.selfacilitytype;
            $scope.TabName = $scope.NON_STATUTORY;
        } else if ($scope.employee.areaofoperation == '' || $scope.employee.areaofoperation == null) {
            $scope.validatestring = $scope.validatestring + $scope.selareaofopt;
            $scope.TabName = $scope.NON_STATUTORY;
        } else if ($scope.employee.nooftowncover == '' || $scope.employee.nooftowncover == null) {
            $scope.validatestring = $scope.validatestring + $scope.enternooftowncov;
            $scope.TabName = $scope.NON_STATUTORY;
        } else if ($scope.employee.noofmandal == '' || $scope.employee.noofmandal == null) {
            $scope.validatestring = $scope.validatestring + $scope.entermandal;
            $scope.TabName = $scope.NON_STATUTORY;
        } else if ($scope.employee.noofincome == '' || $scope.employee.noofincome == null) {
            $scope.validatestring = $scope.validatestring + $scope.enternoofincome;
            $scope.TabName = $scope.NON_STATUTORY;
        } else if ($scope.employee.implementsti === 'no' && $scope.employee.nooftis == null) {
            $scope.validatestring = $scope.validatestring + $scope.enternooftis;
            $scope.TabName = $scope.NON_STATUTORY;
        } else if ($scope.employee.noofnonti < 0) {
            $scope.validatestring = $scope.validatestring + $scope.enternoofnontis;
            $scope.TabName = $scope.NON_STATUTORY;
        } else if ($scope.employee.registrationno == '' || $scope.employee.registrationno == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterregisterno;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.registrationdate == '' || $scope.employee.registrationdate == null) {
            $scope.validatestring = $scope.validatestring + $scope.selregrenewdate;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.pancard == '' || $scope.employee.pancard == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterpancard;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.astatus == '' || $scope.employee.astatus == null) {
            $scope.validatestring = $scope.validatestring + $scope.selastatus;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.astatus == 4 && $scope.employee.aregistrationno == '' || $scope.employee.aregistrationno == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterregisterno;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.gstatus == '' || $scope.employee.gstatus == null) {
            $scope.validatestring = $scope.validatestring + $scope.selgstatus;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.gstatus == 4 && $scope.employee.gregistrationno == '' || $scope.employee.gregistrationno == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterregisterno;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.frcastatus == '' || $scope.employee.frcastatus == null) {
            $scope.validatestring = $scope.validatestring + $scope.selfcrastatus;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.frcastatus == 4 && $scope.employee.frcaregistrationno == '' || $scope.employee.frcaregistrationno == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterregisterno;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.dateoflastabgm == '' || $scope.employee.dateoflastabgm == null) {
            $scope.validatestring = $scope.validatestring + $scope.seldateofagbm;
            $scope.TabName = $scope.STATUTORY;
            /*} else if ($scope.employee.registrationrenewaldate == '' || $scope.employee.registrationrenewaldate == null) {
					$scope.validatestring = $scope.validatestring + 'Select Registration Renewal Date';
					$scope.TabName = 'STATUTORY';
                    */
        } else if ($scope.employee.externalaudit == '' || $scope.employee.externalaudit == null) {
            $scope.validatestring = $scope.validatestring + $scope.selexternalaudit;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.auditreport == '' || $scope.employee.auditreport == null) {
            $scope.validatestring = $scope.validatestring + $scope.selauditreport;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.actionplan == '' || $scope.employee.actionplan == null) {
            $scope.validatestring = $scope.validatestring + $scope.selactionplan;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.electiondate == '' || $scope.employee.electiondate == null) {
            $scope.validatestring = $scope.validatestring + $scope.seldateofelection;
            $scope.TabName = $scope.STATUTORY;
        } else if ($scope.employee.electionbody == '' || $scope.employee.electionbody == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterelectionbody;
            $scope.TabName = $scope.STATUTORY;
        }
        if ($scope.employee.typolog != '' || $scope.employee.typolog != null || $scope.employee.typolog != undefined)
            for (var i = 0; i < $scope.employee.typolog.length; i++) {
                if (i == 0) {
                    $scope.employee.typology = $scope.employee.typolog[i];
                } else {
                    $scope.employee.typology = $scope.employee.typology + ',' + $scope.employee.typolog[i];
                }
            }
        else {
            $scope.employee.typology = null;
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            //$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.employee.lastmodifiedtime = new Date();
            Restangular.one('employees', $scope.getFacilityId).customPUT($scope.employee).then(function (response) {
                //$scope.CallNoodTI(response.id, response.noofnonti);
                //console.log('$scope.employee', response);
                // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.openOneAlert();
                if ($scope.tifacilities.length > 0) {
                    $scope.TIfacilityToCheck = $scope.tifacilities;
                    $scope.TISaveUpdate();
                }
                if (response.implementsti === 'no') {
                    $scope.deleteTIFacility();
                }
                if ($scope.nontifacilities.length > 0) {
                    $scope.NonTIfacilityToCheck = $scope.nontifacilities;
                    $scope.NonTISaveUpdate();
                }
                //                else {
                //                  window.location = '/';
                //                }

                //console.log('$scope.TICounter', $scope.TICounter);
                //console.log('$scope.TIfacilityToCheck.length',$scope.TIfacilityToCheck.length);
                //console.log('$scope.NonTIfacilityToCheck.length',$scope.NonTIfacilityToCheck.length);
                //console.log('$scope.nontifacilities.length', $scope.nontifacilities.length);
                //console.log('$scope.NonTIfacilityToCheck.length', $scope.NonTIfacilityToCheck.length);
                //                if ($scope.NonTICounter == $scope.NonTIfacilityToCheck.length || $scope.TICounter == $scope.TIfacilityToCheck.length || $scope.NonTIfacilityToCheck.length == undefined || $scope.NonTIfacilityToCheck.length == 0) {
                //                    window.location = '/';
                //                }
                $timeout(function () {
                    $scope.callAtTimeout();
                }, 3000);
                $scope.callAtTimeout = function () {
                    //console.log("$scope.callAtTimeout - Timeout occurred");
                    window.location = '/';
                }
                //$timeout(callAtTimeout, 3000);
//                if ($scope.TICounter == $scope.TIfacilityToCheck.length || $scope.TIfacilityToCheck.length == undefined) {
//                    //window.location = '/';
//                }

                //$location.path('/');
            }, function (response) {
                //alert(response.data.error.detail);
                if (response.data.error.constraint == "unique_salescode") {
                    $scope.validatestring1 = 'Facility Code' + ' ' + $scope.employee.salesCode + ' ' + 'Already Exists';
                } else {
                    $scope.validatestring1 = response.data.error.detail;
                }
                console.error(response);
            });
        }
    };
    $scope.openOneAlert = function () {
        console.log('Me Calling')
        $scope.modalOneAlert = $modal.open({
            animation: true,
            templateUrl: 'template/AlertModal.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            windowClass: 'modal-sucess '
        });
    };

    $scope.employee = {};
    /*****************************************************************/
    /*$scope.Update = function () {
        $scope.TICounter = 0;
        if ($scope.tifacilities.length > 0) {
            $scope.TIfacilityToCheck = $scope.tifacilities;
            $scope.TISaveUpdate();
        }
        //$scope.SaveUpdateTI();
    }*/
    $scope.TISaveUpdate = function () {
        console.log('$scope.tifacilities', $scope.tifacilities);
        console.log('$scope.TIfacilityToCheck', $scope.TIfacilityToCheck);
        console.log('$scope.TIfacilityToCheck[$scope.TICounter].id)', $scope.TIfacilityToCheck[$scope.TICounter].id);
        if ($scope.TIfacilityToCheck[$scope.TICounter].id == '' || $scope.TIfacilityToCheck[$scope.TICounter].id == undefined) {
            $scope.item = {
                facilityid: $scope.getFacilityId,
                rating: $scope.TIfacilityToCheck[$scope.TICounter].rating,
                deleteflag: false,
                lastmodifiedtime: new Date(),
                state: $window.sessionStorage.zoneId,
                district: $window.sessionStorage.salesAreaId,
                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                lastmodifiedbyrole: $window.sessionStorage.roleId
            }
            Restangular.all('tifacilities').post($scope.item).then(function (resp) {
                //console.log('tifacilities save', resp);
            });
            $scope.TICounter++;
        } else {
            $scope.item = {
                rating: $scope.TIfacilityToCheck[$scope.TICounter].rating
            };
            Restangular.one('tifacilities/' + $scope.TIfacilityToCheck[$scope.TICounter].id).customPUT($scope.item).then(function (response) {
                //window.location = '/';
                //console.log('tifacilities Up', response);
            });
            $scope.TICounter++;
        }
        if ($scope.TICounter < $scope.TIfacilityToCheck.length) {
            $scope.TISaveUpdate();
        }
    };
    $scope.NonTISaveUpdate = function () {
        //console.log('$scope.tifacilities', $scope.tifacilities);
        //console.log('$scope.TIfacilityToCheck', $scope.TIfacilityToCheck);
        //console.log('$scope.TIfacilityToCheck[$scope.TICounter].id)', $scope.TIfacilityToCheck[$scope.TICounter].id);
        if ($scope.NonTIfacilityToCheck[$scope.NonTICounter].id == '' || $scope.NonTIfacilityToCheck[$scope.NonTICounter].id == undefined) {
            $scope.item = {
                facilityid: $scope.getFacilityId,
                rating: $scope.NonTIfacilityToCheck[$scope.NonTICounter].rating,
                deleteflag: false,
                lastmodifiedtime: new Date(),
                state: $window.sessionStorage.zoneId,
                district: $window.sessionStorage.salesAreaId,
                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                lastmodifiedbyrole: $window.sessionStorage.roleId
            }
            Restangular.all('nontifacilities').post($scope.item).then(function (resp) {
                console.log('nontifacilities', resp);
            });
            $scope.NonTICounter++;
        } else {
            $scope.item = {
                rating: $scope.NonTIfacilityToCheck[$scope.NonTICounter].rating
            };
            Restangular.one('nontifacilities/' + $scope.NonTIfacilityToCheck[$scope.NonTICounter].id).customPUT($scope.item).then(function (response) {
                //window.location = '/';
                //console.log('response', response);
            });
            $scope.NonTICounter++;
        }
        if ($scope.NonTICounter < $scope.NonTIfacilityToCheck.length) {
            $scope.NonTISaveUpdate();
        }
    }
    /*********************** UPDATE **********************************************************/
    $scope.showValidation = false;
    $scope.stakeholderdataModal = false;
    $scope.validatestring = '';
    //    $scope.UpdateTISIMP = function (st, dt, id) {
    //        console.log('I m UpdateTISIMP');
    //        console.log('$scope.employee.implementsti', $scope.employee.implementsti);
    //        //if($scope.employee.implementsti === 'yes'){
    //        for (var i = 0; i < $scope.tifacilities.length; i++) {
    //            $scope.item = {
    //                district: dt
    //                , rating: $scope.tifacilities[i].rating
    //            }
    //            Restangular.one('tifacilities/' + $scope.tifacilities[i].id).customPUT($scope.item).then(function (response) {
    //                window.location = '/';
    //            });
    //        };
    //        //}
    //    };
    //    $scope.count = 0;
    //    $scope.UpdateNONTISIMP = function (st, dt, id) {
    //        //$scope.UpdateTISIMP();
    //        console.log('I m UpdateNONTISIMP');
    //        for (var i = 0; i < $scope.nontifacilities.length; i++) {
    //            $scope.item = {
    //                district: dt
    //                , rating: $scope.nontifacilities[i].rating
    //            }
    //            Restangular.one('nontifacilities/' + $scope.nontifacilities[i].id).customPUT($scope.item).then(function (response) {
    //                window.location = '/';
    //                //console.log('update nontifacilities', response);
    //            });
    //            $scope.count++;
    //            if ($scope.count == $scope.nontifacilities.length) {
    //                // window.location = '/';
    //            }
    //        };
    //    };
    //    $scope.SaveTISIMP = function (st, dt, id) {
    //        if ($scope.addTIRatingSave != true) {
    //            for (var i = 0; i < $scope.tifacilities.length; i++) {
    //                $scope.item = {
    //                    facilityid: id
    //                    , state: st
    //                    , district: dt
    //                    , rating: $scope.tifacilities[i].rating
    //                    , deleteflag: false
    //                    , lastmodifiedtime: new Date()
    //                    , lastmodifiedby: $window.sessionStorage.UserEmployeeId
    //                    , lastmodifiedbyrole: $window.sessionStorage.roleId
    //                }
    //                Restangular.all('tifacilities').post($scope.item).then(function (resp) {
    //                    //console.log('tifacilities', resp);
    //                    window.location = '/';
    //                });
    //            };
    //        }
    //        else {
    //            $scope.current_TIArray = [];
    //            $scope.current_TIArray = $scope.tifacilities.splice($scope.currentTI);
    //            // $scope.courrent_tifacilitiesAdd = $scope.tifacilities.length - $scope.currentTI;
    //            //console.log('$scope.current_TIArray', $scope.current_TIArray);
    //            for (var i = 0; i < $scope.current_TIArray.length; i++) {
    //                $scope.item = {
    //                    facilityid: id
    //                    , state: st
    //                    , district: dt
    //                    , rating: $scope.current_TIArray[i].rating
    //                    , deleteflag: false
    //                    , lastmodifiedtime: new Date()
    //                    , lastmodifiedby: $window.sessionStorage.UserEmployeeId
    //                    , lastmodifiedbyrole: $window.sessionStorage.roleId
    //                }
    //                Restangular.all('tifacilities').post($scope.item).then(function (resp) {
    //                    //console.log('tifacilities', resp);
    //                    window.location = '/';
    //                });
    //            };
    //        }
    //    };
    //    
    //    
    //    $scope.count = 0;
    //    $scope.SaveNONTISIMP = function (st, dt, id) {
    //        if ($scope.addNONTIRatingSave != true) {
    //            for (var i = 0; i < $scope.nontifacilities.length; i++) {
    //                $scope.itemnonti = {
    //                    facilityid: id
    //                    , state: st
    //                    , district: dt
    //                    , rating: $scope.nontifacilities[i].rating
    //                    , deleteflag: false
    //                    , lastmodifiedtime: new Date()
    //                    , lastmodifiedby: $window.sessionStorage.UserEmployeeId
    //                    , lastmodifiedbyrole: $window.sessionStorage.roleId
    //                }
    //                Restangular.all('nontifacilities').post($scope.itemnonti).then(function (resp) {
    //                    console.log('nontifacilities', resp);
    //                    window.location = '/';
    //                });
    //                $scope.count++;
    //                if ($scope.count == $scope.nontifacilities.length) {
    //                    //window.location = '/';
    //                }
    //            };
    //        }
    //        else {
    //            $scope.current_NONTIArray = [];
    //            $scope.current_NONTIArray = $scope.nontifacilities.splice($scope.currentNONTI);
    //            for (var i = 0; i < $scope.current_NONTIArray.length; i++) {
    //                $scope.itemnonti = {
    //                    facilityid: id
    //                    , state: st
    //                    , district: dt
    //                    , rating: $scope.current_NONTIArray[i].rating
    //                    , deleteflag: false
    //                    , lastmodifiedtime: new Date()
    //                    , lastmodifiedby: $window.sessionStorage.UserEmployeeId
    //                    , lastmodifiedbyrole: $window.sessionStorage.roleId
    //                }
    //                Restangular.all('nontifacilities').post($scope.itemnonti).then(function (resp) {
    //                    console.log('nontifacilities', resp);
    //                    window.location = '/';
    //                });
    //                $scope.count++;
    //                if ($scope.count == $scope.nontifacilities.length) {
    //                    //window.location = '/';
    //                }
    //            };
    //        }
    //    };
    /*------------------------------------------------------------------------*/
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    //Datepicker settings start
    $scope.today = function () {
        $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
    };
    $scope.today();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.dtmax = new Date();
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.RegistrationRenewal = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.RegistrationRenewal.opened = true;
    };
    $scope.RegistrationRenewal2 = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.RegistrationRenewal2.opened = true;
    };
    $scope.LastAGBM = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.LastAGBM.opened = true;
    };
    $scope.LastElection = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.LastElection.opened = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };
    $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end
    $scope.HideNoOfTis = true;
    $scope.aRegistration = false;
    $scope.gRegistration = false;
    $scope.fRegistration = false;
    $scope.UpdatenoofTI = function () {
        $scope.items = {
            implementsti: 'no',
            nooftis: null
        }
        Restangular.one('employees', $scope.getFacilityId).customPUT($scope.items).then(function (response) {
            console.log('$scope.employee.implementsti', response);
            //location.reload();
            window.location = '/';
        });
    }
    $scope.deleteTIFacility = function () {
        $scope.item = {
            deleteflag: true
        }
        for (var i = 0; i < $scope.tifacilities.length; i++) {
            console.log('$scope.tifacilities', $scope.tifacilities[i].id);
            Restangular.one('tifacilities/' + $scope.tifacilities[i].id).customPUT($scope.item).then(function (deltifc) {
                console.log('deltifc', deltifc);
                //location.reload();
                $scope.allupdate = true;
            });
            $scope.count++;
            if ($scope.count == $scope.tifacilities.length) {
                $scope.UpdatenoofTI();
            }
        }
    }
    $scope.$watch('employee.implementsti', function (newValue, oldValue) {
        if (newValue == oldValue) {
            return;
        } else if (newValue === 'yes') {
            $scope.HideNoOfTis = false;
            //$scope.employee.nooftis = '';
        } else {
            $scope.nooftis_disable = false;
            $scope.HideNoOfTis = true;
            //$scope.employee.implementsti = 'no';
            //$scope.employee.nooftis = 0;
            /*  $scope.item = {
                  deleteflag: true
              }
              for (var i = 0; i < $scope.tifacilities.length; i++) {
                  console.log('$scope.tifacilities', $scope.tifacilities[i].id);
                  Restangular.one('tifacilities/' + $scope.tifacilities[i].id).customPUT($scope.item).then(function (deltifc) {
                      console.log('deltifc', deltifc);
                      //location.reload();
                      $scope.allupdate = true;
                  });
                  $scope.count++;
                  if ($scope.count == $scope.tifacilities.length) {
                      $scope.UpdatenoofTI();
                  }
              }*/
        }
    });
    $scope.$watch('employee.astatus', function (newValue, oldValue) {
        //console.log('astatus', newValue);
        if (newValue == oldValue) {
            return;
        } else if (newValue != 4) {
            $scope.aRegistration = true;
            $scope.employee.aregistrationno = '';
        } else {
            $scope.aRegistration = false;
        }
    });
    $scope.$watch('employee.gstatus', function (newValue, oldValue) {
        //console.log('gstatus', newValue);
        if (newValue == oldValue) {
            return;
        } else if (newValue != 4) {
            $scope.gRegistration = true;
            $scope.employee.gregistrationno = '';
        } else {
            $scope.gRegistration = false;
        }
    });
    $scope.$watch('employee.frcastatus', function (newValue, oldValue) {
        //console.log('fcrastatus', newValue);
        if (newValue == oldValue) {
            return;
        } else if (newValue != 4) {
            $scope.fRegistration = true;
            $scope.employee.frcaregistrationno = '';
        } else {
            $scope.fRegistration = false;
        }
    });
    $scope.deleteCount = 0;
    //    $scope.DeleteTI2 = function () {
    //        console.log('$scope.deleteCount++', ($scope.deleteCount + 1));
    //        console.log('$scope.employee.nooftis', $scope.employee.nooftis);
    //        $scope.employee.nooftis = $scope.employee.nooftis - ($scope.deleteCount + 1);
    //    }
    $scope.deletebuttonHide = false;
    $scope.deleteCount = 0;
    $scope.DeleteTI = function (family_memberid, index, familymember_beneficiaryid) {
        //        console.log('family_memberid', family_memberid);
        //        console.log('dindex', index);
        //        console.log('familymember_beneficiaryid', familymember_beneficiaryid);
        //        console.log('$scope.deleteCount++', $scope.deleteCount++);
        //        $scope.employee.nooftis = $scope.oldValueTIRating - $scope.deleteCount++;
        if (family_memberid === undefined) {
            $scope.tifacilities.splice(index, 1);
            $scope.employee.nooftis = $scope.employee.nooftis - 1;
        } else {
            //$scope.toggleLoading();
            $scope.item = {
                deleteflag: true
            }
            //            $scope.employti = {
            //                nooftis: $scope.oldValueTIRating - 1
            //            };
            $scope.employee.nooftis = $scope.employee.nooftis - ($scope.deleteCount + 1);
            Restangular.one('tifacilities/' + family_memberid).remove($scope.item).then(function (delTI) {
                console.log('delTI', delTI);
                //$route.reload();
                $scope.tifacilities.splice(index, 1);
                //                   / $scope.tifacilities = $scope.tifacilities;
                //console.log('$scope.tifacilities', $scope.tifacilities);
                //console.log('$scope.tifacilities splice', $scope.tifacilities.splice(index, 1));
                $scope.deletebuttonHide = true;
                //$scope.modalInstanceLoad.close();
                //Restangular.one('employees/' + $scope.getFacilityId).customPUT($scope.employti).then(function (updateCO) {});
            });
            // console.log('$scope.oldValueTIRating', $scope.oldValueTIRating,$scope.getFacilityId)
        }
    }
    $scope.nonDelCount = 0;
    $scope.DeleteNonTI = function (family_memberid, index, familymember_beneficiaryid) {
        //$scope.employee.nooftis = $scope.oldValueTIRating - $scope.deleteCount++;
        if (family_memberid === undefined) {
            $scope.nontifacilities.splice(index, 1);
            $scope.employee.noofnonti = $scope.employee.noofnonti - 1;
        } else {
            //$scope.toggleLoading();
            $scope.item = {
                deleteflag: true
            }
            $scope.employee.noofnonti = $scope.employee.noofnonti - ($scope.nonDelCount + 1);
            Restangular.one('nontifacilities/' + family_memberid).remove().then(function (delTI) {
                console.log('delTI', delTI);
                $scope.nontifacilities.splice(index, 1);
                $scope.deletebuttonHide = true;
            });
        }
    }
    //    $scope.DeleteNonTI2 = function (family_memberid, index, familymember_beneficiaryid) {
    //        //        console.log('family_memberid', family_memberid);
    //        //        console.log('dindex', index);
    //        //        console.log('familymember_beneficiaryid', familymember_beneficiaryid);
    //        if (family_memberid === undefined) {
    //            $scope.nontifacilities.splice(index, 1);
    //            $scope.employee.noofnonti = $scope.employee.noofnonti - 1;
    //        }
    //        //        else if (index == 0){
    //        //          alert('Cant Delete')  
    //        //        }
    //        else {
    //            $scope.toggleLoading();
    //            $scope.employnonti = {
    //                noofnonti: $scope.oldValueNonTIRating - 1
    //            };
    //            Restangular.one('nontifacilities/' + family_memberid).remove().then(function (delTI) {
    //                Restangular.one('employees/' + $scope.getFacilityId).customPUT($scope.employnonti).then(function (updateCO) {
    //                    $route.reload();
    //                    $scope.modalInstanceLoad.close();
    //                });
    //            });
    //        }
    //    }
    $scope.modalTitle = 'Thank You';
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.facilityprofileheader = langResponse.facilityprofileheader;
        $scope.firstname = langResponse.firstname;
        $scope.lastname = langResponse.lastname;
        $scope.pemail = langResponse.email;
        $scope.pstate = langResponse.state;
        $scope.paddress = langResponse.address;
        $scope.ppincode = langResponse.pincode;
        $scope.pmobile = langResponse.mobile;
        $scope.pdistrict = langResponse.district;
        $scope.facilitycode = langResponse.facilitycode;
        $scope.member = langResponse.member;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.okbutton = langResponse.ok;
        $scope.printdate = langResponse.date;
        $scope.printAdd = langResponse.addbutton;
        $scope.printSave = langResponse.savebutton;
        $scope.printEdit = langResponse.update;
        $scope.BASIC = langResponse.basic;
        $scope.NON_STATUTORY = langResponse.nonstatutory;
        $scope.STATUTORY = langResponse.statutory;
        $scope.Facility_Type = langResponse.facilitytype;
        $scope.AreaofOperation = langResponse.areaofoperation;
        $scope.Typology = langResponse.typology;
        $scope.insurancecompany = langResponse.insurancecompany;
        $scope.nooftowncover = langResponse.nooftowncover;
        $scope.noofblockcovered = langResponse.noofblockcovered;
        $scope.corepresentationinhiv = langResponse.corepresentationinhiv;
        $scope.stateornationallevel = langResponse.stateornationallevel;
        $scope.NON_STATUTORY = langResponse.nonstatutory;
        $scope.STATUTORY = langResponse.statutory;
        $scope.Facility_Type = langResponse.facilitytype;
        $scope.Implements_TI = langResponse.implementti;
        $scope.noofincome = langResponse.noofincome;
        $scope.tirating = langResponse.tirating;
        $scope.noofnontiimplemented = langResponse.noofnontiimplemented;
        $scope.nameofprojects = langResponse.nameofprojects;
        $scope.nooftiimplemented = langResponse.nooftiimplemented;
        $scope.Registration_Number = langResponse.registrationno;
        $scope.RegistrationRenewalDate = langResponse.registrationrenewaldt;
        $scope.pancardno = langResponse.pancardno;
        $scope.incometaxreturn = langResponse.incometaxreturn;
        $scope.astatus = langResponse.astatus;
        $scope.gstatus = langResponse.gstatus;
        $scope.fcrastatus = langResponse.fcrastatus;
        $scope.dateoflastagbm = langResponse.dateoflastagbm
        $scope.ExternalAuditCompleted = langResponse.externalaudit;
        $scope.AuditReportSubmitted = langResponse.auditreport;
        $scope.boardhasdiscuss = langResponse.boardhasdiscuss;
        $scope.dateoflastelection = langResponse.dateoflastelection;
        $scope.totalnoofmembers = langResponse.totalnoofmembers;
        $scope.fcrarenewaldate = langResponse.fcrarenewaldate;
        $scope.hasbeenupdated = langResponse.hasbeenupdated;
        $scope.modalTitle = langResponse.thankyou;
        $scope.message = langResponse.facilityhasupdated;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;


        $scope.enterfirstname = langResponse.enterfirstname;
        $scope.selfacilitytype = langResponse.selfacilitytype;
        $scope.selareaofopt = langResponse.selareaofopt;
        $scope.enternooftowncov = langResponse.enternooftowncov;
        $scope.entermandal = langResponse.entermandal;
        $scope.enternoofincome = langResponse.enternoofincome;
        $scope.selimplementsti = langResponse.selimplementsti;
        $scope.enternooftis = langResponse.enternooftis;
        $scope.enternoofnontis = langResponse.enternoofnontis;
        $scope.enterregisterno = langResponse.enterregisterno;
        $scope.selregrenewdate = langResponse.selregrenewdate;
        $scope.enterpancard = langResponse.enterpancard;
        $scope.selastatus = langResponse.selastatus;
        $scope.selgstatus = langResponse.selgstatus;
        $scope.selfcrastatus = langResponse.selfcrastatus;
        $scope.seldateofagbm = langResponse.seldateofagbm;
        $scope.selexternalaudit = langResponse.selexternalaudit;
        $scope.selauditreport = langResponse.selauditreport;
        $scope.selactionplan = langResponse.selactionplan;
        $scope.seldateofelection = langResponse.seldateofelection;
        $scope.enterelectionbody = langResponse.enterelectionbody
        $scope.facilityhasupdated = langResponse.facilityhasupdated;
    });

});
