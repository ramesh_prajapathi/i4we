'use strict';

angular.module('secondarySalesApp')
    .controller('UsersCtrl', function ($scope, Restangular, $http, $window, $route, $filter) {

        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.someFocusVariable = true;

        if ($window.sessionStorage.user_zoneId == null || $window.sessionStorage.user_zoneId == undefined) {

            $window.sessionStorage.user_zoneId = null;
            //  $window.sessionStorage.user_facilityId = null;
            $window.sessionStorage.user_currentPage = 1;
            $window.sessionStorage.user_currentPagesize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.user_zoneId;
            //  $scope.failityId = $window.sessionStorage.user_facilityId;
            $scope.currentpage = $window.sessionStorage.user_currentPage;
            $scope.pageSize = $window.sessionStorage.user_currentPagesize;
        }


        if ($window.sessionStorage.prviousLocation != "partials/users-form") {
            $window.sessionStorage.user_zoneId = '';
            //  $window.sessionStorage.user_facilityId = '';
            $window.sessionStorage.user_currentPage = 1;
            $window.sessionStorage.user_currentPagesize = 25;
            //$scope.currentpage = 1;
            //$scope.pageSize = 25;

            //$scope.tds = Restangular.all('users?filter[where][roleId]=1&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (response) {

            //$scope.tds = Restangular.all('users?filter={"where":{"and":[{"roleId":{"inq":[16]}},{"id":{"inq":[' +$window.sessionStorage.userId+']}}]}}').getList().then(function (response) {	

            //			$scope.led = Restangular.all('users?filter[where][or][0][id]=' + $window.sessionStorage.userId + '&filter[where][or][1][deleteflag]=false' + '&filter[where][or][1][roleId]=16').getList().then(function (response) {
            //
            //				//$scope.tds = Restangular.all('users?filter={"where":{"and":[{"roleId":{"inq":[16]}},{"id":{"inq":[1249]}}]}}').getList().then(function (response) {
            //
            //				//console.log('response', response);
            //				$scope.users = response;
            //				angular.forEach($scope.users, function (member, index) {
            //					member.index = index + 1;
            //				});
            //			});
        }

        $scope.currentpage = $window.sessionStorage.user_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.user_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.user_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.user_currentPagesize = mypage;
        };

        Restangular.all('roles').getList().then(function (rls) {

            $scope.rolesdsply = rls;


            Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (lngs) {

                $scope.languagesdsply = lngs;

                Restangular.all('countries').getList().then(function (cntry) {

                    $scope.displayCountries = cntry;

                    Restangular.all('states').getList().then(function (sts) {

                        $scope.displayStates = sts;

                        Restangular.all('districts').getList().then(function (dtrs) {

                            $scope.displayDistricts = dtrs

                            Restangular.all('sites').getList().then(function (stis) {

                                $scope.displaySites = stis;

                                Restangular.all('users?filter[where][deleteFlag]=false').getList().then(function (user) {

                                    $scope.usersArray = [];

                                    $scope.users = user;

                                    angular.forEach($scope.users, function (member, index) {
                                        member.index = index + 1;

                                        member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                                        member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                                        for (var m = 0; m < $scope.rolesdsply.length; m++) {
                                            if (member.roleId == $scope.rolesdsply[m].id) {
                                                member.rolename = $scope.rolesdsply[m].name;
                                                break;
                                            }
                                        }
                                        for (var n = 0; n < $scope.languagesdsply.length; n++) {
                                            if (member.languageId == $scope.languagesdsply[n].id) {
                                                member.languagename = $scope.languagesdsply[n].name;
                                                break;
                                            }
                                        }
                                        for (var u = 0; u < $scope.users.length; u++) {
                                            if (member.lastModifiedBy == $scope.users[u].id) {
                                                member.lastModifiedByname = $scope.users[u].username;
                                                break;
                                            }
                                        }
                                        for (var v = 0; v < $scope.rolesdsply.length; v++) {
                                            if (member.lastModifiedByRole == $scope.rolesdsply[v].id) {
                                                member.lastModifiedByRolename = $scope.rolesdsply[v].name;
                                                break;
                                            }
                                        }
                                        for (var t = 0; t < $scope.users.length; t++) {
                                            if (member.createdBy == $scope.users[t].id) {
                                                member.createdByname = $scope.users[t].username;
                                                break;
                                            }
                                        }
                                        for (var w = 0; w < $scope.rolesdsply.length; w++) {
                                            if (member.createdByRole == $scope.rolesdsply[w].id) {
                                                member.createdByRolename = $scope.rolesdsply[w].name;
                                                break;
                                            }
                                        }

                                        if (member.countryId != null) {
                                            member.arrcountry = member.countryId.split(",");
                                            member.countryname = "";

                                            for (var i = 0; i < member.arrcountry.length; i++) {
                                                for (var j = 0; j < $scope.displayCountries.length; j++) {
                                                    if ($scope.displayCountries[j].id + "" === member.arrcountry[i] + "") {
                                                        if (member.countryname === "") {
                                                            member.countryname = $scope.displayCountries[j].name;
                                                        } else {
                                                            member.countryname = member.countryname + "," + $scope.displayCountries[j].name;
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        } else {
                                            member.countryname = "";
                                        }

                                        if (member.stateId != null) {
                                            member.arraystate = member.stateId.split(",");
                                            member.statename = "";

                                            for (var i = 0; i < member.arraystate.length; i++) {
                                                for (var j = 0; j < $scope.displayStates.length; j++) {
                                                    if ($scope.displayStates[j].id + "" === member.arraystate[i] + "") {
                                                        if (member.statename === "") {
                                                            member.statename = $scope.displayStates[j].name;
                                                        } else {
                                                            member.statename = member.statename + "," + $scope.displayStates[j].name;
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        } else {
                                            member.statename = "";
                                        }

                                        if (member.districtId != null) {
                                            member.arraydistrict = member.districtId.split(",");
                                            member.districtname = "";

                                            for (var i = 0; i < member.arraydistrict.length; i++) {
                                                for (var j = 0; j < $scope.displayDistricts.length; j++) {
                                                    if ($scope.displayDistricts[j].id + "" === member.arraydistrict[i] + "") {
                                                        if (member.districtname === "") {
                                                            member.districtname = $scope.displayDistricts[j].name;
                                                        } else {
                                                            member.districtname = member.districtname + "," + $scope.displayDistricts[j].name;
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        } else {
                                            member.districtname = "";
                                        }

                                        if (member.siteId != null) {
                                            member.arraysite = member.siteId.split(",");
                                            member.sitename = "";

                                            for (var i = 0; i < member.arraysite.length; i++) {
                                                for (var j = 0; j < $scope.displaySites.length; j++) {
                                                    if ($scope.displaySites[j].id + "" === member.arraysite[i] + "") {
                                                        if (member.sitename === "") {
                                                            member.sitename = $scope.displaySites[j].name;
                                                        } else {
                                                            member.sitename = member.sitename + "," + $scope.displaySites[j].name;
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        } else {
                                            member.sitename = "";
                                        }

                                    });
                                    $scope.changeSorting('name');
                                });
                            });
                        });
                    });
                });
            });
        });

        Restangular.all('states?filter[where][deleteFlag]=false').getList().then(function (stes) {
            $scope.states = stes;
            $scope.countryId = $window.sessionStorage.user_zoneId;
        });


        $scope.getRole = function (roleId) {
            return Restangular.one('roles', roleId).get().$object;
        };

        $scope.getState = function (stateId) {
            return Restangular.one('states', stateId).get().$object;
        };

        $scope.getSite = function (siteId) {
            return Restangular.one('sites', siteId).get().$object;
        };

        /*****************************************************************	


        	$scope.user = {
        		flag: true,
        		email: '',
        		zoneId: '',
        		salesAreaId: '',
        		coorgId: '',
        		lastmodifiedtime: $filter('date')(new Date(), 'y-MM-dd'),
        		lastmodifiedby: $window.sessionStorage.UserEmployeeId
        		
        	};
        	$scope.lastmodifiedby = $scope.user.usrName;

        	console.log('$window.sessionStorage.State', $window.sessionStorage.zoneId);
        	console.log('$window.sessionStorage.Distric', $window.sessionStorage.salesAreaId);
        	console.log('$window.sessionStorage.Facility', $window.sessionStorage.coorgId);
        	console.log('$window.sessionStorage.LastModifyBy', $scope.lastmodifiedby);
        	console.log('$window.sessionStorage.Site', $window.sessionStorage.UserEmployeeId);
        	
        	$scope.getRole = function (stakeholdertype) {
        		return Restangular.one('roles', stakeholdertype).get().$object;
        	};


        	$scope.groups = Restangular.all('groups?filter[where][deleteflag]=null').getList().$object;
        	$scope.departments = Restangular.all('departments?filter[where][deleteflag]=null').getList().$object;
        	$scope.ims = Restangular.all('ims?filter[where][deleteflag]=null').getList().$object;
        	$scope.admins = Restangular.all('employees?filter[where][groupId]=1&filter[where][deleteflag]=null').getList().$object;
        	$scope.spms = Restangular.all('employees?filter[where][groupId]=3&filter[where][deleteflag]=null').getList().$object;
        	$scope.rios = Restangular.all('employees?filter[where][groupId]=4&filter[where][deleteflag]=null').getList().$object;
        	//$scope.fieldworkers = Restangular.all('fieldworkers?filter[where][groupId]=4').getList().$object;

        	$scope.fieldworkers = Restangular.all('fieldworkers?filter[where][deleteflag]=null&filter[where][usercreated]=null').getList().$object;


        	$scope.comembers = Restangular.all('comembers?filter[where][deleteflag]=null&filter[where][usercreated]=null').getList().$object;

        	$scope.employees = Restangular.all('employees?filter[where][deleteflag]=null&filter[where][usercreated]=null').getList().$object;

        	//$scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;

        	//$scope.salesAreas = Restangular.all('sales-areas?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][deleteflag]=null').getList().$object;

        	//$scope.zones = Restangular.all('zones').getList().$object;

        	//$scope.employees = Restangular.all('employees').getList().$object;
        	//$scope.comembers = Restangular.all('comembers').getList().$object;
        	//$scope.languagemasters = Restangular.all('languagemasters').getList().$object;

        	/************************************************ SAVE *******************************
        	$scope.createduser = {
        		"usercreated": 'yes'
        	}
        	$scope.Save = function () {
        		$scope.users.post($scope.user).then(function () {
        			console.log('$scope.user', $scope.user);
        			// window.location = '/users';

        			if ($scope.user.roleId == 1 || $scope.user.roleId == 3 || $scope.user.roleId == 4) {
        				Restangular.one('employees/' + $scope.user.employeeid).customPUT($scope.createduser).then(function () {
        					window.location = '/users';
        				});
        			}

        			if ($scope.user.roleId == 5) {
        				Restangular.one('comembers/' + $scope.user.employeeid).customPUT($scope.createduser).then(function () {
        					window.location = '/users';
        				});
        			}

        			if ($scope.user.roleId == 6) {
        				Restangular.one('fieldworkers/' + $scope.user.employeeid).customPUT($scope.createduser).then(function () {
        					window.location = '/users';
        				});
        			}
        		}, function (error) {
        			console.log('error', error);
        		});
        	};


        	/******************************************** WATCH *************************

        	$scope.$watch('user.usrName', function (newValue, oldValue) {
        		if (newValue === oldValue || newValue == '' || newValue == undefined) {
        			return;
        		} else {
        			$scope.newUser = JSON.parse(newValue);
        			if ($scope.user.roleId == 1 || $scope.user.roleId == 3 || $scope.user.roleId == 4) {
        				$scope.user.username = $scope.newUser.salesCode.toLowerCase();
        				$scope.user.employeeid = $scope.newUser.id;
        				$scope.user.mobile = $scope.newUser.mobile;
        				$scope.user.email = $scope.newUser.email;
        			}

        			if ($scope.user.roleId == 5) {
        				$scope.user.username = $scope.newUser.name.toLowerCase().replace(' ', '');
        				$scope.user.employeeid = $scope.newUser.id;
        				$scope.user.mobile = $scope.newUser.helpline;
        			}

        			if ($scope.user.roleId == 6) {
        				$scope.user.username = $scope.newUser.fwcode.toLowerCase();
        				$scope.user.employeeid = $scope.newUser.id;
        				$scope.user.mobile = $scope.newUser.mobile;
        				$scope.user.email = $scope.newUser.email;
        			}
        		}
        	});

        	$scope.hideState = false;
        	$scope.hideDistrict = false;
        	$scope.hideCo = false;
        	$scope.$watch('user.roleId', function (newValue, oldValue) {
        		if (newValue === oldValue || newValue == '' || newValue == undefined) {
        			return;
        		} else {
        			$scope.user.username = null;
        			$scope.user.employeeid = null;
        			if (newValue == 1) {
        				$scope.hideState = true;
        				$scope.hideDistrict = true;
        				$scope.hideCo = true;
        			} else if (newValue == 3) {
        				$scope.hideState = false;
        				$scope.hideDistrict = true;
        				$scope.hideCo = true;
        			} else if (newValue == 4) {
        				$scope.hideState = false;
        				$scope.hideDistrict = true;
        				$scope.hideCo = true;
        			} else if (newValue == 5) {
        				$scope.hideState = false;
        				$scope.hideDistrict = false;
        				$scope.hideCo = false;
        			} else if (newValue == 6) {
        				$scope.hideState = false;
        				$scope.hideDistrict = false;
        				$scope.hideCo = false;
        			}
        		}
        	});


        	$scope.cofw = {
        		data: 'co'
        	};
        	$scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;
        	$scope.$watch('cofw.data', function (newValue, oldValue) {
        		if (newValue === oldValue) {
        			return;
        		} else {
        			console.log(newValue);
        			if (newValue == 'co') {
        				$scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;
        			} else if (newValue == 'fw') {
        				$scope.partners = Restangular.all('partners?filter[where][groupId]=9').getList().$object;
        			}
        		}
        	});

        	$scope.$watch('user.zoneId', function (newValue, oldValue) {
        		if (newValue === oldValue | newValue == '') {
        			return;
        		} else {
        			//$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue).getList().$object;
        			$scope.salesAreas = Restangular.all('sales-areas?filter[where][groupId]=5' + '&filter[where][deleteflag]=null').getList().$object;
        		}
        	});

        	$scope.$watch('user.salesAreaId', function (newValue, oldValue) {
        		if (newValue === oldValue | newValue == '') {
        			return;
        		} else {
        			//$scope.copartners = Restangular.all('partners?filter[where][salesAreaId]=' + newValue + '&filter[where][groupId]=8').getList().$object;
        			//$scope.copartners = Restangular.all('employees?filter[where][groupId]=5').getList().$object;
        			$scope.copartners = Restangular.all('employees?filter[where][groupId]=5' + '&filter[where][deleteflag]=null').getList().$object;


        		}
        	});


        	//$scope.users = Restangular.all('users').getList().$object;



        	//$scope.employees = Restangular.all('employees').getList().$object;
        	$scope.groups = Restangular.all('groups').getList().$object;
        	$scope.roles = Restangular.all('roles').getList().$object;



        	//$scope.salesAreas = Restangular.all('sales-areas').getList().$object;
        	$scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
        	$scope.distributionSubareas = Restangular.all('distribution-subareas').getList().$object;



        	$scope.$watch('user.distributionAreaId', function (newValue, oldValue) {
        		if (newValue === oldValue) {
        			return;
        		} else {
        			$scope.user.organizationId = newValue;
        		}
        	});

        	$scope.$watch('user.salesAreaId', function (newValue, oldValue) {
        		if (newValue === oldValue) {
        			return;
        		} else {
        			$scope.user.organizationId = newValue;
        		}
        	});
            
             /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        /**************************Export data to excel sheet ***************/

        $scope.valUserCount = 0;

        $scope.exportData = function () {
            $scope.valUserCount = 0;
            // console.log('$scope.householdsArray', $scope.householdsArray);
            if ($scope.users.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.users.length; c++) {
                    $scope.usersArray.push({
                        'NAME': $scope.users[c].name,
                        'TYPE OF DOCTOR': $scope.users[c].typeOfDoctor,
                        'COUNTRY': $scope.users[c].countryname,
                        'STATE': $scope.users[c].statename,
                        'DISTRICT': $scope.users[c].districtname,
                        'SITE': $scope.users[c].sitename,
                        'ROLE': $scope.users[c].rolename,
                        'USERNAME': $scope.users[c].username,
                        'LANGUAGE': $scope.users[c].languagename,
                        'STATUS': $scope.users[c].status,
                        'MOBILE': $scope.users[c].mobile,
                        'EMAIL': $scope.users[c].email,
                        'CREATED BY': $scope.users[c].createdByname,
                        'CREATED DATE': $scope.users[c].createdDate,
                        'CREATED ROLE': $scope.users[c].createdByRolename,
                        'LAST MODIFIED DATE': $scope.users[c].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.users[c].lastModifiedByRolename,
                        'LAST MODIFIED BY': $scope.users[c].lastModifiedByname,
                        'DELETE FLAG': $scope.users[c].deleteFlag
                    });

                    $scope.valUserCount++;
                    if ($scope.users.length == $scope.valUserCount) {
                        alasql('SELECT * INTO XLSX("users.xlsx",{headers:true}) FROM ?', [$scope.usersArray]);
                    }
                }
            }
        };


        /**************************************CALLING FUNCTION**********************/

        //  $scope.countryId = '';

        $scope.$watch('countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $window.sessionStorage.user_zoneId = newValue;

                Restangular.all('users?filter[where][stateId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (user) {
                    $scope.users = user;
                    angular.forEach($scope.users, function (member, index) {
                        member.index = index + 1;
                    });
                    $scope.changeSorting('name');
                });
            }
        });

        //		$scope.$watch('failityId', function (newValue, oldValue) {
        //			if (newValue === oldValue || newValue == '') {
        //				return;
        //			} else {
        //				$window.sessionStorage.user_facilityId = newValue;
        //				$scope.users = Restangular.all('users?filter[where][coorgId]=' + newValue + '&filter[where][zoneId]=' + $scope.countiesid + '&filter[where][deleteflag]=false').getList().then(function (ctyRes) {
        //					$scope.users = ctyRes;
        //					angular.forEach($scope.users, function (member, index) {
        //						member.index = index + 1;
        //					});
        //				});
        //			}
        //		});

        $scope.updateUsrFlag = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('users/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });