'use strict';

angular.module('secondarySalesApp')
    .controller('SitesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
        /*********/
        //		if ($window.sessionStorage.roleId != 1) {
        //			window.location = "/";
        //		}

        $scope.someFocusVariable = true;

        $scope.showForm = function () {
            var visible = $location.path() === '/site/create' || $location.path() === '/site/edit/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/site/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/site/create' || $location.path() === '/site/edit/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/site/create' || $location.path() === '/site/edit/' + $routeParams.id;
            return visible;
        };
        /************************************************************************************/
        if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
            $window.sessionStorage.facility_zoneId = null;
            $window.sessionStorage.facility_stateId = null;
            $window.sessionStorage.facility_currentPage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.facility_zoneId;
            $scope.stateId = $window.sessionStorage.facility_stateId;
            $scope.currentpage = $window.sessionStorage.facility_currentPage;
            $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        }


        if ($window.sessionStorage.prviousLocation != "partials/site") {
            $window.sessionStorage.facility_zoneId = '';
            $window.sessionStorage.facility_stateId = '';
            $window.sessionStorage.facility_currentPage = 1;
            $scope.currentpage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
            $scope.pageSize = 25;
        }

        $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.facility_currentPageSize = mypage;
        };

        $scope.currentpage = $window.sessionStorage.facility_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.facility_currentPage = newPage;
        };

        /************************************************/

        Restangular.all('countries?filter[where][deleteFlag]=false').getList().then(function (ct) {
            $scope.countries = ct;
            $scope.countryId = ct[0].id;
            $scope.stateId = $window.sessionStorage.facility_stateId;
        });

        $scope.statedsply = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;

        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;

        Restangular.all('typeofsites?filter[where][deleteFlag]=false').getList().then(function (typeofsites) {
            $scope.typeofsites = typeofsites;
        });

        //  $scope.countryId = '';
        $scope.stateId = '';
        $scope.statesid = '';
        $scope.countiesid = '';
        $scope.districtId = '';

        $scope.$watch('countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $window.sessionStorage.facility_zoneId = newValue;

                $scope.siteArray = [];

                Restangular.all('states?filter[where][countryId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (responceSt) {
                    $scope.displaystates = responceSt;
                    $scope.stateId = $window.sessionStorage.facility_stateId;
                });

                Restangular.all('sites?filter[where][countryId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (ctyRes1) {
                    $scope.DisplaySites = ctyRes1;
                    angular.forEach($scope.DisplaySites, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.countries.length; a++) {
                            if (member.countryId == $scope.countries[a].id) {
                                member.countryname = $scope.countries[a].name;
                                break;
                            }
                        }
                        for (var b = 0; b < $scope.statedsply.length; b++) {
                            if (member.stateId == $scope.statedsply[b].id) {
                                member.statename = $scope.statedsply[b].name;
                                break;
                            }
                        }
                        for (var d = 0; d < $scope.districtdsply.length; d++) {
                            if (member.districtId == $scope.districtdsply[d].id) {
                                member.districtname = $scope.districtdsply[d].name;
                                break;
                            }
                        }
                        for (var d = 0; d < $scope.typeofsites.length; d++) {
                            if (member.typeOfSite == $scope.typeofsites[d].id) {
                                member.typeofsitename = $scope.typeofsites[d].name;
                                break;
                            }
                        }
                    });
                });
                $scope.countiesid = +newValue;
            }
        });

        $scope.$watch('stateId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $window.sessionStorage.facility_stateId = newValue;

                $scope.siteArray = [];

                Restangular.all('districts?filter[where][deleteFlag]=false' + '&filter[where][stateId]=' + newValue).getList().then(function (dt) {
                    $scope.displaydistricts = dt;
                    $scope.stateId = $window.sessionStorage.facility_stateId;
                });

                Restangular.all('sites?filter[where][stateId]=' + newValue + '&filter[where][countryId]=' + $scope.countiesid + '&filter[where][deleteFlag]=false').getList().then(function (ctyRes) {
                    $scope.DisplaySites = ctyRes;
                    angular.forEach($scope.DisplaySites, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.countries.length; a++) {
                            if (member.countryId == $scope.countries[a].id) {
                                member.countryname = $scope.countries[a].name;
                                break;
                            }
                        }
                        for (var b = 0; b < $scope.statedsply.length; b++) {
                            if (member.stateId == $scope.statedsply[b].id) {
                                member.statename = $scope.statedsply[b].name;
                                break;
                            }
                        }
                        for (var d = 0; d < $scope.districtdsply.length; d++) {
                            if (member.districtId == $scope.districtdsply[d].id) {
                                member.districtname = $scope.districtdsply[d].name;
                                break;
                            }
                        }
                        for (var d = 0; d < $scope.typeofsites.length; d++) {
                            if (member.typeOfSite == $scope.typeofsites[d].id) {
                                member.typeofsitename = $scope.typeofsites[d].name;
                                break;
                            }
                        }

                    });
                });
            }
            $scope.statesid = +newValue;
        });

        $scope.$watch('districtId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $window.sessionStorage.facility_stateId = newValue;

                $scope.siteArray = [];

                Restangular.all('sites?filter[where][districtId]=' + newValue + '&filter[where][stateId]=' + $scope.statesid + '&filter[where][countryId]=' + $scope.countiesid + '&filter[where][deleteFlag]=false').getList().then(function (ctyRes) {
                    $scope.DisplaySites = ctyRes;
                    angular.forEach($scope.DisplaySites, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.countries.length; a++) {
                            if (member.countryId == $scope.countries[a].id) {
                                member.countryname = $scope.countries[a].name;
                                break;
                            }
                        }
                        for (var b = 0; b < $scope.statedsply.length; b++) {
                            if (member.stateId == $scope.statedsply[b].id) {
                                member.statename = $scope.statedsply[b].name;
                                break;
                            }
                        }
                        for (var d = 0; d < $scope.districtdsply.length; d++) {
                            if (member.districtId == $scope.districtdsply[d].id) {
                                member.districtname = $scope.districtdsply[d].name;
                                break;
                            }
                        }
                        for (var d = 0; d < $scope.typeofsites.length; d++) {
                            if (member.typeOfSite == $scope.typeofsites[d].id) {
                                member.typeofsitename = $scope.typeofsites[d].name;
                                break;
                            }
                        }

                    });
                });
            }
        });

        $scope.DisplaySites = [];
        /**********************************************************************************/
        $scope.statedisable = false;
        $scope.districdisable = false;
        $scope.searchCity = $scope.name;
        $scope.getSalesArea = function (salesareaId) {
            return Restangular.one('sales-areas', salesareaId).get().$object;
        };

        $scope.getCountry = function (countryId) {
            return Restangular.one('countries', countryId).get().$object;
        };

        $scope.getState = function (stateId) {
            return Restangular.one('states', stateId).get().$object;
        };

        $scope.getDistrict = function (districtId) {
            return Restangular.one('districts', districtId).get().$object;
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('sites/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /********************************************** WATCH ***************************************/
        $scope.znes = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
            $scope.zones = znes;
            $scope.countryId = $window.sessionStorage.facility_zoneId;
        });

        $scope.site = {
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.UserEmployeeId,
            deleteFlag: false
        };

        $scope.site.countryId = '';
        $scope.site.siteId = '';

        $scope.$watch('site.countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.states = Restangular.all('states?filter[where][countryId]=' + newValue + '&filter[where][deleteFlag]=false').getList().$object;
            }
        });

        $scope.$watch('site.stateId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.districts = Restangular.all('districts?filter[where][stateId]=' + newValue + '&filter[where][deleteFlag]=false').getList().$object;
            }
        });


        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('code').style.border = "";

            if ($scope.site.name == '' || $scope.site.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Site Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.site.code == '' || $scope.site.code == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Site Code';
                document.getElementById('code').style.borderColor = "#FF0000";

            } else if ($scope.site.countryId == '' || $scope.site.countryId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Country';

            } else if ($scope.site.stateId == '' || $scope.site.stateId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select State';

            } else if ($scope.site.districtId == '' || $scope.site.districtId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select District';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.all('sites').post($scope.site).then(function () {
                    window.location = '/site-list';
                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /******************************************************** UPDATE ************************/
        $scope.modalTitle = 'Thank You';
        if ($routeParams.id) {
            $scope.statedisable = true;
            $scope.districdisable = true;
            $scope.message = 'Site has been Updated!';
            Restangular.one('sites', $routeParams.id).get().then(function (site) {
                $scope.original = site;
                Restangular.all('states?filter[where][countryId]=' + $scope.original.countryId + '&filter[where][deleteFlag]=false').getList().then(function (state) {
                    $scope.states = state;

                    Restangular.all('districts?filter[where][countryId]=' + $scope.original.countryId + '&filter[where][stateId]=' + $scope.original.stateId + '&filter[where][deleteFlag]=false').getList().then(function (dist) {
                        $scope.districts = dist;
                        $scope.site = Restangular.copy($scope.original);
                    });
                });
            });
        } else {
            $scope.message = 'Site has been Created!';
        }


        /*
        		$scope.validatestring = '';
        		$scope.Update = function () {
        			delete $scope.city.district;
        			delete $scope.city.facility;
        			delete $scope.city.site;
        			delete $scope.city.state;
        			$scope.submitcities.customPUT($scope.city).then(function (updateRes) {
        				console.log('$scope.city', updateRes);
        				window.location = '/cities';
        			}, function (error) {
        				console.log('error', error);
        			});
        		};*/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('code').style.border = "";

            if ($scope.site.name === '' || $scope.site.name === null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Site Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.site.code == '' || $scope.site.code == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Site Code';
                document.getElementById('code').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.submitDisable = true;
                //                delete $scope.city.district;
                //                delete $scope.city.facility;
                //                delete $scope.city.site;
                //                delete $scope.city.state;
                Restangular.one('sites', $routeParams.id).customPUT($scope.site).then(function () {
                    //$location.path('/cities');
                    window.location = '/site-list';
                });
            }
        };

        /**************************Export data to excel sheet ***************/

        $scope.valSteCount = 0;

        $scope.exportData = function () {
            $scope.valSteCount = 0;
            // console.log('$scope.householdsArray', $scope.householdsArray);
            if ($scope.DisplaySites.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.DisplaySites.length; c++) {
                    $scope.siteArray.push({
                        'SITE': $scope.DisplaySites[c].name,
                        'SITE CODE': $scope.DisplaySites[c].code,
                        'TYPE OF SITE': $scope.DisplaySites[c].typeofsitename,
                        'DISTRICT': $scope.DisplaySites[c].districtname,
                        'STATE': $scope.DisplaySites[c].statename,
                        'COUNTRY': $scope.DisplaySites[c].countryname,
                        'DELETE FLAG': $scope.DisplaySites[c].deleteFlag
                    });

                    $scope.valSteCount++;

                    if ($scope.DisplaySites.length == $scope.valSteCount) {
                        alasql('SELECT * INTO XLSX("sites.xlsx",{headers:true}) FROM ?', [$scope.siteArray]);
                    }
                }
            }
        };
    });