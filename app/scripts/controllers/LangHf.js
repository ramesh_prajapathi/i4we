'use strict';

angular.module('secondarySalesApp')
    .controller('LangHfCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {


        $scope.HideCreateButton = true;
        $scope.langdisable = false;

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;
    
        $scope.$watch('hf.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                
                 Restangular.one('hflanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.hf = Restangular.copy($scope.original);
                    });
                
                Restangular.all('hflanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                        $scope.HideCreateButton = true;
                    } else {
                        // $scope.LangId = response[0].id;
                       // $scope.HideSubmitButton = false;
                       // $scope.langdisable = true;

                        //$scope.reportincident = response[0];
                        // console.log('$scope.reportincident', $scope.reportincident);
                        $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';

                    }
                });

            }
        });
    
            /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/LangHf-list';

        };
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/

        $scope.hf = {
            deleteFlag: false,
            lastModifiedDate: new Date()
        };

        $scope.Save = function () {
            Restangular.all('hflanguages').post($scope.hf).then(function (schoolResponse) {
                window.location = '/LangHf-list';
            });

        };

        $scope.Update = function () {
            Restangular.one('hflanguages', $routeParams.id).customPUT($scope.hf).then(function (school) {
                window.location = '/LangHf-list';
            });
        };

        if ($routeParams.id) {
            $scope.HideCreateButton = false;
            $scope.langdisable = true;
            Restangular.one('hflanguages', $routeParams.id).get().then(function (hf) {
                $scope.original = hf;
                $scope.hf = Restangular.copy($scope.original);
            });
        }

    });
