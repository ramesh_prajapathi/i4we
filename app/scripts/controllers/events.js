'use strict';
angular.module('secondarySalesApp').controller('EventsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter, $modal) {
    /*********/


    /*************/
    $scope.groupcreate = true;
    if ($window.sessionStorage.roleId != 5) {
        window.location = "/";
    }
    $scope.showForm = function () {
        var visible = $location.path() === '/events/create' || $location.path() === '/events/' + $routeParams.id;
        return visible;
    };
    $scope.isCreateView = function () {
        if ($scope.showForm()) {
            var visible = $location.path() === '/events/create';
            return visible;
        }
    };
    $scope.hideCreateButton = function () {
        var visible = $location.path() === '/events/create' || $location.path() === '/events/' + $routeParams.id;
        return visible;
    };
    $scope.hideSearchFilter = function () {
        var visible = $location.path() === '/events/create' || $location.path() === '/events/' + $routeParams.id;
        return visible;
    };
    /***************************************** Pagination ********************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    if ($window.sessionStorage.prviousLocation != "partials/events") {
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
    };
    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    console.log('Date Format', $filter('date')(new Date(), 'yyyy-MM-dd T HH:mm:ss Z'));
    $scope.UserLanguage = $window.sessionStorage.language;
    /******************* Language **********************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.eventheader = langResponse.eventheader;
        $scope.addnew = langResponse.addnew;
        $scope.date = langResponse.date;
        $scope.eventname = langResponse.eventname;
        $scope.show = langResponse.show;
        $scope.entry = langResponse.entry;
        $scope.searchfor = langResponse.searchfor;
        $scope.action = langResponse.action;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.venue = langResponse.venue;
        $scope.pillar = langResponse.pillar;
        $scope.eventtype = langResponse.eventtype;
        $scope.organizedby = langResponse.organizedby;
        $scope.printothers = langResponse.others;
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
		$scope.selpillar = langResponse.selpillar;
        $scope.message = langResponse.eventcreated;
		$scope.entereventname = langResponse.entereventname;
		$scope.entervenue = langResponse.entervenue;
		$scope.seleventtype = langResponse.seleventtype;
		$scope.modalTitle = langResponse.thankyou;
    });
    
    $scope.event = {
        lastmodifiedtime: new Date(),
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        facilityId: $window.sessionStorage.UserEmployeeId,
        facility: $window.sessionStorage.coorgId,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        deleteflag: false
    };
    $scope.auditlog = {
        modifiedbyroleid: $window.sessionStorage.roleId,
        modifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        entityroleid: 54,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        facility: $window.sessionStorage.coorgId,
        facilityId: $window.sessionStorage.UserEmployeeId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId
    };
    $scope.todate = new Date();
    //console.log('$window.sessionStorage.UserEmployeeId', $window.sessionStorage.UserEmployeeId);
    //console.log('$window.sessionStorage.coorgId', $window.sessionStorage.coorgId);
    //console.log('$window.sessionStorage.userId', $window.sessionStorage.userId);
    /****************************************************** INDEX *******************************************/
    /* $scope.EventNameFilter = function () {
         $scope.backgroundCol1 = "blue";
         $scope.backgroundCol2 = "";
         $scope.backgroundCol3 = "";
         $scope.evntype = Restangular.all('eventtypes').getList().then(function (part1) {
             $scope.eventtypes = part1;
             $scope.evnt = Restangular.all('events?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=name%20ASC').getList().then(function (part) {
                 $scope.events = part;
                 $scope.modalInstanceLoad.close();
                 angular.forEach($scope.events, function (member, index) {
                     member.index = index + 1;
                     for (var n = 0; n < $scope.eventtypes.length; n++) {
                         if (member.eventype == $scope.eventtypes[n].id) {
                             member.EventType = $scope.eventtypes[n];
                             member.filterDate = new Date(member.datetime.split("T")[0]);
                             break;
                         }
                     }
                     member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                     $scope.TotalData = [];
                     $scope.TotalData.push(member);
                 });
                 console.log("$scope.events", $scope.events);
             });
         });
     }
     $scope.VenueFilter = function () {
         $scope.backgroundCol1 = "";
         $scope.backgroundCol2 = "blue";
         $scope.backgroundCol3 = "";
         $scope.evntype = Restangular.all('eventtypes').getList().then(function (part1) {
             $scope.eventtypes = part1;
             $scope.evnt = Restangular.all('events?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=venue%20ASC').getList().then(function (part) {
                 $scope.events = part;
                 $scope.modalInstanceLoad.close();
                 angular.forEach($scope.events, function (member, index) {
                     member.index = index + 1;
                     for (var n = 0; n < $scope.eventtypes.length; n++) {
                         if (member.eventype == $scope.eventtypes[n].id) {
                             member.EventType = $scope.eventtypes[n];
                             break;
                         }
                     }
                     member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                     $scope.TotalData = [];
                     $scope.TotalData.push(member);
                 });
             });
         });
     }
     $scope.DateFilter = function () {
         $scope.backgroundCol1 = "";
         $scope.backgroundCol2 = "";
         $scope.backgroundCol3 = "blue";
         $scope.evntype = Restangular.all('eventtypes').getList().then(function (part1) {
             $scope.eventtypes = part1;
             $scope.evnt = Restangular.all('events?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=datetime%20ASC').getList().then(function (part) {
                 $scope.events = part;
                 $scope.modalInstanceLoad.close();
                 angular.forEach($scope.events, function (member, index) {
                     member.index = index + 1;
                     for (var n = 0; n < $scope.eventtypes.length; n++) {
                         if (member.eventype == $scope.eventtypes[n].id) {
                             member.EventType = $scope.eventtypes[n];
                             break;
                         }
                     }
                     member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                     $scope.TotalData = [];
                     $scope.TotalData.push(member);
                 });
             });
         });
     }*/
    $scope.evntype = Restangular.all('eventtypes').getList().then(function (part1) {
        $scope.eventtypes = part1;
        $scope.evnt = Restangular.all('events?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=datetime%20ASC').getList().then(function (part) {
            $scope.events = part;
            $scope.modalInstanceLoad.close();
            angular.forEach($scope.events, function (member, index) {
                member.index = index + 1;
                for (var n = 0; n < $scope.eventtypes.length; n++) {
                    if (member.eventype == $scope.eventtypes[n].id) {
                        member.EventType = $scope.eventtypes[n];
                        member.filterDate = new Date(member.datetime.split("T")[0]);
                        break;
                    }
                }
                member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                $scope.TotalData = [];
                $scope.TotalData.push(member);
            });
        });
    });
    /********************/
    /* $scope.sortingOrder = sortingOrder;
     $scope.reverse = false;

     $scope.sort_by = function (newSortingOrder) {
         if ($scope.sortingOrder == newSortingOrder)
             $scope.reverse = !$scope.reverse;

         $scope.sortingOrder = newSortingOrder;


         // icon setup
         /* $('th i').each(function () {
              // icon reset
              $(this).removeClass().addClass('icon-sort');
          });
          if ($scope.reverse)
              $('th.' + new_sorting_order + ' i').removeClass().addClass('icon-chevron-up');
          else
              $('th.' + new_sorting_order + ' i').removeClass().addClass('icon-chevron-down');*/
    //};

    $scope.sort = {
        active: '',
        descending: undefined
    }

    $scope.changeSorting = function (column) {

        var sort = $scope.sort;

        if (sort.active == column) {
            sort.descending = !sort.descending;

        } else {
            sort.active = column;
            sort.descending = false;
        }
    };

    $scope.getIcon = function (column) {

        var sort = $scope.sort;

        if (sort.active == column) {
            return sort.descending ? 'fa-sort-up' : 'fa-sort-desc';
        }
    }



    /* $scope.sortcolumn = 'name';
     $scope.reverseSort = false;

     $scope.sortData = function (column) {
         $scope.reverseSort = ($scope.sortcolumn == column) ? !$scope.reverseSort : flase;
         $scope.sortcolumn == column;
     }
    $scope.getsortClass = function (newSortingOrder) {
        if ($scope.sortingOrder == newSortingOrder) {
            return $scope.reverse ? 'arrow-up' : 'arrow-down';
        }
        return '';
    }*/

    /*****************/


    /*************************************************** Organised By **************************/
    if ($window.sessionStorage.roleId == 5) {
        Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fwrResponse) {
            $scope.fieldworkers = fwrResponse;
        });
        $scope.Organisedcomembers = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + $window.sessionStorage.UserEmployeeId + ',41]}}}').getList().$object;
    } else {
        Restangular.all('fieldworkers?filter[where][id]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (fwrResponse2) {
            $scope.fieldworkers = fwrResponse2;
        });
        Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fwrResponse3) {
            $scope.Organisedcomembers = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + fwrResponse3.lastmodifiedby + ',41]}}}').getList().$object;
        });
    }
    /********************************************* SAVE *******************************************/
    $scope.validatestring = '';
    $scope.createDisabled = false;
    $scope.SaveEvent = function () {
        //$scope.eventdataModal = !$scope.eventdataModal;
        if ($scope.event.datetime == '' || $scope.event.datetime == null) {
            $scope.validatestring = $scope.validatestring + 'Plese Enter Event Date';
        } else
        if ($scope.event.name == '' || $scope.event.name == null) {
            $scope.validatestring = $scope.validatestring + $scope.entereventname;
        } else if ($scope.event.venue == '' || $scope.event.venue == null) {
            $scope.validatestring = $scope.validatestring + $scope.entervenue;
        } else if ($scope.event.pillarid == '' || $scope.event.pillarid == null) {
            $scope.validatestring = $scope.validatestring + $scope.selpillar;
        } else if ($scope.event.eventype == '' || $scope.event.eventype == null) {
            $scope.validatestring = $scope.validatestring + $scope.seleventtype;
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
            //$scope.eventdataModal = !$scope.eventdataModal;
        } else {
			$scope.openOneAlert();
            $scope.createDisabled = true;
            Restangular.all('events').post($scope.event).then(function (response) {
                $scope.auditlog.entityid = response.id;
                var respdate = $filter('date')(response.datetime, 'dd-MMMM-yyyy');
                $scope.auditlog.description = 'Event Created With Following Details: ' + 'Event Name  - ' + response.name + ' , ' + 'Venue - ' + response.venue + ' , ' + 'Pillar - ' + response.pillarid + ' , ' + 'Event Type - ' + response.eventype + ' , ' + 'Organized By - ' + response.organisedby + ' , ' + 'Date - ' + respdate;
                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                    console.log('responseaudit', responseaudit);
                    window.location = '/events';
                });
            });
        }
    };
	 $scope.openOneAlert = function () {
         console.log('Me Calling')
         $scope.modalOneAlert = $modal.open({
             animation: true
             , templateUrl: 'template/AlertModal.html'
             , scope: $scope
             , backdrop: 'static'
             , keyboard: false
             , size: 'sm'
             , windowClass: 'modal-sucess '
         });
     };
    $scope.eventdataModal = false;
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    $scope.pillars = Restangular.all('pillars').getList().$object;
    $scope.PillarChanged = function (pillarid) {
        $scope.eventtypes = Restangular.all('eventtypes?filter[where][pillarid]=' + pillarid).getList().$object;
    };
    /************************************************** DELETE *******************************************/
    $scope.Delete = function (id) {
            var respdate = $filter('date')(new Date(), 'dd-MMMM-yyyy');
            $scope.auditlog.description = 'Event Deleted With Following Details: ' + 'Deleted By UserId - ' + $window.sessionStorage.userId + ', ' + 'RoleId - ' + $window.sessionStorage.roleId + ', ' + 'Date - ' + respdate;
            $scope.auditlog.entityid = id;
            Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                $scope.item = [{
                    deleteflag: true,
                    lastmodifiedtime: new Date(),
                    lastmodifiedby: $window.sessionStorage.UserEmployeeId
            }]
                Restangular.one('events/' + id).customPUT($scope.item[0]).then(function () {
                    $route.reload();
                });
            });
        }
        /********************************************** Watch Others **************************/
    $scope.documenthide = true;
    $scope.$watch('event.organisedby', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            if (newValue == 41) {
                $scope.documenthide = false;
            } else {
                $scope.documenthide = true;
                $scope.event.others = null;
            }
        }
    });
    //Datepicker settings start
    $scope.today = function () {
        $scope.event.datetime = new Date();
    };
    //console.log('$scope.event.datetime',new Date());
    $scope.today();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.dtmax = new Date();
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.open = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.opened = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end
});
