'use strict';

angular.module('secondarySalesApp')
	.controller('MentorCreateCtrl', function ($scope, Restangular, $filter, $timeout, $window, $routeParams, $location) {
		$scope.hideCreateButton = true;
		$scope.mentor = {
			deleteflag: false,
			usercreated: false,
			latitude: '',
			longitude: '',
			address: '',
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId
		};

		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.zones = Restangular.all('zones').getList().$object;

		$scope.userroles = Restangular.all('roles?filter={"where":{"id":{"inq":[3,4,16,17]}}}').getList().$object;
		/******************************** SAVE *******************************************/
		/*$scope.Save = function () {
			Restangular.all('mentors').post($scope.mentor).then(function (resp) {
				console.log('$scope.mentor', $scope.mentor);
				window.location = '/mentor';
			});
		};*/

		console.log('CurrRole', $scope.mentor.role)
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			if ($scope.mentor.role == '' || $scope.mentor.role == null) {
				$scope.validatestring = $scope.validatestring + 'Please Select a Role';
			} else if ($scope.mentor.name == '' || $scope.mentor.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";
			} else if ($scope.mentor.role != 16) {
				if ($scope.mentor.state == '' || $scope.mentor.state == null) {
					$scope.validatestring = $scope.validatestring + 'Please Select a State';
				}
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.all('mentors').post($scope.mentor).then(function () {
					window.location = '/mentor';
				});
			}
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};


		$scope.showMapModal = false;
		$scope.toggleMapModal = function () {
			$scope.mapcount = 0;
			var geocoder = new google.maps.Geocoder();

			function geocodePosition(pos) {
				geocoder.geocode({
					latLng: pos
				}, function (responses) {
					if (responses && responses.length > 0) {
						updateMarkerAddress(responses[0].formatted_address);
					} else {
						updateMarkerAddress('Cannot determine address at this location.');
					}
				});
			}

			function updateMarkerStatus(str) {
				document.getElementById('markerStatus').innerHTML = str;
			}

			function updateMarkerPosition(latLng) {
				$scope.mentor.latitude = latLng.lat() + ',' + latLng.lng();
				$scope.mentor.longitude = latLng.lng();

				document.getElementById('info').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');
			}

			function updateMarkerAddress(str) {
				document.getElementById('mapaddress').innerHTML = str;
			}
			var map;

			function initialize() {

				$scope.address = $scope.mentor.address;
				// console.log('$scope.address', $scope.address);
				$scope.latitude = 21.0000;
				$scope.longitude = 78.0000;
				if ($scope.address.length > 0) {
					var addressgeocoder = new google.maps.Geocoder();
					addressgeocoder.geocode({
						'address': $scope.address
					}, function (results, status) {

						if (status == google.maps.GeocoderStatus.OK) {
							$scope.latitude = parseInt(results[0].geometry.location.lat());
							$scope.longitude = parseInt(results[0].geometry.location.lng());
							//console.log($scope.latitude, $scope.longitude);

							var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
							map = new google.maps.Map(document.getElementById('mapCanvas'), {
								zoom: 4,
								center: new google.maps.LatLng($scope.latitude, $scope.longitude),
								mapTypeId: google.maps.MapTypeId.ROADMAP
							});
							var marker = new google.maps.Marker({
								position: latLng,
								title: 'Point A',
								map: map,
								draggable: true
							});

							// Update current position info.
							updateMarkerPosition(latLng);
							geocodePosition(latLng);

							// Add dragging event listeners.
							google.maps.event.addListener(marker, 'dragstart', function () {
								updateMarkerAddress('Dragging...');
							});

							google.maps.event.addListener(marker, 'drag', function () {
								updateMarkerStatus('Dragging...');
								updateMarkerPosition(marker.getPosition());
							});

							google.maps.event.addListener(marker, 'dragend', function () {
								updateMarkerStatus('Drag ended');
								geocodePosition(marker.getPosition());
							});
						}
					});
				} else {
					$scope.latitude = 21.0000;
					$scope.longitude = 78.0000;

					var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
					map = new google.maps.Map(document.getElementById('mapCanvas'), {
						zoom: 4,
						center: new google.maps.LatLng($scope.latitude, $scope.longitude),
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					var marker = new google.maps.Marker({
						position: latLng,
						title: 'Point A',
						map: map,
						draggable: true
					});

					// Update current position info.
					updateMarkerPosition(latLng);
					geocodePosition(latLng);

					// Add dragging event listeners.
					google.maps.event.addListener(marker, 'dragstart', function () {
						updateMarkerAddress('Dragging...');
					});

					google.maps.event.addListener(marker, 'drag', function () {
						updateMarkerStatus('Dragging...');
						updateMarkerPosition(marker.getPosition());
					});

					google.maps.event.addListener(marker, 'dragend', function () {
						updateMarkerStatus('Drag ended');
						geocodePosition(marker.getPosition());
					});

				}


			}

			// Onload handler to fire off the app.
			//google.maps.event.addDomListener(window, 'load', initialize);
			initialize();

			window.setTimeout(function () {
				google.maps.event.trigger(map, 'resize');
				map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
				map.setZoom(4);
			}, 1000);


			$scope.SaveMap = function () {
				$scope.showMapModal = !$scope.showMapModal;
				console.log($scope.reportincident);
			};

			//console.log('fdfd');
			$scope.showMapModal = !$scope.showMapModal;
		};

		$scope.CancelMap = function () {
			if ($scope.mapcount == 0) {
				$scope.showMapModal = !$scope.showMapModal;
				$scope.mapcount++;
			}
		};

		if ($routeParams.id) {
			$scope.message = 'Other Role has been Updated!';
			$scope.hideCreateButton = false;
			Restangular.one('mentors', $routeParams.id).get().then(function (partner) {
				$scope.original = partner;
				//$scope.getRoleId = partner.role;
				//console.log('$scope.getRoleId', $scope.getRoleId);
				$scope.mentor = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Other Role has been Created!';
		}


		Restangular.one('mentors', $routeParams.id).get().then(function (partner1) {
			$scope.getRoleId = partner1.role;
			console.log('$scope.getRoleId out', $scope.getRoleId);
		});

		$scope.Update = function () {
			console.log('$scope.getRoleId out2', $scope.getRoleId);
			document.getElementById('name').style.border = "";
			if ($scope.mentor.name == '' || $scope.mentor.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";


			}
			if ($scope.getRoleId != 16) {
				if ($scope.mentor.state == '' || $scope.mentor.state == null) {
					$scope.validatestring = $scope.validatestring + 'Please Select State';
				}
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.one('mentors', $routeParams.id).customPUT($scope.mentor).then(function (res) {
					window.location = '/mentor';
				});
			}
		}

	});
