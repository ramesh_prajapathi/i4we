'use strict';

angular.module('secondarySalesApp')
	.controller('purposeofmeetingsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/purposeofmeetings/create' || $location.path() === '/purposeofmeetings/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/purposeofmeetings/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/purposeofmeetings/create' || $location.path() === '/purposeofmeetings/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/purposeofmeetings/create' || $location.path() === '/purposeofmeetings/' + $routeParams.id;
			return visible;
		};

		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
		}

		if ($window.sessionStorage.prviousLocation != "partials/purposeofmeeting") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};


		/*********/

		$scope.submitpurposeofmeetings = Restangular.all('purposeofmeetings').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'Purpose of meeting has been Updated!';
			Restangular.one('purposeofmeetings', $routeParams.id).get().then(function (purposeofmeeting) {
				$scope.original = purposeofmeeting;
				$scope.purposeofmeeting = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Purpose of meeting has been Created!';
		}
		$scope.searchpurposeofmeeting = $scope.name;

		/************************************* INDEX *******************************************/
		$scope.zn = Restangular.all('purposeofmeetings?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.purposeofmeetings = zn;
			angular.forEach($scope.purposeofmeetings, function (member, index) {
				member.index = index + 1;
			});
		});

		/************************************ SAVE *******************************************/
		$scope.purposeofmeeting = {
			name: '',
			deleteflag: false
		};
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Savepurposeofmeeting = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.purposeofmeeting.name == '' || $scope.purposeofmeeting.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.purposeofmeeting.hnname == '' || $scope.purposeofmeeting.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.purposeofmeeting.knname == '' || $scope.purposeofmeeting.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.purposeofmeeting.taname == '' || $scope.purposeofmeeting.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.purposeofmeeting.tename == '' || $scope.purposeofmeeting.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.purposeofmeeting.mrname == '' || $scope.purposeofmeeting.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitpurposeofmeetings.post($scope.purposeofmeeting).then(function () {
					console.log('purposeofmeeting Saved');
					window.location = '/purposeofmeetings';
				});
			};
		};
		/********************************* UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Updatepurposeofmeeting = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.purposeofmeeting.name == '' || $scope.purposeofmeeting.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.purposeofmeeting.hnname == '' || $scope.purposeofmeeting.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.purposeofmeeting.knname == '' || $scope.purposeofmeeting.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.purposeofmeeting.taname == '' || $scope.purposeofmeeting.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.purposeofmeeting.tename == '' || $scope.purposeofmeeting.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.purposeofmeeting.mrname == '' || $scope.purposeofmeeting.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitpurposeofmeetings.customPUT($scope.purposeofmeeting).then(function () {
					console.log('purposeofmeeting Saved');
					window.location = '/purposeofmeetings';
				});
			}
		};
		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/********************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('purposeofmeetings/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

	});
