'use strict';
angular.module('secondarySalesApp')
    .controller('TrendChartsCtrl', function ($scope, $http, Restangular, $window, $timeout, $location, baseUrl, $rootScope, $idle, $modal, $filter, $route) {
var curr_data;
       // d3.json('//i4we-api.herokuapp.com/api/v1/hhtrendviews?access_token='+$window.sessionStorage.accessToken, function(day_data) {
  
var margin = {top: 40, right: 20, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

var formatPercent = d3.format(".0%");

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], .1);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

var hhtrendtip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>No Of HH Added:</strong> <span style='color:red'>" + d.value + "</span>";
  })

var hhtrendsvg = d3.select(".hhtrend").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

hhtrendsvg.call(hhtrendtip);

d3.json('//i4we-api.herokuapp.com/api/v1/hhtrendviews?access_token='+$window.sessionStorage.accessToken,function(data) {
    console.log("data",data);
  x.domain(data.map(function(d) { return d.date; }));
  y.domain([0, d3.max(data, function(d) { return +d.value; })]);

  hhtrendsvg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  hhtrendsvg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("No Of HH Added");

  hhtrendsvg.selectAll(".bar")
      .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.date); })
      .attr("width", x.rangeBand())
      .attr("y", function(d) { return y(d.value); })
      .attr("height", function(d) { return height - y(d.value); })
      .on('mouseover', hhtrendtip.show)
      .on('mouseout', hhtrendtip.hide)

});



var opdtrendtip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>No Of OPD Added:</strong> <span style='color:red'>" + d.value + "</span>";
  })

var opdtrendsvg = d3.select(".opdtrend").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

opdtrendsvg.call(opdtrendtip);

d3.json('//i4we-api.herokuapp.com/api/v1/opdtrendviews?access_token='+$window.sessionStorage.accessToken,function(data) {
    console.log("data",data);
  x.domain(data.map(function(d) { return d.date; }));
  y.domain([0, d3.max(data, function(d) { return +d.value; })]);

  opdtrendsvg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  opdtrendsvg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("No Of OPD Added");

  opdtrendsvg.selectAll(".bar")
      .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.date); })
      .attr("width", x.rangeBand())
      .attr("y", function(d) { return y(d.value); })
      .attr("height", function(d) { return height - y(d.value); })
      .on('mouseover', opdtrendtip.show)
      .on('mouseout', opdtrendtip.hide)

});


function type(d) {
  d.value = +d.value;
  return d;
}

    });