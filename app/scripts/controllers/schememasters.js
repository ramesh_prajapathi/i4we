'use strict';

angular.module('secondarySalesApp')
    .controller('SchemeMastersCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $modal) {

        $scope.DisableLang = false;
        $scope.disabledState = false;

        $scope.showForm = function () {
            var visible = $location.path() === '/scheme/create' || $location.path() === '/scheme/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/scheme/create';
                return visible;
            }
        };

        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/scheme/create' || $location.path() === '/scheme/edit/' + $routeParams.id;
            return visible;
        };

        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/scheme/create' || $location.path() === '/scheme/edit/' + $routeParams.id;
            return visible;
        };
        /***********************************************************************************/
        $scope.ShowAllDetails = false;
        $scope.ShowAllDetailsDiv = false;
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute1 == null || $window.sessionStorage.myRoute1 == undefined) {
            $window.sessionStorage.myRoute1 = null;
            $window.sessionStorage.myRoute1_currentPage = 1;
            $window.sessionStorage.myRoute1_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute1_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute1_currentPage;
            $scope.pillarId = $window.sessionStorage.myRoute1;
        }

        if ($window.sessionStorage.prviousLocation != "partials/schemes-list") {
            $window.sessionStorage.myRoute1 = '';
            $window.sessionStorage.myRoute1_currentPage = 1;
            $window.sessionStorage.myRoute1_currentPagesize = 25;
        }


        $scope.currentpage = $window.sessionStorage.myRoute1_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute1_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute1_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute1_currentPagesize = mypage;
        };

        /*.................................... INDEX ................................................*/
        $scope.st = Restangular.all('states?filter[where][deleteFlag]=false').getList().then(function (znRes) {
            $scope.states = znRes;
            $scope.stateId = $window.sessionStorage.myRoute1;
        });

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        $scope.languagesdisplay = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        //$scope.schememasters = {};
        //$scope.stateId = '';
        $scope.zonalid = '';
        $scope.$watch('stateId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {

                return;
            } else {
                $scope.topfive = false;
                $scope.zonalid = +newValue;
                $window.sessionStorage.myRoute1 = newValue;
                //$window.sessionStorage.myRoute = newValue;
                Restangular.all('schemes?filter[where][deleteFlag]=false&filter[where][language]=1&filter[where][schemeState]=' + newValue).getList().then(function (con) {
                    $scope.Displayschememasters = con;
                    //  console.log('$scope.Displayschememasters', $scope.Displayschememasters);
                    angular.forEach($scope.Displayschememasters, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.languagesdisplay.length; a++) {
                            if (member.language == $scope.languagesdisplay[a].id) {
                                member.languagename = $scope.languagesdisplay[a].name;
                                break;
                            }
                        }
                        for (var b = 0; b < $scope.states.length; b++) {
                            if (member.schemeState == $scope.states[b].id) {
                                member.schemeStatename = $scope.states[b].name;
                                break;
                            }
                        }
                        // console.log(member);
                    });
                });
            }
        });

        $scope.$watch('topfive', function (newValue, oldValue) {
            //console.log('$scope.stateId',$scope.stateId);
            if ($scope.stateId == '' || $scope.stateId == undefined) {
                return;
            } else {
                //console.log(newValue);
                if (newValue == true) {
                    // $scope.stateId = +newValue;
                    //$window.sessionStorage.myRoute = newValue;
                    $scope.con1 = Restangular.all('schemes?filter[where][deleteFlag]=false&filter[where][language]=1&filter[where][and][0][topscheme][lt]=6&filter[where][and][1][topscheme][gt]=0&filter[where][schemeState]=' + $scope.stateId + '&filter[order]=topscheme%20ASC').getList().then(function (con1) {
                        $scope.Displayschememasters = con1;
                        angular.forEach($scope.Displayschememasters, function (member, index) {
                            member.index = index + 1;

                            for (var a = 0; a < $scope.languagesdisplay.length; a++) {
                                if (member.language == $scope.languagesdisplay[a].id) {
                                    member.languagename = $scope.languagesdisplay[a].name;
                                    break;
                                }
                            }
                            for (var b = 0; b < $scope.states.length; b++) {
                                if (member.schemeState == $scope.states[b].id) {
                                    member.schemeStatename = $scope.states[b].name;
                                    break;
                                }
                            }

                        });
                    });

                } else {
                    $scope.con2 = Restangular.all('schemes?filter[where][deleteFlag]=false&filter[where][language]=1&filter[where][schemeState]=' + $scope.stateId).getList().then(function (con2) {
                        $scope.Displayschememasters = con2;
                        angular.forEach($scope.Displayschememasters, function (member, index) {
                            member.index = index + 1;

                            for (var a = 0; a < $scope.languagesdisplay.length; a++) {
                                if (member.language == $scope.languagesdisplay[a].id) {
                                    member.languagename = $scope.languagesdisplay[a].name;
                                    break;
                                }
                            }
                            for (var b = 0; b < $scope.states.length; b++) {
                                if (member.schemeState == $scope.states[b].id) {
                                    member.schemeStatename = $scope.states[b].name;
                                    break;
                                }
                            }
                            //  console.log(member);
                        });
                    });

                }
            }
        });

        $scope.submitschememasters = Restangular.all('schemes').getList().$object;

        $scope.schememaster = {
            name: '',
            language: 1,
            lastmodifiedtime: new Date(),
            lastmodifiedby: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            deleteFlag: false
        };

        $scope.showenglishLang = false;
        $scope.disabledFields = false;

        $scope.$watch('schememaster.language', function (newValue, oldValue) {

            $scope.genders = Restangular.all('genders?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().$object;

            $scope.educations = Restangular.all('educations?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().$object;

            $scope.schemecategories = Restangular.all('schemecategories?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().$object;

            $scope.agegroups = Restangular.all('agegroups?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().$object;

            $scope.healthstatuses = Restangular.all('healthstatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().$object;

            $scope.minoritystatuses = Restangular.all('minoritystatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().$object;

            $scope.socialstatuses = Restangular.all('socialstatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().$object;

            $scope.occupationstatuses = Restangular.all('occupationstatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().$object;

            $scope.incomestatuses = Restangular.all('incomestatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().$object;

            $scope.locationtypes = Restangular.all('locationtypes?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().$object;

            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = true;
                    $scope.disabledFields = true;
                    // $scope.disabledState = true;
                } else {
                    $scope.showenglishLang = false;
                    $scope.disabledFields = false;
                    // $scope.disabledState = false;
                }
            }
        });

        $scope.$watch('schememaster.parentId', function (newValue, oldValue) {
            if (newValue === '' || newValue === null || newValue === oldValue) {
                return;
            } else {
                Restangular.one('schemes', newValue).get().then(function (sch) {
                    // console.log();
                    $scope.schememaster.schemeState = sch.schemeState;
                    $scope.schememaster.category = sch.category.split(",");
                    $scope.schememaster.objectives = sch.objectives;
                    $scope.schememaster.department = sch.department;
                    $scope.schememaster.agegroup = sch.agegroup.split(",");
                    $scope.schememaster.gender = sch.gender;
                    $scope.schememaster.educationstatus = sch.educationstatus;
                    $scope.schememaster.healthstatus = sch.healthstatus;
                    $scope.schememaster.minoritystatus = sch.minoritystatus;
                    $scope.schememaster.socialstatus = sch.socialstatus;
                    $scope.schememaster.occupationstatus = sch.occupationstatus;
                    $scope.schememaster.locationtype = sch.locationtype;
                    $scope.schememaster.incomestatus = sch.incomestatus;
                    $scope.schememaster.minimumincome = sch.minimumincome;
                    $scope.schememaster.maximumincome = sch.maximumincome;
                    $scope.schememaster.anyothercriteria = sch.anyothercriteria;
                    // console.log('$scope.schememaster', $scope.schememaster);
                });
            }
        });

        $scope.$watch('schememaster.schemeState', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {
                Restangular.all('schemes?filter[where][deleteFlag]=false&filter[where][language]=1' + '&filter[where][schemeState]=' + newValue).getList().then(function (schme) {
                    $scope.englishschemes = schme;
                });
            }
        });

        $scope.$watch('schememaster.name', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id) {
                return;
            } else {
                angular.forEach($scope.rowArray, function (member, index) {
                    member.index = index;
                    member.name = newValue;
                });
            }
        });

        $scope.$watch('schememaster.localname', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id) {
                return;
            } else {
                angular.forEach($scope.rowArray, function (member, index) {
                    member.index = index;
                    member.localname = newValue;
                });
            }
        });

        $scope.rowArray = [];

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablang) {
            $scope.tablanguages = tablang;
            $scope.tablanguages.splice(0, 1);
            $scope.currValue = 0;

            angular.forEach($scope.tablanguages, function (member, index) {
                member.index = index;

                if (!$routeParams.id) {
                    $scope.rowArray.push({
                        language: member.id,
                        name: ''
                    });
                }

                if (member.index == 0) {
                    member.active = true;
                    member.visible = true;
                } else {
                    member.active = false;
                    member.visible = false;
                }
            });
        });

        $scope.tabClick = function (row) {
            $scope.tablanguages[row.index].visible = true;
            if (row.index != 0) {
                $scope.tablanguages[0].visible = false;
            }
            $scope.currValue = row.index;
        };

        /******************************* INDEX *****************************************/

        $scope.getGender = function (genderId) {
            return Restangular.one('genders', genderId).get().$object;
        };

        $scope.showDetails = function () {
            $scope.ShowAllDetails = !$scope.ShowAllDetails;
        };

        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {

            $scope.item = [{
                deleteFlag: true
            }]

            Restangular.one('schemes/' + id).customPUT($scope.item[0]).then(function () {
                Restangular.all('schemes?filter[where][parentId]=' + id).getList().then(function (stypes) {
                    $scope.deleteRows = stypes;
                    $scope.DeleteFunc();
                });
            });
        };

        $scope.updateFlag = {
            deleteFlag: true
        };

        $scope.DeleteCount = 0;

        $scope.DeleteFunc = function (id) {

            if ($scope.DeleteCount < $scope.deleteRows.length) {

                Restangular.one('schemes/' + $scope.deleteRows[$scope.DeleteCount].id).customPUT($scope.updateFlag).then(function () {
                    $scope.DeleteCount++;
                    $scope.DeleteFunc();
                });

            } else {
                $route.reload();
            }
        };

        $scope.validatestring = '';

        $scope.Save = function () {

            if ($scope.schememaster.name == '' || $scope.schememaster.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name';
            } else if ($scope.schememaster.schemeState == '' || $scope.schememaster.schemeState == null) {
                $scope.validatestring = $scope.validatestring + 'Please select a State or Union Territory';
            } else if ($scope.schememaster.name == '' || $scope.schememaster.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name';
            } else if ($scope.schememaster.localname == '' || $scope.schememaster.localname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme local name';
            } else if ($scope.schememaster.category == '' || $scope.schememaster.category == null) {
                $scope.validatestring = $scope.validatestring + 'Please select category';
            } else if ($scope.schememaster.agegroup == '' || $scope.schememaster.agegroup == null) {
                $scope.validatestring = $scope.validatestring + 'Please select age group';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.toggleLoading();

                $scope.submitDisable = true;

                $scope.schememaster.parentFlag = true;
                $scope.schememaster.createdDate = new Date();
                $scope.schememaster.createdByRole = $window.sessionStorage.roleId;
                $scope.schememaster.createdBy = $window.sessionStorage.userId;
                $scope.schememaster.lastModifiedBy = $window.sessionStorage.userId;
                $scope.schememaster.lastModifiedDate = new Date();
                $scope.schememaster.lastModifiedByRole = $window.sessionStorage.roleId;

                Restangular.all('schemes').post($scope.schememaster).then(function (response) {
                    $scope.langFunc(response);
                });
            }
        };

        $scope.langcount = 0;

        $scope.langFunc = function (respObject) {

            if ($scope.langcount < $scope.rowArray.length) {

                $scope.rowArray[$scope.langcount].parentId = respObject.id;
                $scope.rowArray[$scope.langcount].schemeState = respObject.schemeState;
                $scope.rowArray[$scope.langcount].category = respObject.category;
                $scope.rowArray[$scope.langcount].objectives = respObject.objectives;
                $scope.rowArray[$scope.langcount].department = respObject.department;
                $scope.rowArray[$scope.langcount].agegroup = respObject.agegroup;
                $scope.rowArray[$scope.langcount].gender = respObject.gender;
                $scope.rowArray[$scope.langcount].educationstatus = respObject.educationstatus;
                $scope.rowArray[$scope.langcount].healthstatus = respObject.healthstatus;
                $scope.rowArray[$scope.langcount].minoritystatus = respObject.minoritystatus;
                $scope.rowArray[$scope.langcount].socialstatus = respObject.socialstatus;
                $scope.rowArray[$scope.langcount].occupationstatus = respObject.occupationstatus;
                $scope.rowArray[$scope.langcount].locationtype = respObject.locationtype;
                $scope.rowArray[$scope.langcount].incomestatus = respObject.incomestatus;
                $scope.rowArray[$scope.langcount].minimumincome = respObject.minimumincome;
                $scope.rowArray[$scope.langcount].maximumincome = respObject.maximumincome;
                $scope.rowArray[$scope.langcount].anyothercriteria = respObject.anyothercriteria;
                $scope.rowArray[$scope.langcount].parentFlag = false;
                $scope.rowArray[$scope.langcount].deleteFlag = false;
                $scope.rowArray[$scope.langcount].createdDate = new Date();
                $scope.rowArray[$scope.langcount].createdByRole = $window.sessionStorage.roleId;
                $scope.rowArray[$scope.langcount].createdBy = $window.sessionStorage.userId;
                $scope.rowArray[$scope.langcount].lastModifiedBy = $window.sessionStorage.userId;
                $scope.rowArray[$scope.langcount].lastModifiedDate = new Date();
                $scope.rowArray[$scope.langcount].lastModifiedByRole = $window.sessionStorage.roleId;

                Restangular.all('schemes').post($scope.rowArray[$scope.langcount]).then(function () {
                    $scope.langcount++;
                    $scope.langFunc(respObject);
                });

            } else {

                $scope.modalInstanceLoad.close();
                $scope.schememasterdataModal = !$scope.schememasterdataModal;

                setInterval(function () {
                    window.location = '/schemes-list';
                }, 350);
            }
        };


        $scope.modalTitle = 'Thank You';
        $scope.message = 'Scheme has been Created!';

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.$watch('schememaster.topscheme', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {
                $scope.alterTopScheme = false;
                if ($scope.schememaster.schemeState == undefined || $scope.schememaster.schemeState == '') {
                    alert('Select State');
                    $scope.schememaster.topscheme = '';
                } else {
                    Restangular.all('schemes?filter[where][schemeState]=' + $scope.schememaster.schemeState + '&filter[where][topscheme]=' + newValue).getList().then(function (topscheme) {
                        console.log('topscheme', topscheme);
                        if (topscheme.length > 0 && topscheme[0].id != $scope.schememaster.id) {
                            $scope.openSp();
                            $scope.schememastertopscheme = oldValue;
                            $scope.schemeToAlter = topscheme[0];
                        }
                    });
                }
            }
        });

        ///////////////////////////////////////Modal////////////////////////////////////////
        $scope.openSp = function () {
            $scope.modalSP = $modal.open({
                animation: true,
                templateUrl: 'template/sp.html',
                scope: $scope,
                backdrop: 'static'

            });
        };
        $scope.alterTopScheme = false;
        $scope.SaveSP = function () {
            $scope.alterTopScheme = true;
            $scope.modalSP.close();
        };

        $scope.okSp = function () {
            $scope.schememaster.topscheme = $scope.schememastertopscheme;
            $scope.modalSP.close();
        };


        ///////////////////////////////////////Modal////////////////////////////////////////
    });
