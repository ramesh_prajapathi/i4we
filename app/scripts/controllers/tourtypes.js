'use strict';

angular.module('secondarySalesApp')
  .controller('TourtypeCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams) {
    /*********/

    $scope.showForm = function(){
      var visible = $location.path() === '/tourtypes/create' || $location.path() === '/tourtypes/' + $routeParams.id;
      return visible;
    };
    
    $scope.isCreateView = function(){
      if($scope.showForm()){
        var visible = $location.path() === '/tourtypes/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function (){
        var visible = $location.path() === '/tourtypes/create' || $location.path() === '/tourtypes/' + $routeParams.id;
        return visible;
      };

    $scope.testf = function(){
      console.log('testf');
    };

    /*********/

    $scope.tourtypes = Restangular.all('tourtypes').getList().$object;
    
    if($routeParams.id){
      Restangular.one('tourtypes', $routeParams.id).get().then(function(tourtype){
				$scope.original = tourtype;
				$scope.tourtype = Restangular.copy($scope.original);
      });
    }

    $scope.cbGridConfig = [{
        'header' : 'Tourtype',
        'column' : 'name'
      }];
  });