'use strict';

angular.module('secondarySalesApp')

    .controller('HouseholdCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $rootScope) {

        $rootScope.clickFW();
        $scope.HHLanguage = {};
        $scope.multiLang = Restangular.one('householdlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.HHLanguage = langResponse[0];

        });


        $scope.someFocusVariable = true;

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }


        //        if ($window.sessionStorage.roleId + "" === "7") {
        //            $scope.hideAddBtn = false;
        //
        //        } else {
        //            $scope.hideAddBtn = true;
        //            
        //        }
        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/households-form") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
        /*************************************************************************************************/
        $scope.auditlog = {
            modifiedbyroleid: $window.sessionStorage.roleId,
            modifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            entityroleid: 49,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,

        };

        $scope.ssgThisHHBelongs = [];

        $scope.householdsArray = [];

        $scope.gendersdsplsy = Restangular.all('genders?filter[where][deleteFlag]=false').getList().$object;
        $scope.occupationsdsplsy = Restangular.all('occupations?filter[where][deleteFlag]=false').getList().$object;
        $scope.castesdsply = Restangular.all('castes?filter[where][deleteFlag]=false').getList().$object;
        $scope.religionsdsplsy = Restangular.all('religions?filter[where][deleteFlag]=false').getList().$object;
        $scope.migrantsdsplsy = Restangular.all('migrants?filter[where][deleteFlag]=false').getList().$object;
        $scope.bplstatusesdsplsy = Restangular.all('bplstatuses?filter[where][deleteFlag]=false').getList().$object;
        $scope.relationwithhhsdsplsy = Restangular.all('relationwithhhs?filter[where][deleteFlag]=false').getList().$object;
        $scope.literacylevelsdsply = Restangular.all('literacylevels?filter[where][deleteFlag]=false').getList().$object;
        $scope.nameofschoolsdsply = Restangular.all('nameofschools?filter[where][deleteFlag]=false').getList().$object;
        $scope.nameoffactoriesdsply = Restangular.all('nameoffactories?filter[where][deleteFlag]=false').getList().$object;
        $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;
        $scope.roledsply = Restangular.all('roles').getList().$object;
        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;
        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;
        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;
        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;

        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.hideforHf = true;
            $scope.Showforsite = false;

            $scope.Showforsite1 = false;
            $scope.hideAddBtn = true;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.hhfilterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.ShgfilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}}]}}';

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
            });
        } else if ($window.sessionStorage.roleId + "" === "7") {
            $scope.hideAddBtn = false;
            $scope.Showforsite = false;
            $scope.Showforsite1 = true;
            $scope.hideforHf = false;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.hhfilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.ShgfilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}}]}}';

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
            });
        } else {

            $scope.hideforHf = false;
            $scope.Showforsite = true;
            $scope.Showforsite1 = true;
            $scope.hideAddBtn = true;
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.hhfilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.ShgfilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}}]}}';

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
            });

        }

        Restangular.all($scope.memberFilterCall).getList().then(function (mrslist) {
            // console.log('mrslist', mrslist);
            Restangular.all($scope.hhfilterCall).getList().then(function (hhslist) {

                $scope.hhslist = hhslist;

                Restangular.all($scope.ShgfilterCall).getList().then(function (shglst) {
                    $scope.shgsdsply = shglst;

                    $scope.sortedArray = mrslist.sort(function (a, b) {
                        return a.individualId > b.individualId ? 1 : a.individualId < b.individualId ? -1 : 0
                    })

                    angular.forEach($scope.sortedArray, function (member, index) {
                        //  console.log('member', member);
                        member.index = index + 1;
                        if (member.healthyDays == null) {
                            member.healthyDays1 = 0;
                        } else {
                            member.healthyDays1 = member.healthyDays;
                        }

                        var data = _.findWhere($scope.hhslist, {
                            id: member.parentId
                        });

                        //console.log('data', data);

                        if (data != undefined) {
                            if (data.ssgThisHHBelongs == '' || data.ssgThisHHBelongs == null || data.ssgThisHHBelongs == "0" || data.ssgThisHHBelongs == 0) {
                                member.isSHg = 'No';
                            } else {
                                member.isSHg = 'Yes';
                            }
                        } else {
                            member.isSHg = 'No';
                        }


                        if (member.quickaddFlag == true) {
                            member.isquickadd = 'Yes';
                        } else {
                            member.isquickadd = 'No';
                        }


                        /** if(member.shgId == null || member.shgId == 0 || member.shgId == ''){
                            member.isSHg = 'No';
                        } else{
                            member.isSHg = 'Yes';
                        } **/

                        member.dob = $filter('date')(member.dob, 'dd/MM/yyyy');
                        member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                        member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                        if (member.aadharNumber == null) {
                            member.aadharNumber = '';
                        }

                        if (member.esiNumber == null) {
                            member.esiNumber = '';
                        }

                        if (member.mobile == null) {
                            member.mobile = '';
                        }

                        if (member.email == null) {
                            member.email = '';
                        }

                        if (member.lastModifiedDate == null) {
                            member.lastModifiedDate = '';
                        }

                        var data2 = $scope.gendersdsplsy.filter(function (arr) {
                            return arr.id == member.gender
                        })[0];

                        if (data2 != undefined) {
                            member.gendername = data2.name;
                        }

                        var data3 = $scope.occupationsdsplsy.filter(function (arr) {
                            return arr.id == member.occupation
                        })[0];

                        if (data3 != undefined) {
                            member.occupationname = data3.name;
                        }

                        var data4 = $scope.castesdsply.filter(function (arr) {
                            return arr.id == member.caste
                        })[0];

                        if (data4 != undefined) {
                            member.castename = data4.name;
                        }

                        var data5 = $scope.religionsdsplsy.filter(function (arr) {
                            return arr.id == member.religion
                        })[0];

                        if (data5 != undefined) {
                            member.religionname = data5.name;
                        }

                        var data6 = $scope.migrantsdsplsy.filter(function (arr) {
                            return arr.id == member.migrant
                        })[0];

                        if (data6 != undefined) {
                            member.migrantname = data6.name;
                        }

                        var data7 = $scope.bplstatusesdsplsy.filter(function (arr) {
                            return arr.id == member.bplStatus
                        })[0];

                        if (data7 != undefined) {
                            member.bplstatusname = data7.name;
                        }

                        var data8 = $scope.relationwithhhsdsplsy.filter(function (arr) {
                            return arr.id == member.relation
                        })[0];

                        if (data8 != undefined) {
                            member.relationname = data8.name;
                        } else if (data8 == null) {
                            member.relationname = '';
                        }

                        var data9 = $scope.literacylevelsdsply.filter(function (arr) {
                            return arr.id == member.literacyLevel
                        })[0];

                        if (data9 != undefined) {
                            member.literacyLevelname = data9.name;
                        } else if (data9 == null) {
                            member.literacyLevelname = '';
                        }

                        var data10 = $scope.nameofschoolsdsply.filter(function (arr) {
                            return arr.id == member.nameOfSchool
                        })[0];

                        if (data10 != undefined) {
                            member.nameOfSchoolname = data10.name;
                        } else if (data10 == null) {
                            member.nameOfSchoolname = '';
                        }

                        var data11 = $scope.nameoffactoriesdsply.filter(function (arr) {
                            return arr.id == member.nameOfFactory
                        })[0];

                        if (data11 != undefined) {
                            member.nameOfFactoryname = data11.name;
                        } else if (data11 == null) {
                            member.nameOfFactoryname = '';
                        }

                        var data12 = $scope.shgsdsply.filter(function (arr) {
                            return arr.id == member.shgId
                        })[0];

                        if (data12 != undefined) {
                            member.shgname = data12.groupName;
                        } else if (data12 == null || data12 == '') {
                            member.shgname = '';
                        }

                        var data13 = $scope.countrydisplay.filter(function (arr) {
                            return arr.id == member.countryId
                        })[0];

                        if (data13 != undefined) {
                            member.countryname = data13.name;
                        }

                        var data14 = $scope.statedisplay.filter(function (arr) {
                            return arr.id == member.stateId
                        })[0];

                        if (data14 != undefined) {
                            member.statename = data14.name;
                        }

                        var data15 = $scope.districtdsply.filter(function (arr) {
                            return arr.id == member.districtId
                        })[0];

                        if (data15 != undefined) {
                            member.districtname = data15.name;
                        }

                        var data16 = $scope.sitedsply.filter(function (arr) {
                            return arr.id == member.siteId
                        })[0];

                        if (data16 != undefined) {
                            member.sitename = data16.name;
                        }

                        var data17 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.associatedHF
                        })[0];

                        if (data17 != undefined) {
                            member.assignedTo = data17.name;
                        }

                        var data18 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.lastModifiedBy
                        })[0];

                        if (data18 != undefined) {
                            member.lastModifiedByname = data18.username;
                        } else if (data18 == null) {
                            member.lastModifiedByname = '';
                        }

                        var data19 = $scope.roledsply.filter(function (arr) {
                            return arr.id == member.lastModifiedByRole
                        })[0];

                        if (data19 != undefined) {
                            member.lastModifiedByRolename = data19.name;
                        } else if (data19 == null) {
                            member.lastModifiedByRolename = '';
                        }
                        var data20 = $scope.roledsply.filter(function (arr) {
                            return arr.id == member.createdByRole
                        })[0];

                        if (data20 != undefined) {
                            member.createdByRolename = data20.name;
                        }

                        var data21 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.createdBy
                        })[0];

                        if (data21 != undefined) {
                            member.createdByname = data21.username;
                        }
                    });
                });
            });
        });

        $scope.houseHoldDisplay = [];



        $scope.filterFields = ['nameOfHeadInHH', 'HHId', 'mobile', 'mobileNumbers', 'assignedTo', 'familyMember', 'familyMemberIndIds'];
        $scope.searchInput = '';

        Restangular.all($scope.ShgfilterCall).getList().then(function (shgs) {
            $scope.finalSHGS = shgs;

            Restangular.all($scope.memberFilterCall).getList().then(function (mbrs) {
                $scope.memberdisly = mbrs;

                if ($window.sessionStorage.roleId + "" === "3") {
                    $scope.filterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                } else {

                    $scope.filterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                }

                Restangular.all($scope.filterCall).getList().then(function (household) {
                    $scope.households = household;

                    $scope.modalInstanceLoad.close();

                    angular.forEach($scope.households, function (member, index) {
                        if (member.quickaddFlag == true) {
                            member.backgroundcolor = 'red';
                        } else {
                            member.backgroundcolor = 'black';
                        }
                        member.index = index + 1;

                        // console.log('member.ssgThisHHBelongs', member.ssgThisHHBelongs);
                        if (member.ssgThisHHBelongs == null || member.ssgThisHHBelongs == "" || member.ssgThisHHBelongs == 0 || member.ssgThisHHBelongs == ",0") {

                            member.shgBelongsName = "";
                            member.disableAssign = false;
                            member.disableDelete = true;

                        } else {

                            member.disableAssign = true;
                            member.disableDelete = false;
                            member.arrayshgbelongs = member.ssgThisHHBelongs.split(",");
                            member.shgBelongsName = "";


                            for (var i = 0; i < member.arrayshgbelongs.length; i++) {
                                for (var j = 0; j < $scope.finalSHGS.length; j++) {
                                    if ($scope.finalSHGS[j].id == member.arrayshgbelongs[i]) {
                                        if (member.shgBelongsName == "") {
                                            member.shgBelongsName = $scope.finalSHGS[j].groupName;
                                        } else {
                                            member.shgBelongsName = member.shgBelongsName + "," + $scope.finalSHGS[j].groupName;
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        /*Restangular.one('members?filter[where][parentId]=' + member.id).getList().then(function (mmlist) {

                            if (mmlist.length > 0) {
                                member.mobile = mmlist[0].mobile;
                                member.noOfMembersDisplay = mmlist.length;
                            }
                        });*/

                        member.familyMember = [];
                        member.mobileNumbers = [];
                        member.familyMemberIndIds = [];
                        member.noOfMembersDisplay = 0;

                        var data = $scope.memberdisly.filter(function (arr) {
                            return arr.parentId == member.id
                        });

                        if (data.length != 0) {
                            member.mobile = data[0].mobile;
                            member.noOfMembersDisplay = data.length;

                            for (var k = 0; k < data.length; k++) {
                                member.familyMember.push(data[k].name);
                                member.mobileNumbers.push(data[k].mobile);
                                member.familyMemberIndIds.push(data[k].individualId);
                            }
                        }

                        var data101 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.associatedHF
                        })[0];

                        if (data101 != undefined) {
                            member.assignedTo = data101.name;
                        }
                        // console.log('member', member);
                    });
                });
            });
        });

        $scope.status = 'active';

        $scope.$watch('status', function (newValue, oldValue) {
            if (newValue === oldValue || newValue === '' || newValue === null) {
                return;
            } else {

                $scope.householdsArray = [];

                Restangular.all($scope.ShgfilterCall).getList().then(function (shgs) {
                    $scope.finalSHGS = shgs;

                    if ($window.sessionStorage.roleId + "" === "3") {
                        $scope.filterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                    } else {
                        $scope.filterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                    }
                    Restangular.all($scope.filterCall).getList().then(function (household) {
                        $scope.households = household;
                        angular.forEach($scope.households, function (member, index) {

                            if (member.quickaddFlag == true) {
                                member.backgroundcolor = 'red';
                            } else {
                                member.backgroundcolor = 'black';
                            }

                            member.index = index + 1;

                            Restangular.all('members?filter[where][parentId]=' + member.id + '&filter[where][isHeadOfFamily]=true').getList().then(function (mmlist) {
                                member.mobile = mmlist[0].mobile;
                            });

                            // console.log('member.ssgThisHHBelongs', member.ssgThisHHBelongs);
                            if (member.ssgThisHHBelongs == null || member.ssgThisHHBelongs == "" || member.ssgThisHHBelongs == 0 || member.ssgThisHHBelongs == ",0") {

                                member.shgBelongsName = "";
                                member.disableAssign = false;
                                member.disableDelete = true;

                            } else {

                                member.disableAssign = true;
                                member.disableDelete = false;
                                member.arrayshgbelongs = member.ssgThisHHBelongs.split(",");
                                member.shgBelongsName = "";


                                for (var i = 0; i < member.arrayshgbelongs.length; i++) {
                                    for (var j = 0; j < $scope.finalSHGS.length; j++) {
                                        if ($scope.finalSHGS[j].id == member.arrayshgbelongs[i]) {
                                            if (member.shgBelongsName == "") {
                                                member.shgBelongsName = $scope.finalSHGS[j].groupName;
                                            } else {
                                                member.shgBelongsName = member.shgBelongsName + "," + $scope.finalSHGS[j].groupName;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }

                            member.familyMember = [];
                            member.mobileNumbers = [];
                            member.familyMemberIndIds = [];
                            member.noOfMembersDisplay = 0;

                            var data = $scope.memberdisly.filter(function (arr) {
                                return arr.parentId == member.id
                            });

                            if (data.length != 0) {
                                member.mobile = data[0].mobile;
                                member.noOfMembersDisplay = data.length;

                                for (var k = 0; k < data.length; k++) {
                                    member.familyMember.push(data[k].name);
                                    member.mobileNumbers.push(data[k].mobile);
                                    member.familyMemberIndIds.push(data[k].individualId);
                                }
                            }

                        });
                    });
                });
            };
        });

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        $scope.obj1 = {};
        $scope.$watch('obj1.shgId', function (newValue, oldValue) {
            //  console.log('newValue', newValue);
            // console.log('oldValue', oldValue);
            if (newValue == oldValue || newValue == '') {
                return;
            } else {

                $scope.householdsArray = [];
                $scope.obj.HfId = '';

                if ($window.sessionStorage.roleId + "" === "3") {
                    $scope.filterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                } else {
                    $scope.filterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
                }


                Restangular.all($scope.filterCall).getList().then(function (household) {
                    $scope.households = [];
                    for (var i = 0; i < household.length; i++) {
                        if (household[i].ssgThisHHBelongs != null && household[i].ssgThisHHBelongs.split(",").indexOf(newValue) != -1) {
                            $scope.households.push(household[i]);
                        }
                    }

                    angular.forEach($scope.households, function (member, index) {

                        if (member.quickaddFlag == true) {
                            member.backgroundcolor = 'red';
                        } else {
                            member.backgroundcolor = 'black';
                        }

                        member.index = index + 1;

                        // console.log('member.ssgThisHHBelongs', member.ssgThisHHBelongs);
                        if (member.ssgThisHHBelongs == null || member.ssgThisHHBelongs == "") {

                            member.shgBelongsName = "";
                            member.disableAssign = false;
                            member.disableDelete = true;

                        } else {

                            member.disableAssign = true;
                            member.disableDelete = false;
                            member.arrayshgbelongs = member.ssgThisHHBelongs.split(",");
                            member.shgBelongsName = "";


                            for (var i = 0; i < member.arrayshgbelongs.length; i++) {
                                for (var j = 0; j < $scope.finalSHGS.length; j++) {
                                    if ($scope.finalSHGS[j].id == member.arrayshgbelongs[i]) {
                                        if (member.shgBelongsName == "") {
                                            member.shgBelongsName = $scope.finalSHGS[j].groupName;
                                        } else {
                                            member.shgBelongsName = member.shgBelongsName + "," + $scope.finalSHGS[j].groupName;
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        member.familyMember = [];
                        member.mobileNumbers = [];
                        member.familyMemberIndIds = [];
                        member.noOfMembersDisplay = 0;

                        var data = $scope.memberdisly.filter(function (arr) {
                            return arr.parentId == member.id
                        });

                        if (data.length != 0) {
                            member.mobile = data[0].mobile;
                            member.noOfMembersDisplay = data.length;

                            for (var k = 0; k < data.length; k++) {
                                member.familyMember.push(data[k].name);
                                member.mobileNumbers.push(data[k].mobile);
                                member.familyMemberIndIds.push(data[k].individualId);
                            }
                        }

                        /** member.familyMember = [];
                        for (var q = 0; q < $scope.memberdisly.length; q++) {
                            if (member.id == $scope.memberdisly[q].parentId) {
                                member.familyMember = $scope.memberdisly[q].name;
                            }
                        }**/

                        var data111 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.associatedHF
                        })[0];

                        if (data111 != undefined) {
                            member.assignedTo = data111.name;
                        }
                        //console.log('member', member);
                    });
                });
                $scope.shgIdSelected = newValue;

            }
        });

        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.shgs = Restangular.all('shgs?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().$object;
        } else {
            $scope.shgs = Restangular.all('shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().$object;
        }

        /*************************************** New Changes ***********************************/
        $scope.obj = {};

        $scope.$watch('obj.HfId', function (newValue, oldValue) {
            // console.log('newValue', newValue);
            //console.log('oldValue', oldValue);
            if (newValue == oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {

                $scope.obj1.shgId = '';
                $scope.householdsArray = [];

                $scope.households = [];
                $scope.filterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + newValue + ']}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                Restangular.all($scope.filterCall).getList().then(function (household) {
                    // console.log('household', household);
                    $scope.households = household;
                    /** $scope.households = [];
                     for (var i = 0; i < household.length; i++) {
                         
                         if (household[i].ssgThisHHBelongs != null && household[i].ssgThisHHBelongs.split(",").indexOf($scope.shgIdSelected) != -1) {
                             $scope.households.push(household[i]);
                         }
                     }**/


                    angular.forEach($scope.households, function (member, index) {

                        if (member.quickaddFlag == true) {
                            member.backgroundcolor = 'red';
                        } else {
                            member.backgroundcolor = 'black';
                        }

                        member.index = index + 1;

                        // console.log('member.ssgThisHHBelongs', member.ssgThisHHBelongs);
                        if (member.ssgThisHHBelongs == null || member.ssgThisHHBelongs == "") {

                            member.shgBelongsName = "";
                            member.disableAssign = false;
                            member.disableDelete = true;

                        } else {

                            member.disableAssign = true;
                            member.disableDelete = false;
                            member.arrayshgbelongs = member.ssgThisHHBelongs.split(",");
                            member.shgBelongsName = "";


                            for (var i = 0; i < member.arrayshgbelongs.length; i++) {
                                for (var j = 0; j < $scope.finalSHGS.length; j++) {
                                    if ($scope.finalSHGS[j].id == member.arrayshgbelongs[i]) {
                                        if (member.shgBelongsName == "") {
                                            member.shgBelongsName = $scope.finalSHGS[j].groupName;
                                        } else {
                                            member.shgBelongsName = member.shgBelongsName + "," + $scope.finalSHGS[j].groupName;
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        member.familyMember = [];
                        member.familyMemberIndIds = [];
                        member.mobileNumbers = [];
                        member.noOfMembersDisplay = 0;

                        var data = $scope.memberdisly.filter(function (arr) {
                            return arr.parentId == member.id
                        });

                        if (data.length != 0) {
                            member.mobile = data[0].mobile;
                            member.noOfMembersDisplay = data.length;

                            for (var k = 0; k < data.length; k++) {
                                member.familyMember.push(data[k].name);
                                member.mobileNumbers.push(data[k].mobile);
                                member.familyMemberIndIds.push(data[k].individualId);
                            }
                        }

                        var data111 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.associatedHF
                        })[0];

                        if (data111 != undefined) {
                            member.assignedTo = data111.name;
                        }
                    });
                });

            }
        });




        /**************************Export data to excel sheet ***************/

        $scope.valHHCount = 0;

        $scope.exportData = function () {
            $scope.householdsArray = [];
            $scope.valHHCount = 0;

            if ($scope.sortedArray.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.sortedArray.length; c++) {

                    $scope.householdsArray.push({
                        'Sr No': $scope.sortedArray[c].index,
                        'COUNTRY': $scope.sortedArray[c].countryname,
                        'STATE': $scope.sortedArray[c].statename,
                        'DISTRICT': $scope.sortedArray[c].districtname,
                        'SITE': $scope.sortedArray[c].sitename,
                        'NAME': $scope.sortedArray[c].name,
                        'ASSIGNED TO': $scope.sortedArray[c].assignedTo,
                        'HOUSEHOLD ID': $scope.sortedArray[c].HHId,
                        'INDIVIDUAL ID': $scope.sortedArray[c].individualId,
                        'IS HEAD OF FAMILY': $scope.sortedArray[c].isHeadOfFamily,
                        'DOB': $scope.sortedArray[c].dob,
                        'AGE': $scope.sortedArray[c].age,
                        'GENDER': $scope.sortedArray[c].gendername,
                        'PHONE NUMBER': $scope.sortedArray[c].mobile,
                        'OCCUPATION': $scope.sortedArray[c].occupationname,
                        'ADDRESS': $scope.sortedArray[c].address,
                        'CASTE': $scope.sortedArray[c].castename,
                        'RELIGION': $scope.sortedArray[c].religionname,
                        'MIGRANT': $scope.sortedArray[c].migrantname,
                        'BPL STATUS': $scope.sortedArray[c].bplstatusname,
                        'LATITUDE': $scope.sortedArray[c].latitude,
                        'LONGITUDE': $scope.sortedArray[c].longitude,
                        'EMAIL ID': $scope.sortedArray[c].email,
                        'AADHAR NO': $scope.sortedArray[c].aadharNumber,
                        'ESI NO': $scope.sortedArray[c].esiNumber,
                        'RELATIONSHIP WITH HH HEAD': $scope.sortedArray[c].relationname,
                        'LITERACY LEVEL': $scope.sortedArray[c].literacyLevelname,
                        'NAME OF SCHOOL': $scope.sortedArray[c].nameOfSchoolname,
                        'NAME OF FACTORY': $scope.sortedArray[c].nameOfFactoryname,
                        'SHG': $scope.sortedArray[c].shgname,
                        'IS SHG MEMBER': $scope.sortedArray[c].isSHg,
                        'QUICK ADD': $scope.sortedArray[c].quickaddFlag,
                        'HEALTHYDAYS': $scope.sortedArray[c].healthyDays1,
                        'CREATED BY': $scope.sortedArray[c].createdByname,
                        'CREATED DATE': $scope.sortedArray[c].createdDate,
                        'CREATED ROLE': $scope.sortedArray[c].createdByRolename,
                        'LAST MODIFIED BY': $scope.sortedArray[c].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.sortedArray[c].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.sortedArray[c].lastModifiedByRolename,
                        'DELETE FLAG': $scope.sortedArray[c].deleteFlag
                    });

                    $scope.valHHCount++;

                    if ($scope.sortedArray.length == $scope.valHHCount) {
                        alasql('SELECT * INTO XLSX("households.xlsx",{headers:true}) FROM ?', [$scope.householdsArray]);
                    }
                }
            }
        };

        /************** Delete Function *************************************/

        $scope.item = {
            deleteFlag: true
        };

        $scope.Delete = function (id) {
            
            Restangular.one('households/' + id).customPUT($scope.item).then(function (rsp) {
                Restangular.all('members?filter[where][parentId]=' + rsp.id).getList().then(function (lmems) {
                    $scope.listmembers = lmems;
                    $scope.DeleteFunc();
                });
            });
        };

        $scope.deleteCount = 0;

        $scope.DeleteFunc = function (id) {

            if ($scope.deleteCount < $scope.listmembers.length) {
                
                Restangular.one('members', $scope.listmembers[$scope.deleteCount].id).customPUT($scope.item).then(function () {
                    $scope.deleteCount++;
                    $scope.DeleteFunc();
                });
                
            } else {
                $route.reload();
            }
        };

        /*******************New Changes for UnassignedSHG ***************/

        $scope.hhUpdate = {};

        $scope.UnassignedSHG = function (id) {

            $scope.hhUpdate.ssgThisHHBelongs = '';
            $scope.currentHHId = id;
            //  console.log($scope.currentHHId);

            Restangular.one('households', id).customPUT($scope.hhUpdate).then(function (Response) {

                // console.log('Response', Response.ssgThisHHBelongs);

                Restangular.all('members?filter[where][parentId]=' + Response.id).getList().then(function (memResponse) {
                    $scope.dataREmove = memResponse;
                    var count = 0;
                    angular.forEach(memResponse, function (memberdata, index) {
                        count++;
                        if (memResponse.length == count) {
                            $scope.RemoveShgID();
                        }
                    });
                });
            });
        };

        $scope.membershgclear = [{
            shgId: null
                    }]

        $scope.CountSave = 0;
        $scope.RemoveShgID = function () {

            if ($scope.CountSave < $scope.dataREmove.length) {

                Restangular.one('members', $scope.dataREmove[$scope.CountSave].id).get().then(function (member) {

                    if (member.shgId == null || member.shgId == 'null' || member.shgId == "" || member.shgId == '0' || member.shgId == 0) {
                        $scope.CountSave++;
                        $scope.RemoveShgID();
                    } else {

                        Restangular.one('shgs', member.shgId).get().then(function (shg) {

                            $scope.shgMemUpdate = {}

                            $scope.membersShg = shg.membersInGroup.split(",");

                            for (var i = 0; i <= $scope.membersShg.length; i++) {
                                if ($scope.membersShg[i] == member.id) {
                                    $scope.membersShg.splice(i, 1);
                                    // console.log('$scope.membersShg', $scope.membersShg);
                                }
                            }

                            $scope.shgMemUpdate.membersInGroup = $scope.membersShg;

                            Restangular.one('shgs', shg.id).customPUT($scope.shgMemUpdate).then(function (shgResp) {
                                //console.log('ShgUpdated', shgResp);

                                Restangular.one('members/' + $scope.dataREmove[$scope.CountSave].id).customPUT($scope.membershgclear[0]).then(function (memshgRemoveResp) {
                                    // console.log('finalUpdate', memshgRemoveResp);
                                    $scope.CountSave++;
                                    $scope.RemoveShgID();

                                });

                            });
                        });

                    }
                });
            } else {

                $scope.hhUpdate.ssgThisHHBelongs = '';

                Restangular.one('households', $scope.currentHHId).customPUT($scope.hhUpdate).then(function (Response) {
                    $route.reload();
                });
                // console.log('fineshed');
            }
        };

        /***********************************/

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };
    })

    .directive('focus', function ($timeout, $parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$watch(attrs.focus, function (newValue, oldValue) {
                    if (newValue) {
                        element[0].focus();
                    }
                });
                element.bind("blur", function (e) {
                    $timeout(function () {
                        scope.$apply(attrs.focus + "=false");
                    }, 0);
                });
                element.bind("focus", function (e) {
                    $timeout(function () {
                        scope.$apply(attrs.focus + "=true");
                    }, 0);
                })
            }
        }
    })

    .filter('search', function () {
        return function (list, query, fields) {

            if (!query) {
                return list;
            }

            query = query.toLowerCase().split(' ');

            if (!angular.isArray(fields)) {
                fields = [fields.toString()];
            }

            return list.filter(function (item) {
                return query.every(function (needle) {
                    return fields.some(function (field) {
                        var content = item[field] != null ? item[field] : '';

                        if (!angular.isString(content)) {
                            content = '' + content;
                        }

                        return content.toLowerCase().indexOf(needle) > -1;
                    });
                });
            });
        };
    });
