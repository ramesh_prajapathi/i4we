'use strict';

angular.module('secondarySalesApp')
    .controller('AreaCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {
        /*********/
        //		if ($window.sessionStorage.roleId != 1) {
        //			window.location = "/";
        //		}

        $scope.someFocusVariable = true;

        $scope.showForm = function () {
            var visible = $location.path() === '/area/create' || $location.path() === '/area/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/area/create';
                return visible;
            }
        };

        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/area/create' || $location.path() === '/area/edit/' + $routeParams.id;
            return visible;
        };

        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/area/create' || $location.path() === '/area/edit/' + $routeParams.id;
            return visible;
        };

        /************************************************************************************/
        if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
            $window.sessionStorage.facility_zoneId = null;
            $window.sessionStorage.facility_stateId = null;
            $window.sessionStorage.facility_currentPage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.facility_zoneId;
            $scope.stateId = $window.sessionStorage.facility_stateId;
            $scope.currentpage = $window.sessionStorage.facility_currentPage;
            $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        }


        if ($window.sessionStorage.prviousLocation != "partials/sub-areas") {
            $window.sessionStorage.facility_zoneId = '';
            $window.sessionStorage.facility_stateId = '';
            $window.sessionStorage.facility_currentPage = 1;
            $scope.currentpage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
            $scope.pageSize = 25;
        }

        $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.facility_currentPageSize = mypage;
        };

        $scope.currentpage = $window.sessionStorage.facility_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.facility_currentPage = newPage;
        };

        /************************************************/

        Restangular.all('countries?filter[where][deleteFlag]=false').getList().then(function (ct) {
            $scope.countries = ct;
            $scope.countryId = ct[0].id;
            $scope.stateId = $window.sessionStorage.facility_stateId;
        });

        $scope.statedsply = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;

        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;

        $scope.sitesdsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;

        //  $scope.countryId = '';
        $scope.stateId = '';
        $scope.statesid = '';
        $scope.countiesid = '';
        $scope.districtId = '';

        $scope.$watch('countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $window.sessionStorage.facility_zoneId = newValue;

                $scope.areaArray = [];

                Restangular.all('states?filter[where][countryId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (responceSt) {
                    $scope.displaystates = responceSt;
                    $scope.stateId = $window.sessionStorage.facility_stateId;
                });

                Restangular.all('areas?filter[where][countryId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (ctyRes1) {
                    $scope.Displayareas = ctyRes1;
                    angular.forEach($scope.Displayareas, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.countries.length; a++) {
                            if (member.countryId == $scope.countries[a].id) {
                                member.countryname = $scope.countries[a].name;
                                break;
                            }
                        }
                        for (var b = 0; b < $scope.statedsply.length; b++) {
                            if (member.stateId == $scope.statedsply[b].id) {
                                member.statename = $scope.statedsply[b].name;
                                break;
                            }
                        }
                        for (var d = 0; d < $scope.districtdsply.length; d++) {
                            if (member.districtId == $scope.districtdsply[d].id) {
                                member.districtname = $scope.districtdsply[d].name;
                                break;
                            }
                        }
                        for (var e = 0; e < $scope.sitesdsply.length; e++) {
                            if (member.siteId == $scope.sitesdsply[e].id) {
                                member.sitename = $scope.sitesdsply[e].name;
                                break;
                            }
                        }
                    });
                });
                $scope.countiesid = +newValue;
            }
        });

        $scope.$watch('stateId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $window.sessionStorage.facility_stateId = newValue;

                $scope.areaArray = [];

                Restangular.all('districts?filter[where][deleteFlag]=false' + '&filter[where][stateId]=' + newValue).getList().then(function (dt) {
                    $scope.displaydistricts = dt;
                    $scope.stateId = $window.sessionStorage.facility_stateId;
                });

                Restangular.all('areas?filter[where][stateId]=' + newValue + '&filter[where][countryId]=' + $scope.countiesid + '&filter[where][deleteFlag]=false').getList().then(function (ctyRes) {
                    $scope.Displayareas = ctyRes;
                    angular.forEach($scope.Displayareas, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.countries.length; a++) {
                            if (member.countryId == $scope.countries[a].id) {
                                member.countryname = $scope.countries[a].name;
                                break;
                            }
                        }
                        for (var b = 0; b < $scope.statedsply.length; b++) {
                            if (member.stateId == $scope.statedsply[b].id) {
                                member.statename = $scope.statedsply[b].name;
                                break;
                            }
                        }
                        for (var d = 0; d < $scope.districtdsply.length; d++) {
                            if (member.districtId == $scope.districtdsply[d].id) {
                                member.districtname = $scope.districtdsply[d].name;
                                break;
                            }
                        }
                        for (var e = 0; e < $scope.sitesdsply.length; e++) {
                            if (member.siteId == $scope.sitesdsply[e].id) {
                                member.sitename = $scope.sitesdsply[e].name;
                                break;
                            }
                        }
                    });
                });
            }
            $scope.statesid = +newValue;
        });

        $scope.$watch('districtId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $window.sessionStorage.facility_stateId = newValue;

                $scope.areaArray = [];

                Restangular.all('sites?filter[where][deleteFlag]=false' + '&filter[where][districtId]=' + newValue).getList().then(function (dt) {
                    $scope.displaysites = dt;
                    $scope.siteId = $window.sessionStorage.facility_siteId;
                });

                Restangular.all('areas?filter[where][districtId]=' + newValue + '&filter[where][stateId]=' + $scope.statesid + '&filter[where][countryId]=' + $scope.countiesid + '&filter[where][deleteFlag]=false').getList().then(function (ctyRes) {
                    $scope.Displayareas = ctyRes;
                    angular.forEach($scope.Displayareas, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.countries.length; a++) {
                            if (member.countryId == $scope.countries[a].id) {
                                member.countryname = $scope.countries[a].name;
                                break;
                            }
                        }
                        for (var b = 0; b < $scope.statedsply.length; b++) {
                            if (member.stateId == $scope.statedsply[b].id) {
                                member.statename = $scope.statedsply[b].name;
                                break;
                            }
                        }
                        for (var d = 0; d < $scope.districtdsply.length; d++) {
                            if (member.districtId == $scope.districtdsply[d].id) {
                                member.districtname = $scope.districtdsply[d].name;
                                break;
                            }
                        }
                        for (var e = 0; e < $scope.sitesdsply.length; e++) {
                            if (member.siteId == $scope.sitesdsply[e].id) {
                                member.sitename = $scope.sitesdsply[e].name;
                                break;
                            }
                        }
                    });
                });
            }
            $scope.distid = +newValue;
        });

        $scope.$watch('siteId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $window.sessionStorage.facility_stateId = newValue;

                $scope.areaArray = [];

                Restangular.all('areas?filter[where][siteId]=' + newValue + '&filter[where][stateId]=' + $scope.statesid + '&filter[where][countryId]=' + $scope.countiesid + '&filter[where][districtId]=' + $scope.distid + '&filter[where][deleteFlag]=false').getList().then(function (ctyRes) {
                    $scope.Displayareas = ctyRes;
                    angular.forEach($scope.Displayareas, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.countries.length; a++) {
                            if (member.countryId == $scope.countries[a].id) {
                                member.countryname = $scope.countries[a].name;
                                break;
                            }
                        }
                        for (var b = 0; b < $scope.statedsply.length; b++) {
                            if (member.stateId == $scope.statedsply[b].id) {
                                member.statename = $scope.statedsply[b].name;
                                break;
                            }
                        }
                        for (var d = 0; d < $scope.districtdsply.length; d++) {
                            if (member.districtId == $scope.districtdsply[d].id) {
                                member.districtname = $scope.districtdsply[d].name;
                                break;
                            }
                        }
                        for (var e = 0; e < $scope.sitesdsply.length; e++) {
                            if (member.siteId == $scope.sitesdsply[e].id) {
                                member.sitename = $scope.sitesdsply[e].name;
                                break;
                            }
                        }
                    });
                });
            }
        });

        $scope.Displayareas = [];

        /**********************************************************************************/
        $scope.statedisable = false;
        $scope.districdisable = false;
        $scope.searchCity = $scope.name;
        $scope.getSalesArea = function (salesareaId) {
            return Restangular.one('sales-areas', salesareaId).get().$object;
        };

        $scope.getCountry = function (countryId) {
            return Restangular.one('countries', countryId).get().$object;
        };

        $scope.getState = function (stateId) {
            return Restangular.one('states', stateId).get().$object;
        };

        $scope.getDistrict = function (districtId) {
            return Restangular.one('districts', districtId).get().$object;
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('areas/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /********************************************** WATCH ***************************************/

        Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
            $scope.zones = znes;
            $scope.countryId = $window.sessionStorage.facility_zoneId;
        });

        $scope.subarea = {
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.UserEmployeeId,
            deleteFlag: false
        };

        $scope.subarea.countryId = '';
        $scope.subarea.siteId = '';
        $scope.subarea.districtId = '';

        $scope.$watch('subarea.countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                Restangular.all('states?filter[where][countryId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (ste) {
                    $scope.states = ste;
                    $scope.subarea.stateId = $scope.subarea.stateId;
                });
            }
        });

        $scope.$watch('subarea.stateId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                Restangular.all('districts?filter[where][stateId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (dte) {
                    $scope.districts = dte;
                    $scope.subarea.districtId = $scope.subarea.districtId;
                });
            }
        });


        $scope.$watch('subarea.districtId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                Restangular.all('sites?filter[where][districtId]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (ste) {
                    $scope.sites = ste;
                    $scope.subarea.sitetId = $scope.subarea.sitetId;
                });
            }
        });
        /****************************************** CREATE *********************************/

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            //  document.getElementById('code').style.border = "";

            if ($scope.subarea.name == '' || $scope.subarea.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.subarea.countryId == '' || $scope.subarea.countryId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Country';

            } else if ($scope.subarea.stateId == '' || $scope.subarea.stateId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select State';

            } else if ($scope.subarea.districtId == '' || $scope.subarea.districtId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select District';

            } else if ($scope.subarea.siteId == '' || $scope.subarea.siteId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Site';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.all('areas').post($scope.subarea).then(function () {
                    window.location = '/area-list';
                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /******************************************************** UPDATE ************************/
        $scope.modalTitle = 'Thank You';

        if ($routeParams.id) {
            $scope.statedisable = true;
            $scope.districdisable = true;
            $scope.message = 'Sub Location has been Updated!';

            Restangular.one('areas', $routeParams.id).get().then(function (subarea) {
                $scope.original = subarea;
                $scope.subarea = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Sub Location has been Created!';
        }

        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.subarea.name === '' || $scope.subarea.name === null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } 
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.submitDisable = true;
                Restangular.one('areas', $routeParams.id).customPUT($scope.subarea).then(function () {
                    window.location = '/area-list';
                });
            }
        };

        /**************************Export data to excel sheet ***************/

        $scope.valAreaCount = 0;

        $scope.exportData = function () {
            $scope.valSteCount = 0;
            // console.log('$scope.householdsArray', $scope.householdsArray);
            if ($scope.Displayareas.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.Displayareas.length; c++) {
                    $scope.areaArray.push({
                        'AREA': $scope.Displayareas[c].name,
                        'SITE': $scope.Displayareas[c].sitename,
                        'DISTRICT': $scope.Displayareas[c].districtname,
                        'STATE': $scope.Displayareas[c].statename,
                        'COUNTRY': $scope.Displayareas[c].countryname,
                        'DELETE FLAG': $scope.Displayareas[c].deleteFlag
                    });

                    $scope.valAreaCount++;

                    if ($scope.Displayareas.length == $scope.valAreaCount) {
                        alasql('SELECT * INTO XLSX("areas.xlsx",{headers:true}) FROM ?', [$scope.areaArray]);
                    }
                }
            }
        };
    });