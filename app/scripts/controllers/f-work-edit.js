'use strict';
angular.module('secondarySalesApp').controller('FWEditCtrl', function ($scope, Restangular, $routeParams, $window, $modal, AnalyticsRestangular, $filter) {

    $scope.isCreateView = false;
    $scope.heading = 'Field Worker Edit';
    $scope.HideUsername = true;
    $scope.tabtwohide = true;
    $scope.hideSitesAssign = false;
    $scope.createDisabled = false;
    $scope.UpdateClicked = false;

    $scope.HfLanguage = {};

    Restangular.one('hflanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.HfLanguage = langResponse[0];
        $scope.hfHeading = langResponse[0].hfEdit;
    });

    Restangular.one('users', $routeParams.id).get().then(function (hf) {
        $scope.original = hf;
        $scope.hf = Restangular.copy($scope.original);
        
        $scope.existusername = hf.username;
        $scope.existemail = hf.email;

        Restangular.one('countries', $window.sessionStorage.countryId).get().then(function (cn) {
            $scope.countryName = cn.name;
        });

        Restangular.one('states', $window.sessionStorage.stateId).get().then(function (st) {
            $scope.stateName = st.name;
        });

        Restangular.one('districts', $window.sessionStorage.districtId).get().then(function (dt) {
            $scope.districtName = dt.name;
        });

        Restangular.one('sites', $window.sessionStorage.siteId.split(",")[0]).get().then(function (ste) {
            console.log('ste', ste);
            console.log('$window.sessionStorage.siteId', $window.sessionStorage.siteId);
            $scope.siteName = ste.name;
        });

        Restangular.one('users', $window.sessionStorage.userId).get().then(function (stm) {
            $scope.sitemgrName = stm.name;
            $scope.modalInstanceLoad.close();
        });
    });
    /*********************************************** Update *******************************************/

    $scope.auditlog = {
        description: 'Health Facilitator Update',
        action: 'Insert',
        module: 'Health Facilitator',
        owner: $window.sessionStorage.userId,
        datetime: new Date(),
        modifiedbyroleid: $window.sessionStorage.roleId,
        modifiedby: $window.sessionStorage.userId,
        lastmodifiedtime: new Date(),
        entityroleid: 55,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        facility: $window.sessionStorage.coorgId
    };
    
     $scope.duplicateusername = false;
    $scope.duplicateemail = false;

    $scope.checkuser = function (username) {

        var data = $scope.userslist.filter(function (arr) {
            return arr.username == username
        })[0];

        if (data != undefined) {
            $scope.duplicateusername = true;
        } else {
            $scope.duplicateusername = false;
        }
    };

    $scope.checkemail = function (email) {

        var data1 = $scope.userslist.filter(function (arr) {
            return arr.email == email
        })[0];

        if (data1 != undefined) {
            $scope.duplicateemail = true;
        } else {
            $scope.duplicateemail = false;
        }
    };

    $scope.validatestring = '';

    $scope.UpdateComember = {
        fwcount: 0
    };

    $scope.dataModal = false;

    Restangular.all('users?filter[where][employeeid]=' + $routeParams.id + '&filter[where][roleId]=6').getList().then(function (submituser) {
        $scope.getUserId = submituser[0].id;
    });

    $scope.Update = function () {

        document.getElementById('firstname').style.border = "";
        document.getElementById('lastname').style.border = "";
        document.getElementById('email').style.border = "";
        document.getElementById('mobile').style.border = "";
        document.getElementById('password').style.border = "";

        if ($scope.hf.username == '' || $scope.hf.username == null) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.enterusername;
            document.getElementById('firstname').style.border = "1px solid #ff0000";

        } else if ($scope.duplicateusername == true && $scope.existusername != $scope.hf.username) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.usernameexist;
            document.getElementById('firstname').style.border = "1px solid #ff0000";

        } else if ($scope.hf.name == '' || $scope.hf.name == null) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.enterfullname;
            document.getElementById('lastname').style.border = "1px solid #ff0000";

        } else if ($scope.hf.email == '' || $scope.hf.email == null) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.enteremail;
            document.getElementById('email').style.border = "1px solid #ff0000";

        } else if ($scope.duplicateemail == true && $scope.existemail != $scope.hf.email) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.emailexist;
            document.getElementById('email').style.border = "1px solid #ff0000";

        } else if ($scope.hf.mobile == '' || $scope.hf.mobile == null) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.entermobile;
            document.getElementById('mobile').style.border = "1px solid #ff0000";

        } else if ($scope.hf.languageId == '' || $scope.hf.languageId == null) {
            $scope.validatestring = $scope.validatestring + $scope.HfLanguage.selectLanguage;
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {

            $scope.toggleLoading();

            $scope.UpdateClicked = true;
            $scope.createDisabled = true;

            $scope.hf.modifiedbyroleid = $window.sessionStorage.roleId;
            $scope.hf.modifiedby = $window.sessionStorage.userId;
            $scope.hf.lastmodifiedtime = new Date();

            Restangular.one('users', $routeParams.id).customPUT($scope.hf).then(function (memResp) {

                Restangular.all('audittrials').post($scope.auditlog).then(function (auditResp) {

                    $scope.modalInstanceLoad.close();
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('reloading...');

                    setInterval(function () {
                        window.location = "/healthfacilitator-list";
                    }, 1500);
                });
            });
        }
    };

    $scope.showValidation = false;

    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };

    ///////////////////////////////////////////////////////MAP//////////////////////////

    $scope.showMapModal = false;
    $scope.toggleMapModal = function () {
        $scope.mapcount = 0;
        var geocoder = new google.maps.Geocoder();

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function (responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        }

        function updateMarkerStatus(str) {
            document.getElementById('markerStatus').innerHTML = str;
        }

        function updateMarkerPosition(latLng) {
            $scope.fieldworker.latitude = latLng.lat() + ',' + latLng.lng();
            $scope.fieldworker.longitude = latLng.lng();
            document.getElementById('info').innerHTML = [
    latLng.lat()
    , latLng.lng()
  ].join(', ');
        }

        function updateMarkerAddress(str) {
            document.getElementById('mapaddress').innerHTML = str;
        }
        var map;

        function initialize() {
            $scope.address = $scope.fieldworker.address;
            // console.log('$scope.address', $scope.address);
            $scope.latitude = 21.0000;
            $scope.longitude = 78.0000;
            if ($scope.address.length > 0) {
                var addressgeocoder = new google.maps.Geocoder();
                addressgeocoder.geocode({
                    'address': $scope.address
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $scope.latitude = parseInt(results[0].geometry.location.lat());
                        $scope.longitude = parseInt(results[0].geometry.location.lng());
                        //console.log($scope.latitude, $scope.longitude);
                        var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                        map = new google.maps.Map(document.getElementById('mapCanvas'), {
                            zoom: 4,
                            center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });
                        var marker = new google.maps.Marker({
                            position: latLng,
                            title: 'Point A',
                            map: map,
                            draggable: true
                        });
                        // Update current position info.
                        updateMarkerPosition(latLng);
                        geocodePosition(latLng);
                        // Add dragging event listeners.
                        google.maps.event.addListener(marker, 'dragstart', function () {
                            updateMarkerAddress('Dragging...');
                        });
                        google.maps.event.addListener(marker, 'drag', function () {
                            updateMarkerStatus('Dragging...');
                            updateMarkerPosition(marker.getPosition());
                        });
                        google.maps.event.addListener(marker, 'dragend', function () {
                            updateMarkerStatus('Drag ended');
                            geocodePosition(marker.getPosition());
                        });
                    }
                });
            } else {
                $scope.latitude = 21.0000;
                $scope.longitude = 78.0000;
                var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                map = new google.maps.Map(document.getElementById('mapCanvas'), {
                    zoom: 4,
                    center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var marker = new google.maps.Marker({
                    position: latLng,
                    title: 'Point A',
                    map: map,
                    draggable: true
                });
                // Update current position info.
                updateMarkerPosition(latLng);
                geocodePosition(latLng);
                // Add dragging event listeners.
                google.maps.event.addListener(marker, 'dragstart', function () {
                    updateMarkerAddress('Dragging...');
                });
                google.maps.event.addListener(marker, 'drag', function () {
                    updateMarkerStatus('Dragging...');
                    updateMarkerPosition(marker.getPosition());
                });
                google.maps.event.addListener(marker, 'dragend', function () {
                    updateMarkerStatus('Drag ended');
                    geocodePosition(marker.getPosition());
                });
            }
        }
        // Onload handler to fire off the app.
        //google.maps.event.addDomListener(window, 'load', initialize);
        initialize();
        window.setTimeout(function () {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
            map.setZoom(4);
        }, 1000);
        $scope.SaveMap = function () {
            $scope.showMapModal = !$scope.showMapModal;
        };
        $scope.showMapModal = !$scope.showMapModal;
    };
    $scope.CancelMap = function () {
        if ($scope.mapcount == 0) {
            $scope.showMapModal = !$scope.showMapModal;
            $scope.mapcount++;
        }
    };
    /****************************** Language *********************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.fieldworkercreate = langResponse.fieldworkercreate;
        $scope.divbasicinfo = langResponse.divbasicinfo;
        $scope.divuserinfo = langResponse.divuserinfo;
        $scope.printfirstname = langResponse.firstname;
        $scope.printlastname = langResponse.lastname;
        $scope.printfwcode = langResponse.fwcode;
        $scope.printemail = langResponse.email;
        $scope.state = langResponse.state;
        $scope.district = langResponse.district;
        $scope.facility = langResponse.facility;
        $scope.comanagerhdf = langResponse.comanagerhdf;
        $scope.fullname = langResponse.fullname;
        $scope.printmobile = langResponse.mobile;
        $scope.printaddress = langResponse.address;
        $scope.latlong = langResponse.latlong;
        $scope.username = langResponse.username;
        $scope.password = langResponse.password;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.ok = langResponse.ok;
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;

        $scope.enterfullname = langResponse.enterfullname;
        $scope.entermobile = langResponse.entermobile;
        $scope.enteremail = langResponse.enteremail;
        $scope.enterpassword = langResponse.enterpassword;
        $scope.sellanguage = langResponse.sellanguage;
        $scope.usernameexits = langResponse.usernameexits;
        $scope.emailexit = langResponse.emailexit;
        $scope.modalTitle12 = langResponse.createdetails;
    });
    if ($window.sessionStorage.language == 1) {
        //$scope.modalTitle12 = 'Created With the Following Details';
        $scope.LanguagePrint = 'Language';
    } else if ($window.sessionStorage.language == 2) {
        //$scope.modalTitle12 = 'निम्न विवरण के साथ बनाया';
        $scope.LanguagePrint = 'भाषा';
    } else if ($window.sessionStorage.language == 3) {
        //$scope.modalTitle12 = 'ಕೆಳಗಿನ ವಿವರಗಳಿಂದ  ರಚಿಸಲಾಗಿದೆा';
        $scope.LanguagePrint = 'ಭಾಷೆ';
    } else if ($window.sessionStorage.language == 4) {
        //$scope.modalTitle12 = 'பின்வரும் விவரங்கள் உடன் உருவாக்கப்பட்டதுा';
        $scope.LanguagePrint = 'மொழி';
    } else if ($window.sessionStorage.language == 5) {
        //$scope.modalTitle12 = 'క్రింది వివరాలను తో రూపొందించబడింది';
        $scope.LanguagePrint = 'భాష';
    } else if ($window.sessionStorage.language == 6) {
        //$scope.modalTitle12 = 'ह्या माहिती सुधारणा करण्यात आलेले आहे';
        $scope.LanguagePrint = 'भाषा';
    }
});
/*************************************** Not In Use ***************************
        $scope.employees = Restangular.all('employees').getList().$object;
        $scope.partners = Restangular.all('fieldworkers').getList().$object;
        $scope.submitpartners = Restangular.all('fieldworkers').getList().$object;
        $scope.retailers = Restangular.all('retailers').getList().$object;
        $scope.distributionRoutes = Restangular.all('distribution-routes').getList().$object;
        //  $scope.groups = Restangular.all('groups').getList().$object;
        $scope.cities = Restangular.all('cities').getList().$object;
        $scope.states = Restangular.all('states').getList().$object;
        $scope.ims = Restangular.all('ims').getList().$object;
        $scope.retailercategories = Restangular.all('retailercategories').getList().$object;

        $scope.zone = Restangular.all('zones').getList().$object;
        $scope.salesareas = Restangular.all('sales-areas').getList().$object;
        $scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
        $scope.distributionSubAreas = Restangular.all('distribution-subareas').getList().$object;
        $scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;
        $scope.submitusers = Restangular.all('users').getList().$object;

        

        $scope.partner = {};
        $scope.groups = Restangular.one('groups', 9).get().then(function (group) {
            $scope.groupname = group.name;
            $scope.partner.groupId = group.id;
        });
		
		 /*
					$scope.user = {
						flag: true,
						lastmodifiedtime: new Date(),
						lastmodifiedby: $window.sessionStorage.UserEmployeeId,
						deleteflag: false,
						roleId: 6,
						status: 'active',
						salesAreaId: $window.sessionStorage.salesAreaId,
						coorgId: $window.sessionStorage.coorgId,
						zoneId: $window.sessionStorage.zoneId,
						username: submitfw.firstname,
						mobile: submitfw.mobile,
						employeeid: submitfw.id,
						email: submitfw.email
					};*/
/*

		        $scope.openResetPswd = function () {
		            $scope.modalPswd = $modal.open({
		                animation: true,
		                templateUrl: 'template/resetpassword.html',
		                scope: $scope,
		                backdrop: 'static',
		                size: 'lg'

		            });
		        };

		        $scope.SaveResetPswd = function () {
		            Restangular.all('users').login($scope.user).then(function (loginResult) {
		                console.log('loginResult', loginResult);
		            }).then(function () {
		                if ($scope.user.newpassword === $scope.user.confirmnewpassword) {
		                    Restangular.all('users?filter[where][username]=' + $scope.user.username).getList().then(function (detail) {
		                        if (detail.length > 0) {
		                            $scope.userId = detail[0].id;

		                            $scope.newdata = {
		                                password: $scope.user.newpassword
		                            };

		                            Restangular.one('users/' + $scope.userId).customPUT($scope.newdata).then(function (response) {
		                                console.log('response', response);

		                                $scope.modalPswd.close();
		                                $scope.logout();
		                            });
		                        }
		                    });
		                } else {
		                    $scope.errormsg = 'Passwords Doesnt Match';
		                }
		            }, function (response) {
		                console.log('response', response);

		                $scope.errormsg = 'Invalid Password';

		            });
		        };

		        $scope.okResetPswd = function () {
		            $scope.modalPswd.close();
		        };

				*/
/**********************************************************************/
