'use strict';

angular.module('secondarySalesApp')
    .controller('LangInterventionsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/


        $scope.HideCreateButton = true;
        $scope.langdisable = false;

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        $scope.$watch('intervention.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                
                 Restangular.one('interventionLanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.intervention = Restangular.copy($scope.original);
                    });
                
                Restangular.all('interventionLanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                        $scope.HideCreateButton = true;
                    } else {
                        //$scope.LangId = response[0].id;
                        // $scope.HideCreateButton = false;
                        //  $scope.langdisable = true;

                        //  $scope.lhs = response[0];
                        //  console.log('$scope.lhs', $scope.lhs);
                         $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';
                    }
                });

            }
        });
    
                 /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/LangInterventions-list';

        };
    
    
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/

        $scope.intervention = {
            deleteFlag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId
        };

        $scope.Save = function () {
            Restangular.all('interventionLanguages').post($scope.intervention).then(function (intResponse) {
               // console.log('intResponse', intResponse);
                window.location = '/LangInterventions-list';
            });

        };

        $scope.Update = function () {
            Restangular.one('interventionLanguages', $routeParams.id).customPUT($scope.intervention).then(function (intResponse) {
              //  console.log('intResponse', intResponse);
                window.location = '/LangInterventions-list';
            });
        };

        if ($routeParams.id) {
            $scope.HideCreateButton = false;
            $scope.langdisable = true;
            Restangular.one('interventionLanguages', $routeParams.id).get().then(function (intervention) {
                $scope.original = intervention;
                $scope.intervention = Restangular.copy($scope.original);
            });
        }
    });