'use strict';
angular.module('secondarySalesApp')
    .controller('ReportIncidentCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter, $modal) {
        $rootScope.clickFW();

        if ($window.sessionStorage.roleId == 1) {
            window.location = "/";
        }

        $scope.RILanguage = {};

        $scope.HideSubmitButton = true;
        $scope.Violence = false;
        $scope.MedicalEmergency = false;
        $scope.SAWFIssue = false;
        $scope.Showmember = false;
        $scope.ShowmemberOne = false;
        $scope.hideReportedTo = false;
        $scope.dateofclosureHide = false;
        $scope.followupHide = false;

        Restangular.one('riLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.RILanguage = langResponse[0];
            $scope.RiHeading = $scope.RILanguage.riCreate;
            // $scope.modalTitle = $scope.RILanguage.thankYou;
            $scope.message = $scope.RILanguage.reportIncidentCreated;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        $scope.auditlog = {
            action: 'Insert',
            module: 'Report Incident',
            owner: $window.sessionStorage.userId,
            datetime: new Date(),
            details: 'Report Incident Created',
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        };
        /***********************************************************************/
        $scope.HideSubmitButton = false;

        $scope.reportincident = {
            "co": false,
            "physical": false,
            "sexual": false,
            "childrelated": false,
            "emotional": false,
            "propertyrelated": false,
            "mental": false,
            "wound": false,
            "cut": false,
            "severepain": false,
            "bleeding": false,
            "immobile": false,
            "passingout": false,
            "uncoveredfood": false,
            "waterstagnation": false,
            "unclearedgarbage": false,
            "police": false,
            "fatherinlaw": false,
            "motherinlaw": false,
            "sisbroinlaw": false,
            "sondaughter": false,
            "othermem": false,
            "husfriends": false,
            "goons": false,
            "partners": false,
            "husband": false,
            "anyfamilymember": false,
            "daughterinlaw": false,
            "landlord": false,
            "familymember": false,
            "reported": false,
            "reportedpolice": false,
            "reportedngos": false,
            "reportedfriends": false,
            "reportedplv": false,
            "shshg": false,
            "hfs": false,
            "reportedchampions": false,
            "doctornurse": false,
            "reportedlegalaid": false,
            "grampanchayat": false,
            "timetorespond": false,
            "referredhf": false,
            "referredmedicalcare": false,
            "referredcomanager": false,
            "referredheadofgrampanchayat": false,
            "referredplv": false,
            "referredlegalaid": false,
            "referredpolice": false,
            "referredsgshg": false,
            "emergencyactionclinic": false,
            "referredhospital": false,
            "complaintmade": false,
            "communitymobilized": false,
            "prioritizedflorsawfaction": false,
            "createdDate": new Date(),
            "createdBy": $window.sessionStorage.userId,
            "createdByRole": $window.sessionStorage.roleId,
            "lastModifiedDate": new Date(),
            "lastModifiedBy": $window.sessionStorage.userId,
            "lastModifiedByRole": $window.sessionStorage.roleId,
            "countryId": $window.sessionStorage.countryId,
            "stateId": $window.sessionStorage.stateId,
            "districtId": $window.sessionStorage.districtId,
            "siteId": $window.sessionStorage.siteId.split(",")[0],
            "multiplemembers": '',
            "deleteFlag": false
        };

        /**************************************** Member *******************************************/

        $scope.UserLanguage = $window.sessionStorage.language;

        Restangular.all('typeofincidents?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (incident) {
            $scope.typeofincidents = incident;

            if ($scope.UserLanguage == 1) {
                $scope.reportincident.incidenttype = incident[1].id;
            } else {
                $scope.reportincident.incidenttype = incident[1].parentId;
            }
        });

        Restangular.all('severityofincidents?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (responseseservity) {
            $scope.severityofincidents = responseseservity;

            if ($scope.UserLanguage == 1) {
                $scope.reportincident.severity = responseseservity[1].id;
            } else {
                $scope.reportincident.severity = responseseservity[1].parentId;
            }
        });

        Restangular.all('currentstatusofcases?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (response) {
            $scope.currentstatusofcases = response;

            if ($scope.UserLanguage == 1) {
                $scope.reportincident.currentstatus = response[0].id;
            } else {
                $scope.reportincident.currentstatus = response[0].parentId;
            }
        });


        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.hideAssigned = true;

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.reportincident.associatedHF = $window.sessionStorage.userId;

            });
        } else {

            $scope.hideAssigned = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.reportincident.associatedHF = urs[0].id;
            });
        }


        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

        } else {
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        }
        Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
            $scope.members = mems;

            angular.forEach($scope.members, function (member, index) {
                member.index = index;
            });
        });

        Restangular.all('reportincidentfollowups?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (follow) {
            $scope.reportincidentfollowups = follow;

            if ($scope.UserLanguage == 1) {
                $scope.reportincident.followupneeded = follow[0].id;
            } else {
                $scope.reportincident.followupneeded = follow[0].parentId;
            }
        });

        $scope.$watch('reportincident.incidenttype', function (newValue, oldValue) {
            if (newValue === oldValue || newValue === '') {
                return;
            } else {
                if (newValue == 1) {
                    $scope.Violence = true;
                    $scope.MedicalEmergency = false;
                    $scope.SAWFIssue = false;

                    $scope.reportincident.reported = false;

                    $scope.reportincident.wound = false;
                    $scope.reportincident.cut = false;
                    $scope.reportincident.severepain = false;
                    $scope.reportincident.bleeding = false;
                    $scope.reportincident.immobile = false;
                    $scope.reportincident.passingout = false;
                    $scope.reportincident.uncoveredfood = false;
                    $scope.reportincident.waterstagnation = false;
                    $scope.reportincident.unclearedgarbage = false;

                    $scope.reportincident.emergencyactionclinic = false;
                    $scope.reportincident.referredhospital = false;

                    $scope.reportincident.complaintmade = false;
                    $scope.reportincident.communitymobilized = false;
                    $scope.reportincident.prioritizedflorsawfaction = false;

                    if ($scope.reportincident.co == false) {
                        $scope.Showmember = false;
                        $scope.ShowmemberOne = true;
                        $scope.reportincident.multiplemembers = '';
                        $scope.reportincident.multiplemembers = null;

                    } else if ($scope.reportincident.co == true) {
                        $scope.Showmember = true;
                        $scope.ShowmemberOne = false;
                        $scope.reportincident.multiplemembers = '';
                        $scope.reportincident.multiplemembers = null;
                    }

                } else if (newValue == 2) {
                    $scope.Violence = false;
                    $scope.MedicalEmergency = true;
                    $scope.SAWFIssue = false;

                    $scope.reportincident.reported = false;

                    $scope.reportincident.physical = false;
                    $scope.reportincident.sexual = false;
                    $scope.reportincident.childrelated = false;
                    $scope.reportincident.emotional = false;
                    $scope.reportincident.propertyrelated = false;
                    $scope.reportincident.mental = false;
                    $scope.reportincident.uncoveredfood = false;
                    $scope.reportincident.waterstagnation = false;
                    $scope.reportincident.unclearedgarbage = false;

                    $scope.reportincident.police = false;
                    $scope.reportincident.fatherinlaw = false;
                    $scope.reportincident.motherinlaw = false;
                    $scope.reportincident.sisbroinlaw = false;
                    $scope.reportincident.sondaughter = false;
                    $scope.reportincident.othermem = false;
                    $scope.reportincident.husfriends = false;
                    $scope.reportincident.goons = false;
                    $scope.reportincident.partners = false;
                    $scope.reportincident.husband = false;
                    $scope.reportincident.anyfamilymember = false;
                    $scope.reportincident.daughterinlaw = false;
                    $scope.reportincident.landlord = false;
                    $scope.reportincident.familymember = false;

                    $scope.reportincident.referredhf = false;
                    $scope.reportincident.referredmedicalcare = false;
                    $scope.reportincident.referredcomanager = false;
                    $scope.reportincident.referredheadofgrampanchayat = false;
                    $scope.reportincident.referredplv = false;
                    $scope.reportincident.referredlegalaid = false;
                    $scope.reportincident.referredpolice = false;
                    $scope.reportincident.referredsgshg = false;

                    $scope.reportincident.complaintmade = false;
                    $scope.reportincident.communitymobilized = false;
                    $scope.reportincident.prioritizedflorsawfaction = false;

                    if ($scope.reportincident.co == false) {
                        $scope.Showmember = false;
                        $scope.ShowmemberOne = true;
                        $scope.reportincident.multiplemembers = '';
                        $scope.reportincident.multiplemembers = null;
                    } else if ($scope.reportincident.co == true) {
                        $scope.Showmember = true;
                        $scope.ShowmemberOne = false;
                        $scope.reportincident.multiplemembers = '';
                        $scope.reportincident.multiplemembers = null;
                    }

                } else if (newValue == 3) {
                    $scope.Violence = false;
                    $scope.MedicalEmergency = false;
                    $scope.SAWFIssue = true;
                    $scope.Showmember = false;
                    $scope.ShowmemberOne = false;
                    $scope.reportincident.multiplemembers = '';
                    $scope.reportincident.multiplemembers = null;

                    $scope.reportincident.reported = false;

                    $scope.reportincident.physical = false;
                    $scope.reportincident.sexual = false;
                    $scope.reportincident.childrelated = false;
                    $scope.reportincident.emotional = false;
                    $scope.reportincident.propertyrelated = false;
                    $scope.reportincident.mental = false;
                    $scope.reportincident.wound = false;
                    $scope.reportincident.cut = false;
                    $scope.reportincident.severepain = false;
                    $scope.reportincident.bleeding = false;
                    $scope.reportincident.immobile = false;
                    $scope.reportincident.passingout = false;

                    $scope.reportincident.police = false;
                    $scope.reportincident.fatherinlaw = false;
                    $scope.reportincident.motherinlaw = false;
                    $scope.reportincident.sisbroinlaw = false;
                    $scope.reportincident.sondaughter = false;
                    $scope.reportincident.othermem = false;
                    $scope.reportincident.husfriends = false;
                    $scope.reportincident.goons = false;
                    $scope.reportincident.partners = false;
                    $scope.reportincident.husband = false;
                    $scope.reportincident.anyfamilymember = false;
                    $scope.reportincident.daughterinlaw = false;
                    $scope.reportincident.landlord = false;
                    $scope.reportincident.familymember = false;

                    $scope.reportincident.referredhf = false;
                    $scope.reportincident.referredmedicalcare = false;
                    $scope.reportincident.referredcomanager = false;
                    $scope.reportincident.referredheadofgrampanchayat = false;
                    $scope.reportincident.referredplv = false;
                    $scope.reportincident.referredlegalaid = false;
                    $scope.reportincident.referredpolice = false;
                    $scope.reportincident.referredsgshg = false;

                    $scope.reportincident.emergencyactionclinic = false;
                    $scope.reportincident.referredhospital = false;
                }
            }
        });

        $scope.$watch('reportincident.co', function (newValue, oldValue) {
            if (newValue === oldValue || newValue === '') {
                return;
            } else {
                console.log('newValue', newValue);
                if (newValue == false && ($scope.reportincident.incidenttype == 1 || $scope.reportincident.incidenttype == 2)) {
                    $scope.Showmember = false;
                    $scope.ShowmemberOne = true;
                    $scope.reportincident.multiplemembers = '';
                    $scope.reportincident.multiplemembers = null;
                } else if (newValue == true && ($scope.reportincident.incidenttype == 1 || $scope.reportincident.incidenttype == 2)) {
                    $scope.Showmember = true;
                    $scope.ShowmemberOne = false;
                    $scope.reportincident.multiplemembers = '';
                    $scope.reportincident.multiplemembers = null;
                }
            }
        });

        $scope.$watch('reportincident.reported', function (newValue, oldValue) {
            if (newValue === oldValue || newValue === '') {
                return;
            } else {
                if (newValue == false) {
                    $scope.hideReportedTo = false;
                    $scope.reportincident.reportedpolice = false;
                    $scope.reportincident.reportedngos = false;
                    $scope.reportincident.reportedfriends = false;
                    $scope.reportincident.reportedplv = false;
                    $scope.reportincident.shshg = false;
                    $scope.reportincident.hfs = false;
                    $scope.reportincident.reportedchampions = false;
                    $scope.reportincident.doctornurse = false;
                    $scope.reportincident.reportedlegalaid = false;
                    $scope.reportincident.grampanchayat = false;
                    $scope.reportincident.timetorespond = '';
                } else if (newValue == true) {
                    $scope.hideReportedTo = true;
                }
            }
        });

        $scope.$watch('reportincident.currentstatus', function (newValue, oldValue) {
            if (newValue === oldValue || newValue === '') {
                return;
            } else {
                if (newValue == 5 || newValue == 9) {
                    $scope.followupHide = false;
                    $scope.dateofclosureHide = true;
                    $scope.reportincident.followupneeded = null;
                    $scope.reportincident.followupdate = null;
                    $scope.reportincident.dateofclosure = new Date();
                } else {
                    $scope.followupHide = true;
                    $scope.dateofclosureHide = false;
                    $scope.reportincident.followupneeded = 1;
                    var sevendays = new Date();
                    sevendays.setDate(sevendays.getDate() + 7);
                    $scope.reportincident.followupdate = sevendays;
                }
            }
        });

        $scope.validatestring = '';

        $scope.SaveReportIncident = function () {

            //            if ($window.sessionStorage.roleId + "" === "3") {
            //                $scope.reportincident.associatedHF = $window.sessionStorage.userId;
            //            } else {
            //                $scope.reportincident.associatedHF = null;
            //            }

            if ($scope.reportincident.incidenttype == '' || $scope.reportincident.incidenttype == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Incident Type';

            } else if ($scope.reportincident.associatedHF == '' || $scope.reportincident.associatedHF == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Assigned To';

            } else if ($scope.reportincident.severity == '' || $scope.reportincident.severity == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Severity of Incident';

            } else if ($scope.reportincident.incidenttype == 1 || $scope.reportincident.incidenttype == 2) {
                if ($scope.reportincident.multiplemembers == '' || $scope.reportincident.multiplemembers == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Members';
                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.toggleLoading();
                $scope.message = $scope.RILanguage.reportIncidentCreated;
                $scope.submitDisable = true;
                Restangular.all('reportincidents').post($scope.reportincident).then(function (Response) {

                    $scope.auditlog.rowId = Response.id;

                    Restangular.all('audittrials').post($scope.auditlog).then(function (respAudit) {

                        // console.log('Response', Response);
                        $scope.modalInstanceLoad.close();
                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        console.log('reloading...');

                        setInterval(function () {
                            window.location = '/reportincident-list';
                        }, 1500);
                    });
                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.reportincident.incidentdate = new Date();
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        $scope.reportincident.followupdate = sevendays;


        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.open2 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepickerfollowup' + index).focus();
            });
            $scope.reportincident.followupopened = true;
        };

        $scope.reportincidentfollowupopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.reportincident.followupdatepick = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end///

    })

.directive('modal2', function () {
    return {
        template: '<div class="modal fade" data-backdrop="static">' +
            '<div class="modal-dialog modal-sm" style="position:absolute;left:25px;bottom:0px;" >' +
            '<div class="modal-content" style="margin:0px;background:#74b53f;width:370px;">' +
            '<div class="row" style="margin:0px;">' +
            '<div class="col-xs-2">' +
            '<img src="images/piclinks/tick2.png" style="padding-top: 2em;padding-left: .5em;">' +
            '</div>' +
            '<div class="col-xs-8" style="padding:0px;">' +
            '<div class="modal-header"  style="padding-left:15px;padding-bottom:0px;border-color:#74b53f;">' +
            // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
            '<h4 class="modal-title" style="color:#ffffff;font-weight:600;" >{{ title1 }}</h4>' +
            '</div>' +
            '<div class="modal-body" style="color:#ffffff;font-weight:600;"  ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title1 = attrs.title1;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});
