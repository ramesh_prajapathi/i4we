'use strict';
angular.module('secondarySalesApp').controller('TermCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $modal) {
    /*********/
    if ($window.sessionStorage.roleId != 1) {
        window.location = "/";
    }
    $scope.showForm = function () {
        var visible = $location.path() === '/noofterms/create' || $location.path() === '/noofterms/' + $routeParams.id;
        return visible;
    };
    $scope.isCreateView = function () {
        if ($scope.showForm()) {
            var visible = $location.path() === '/noofterms/create';
            return visible;
        }
    };
    $scope.hideCreateButton = function () {
        var visible = $location.path() === '/noofterms/create' || $location.path() === '/noofterms/' + $routeParams.id;
        return visible;
    };
    $scope.hideSearchFilter = function () {
        var visible = $location.path() === '/noofterms/create' || $location.path() === '/noofterms/' + $routeParams.id;
        return visible;
    };
    /*********/
    /*********************************** Pagination *******************************************
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    else {
        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        //console.log('$scope.countryId From Landing', $scope.pageSize);
    }
    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
    };
    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
        console.log('mypage', mypage);
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
    if ($window.sessionStorage.prviousLocation != "partials/noofterms") {
        $window.sessionStorage.myRoute = '';
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
        $scope.currentpage = 1;
        $scope.pageSize = 25;
    }
    //  $scope.noofterms = Restangular.all('noofterms').getList().$object;
//    if ($routeParams.id) {
//        $scope.message = 'Number of Term has been Updated!';
//        Restangular.one('noofterms', $routeParams.id).get().then(function (noofterm) {
//            $scope.original = noofterm;
//            $scope.noofterm = Restangular.copy($scope.original);
//        });
//    }
//    else {
//        $scope.message = 'Number of Term has been Created!';
//    }
//    $scope.Search = $scope.name;
    
    $scope.message = 'Number of Term has been Updated!';*/
    //    $scope.myChange = function(){
    //        
    //    var sValue = document.getElementById("Validate").value;
    //        var pattern2 = /^\d{5}([\-]\d{4})?$/;
    //        var pattern = /^[0-9@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/;
    //        if (pattern.test(sValue)) {
    //            alert("Your Input is valid : "+sValue);
    //            return true;
    //        } 
    //            alert("Input except Number & Special Characters only !");
    //            return false;
    //    }
    /******************************** INDEX *******************************************/
    $scope.zn = Restangular.all('noofterms?filter[where][deleteflag]=false').getList().then(function (zn) {
        $scope.noofterms = zn[0];
        //console.log('$scope.noofterms',$scope.noofterms);
        $scope.original = zn[0];;
        $scope.noofterm = Restangular.copy($scope.original);
        //        angular.forEach($scope.noofterms, function (member, index) {
        //            member.index = index + 1;
        //        });
    });
    /********************************************* SAVE *******************************************/
    $scope.noofterm = {
        "name": ''
        , "deleteflag": false
    };
    /*  $scope.CreateArray1 = function () {}
      $scope.CreateArray = function (st, ed) {
          console.log('start', st);
          console.log('end', ed);
          $scope.CreateArray1();
          $scope.item = [];
          for (var i = st; i <= ed; i++) {
              $scope.item.push = ({
                  name: ''
                  , deleteflag: false
              })
          }
          console.log('$scope.item', $scope.item)
      }*/
    $scope.validatestring = '';
    $scope.submitDisable = false;
    /*  $scope.Save = function () {
          document.getElementById('name').style.border = "";
          if ($scope.noofterm.name == '' || $scope.noofterm.name == null) {
              $scope.validatestring = $scope.validatestring + 'Please Enter No of Term';
              document.getElementById('name').style.borderColor = "#FF0000";
          }
          else if ($scope.noofterm.name.indexOf('-') == -1) {
              $scope.validatestring = $scope.validatestring + 'No of Term Consist a Range, E.g. 1-10';
              document.getElementById('name').style.borderColor = "#FF0000";
          }
          if ($scope.validatestring != '') {
              $scope.toggleValidation();
              $scope.validatestring1 = $scope.validatestring;
              $scope.validatestring = '';
          }
          else {
              $scope.submitDisable = true;
              $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
              $scope.noofterms.post($scope.noofterm).then(function () {
                  window.location = '/noofterms';
                  console.log('Save2', $scope.noofterm);
              });
          };
      };*/
    $scope.modalTitle = 'Thank You';
    $scope.message = 'Number of Term has been Updated!';
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    /***************************************************** UPDATE *******************************************/
   $scope.stakeholderdataModal = false;
    $scope.Update = function () {
        var arr2 = [];
        var str = $scope.noofterm.name;
        var arr2 = str.split('-');  
        console.log('arr2',arr2);
        if (arr2.length > 2) {
            $scope.AlertMessage = 'Missing or wrong format';
            $scope.openOneAlert();
            $scope.noofterm = Restangular.copy($scope.original);
            return;
        }
        if ((isNaN(arr2[0])) || (isNaN(arr2[1]))) {
            $scope.AlertMessage = 'Missing or wrong format';
            $scope.openOneAlert();
            $scope.noofterm = Restangular.copy($scope.original);
            return;
        }
        if (parseFloat(arr2[0]) > parseInt(arr2[1])) {
            
             $scope.AlertMessage = 'Start number should be lesser than the end number';
            $scope.openOneAlert();
            //console.log('Condition3');
            $scope.noofterm = Restangular.copy($scope.original);
            return;
        }
        if (arr2[0].length == 0 || arr2[1].length == 0) {
            $scope.AlertMessage = 'Wrong format';
            $scope.openOneAlert();
            $scope.noofterm = Restangular.copy($scope.original);
            return;
        }
        if (arr2[0] == 0 || arr2[1] == 0) {
            $scope.AlertMessage = 'Wrong format';
            $scope.openOneAlert();
            $scope.noofterm = Restangular.copy($scope.original);
            return;
        }
        
        Restangular.one('noofterms', $scope.noofterm.id).customPUT($scope.noofterm).then(function () {
            console.log('noofterm Saved');
            //$scope.stakeholderdataModal != $scope.stakeholderdataModal;
            $scope.AlertMessage = 'Number of Term has been Updated!';
            $scope.openOneAlert2();
            window.location = '/';
        });
        
    }
    
      $scope.openOneAlert = function () {
            $scope.modalOneAlert = $modal.open({
                animation: true
                , templateUrl: 'template/AlertModal.html'
                , scope: $scope
                , backdrop: 'static'
                , keyboard: false
                , size: 'sm'
                , windowClass: 'modal-danger'
            });
        };
     $scope.openOneAlert2 = function () {
            $scope.modalOneAlert = $modal.open({
                animation: true
                , templateUrl: 'template/AlertModal2.html'
                , scope: $scope
                , backdrop: 'static'
                , keyboard: false
                , size: 'sm'
                , windowClass: 'modal-sucess'
            });
        };
        $scope.okAlert = function () {
            $scope.modalOneAlert.close();
        };
 /*   $scope.Update4 = function () {
        var shortDateRex = /^\d{1,2}(?-)\d{2}$/;
        var pattern = /^[0-9@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/;
        var pattern2 = /^[0-9@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/;
        if ($scope.noofterm.name.indexOf('-') == -1) {
            console.log('gatat h')
        }
        else {
            console.log('Shi h')
        }
    }
    $scope.mypattern = /^[0-9\-\?]+$/;
    $scope.Update1 = function () {
            //console.log('Update',$scope.noofterm.name)
            document.getElementById('name').style.border = "";
            var pattern2 = /^\d{5}([\-]\d{4})?$/;
            var pattern = /^[0-9@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/;
            var pattern3 = /[0-9]*\.?[0-9]
            var str = $scope.noofterm.name
            var arr = str.split('-');
            var a1 = parseInt(arr[0]);
            var a2 = parseInt(arr[1]);
            console.log('a1', a1);
            console.log('a2', a2);
            if ($scope.noofterm.name == '' || $scope.noofterm.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter No of Term';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            else if (a1 > a2) {
                $scope.validatestring = $scope.validatestring + 'The range must be from lower to higher number';
            }
             else if ($scope.noofterm.name.indexOf('-') != -1 && pattern.test($scope.noofterm.name)) {
                 console.log('shi h') 
             } else {
                 $scope.validatestring = $scope.validatestring + 'No of Term consist a range, E.g. 1-10 and  Charecter is not allowed' ;
                 document.getElementById('name').style.borderColor = "#FF0000"; 
             }
            else if (pattern3.test($scope.noofterm.name) && pattern.test($scope.noofterm.name)) {
                console.log('shi h')
            }
            else {
                $scope.validatestring = $scope.validatestring + 'No of Term consist a range, E.g. 1-10 and  Charecter is not allowed';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            }
            else {
                console.log('Update', $scope.noofterm);
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                 Restangular.one('noofterms', $routeParams.id).customPUT($scope.noofterm).then(function () {
                    console.log('noofterm Saved');
                    window.location = '/noofterms';
                });
            }
        }*/
        /*  $scope.Update1 = function () {
              document.getElementById('name').style.border = "";
              if ($scope.noofterm.name == '' || $scope.noofterm.name == null) {
                  $scope.validatestring = $scope.validatestring + 'Please Enter noofterm';
                  document.getElementById('name').style.borderColor = "#FF0000";
              }
              else if ($scope.noofterm.name.indexOf('-') == -1) {
                  $scope.validatestring = $scope.validatestring + 'No of Term Consist a Range, E.g. 1-10';
                  document.getElementById('name').style.borderColor = "#FF0000";
              }
            
              if ($scope.validatestring != '') {
                  $scope.toggleValidation();
                  $scope.validatestring1 = $scope.validatestring;
                  $scope.validatestring = '';
              }
              else {
                  $scope.submitDisable = true;
                  $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                  Restangular.one('noofterms', $routeParams.id).customPUT($scope.noofterm).then(function () {
                      console.log('noofterm Saved');
                      window.location = '/noofterms';
                  });
              }
          };*/
      
});