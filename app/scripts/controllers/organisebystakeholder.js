'use strict';

angular.module('secondarySalesApp')
	.controller('organisebystakeholderCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
		/*********/
		//$scope.modalTitle = 'Thank You';
		//$scope.message = 'organisebystakeholder has been created!';
		$scope.showForm = function () {
			var visible = $location.path() === '/organisebystakeholder/create' || $location.path() === '/organisebystakeholder/' + $routeParams.id;
			return visible;
		};
		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/organisebystakeholder/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/organisebystakeholder/create' || $location.path() === '/organisebystakeholder/' + $routeParams.id;
			return visible;
		};
		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/organisebystakeholder/create' || $location.path() === '/organisebystakeholder/' + $routeParams.id;
			return visible;
		};
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}


		/*********************************** Pagination *******************************************/

		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} /*else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
		}*/

		if ($window.sessionStorage.prviousLocation != "partials/organisebystakeholder") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}


		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};

		/*********************************** INDEX *******************************************/

		$scope.part = Restangular.all('organisebystakeholders?filter[where][deleteflag]=false').getList().then(function (part) {
			$scope.organisebystakeholders = part;
			$scope.organisebystakeholderId = $window.sessionStorage.sales_organisebystakeholderId;
			angular.forEach($scope.organisebystakeholders, function (member, index) {
				member.index = index + 1;

				$scope.TotalTodos = [];
				$scope.TotalTodos.push(member);
			});
		});

		/*-------------------------------------------------------------------------------------*/
		$scope.organisebystakeholder = {
			deleteflag: false
		};

		$scope.statecodeDisable = false;
		$scope.membercountDisable = false;
		if ($routeParams.id) {
			$scope.message = 'Stakeholder Organiser has been Updated!';
			$scope.statecodeDisable = true;
			$scope.membercountDisable = true;
			Restangular.one('organisebystakeholders', $routeParams.id).get().then(function (organisebystakeholder) {
				$scope.original = organisebystakeholder;
				$scope.organisebystakeholder = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Stakeholder Organiser has been Created!';
		}
		/************* SAVE *******************************************/

		$scope.submitDisable = false;
		$scope.validatestring = '';
		$scope.Saveorganisebystakeholder = function (clicked) {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.organisebystakeholder.name == '' || $scope.organisebystakeholder.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.organisebystakeholder.hnname == '' || $scope.organisebystakeholder.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.organisebystakeholder.knname == '' || $scope.organisebystakeholder.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.organisebystakeholder.taname == '' || $scope.organisebystakeholder.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.organisebystakeholder.tename == '' || $scope.organisebystakeholder.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.organisebystakeholder.mrname == '' || $scope.organisebystakeholder.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.all('organisebystakeholders').post($scope.organisebystakeholder).then(function (Response) {
					window.location = '/organisebystakeholder';

				});
			}
		};

		/***************************** UPDATE *******************************************/
		$scope.Updateorganisebystakeholder = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.organisebystakeholder.name == '' || $scope.organisebystakeholder.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.organisebystakeholder.hnname == '' || $scope.organisebystakeholder.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.organisebystakeholder.knname == '' || $scope.organisebystakeholder.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.organisebystakeholder.taname == '' || $scope.organisebystakeholder.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.organisebystakeholder.tename == '' || $scope.organisebystakeholder.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.organisebystakeholder.mrname == '' || $scope.organisebystakeholder.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.one('organisebystakeholders', $routeParams.id).customPUT($scope.organisebystakeholder).then(function () {
					window.location = '/organisebystakeholder';
				});
			}
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/*---------------------------Delete---------------------------------------------------*/

		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('organisebystakeholders/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}


	});
