'use strict';

angular.module('secondarySalesApp')
	.controller('COEditCtrl', function ($scope, Restangular, $routeParams, $filter, $timeout, $window) {
		$scope.isCreateView = false;
		$scope.fscodedisable = true;
		$scope.fsnamedisable = true;
		$scope.hfdflagDisabled = true;
		$scope.heading = 'Facility/CO Edit';

		$scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.modalTitle = 'Thank You';
		$scope.message = 'Facility Manager/HDF has been Updated!';

		if ($routeParams.id) {
			Restangular.one('comembers', $routeParams.id).get().then(function (partner) {
				$scope.original = partner;

				if (partner.helplineone != null) {
					$scope.anotherHelplinenumber = 1;
				}
				if (partner.helplinetwo != null) {
					$scope.anotherHelplinenumber = 2;
				}
				if (partner.helplinethree != null) {
					$scope.anotherHelplinenumber = 3;
				}

				$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + $scope.original.state + '&filter[where][deleteflag]=false').getList().then(function (salearea) {
					$scope.salesAreas = salearea;
					$scope.partner = Restangular.copy($scope.original);
				});

			});
		};

		/*
			$scope.$watch('partner.state', function (newValue, oldValue) {
				if (newValue === oldValue || newValue == '') {
					return;
				} else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
					return;
				} else {
					$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
				}
			});*/

		$scope.$watch('partner.facility', function (newValue, oldValue) {
			console.log('newValue', newValue);
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$scope.employees1 = Restangular.one('employees', newValue).get().then(function (response) {
					$scope.getStateId = response.stateId;
					$scope.gettDistricId = response.district;
					console.log('$scope.getStateId', response.stateId);

					$scope.salesAreas = Restangular.one('sales-areas?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.gettDistricId).get().then(function (responsesalesAreas) {
						$scope.districtname = responsesalesAreas[0].name;
						$scope.partner.district = responsesalesAreas[0].id;
						console.log('responseZone.name', $scope.partner.district);
					});

					$scope.zones = Restangular.one('zones?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.getStateId).get().then(function (responseZone) {
						$scope.statename = responseZone[0].name;
						$scope.partner.state = responseZone[0].id;
						console.log('responseZone.name', $scope.partner.state);
					});
				});

				$scope.zonalid = newValue;

			}
		});



		$scope.employees = Restangular.all('employees?filter[deleteflag]=false').getList().$object;
		/*
		$scope.retailers = Restangular.all('retailers').getList().$object;
		$scope.distributionRoutes = Restangular.all('distribution-routes').getList().$object;
		$scope.cities = Restangular.all('cities').getList().$object;
		$scope.states = Restangular.all('states').getList().$object;
		$scope.ims = Restangular.all('ims').getList().$object;
		$scope.retailercategories = Restangular.all('retailercategories').getList().$object;
		$scope.submitpartners = Restangular.all('comembers').getList().$object;
		$scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
		$scope.cocategories = Restangular.all('cocategories').getList().$object;
		*/
		$scope.partner = {
			stateId: '',
			latitude: '',
			longitude: '',
			address: '',
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId
		};
		$scope.groups = Restangular.one('groups', 8).get().then(function (group) {
			console.log(group);
			$scope.groupname = group.name;
			$scope.partner.groupId = group.id;
		});

		$scope.anotherhelplinenumber = 0;
		$scope.AnotherHelpline = function () {
			if ($scope.anotherhelplinenumber < 3) {
				$scope.anotherhelplinenumber++;
				console.log('$scope.anotherphonenumber', $scope.anotherhelplinenumber);
			} else {
				alert('Only Three Numbers Are Allowed');
			}

		}

		/************************************ Update *******************************************/
		Restangular.all('users?filter[where][employeeid]=' + $routeParams.id).getList().then(function (submituser) {
			$scope.getUserId = submituser[0].id;
		});
		console.log('$scope.partner.facility',$scope.partner.facility);
		console.log('$scope.partner.name',$scope.partner.facilnameity);
		$scope.Update = function () {
		/*	document.getElementById('name').style.border = "";
			if ($scope.partner.facility == '' || $scope.partner.facility == null || $scope.partner.facility == undefined) {
				$scope.validatestring = $scope.validatestring + 'Plese Select Facility Code';
			} else if ($scope.partner.name == '' || $scope.partner.name == null || $scope.partner.facility == undefined) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {*/
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.one('comembers/' + $routeParams.id).customPUT($scope.partner).then(function (subResponse) {
					$scope.user.mobile = subResponse.helpline;
					window.location = '/co-organisation';

					Restangular.one('users/' + $scope.getUserId).customPUT($scope.user).then(function (submituser) {
						//$scope.dataModal = !$scope.dataModal;
						//});
					});
				});
			
		};

		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

		//Datepicker settings start
		$scope.today = function () {
			$scope.dt = $filter('date')(new Date(), 'y-MM-dd');
		};
		$scope.today();

		$scope.showWeeks = true;
		$scope.toggleWeeks = function () {
			$scope.showWeeks = !$scope.showWeeks;
		};

		$scope.clear = function () {
			$scope.dt = null;
		};

		// Disable weekend selection
		$scope.disabled = function (date, mode) {
			return (mode === 'day' && (date.getDay() === 0));
		};

		$scope.toggleMin = function () {
			$scope.minDate = ($scope.minDate) ? null : new Date();
		};
		$scope.toggleMin();

		$scope.open = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();

			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.opened = true;
		};


		$scope.dateOptions = {
			'year-format': 'yy',
			'starting-day': 1
		};

		$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
		$scope.format = $scope.formats[0];
		//Datepicker settings end



		$scope.showMapModal = false;
		$scope.toggleMapModal = function () {
			$scope.mapcount = 0;

			///////////////////////////////////////////////////////MAP//////////////////////////



			var geocoder = new google.maps.Geocoder();

			function geocodePosition(pos) {
				geocoder.geocode({
					latLng: pos
				}, function (responses) {
					if (responses && responses.length > 0) {
						updateMarkerAddress(responses[0].formatted_address);
					} else {
						updateMarkerAddress('Cannot determine address at this location.');
					}
				});
			}

			function updateMarkerStatus(str) {
				document.getElementById('markerStatus').innerHTML = str;
			}

			function updateMarkerPosition(latLng) {
				$scope.partner.latitude = latLng.lat() + ',' + latLng.lng();
				$scope.partner.longitude = latLng.lng();

				document.getElementById('info').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');
			}

			function updateMarkerAddress(str) {
				document.getElementById('mapaddress').innerHTML = str;
			}
			var map;

			function initialize() {

				$scope.address = $scope.partner.address;
				// console.log('$scope.address', $scope.address);
				$scope.latitude = 21.0000;
				$scope.longitude = 78.0000;
				if ($scope.address.length > 0) {
					var addressgeocoder = new google.maps.Geocoder();
					addressgeocoder.geocode({
						'address': $scope.address
					}, function (results, status) {

						if (status == google.maps.GeocoderStatus.OK) {
							$scope.latitude = parseInt(results[0].geometry.location.lat());
							$scope.longitude = parseInt(results[0].geometry.location.lng());
							//console.log($scope.latitude, $scope.longitude);

							var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
							map = new google.maps.Map(document.getElementById('mapCanvas'), {
								zoom: 4,
								center: new google.maps.LatLng($scope.latitude, $scope.longitude),
								mapTypeId: google.maps.MapTypeId.ROADMAP
							});
							var marker = new google.maps.Marker({
								position: latLng,
								title: 'Point A',
								map: map,
								draggable: true
							});

							// Update current position info.
							updateMarkerPosition(latLng);
							geocodePosition(latLng);

							// Add dragging event listeners.
							google.maps.event.addListener(marker, 'dragstart', function () {
								updateMarkerAddress('Dragging...');
							});

							google.maps.event.addListener(marker, 'drag', function () {
								updateMarkerStatus('Dragging...');
								updateMarkerPosition(marker.getPosition());
							});

							google.maps.event.addListener(marker, 'dragend', function () {
								updateMarkerStatus('Drag ended');
								geocodePosition(marker.getPosition());
							});
						}
					});
				} else {
					$scope.latitude = 21.0000;
					$scope.longitude = 78.0000;

					var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
					map = new google.maps.Map(document.getElementById('mapCanvas'), {
						zoom: 4,
						center: new google.maps.LatLng($scope.latitude, $scope.longitude),
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					var marker = new google.maps.Marker({
						position: latLng,
						title: 'Point A',
						map: map,
						draggable: true
					});

					// Update current position info.
					updateMarkerPosition(latLng);
					geocodePosition(latLng);

					// Add dragging event listeners.
					google.maps.event.addListener(marker, 'dragstart', function () {
						updateMarkerAddress('Dragging...');
					});

					google.maps.event.addListener(marker, 'drag', function () {
						updateMarkerStatus('Dragging...');
						updateMarkerPosition(marker.getPosition());
					});

					google.maps.event.addListener(marker, 'dragend', function () {
						updateMarkerStatus('Drag ended');
						geocodePosition(marker.getPosition());
					});

				}


			}

			// Onload handler to fire off the app.
			//google.maps.event.addDomListener(window, 'load', initialize);
			initialize();

			window.setTimeout(function () {
				google.maps.event.trigger(map, 'resize');
				map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
				map.setZoom(4);
			}, 1000);


			$scope.SaveMap = function () {
				$scope.showMapModal = !$scope.showMapModal;
				console.log($scope.reportincident);
			};

			//console.log('fdfd');
			$scope.showMapModal = !$scope.showMapModal;
		};

		$scope.CancelMap = function () {
			if ($scope.mapcount == 0) {
				$scope.showMapModal = !$scope.showMapModal;
				$scope.mapcount++;
			}
		};

	});
