'use strict';

angular.module('secondarySalesApp')
    .controller('learningCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        $scope.modalTitle = 'Thank You';

        $scope.showForm = function () {
            var visible = $location.path() === '/learning/create' || $location.path() === '/learning/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/learning/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/learning/create' || $location.path() === '/learning/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/learning/create' || $location.path() === '/learning/' + $routeParams.id;
            return visible;
        };
        //		if ($window.sessionStorage.roleId != 1) {
        //			window.location = "/";
        //		}


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/learnings") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/
    
    /************* Focus ***************************************/


        /*if ($window.sessionStorage.roleId + "" === "3") {

           
            $scope.hideAddBtn = false;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                //  $scope.patient.associatedHF = $window.sessionStorage.userId;

            });
        } else {

          
            $scope.hideAddBtn = false;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                //   $scope.patient.associatedHF = $scope.shg.associatedHF;

            });

        }
      if ($window.sessionStorage.roleId + "" === "1") {
           $scope.hideAddBtn = true;
      }*/
    
        /************* Focus ***************************************/
    


        //$scope.learning.featured = 'no';
        Restangular.all('learningtypes').getList().then(function (response) {
            $scope.types = response;
        });
        //$scope.languages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;
        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (response) {
            $scope.languages = response;
            angular.forEach($scope.languages, function (member, index) {
                member.enabled = true;
            });
        });
        $scope.getType = function (id) {
                return Restangular.one('learningtypes', id).get().$object;

            }
            /*-------------------------------------------------------------------------------------*/
        $scope.learning = {
            lastmodifiedtime: new Date(),
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            deleteflag: false,
            featured: 'no',
            type: 1
        };
        $scope.language = {};
        $scope.statecodeDisable = false;
        $scope.membercountDisable = false;
        if ($routeParams.id) {
            console.log('i am working');
            $scope.message = 'Learning has been Updated!';
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('learnings', $routeParams.id).get().then(function (learning) {
                $scope.original = learning;
                $scope.learning = Restangular.copy($scope.original);
                if ($scope.original.language != null) {
                    $scope.selectedpillars = learning.language.split(",");
                }
                Restangular.all('languages').getList().then(function (response) {
                    $scope.languages
                    angular.forEach($scope.languages, function (member, index) {
                        if ($scope.selectedpillars.indexOf(member.id.toString()) == -1) {
                            member.enabled = false;
                        } else {
                            member.enabled = true;
                        }
                    });
                });

            });
        } else {
            $scope.message = 'Learning has been created!';
            $scope.part = Restangular.all('learnings?filter[where][deleteflag]=false').getList().then(function (part) {
                $scope.learnings = part;
                $scope.learningId = $window.sessionStorage.sales_learningId;
                angular.forEach($scope.learnings, function (member, index) {
                    member.index = index + 1;

                    $scope.TotalTodos = [];
                    $scope.TotalTodos.push(member);
                });
            });

        }
        //$scope.representativeHide = false;
        $scope.repDisable = true;
        $scope.$watch('learning.type', function (newValue, oldValue) {
            if (newValue == oldValue) {
                return;
            } else if (newValue == 1) {
                //$scope.representativeHide = false;
                $scope.repDisable = true;
                //$scope.learning.representative = null;
                //$scope.learning.learningurl = null;
            } else {
                //$scope.representativeHide = true;
                $scope.repDisable = false;
                //$scope.learning.representative = null;
                //$scope.learning.learningurl = null;
            }
        });

        /************* SAVE *******************************************/
        /*$scope.GetLanguage = function () {
            var checkedValue = null;
            var inputElements1 = document.getElementsByClassName('learningCheckbox');
            for (var i = 0; inputElements1[i]; ++i) {
                if (inputElements1[i].checked) {
                    if (checkedValue == null) {
                        checkedValue = inputElements1[i].value;
                    } else {
                        checkedValue = checkedValue + ',' + inputElements1[i].value;
                    }
                }
            }
            console.log('checkedValue', checkedValue);
        };
        */

        $scope.UrlTest = function () {
            console.log('$scope.learning.type',$scope.learning.type);
            if ($scope.learning.type == 1) {
                $scope.urlImage = $scope.learning.learningurl.split('=');
                console.log('url', $scope.urlImage[1]);
                $scope.learning.representative = 'https://img.youtube.com/vi/' + $scope.urlImage[1] + '/0.jpg';
                console.log('$scope.learning.representative',$scope.learning.representative);
            }
        }

        /****************************************************************************************************/
        /* var url = "http://www.youtube.com/watch?v=NLqASIXrVbY"
         var VID_REGEX = /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/
         var test = url.match(VID_REGEX)[1]
         console.log('video_id', url, url.length);

         var matches = $('#videoUrl').val().match(/http:\/\/(?:www\.)?youtube.*watch\?v=([a-zA-Z0-9\-_]+)/);
         if (matches) {
             alert('valid');
         } else {
             alert('Invalid');
         }*/

        /*----------------------------------------------------------------------------------*/

        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Savelearning = function (clicked) {


            document.getElementById('name').style.border = "";
            document.getElementById('learningulr').style.border = "";
            var checkedValue = null;
            var inputElements1 = document.getElementsByClassName('learningCheckbox');
            for (var i = 0; inputElements1[i]; ++i) {
                if (inputElements1[i].checked) {
                    if (checkedValue == null) {
                        checkedValue = inputElements1[i].value;
                    } else {
                        checkedValue = checkedValue + ',' + inputElements1[i].value;
                    }
                }
            }

            $scope.learning.language = checkedValue;
            //$scope.urlImage = $scope.learning.learningurl.split('=');
            //console.log('url', $scope.urlImage[1]);
            //$scope.learning.representative = 'https://img.youtube.com/vi/' + $scope.urlImage[1] + '/0.jpg';
            if ($scope.learning.name == '' || $scope.learning.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Learning Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            } else if ($scope.learning.type == '' || $scope.learning.type == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Learning Type';

            } else if ($scope.learning.learningurl == '' || $scope.learning.learningurl == null) {
                document.getElementById('learningurl').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please Enter Learning URL';
            } else if ($scope.learning.featured == '' || $scope.learning.featured == null) {
                document.getElementById('featured').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please select feature';
            } else if (checkedValue === null) {
                $scope.validatestring = $scope.validatestring + 'Please select language';
            }

            if ($scope.learning.type == 1) {
                var str = $scope.learning.learningurl;
                var res = str.match(/youtube/g);

                if (res === null || res === undefined) {
                    console.log('res', res)
                    $scope.validatestring = $scope.validatestring + 'Url is not valid';
                }
            }

            if ($scope.learning.type == 2) {
                var str = $scope.learning.learningurl;
                var res = str.match(/youtube/g);

                if (res != null || res != undefined) {
                    // console.log('res', res)
                    $scope.validatestring = $scope.validatestring + 'Url is not valid';
                }
            }

            //$scope.learning.representative = $scope.learning.learningurl.split('=');
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                //toaster.pop('success', "State has been created", null, null, 'trustedHtml');
                Restangular.all('learnings').post($scope.learning).then(function (znResponse) {
                    console.log('znResponse', znResponse);
                    window.location = '/learning';
                });
            }
        };

        $scope.callUrlValidate = function () {
            console.log('Validate');
        }

        /***************************** UPDATE *******************************************/
        $scope.Updatelearning = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('learningulr').style.border = "";
            var checkedValue = null;
            var inputElements1 = document.getElementsByClassName('learningCheckbox');
            for (var i = 0; inputElements1[i]; ++i) {
                if (inputElements1[i].checked) {
                    if (checkedValue == null) {
                        checkedValue = inputElements1[i].value;
                    } else {
                        checkedValue = checkedValue + ',' + inputElements1[i].value;
                    }
                }
            }
            $scope.learning.language = checkedValue;
            //$scope.urlImage = $scope.learning.learningurl.split('=');
            //console.log('url', $scope.urlImage[1]);
            //$scope.learning.representative = 'https://img.youtube.com/vi/' + $scope.urlImage[1] + '/0.jpg';
            if ($scope.learning.name == '' || $scope.learning.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Learning Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            } else if ($scope.learning.type == '' || $scope.learning.type == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Learning Type';


            } else if ($scope.learning.learningurl == '' || $scope.learning.learningurl == null) {
                document.getElementById('learningurl').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please Enter Learning URL';


            } else if ($scope.learning.featured == '' || $scope.learning.featured == null) {
                document.getElementById('featured').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please select feature';
            } else if (checkedValue === null) {
                $scope.validatestring = $scope.validatestring + 'Please select language';
            }
            if ($scope.learning.type == 1) {
                var str = $scope.learning.learningurl;
                var res = str.match(/youtube/g);

                if (res === null || res === undefined) {
                    console.log('res', res)
                    $scope.validatestring = $scope.validatestring + 'Url is not valid';
                }
            }

            if ($scope.learning.type == 2) {
                var str = $scope.learning.learningurl;
                var res = str.match(/youtube/g);

                if (res != null || res != undefined) {
                    // console.log('res', res)
                    $scope.validatestring = $scope.validatestring + 'Url is not valid';
                }
            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.one('learnings', $routeParams.id).customPUT($scope.learning).then(function (res) {
                    //console.log('res', res.representative);
                    window.location = '/learning';

                });
            }
        };
        /*---------------------------Delete---------------------------------------------------*/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('learnings/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };



    });

/********************************* Not In Use ************************


		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		
		$scope.validatestring = '';
		/*$scope.Savelearning = function () {
			document.getElementById('name').style.border = "";
			if ($scope.learning.name == '' || $scope.learning.name == null) {
				//$scope.learning.name = null;
				$scope.validatestring = $scope.validatestring + 'Please enter state name';
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			//	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				Restangular.all('learnings').post($scope.learning).then(function (res) {
					console.log('learning Saved', res);
					window.location = '/learnings';
				});
			}
		};*/


/****************************************************************/