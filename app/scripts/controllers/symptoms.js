'use strict';

angular.module('secondarySalesApp')
    .controller('SymptomsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 9) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/symptom/create' || $location.path() === '/symptom/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/symptom/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/symptom/create' || $location.path() === '/symptom/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/symptom/create' || $location.path() === '/symptom/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/symptoms-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.showenglishLang = true;

        $scope.$watch('symptom.name', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id) {
                return;
            } else {
                angular.forEach($scope.rowArray, function (member, index) {
                    member.index = index;
                    member.name = newValue;
                });
            }
        });

        $scope.$watch('symptom.language', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                } else {
                    $scope.showenglishLang = true;
                }
            }
        });
        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {

            $scope.message = 'Symptom has been Updated!';

            Restangular.one('symptoms', $routeParams.id).get().then(function (symptom) {
                $scope.original = symptom;
                $scope.symptom = Restangular.copy($scope.original);
            });

            Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablangs) {
                $scope.tablanguages1 = tablangs;
                $scope.tablanguages1.splice(0, 1);
                $scope.currValue = 0;

                Restangular.all('symptoms?filter[where][deleteFlag]=false&filter[where][parentId]=' + $routeParams.id + '&filter[where][parentFlag]=false').getList().then(function (intvls1) {
                    $scope.mtlangs1 = intvls1;

                    angular.forEach($scope.tablanguages1, function (member, index) {
                        member.index = index;

                        var data = $scope.mtlangs1.filter(function (arr) {
                            return arr.language == member.id
                        })[0];

                        if (data == undefined) {
                            $scope.rowArray.push({
                                language: member.id,
                                id: 0
                            });
                        } else {
                            $scope.rowArray.push(data);
                        }
                    });
                });
            });

        } else {
            $scope.message = 'Symptom has been Created!';
        }

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];

        /******************************** INDEX *******************************************/

        Restangular.all('symptoms?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (sy) {
            $scope.symptoms = sy;
            angular.forEach($scope.symptoms, function (member, index) {
                member.index = index + 1;

                Restangular.one('languages', member.language).get().then(function (lng) {
                    member.langname = lng.name;
                });
            });
        });

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (sym) {
            $scope.languages = sym;
        });

        Restangular.all('symptoms?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (sym) {
            $scope.englishsymptoms = sym;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        $scope.rowArray = [];

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablang) {
            $scope.tablanguages = tablang;
            $scope.tablanguages.splice(0, 1);
            $scope.currValue = 0;

            angular.forEach($scope.tablanguages, function (member, index) {
                member.index = index;

                if (!$routeParams.id) {
                    $scope.rowArray.push({
                        language: member.id,
                        name: ''
                    });
                }

                if (member.index == 0) {
                    member.active = true;
                    member.visible = true;
                } else {
                    member.active = false;
                    member.visible = false;
                }
            });
        });

        $scope.tabClick = function (row) {
            $scope.tablanguages[row.index].visible = true;
            if (row.index != 0) {
                $scope.tablanguages[0].visible = false;
            }
            $scope.currValue = row.index;
        };


        /********************************************* SAVE *******************************************/

        $scope.symptom = {
            "name": '',
            "language": 1,
            "deleteFlag": false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {

            document.getElementById('name').style.border = "";

            if ($scope.symptom.name == '' || $scope.symptom.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Symptom';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.symptom.parentId === '') {
                    delete $scope.symptom['parentId'];
                }

                $scope.submitDisable = true;
                $scope.symptom.parentFlag = $scope.showenglishLang;

                Restangular.all('symptoms').post($scope.symptom).then(function (response) {
                    $scope.langFunc(response.id);
                }, function (error) {
                    if (error.data.error.constraint === 'symptom_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.langcount = 0;

        $scope.langFunc = function (intervalId) {

            if ($scope.langcount < $scope.rowArray.length) {

                $scope.rowArray[$scope.langcount].parentFlag = false;
                $scope.rowArray[$scope.langcount].parentId = intervalId;
                $scope.rowArray[$scope.langcount].deleteFlag = false;

                Restangular.all('symptoms').post($scope.rowArray[$scope.langcount]).then(function () {
                    $scope.langcount++;
                    $scope.langFunc(intervalId);
                });

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/symptoms-list';
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {

            document.getElementById('name').style.border = "";

            if ($scope.symptom.name == '' || $scope.symptom.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Symptom';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.symptom.parentId === '') {
                    delete $scope.symptom['parentId'];
                }

                $scope.submitDisable = true;

                Restangular.one('symptoms', $routeParams.id).customPUT($scope.symptom).then(function (resp) {
                    $scope.langUpdateFunc(resp.id);
                }, function (error) {
                    if (error.data.error.constraint === 'symptom_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        $scope.langUpdatecount = 0;

        $scope.langUpdateFunc = function (intervalId) {

            if ($scope.langUpdatecount < $scope.rowArray.length) {

                if ($scope.rowArray[$scope.langUpdatecount].id == 0 || $scope.rowArray[$scope.langUpdatecount].id == "0") {

                    $scope.rowArray[$scope.langUpdatecount].parentFlag = false;
                    $scope.rowArray[$scope.langUpdatecount].parentId = intervalId;
                    $scope.rowArray[$scope.langUpdatecount].deleteFlag = false;
                    delete $scope.rowArray[$scope.langUpdatecount]['id'];

                    Restangular.all('symptoms').post($scope.rowArray[$scope.langUpdatecount]).then(function () {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId);
                    });

                } else {

                    Restangular.one('symptoms', $scope.rowArray[$scope.langUpdatecount].id).customPUT($scope.rowArray[$scope.langUpdatecount]).then(function (resp1) {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId);
                    });
                }

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/symptoms-list';
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
    
/******************************************************** DELETE *******************************************/
        
        $scope.Delete = function (id) {
            
            $scope.item = [{
                deleteFlag: true
            }]

            Restangular.one('symptoms/' + id).customPUT($scope.item[0]).then(function () {
                Restangular.all('symptoms?filter[where][parentId]=' + id).getList().then(function (stypes) {
                    $scope.deleteRows = stypes;
                    $scope.DeleteFunc();
                });
            });
        };

        $scope.updateFlag = {
            deleteFlag: true
        };

        $scope.DeleteCount = 0;

        $scope.DeleteFunc = function (id) {

            if ($scope.DeleteCount < $scope.deleteRows.length) {

                Restangular.one('symptoms/' + $scope.deleteRows[$scope.DeleteCount].id).customPUT($scope.updateFlag).then(function () {
                    $scope.DeleteCount++;
                    $scope.DeleteFunc();
                });

            } else {
                $route.reload();
            }
        };

    });
