'use strict';

angular.module('secondarySalesApp')
    .controller('TrackApplicationCtrl', function ($scope, Restangular, $window, $modal, $timeout, $route) {
        $scope.searchTrackApp = '';
        //$scope.todos = Restangular.all('todos?filter[where][todotype]=28' +'&filter[where][todotype]=29').getList().$object;
        $scope.todo = {
            status: 1,
            stateid: $window.sessionStorage.zoneId,
            state: $window.sessionStorage.zoneId,
            districtid: $window.sessionStorage.salesAreaId,
            district: $window.sessionStorage.salesAreaId,
            coid: $window.sessionStorage.coorgId,
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date()
        };

        $scope.follow = {};
        $scope.picker = {};
        $scope.cdids = {};
        $scope.schememaster = {};
        $scope.plhiv = {};
        $scope.condom = {};
        $scope.finliteracy = {};
        $scope.finplanning = {};
        $scope.increment = {};
        $scope.beneficiarycondom = {};
        $scope.RemainingPillars = 3;
        $scope.SSJPillar = 2;
        $scope.TotalTodos = [];
        $scope.reportincident = {};
        $scope.submitdocumentmasters = Restangular.all('schememasters');
        $scope.submittodos = Restangular.all('todos');

        if ($window.sessionStorage.roleId == 15) {
            /*$scope.tds = Restangular.all('todos?filter[where][todotype][inq]=28&filter[where][todotype][inq]=29' + '&filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (response) {
				$scope.printTodo = response;
				angular.forEach($scope.printTodo, function (member, index) {
					member.index = index;
				});
			});*/


            $scope.tds = Restangular.all('todos?filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (response) {
                $scope.printTodo = response;
                angular.forEach($scope.printTodo, function (member, index) {
                    member.index = index;
                });
            });
        };


        //$scope.schememaster = {};


        $scope.beneficiaries = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=null' + '&filter[where][facilityId]=' + $window.sessionStorage.coorgId).getList().$object;


        $scope.getMemberName = function (beneficiaryid) {
            return Restangular.one('beneficiaries', beneficiaryid).get().$object;
        };

        $scope.getApplicationStage = function (documentid) {
            return Restangular.one('schemestages', documentid).get().$object;
        };

        $scope.getSchemeDocName = function (docId) {
            return Restangular.one('documenttypes', docId).get().$object;
        };

        $scope.getSchemeName = function (scId) {
            return Restangular.one('schemes', scId).get().$object;
        };


        $scope.EditTodo = function (todotype, id, data, pillarid, beneficiaryid) {
            //console.log('data.documentflag',data.documentflag);
            if (data.documentflag == true) {
                Restangular.one('documenttypes', data.documentid).get().then(function (doc) {
                    $scope.SchemeOrDocumentname = doc.name;
                });
                if (data.reportincidentid != undefined && data.reportincidentid != '' && data.reportincidentid != null) {
                    $scope.report = Restangular.one('schememasters', data.reportincidentid).get().then(function (report) {

                        $scope.schemstage = Restangular.all('schemestages').getList().then(function (schemstage) {
                            $scope.schemestages = schemstage;
                            angular.forEach($scope.schemestages, function (member, index) {
                                member.index = index;
                                if (report.stage <= member.id) {
                                    member.enabled = false;
                                } else {
                                    member.enabled = true;
                                }
                            });
                        });
                        ////console.log('report',report);
                        $scope.schememaster = report;
                        $scope.todo = data;
                        $scope.openAW('lg');
                    });
                }
            } else {
                Restangular.one('schemes', data.documentid).get().then(function (sch) {
                    $scope.SchemeOrDocumentname = sch.name;
                });
                if (data.reportincidentid != undefined && data.reportincidentid != '' && data.reportincidentid != null) {
                    $scope.report = Restangular.one('schememasters', data.reportincidentid).get().then(function (report) {

                        $scope.schemstage = Restangular.all('schemestages').getList().then(function (schemstage) {
                            $scope.schemestages = schemstage;
                            angular.forEach($scope.schemestages, function (member, index) {
                                member.index = index;
                                if (report.stage <= member.id) {
                                    member.enabled = false;
                                } else {
                                    member.enabled = true;
                                }
                            });
                        });
                        ////console.log('report',report);
                        $scope.schememaster = report;
                        $scope.todo = data;
                        $scope.openAW('lg');
                    });
                }
            }
        }

        $scope.updateAW = function () {
            $scope.submitdocumentmasters.customPUT($scope.schememaster, $scope.schememaster.id).then(function (resp) {
                //console.log('$scope.documentmasters', $scope.schememaster);
                $scope.todo.reportincidentid = resp.id;
                $scope.todo.datetime = resp.datetime;
                $scope.todo.stage = resp.stage;
                $scope.submittodos.customPUT($scope.todo, $scope.todo.id).then(function () {
                    //console.log('$scope.todo', $scope.todo);
                    $route.reload();
                    $scope.okAW();
                });
            });

        };


        $scope.openAW = function (size) {
            $scope.modalAW = $modal.open({
                animation: true,
                templateUrl: 'template/ApplicationWorkflowMain.html',
                scope: $scope,
                backdrop: 'static',
                size: size

            });
        };


        $scope.okAW = function () {
            $scope.modalAW.close();
        };


        $scope.datetimehide = false;
        $scope.reponserec = true;
        $scope.delaydis = true;
        $scope.collectedrequired = true;
        $scope.$watch('schememaster.stage', function (newValue, oldValue) {
            //console.log('newValue', newValue);
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 15);
            if (newValue === oldValue) {
                return;
            } else if (newValue === '4') {
                $scope.reponserec = false;
                $scope.delaydis = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
            } else if (newValue === '5') {
                $scope.delaydis = false;
                $scope.reponserec = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.schememaster.datetime = sevendays;
            } else if (newValue === '2') {
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.schememaster.datetime = sevendays;
            } else if (newValue === '3') {
                $scope.schememaster.datetime = sevendays;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
            } else if (newValue === '7') {
                var sixmonth = new Date();
                sixmonth.setDate(sixmonth.getDate() + 180);
                $scope.schememaster.datetime = sixmonth;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
            } else if (newValue === '1') {
                $scope.schememaster.datetime = sevendays;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;

            } else if (newValue === '6' || newValue === '8' || newValue === '10' || newValue === '9') {
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;

            } else {
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.delay = null;
                //$scope.schememaster.datetime = '';
                $scope.datetimehide = false;
                $scope.rejectdis = true;
            }
        });


        $scope.rejectdis = true;
        $scope.$watch('schememaster.responserecieve', function (newValue, oldValue) {
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 180);
            if (newValue === oldValue) {
                return;
            } else if (newValue === 'Approved') {
                $scope.schememaster.datetime = sevendays;
                $scope.rejectdis = true;
            } else if (newValue === 'Rejected') {
                $scope.rejectdis = false;
                //$scope.schememaster.datetime = '';


            } else {
                //$scope.schememaster.datetime = '';
                $scope.rejectdis = true;
                $scope.schememaster.rejection = null;

                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.delay = null;
                //$scope.schememaster.datetime = '';
                $scope.datetimehide = false;
            }
        });


        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = new Date();
            $scope.picker.dtmax = new Date();

            //$scope.todo.datetime = $filter('date')(new Date(), 'y-MM-dd');

            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            //$scope.reportincident.followupdate = sevendays;
            $scope.todo.datetime = sevendays;

        };

        // var nextmonth = new Date();
        // nextmonth.setMonth(sevendays.getMonth + 1);
        // $scope.cdids.maxdate = nextmonth;

        var oneday = new Date();
        oneday.setDate(oneday.getDate() + 1);

        $scope.reportincidentdtmin = new Date();

        $scope.today();
        $scope.condom.dt = new Date();
        $scope.picker.dt = new Date();
        $scope.picker.selectdate = new Date();
        $scope.condom.dateasked = new Date();
        $scope.plhiv.month = new Date();
        $scope.plhiv.maxdate = new Date();
        $scope.cdids.availeddate = new Date();
        $scope.cdidsmaxdate = new Date();
        $scope.schememaster.datetime = new Date();
        $scope.increment.selectdate = new Date();
        //$scope.reportincident.incidentdate = new Date();
        //$scope.reportincident.dtmax = new Date();
        $scope.condom.dateasked = new Date();
        $scope.todomin = new Date();
        $scope.schemedocmaxdt = new Date();


        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();

        $scope.todoopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.todo.opened = true;
        };

        $scope.fsopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.fs.opened = true;
        };

        $scope.healthopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.health.opened = true;
        };

        $scope.spopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.sp.opened = true;
        };

        $scope.reportopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.reportincident.opened = true;
        };

        $scope.followopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.follow.opened = true;
        };

        $scope.pickeropen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.picker.opened = true;
        };

        $scope.cdidsopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.cdids.opened = true;
        };

        $scope.workflowopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.schememaster.opened = true;
        };

        $scope.plhivopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.plhiv.opened = true;
        };

        $scope.condomopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.condom.opened = true;
        };

        $scope.incrementopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.increment.opened = true;
        };

        $scope.folowupdateopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.schememaster.opened = true;
        };

        $scope.reportincidentopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.reportincident.opened = true;
        };

        $scope.reportincidentfollowupopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.reportincident.followupdatepick = true;
        };

        $scope.reportincidentcloser = {};
        $scope.reportincidentcloseropen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.reportincidentcloser.closedate = true;
        };





        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];
        //Datepicker settings end

    });