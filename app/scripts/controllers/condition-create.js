'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/
        //$scope.hideAddBtn = true;
        $scope.disablePatientAdd = true;
        //$window.sessionStorage.previous = '/condition/create';

        $scope.HideCreateButton = true;
        $scope.editDisable = false;
        $scope.confirmationModel = false;
        $scope.checkListModal = false;
        $scope.disableCaseClosed = true;
        $scope.disableAddBtn = false;
        $scope.ConditionLanguage = {};
        $scope.UserLanguage = $window.sessionStorage.language;
        $scope.hideDiagnosis = false;

        Restangular.one('conditionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.ConditionLanguage = langResponse[0];
            $scope.conditionHeading = langResponse[0].addNewCondition;
            $scope.message = langResponse[0].conditionSaved;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }




        $scope.auditArray = [];

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.hideAssigned = true;
            $scope.hideAddBtn = false;
            $scope.hideLabel = true;

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.condition.associatedHF = $window.sessionStorage.userId;

            });

            $scope.memberFilterCall = 'members?filter[where][deleteFlag]=false' + '&filter[where][associatedHF]=' + $window.sessionStorage.userId;

        } else {

            $scope.hideAssigned = false;
            $scope.hideAddBtn = true;
            $scope.hideLabel = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.condition.associatedHF = urs[0].id;

            });

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        }

        Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
            $scope.members = mems;
            $scope.condition.memberId = $window.sessionStorage.MemberId;
            // $scope.condition.memberId = $window.sessionStorage.ConditionMemberId;
            $scope.condition.memberId = $scope.condition.memberId;
            // $scope.hideSpan = true;
            //            angular.forEach($scope.members, function (member, index) {
            //                //member.hideSpan = true;
            //                //console.log('member', member);
            //            });
        });




        Restangular.all('conditions?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false' + '&filter[where][enabledFor]=true').getList().then(function (cn) {
            $scope.conditionsdsply = cn;

            if ($scope.UserLanguage == 1) {
                $scope.condition.condition = cn[0].id;
            } else {
                $scope.condition.condition = cn[0].parentId;
            }
        });

        Restangular.all('reasonforcancellations?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().then(function (rfc) {
            $scope.reasonforcancellations = rfc;

            if ($scope.UserLanguage == 1) {
                $scope.condition.reasonForCancel = rfc[0].id;
            } else {
                $scope.condition.reasonForCancel = rfc[0].parentId;
            }
        });

        $scope.openCheckList = function (id, status, index) {
            $scope.checkListModal = true;
            if (status == 3) {
                $scope.disableCheck = false;
            } else {
                $scope.disableCheck = false;
            }
        };

        $scope.checkvalid = [];

        $scope.saveChecklist = function () {
            // console.log($scope.checklists);

            $scope.checkvalid = [];

            angular.forEach($scope.checklists, function (value, index) {
                $scope.checkvalid.push(value.statusFlag);
            });

            // console.log($scope.checkvalid);
            $scope.checkListModal = false;
        };

        $scope.cancelCheckList = function () {

            for (var i = 0; i < $scope.checklists.length; i++) {
                $scope.checklists[i].statusFlag = $scope.checkvalid[i];
                // console.log($scope.checkvalid[i]);
            }

            $scope.checkListModal = false;
        };

        $scope.trailerArray = [];

        $scope.getSteps = function (value) {

            //  console.log('i am here', value);

            $scope.trailerArray = [];

            Restangular.all('conditionstatuses?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().then(function (csts) {

                $scope.conditionstatuses = csts;

                if ($window.sessionStorage.language == 1) {

                    Restangular.all('conditionsteps?filter={"where":{"and":[{"language":{"inq":[' + $window.sessionStorage.language + ']}},{"deleteFlag":{"inq":[false]}},{"id":{"inq":[' + value + ']}}]}}').getList().then(function (cs) {
                        $scope.conditionsteps = cs;
                        // console.log($scope.conditionsteps);

                        angular.forEach($scope.conditionsteps, function (value, index) {
                            value.index = index;

                            if ($scope.UserLanguage == 1) {
                                value.id = value.id;
                            } else {
                                value.id = value.parentId;
                            }

                            $scope.trailerArray.push({
                                step: value.name,
                                status: '',
                                date: '',
                                id: value.id,
                                enabled: true,
                                statuses: $scope.conditionstatuses
                            });

                            $scope.trailerArray[0].enabled = false;
                            $scope.trailerArray[0].status = 3;
                            $scope.trailerArray[0].date = new Date();
                        });

                        angular.forEach($scope.trailerArray[0].statuses, function (data, index) {
                            var enabledFor = data.enabledFor.split(",");
                            var myFlag = enabledFor.includes($scope.trailerArray[0].id.toString());

                            if (myFlag == true) {
                                data.enabled = false;
                            } else {
                                data.enabled = true;
                            }
                        });
                    });

                } else {

                    Restangular.all('conditionsteps?filter={"where":{"and":[{"language":{"inq":[' + $window.sessionStorage.language + ']}},{"deleteFlag":{"inq":[false]}},{"parentId":{"inq":[' + value + ']}}]}}').getList().then(function (cs) {
                        $scope.conditionsteps = cs;
                        // console.log($scope.conditionsteps);

                        angular.forEach($scope.conditionsteps, function (value, index) {
                            value.index = index;

                            if ($scope.UserLanguage == 1) {
                                value.id = value.id;
                            } else {
                                value.id = value.parentId;
                            }

                            $scope.trailerArray.push({
                                step: value.name,
                                status: '',
                                date: '',
                                id: value.id,
                                enabled: true,
                                statuses: $scope.conditionstatuses
                            });

                            $scope.trailerArray[0].enabled = false;
                            $scope.trailerArray[0].status = 3;
                            $scope.trailerArray[0].date = new Date();
                        });

                        angular.forEach($scope.trailerArray[0].statuses, function (data, index) {
                            var enabledFor = data.enabledFor.split(",");
                            var myFlag = enabledFor.includes($scope.trailerArray[0].id.toString());

                            if (myFlag == true) {
                                data.enabled = false;
                            } else {
                                data.enabled = true;
                            }
                        });
                    });

                }

                //  Restangular.all('conditionsteps?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().then(function (cs) {

            });
        };

        $scope.addDiagnosis = function (id, index) {
            $scope.diagnosisArray.push({
                diagnosis: '',
                value: '',
                conditiondiagnosis: $scope.conditiondiagnosis
            });
        };

        $scope.removeDiagnosis = function (id, index) {
            $scope.diagnosisArray.splice(index, 1);
        };

        $scope.diagnosisChange = function (id, index) {

            if ($window.sessionStorage.language == 1) {
                Restangular.one('conditiondiagnosis', id).get().then(function (cdgs) {
                    $scope.diagnosisArray[index].name = cdgs.name;
                    $scope.diagnosisArray[index].metric = cdgs.metric;
                });
            } else {
                Restangular.one('conditiondiagnosis/findOne?filter[where][parentId]=' + id + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').get().then(function (cdgs) {
                    $scope.diagnosisArray[index].name = cdgs.name;
                    console.log($scope.diagnosisArray[index].name);
                    $scope.diagnosisArray[index].metric = cdgs.metric;
                });
            }
        };

        $scope.closedCase = function (value) {

            if (value == true) {
                $scope.hideFollowUp = false;

            } else if (value == false) {

                if ($scope.followupFlag == true) {
                    $scope.hideFollowUp = true;
                } else {
                    $scope.hideFollowUp = false;
                }
            }
        };

        $scope.getStep = function (id) {
            return Restangular.one('conditionsteps', id).get().$object;
        };

        $scope.condition = {
            caseClosed: false,
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            createdDate: new Date(),
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0],
            deleteFlag: false
        };
        if ($window.sessionStorage.previous == '/patientrecord/create') {
            // console.log("i m in if");
            $scope.condition.memberId = $window.sessionStorage.ConditionMemberId;
            Restangular.one('members', $window.sessionStorage.ConditionMemberId).get().then(function (memData) {
                $scope.memberName = memData.name + ' - ' + memData.individualId;
                $scope.condition.headOfHouseholdId = memData.parentId;
                $scope.associatedHF = memData.associatedHF;
                $scope.selectedMember = memData;
            });
            $scope.disablePatientAdd = false;
        } else if ($window.sessionStorage.current == '/login' || $window.sessionStorage.current == '/') {
            // console.log("i m in else if");
            $scope.condition.memberId = $window.sessionStorage.MemberId;
            $window.sessionStorage.ConditionMemberId = $window.sessionStorage.MemberId;
            Restangular.one('members', $window.sessionStorage.MemberId).get().then(function (memData) {
                $scope.memberName = memData.name + ' - ' + memData.individualId;
                $scope.condition.headOfHouseholdId = memData.parentId;
                $scope.associatedHF = memData.associatedHF;
                $scope.selectedMember = memData;
            });
            $scope.disablePatientAdd = false;

            //  console.log('$scope.condition.memberId', $scope.condition.memberId);

        } else {
            $scope.condition.memberId = '';
            $scope.disablePatientAdd = true;
        }

        $scope.$watch('condition.memberId', function (newValue, oldValue) {
            // console.log('newValue', newValue);
            if (newValue == oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {
                $scope.disablePatientAdd = false;
                $window.sessionStorage.ConditionMemberId = newValue;
                Restangular.one('members', newValue).get().then(function (memData) {
                    $scope.memberName = memData.name + ' - ' + memData.individualId;
                    $scope.condition.headOfHouseholdId = memData.parentId;
                    $scope.associatedHF = memData.associatedHF;
                    $scope.selectedMember = memData;
                });
            }
        });

        $scope.$watch('condition.condition', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {
                
                $scope.trailerArray = [];

                if ($window.sessionStorage.language == 1) {
                    Restangular.one('conditions', newValue).get().then(function (ctns) {
                        $scope.conditionName = ctns.name;
                        var enabledVal = ctns.enabled.split();
                        $scope.getSteps(enabledVal);
                    });
                } else {
                    Restangular.one('conditions/findOne?filter[where][parentId]=' + newValue + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').get().then(function (ctns) {
                        $scope.conditionName = ctns.name;
                        var enabledVal = ctns.enabled.split();
                        $scope.getSteps(enabledVal);
                    });
                }

                $scope.conditionfollowups = Restangular.all('conditionfollowups?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false' + '&filter[where][condition]=' + newValue).getList().$object;

                $scope.screenedbys = Restangular.all('screenedbys?filter[order]=orderNo%20ASC&filter[where][deleteFlag]=false' + '&filter[where][conditionId]=' + newValue).getList().then(function (scrns) {
                    $scope.screenedbys = scrns;

                    var data = $scope.screenedbys.filter(function (arr) {
                        return arr.default == 'yes'
                    })[0];

                    if (data != undefined) {
                        $scope.condition.screenedby = data.id;
                    }
                });

                Restangular.all('conditiondiagnosis?filter[order]=orderNo%20ASC&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false' + '&filter[where][condition]=' + newValue).getList().then(function (diagnosis) {
                    $scope.conditiondiagnosis = diagnosis;

                    $scope.diagnosisArray = [{
                        diagnosis: '',
                        value: '',
                        conditiondiagnosis: diagnosis
                    }];
                });

                Restangular.all('conditionchecklists?filter[order]=orderNo%20ASC&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false' + '&filter[where][condition]=' + newValue).getList().then(function (check) {
                    $scope.checklists = check;

                    angular.forEach($scope.checklists, function (value, index) {
                        value.index = index + 1;
                        value.statusFlag = false;
                    });
                });
            }
        });

        $scope.$watch('condition.followup', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                if ($window.sessionStorage.language == 1) {
                    Restangular.one('conditionfollowups', newValue).get().then(function (cntn) {
                        $scope.followUpName = cntn.name;
                        var myDate = new Date();
                        myDate.setDate(myDate.getDate() + cntn.followUpDays);
                        $scope.condition.followupdate = myDate;
                    });
                } else {
                    Restangular.one('conditionfollowups/findOne?filter[where][parentId]=' + newValue + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').get().then(function (cntn) {
                        $scope.followUpName = cntn.name;
                        var myDate = new Date();
                        myDate.setDate(myDate.getDate() + cntn.followUpDays);
                        $scope.condition.followupdate = myDate;
                    });
                }
            }
        });

        $scope.$watch('condition.reasonForCancel', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {
                if ($window.sessionStorage.language == 1) {
                    Restangular.one('reasonforcancellations', newValue).get().then(function (resn) {
                        $scope.reasonForCancelName = resn.name;
                    });
                } else {
                    Restangular.one('reasonforcancellations/findOne?filter[where][parentId]=' + newValue + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').get().then(function (resn) {
                        $scope.reasonForCancelName = resn.name;
                    });
                }
            }
        });

        $scope.hideReason = false;
        $scope.hideFollowUp = false;
        $scope.followupFlag = false;
        $scope.vendorField = false;

        $scope.myArray = [];

        $scope.statusChange = function (id, index, status, statuses, enabledFor, step, oldvalue) {

            $scope.myArray = [];

            $scope.indexValue = index;

            $scope.auditArray.push({
                stepId: id,
                oldStatusId: oldvalue,
                newStatusId: status
            });

            var indexVal = index + 1;
            var prevIndexVal = index - 1;

            var last = $scope.trailerArray[$scope.trailerArray.length - 1];

            $scope.trailerArray[index].date = new Date();

            if (index != 0) {
                $scope.trailerArray[prevIndexVal].enabled = true;
            }

            if (status == 1) {
                $scope.hideFollowUp = false;
                $scope.followupFlag = false;
                $scope.hideDiagnosis = false;
                $scope.condition.caseClosed = false;
                $scope.disableCaseClosed = true;
                $scope.condition.followup = null;
                $scope.condition.followupdate = null;
                $scope.hideReason = false;
                $scope.trailerArray[index].enabled = false;
                $scope.trailerArray[indexVal].enabled = true;

            } else if (status == 2) {
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.hideReason = false;
                $scope.hideDiagnosis = false;
                $scope.disableCaseClosed = true;
                $scope.condition.caseClosed = false;
                $scope.trailerArray[index].enabled = false;
                $scope.trailerArray[indexVal].enabled = true;

            } else if (status == 3) {
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.hideReason = false;
                $scope.hideDiagnosis = false;
                $scope.condition.caseClosed = false;
                $scope.disableCaseClosed = true;

                if (id == 4) {

                    $scope.trailerArray[index].enabled = false;
                    $scope.trailerArray[indexVal].enabled = false;

                    if ($scope.trailerArray.lastIndexOf(last) != index) {

                        angular.forEach($scope.trailerArray[indexVal].statuses, function (data) {

                            var enabledFor = data.enabledFor.split(",");

                            var myFlag = enabledFor.includes($scope.trailerArray[indexVal].id.toString());

                            if (myFlag == true) {
                                data.enabled = false;
                            } else {
                                data.enabled = true;
                            }
                        });
                    }
                } else {
                    $scope.trailerArray[index].enabled = false;
                    $scope.trailerArray[indexVal].enabled = true;
                }

            } else if (status == 4) {
                
                $scope.hideReason = false;
                $scope.condition.followup = null;
                $scope.condition.followupdate = null;

                setTimeout(function () {
                    if (id == 3 || id == 5) {
                        $scope.hideDiagnosis = true;
                        $scope.condition.caseClosed = false;
                        $scope.disableCaseClosed = true;
                    } else {
                        $scope.hideDiagnosis = false;
                        $scope.condition.caseClosed = false;
                        $scope.disableCaseClosed = true;
                    }
                }, 250);

                if (id == 5) {
                    $scope.hideFollowUp = true;
                    $scope.followupFlag = true;
                    $scope.trailerArray[index].enabled = false;
                    $scope.trailerArray[indexVal].enabled = false;
                } else {
                    $scope.hideFollowUp = false;
                    $scope.followupFlag = false;
                    $scope.trailerArray[index].enabled = false;
                    $scope.trailerArray[indexVal].enabled = false;
                }

                if (id == 1) {
                    $scope.vendorField = true;
                }
                
              //  $scope.trailerArray[indexVal].status = 3;

                if ($scope.trailerArray.lastIndexOf(last) != index || id == 3) {

                    angular.forEach($scope.trailerArray[indexVal].statuses, function (data) {

                        var enabledFor = data.enabledFor.split(",");
                        // console.log('enabledFor', enabledFor);

                        var myFlag = enabledFor.includes($scope.trailerArray[indexVal].id.toString());
                        // console.log('myFlag', myFlag);

                        if (myFlag == true) {
                            data.enabled = false;
                        } else {
                            data.enabled = true;
                        }

                        $scope.myArray.push({
                            name: data.name,
                            orderNo: data.orderNo,
                            enabledFor: data.enabledFor,
                            deleteFlag: data.deleteFlag,
                            id: data.id,
                            enabled: data.enabled,
                            parentId: data.parentId
                        });

                        $scope.trailerArray[indexVal].statuses = $scope.myArray;

                    });
                }
            } else if (status == 5) {

                if (id == 3 || id == 5) {
                    $scope.hideDiagnosis = true;
                } else {
                    $scope.hideDiagnosis = false;
                }

                if (id == 5) {
                    $scope.hideFollowUp = true;
                    $scope.followupFlag = true;
                    $scope.disableCaseClosed = false;
                    $scope.condition.caseClosed = false;
                } else {
                    $scope.hideFollowUp = false;
                    $scope.followupFlag = false;
                    $scope.disableCaseClosed = true;
                    $scope.condition.caseClosed = false;
                }

                $scope.hideReason = false;
                $scope.condition.followup = null;
                //  $scope.hideDiagnosis = false;
                $scope.condition.followupdate = null;
                $scope.trailerArray[index].enabled = false;
                $scope.trailerArray[indexVal].enabled = true;


            } else if (status == 7) {

                $scope.hideReason = true;
                $scope.hideFollowUp = false;
                $scope.hideDiagnosis = false;
                $scope.condition.caseClosed = false;
                $scope.disableCaseClosed = true;
                $scope.condition.followup = null;
                $scope.condition.followupdate = null;
                $scope.trailerArray[index].enabled = false;
                $scope.trailerArray[indexVal].enabled = true;

            } else {
                $scope.hideReason = false;
                $scope.hideFollowUp = false;
                $scope.followupFlag = false;
                $scope.hideDiagnosis = false;
                $scope.disableCaseClosed = true;
                $scope.condition.followup = null;
                $scope.condition.followupdate = null;
                $scope.condition.caseClosed = false;
            }
        };

        $scope.validatestring = '';

        $scope.okConfirm = function () {

            var last = $scope.trailerArray[$scope.trailerArray.length - 1];

            if ($scope.indexValue == $scope.trailerArray.lastIndexOf(last)) {

                $scope.condition.step = $scope.trailerArray[$scope.indexValue].id;
                $scope.condition.status = $scope.trailerArray[$scope.indexValue].status;

                if ($window.sessionStorage.language == 1) {
                    Restangular.one('conditionstatuses', $scope.trailerArray[$scope.indexValue].status).get().then(function (sts) {
                        $scope.statusName = sts.name;
                    });
                } else {
                    Restangular.one('conditionstatuses/findOne?filter[where][parentId]=' + $scope.trailerArray[$scope.indexValue].status + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (sts) {
                        $scope.statusName = sts.name;
                    });
                }

                if ($window.sessionStorage.language == 1) {
                    Restangular.one('conditionsteps', $scope.trailerArray[$scope.indexValue].id).get().then(function (stp) {
                        $scope.stepName = stp.name;
                    });
                } else {
                    Restangular.one('conditionsteps/findOne?filter[where][parentId]=' + $scope.trailerArray[$scope.indexValue].id + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (stp) {
                        $scope.stepName = stp.name;
                    });
                }
            }

            for (var g = 0; g < $scope.trailerArray.length; g++) {
                
                if ($scope.trailerArray[g].enabled == false && $scope.trailerArray[g].status != '') {

                    $scope.condition.step = $scope.trailerArray[g].id;
                    $scope.condition.status = $scope.trailerArray[g].status;
                    
                    if ($window.sessionStorage.language == 1) {
                        Restangular.one('conditionstatuses', $scope.trailerArray[g].status).get().then(function (sts) {
                            $scope.statusName = sts.name;
                        });
                    } else {
                        Restangular.one('conditionstatuses/findOne?filter[where][parentId]=' + $scope.trailerArray[g].status + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (sts) {
                            $scope.statusName = sts.name;
                        });
                    }

                    if ($window.sessionStorage.language == 1) {
                        Restangular.one('conditionsteps', $scope.trailerArray[g].id).get().then(function (stp) {
                            $scope.stepName = stp.name;
                        });
                    } else {
                        Restangular.one('conditionsteps/findOne?filter[where][parentId]=' + $scope.trailerArray[g].id + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (stp) {
                            $scope.stepName = stp.name;
                        });
                    }
                } else if ($scope.trailerArray[g].status == 5 || $scope.trailerArray[g].status == 7) {

                    $scope.condition.step = $scope.trailerArray[g].id;
                    $scope.condition.status = $scope.trailerArray[g].status;

                    if ($window.sessionStorage.language == 1) {
                        Restangular.one('conditionstatuses', $scope.trailerArray[g].status).get().then(function (sts) {
                            $scope.statusName = sts.name;
                        });
                    } else {
                        Restangular.one('conditionstatuses/findOne?filter[where][parentId]=' + $scope.trailerArray[g].status + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (sts) {
                            $scope.statusName = sts.name;
                        });
                    }

                    if ($window.sessionStorage.language == 1) {
                        Restangular.one('conditionsteps', $scope.trailerArray[g].id).get().then(function (stp) {
                            $scope.stepName = stp.name;
                        });
                    } else {
                        Restangular.one('conditionsteps/findOne?filter[where][parentId]=' + $scope.trailerArray[g].id + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (stp) {
                            $scope.stepName = stp.name;
                        });
                    }
                }
            }

            if ($scope.hideFollowUp === false) {
                $scope.condition.followup = null;
                $scope.condition.followupdate = '';
                $scope.followUpName = '';
                $scope.followUpDate = '';
            }

            if ($scope.hideReason === false) {
                $scope.condition.reasonForCancel = null;
                $scope.reasonForCancelName = '';
            }

            $scope.followUpDate = $filter('date')($scope.condition.followupdate, 'dd-MMM-yyyy');

            if ($scope.condition.memberId == '' || $scope.condition.memberId == null) {
                $scope.validatestring = $scope.validatestring + $scope.ConditionLanguage.pleaseSelectMember;

            } else if ($scope.condition.condition == '' || $scope.condition.condition == null) {
                $scope.validatestring = $scope.validatestring + $scope.ConditionLanguage.pleaseSelectCondition;

            } else if ($scope.condition.associatedHF == '' || $scope.condition.associatedHF == null) {
                $scope.validatestring = $scope.validatestring + $scope.ConditionLanguage.selectAssignedTo;

            } else if ($scope.hideFollowUp === true) {
                if ($scope.condition.followup == '' || $scope.condition.followup == null) {
                    $scope.validatestring = $scope.validatestring + $scope.ConditionLanguage.pleaseSelectFollowup;
                }
            } else if ($scope.hideReason === true) {
                if ($scope.condition.reasonForCancel == '' || $scope.condition.reasonForCancel == null) {
                    $scope.validatestring = $scope.validatestring + $scope.ConditionLanguage.reasonForCancelling;
                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                //  console.log($scope.condition);

                Restangular.one('conditionheaders?filter[where][condition]=' + $scope.condition.condition + '&filter[where][memberId]=' + $scope.condition.memberId + '&filter[where][caseClosed]=false').get().then(function (cndnResp) {
                    if (cndnResp.length > 0) {
                        $scope.toggleValidation();
                        $scope.validatestring1 = 'Open Screen and Treat Already Exists...!!!';
                    } else {
                        $scope.confirmationModel = true;
                    }
                });
            }
        };

        var tArray = 0;

        $scope.Save = function () {

            for (var h = 0; h < $scope.trailerArray.length; h++) {

                if ($scope.trailerArray[h].enabled == false && ($scope.trailerArray[h].status == 5 || $scope.trailerArray[h].status == 7)) {
                    $scope.trailerArray[h].enabled = true;
                    tArray++;
                } else {
                    tArray++;
                }

                if (tArray == $scope.trailerArray.length) {
                    $scope.saveFunc();
                }
            }
        };

        $scope.saveFunc = function () {

            $scope.confirmationModel = false;

            $scope.toggleLoading();
            //  $scope.condition.associatedHF = $scope.associatedHF;
            Restangular.all('conditionheaders').post($scope.condition).then(function (response) {


                // console.log(response);
                $scope.saveTrailer(response.id, response.memberId, response.condition);
            });
        };

        $scope.saveCount = 0;
        $scope.trailerpost = {};

        $scope.saveTrailer = function (headerId, memberId, condition) {
            if ($scope.saveCount < $scope.trailerArray.length) {
                $scope.trailerpost.memberId = memberId;
                $scope.trailerpost.condition = condition;
                $scope.trailerpost.conditionHeaderId = headerId;
                $scope.trailerpost.step = $scope.trailerArray[$scope.saveCount].id;
                $scope.trailerpost.status = $scope.trailerArray[$scope.saveCount].status;
                $scope.trailerpost.enabled = $scope.trailerArray[$scope.saveCount].enabled;
                $scope.trailerpost.createdDate = $scope.trailerArray[$scope.saveCount].date;
                $scope.trailerpost.countryId = $window.sessionStorage.countryId;
                $scope.trailerpost.stateId = $window.sessionStorage.stateId;
                $scope.trailerpost.districtId = $window.sessionStorage.districtId;
                $scope.trailerpost.siteId = $window.sessionStorage.siteId.split(",")[0];
                $scope.trailerpost.createdDate = new Date();
                $scope.trailerpost.createdBy = $window.sessionStorage.userId;
                $scope.trailerpost.createdByRole = $window.sessionStorage.roleId;
                $scope.trailerpost.lastModifiedDate = new Date();
                $scope.trailerpost.lastModifiedBy = $window.sessionStorage.userId;
                $scope.trailerpost.lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.trailerpost.associatedHF = $scope.condition.associatedHF;
                $scope.trailerpost.deleteFlag = false;

                if ($scope.trailerpost.enabled == true) {
                    $scope.trailerpost.enabled = false;
                } else {
                    $scope.trailerpost.enabled = true;
                }

                Restangular.all('conditiontrailers').post($scope.trailerpost).then(function (resp) {
                    // console.log(response);
                    $scope.saveCount++;
                    $scope.saveTrailer(headerId, memberId, condition);
                });
            } else {
                $scope.postCheckList(headerId, memberId, condition);
            }
        };

        $scope.checkCount = 0;

        $scope.checklistpost = {};

        $scope.postCheckList = function (headerId, memberId, condition) {

            if ($scope.checkCount < $scope.checklists.length) {

                $scope.checklistpost.checklist = $scope.checklists[$scope.checkCount].id;
                $scope.checklistpost.associatedHF = $scope.condition.associatedHF;
                $scope.checklistpost.conditionHeaderId = headerId;
                $scope.checklistpost.status = $scope.checklists[$scope.checkCount].statusFlag;
                $scope.checklistpost.countryId = $window.sessionStorage.countryId;
                $scope.checklistpost.stateId = $window.sessionStorage.stateId;
                $scope.checklistpost.districtId = $window.sessionStorage.districtId;
                $scope.checklistpost.siteId = $window.sessionStorage.siteId.split(",")[0];
                $scope.checklistpost.createdDate = new Date();
                $scope.checklistpost.createdBy = $window.sessionStorage.userId;
                $scope.checklistpost.createdByRole = $window.sessionStorage.roleId;
                $scope.checklistpost.lastModifiedDate = new Date();
                $scope.checklistpost.lastModifiedBy = $window.sessionStorage.userId;
                $scope.checklistpost.lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.checklistpost.deleteFlag = false;

                Restangular.all('checklistheaders').post($scope.checklistpost).then(function (checkResp) {
                    $scope.checkCount++;
                    $scope.postCheckList(headerId, memberId, condition);
                });
            } else {
                $scope.postDiagnosis(headerId, memberId, condition);
            }
        };

        $scope.diagnosisCount = 0;

        $scope.diagnosispost = {};

        $scope.postDiagnosis = function (headerId, memberId, condition) {

            if ($scope.diagnosisCount < $scope.diagnosisArray.length) {

                $scope.diagnosispost.diagnosis = $scope.diagnosisArray[$scope.diagnosisCount].diagnosis;
                $scope.diagnosispost.associatedHF = $scope.condition.associatedHF;
                $scope.diagnosispost.conditionHeaderId = headerId;
                $scope.diagnosispost.value = $scope.diagnosisArray[$scope.diagnosisCount].value;
                $scope.diagnosispost.countryId = $window.sessionStorage.countryId;
                $scope.diagnosispost.stateId = $window.sessionStorage.stateId;
                $scope.diagnosispost.districtId = $window.sessionStorage.districtId;
                $scope.diagnosispost.siteId = $window.sessionStorage.siteId.split(",")[0];
                $scope.diagnosispost.createdDate = new Date();
                $scope.diagnosispost.createdBy = $window.sessionStorage.userId;
                $scope.diagnosispost.createdByRole = $window.sessionStorage.roleId;
                $scope.diagnosispost.lastModifiedDate = new Date();
                $scope.diagnosispost.lastModifiedBy = $window.sessionStorage.userId;
                $scope.diagnosispost.lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.diagnosispost.deleteFlag = false;

                if ($scope.diagnosisArray[$scope.diagnosisCount].diagnosis == '' || $scope.diagnosisArray[$scope.diagnosisCount].value == '') {

                    $scope.diagnosisCount++;
                    $scope.postDiagnosis(headerId, memberId, condition);

                } else {

                    Restangular.all('diagnosisheaders').post($scope.diagnosispost).then(function (checkResp) {
                        $scope.diagnosisCount++;
                        $scope.postDiagnosis(headerId, memberId, condition);
                    });
                }

            } else {
                $scope.saveAuditTrail(headerId, memberId, condition);
            }
        };

        $scope.auidtCount = 0;

        $scope.saveAuditTrail = function (headerId, memberId, condition) {
            if ($scope.auidtCount < $scope.auditArray.length) {

                $scope.auditArray[$scope.auidtCount].rowId = headerId;
                $scope.auditArray[$scope.auidtCount].conditionId = condition;
                $scope.auditArray[$scope.auidtCount].memberId = memberId;
                $scope.auditArray[$scope.auidtCount].action = 'Insert';
                $scope.auditArray[$scope.auidtCount].module = 'Condition';
                $scope.auditArray[$scope.auidtCount].owner = $window.sessionStorage.userId;
                $scope.auditArray[$scope.auidtCount].datetime = new Date();
                $scope.auditArray[$scope.auidtCount].details = $scope.selectedMember.individualId;
                $scope.auditArray[$scope.auidtCount].countryId = $window.sessionStorage.countryId;
                $scope.auditArray[$scope.auidtCount].stateId = $window.sessionStorage.stateId;
                $scope.auditArray[$scope.auidtCount].districtId = $window.sessionStorage.districtId;
                $scope.auditArray[$scope.auidtCount].siteId = $window.sessionStorage.siteId.split("")[0];
                $scope.auditArray[$scope.auidtCount].associatedHF = $scope.condition.associatedHF;

                Restangular.all('audittrials').post($scope.auditArray[$scope.auidtCount]).then(function (auditresp) {
                    $scope.auidtCount++;
                    $scope.saveAuditTrail(headerId, memberId, condition);
                });
            } else {
                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setTimeout(function () {
                    $window.sessionStorage.MemberId = '';
                    window.location = "/screentreats-list";
                }, 1500);
            }
        };

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };

        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dtmax = new Date();

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };

        $scope.toggleMin();
        $scope.picker = {};

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.open2 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepickerfollowup' + index).focus();
            });
            $scope.condition.followupopened = true;
        };

        $scope.conditionfollowupopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.condition.followupdatepick = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end///


    })
    /*******shg modal*********************/
    .directive('checklistmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-md-6">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    })
    .directive('validNumber', function () {
        return {
            require: '?ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }

                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }

                    var clean = val.replace(/[^-0-9\.]/g, '');
                    var negativeCheck = clean.split('-');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(negativeCheck[1])) {
                        negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                        clean = negativeCheck[0] + '-' + negativeCheck[1];
                        if (negativeCheck[0].length > 0) {
                            clean = negativeCheck[0];
                        }

                    }

                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }

                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });

                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
