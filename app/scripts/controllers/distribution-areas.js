'use strict';

angular.module('secondarySalesApp')
    .controller('DistributionAreasCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/distribution-areas/create' || $location.path() === '/distribution-areas/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/distribution-areas/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/distribution-areas/create' || $location.path() === '/distribution-areas/' + $routeParams.id;
            return visible;
        };
        
     $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/distribution-areas/create' || $location.path() === '/distribution-areas/' + $routeParams.id;
            return visible;
        };
        
    /********************************/
    
     $scope.salesArea = {
            zoneId: ''    
        }
    
    $scope.distributionArea = {
            zoneId: '',
            salesAreaId:''
        };

        /*********/

      

    
 
    
      $scope.$watch('distributionArea.zoneId', function (newValue, oldValue) {
            $scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue).getList().$object;
        });

        $scope.getZone = function (zoneId) {
            return Restangular.one('zones', zoneId).get().$object;
        };

        $scope.getSalesArea = function (salesAreaId) {
            return Restangular.one('sales-areas', salesAreaId).get().$object;
        };
    
    
    /***************************************************************************************************************/
    
    
    
    
    
      $scope.$watch('distributionArea.name', function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                } else {
                    var RegExpression = /^[\a-zA-Z\s]*$/;

                    if (RegExpression.test(newValue)) {

                    } else {
                       $scope.distributionArea.name = oldValue;
                    }
                    
                }
            });
    
       
    
        $scope.searchDA = $scope.name;
    
      $scope.zones = Restangular.all('zones').getList().$object;
      $scope.salesAreas = Restangular.all('sales-areas').getList().$object;
      
    
    /************************************************** INDEX ***************************************************/
    
       
     $scope.Sal = Restangular.all('distribution-areas').getList().then(function (Sal) {
        $scope.distributionAreas = Sal;
        angular.forEach($scope.distributionAreas, function (member, index) {
            member.index = index + 1;
        });});
       
    /******************************************************* SAVE ***********************************************/
    
        $scope.validatestring = '';
        $scope.Save = function () {
                document.getElementById('name').style.border = "";
                document.getElementById('zoneId').style.border = "";
                document.getElementById('salesAreaId').style.border = "";
            
         if ($scope.distributionArea.name  == '' || $scope.distributionArea.name  == null) {
                $scope.distributionArea.name  = null;
               // $scope.salesArea.zoneId = '';
                $scope.validatestring = $scope.validatestring + 'Plese enter your Distribution-Area';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } else if(  $scope.distributionArea.zoneId == '' || $scope.distributionArea.zoneId  == null) {
               $scope.distributionArea.zoneId = '';
                $scope.validatestring = $scope.validatestring + 'Plese Select your Zone';
                document.getElementById('zoneId').style.border = "1px solid #ff0000";
        
          } else if(  $scope.distributionArea.salesAreaId == '' || $scope.distributionArea.salesAreaId  == null) {
               $scope.distributionArea.salesAreaId = '';
                $scope.validatestring = $scope.validatestring + 'Plese Select your Sales-Area';
                document.getElementById('salesAreaId').style.border = "1px solid #ff0000";
          }
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.distributionareas.post($scope.distributionArea).then(function () {
                        console.log('$scope.distributionArea', $scope.distributionArea);
                       window.location = '/distribution-areas';
                    });
                }


        };
        
     /******************************************************* DELETE ***********************************************/
   
     $scope.updateFlag = function (id) {
        if (confirm("Are you sure want to delete..!") == true) {
            Restangular.one('distribution-areas/' + id).remove($scope.distributionArea).then(function () {
                $route.reload();
            });

        } else {

        }

    }
  /******************************************************* UPDATE ***********************************************/
  
    
        
      $scope.validatestring = '';
        $scope.Update = function () {
                document.getElementById('name').style.border = "";
                document.getElementById('zoneId').style.border = "";
                document.getElementById('salesAreaId').style.border = "";
            
         if ($scope.distributionArea.name  == '' || $scope.distributionArea.name  == null) {
                $scope.distributionArea.name  = null;
               // $scope.salesArea.zoneId = '';
                $scope.validatestring = $scope.validatestring + 'Plese enter your Distribution-Area';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } else if(  $scope.distributionArea.zoneId == '' || $scope.distributionArea.zoneId  == null) {
               $scope.distributionArea.zoneId = '';
                $scope.validatestring = $scope.validatestring + 'Plese Select your Zone';
                document.getElementById('zoneId').style.border = "1px solid #ff0000";
        
          } else if(  $scope.distributionArea.salesAreaId == '' || $scope.distributionArea.salesAreaId  == null) {
               $scope.distributionArea.salesAreaId = '';
                $scope.validatestring = $scope.validatestring + 'Plese Select your Sales-Area';
                document.getElementById('salesAreaId').style.border = "1px solid #ff0000";
          }
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.distributionareas.customPUT($scope.distributionArea).then(function () {
                        console.log('$scope.distributionArea', $scope.distributionArea);
                       window.location = '/distribution-areas';
                    });
                }


        };
    
    
    
     if ($routeParams.id) {
            Restangular.one('distribution-areas', $routeParams.id).get().then(function (distributionArea) {
                $scope.original = distributionArea;
                $scope.distributionArea = Restangular.copy($scope.original);  
                Restangular.all('sales-areas').getList().object;
        
            });
        }
    
/////////////////////////////////////////////////// CALLING FUNCTION/////////////////////////////////////////////////// 
   

        $scope.zoneId = '';
        $scope.salesareaId = '';
        $scope.salesid = '';
        $scope.zonalid = '';

        $scope.$watch('zoneId', function (newValue, oldValue) {
              if (newValue === oldValue || newValue == '') {
                return;
            } else {
            $scope.salesareas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue).getList().$object;
        $scope.sal = Restangular.all('distribution-areas?filter[where][zoneId]=' + newValue).getList().then( function (sal) {
            $scope.distributionAreas = sal;
            angular.forEach($scope.distributionAreas, function (member, index) {
            member.index = index + 1;
        });}); 
            $scope.zonalid = +newValue;
            }
        });

        $scope.$watch('salesareaId', function (newValue, oldValue) {
              if (newValue === oldValue || newValue == '') {
                return;
            } else {
    $scope.sal = Restangular.all('distribution-areas?filter[where][zoneId]=' + $scope.zonalid +'&filter[where][salesAreaId]=' + newValue).getList().then( function (sal) {
            $scope.distributionAreas = sal;
            angular.forEach($scope.distributionAreas, function (member, index) {
            member.index = index + 1;
        });}); 
            $scope.salesid = +newValue;
            }
        });
   

 
    });