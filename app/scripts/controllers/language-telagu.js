'use strict';

angular.module('secondarySalesApp')
	.controller('TelaguCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
		$scope.isCreateSave = true;
		$scope.heading = 'Multi-Language';

		$scope.multilanguage = {
			languageId: 5
		};

		$scope.submitmultilanguages = Restangular.all('multilanguages').getList().$object;

		$scope.SaveLanguage = function () {
			$scope.submitmultilanguages.customPUT($scope.multilanguage).then(function (response) {
				console.log('Response', response);
				window.location = '/';
			});
		};

		Restangular.one('multilanguages', 5).get().then(function (zone) {
			$scope.original = zone;
			$scope.multilanguage = Restangular.copy($scope.original);
			$scope.modalInstanceLoad.close();
		});

		$scope.LanguageName = 'తెలుగు';
		$scope.Language = 'భాషా';
	});
