'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/

        $scope.disablePatientAdd = true;
        // $window.sessionStorage.previous = '/condition/create';
        $window.sessionStorage.previousRouteparamsId = $routeParams.id;

        $scope.HideCreateButton = false;
        $scope.editDisable = true;
        $scope.confirmationModel = false;
        $scope.ConditionLanguage = {};
        $scope.UserLanguage = $window.sessionStorage.language;
        $scope.hideReason = false;
        $scope.disableCaseClosed = true;
        //  $scope.hideFollowUp = false;

        Restangular.one('conditionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.ConditionLanguage = langResponse[0];
            $scope.conditionHeading = langResponse[0].editCondition;
            $scope.message = langResponse[0].conditionUpdated;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        //    if ($window.sessionStorage.previous == '/patientrecord/create') {
        //             $scope.condition.memberId = $window.sessionStorage.ConditionMemberId;
        //             $scope.disablePatientAdd = false;
        //         } 


        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.hideAssigned = true;
            $scope.hideAddBtn = false;
            $scope.hideLabel = true;

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.condition.associatedHF = $window.sessionStorage.userId;

            });

            $scope.memberFilterCall = 'members?filter[where][deleteFlag]=false' + '&filter[where][associatedHF]=' + $window.sessionStorage.userId;

        } else {

            $scope.hideAssigned = false;
            $scope.hideAddBtn = true;
            $scope.hideLabel = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.condition.associatedHF = $scope.condition.associatedHF;

            });

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        }

        Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
            $scope.members = mems;
        });

        Restangular.all('conditions?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false' + '&filter[where][enabledFor]=true').getList().then(function (cn) {
            $scope.conditionsdsply = cn;
        });

        Restangular.all('reasonforcancellations?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().then(function (rfc) {
            $scope.reasonforcancellations = rfc;
        });

        $scope.openCheckList = function (id, status, index) {

            $scope.checkListModal = true;

            if (status == 3) {
                $scope.disableCheck = false;
            } else {
                $scope.disableCheck = false;
            }
        };

        $scope.checkvalid = [];

        $scope.saveChecklist = function () {
            $scope.checkListModal = false;

            $scope.checkvalid = [];

            angular.forEach($scope.checklists, function (value, index) {
                $scope.checkvalid.push(value.statusFlag);
            });

            //            console.log($scope.existingFlag);
            //            console.log($scope.checklists);
        };

        $scope.cancelCheckList = function () {

            for (var i = 0; i < $scope.checklists.length; i++) {
                $scope.checklists[i].statusFlag = $scope.checkvalid[i];
                // console.log($scope.checkvalid[i]);
            }

            $scope.checkListModal = false;
        };

        $scope.addDiagnosis = function (id, index) {
            if ($scope.disableAddBtn == true) {
                return;
            } else {
                $scope.diagnosisArray.push({
                    diagnosis: '',
                    value: '',
                    conditiondiagnosis: $scope.conditiondiagnosis,
                    disableEdit: false
                });
                // console.log($scope.diagnosisArray);
            }
        };

        $scope.removeDiagnosis = function (id, index) {
            if ($scope.disableAddBtn == true) {
                return;
            } else {
                $scope.diagnosisArray.splice(index, 1);
            }
        };

        $scope.diagnosisChange = function (id, index) {

            if ($window.sessionStorage.language == 1) {
                Restangular.one('conditiondiagnosis', id).get().then(function (cdgs) {
                    $scope.diagnosisArray[index].name = cdgs.name;
                    $scope.diagnosisArray[index].metric = cdgs.metric;
                });
            } else {
                Restangular.one('conditiondiagnosis/findOne?filter[where][parentId]=' + id + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').get().then(function (cdgs) {
                    $scope.diagnosisArray[index].name = cdgs.name;
                    $scope.diagnosisArray[index].metric = cdgs.metric;
                });
            }
        };

        $scope.closedCase = function (value) {
            if (value == true) {
                $scope.hideFollowUp = false;
                $scope.disableAddBtn = true;
            } else if (value == false) {
                if ($scope.followupFlag == true) {
                    $scope.hideFollowUp = true;
                    $scope.disableAddBtn = false;
                } else {
                    $scope.hideFollowUp = false;
                    $scope.disableAddBtn = false;
                }
            }
        };

        $scope.vendorField = false;

        $scope.trailerArray = [];

        $scope.myArray = [];

        if ($routeParams.id) {
            Restangular.one('conditionheaders', $routeParams.id).get().then(function (cnd) {
                $scope.original = cnd;
                $scope.condition = Restangular.copy($scope.original);

                if (cnd.caseClosed == true) {
                    $scope.disableCaseClosed = true;
                }

                if (cnd.screenedby != undefined || cnd.screenedby != '' || cnd.screenedby != null) {
                    $scope.vendorField = true;
                }

                var closedFlag = $scope.condition.caseClosed;
                $scope.disableAddBtn = closedFlag;

                Restangular.all('conditiondiagnosis?filter[order]=orderNo%20ASC&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false' + '&filter[where][condition]=' + cnd.condition).getList().then(function (diagnosis) {
                    $scope.conditiondiagnosis = diagnosis;

                    Restangular.all('diagnosisheaders?filter[where][deleteFlag]=false' + '&filter[where][conditionHeaderId]=' + cnd.id).getList().then(function (diagnosisheader) {

                        if (diagnosisheader.length == 0) {
                            $scope.diagnosisArray = [{
                                diagnosis: '',
                                value: '',
                                conditiondiagnosis: diagnosis,
                                disableEdit: false
                        }];
                        } else {
                            $scope.diagnosisArray = diagnosisheader;
                            $scope.hideDiagnosis = true;

                            angular.forEach($scope.diagnosisArray, function (data, index) {
                                data.disableEdit = true;
                                data.conditiondiagnosis = diagnosis;
                                data.diagnosis = data.diagnosis;

                                if ($window.sessionStorage.language == 1) {

                                    Restangular.one('conditiondiagnosis', data.diagnosis).get().then(function (cdgs) {
                                        $scope.diagnosisArray[index].name = cdgs.name;
                                        $scope.diagnosisArray[index].metric = cdgs.metric;
                                    });
                                } else {
                                    Restangular.one('conditiondiagnosis/findOne?filter[where][parentId]=' + data.diagnosis + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').get().then(function (cdgs) {
                                        $scope.diagnosisArray[index].name = cdgs.name;
                                        $scope.diagnosisArray[index].metric = cdgs.metric;
                                    });

                                }
                            });
                        }
                    });
                });

                Restangular.one('members', cnd.memberId).get().then(function (memData) {
                    Restangular.one('conditions', cnd.condition).get().then(function (cnts) {
                        $scope.memberName = memData.name + ' - ' + memData.individualId;
                        //$scope.conditionName = cnts.name;
                    });
                });

                Restangular.all('conditiontrailers?filter[where][deleteFlag]=false' + '&filter[where][conditionHeaderId]=' + cnd.id).getList().then(function (trailers) {

                    Restangular.all('conditionstatuses?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().then(function (csts) {

                        $scope.conditionstatuses = csts;

                        Restangular.all('conditionsteps?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').getList().then(function (cs) {
                            $scope.conditionsteps = cs;

                            angular.forEach(trailers, function (value, index) {
                                value.index = index;

                                $scope.myArray = [];

                                $scope.trailerArray.push({
                                    status: value.status,
                                    date: value.createdDate,
                                    id: value.step,
                                    enabled: value.enabled,
                                    statuses: $scope.conditionstatuses,
                                    trailerid: value.id
                                });

                                if (value.enabled == true) {

                                    if (value.status == 2 || value.status == 3) {
                                        $scope.hideFollowUp = true;
                                        $scope.followupFlag = true;
                                    }

                                    angular.forEach($scope.trailerArray[value.index].statuses, function (Arrdata, index) {
                                        var enabledFor = Arrdata.enabledFor.split(",");
                                        var myFlag = enabledFor.includes($scope.trailerArray[value.index].id.toString())

                                        if (myFlag == true) {
                                            Arrdata.enabled = false;
                                        } else if (myFlag == false) {
                                            Arrdata.enabled = true;
                                        }

                                        $scope.myArray.push({
                                            name: Arrdata.name,
                                            orderNo: Arrdata.orderNo,
                                            enabledFor: Arrdata.enabledFor,
                                            deleteFlag: Arrdata.deleteFlag,
                                            id: Arrdata.id,
                                            enabled: Arrdata.enabled,
                                            parentId: Arrdata.parentId
                                        });

                                        $scope.trailerArray[value.index].statuses = $scope.myArray;
                                    });
                                } else {

                                    angular.forEach($scope.trailerArray[value.index].statuses, function (ArrdataOne, index) {
                                        
                                        var enabledFor = ArrdataOne.enabledFor.split(",");
                                        var myFlag = enabledFor.includes($scope.trailerArray[value.index].id.toString());

                                        if (myFlag == true) {
                                            ArrdataOne.enabled = false;
                                        } else if (myFlag == false) {
                                            ArrdataOne.enabled = true;
                                        }
                                        
                                        $scope.myArray.push({
                                            name: ArrdataOne.name,
                                            orderNo: ArrdataOne.orderNo,
                                            enabledFor: ArrdataOne.enabledFor,
                                            deleteFlag: ArrdataOne.deleteFlag,
                                            id: ArrdataOne.id,
                                            enabled: ArrdataOne.enabled,
                                            parentId: ArrdataOne.parentId
                                        });

                                        $scope.trailerArray[value.index].statuses = $scope.myArray;
                                    });
                                }

                                if (trailers.length == $scope.trailerArray.length) {
                                    angular.forEach($scope.trailerArray, function (tdata, index) {

                                        tdata.index = index;

                                        if (tdata.enabled == true) {
                                            tdata.enabled = false;
                                        } else if (tdata.enabled == false) {
                                            tdata.enabled = true;
                                        }

                                        if (tdata.status == 5 && tdata.id == 5 && $scope.condition.caseClosed == true) {
                                            $scope.disableCaseClosed = true;
                                            $scope.hideDiagnosis = false;
                                            $scope.hideFollowUp = false;
                                            $scope.followupFlag = false;
                                        } else if (tdata.status == 5 && tdata.id == 5 && $scope.condition.caseClosed == false) {
                                            $scope.disableCaseClosed = false;
                                            $scope.hideFollowUp = true;
                                            $scope.followupFlag = true;
                                            $scope.hideDiagnosis = false;
                                        }

                                        if (tdata.status != 0) {
                                            if (tdata.id != 3 || tdata.id != 5) {
                                                $scope.hideDiagnosis = false;
                                            }
                                        }

                                        if (tdata.status == 4 && (tdata.id == 3 || tdata.id == 5)) {
                                            $scope.hideDiagnosis = true;
                                        }

                                        if (tdata.status == 4 && tdata.id == 5 && $scope.condition.caseClosed == false) {
                                            $scope.hideFollowUp = true;
                                            $scope.followupFlag = true;
                                        }

                                        if (tdata.status == 0) {
                                            tdata.status = '';
                                        }

                                        if ($window.sessionStorage.language == 1) {
                                            Restangular.one('conditionsteps', tdata.id).get().then(function (ctp) {
                                                tdata.step = ctp.name;
                                            });
                                        } else {
                                            Restangular.one('conditionsteps/findOne?filter[where][parentId]=' + tdata.id + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (ctp) {
                                                tdata.step = ctp.name;
                                            });
                                        }
                                    });

                                    var last = $scope.trailerArray[$scope.trailerArray.length - 1];
                                }
                            });
                        });
                    });
                });
            });
        }

        $scope.getStep = function (id) {
            return Restangular.one('conditionsteps', id).get().$object;
        };

        $scope.$watch('condition.memberId', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {
                $scope.disablePatientAdd = false;
                $window.sessionStorage.ConditionMemberId = newValue;
                Restangular.one('members', newValue).get().then(function (memData) {
                    $scope.memberName = memData.name + ' - ' + memData.individualId;
                    $scope.condition.headOfHouseholdId = memData.parentId;
                    $scope.associatedHF = memData.associatedHF;
                    $scope.selectedMember = memData;
                });
            }
        });

        $scope.$watch('condition.condition', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {

                if ($window.sessionStorage.language == 1) {
                    Restangular.one('conditions', newValue).get().then(function (ctns) {
                        $scope.conditionName = ctns.name;
                    });
                } else {
                    Restangular.one('conditions/findOne?filter[where][parentId]=' + newValue + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (ctns) {
                        $scope.conditionName = ctns.name;
                    });
                }

                $scope.conditiondiagnosis = Restangular.all('conditiondiagnosis?filter[order]=orderNo%20ASC&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false' + '&filter[where][condition]=' + newValue).getList().$object;

                $scope.conditionfollowups = Restangular.all('conditionfollowups?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][condition]=' + newValue + '&filter[where][deleteFlag]=false').getList().$object;

                $scope.screenedbys = Restangular.all('screenedbys?filter[order]=orderNo%20ASC&filter[where][deleteFlag]=false' + '&filter[where][conditionId]=' + newValue).getList().then(function (scrns) {
                    $scope.screenedbys = scrns;
                });

                Restangular.all('conditionchecklists?filter[order]=orderNo%20ASC&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false' + '&filter[where][condition]=' + newValue).getList().then(function (chck) {
                    $scope.checklistdata = chck;

                    angular.forEach($scope.checklistdata, function (value, index) {
                        value.statusFlag = false;
                        value.index = index + 1;
                    });

                    Restangular.all('checklistheaders?filter[where][conditionHeaderId]=' + $routeParams.id + '&filter[where][deleteFlag]=false').getList().then(function (chklist) {

                        if (chklist.length == 0) {
                            $scope.checklists = $scope.checklistdata;
                            $scope.existingFlag = false;

                        } else {
                            $scope.existingFlag = true;

                            $scope.checklists = chklist;

                            angular.forEach($scope.checklists, function (value, index) {
                                value.index = index + 1;
                                value.statusFlag = value.status;

                                $scope.checkvalid.push(value.statusFlag);

                                Restangular.one('conditionchecklists', value.checklist).get().then(function (check) {
                                    value.name = check.name;
                                });
                            });
                        }
                    });
                });
            }
        });

        $scope.$watch('condition.followup', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                if ($window.sessionStorage.language == 1) {
                    Restangular.one('conditionfollowups', newValue).get().then(function (cntn) {
                        $scope.followUpName = cntn.name;
                        var myDate = new Date();
                        myDate.setDate(myDate.getDate() + cntn.followUpDays);
                        $scope.condition.followupdate = myDate;
                    });
                } else {
                    Restangular.one('conditionfollowups/findOne?filter[where][parentId]=' + newValue + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').get().then(function (cntn) {
                        $scope.followUpName = cntn.name;
                        var myDate = new Date();
                        myDate.setDate(myDate.getDate() + cntn.followUpDays);
                        $scope.condition.followupdate = myDate;
                    });
                }
            }
        });

        $scope.$watch('condition.reasonForCancel', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {
                if ($window.sessionStorage.language == 1) {
                    Restangular.one('reasonforcancellations', newValue).get().then(function (resn) {
                        $scope.reasonForCancelName = resn.name;
                    });
                } else {
                    Restangular.one('reasonforcancellations/findOne?filter[where][parentId]=' + newValue + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteFlag]=false').get().then(function (resn) {
                        $scope.reasonForCancelName = resn.name;
                    });
                }
            }
        });

        $scope.trailer = {};

        $scope.auditArray = [];

        $scope.mytArray = [];

        $scope.statusChange = function (id, index, status, statuses, enabledFor, step, oldvalue) {

            $scope.mytArray = [];

            $scope.indexValue = index;

            $scope.auditArray.push({
                stepId: id,
                oldStatusId: oldvalue,
                newStatusId: status
            });

            var indexVal = index + 1;
            var prevIndexVal = index - 1;

            var last = $scope.trailerArray[$scope.trailerArray.length - 1];

            $scope.trailerArray[index].date = new Date();

            if (index != 0) {
                $scope.trailerArray[prevIndexVal].enabled = true;
            }

            if (status == 1) {
                $scope.hideFollowUp = false;
                $scope.followupFlag = false;
                $scope.hideDiagnosis = false;
                $scope.disableCaseClosed = true;
                $scope.condition.caseClosed = false;
                $scope.disableAddBtn = false;
                $scope.condition.followup = null;
                $scope.condition.followupdate = null;
                $scope.hideReason = false;
                $scope.trailerArray[index].enabled = false;
                $scope.trailerArray[indexVal].enabled = true;

            } else if (status == 2) {
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.hideReason = false;
                $scope.hideDiagnosis = false;
                $scope.condition.caseClosed = false;
                $scope.disableCaseClosed = true;
                $scope.disableAddBtn = false;
                $scope.trailerArray[index].enabled = false;
                $scope.trailerArray[indexVal].enabled = true;

            } else if (status == 3) {
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.hideReason = false;
                $scope.hideDiagnosis = false;
                $scope.condition.caseClosed = false;
                $scope.disableCaseClosed = true;

                if (id == 4) {

                    $scope.trailerArray[index].enabled = false;
                    $scope.trailerArray[indexVal].enabled = false;

                    if ($scope.trailerArray.lastIndexOf(last) != index) {

                        angular.forEach($scope.trailerArray[indexVal].statuses, function (data) {

                            var enabledFor = data.enabledFor.split(",");

                            var myFlag = enabledFor.includes($scope.trailerArray[indexVal].id.toString());

                            if (myFlag == true) {
                                data.enabled = false;
                            } else {
                                data.enabled = true;
                            }
                        });
                    }
                } else {
                    $scope.trailerArray[index].enabled = false;
                    $scope.trailerArray[indexVal].enabled = true;
                }
            } else if (status == 4) {

                $scope.hideReason = false;
                $scope.disableAddBtn = false;
                $scope.condition.followup = null;
                $scope.condition.followupdate = null;
                $scope.trailerArray[index].enabled = false;
                $scope.trailerArray[indexVal].enabled = false;

                setTimeout(function () {
                    if (id == 3 || id == 5) {
                        $scope.hideDiagnosis = true;
                        $scope.condition.caseClosed = false;
                        $scope.disableCaseClosed = true;
                        $scope.hideReason = false;
                    } else {
                        $scope.hideDiagnosis = false;
                        $scope.condition.caseClosed = false;
                        $scope.disableCaseClosed = true;
                        $scope.hideReason = false;
                    }
                }, 250);

                if (id == 5) {
                    $scope.hideFollowUp = true;
                    $scope.followupFlag = true;
                    $scope.trailerArray[index].enabled = false;
                    $scope.trailerArray[indexVal].enabled = false;
                } else {
                    $scope.hideFollowUp = false;
                    $scope.followupFlag = false;
                    $scope.trailerArray[index].enabled = false;
                    $scope.trailerArray[indexVal].enabled = false;
                }

                // $scope.trailerArray[indexVal].status = 3;

                if ($scope.trailerArray.lastIndexOf(last) != index || id == 3) {

                    angular.forEach($scope.trailerArray[indexVal].statuses, function (data) {

                        var enabledFor = data.enabledFor.split(",");
                        // console.log('enabledFor', enabledFor);

                        var myFlag = enabledFor.includes($scope.trailerArray[indexVal].id.toString());
                        // console.log('myFlag', myFlag);

                        if (myFlag == true) {
                            data.enabled = false;
                        } else {
                            data.enabled = true;
                        }

                        $scope.mytArray.push({
                            name: data.name,
                            orderNo: data.orderNo,
                            enabledFor: data.enabledFor,
                            deleteFlag: data.deleteFlag,
                            id: data.id,
                            enabled: data.enabled,
                            parentId: data.parentId
                        });

                        $scope.trailerArray[indexVal].statuses = $scope.mytArray;

                    });
                }
            } else if (status == 5) {

                setTimeout(function () {
                    if (id == 3 || id == 5) {
                        $scope.hideDiagnosis = true;
                    } else {
                        $scope.hideDiagnosis = false;
                    }
                }, 250);

                if (id == 5) {
                    $scope.hideFollowUp = true;
                    $scope.followupFlag = true;
                    $scope.disableCaseClosed = false;
                    $scope.condition.caseClosed = false;
                } else {
                    $scope.hideFollowUp = false;
                    $scope.followupFlag = false;
                    $scope.disableCaseClosed = true;
                    $scope.condition.caseClosed = false;
                }

                $scope.hideReason = false;
                //   $scope.hideDiagnosis = false;
                $scope.disableAddBtn = false;
                $scope.condition.followup = null;
                $scope.condition.followupdate = null;
                $scope.trailerArray[index].enabled = false;
                $scope.trailerArray[indexVal].enabled = true;


            } else if (status == 7) {

                if (id == 4) {
                    $scope.condition.caseClosed = false;
                    $scope.disableCaseClosed = true;
                } else {
                    $scope.condition.caseClosed = false;
                    $scope.disableCaseClosed = true;
                }

                if ($scope.condition.reasonForCancel == null || $scope.condition.reasonForCancel == '') {
                    if ($scope.UserLanguage == 1) {
                        $scope.condition.reasonForCancel = $scope.reasonforcancellations[0].id;
                    } else {
                        $scope.condition.reasonForCancel = $scope.reasonforcancellations[0].parentId;
                    }
                }

                $scope.hideReason = true;
                $scope.hideFollowUp = false;
                $scope.followupFlag = false;
                $scope.hideDiagnosis = false;
                $scope.condition.followup = null;
                $scope.disableAddBtn = false;
                $scope.condition.followupdate = null;
                $scope.trailerArray[index].enabled = false;
                $scope.trailerArray[indexVal].enabled = true;

            } else {
                $scope.hideReason = false;
                $scope.hideFollowUp = false;
                $scope.followupFlag = false;
                $scope.hideDiagnosis = false;
                $scope.condition.caseClosed = false;
                $scope.disableCaseClosed = true;
                $scope.disableAddBtn = false;
                $scope.condition.followup = null;
                $scope.condition.followupdate = null;
            }
        };

        $scope.validatestring = '';

        $scope.okConfirm = function () {

            $scope.diagnosisDataArray = [];

            var last = $scope.trailerArray[$scope.trailerArray.length - 1];
            //                      
            //            console.log($scope.indexValue);
            //            console.log($scope.trailerArray.lastIndexOf(last));
            //            
            if ($scope.indexValue == $scope.trailerArray.lastIndexOf(last)) {

                $scope.condition.step = $scope.trailerArray[$scope.indexValue].id;
                $scope.condition.status = $scope.trailerArray[$scope.indexValue].status;

                if ($window.sessionStorage.language == 1) {
                    Restangular.one('conditionstatuses', $scope.trailerArray[$scope.indexValue].status).get().then(function (sts) {
                        $scope.statusName = sts.name;
                    });
                } else {
                    Restangular.one('conditionstatuses/findOne?filter[where][parentId]=' + $scope.trailerArray[$scope.indexValue].status + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (sts) {
                        $scope.statusName = sts.name;
                    });
                }

                if ($window.sessionStorage.language == 1) {
                    Restangular.one('conditionsteps', $scope.trailerArray[$scope.indexValue].id).get().then(function (stp) {
                        $scope.stepName = stp.name;
                    });
                } else {
                    Restangular.one('conditionsteps/findOne?filter[where][parentId]=' + $scope.trailerArray[$scope.indexValue].id + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (stp) {
                        $scope.stepName = stp.name;
                    });
                }
            }

            for (var g = 0; g < $scope.trailerArray.length; g++) {
                if ($scope.trailerArray[g].enabled == false && $scope.trailerArray[g].status != '') {

                    $scope.condition.step = $scope.trailerArray[g].id;
                    $scope.condition.status = $scope.trailerArray[g].status;

                    if ($window.sessionStorage.language == 1) {
                        Restangular.one('conditionstatuses', $scope.trailerArray[g].status).get().then(function (sts) {
                            $scope.statusName = sts.name;
                        });
                    } else {
                        Restangular.one('conditionstatuses/findOne?filter[where][parentId]=' + $scope.trailerArray[g].status + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (sts) {
                            $scope.statusName = sts.name;
                        });
                    }

                    if ($window.sessionStorage.language == 1) {
                        Restangular.one('conditionsteps', $scope.trailerArray[g].id).get().then(function (stp) {
                            $scope.stepName = stp.name;
                        });
                    } else {
                        Restangular.one('conditionsteps/findOne?filter[where][parentId]=' + $scope.trailerArray[g].id + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (stp) {
                            $scope.stepName = stp.name;
                        });
                    }
                } else if ($scope.trailerArray[g].status == 5 || $scope.trailerArray[g].status == 7) {

                    $scope.condition.step = $scope.trailerArray[g].id;
                    $scope.condition.status = $scope.trailerArray[g].status;

                    if ($window.sessionStorage.language == 1) {
                        Restangular.one('conditionstatuses', $scope.trailerArray[g].status).get().then(function (sts) {
                            $scope.statusName = sts.name;
                        });
                    } else {
                        Restangular.one('conditionstatuses/findOne?filter[where][parentId]=' + $scope.trailerArray[g].status + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (sts) {
                            $scope.statusName = sts.name;
                        });
                    }

                    if ($window.sessionStorage.language == 1) {
                        Restangular.one('conditionsteps', $scope.trailerArray[g].id).get().then(function (stp) {
                            $scope.stepName = stp.name;
                        });
                    } else {
                        Restangular.one('conditionsteps/findOne?filter[where][parentId]=' + $scope.trailerArray[g].id + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (stp) {
                            $scope.stepName = stp.name;
                        });
                    }
                }
            }

            for (var v = 0; v < $scope.diagnosisArray.length; v++) {
                if ($scope.diagnosisArray[v].disableEdit == false) {
                    $scope.diagnosisDataArray.push($scope.diagnosisArray[v]);
                }
            }

            if ($scope.hideFollowUp === false) {
                $scope.condition.followup = null;
                $scope.condition.followupdate = '';
                $scope.followUpName = '';
                $scope.followUpDate = '';
            }

            if ($scope.hideReason === false) {
                $scope.condition.reasonForCancel = null;
                $scope.reasonForCancelName = '';
            }

            $scope.followUpDate = $filter('date')($scope.condition.followupdate, 'dd-MMM-yyyy');

            if ($scope.condition.memberId == '' || $scope.condition.memberId == null) {
                $scope.validatestring = $scope.validatestring + $scope.ConditionLanguage.pleaseSelectMember;

            } else if ($scope.condition.condition == '' || $scope.condition.condition == null) {
                $scope.validatestring = $scope.validatestring + $scope.ConditionLanguage.pleaseSelectCondition;

            } else if ($scope.hideFollowUp === true) {
                if ($scope.condition.followup == '' || $scope.condition.followup == null) {
                    $scope.validatestring = $scope.validatestring + $scope.ConditionLanguage.pleaseSelectFollowup;
                }
            } else if ($scope.hideReason === true) {
                if ($scope.condition.reasonForCancel == '' || $scope.condition.reasonForCancel == null) {
                    $scope.validatestring = $scope.validatestring + $scope.ConditionLanguage.reasonForCancelling;
                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                $scope.confirmationModel = true;
            }
        };

        var tArray = 0;

        $scope.Save = function () {

            for (var h = 0; h < $scope.trailerArray.length; h++) {

                if ($scope.trailerArray[h].enabled == false && ($scope.trailerArray[h].status == 5 || $scope.trailerArray[h].status == 7)) {
                    $scope.trailerArray[h].enabled = true;
                    tArray++;
                } else {
                    tArray++;
                }

                if (tArray == $scope.trailerArray.length) {
                    $scope.saveFunc();
                }
            }
        };

        $scope.saveFunc = function () {

            $scope.confirmationModel = false;

            $scope.toggleLoading();

            // $scope.condition.associatedHF = $scope.associatedHF;
            Restangular.one('conditionheaders', $routeParams.id).customPUT($scope.condition).then(function (response) {
                // console.log(response);
                $scope.updateTrailer(response.id, response.memberId, response.condition);
            });
        };

        $scope.updateCount = 0;
        $scope.trailerupdate = {};

        $scope.updateTrailer = function (headerId, memberId, condition) {
            if ($scope.updateCount < $scope.trailerArray.length) {
                $scope.trailerupdate.memberId = memberId;
                $scope.trailerupdate.condition = condition;
                $scope.trailerupdate.conditionHeaderId = headerId;
                $scope.trailerupdate.step = $scope.trailerArray[$scope.updateCount].id;
                $scope.trailerupdate.status = $scope.trailerArray[$scope.updateCount].status;
                $scope.trailerupdate.enabled = $scope.trailerArray[$scope.updateCount].enabled;
                $scope.trailerupdate.createdDate = $scope.trailerArray[$scope.updateCount].date;
                $scope.trailerupdate.countryId = $window.sessionStorage.countryId;
                $scope.trailerupdate.stateId = $window.sessionStorage.stateId;
                $scope.trailerupdate.districtId = $window.sessionStorage.districtId;
                $scope.trailerupdate.siteId = $window.sessionStorage.siteId.split(",")[0];
                $scope.trailerupdate.createdBy = $window.sessionStorage.userId;
                $scope.trailerupdate.createdByRole = $window.sessionStorage.roleId;
                $scope.trailerupdate.lastModifiedDate = new Date();
                $scope.trailerupdate.lastModifiedBy = $window.sessionStorage.userId;
                $scope.trailerupdate.lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.trailerupdate.associatedHF = $scope.condition.associatedHF;
                $scope.trailerupdate.deleteFlag = false;

                if ($scope.trailerupdate.enabled == true) {
                    $scope.trailerupdate.enabled = false;
                } else {
                    $scope.trailerupdate.enabled = true;
                }

                // console.log($scope.trailerupdate.enabled);

                Restangular.one('conditiontrailers', $scope.trailerArray[$scope.updateCount].trailerid).customPUT($scope.trailerupdate).then(function (resp) {
                    // console.log(response);
                    $scope.updateCount++;
                    $scope.updateTrailer(headerId, memberId, condition);
                });
            } else {
                $scope.postDiagnosis(headerId, memberId, condition);
            }
        };

        $scope.diagnosisCount = 0;

        $scope.diagnosispost = {};

        $scope.postDiagnosis = function (headerId, memberId, condition) {

            if ($scope.diagnosisCount < $scope.diagnosisDataArray.length) {

                $scope.diagnosispost.diagnosis = $scope.diagnosisDataArray[$scope.diagnosisCount].diagnosis;
                $scope.diagnosispost.associatedHF = $scope.condition.associatedHF;
                $scope.diagnosispost.conditionHeaderId = headerId;
                $scope.diagnosispost.value = $scope.diagnosisDataArray[$scope.diagnosisCount].value;
                $scope.diagnosispost.countryId = $window.sessionStorage.countryId;
                $scope.diagnosispost.stateId = $window.sessionStorage.stateId;
                $scope.diagnosispost.districtId = $window.sessionStorage.districtId;
                $scope.diagnosispost.siteId = $window.sessionStorage.siteId.split(",")[0];
                $scope.diagnosispost.createdDate = new Date();
                $scope.diagnosispost.createdBy = $window.sessionStorage.userId;
                $scope.diagnosispost.createdByRole = $window.sessionStorage.roleId;
                $scope.diagnosispost.lastModifiedDate = new Date();
                $scope.diagnosispost.lastModifiedBy = $window.sessionStorage.userId;
                $scope.diagnosispost.lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.diagnosispost.deleteFlag = false;

                if ($scope.diagnosisDataArray[$scope.diagnosisCount].diagnosis == '' || $scope.diagnosisDataArray[$scope.diagnosisCount].value == '') {

                    $scope.diagnosisCount++;
                    $scope.postDiagnosis(headerId, memberId, condition);

                } else {
                    Restangular.all('diagnosisheaders').post($scope.diagnosispost).then(function (checkResp) {
                        $scope.diagnosisCount++;
                        $scope.postDiagnosis(headerId, memberId, condition);
                    });
                }
            } else {
                $scope.saveAuditTrail(headerId, memberId, condition);
            }
        };

        $scope.auidtCount = 0;

        $scope.saveAuditTrail = function (headerId, memberId, condition) {
            if ($scope.auidtCount < $scope.auditArray.length) {

                $scope.auditArray[$scope.auidtCount].rowId = headerId;
                $scope.auditArray[$scope.auidtCount].conditionId = condition;
                $scope.auditArray[$scope.auidtCount].memberId = memberId;
                $scope.auditArray[$scope.auidtCount].action = 'Update';
                $scope.auditArray[$scope.auidtCount].module = 'Condition';
                $scope.auditArray[$scope.auidtCount].owner = $window.sessionStorage.userId;
                $scope.auditArray[$scope.auidtCount].datetime = new Date();
                $scope.auditArray[$scope.auidtCount].details = $scope.selectedMember.individualId;
                $scope.auditArray[$scope.auidtCount].countryId = $window.sessionStorage.countryId;
                $scope.auditArray[$scope.auidtCount].stateId = $window.sessionStorage.stateId;
                $scope.auditArray[$scope.auidtCount].districtId = $window.sessionStorage.districtId;
                $scope.auditArray[$scope.auidtCount].siteId = $window.sessionStorage.siteId.split(",")[0];

                Restangular.all('audittrials').post($scope.auditArray[$scope.auidtCount]).then(function (auditresp) {
                    $scope.auidtCount++;
                    $scope.saveAuditTrail(headerId, memberId, condition);
                });
            } else {
                if ($scope.existingFlag == true) {
                    $scope.existingDateUpdate();
                } else if ($scope.existingFlag == false) {
                    $scope.postCheckList(headerId, memberId, condition);
                }
            }
        };

        $scope.existCount = 0;

        $scope.checklistupdate = {};

        $scope.existingDateUpdate = function () {

            if ($scope.existCount < $scope.checklists.length) {

                $scope.checklists[$scope.existCount].status = $scope.checklists[$scope.existCount].statusFlag;

                Restangular.all('checklistheaders').customPUT($scope.checklists[$scope.existCount]).then(function (checkResp) {
                    //  console.log(checkResp);
                    $scope.existCount++;
                    $scope.existingDateUpdate();
                });
            } else {
                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setTimeout(function () {
                    window.location = "/conditions-list";
                }, 1500);
            }
        };

        $scope.checkCount = 0;

        $scope.checklistpost = {};

        $scope.postCheckList = function (headerId, memberId, condition) {

            if ($scope.checkCount < $scope.checklists.length) {

                $scope.checklistpost.checklist = $scope.checklists[$scope.checkCount].id;
                $scope.checklistpost.associatedHF = $scope.condition.associatedHF;
                $scope.checklistpost.conditionHeaderId = headerId;
                $scope.checklistpost.status = $scope.checklists[$scope.checkCount].statusFlag;
                $scope.checklistpost.countryId = $window.sessionStorage.countryId;
                $scope.checklistpost.stateId = $window.sessionStorage.stateId;
                $scope.checklistpost.districtId = $window.sessionStorage.districtId;
                $scope.checklistpost.siteId = $window.sessionStorage.siteId.split(",")[0];
                $scope.checklistpost.createdDate = new Date();
                $scope.checklistpost.createdBy = $window.sessionStorage.userId;
                $scope.checklistpost.createdByRole = $window.sessionStorage.roleId;
                $scope.checklistpost.lastModifiedDate = new Date();
                $scope.checklistpost.lastModifiedBy = $window.sessionStorage.userId;
                $scope.checklistpost.lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.checklistpost.deleteFlag = false;

                Restangular.all('checklistheaders').post($scope.checklistpost).then(function (checkResp) {
                    $scope.checkCount++;
                    $scope.postCheckList(headerId, memberId, condition);
                });
            } else {
                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setTimeout(function () {
                    window.location = "/screentreats-list";
                }, 350);
            }
        };

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };

        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dtmax = new Date();

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };

        $scope.toggleMin();
        $scope.picker = {};

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.open2 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepickerfollowup' + index).focus();
            });
            $scope.condition.followupopened = true;
        };

        $scope.conditionfollowupopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.condition.followupdatepick = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end///

    });
