'use strict';

angular.module('secondarySalesApp')
	.controller('RolesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
		/*********/
//		if ($window.sessionStorage.roleId != 1) {
//			window.location = "/";
//		}
		$scope.showForm = function () {
			var visible = $location.path() === '/roles/create' || $location.path() === '/roles/' + $routeParams.id;
			return visible;
		};
		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/roles/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/roles/create' || $location.path() === '/roles/' + $routeParams.id;
			return visible;
		};
		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/roles/create' || $location.path() === '/roles/' + $routeParams.id;
			return visible;
		};
		/*********/
		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.countryId = $window.sessionStorage.myRoute;
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};

		if ($window.sessionStorage.prviousLocation != "partials/roles") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
			$scope.currentpage = 1;
			$scope.pageSize = 25;
		}

		/*********************************** INDEX *******************************************/
		$scope.searchRole = $scope.name;
		$scope.$watch('role.name', function (newValue, oldValue) {
			if (newValue === oldValue) {
				return;
			} else {
				var RegExpression = /^[\a-zA-Z\s]*$/;
				if (RegExpression.test(newValue)) {

				} else {
					$scope.role.name = oldValue;
				}

			}
		});



		//  $scope.zones = Restangular.all('zones').getList().$object;
		$scope.displrol = Restangular.all('roles').getList().then(function (rol) {
			$scope.displayroles = rol;
			angular.forEach($scope.displayroles, function (member, index) {
				member.index = index + 1;
			});
		});

		/******************************************************* INDEX ********************************/
		$scope.rol = Restangular.all('roles').getList().then(function (rol) {
			$scope.roles = rol;
			angular.forEach($scope.roles, function (member, index) {
				member.index = index + 1;
			});
		});

		$scope.role = {
			"statearea": false,
			"districtarea": false,
			"communityarea": false,
			"countries": false,
			"states": false,
			"cities": false,
			"users": false,
			"roles": false,
			"groups": false,
			"employees": false,
			"im": false,
			"departments": false,
			"itemdefinition": false,
			"pricedefinition": false,
			"itemcategory": false,
			"itemtype": false,
			"pricecodes": false,
			"itemstatus": false,
			"warehouses": false,
			"warehousetypes": false,
			"unitmeasurements": false,
			"expensecategories": false,
			"expensetypes": false,
			"sdocumenttype": false,
			"fdocumenttype": false,
			"gender": false,
			"typology": false,
			"education": false,
			"maritalstatus": false,
			"employmentstatus": false,
			"physicalappearance": false,
			"emotionalstate": false,
			"reminderreason": false,
			"retailercategory": false,
			"competitorcompanies": false,
			"payterms": false,
			"paymodes": false,
			"site": false,
			"coorganisation": false,
			"fieldworkers": false,
			"beneficiaries": false,
			"salesroutelink": false,
			"salesdistributorlink": false,
			"salesrouteassignment": false,
			"fwsitelink": false,
			"fwsiteassignment": false,
			"retailerdetails": false,
			"tourtypes": false,
			"map": false,
			"agentroutelink": false,
			"routeassignment": false,
			"codashboard": false,
			"mgntdashboard": false,
			"schemes": false,
			"providers": false,
			"cocategory": false,
			"onetoonemember": false,
			"memberregistration": false,
			"applyforscheme": false,
			"applyfordocument": false,
			"groupmeeting": false,
			"stakeholdermeeting": false,
			"reportincident": false,
			"bulkupdate": false,
			"archivemember": false,
			"dashboardfw": false,
			"fieldworker": false,
			"assignfieldworkertosite": false,
			"event": false,
			"comanagerprofile": false,
			"siteview": false,
			"dashboardfacility": false,
            "spmbudget": false

		};
		/****************************************** SAVE ********************************/
		/*
       $scope.validatestring = '';
        $scope.Save = function () {
                document.getElementById('name').style.border = "";
        if ($scope.role.name == '' || $scope.role.name == null) {
                $scope.role.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your role';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } 
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.roles.post($scope.role).then(function () {
                        console.log('$scope.role', $scope.role);
                        window.location = '/roles';
                    });
                }
        };
 */
		$scope.validatestring = '';
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			if ($scope.role.name == '' || $scope.role.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.all('roles').post($scope.role).then(function () {
					//$scope.roles.post($scope.role).then(function () {
					console.log('$scope.role', $scope.role);
					window.location = '/roles';
				});
			}
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/******************************** UPDATE ********************************/

		$scope.validatestring = '';
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			if ($scope.role.name == '' || $scope.role.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				//$scope.roles.customPUT($scope.role).then(function () {
				Restangular.one('roles',$routeParams.id).customPUT($scope.role).then(function () {
					console.log('$scope.role', $scope.role);
					window.location = '/roles';
				});
			}
		};


		if ($routeParams.id) {
			$scope.message = 'Role has been Updated!';
			Restangular.one('roles', $routeParams.id).get().then(function (role) {
				$scope.original = role;
				$scope.role = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Role has been Created!';
		}

		/*************************************************** DELETE ********************************/

		$scope.updateRoleFlag = function (id) {
			if (confirm("Are you sure want to delete..!") == true) {
				Restangular.one('roles/' + id).remove($scope.role).then(function () {
					$route.reload();
				});

			} else {

			}

		}



	});
