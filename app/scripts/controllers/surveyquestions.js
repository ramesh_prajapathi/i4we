'use strict';

angular.module('secondarySalesApp')
    .controller('SurveyQuestionsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/surveyquestions/create' || $location.path() === '/surveyquestions/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/surveyquestions/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/surveyquestions/create';
            return visible;
        };
        $scope.distributionArea = {
            zoneId: ''
        };

        $scope.YesPopup = true;
        $scope.NoPopup = true;
        $scope.YesIncrement = true;
        $scope.NoIncrement = true;
        $scope.YesProvideInfo = true;
        $scope.NoProvideInfo = true;

        /*********/
		 /***************************** Pagination ***********************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.pillarid = $window.sessionStorage.myRoute;
        }

		if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $window.sessionStorage.myRoute_currentPage = 1;
         }
	
        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        
        /*************************************************************************************************/

        $scope.surveyquestns = Restangular.all('surveyquestions').getList().$object;
        $scope.pillars = Restangular.all('pillars').getList().$object;

        // $scope.salesAreas = Restangular.all('sales-areas').getList().$object;

        $scope.NotSubQuestion = true;
        $scope.NotSkipQuestion = true;
        $scope.YesNotSkipQuestion = true;
        $scope.NoNotSkipQuestion = true;
        $scope.NotCountedCheckbox = true;
        $scope.NotRange = true;
        $scope.NotScheme = true;

        $scope.$watch('surveyquestion.pillarid', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.todotypes = Restangular.all('todotypes?filter[where][pillarid]=' + newValue).getList().$object;
            }
        });

        $scope.$watch('surveyquestion.questiontype', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else if (newValue == 'sq') {
                $scope.NotSubQuestion = false;
                $scope.NotSkipQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            } else if (newValue == 'skp') {
                $scope.NotSkipQuestion = false;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            } else if (newValue == 'cc') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = false;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            } else if (newValue == 'rt') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = false;
                $scope.NotScheme = true;
            } else if (newValue == 'scheme') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = false;
            } else {
                $scope.NotSubQuestion = true;
                $scope.NotSkipQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            }
        });

        $scope.$watch('surveyquestion.yesaction', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else if (newValue == 'popup') {
                $scope.YesPopup = false;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'skp') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = false;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'increment') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = false;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'incrementonly') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = false;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'incrementskip') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = false;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'incrementonlyskip') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = false;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'providenextquestion') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = false;
            } else {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
            }
        });

        $scope.$watch('surveyquestion.noaction', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else if (newValue == 'popup') {
                $scope.NoPopup = false;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'skp') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = false;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'increment') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementonly') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementskip') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementonlyskip') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'providenextquestion') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = false;
            } else {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
            }
        });
        $scope.YesTodo = true;
        $scope.YesDocument = true;
        $scope.NoTodo = true;
        $scope.NoDocument = true;
        $scope.$watch('surveyquestion.yespopup', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else if (newValue == 'todos') {
                $scope.YesTodo = false;
                $scope.YesDocument = true;
            } else if (newValue == 'dateforid') {
                $scope.YesDocument = false;
                $scope.YesTodo = true;
            } else if (newValue == 'applicationflow') {
                $scope.YesDocument = false;
                $scope.YesTodo = true;
            } else {
                $scope.YesTodo = true;
                $scope.YesDocument = true;
            }
        });

        $scope.$watch('surveyquestion.nopopup', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else if (newValue == 'todos') {
                $scope.NoTodo = false;
                $scope.NoDocument = true;
            } else if (newValue == 'dateforid') {
                $scope.NoDocument = false;
                $scope.NoTodo = true;
            } else if (newValue == 'applicationflow') {
                $scope.NoDocument = false;
                $scope.NoTodo = true;
            } else {
                $scope.NoTodo = true;
                $scope.NoDocument = true;
            }
        });

        $scope.$watch('surveyquestion.yesdocumentflag', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                if (newValue == 'yes') {
                    $scope.yesdocumentsorschemes = Restangular.all('documenttypes?filter[where][deleteflag]=null').getList().$object;
                }
                else{
                    $scope.yesdocumentsorschemes = [{id:1,name:'One'},{id:2,name:'Two'},{id:3,name:'Three'},{id:4,name:'Four'},{id:5,name:'Five'}];
                }
            }
        });

        $scope.$watch('surveyquestion.nodocumentflag', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                if (newValue == 'yes') {
                    $scope.nodocumentsorschemes = Restangular.all('documenttypes?filter[where][deleteflag]=null').getList().$object;
                }
                else{
                    $scope.nodocumentsorschemes = [{id:1,name:'One'},{id:2,name:'Two'},{id:3,name:'Three'},{id:4,name:'Four'},{id:5,name:'Five'}];
                }
            }
        });


        $scope.getSurveyType = function (surveytypeid) {
            return Restangular.one('surveytypes', surveytypeid).get().$object;
        };

        $scope.getPilllar = function (pillarid) {
            return Restangular.one('pillars', pillarid).get().$object;
        };
        /*********/

        //$scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
        $scope.surveyquestions = Restangular.all('surveyquestions?filter[order][0]=pillarid ASC&filter[order][1]=serialno ASC').getList().$object;
        $scope.surveytypeid = '';
        $scope.surveysubcategoryid = '';
        $scope.subcategoryid = '';
        $scope.surveyid = '';

        $scope.$watch('pillarid', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
				$scope.pillarid = $window.sessionStorage.myRoute;
                $scope.surveyquestions = Restangular.all('surveyquestions?filter[where][pillarid]=' + newValue + '&filter[order][0]=pillarid ASC&filter[order][1]=serialno ASC').getList().$object;
                $scope.surveyid = +newValue;
            }
        });

        $scope.sub_surveyquestions = [];
        $scope.$watch('taskbreakup', function (newValue, oldValue) {
            if (newValue > 20) {
                $scope.taskbreakup = '';
                alert('Task Breakup Cannot Exceed 20');
            }
            if (newValue <= 20) {
                $scope.sub_surveyquestions = [];
                for (var i = 0; i < newValue; i++) {

                    $scope.sub_surveyquestions.push({
                        "question": null,
                        "gujrathi": null,
                        "surveytypeid": null,
                        "surveysubcategoryid": null,
                        "questiontype": null,
                        "noofdropdown": null,
                        "answers": null,
                        "questionid": null,
                        "questionno": null
                    });

                }

            }
        });

        $scope.addRouteLinks = function (value) {
            console.log('length', $scope.sub_surveyquestions.length);
            var count = 0;
            if ($scope.sub_surveyquestions.length == 0) {
                $scope.surveyquestns.post($scope.surveyquestion).then(function (surveyresponse) {
                    console.log('main question saved', surveyresponse);
                    window.location = "/surveyquestions";
                });
            } else {
                $scope.surveyquestns.post($scope.surveyquestion).then(function (surveyresponse) {
                    console.log('main question saved', surveyresponse);
                    for (var i = 0; i < $scope.sub_surveyquestions.length; i++) {
                        $scope.sub_surveyquestions[i].surveytypeid = surveyresponse.surveytypeid;
                        $scope.sub_surveyquestions[i].surveysubcategoryid = surveyresponse.surveysubcategoryid;
                        $scope.sub_surveyquestions[i].questionid = surveyresponse.id;
                        $scope.surveyquestns.post($scope.sub_surveyquestions[i]).then(function (response) {
                            console.log('sub question saved', response);
                            count++;
                            if (count == $scope.sub_surveyquestions.length) {
                                //$route.reload();
                                window.location = "/surveyquestions";
                            }
                        });
                    }
                });
            }
        };

        if ($routeParams.id) {
            Restangular.one('surveyquestions', $routeParams.id).get().then(function (surveyquestion) {
                $scope.original = surveyquestion;
                $scope.surveyquestion = Restangular.copy($scope.original);
            });
        }
    });