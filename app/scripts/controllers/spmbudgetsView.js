'use strict';
angular.module('secondarySalesApp').controller('spmbudgetsViewCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $modal) {

    $scope.SPM_Budget_Year = '';
    $scope.SPM_Budget_CO = '';
    /*     $scope.$watch('SPM_Budget_Year', function (newValue, oldValue) {
                //console.log('SPM_Budget_Year', newValue);
                if (newValue === oldValue || newValue == '') {
                    return;
                } else {
                    $scope.SPM_Budget_CO = '';
                    $scope.CO_total_Budget = 0;
                    Restangular.all('spmbudgets?filter[where][year]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (respon) {
                        $scope.spmbudgets = respon;
                        angular.forEach($scope.spmbudgets, function (member, index) {
                            member.index = index + 1;
                        });


                        Restangular.all('cobudgets?filter[where][year]=' + respon[0].year + '&filter[where][facility]=' + respon[0].facility).getList().then(function (cobudget) {
                            console.log('cobudget', cobudget)
                            for (var i = 0; i < cobudget.length; i++) {
                                $scope.CO_total_Budget = $scope.CO_total_Budget + cobudget[i].totalamount;
                            }
                            console.log('$scope.CO_total_Budget', $scope.CO_total_Budget)
                        });
                    });
                    $scope.yearid = +newValue;
                }
            });

            $scope.$watch('SPM_Budget_Year', function (newValue, oldValue) {
                //console.log('SPM_Budget_Year', newValue);
                if (newValue === oldValue || newValue == '') {
                    return;
                } else {
                    $scope.totalCo = 0;
                    $scope.emps = Restangular.all('cobudgets?filter[where][year]=1' + '&filter[where][deleteflag]=false').getList().then(function (emps) {
                        $scope.cobudgets = emps;
                        console.log('$scope.cobudgets ', $scope.cobudgets);
                        $scope.emps = Restangular.all('spmbudgets?filter[where][year]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (emps) {
                            $scope.stakeholdertypes = emps;
                            $scope.zn = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (zn) {
                                $scope.stakeholders = zn;
                                //$scope.modalInstanceLoad.close();
                                angular.forEach($scope.stakeholders, function (member, index) {
                                    member.index = index + 1;
                                    /// member.stakeholdertypes = $scope.getStakeholdertype(member.stakeholdertype);
                                    for (var n = 0; n < $scope.stakeholdertypes.length; n++) {
                                        if (member.id == $scope.stakeholdertypes[n].state) {
                                            member.Sitename = $scope.stakeholdertypes[n];
                                            $scope.Sitename_humanresources = $scope.stakeholdertypes[n].totalamount;
                                            //console.log('Sitename', member.Sitename.totalamount)
                                            break;
                                        }
                                    }

                                    for (var o = 0; o < $scope.cobudgets.length; o++) {
                                        if (member.id == $scope.cobudgets[o].state) {
                                            member.COTotal = $scope.totalCo + $scope.cobudgets[o].totalamount;
                                            //$scope.Sitename_humanresources = $scope.stakeholdertypes[n].totalamount;
                                            console.log('Sitename', member.COTotal)
                                            $scope.totalCo = $scope.totalCo + $scope.cobudgets[o].totalamount;
                                            console.log(' $scope.totalCo', $scope.totalCo)
                                        }
                                    }
                                    //member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                                    $scope.TotalTodos = [];
                                    $scope.TotalTodos.push(member);
                                });
                            });
                        });
                    });
                }
            });
*/

    console.log('State', $window.sessionStorage.zoneId);
    $scope.totalCo = 0;
    $scope.Yr_Total = 0;
    $scope.$watch('SPM_Budget_Year', function (newValue, oldValue) {
        //console.log('SPM_Budget_Year', newValue);
        if (newValue === oldValue || newValue == '') {
            return;
        } else {

            $scope.SPM_Budget_CO = '';
            Restangular.all('cobudgets?filter[where][year]=' + newValue + '&filter[where][state]=' + $window.sessionStorage.zoneId).getList().then(function (cobudget) {
                $scope.cobudgets = cobudget;
                $scope.emps = Restangular.all('spmbudgets?filter[where][year]=' + newValue + '&filter[where][deleteflag]=false' + '&filter[where][state]=' + $window.sessionStorage.zoneId).getList().then(function (emps) {
                    $scope.spmbudgets = emps;
                    angular.forEach($scope.spmbudgets, function (member, index) {
                        member.index = index + 1;
                        member.COTotal = 0;
                        for (var n = 0; n < $scope.cobudgets.length; n++) {
                            if (member.facility == $scope.cobudgets[n].facility) {
                                member.COTotal = member.COTotal + $scope.cobudgets[n].totalamount;
                            }
                        }
                        $scope.TotalTodos = [];
                        $scope.TotalTodos.push(member);
                    });
                });
            });
            $scope.yearid = +newValue;
        }

    });


    /*$scope.$watch('SPM_Budget_CO', function (newValue, oldValue) {
        // console.log('SPM_Budget_CO', newValue);
        if (newValue === oldValue || newValue == '') {
            return;
        } else {
            Restangular.all('comembers?filter[where][facility]=' + newValue + '&filter[where][deleteflag]=false&filter[where][hdfflag]=false').getList().then(function (comembers) {
                console.log('comembers.length', comembers.length);
                if (comembers.length == 0) {
                    $scope.spmbudgets = '';
                    return;
                } else {
                    //if (comembers.length > 0) {
                    Restangular.all('spmbudgets?filter[where][comember]=' + comembers[0].id + '&filter[where][year]=' + $scope.yearid + '&filter[where][deleteflag]=false').getList().then(function (respon) {
                        $scope.spmbudgets = respon;
                        $scope.facility_Id = respon[0].facility;
                        angular.forEach($scope.spmbudgets, function (member, index) {
                            member.index = index + 1;
                        });
                    });
                    $scope.Your_Selected_Co = comembers[0].id;
                }



            });

            Restangular.all('cobudgets?filter[where][year]=' + $scope.yearid + '&filter[where][facility]=' + $scope.facility_Id).getList().then(function (cobudget) {
                console.log('cobudget', cobudget)
                for (var i = 0; i < cobudget.length; i++) {
                    $scope.CO_total_Budget = $scope.CO_total_Budget + cobudget[i].totalamount;
                }
                console.log('$scope.CO_total_Budget', $scope.CO_total_Budget)
            });

        }

    });*/

    /* $scope.$watch('SPM_Budget_CO', function (newValue, oldValue) {
         // console.log('SPM_Budget_CO', newValue);
         if (newValue === oldValue || newValue == '') {
             return;
         } else {
             Restangular.all('comembers?filter[where][facility]=' + newValue + '&filter[where][deleteflag]=false&filter[where][hdfflag]=false').getList().then(function (comembers) {
                 console.log('comembers.length', comembers.length);
                 if (comembers.length == 0) {
                     $scope.spmbudgets = '';
                     return;
                 } else {
                     //if (comembers.length > 0) {
                     Restangular.all('spmbudgets?filter[where][comember]=' + comembers[0].id + '&filter[where][year]=' + $scope.yearid + '&filter[where][deleteflag]=false').getList().then(function (respon) {
                         $scope.spmbudgets = respon;
                         $scope.facility_Id = respon[0].facility;
                         angular.forEach($scope.spmbudgets, function (member, index) {
                             member.index = index + 1;
                         });
                     });
                     $scope.Your_Selected_Co = comembers[0].id;
                 }



             });

             Restangular.all('cobudgets?filter[where][year]=' + $scope.yearid + '&filter[where][facility]=' + $scope.facility_Id).getList().then(function (cobudget) {
                 console.log('cobudget', cobudget)
                 for (var i = 0; i < cobudget.length; i++) {
                     $scope.CO_total_Budget = $scope.CO_total_Budget + cobudget[i].totalamount;
                 }
                 console.log('$scope.CO_total_Budget', $scope.CO_total_Budget)
             });

         }

     })*/

    $scope.$watch('SPM_Budget_CO', function (newValue, oldValue) {
        // console.log('SPM_Budget_CO', newValue);
        if (newValue === oldValue || newValue == '') {
            return;
        } else {

            Restangular.all('comembers?filter[where][facility]=' + newValue + '&filter[where][deleteflag]=false&filter[where][hdfflag]=false').getList().then(function (comembers) {
                console.log('comembers.length', comembers.length);
                if (comembers.length == 0) {
                    $scope.spmbudgets = '';
                    return;
                } else {
                    //if (comembers.length > 0) {
                    Restangular.all('cobudgets?filter[where][year]=' + $scope.yearid + '&filter[where][state]=' + $window.sessionStorage.zoneId).getList().then(function (cobudget) {
                        $scope.cobudgets = cobudget;
                        Restangular.all('spmbudgets?filter[where][comember]=' + comembers[0].id + '&filter[where][year]=' + $scope.yearid + '&filter[where][deleteflag]=false' + '&filter[where][state]=' + $window.sessionStorage.zoneId).getList().then(function (respon) {
                            $scope.spmbudgets = respon;
                           // $scope.facility_Id = respon[0].facility;
                            angular.forEach($scope.spmbudgets, function (member, index) {
                                member.index = index + 1;
                                member.COTotal = 0;

                                for (var n = 0; n < $scope.cobudgets.length; n++) {
                                    if (member.facility == $scope.cobudgets[n].facility) {
                                        member.COTotal = member.COTotal + $scope.cobudgets[n].totalamount;
                                        //console.log('Sitename', member.Sitename.totalamount)
                                       // break;
                                    }
                                }
                                $scope.TotalTodos = [];
                                $scope.TotalTodos.push(member);
                            });
                        });
                    });
                    $scope.Your_Selected_Co = comembers[0].id;
                }
            });
        }

    });

    $scope.Comemer = function (coid) {
        return Restangular.one('employees', coid).get().$object;
    }

    Restangular.all('spmbudgetyears?filter[order]=name%20ASC').getList().then(function (bud) {
        $scope.spmbudgetyears = bud;
    });

    Restangular.all('employees?filter[where][stateId]=' + $window.sessionStorage.zoneId + '&filter[where][deleteflag]=false').getList().then(function (coRes) {
        $scope.printcomembers = coRes;
       // console.log('printcomembers', coRes)
    });




});
