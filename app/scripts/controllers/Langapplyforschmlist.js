'use strict';

angular.module('secondarySalesApp')
    .controller('LangapplyforschmlistCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/
    
      /***************************** Pagination ***********************************/
            if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
                $window.sessionStorage.myRoute = null;
                $window.sessionStorage.myRoute_currentPage = 1;
                $window.sessionStorage.myRoute_currentPagesize = 25;
            } else {
                $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
                $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
                $scope.pillarid = $window.sessionStorage.myRoute;
                //console.log('$scope.pillarid', $scope.pillarid);
            }

            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.PageChanged = function (newPage, oldPage) {
                $scope.currentpage = newPage;
                $window.sessionStorage.myRoute_currentPage = newPage;
            };

            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.pageFunction = function (mypage) {
                $scope.pageSize = mypage;
                $window.sessionStorage.myRoute_currentPagesize = mypage;
            };

            // console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
            //if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage-View") {
            if ($window.sessionStorage.prviousLocation != "partials/Langapplyforschmlist" && $window.sessionStorage.prviousLocation != "partials/LangAFScheme") {
                $window.sessionStorage.myRoute = '';
                $window.sessionStorage.myRoute_currentPagesize = 25;
                $window.sessionStorage.myRoute_currentPage = 1;
                //$scope.currentpage = 1;
                //$scope.pageSize = 5;
            }



        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

    
        Restangular.all('schemeLanguages?filter[where][deleteFlag]=false').getList().then(function (response) {
            $scope.schemeLanguages = response;

            angular.forEach($scope.schemeLanguages, function (member, index) {
                member.index = index + 1;
                
                Restangular.one('languages', member.language).get().then (function (lng) {
                     member.langname = lng.name;
                });
                // console.log(' $scope.schemeLanguages', $scope.schemeLanguages);
            });
        });
    
        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /************************************************/
    
    
});