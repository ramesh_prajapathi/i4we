'use strict';

angular.module('secondarySalesApp')
	.controller('RoutesCreateCtrl', function ($scope, Restangular, $window, $filter) {
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.isCreateView = true;
		$scope.heading = 'Site Create';
		$scope.Sitedistricdisable = false;
		$scope.Sitedistricdisable = false;
		$scope.Sitefacilitydisable = false;
		//$scope.routes = Restangular.all('distribution-routes').getList().$object;
		/*********************************** INDEX *******************************************/
		$scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;


		$scope.$watch('route.zoneId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
				$scope.zonalid = newValue;
			}
		});


		$scope.$watch('route.salesAreaId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$scope.facilities = Restangular.all('employees?filter[where][stateId]=' + $scope.zonalid + '&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
			}
		});

	$scope.route = {
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			deleteflag: false
		}

		$scope.modalTitle = 'Thank You';
		$scope.message = 'Site has been created';
		$scope.showValidation = false;
	
		$scope.toggleValidation = function () {
			console.log('toggleValidation');
			$scope.showValidation = !$scope.showValidation;
		};

		/************************************ SAVE *******************************************/
		$scope.validatestring = '';
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			
			if ($scope.route.name == '' || $scope.route.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Site Name';
				document.getElementById('name').style.borderColor = "#FF0000";
				
			} else if ($scope.route.zoneId == '' || $scope.route.zoneId == null) {
				$scope.validatestring = $scope.validatestring + 'Please Select State';
				//document.getElementById('state').style.borderColor = "#FF0000";
				
			} else if ($scope.route.salesAreaId === '' || $scope.route.salesAreaId === null) {
				$scope.validatestring = $scope.validatestring + 'Please Select District';
				//document.getElementById('state').style.borderColor = "#FF0000";
				
			} else if ($scope.route.partnerId == '' || $scope.route.partnerId == null) {
				$scope.validatestring = $scope.validatestring + 'Please Select Facility';
				//document.getElementById('state').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				Restangular.all('distribution-routes').post($scope.route).then(function (znResponse) {	
					console.log('$scope.route', $scope.znResponse);
					window.location = '/routes';
				});
			}
		};

	});
