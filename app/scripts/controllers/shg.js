'use strict';

angular.module('secondarySalesApp')
    .controller('ShgCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/

        $rootScope.clickFW();
        $scope.SHGLanguage = {};

        //$scope.loaderMainModal = true;

        Restangular.one('shglanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.SHGLanguage = langResponse[0];

            $scope.SHGheading = $scope.SHGLanguage.shgCreate;
            //  $scope.modalTitle = $scope.SHGLanguage.thankYou;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        $scope.someFocusVariable = true;
        $scope.FocusMe = true;

        $scope.showForm = function () {
            var visible = $location.path() === '/shg/create' || $location.path() === '/shg/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/shg/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/shg/create';
            return visible;
        };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/zones") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            // console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*$scope.distributionArea = {
                zoneId: ''
            };*/

        /*  $scope.mybool = 'false';
          console.log('$scope.mybool', $scope.mybool);*/

        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};

        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };

        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };

        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };

        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];
        //Datepicker settings end

        /***************************** Pagination ***********************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.pillarid = $window.sessionStorage.myRoute;
            //console.log('$scope.pillarid', $scope.pillarid);
        }

        $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        // console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        //if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage-View") {
        if ($window.sessionStorage.prviousLocation != "partials/shg-view" && $window.sessionStorage.prviousLocation != "partials/shg") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $window.sessionStorage.myRoute_currentPage = 1;
            //$scope.currentpage = 1;
            //$scope.pageSize = 5;
        }

        /*************************************************************************************************/

        $scope.YesPopup = true;
        $scope.NoPopup = true;
        $scope.YesIncrement = true;
        $scope.NoIncrement = true;
        $scope.YesProvideInfo = true;
        $scope.NoProvideInfo = true;

        /*********/

        $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;
        $scope.meetingintaldisplay = Restangular.all('meetingintervals?filter[where][deleteFlag]=false').getList().$object;
        $scope.associatedbanksdsply = Restangular.all('associatedbanks?filter[where][deleteFlag]=false').getList().$object;
        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;
        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;
        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;
        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;
        $scope.roledsply = Restangular.all('roles').getList().$object;

        //  $scope.submitsurveyquestns = Restangular.all('surveyquestions').getList().$object;

        // $scope.shgArray = [];
        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.filterCall = 'shgs?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            //{"membersInGroup":{"nin":[""]}},
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}}]}}';
            
             $scope.hideDelete = true;


        } else {
            $scope.hideDelete = false;

            $scope.filterCall = 'shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            // {"membersInGroup":{"nin":[""]}},
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}}]}}';

        }

        Restangular.all($scope.memberFilterCall).getList().then(function (mbrs) {
            $scope.finalMembers = mbrs;


            Restangular.all($scope.filterCall).getList().then(function (shg) {
                $scope.shgs = shg;

                $scope.modalInstanceLoad.close();
                $scope.shgArray = [];

                angular.forEach($scope.shgs, function (member, index) {
                    member.index = index + 1;
                    member.DispalylastMeetingDate = $filter('date')(member.lastMeetingDate, 'dd/MM/yyyy');
                    member.formationDate = $filter('date')(member.formationDate, 'dd/MM/yyyy');
                    member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                    member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');
                    member.lastMeetingDate = $filter('date')(member.lastMeetingDate, 'dd-MMM-yyyy');


                    var data2 = $scope.usersdisplay.filter(function (arr) {
                        return arr.id == member.associatedHF
                    })[0];

                    if (data2 != undefined) {
                        member.associatedHFname = data2.name;
                    }

                    var data3 = $scope.meetingintaldisplay.filter(function (arr) {
                        return arr.id == member.meetingInterval
                    })[0];

                    if (data3 != undefined) {
                        member.meetingIntervalname = data3.name;
                    }

                    var data4 = $scope.associatedbanksdsply.filter(function (arr) {
                        return arr.id == member.associatedBank
                    })[0];

                    if (data4 != undefined) {
                        member.associatedBankname = data4.name;
                    }

                    var data5 = $scope.countrydisplay.filter(function (arr) {
                        return arr.id == member.countryId
                    })[0];

                    if (data5 != undefined) {
                        member.countryname = data5.name;
                    }

                    var data6 = $scope.statedisplay.filter(function (arr) {
                        return arr.id == member.stateId
                    })[0];

                    if (data6 != undefined) {
                        member.statename = data6.name;
                    }

                    var data7 = $scope.districtdsply.filter(function (arr) {
                        return arr.id == member.districtId
                    })[0];

                    if (data7 != undefined) {
                        member.districtname = data7.name;
                    }

                    var data8 = $scope.sitedsply.filter(function (arr) {
                        return arr.id == member.siteId
                    })[0];

                    if (data8 != undefined) {
                        member.sitename = data8.name;
                    }

                    var data9 = $scope.usersdisplay.filter(function (arr) {
                        return arr.id == member.createdBy
                    })[0];

                    if (data9 != undefined) {
                        member.createdByname = data9.username;
                    }

                    var data10 = $scope.usersdisplay.filter(function (arr) {
                        return arr.id == member.lastModifiedBy
                    })[0];

                    if (data10 != undefined) {
                        member.lastModifiedByname = data10.username;
                    }

                    var data11 = $scope.roledsply.filter(function (arr) {
                        return arr.id == member.lastModifiedByRole
                    })[0];

                    if (data11 != undefined) {
                        member.lastModifiedByRolename = data11.name;
                    }

                    var data12 = $scope.roledsply.filter(function (arr) {
                        return arr.id == member.createdByRole
                    })[0];

                    if (data12 != undefined) {
                        member.createdByRolename = data12.name;
                    }



                    if (member.membersInGroup == null || member.membersInGroup == "") {
                        member.membersInGroupName = "";
                        member.membersInGroupLength = 0;

                    } else {

                        member.membersInGroupLength = member.membersInGroup.split(',').length;
                        member.arraymembersInGroup = member.membersInGroup.split(",");
                        
                        member.membersInGroupName = "";
                        
                      //  console.log(member.arraymembersInGroup);

                        for (var i = 0; i < member.arraymembersInGroup.length; i++) {
                            for (var j = 0; j < $scope.finalMembers.length; j++) {
                                
                                member.arraymembersInGroup[i] = member.arraymembersInGroup[i].trim();
                                
                                if ($scope.finalMembers[j].id == member.arraymembersInGroup[i]) {
                                    if (member.membersInGroupName == "") {
                                        member.membersInGroupName = $scope.finalMembers[j].name + ' - ' + $scope.finalMembers[j].individualId;
                                       // console.log(member.membersInGroupName);
                                    } else {
                                        member.membersInGroupName = member.membersInGroupName + ' , ' + $scope.finalMembers[j].name + ' - ' + $scope.finalMembers[j].individualId;
                                       // console.log(member.membersInGroupName);
                                    }
                                    break;
                                }
                            }
                        }

                    }
                    if (member.lastMeetingDate == null) {
                        member.DispalylastMeetingDate = "";
                    }

                    $scope.TotalData = [];
                    $scope.TotalData.push(member);
                });
            });
        });

        $scope.status = 'active';

        $scope.$watch('status', function (newValue, oldValue) {
            if (newValue === oldValue || newValue === '' || newValue === null) {
                return;
            } else {

                $scope.shgArray = [];

                Restangular.all($scope.memberFilterCall).getList().then(function (mbrs) {

                    $scope.finalMembers = mbrs;

                    if ($window.sessionStorage.roleId + "" === "3") {
                        $scope.filterCall = 'shgs?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                        // {"membersInGroup":{"nin":[""]}},

                    } else {
                        $scope.filterCall = 'shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

                        //{"membersInGroup":{"nin":[""]}},
                    }

                    Restangular.all($scope.filterCall).getList().then(function (shg) {

                        $scope.shgs = shg;

                        angular.forEach($scope.shgs, function (member, index) {
                            member.index = index + 1;

                            member.DispalylastMeetingDate = $filter('date')(member.lastMeetingDate, 'dd/MM/yyyy');
                            member.formationDate = $filter('date')(member.formationDate, 'dd/MM/yyyy');
                            member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                            member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');
                            member.lastMeetingDate = $filter('date')(member.lastMeetingDate, 'dd-MMM-yyyy');


                            var data2 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.associatedHF
                            })[0];

                            if (data2 != undefined) {
                                member.associatedHFname = data2.name;
                            }

                            var data3 = $scope.meetingintaldisplay.filter(function (arr) {
                                return arr.id == member.meetingInterval
                            })[0];

                            if (data3 != undefined) {
                                member.meetingIntervalname = data3.name;
                            }

                            var data4 = $scope.associatedbanksdsply.filter(function (arr) {
                                return arr.id == member.associatedBank
                            })[0];

                            if (data4 != undefined) {
                                member.associatedBankname = data4.name;
                            }

                            var data5 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data5 != undefined) {
                                member.countryname = data5.name;
                            }

                            var data6 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == member.stateId
                            })[0];

                            if (data6 != undefined) {
                                member.statename = data6.name;
                            }

                            var data7 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == member.districtId
                            })[0];

                            if (data7 != undefined) {
                                member.districtname = data7.name;
                            }

                            var data8 = $scope.sitedsply.filter(function (arr) {
                                return arr.id == member.siteId
                            })[0];

                            if (data8 != undefined) {
                                member.sitename = data8.name;
                            }

                            var data9 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.createdBy
                            })[0];

                            if (data9 != undefined) {
                                member.createdByname = data9.username;
                            }

                            var data10 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.lastModifiedBy
                            })[0];

                            if (data10 != undefined) {
                                member.lastModifiedByname = data10.username;
                            }

                            var data11 = $scope.roledsply.filter(function (arr) {
                                return arr.id == member.lastModifiedByRole
                            })[0];

                            if (data11 != undefined) {
                                member.lastModifiedByRolename = data11.name;
                            }

                            var data12 = $scope.roledsply.filter(function (arr) {
                                return arr.id == member.createdByRole
                            })[0];

                            if (data12 != undefined) {
                                member.createdByRolename = data12.name;
                            }

                            if (member.membersInGroup == null || member.membersInGroup == "") {

                                member.membersInGroupName = "";
                                member.membersInGroupLength = 0;

                            } else {
                                
                                member.membersInGroupLength = member.membersInGroup.split(',').length;
                                //console.log('member.membersInGroupLength', member.membersInGroupLength);
                                member.arraymembersInGroup = member.membersInGroup.split(",");
                                member.membersInGroupName = "";

                                for (var i = 0; i < member.arraymembersInGroup.length; i++) {
                                    for (var j = 0; j < $scope.finalMembers.length; j++) {
                                        
                                        member.arraymembersInGroup[i] = member.arraymembersInGroup[i].trim();
                                        
                                        if ($scope.finalMembers[j].id == member.arraymembersInGroup[i]) {
                                            if (member.membersInGroupName == "") {
                                                member.membersInGroupName = $scope.finalMembers[j].name + ' - ' + $scope.finalMembers[j].individualId;
                                               // console.log(member.membersInGroupName);
                                            } else {
                                                member.membersInGroupName = member.membersInGroupName + ' , ' + $scope.finalMembers[j].name + ' - ' + $scope.finalMembers[j].individualId;
                                              //  console.log(member.membersInGroupName);
                                            }
                                            break;
                                        }
                                    }
                                }
                            }

                            if (member.lastMeetingDate == null) {
                                member.DispalylastMeetingDate = "";
                            }

                            $scope.TotalData = [];
                            $scope.TotalData.push(member);
                        });
                    });
                });
            }
        });

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        /**************************Export data to excel sheet ***************/

        $scope.DisableExport = false;
        $scope.valShgCount = 0;

        $scope.exportData = function () {
            $scope.shgArray = [];
            $scope.valShgCount = 0;
            // console.log('$scope.shgs', $scope.shgs);
            if ($scope.shgs.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.shgs.length; c++) {
                    $scope.shgArray.push({
                        'Sr No': $scope.shgs[c].index,
                        'COUNTRY': $scope.shgs[c].countryname,
                        'STATE': $scope.shgs[c].statename,
                        'DISTRICT': $scope.shgs[c].districtname,
                        'SITE': $scope.shgs[c].sitename,
                        'GROUP NAME': $scope.shgs[c].groupName,
                        'DATE OF FORMATION': $scope.shgs[c].formationDate,
                        'ASSIGNED TO': $scope.shgs[c].associatedHFname,
                        'MEETING INTERVAL': $scope.shgs[c].meetingIntervalname,
                        'MEMBERS IN GROUP': $scope.shgs[c].membersInGroupName,
                        'AMOUNT TO SAVE': $scope.shgs[c].amountToSave,
                        'INTEREST RATE': $scope.shgs[c].interestRate,
                        'ASSOCIATED BANK': $scope.shgs[c].associatedBankname,
                        'SHG ACCOUNT NO.': $scope.shgs[c].shgAccountNumber,
                        'LAST METTING DATE': $scope.shgs[c].DispalylastMeetingDate,
                        'CREATED BY': $scope.shgs[c].createdByname,
                        'CREATED DATE': $scope.shgs[c].createdDate,
                        'CREATED ROLE': $scope.shgs[c].createdByRolename,
                        'LAST MODIFIED BY': $scope.shgs[c].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.shgs[c].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.shgs[c].lastModifiedByRolename,
                        'DELETE FLAG': $scope.shgs[c].deleteFlag
                    });

                    $scope.valShgCount++;
                    if ($scope.shgs.length == $scope.valShgCount) {
                        alasql('SELECT * INTO XLSX("shg.xlsx",{headers:true}) FROM ?', [$scope.shgArray]);
                    }
                }
            }
        };

        /*********************end******************************************/

        Restangular.all('households?filter[where][deleteFlag]=false').getList().then(function (hh) {
            $scope.households = hh;
            $scope.pillarid = $window.sessionStorage.myRoute;
        });

        Restangular.all('associatedbanks?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (banks) {
            $scope.associatedbanks = banks;
            $scope.shg.associatedBank = $scope.shg.associatedBank;
        });

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.disableAssigned = true;

            $scope.hideAddBtn = true;

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.shg.associatedHF = $window.sessionStorage.userId;

            });
        } else {

            $scope.disableAssigned = false;

            $scope.hideAddBtn = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.shg.associatedHF = $scope.shg.associatedHF;

            });

        }


        //('sites?filter={"where":{"and":[{"gender":2},{"age":{"gt":[17]}},{"shgId":{"inq":[null]}}]}}')
        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.memberFilterCall = 'members?filter[where][deleteFlag]=false' + '&filter[where][age][gt]=17' + '&filter[where][associatedHF]=' + $window.sessionStorage.userId;
        } else {
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"age":{"gt":17}}]}}';
        }

        Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
            // $scope.members = mems;
            //console.log('$scope.members', $scope.members);

            $scope.members = [];

            angular.forEach(mems, function (member, index) {
                member.index = index;

                if (member.shgId == 0 || member.shgId == '0' || member.shgId == null || member.shgId == '') {
                    $scope.members.push(member);
                } else {
                    return;
                }
            });
        });

        $scope.shg = {
            meetingIntervals: 2,
            deleteFlag: false,
            formationDate: new Date(),
            shgAccountNumber: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        };

        //        $scope.Validate = function ($event) {
        //            Restangular.one('shgs/findOne?filter[where][groupName]=' + $scope.shg.groupName).get().then(function (grp) {
        //
        //                if (grp.groupName === '' || grp.groupName === null) {
        //                    return;
        //                } else {
        //                    alert($scope.SHGLanguage.alreadyGroupExists);
        //                    $scope.shg.groupName = '';
        //                }
        //            });
        //        };

        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            //{"membersInGroup":{"nin":[""]}},
        } else {
            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
            //{"membersInGroup":{"nin":[""]}},
        }

        $scope.duplicateSHG = false;

        $scope.Validate = function ($event, value) {

            value = value.toLowerCase();

            function search(nameKey, myArray) {
                for (var i = 0; i < myArray.length; i++) {
                    myArray[i].groupName = myArray[i].groupName.toLowerCase();
                    if (myArray[i].groupName === nameKey) {
                        return myArray[i];
                    }
                }
            }

            if (value == '' || value == null) {
                return;
            } else {
                Restangular.all($scope.shgFilterCall).getList().then(function (shgList) {
                    var resultObject = search(value, shgList);
                    if (resultObject == undefined) {
                        document.getElementById('name').style.borderColor = "";
                        $scope.duplicateSHG = false;
                    } else {
                        $scope.toggleValidation();
                        $scope.validatestring1 = $scope.SHGLanguage.existingShgNamePleaseChange;
                        $scope.shg.groupName = '';
                        document.getElementById('name').style.borderColor = "#FF0000";
                        $scope.duplicateSHG = true;
                    }
                });
            }
        };

        $('#name').keypress(function (evt) {
            if (/^[a-zA-Z 0-9 _]*$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $('#accno').keypress(function (evt) {
            if (/^[0-9]+$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $('#amount').keypress(function (evt) {
            if (/((\d+)((\.\d{1,2})?))$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $('#interest').keypress(function (evt) {
            if (/^[0-9.]+$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $scope.UserLanguage = $window.sessionStorage.language;

        $scope.displaymembers = [];
        $scope.grpmembers = [];

        if ($window.sessionStorage.previous == '/shg/household/create') {
            $scope.shg = JSON.parse($window.sessionStorage.ShgData);
            $scope.displaymembers = JSON.parse($window.sessionStorage.ShgMemberData);
            //  $scope.grpmembers = JSON.parse($window.sessionStorage.ShgGroupMemData);

            Restangular.all('meetingIntervals?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (meets) {
                $scope.meetingIntervals = meets;
                $scope.shg.meetingInterval = $scope.shg.meetingInterval;
            });

            if ($window.sessionStorage.shgMemberId != null) {
                Restangular.one('members', $window.sessionStorage.shgMemberId).get().then(function (customer) {
                    $scope.idvalue = customer.id;
                    $scope.displaymembers.push({
                        name: customer.name,
                        individualId: customer.individualId,
                        age: customer.age,
                        mobile: customer.mobile,
                        id: customer.id,
                        disableDelete: false
                    });
                    // $scope.grpmembers.push(customer.id);
                    $scope.selected = '';
                });

            }
        } else {
            $window.sessionStorage.shgMemberId = null;
            $window.sessionStorage.ShgData = null;
            $window.sessionStorage.ShgMemberData = null;
            $window.sessionStorage.ShgGroupMemData = null;

            $scope.shg = {
                meetingIntervals: 2,
                deleteFlag: false,
                formationDate: new Date(),
                shgAccountNumber: '',
                createdDate: new Date(),
                createdBy: $window.sessionStorage.userId,
                createdByRole: $window.sessionStorage.roleId,
                lastModifiedDate: new Date(),
                lastModifiedBy: $window.sessionStorage.userId,
                lastModifiedByRole: $window.sessionStorage.roleId,
                countryId: $window.sessionStorage.countryId,
                stateId: $window.sessionStorage.stateId,
                districtId: $window.sessionStorage.districtId,
                siteId: $window.sessionStorage.siteId.split(",")[0]
            };

            Restangular.all('meetingIntervals?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (meets) {
                $scope.meetingIntervals = meets;
                $scope.shg.meetingInterval = 2;
            });
        }

        $scope.onSelect = function ($item, $model, $label) {

            Restangular.one('members/findOne?filter[where][id]=' + $model.id + '&filter[where][age][gt]=17').get().then(function (customer) {
                $scope.idvalue = customer.id;
                $scope.displaymembers.push({
                    name: customer.name,
                    individualId: customer.individualId,
                    age: customer.age,
                    mobile: customer.mobile,
                    id: customer.id,
                    parentId: customer.parentId,
                    disableDelete: false
                });

                var a = $scope.displaymembers;

                var b = _.uniq(a, function (v) {
                    return v.id;
                })

                $scope.displaymembers = b;

                $scope.selected = '';
            });
            // console.log('$scope.displaymembers', $scope.displaymembers);
        };

        $scope.$watch('selected', function (newValue, oldValue) {
            // console.log("newValue", newValue);

            if (newValue === oldValue || newValue == '') {
                return;
            } else {

                //                Restangular.one('members/findOne?filter[where][name]=' + newValue + '&filter[where][age][gt]=17').get().then(function (customer) {
                //                    $scope.idvalue = customer.id;
                //                    $scope.displaymembers.push({
                //                        name: customer.name,
                //                        individualId: customer.individualId,
                //                        age: customer.age,
                //                        mobile: customer.mobile,
                //                        id: customer.id,
                //                        parentId: customer.parentId
                //                    });
                //
                //                    var a = $scope.displaymembers;
                //
                //                    var b = _.uniq(a, function (v) {
                //                        return v.id && v.parentId;
                //                    })
                //
                //                    $scope.displaymembers = b;
                //
                //                    $scope.selected = '';
                //                });
            }
        });

        $scope.StoreData = function () {
            $window.sessionStorage.shgMemberId = null;
            $window.sessionStorage.ShgData = angular.toJson($scope.shg);
            $window.sessionStorage.ShgMemberData = angular.toJson($scope.displaymembers);
            // $window.sessionStorage.ShgGroupMemData = angular.toJson($scope.grpmembers);
        };

        $scope.value = '';
        var timeout, delay = 1000;

        $scope.$watch('shg.amountToSave', function (newValue, oldValue) {
            if (timeout) {
                $timeout.cancel(timeout);
            }
            timeout = $timeout(function () {
                    if (newValue === oldValue || newValue == '') {
                        return;
                    } else {
                        if (newValue < 50) {
                            // alert($scope.SHGLanguage.amountShouldBe);
                            // $scope.shg.amountToSave = '';
                        } else {
                            return;
                        }
                    }
                },
                delay);
        });

        $(document).on('keyup', '#interest', function (event) {

            var input = event.currentTarget.value;

            if (input.search(/^0/) != -1) {
                $scope.shg.interestRate = '';
            }
        });

        $scope.Remove = function (index) {
            $scope.displaymembers.splice(index, 1);
            // $scope.grpmembers.splice(index, 1);
        };

        function filterByName(patients, typedValue) {
            var matches_name = -1;
            var matches_individualid = -1;
            var matches_phone = -1;
            return patients.filter(function (patient) {
                if (patient.name != null) {
                    matches_name = patient.name.toLowerCase().indexOf(typedValue.toLowerCase()) != -1;
                }
                if (patient.individualId != null) {
                    matches_individualid = patient.individualId.toLowerCase().indexOf(typedValue.toLowerCase()) != -1;
                }
                if (patient.mobile != null) {
                    matches_phone = patient.mobile.indexOf(typedValue) != -1;
                }

                return matches_name || matches_individualid || matches_phone;
            });
        }
        $scope.filterByName = filterByName;

        $scope.validatestring = '';

        $scope.memeberupdate = {};
        $scope.shjObj = {};
        $scope.confirmSaveSHG = false;

        Restangular.one('users', $window.sessionStorage.userId).get().then(function (usr) {
            $scope.shg.countryId = usr.countryId;
            $scope.shg.stateId = usr.stateId;
            $scope.shg.districtId = usr.districtId;
            $scope.shg.siteId = usr.siteId.split(",")[0];
        });


        $scope.CancleButoon = function () {

            window.location = '/shg-list';
        };



        /*************************************/
        $scope.Save = function (clicked) {

            for (var i = 0; i < $scope.displaymembers.length; i++) {
                $scope.grpmembers.push($scope.displaymembers[i].id);
            }

            var c = $scope.grpmembers;

            var d = _.uniq(c, function (w) {
                return w;
            })

            $scope.grpmembers = d;

            $scope.shg.membersInGroup = $scope.grpmembers.toString();
            // console.log('$scope.shg', $scope.shg);

            document.getElementById('name').style.border = "";
            // document.getElementById('date').style.border = "";
            document.getElementById('amount').style.border = "";
            document.getElementById('interest').style.border = "";
            document.getElementById('accno').style.border = "";

            if ($scope.shg.groupName == '' || $scope.shg.groupName == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.enterNameOfGroup;
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.duplicateSHG == true) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.existingShgNamePleaseChange;
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.shg.formationDate == '' || $scope.shg.formationDate == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.selectDateOfFormation;
                // document.getElementById('date').style.borderColor = "#FF0000";

            } else if ($scope.shg.associatedHF == '' || $scope.shg.associatedHF == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.selectAssociateHF;

            } else if ($scope.shg.meetingInterval == '' || $scope.shg.meetingInterval == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.selectMeetingInterval;

            } else if ($scope.shg.amountToSave == '' || $scope.shg.amountToSave == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.enterAmount;
                document.getElementById('amount').style.borderColor = "#FF0000";

            } else if ($scope.shg.interestRate == '' || $scope.shg.interestRate == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.enterInterestRate;
                document.getElementById('interest').style.borderColor = "#FF0000";

            } 
            /**else if ($scope.displaymembers.length == 0) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.atleastOneMember;
            }**/
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.shjObj.displayDateOfFormation = $filter('date')($scope.shg.formationDate, 'dd-MMM-yyyy');
                $scope.shjObj.displayNameOfGroup = $scope.shg.groupName;
                $scope.shjObj.displayAmountToBeSave = $scope.shg.amountToSave;
                $scope.shjObj.displayInterest = $scope.shg.interestRate;
                $scope.confirmSaveSHG = true;
                if ($window.sessionStorage.language == 1) {

                    Restangular.one('users', $scope.shg.associatedHF).get().then(function (udr) {
                        Restangular.one('meetingintervals', $scope.shg.meetingInterval).get().then(function (vdr) {

                            $scope.shjObj.displayAssociateHF = udr.name;
                            $scope.shjObj.displayMeetingInterval = vdr.name;
                        });
                    });


                } else {

                    Restangular.one('users', $scope.shg.associatedHF).get().then(function (udr) {
                        Restangular.one('meetingintervals/findOne?filter[where][parentId]=' + $scope.shg.meetingInterval + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (vdr) {

                            $scope.shjObj.displayAssociateHF = udr.name;
                            $scope.shjObj.displayMeetingInterval = vdr.name;
                        });
                    });
                }
            }
        };

        /*********************** Final Save After confrimation ***************/
        $scope.SaveShgConfirm = function () {

            $scope.toggleLoading();
            $scope.message = $scope.SHGLanguage.thankYouCreated;
            //$scope.submitDisable = true;
            Restangular.all('shgs').post($scope.shg).then(function (znResponse) {
                //  console.log('znResponse', znResponse);
                $scope.updateHouseholdData(znResponse.id);
            });
        };


        /*********************** Final Save After confrimation ***************/

        $scope.memberCount = 0;

        $scope.updateHouseholdData = function (shgId) {

            if ($scope.memberCount < $scope.displaymembers.length) {
                $scope.memeberupdate.shgId = shgId;
                Restangular.one('members', $scope.displaymembers[$scope.memberCount].id).customPUT($scope.memeberupdate).then(function (memResp) {
                    Restangular.one('households', memResp.parentId).get().then(function (hsehld) {
                        $scope.shgbelongs = hsehld.ssgThisHHBelongs;
                        if ($scope.shgbelongs === null || $scope.shgbelongs === 'null') {
                            $scope.shgbelongs = "";
                        }
                        $scope.householdsData = {
                            id: hsehld.id,
                            ssgThisHHBelongs: ""
                        }

                        if ($scope.shgbelongs === "") {
                            $scope.householdsData.ssgThisHHBelongs = shgId;
                        } else {
                            $scope.householdsData.ssgThisHHBelongs = $scope.shgbelongs + "," + shgId;

                            var array = $scope.householdsData.ssgThisHHBelongs.split(",");

                            array.sort(function (a, b) {
                                return a - b
                            });

                            var uniqueArray = [array[0]];

                            for (var i = 1; i < array.length; i++) {
                                if (array[i - 1] !== array[i])
                                    uniqueArray.push(array[i]);
                            }

                            $scope.householdsData.ssgThisHHBelongs = uniqueArray.toString();

                        }
                        Restangular.one('households', hsehld.id).customPUT($scope.householdsData).then(function (memResp) {
                            $scope.memberCount++
                                if ($scope.memberCount < $scope.displaymembers.length) {
                                    $scope.updateHouseholdData(shgId);
                                } else {
                                    $scope.modalInstanceLoad.close();
                                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                    console.log('reloading...');

                                    setInterval(function () {
                                        window.location = '/shg-list';
                                    }, 350);
                                }
                        });
                    });
                });
            } else {
                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setInterval(function () {
                    window.location = '/shg-list';
                }, 1500);
            }
        }


        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        // $scope.message = 'SHG has been Created!';

        $scope.Delete = function (id, membersInGroup) {
            $scope.item = [{
                deleteFlag: true
            }]
            if (membersInGroup != 0) {
                alert($scope.SHGLanguage.shgHavingMembers);
            } else {
                Restangular.one('shgs/' + id).customPUT($scope.item[0]).then(function () {
                    $route.reload();
                });
            }
        };

    });
