'use strict';

angular.module('secondarySalesApp')
    .controller('InterventionsEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {

        $scope.SaveBtn = false;
        $scope.UpdateBtn = true;

        $scope.InterventionLanguage = {};

        Restangular.one('interventionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.InterventionLanguage = langResponse[0];
            $scope.message = langResponse[0].interventionHasBeenUpdated;
            $scope.interventionHeading = langResponse[0].editIntervention;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.hideAssigned = true;

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.intervention.associatedHF = $window.sessionStorage.userId;

            });
        } else {

            $scope.hideAssigned = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.intervention.associatedHF = $scope.intervention.associatedHF;

            });

        }

        $scope.maleAdd = function () {
            $scope.intervention.maleMemberCovered = parseInt($scope.intervention.maleMemberCovered) + 1;
        };

        $scope.maleMinus = function () {
            if ($scope.intervention.maleMemberCovered == 0) {
                $scope.intervention.maleMemberCovered = $scope.intervention.maleMemberCovered;
            } else {
                $scope.intervention.maleMemberCovered = parseInt($scope.intervention.maleMemberCovered) - 1;
            }
        };

        $scope.femaleAdd = function () {
            $scope.intervention.femaleMemberCovered = parseInt($scope.intervention.femaleMemberCovered) + 1;
        };

        $scope.femaleMinus = function () {
            if ($scope.intervention.femaleMemberCovered == 0) {
                $scope.intervention.femaleMemberCovered = $scope.intervention.femaleMemberCovered;
            } else {
                $scope.intervention.femaleMemberCovered = parseInt($scope.intervention.femaleMemberCovered) - 1;
            }
        };

        Restangular.all('venues?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (vne) {
            $scope.venues = vne;
            $scope.intervention.venue = $scope.intervention.venue;
        });

        Restangular.all('typeofinterventions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (typ) {
            $scope.types = typ;
            $scope.intervention.typeOfIntervention = $scope.intervention.typeOfIntervention;
        });

        if ($routeParams.id) {
            Restangular.one('interventions', $routeParams.id).get().then(function (intrvn) {
                $scope.original = intrvn;
                $scope.intervention = Restangular.copy($scope.original);

//                if ($scope.intervention.maleMemberCovered == null) {
//                    $scope.intervention.maleMemberCovered = 0;
//                }
//
//                if ($scope.intervention.femaleMemberCovered == null) {
//                    $scope.intervention.femaleMemberCovered = 0;
//                }
            });
        }

        $scope.validatestring = '';

        $scope.interventionModel = false;
    
        $scope.popupObj = {};

        $scope.validatestring = '';

        $scope.Confirm = function () {

            document.getElementById('male').style.border = "";
            document.getElementById('female').style.border = "";

            if ($scope.intervention.date == '' || $scope.intervention.date == null) {
                $scope.validatestring = $scope.validatestring + $scope.InterventionLanguage.pleaseSelectDate;

            } else if ($scope.intervention.associatedHF == '' || $scope.intervention.associatedHF == null) {
                $scope.validatestring = $scope.validatestring + $scope.InterventionLanguage.pleaseSelectAssignedTo;

            } else if ($scope.intervention.venue == '' || $scope.intervention.venue == null) {
                $scope.validatestring = $scope.validatestring + $scope.InterventionLanguage.pleaseSelectVenue;

            } else if ($scope.intervention.typeOfIntervention == '' || $scope.intervention.typeOfIntervention == null) {
                $scope.validatestring = $scope.validatestring + $scope.InterventionLanguage.pleaseSelectType;

            } else if ($scope.intervention.maleMemberCovered === '' || $scope.intervention.maleMemberCovered == null) {
                $scope.validatestring = $scope.validatestring + $scope.InterventionLanguage.pleaseSelectnoOfMaleMembersCovered;
                document.getElementById('male').style.borderColor = "#FF0000";

            } else if ($scope.intervention.femaleMemberCovered === '' || $scope.intervention.femaleMemberCovered == null) {
                $scope.validatestring = $scope.validatestring + $scope.InterventionLanguage.pleaseSelectnoOfFemaleMembersCovered;
                document.getElementById('female').style.borderColor = "#FF0000";

            } else if ($scope.intervention.maleMemberCovered == '0' && $scope.intervention.femaleMemberCovered == '0') {
                $scope.validatestring = $scope.validatestring + 'Atleast male or female has to be a positive integer';
                //document.getElementById('female').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                
                $scope.popupObj.date = $filter('date')($scope.intervention.date, 'dd-MMM-yyyy');
                $scope.popupObj.maleMemsCovered = $scope.intervention.maleMemberCovered;
                $scope.popupObj.femaleMemsCovered = $scope.intervention.femaleMemberCovered;
                $scope.interventionModel = true;
                
                if ($window.sessionStorage.language == 1) {

                    Restangular.one('users', $scope.intervention.associatedHF).get().then(function (udr) {
                        Restangular.one('venues', $scope.intervention.venue).get().then(function (vdr) {
                            Restangular.one('typeofinterventions', $scope.intervention.typeOfIntervention).get().then(function (tdr) {
                                $scope.popupObj.assignedTo = udr.name;
                                $scope.popupObj.venueName = vdr.name;
                                $scope.popupObj.typeName = tdr.name;
                            });
                        });
                    });
                    
                } else {

                    Restangular.one('users', $scope.intervention.associatedHF).get().then(function (udr) {
                        Restangular.one('venues/findOne?filter[where][parentId]=' + $scope.intervention.venue + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (vdr) {
                            Restangular.one('typeofinterventions/findOne?filter[where][parentId]=' + $scope.intervention.typeOfIntervention + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (tdr) {
                                $scope.popupObj.assignedTo = udr.name;
                                $scope.popupObj.venueName = vdr.name;
                                $scope.popupObj.typeName = tdr.name;
                            });
                        });
                    });
                }
            }
        };

        $scope.Save = function () {

            $scope.toggleLoading();

            Restangular.one('interventions', $routeParams.id).customPUT($scope.intervention).then(function (resp) {

                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setInterval(function () {
                    window.location = '/interventions-list';
                }, 1500);
            });
        };

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start
        var sevendays = new Date();

        sevendays.setDate(sevendays.getDate() + 7);

        $scope.today = function () {};

        $scope.today();

        $scope.showWeeks = true;

        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dtmin = new Date();

        $scope.dtmax = new Date();

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };

        $scope.toggleMin();

        $scope.picker = {};

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end
    });