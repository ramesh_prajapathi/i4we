'use strict';

angular.module('secondarySalesApp')
    .controller('VitalRecordCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 9) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/vitalrecord/create' || $location.path() === '/vitalrecord/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/vitalrecord/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/vitalrecord/create' || $location.path() === '/vitalrecord/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/vitalrecord/create' || $location.path() === '/vitalrecord/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/vitalrecord-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.showenglishLang = true;
        $scope.OtherLang = true;

        $scope.$watch('vitalrecord.name', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id) {
                return;
            } else {
                angular.forEach($scope.rowArray, function (member, index) {
                    member.index = index;
                    member.name = newValue;
                });
            }
        });

        $scope.$watch('vitalrecord.language', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });

        $scope.$watch('vitalrecord.parentId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.one('vitalrecords', newValue).get().then(function (sts) {
                    $scope.vitalrecord.orderNo = sts.orderNo;
                    $scope.vitalrecord.lowerBound = sts.lowerBound;
                    $scope.vitalrecord.upperBound = sts.upperBound;
                    $scope.vitalrecord.mandatoryFlag = sts.mandatoryFlag;
                    $scope.vitalrecord.enabled = sts.enabled;
                });
            }
        });
        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {

            $scope.message = 'Vital Record has been Updated!';

            Restangular.one('vitalrecords', $routeParams.id).get().then(function (vitalrecord) {
                $scope.original = vitalrecord;
                $scope.vitalrecord = Restangular.copy($scope.original);

            });

            Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablangs) {
                $scope.tablanguages1 = tablangs;
                $scope.tablanguages1.splice(0, 1);
                $scope.currValue = 0;

                Restangular.all('vitalrecords?filter[where][deleteFlag]=false&filter[where][parentId]=' + $routeParams.id + '&filter[where][parentFlag]=false').getList().then(function (intvls1) {
                    $scope.mtlangs1 = intvls1;

                    angular.forEach($scope.tablanguages1, function (member, index) {
                        member.index = index;

                        var data = $scope.mtlangs1.filter(function (arr) {
                            return arr.language == member.id
                        })[0];

                        if (data == undefined) {
                            $scope.rowArray.push({
                                language: member.id,
                                id: 0
                            });
                        } else {
                            $scope.rowArray.push(data);
                        }
                    });
                });
            });

        } else {
            $scope.message = 'Vital Record has been Created!';
        }

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];

        /******************************** INDEX *******************************************/

        Restangular.all('vitalrecords?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (vital) {
            $scope.vitalrecords = vital;
            angular.forEach($scope.vitalrecords, function (member, index) {
                member.index = index + 1;

                Restangular.one('languages', member.language).get().then(function (lng) {
                    member.langname = lng.name;

                    if (member.enabled == false) {
                        member.enabledname = 'Vital Record';
                    } else if (member.enabled == true) {
                        member.enabledname = 'Non Systematic Record';
                    }
                });
            });
        });

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (sev) {
            $scope.vrlanguages = sev;
        });

        Restangular.all('vitalrecords?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.englishseverityofincidents = zn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        $scope.rowArray = [];

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablang) {
            $scope.tablanguages = tablang;
            $scope.tablanguages.splice(0, 1);
            $scope.currValue = 0;

            angular.forEach($scope.tablanguages, function (member, index) {
                member.index = index;

                if (!$routeParams.id) {
                    $scope.rowArray.push({
                        language: member.id,
                        name: ''
                    });
                }

                if (member.index == 0) {
                    member.active = true;
                    member.visible = true;
                } else {
                    member.active = false;
                    member.visible = false;
                }
            });
        });

        $scope.tabClick = function (row) {
            $scope.tablanguages[row.index].visible = true;
            if (row.index != 0) {
                $scope.tablanguages[0].visible = false;
            }
            $scope.currValue = row.index;
        };

/********************************************* SAVE *******************************************/

        $scope.vitalrecord = {
            "name": '',
            "language": 1,
            "deleteFlag": false,
            "mandatoryFlag": false,
            "enabled": false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {

            document.getElementById('name').style.border = "";
            document.getElementById('order').style.border = "";
            document.getElementById('lowerbound').style.border = "";
            document.getElementById('upperbound').style.border = "";

            if ($scope.vitalrecord.name == '' || $scope.vitalrecord.name == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.vitalrecord.orderNo == '' || $scope.vitalrecord.orderNo == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Order No';
                document.getElementById('order').style.borderColor = "#FF0000";

            } else if ($scope.vitalrecord.lowerBound == '' || $scope.vitalrecord.lowerBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Lower Bound';
                document.getElementById('lowerbound').style.borderColor = "#FF0000";

            } else if ($scope.vitalrecord.upperBound == '' || $scope.vitalrecord.upperBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Upper Bound';
                document.getElementById('upperbound').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.vitalrecord.parentId === '') {
                    delete $scope.vitalrecord['parentId'];
                }

                $scope.submitDisable = true;
                $scope.vitalrecord.parentFlag = $scope.showenglishLang;

                Restangular.all('vitalrecords').post($scope.vitalrecord).then(function (response) {
                    $scope.langFunc(response.id, response.orderNo, response.lowerBound, response.upperBound, response.mandatoryFlag, response.enabled);
                }, function (error) {
                    if (error.data.error.constraint === 'vitalrecord_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.langcount = 0;

        $scope.langFunc = function (intervalId, orderNo, lowerBound, upperBound, mandatoryFlag, enabled) {

            if ($scope.langcount < $scope.rowArray.length) {

                $scope.rowArray[$scope.langcount].parentFlag = false;
                $scope.rowArray[$scope.langcount].parentId = intervalId;
                $scope.rowArray[$scope.langcount].orderNo = orderNo;
                $scope.rowArray[$scope.langcount].lowerBound = lowerBound;
                $scope.rowArray[$scope.langcount].upperBound = upperBound;
                $scope.rowArray[$scope.langcount].mandatoryFlag = mandatoryFlag;
                $scope.rowArray[$scope.langcount].enabled = enabled;
                $scope.rowArray[$scope.langcount].deleteFlag = false;
                $scope.rowArray[$scope.langcount].createdDate = new Date();
                $scope.rowArray[$scope.langcount].createdBy = $window.sessionStorage.userId;
                $scope.rowArray[$scope.langcount].createdByRole = $window.sessionStorage.roleId;
                $scope.rowArray[$scope.langcount].lastModifiedDate = new Date();
                $scope.rowArray[$scope.langcount].lastModifiedBy = $window.sessionStorage.userId;
                $scope.rowArray[$scope.langcount].lastModifiedByRole = $window.sessionStorage.roleId;

                Restangular.all('vitalrecords').post($scope.rowArray[$scope.langcount]).then(function () {
                    $scope.langcount++;
                    $scope.langFunc(intervalId, orderNo, lowerBound, upperBound, mandatoryFlag, enabled);
                });

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/vitalrecord-list';
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


        /***************************************************** UPDATE *******************************************/

        $scope.Update = function () {

            document.getElementById('name').style.border = "";
            document.getElementById('order').style.border = "";
            document.getElementById('lowerbound').style.border = "";
            document.getElementById('upperbound').style.border = "";

            if ($scope.vitalrecord.name == '' || $scope.vitalrecord.name == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.vitalrecord.orderNo == '' || $scope.vitalrecord.orderNo == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Order No';
                document.getElementById('order').style.borderColor = "#FF0000";

            } else if ($scope.vitalrecord.lowerBound == '' || $scope.vitalrecord.lowerBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Lower Bound';
                document.getElementById('lowerbound').style.borderColor = "#FF0000";

            } else if ($scope.vitalrecord.upperBound == '' || $scope.vitalrecord.upperBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Upper Bound';
                document.getElementById('upperbound').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.vitalrecord.parentId === '') {
                    delete $scope.vitalrecord['parentId'];
                }

                $scope.submitDisable = true;

                Restangular.one('vitalrecords', $routeParams.id).customPUT($scope.vitalrecord).then(function (resp) {
                    $scope.langUpdateFunc(resp.id, resp.orderNo, resp.lowerBound, resp.upperBound, resp.mandatoryFlag, resp.enabled);
                }, function (error) {
                    if (error.data.error.constraint === 'vitalrecord_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        $scope.langUpdatecount = 0;

        $scope.langUpdateFunc = function (intervalId, orderNo, lowerBound, upperBound, mandatoryFlag, enabled) {

            if ($scope.langUpdatecount < $scope.rowArray.length) {

                if ($scope.rowArray[$scope.langUpdatecount].id == 0 || $scope.rowArray[$scope.langUpdatecount].id == "0") {
                    $scope.rowArray[$scope.langUpdatecount].parentFlag = false;
                    $scope.rowArray[$scope.langUpdatecount].parentId = intervalId;
                    $scope.rowArray[$scope.langUpdatecount].orderNo = orderNo;
                    $scope.rowArray[$scope.langUpdatecount].lowerBound = lowerBound;
                    $scope.rowArray[$scope.langUpdatecount].upperBound = upperBound;
                    $scope.rowArray[$scope.langUpdatecount].mandatoryFlag = mandatoryFlag;
                    $scope.rowArray[$scope.langUpdatecount].enabled = enabled;
                    $scope.rowArray[$scope.langUpdatecount].deleteFlag = false;
                    $scope.rowArray[$scope.langUpdatecount].createdDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].createdBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].createdByRole = $window.sessionStorage.roleId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedByRole = $window.sessionStorage.roleId;
                    delete $scope.rowArray[$scope.langUpdatecount]['id'];

                    Restangular.all('vitalrecords').post($scope.rowArray[$scope.langUpdatecount]).then(function () {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId, orderNo, lowerBound, upperBound, mandatoryFlag, enabled);
                    });

                } else {

                    $scope.rowArray[$scope.langUpdatecount].orderNo = orderNo;
                    $scope.rowArray[$scope.langUpdatecount].lowerBound = lowerBound;
                    $scope.rowArray[$scope.langUpdatecount].upperBound = upperBound;
                    $scope.rowArray[$scope.langUpdatecount].mandatoryFlag = mandatoryFlag;
                    $scope.rowArray[$scope.langUpdatecount].enabled = enabled;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedByRole = $window.sessionStorage.roleId;

                    Restangular.one('vitalrecords', $scope.rowArray[$scope.langUpdatecount].id).customPUT($scope.rowArray[$scope.langUpdatecount]).then(function (resp1) {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId, orderNo, lowerBound, upperBound, mandatoryFlag, enabled);
                    });
                }

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/vitalrecord-list';
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

/******************************************************** DELETE *******************************************/
        
        $scope.Delete = function (id) {
            
            $scope.item = [{
                deleteFlag: true
            }]

            Restangular.one('vitalrecords/' + id).customPUT($scope.item[0]).then(function () {
                Restangular.all('vitalrecords?filter[where][parentId]=' + id).getList().then(function (stypes) {
                    $scope.deleteRows = stypes;
                    $scope.DeleteFunc();
                });
            });
        };

        $scope.updateFlag = {
            deleteFlag: true
        };

        $scope.DeleteCount = 0;

        $scope.DeleteFunc = function (id) {

            if ($scope.DeleteCount < $scope.deleteRows.length) {

                Restangular.one('vitalrecords/' + $scope.deleteRows[$scope.DeleteCount].id).customPUT($scope.updateFlag).then(function () {
                    $scope.DeleteCount++;
                    $scope.DeleteFunc();
                });

            } else {
                $route.reload();
            }
        };

    });
