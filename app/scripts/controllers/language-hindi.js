'use strict';

angular.module('secondarySalesApp')
	.controller('HindiCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
		$scope.isCreateSave = true;
		$scope.heading = 'Multi-Language';

		$scope.multilanguage = {
			languageId: 2
		};

		$scope.submitmultilanguages = Restangular.all('multilanguages').getList().$object;

		$scope.SaveLanguage = function () {
			$scope.submitmultilanguages.customPUT($scope.multilanguage).then(function (response) {
				console.log('Response', response);
				window.location = '/';
			});
		};

		Restangular.one('multilanguages', 2).get().then(function (zone) {
			$scope.original = zone;
			$scope.multilanguage = Restangular.copy($scope.original);
			$scope.modalInstanceLoad.close();
		});

		$scope.LanguageName = 'हिंदी';
		$scope.Language = 'भाषा';
	});
