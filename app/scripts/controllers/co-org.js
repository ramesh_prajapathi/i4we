'use strict';

angular.module('secondarySalesApp')
	.controller('COgrCtrl', function ($scope, Restangular, $route, $window) {
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.countryId = $window.sessionStorage.myRoute;
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};

		if ($window.sessionStorage.prviousLocation != "partials/co-org-form") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
			$scope.currentpage = 1;
			$scope.pageSize = 25;
		}
		/***********************************************************************************/
		$scope.countryId = null;
		$scope.znes = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
			$scope.zones = znes;
			$scope.countryId = $window.sessionStorage.myRoute;
		});

		$scope.getFacilityName = function (id) {
			return Restangular.one('employees', id).get().$object;
		};

		$scope.searchPart = $scope.firstName;
		$scope.partners = {};
		$scope.countryId = '';
		$scope.zonalid = '';
		$scope.$watch('countryId', function (newValue, oldValue) {
			if (newValue === oldValue) {
				return;
			} else {
				$window.sessionStorage.myRoute = newValue;
				$scope.countryId = $window.sessionStorage.myRoute;
				$scope.sal = Restangular.all('comembers?filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (sal) {
					$scope.partners = sal;
					angular.forEach($scope.partners, function (member, index) {
						member.index = index + 1;
					});
				});
			}
		});

		/*********************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('comembers/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}


	});
