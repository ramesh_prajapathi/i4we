'use strict';

angular.module('secondarySalesApp')
	.controller('FWEditProfileCtrl', function ($scope, Restangular, $routeParams, $window, $modal) {
		$scope.isCreateView = false;
		$scope.heading = 'Field Worker Edit';
		$scope.HideUsername = true;
		$scope.tabtwohide = true;

		$scope.employees = Restangular.all('employees').getList().$object;
		$scope.partners = Restangular.all('fieldworkers').getList().$object;
		$scope.submitpartners = Restangular.all('fieldworkers').getList().$object;
		$scope.retailers = Restangular.all('retailers').getList().$object;
		$scope.distributionRoutes = Restangular.all('distribution-routes').getList().$object;
		//  $scope.groups = Restangular.all('groups').getList().$object;
		$scope.cities = Restangular.all('cities').getList().$object;
		$scope.states = Restangular.all('states').getList().$object;
		$scope.ims = Restangular.all('ims').getList().$object;
		$scope.retailercategories = Restangular.all('retailercategories').getList().$object;

		$scope.zone = Restangular.all('zones').getList().$object;
		$scope.salesareas = Restangular.all('sales-areas').getList().$object;
		$scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
		$scope.distributionSubAreas = Restangular.all('distribution-subareas').getList().$object;
		$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;
		$scope.submitusers = Restangular.all('users').getList().$object;



		$scope.partner = {};
		$scope.groups = Restangular.one('groups', 9).get().then(function (group) {
			$scope.groupname = group.name;
			$scope.partner.groupId = group.id;
		});


		$scope.fieldworker = {
			address: '',
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			facility: $window.sessionStorage.coorgId,
			deleteflag: false
		};
		
				$scope.user = {
					flag: true,
					lastmodifiedtime: new Date(),
					lastmodifiedby: $window.sessionStorage.UserEmployeeId,
					deleteflag: false,
					roleId: 6,
					status: 'active',
					salesAreaId: $window.sessionStorage.salesAreaId,
					coorgId: $window.sessionStorage.coorgId,
					zoneId: $window.sessionStorage.zoneId
				};
			

		Restangular.all('fieldworkers?filter[where][deleteflag]=null' + '&filter[where][facilityId]=' + $window.sessionStorage.coorgId + '&filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId).getList().then(function (partners) {

			$scope.partners = partners;
			Restangular.one('fieldworkers', $routeParams.id).get().then(function (partner) {
				$scope.original = partner;
				$scope.fieldworker = Restangular.copy($scope.original);

				Restangular.one('zones', partner.state).get().then(function (zone) {
					$scope.zoneName = zone.name;
				});

				Restangular.one('sales-areas', partner.district).get().then(function (salesarea) {
					$scope.salesareaName = salesarea.name;
				});

				Restangular.one('comembers', partner.facilityId).get().then(function (partner) {
					$scope.coorgName = partner.name;
					$scope.fieldworker.facilityId = partner.id;
					$scope.auditlog.facilityId = partner.id;
				});
				$scope.modalInstanceLoad.close();
			});

		});

		/*********************************************** Update *******************************************/
		$scope.auditlog = {
			description: 'Field Worker Update',
			modifiedbyroleid: $window.sessionStorage.roleId,
			modifiedby: $window.sessionStorage.UserEmployeeId,
			lastmodifiedtime: new Date(),
			entityroleid: 51,
			state: $window.sessionStorage.zoneId,
			district: $window.sessionStorage.salesAreaId,
			facility: $window.sessionStorage.coorgId,
			lastmodifiedby: $window.sessionStorage.UserEmployeeId
		};

		$scope.validatestring = '';
		$scope.UpdateComember = {
			fwcount: 0
		};
		console.log('$window.sessionStorage.UserEmployeeId', $window.sessionStorage.UserEmployeeId);
		$scope.dataModal = false;

		Restangular.all('users?filter[where][employeeid]=' + $routeParams.id).getList().then(function (submituser) {
			$scope.getUserId = submituser[0].id;
			console.log('submituser',submituser);
			console.log('$scope.getUserId', $scope.getUserId);
		});
		$scope.Update = function () {
			$scope.dataModal = !$scope.dataModal;
			//document.getElementById('firstname').style.border = "";
			//document.getElementById('lastname').style.border = "";
			//document.getElementById('mobile').style.border = "";

			if ($scope.fieldworker.firstname == '' || $scope.fieldworker.firstname == null) {
				//$scope.fieldworker.firstname = null;
				$scope.validatestring = $scope.validatestring + 'Plese Enter First Name';
				//document.getElementById('firstname').style.border = "1px solid #ff0000";

			} else if ($scope.fieldworker.lastname == '' || $scope.fieldworker.lastname == null) {
				//$scope.fieldworker.lastname = null;
				$scope.validatestring = $scope.validatestring + 'Plese Enter Last Name';
				//document.getElementById('lastname').style.border = "1px solid #ff0000";

			} else if ($scope.fieldworker.mobile == '' || $scope.fieldworker.mobile == null) {
				//$scope.fieldworker.mobile = null;
				$scope.validatestring = $scope.validatestring + 'Plese Enter Mobile Number';
				//document.getElementById('mobile').style.border = "1px solid #ff0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
				//alert($scope.validatestring);
				//$scope.validatestring = '';
				$scope.dataModal = !$scope.dataModal;
			} else {
				$scope.submitpartners.customPUT($scope.fieldworker).then(function (submitfw) {
					$scope.auditlog.entityid = submitfw.id;
					/*
					$scope.user = {
						flag: true,
						lastmodifiedtime: new Date(),
						lastmodifiedby: $window.sessionStorage.UserEmployeeId,
						deleteflag: false,
						roleId: 6,
						status: 'active',
						salesAreaId: $window.sessionStorage.salesAreaId,
						coorgId: $window.sessionStorage.coorgId,
						zoneId: $window.sessionStorage.zoneId,
						username: submitfw.firstname,
						mobile: submitfw.mobile,
						employeeid: submitfw.id,
						email: submitfw.email
					};*/

					
					$scope.user.username = submitfw.firstname;
					$scope.user.mobile = submitfw.mobile;
					$scope.user.employeeid = submitfw.id;
					$scope.user.email = submitfw.email;
					

					$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
						console.log('responseaudit', responseaudit);
						console.log('$scope.partner', $scope.fieldworker);
						$scope.UpdateComember.fwcount = +($scope.fieldworker.fwcode) + 1;


						Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).customPUT($scope.UpdateComember).then(function (comember) {
							//window.location = '/field-work';
						});
						/*
						Restangular.one('users?filter[where][employeeid]=' + $routeParams.id).get().then(function (submituser) {
							$scope.getUserId = submituser;
							console.log('$scope.getUserId', $scope.getUserId);
						*/	
							
							Restangular.one('users/' + $scope.getUserId).customPUT($scope.user).then(function (submituser) {
								console.log('submituser', submituser);
							//});
						});
					});
				});
			}
			$scope.OK = function () {
				$scope.dataModal = !$scope.dataModal;
				console.log('Update');
				window.location = '/';
			};
		};

		console.log('$routeParams.id', $routeParams.id);
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};



		/* $scope.Update = function () {
		     $scope.submitpartners.customPUT($scope.fieldworker).then(function () {
		         console.log('$scope.partner', $scope.fieldworker);
		         window.location = '/field-work';
		     });
		 };*/

		$scope.partner = {
			address: ''
		}


		///////////////////////////////////////////////////////MAP//////////////////////////

		$scope.showMapModal = false;
		$scope.toggleMapModal = function () {
			$scope.mapcount = 0;

			var geocoder = new google.maps.Geocoder();

			function geocodePosition(pos) {
				geocoder.geocode({
					latLng: pos
				}, function (responses) {
					if (responses && responses.length > 0) {
						updateMarkerAddress(responses[0].formatted_address);
					} else {
						updateMarkerAddress('Cannot determine address at this location.');
					}
				});
			}

			function updateMarkerStatus(str) {
				document.getElementById('markerStatus').innerHTML = str;
			}

			function updateMarkerPosition(latLng) {
				$scope.fieldworker.latitude = latLng.lat() + ',' + latLng.lng();
				$scope.fieldworker.longitude = latLng.lng();

				document.getElementById('info').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');
			}

			function updateMarkerAddress(str) {
				document.getElementById('mapaddress').innerHTML = str;
			}
			var map;

			function initialize() {

				$scope.address = $scope.fieldworker.address;
				// console.log('$scope.address', $scope.address);
				$scope.latitude = 21.0000;
				$scope.longitude = 78.0000;
				if ($scope.address.length > 0) {
					var addressgeocoder = new google.maps.Geocoder();
					addressgeocoder.geocode({
						'address': $scope.address
					}, function (results, status) {

						if (status == google.maps.GeocoderStatus.OK) {
							$scope.latitude = parseInt(results[0].geometry.location.lat());
							$scope.longitude = parseInt(results[0].geometry.location.lng());
							//console.log($scope.latitude, $scope.longitude);

							var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
							map = new google.maps.Map(document.getElementById('mapCanvas'), {
								zoom: 4,
								center: new google.maps.LatLng($scope.latitude, $scope.longitude),
								mapTypeId: google.maps.MapTypeId.ROADMAP
							});
							var marker = new google.maps.Marker({
								position: latLng,
								title: 'Point A',
								map: map,
								draggable: true
							});

							// Update current position info.
							updateMarkerPosition(latLng);
							geocodePosition(latLng);

							// Add dragging event listeners.
							google.maps.event.addListener(marker, 'dragstart', function () {
								updateMarkerAddress('Dragging...');
							});

							google.maps.event.addListener(marker, 'drag', function () {
								updateMarkerStatus('Dragging...');
								updateMarkerPosition(marker.getPosition());
							});

							google.maps.event.addListener(marker, 'dragend', function () {
								updateMarkerStatus('Drag ended');
								geocodePosition(marker.getPosition());
							});
						}
					});
				} else {
					$scope.latitude = 21.0000;
					$scope.longitude = 78.0000;

					var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
					map = new google.maps.Map(document.getElementById('mapCanvas'), {
						zoom: 4,
						center: new google.maps.LatLng($scope.latitude, $scope.longitude),
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					var marker = new google.maps.Marker({
						position: latLng,
						title: 'Point A',
						map: map,
						draggable: true
					});

					// Update current position info.
					updateMarkerPosition(latLng);
					geocodePosition(latLng);

					// Add dragging event listeners.
					google.maps.event.addListener(marker, 'dragstart', function () {
						updateMarkerAddress('Dragging...');
					});

					google.maps.event.addListener(marker, 'drag', function () {
						updateMarkerStatus('Dragging...');
						updateMarkerPosition(marker.getPosition());
					});

					google.maps.event.addListener(marker, 'dragend', function () {
						updateMarkerStatus('Drag ended');
						geocodePosition(marker.getPosition());
					});

				}


			}

			// Onload handler to fire off the app.
			//google.maps.event.addDomListener(window, 'load', initialize);
			initialize();

			window.setTimeout(function () {
				google.maps.event.trigger(map, 'resize');
				map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
				map.setZoom(4);
			}, 1000);


			$scope.SaveMap = function () {
				$scope.showMapModal = !$scope.showMapModal;
				console.log($scope.reportincident);
			};

			//console.log('fdfd');
			$scope.showMapModal = !$scope.showMapModal;
		};


		$scope.CancelMap = function () {
			if ($scope.mapcount == 0) {
				$scope.showMapModal = !$scope.showMapModal;
				$scope.mapcount++;
			}
		};



		$scope.openResetPswd = function () {
			$scope.modalPswd = $modal.open({
				animation: true,
				templateUrl: 'template/resetpassword.html',
				scope: $scope,
				backdrop: 'static',
				size: 'lg'

			});
		};

		$scope.SaveResetPswd = function () {
			Restangular.all('users').login($scope.user).then(function (loginResult) {
				console.log('loginResult', loginResult);
			}).then(function () {
				if ($scope.user.newpassword === $scope.user.confirmnewpassword) {
					Restangular.all('users?filter[where][username]=' + $scope.user.username).getList().then(function (detail) {
						if (detail.length > 0) {
							$scope.userId = detail[0].id;

							$scope.newdata = {
								password: $scope.user.newpassword
							};

							Restangular.one('users/' + $scope.userId).customPUT($scope.newdata).then(function (response) {
								console.log('response', response);

								$scope.modalPswd.close();
								$scope.logout();
							});
						}
					});
				} else {
					$scope.errormsg = 'Passwords Doesnt Match';
				}
			}, function (response) {
				console.log('response', response);

				$scope.errormsg = 'Invalid Password';

			});
		};

		$scope.okResetPswd = function () {
			$scope.modalPswd.close();
		};


	});
