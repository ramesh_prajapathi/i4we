'use strict';

angular.module('secondarySalesApp')
    .controller('NavbarCtrl', function ($scope, $http, Restangular, $window, $timeout, $location, baseUrl, $idle, $modal, $route, $rootScope, dataShareService, $filter) {


        Restangular.one('lhslanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.LHSLanguage = langResponse[0];
        });

        $scope.userId = $window.sessionStorage.userId;
        $scope.state = $window.sessionStorage.stateId;

        /***************** Overdue count **********************/

        $scope.todoCount = 0;

        $scope.showHfMenu = false;

        if ($window.sessionStorage.roleId == 8) {
            $scope.showHfMenu = true;
        }

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.todoCall1 = 'conditionheaders?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"status":{"nin":[7,5]}},{"caseClosed":{"inq":[false]}}]}}';

            $scope.todoCall2 = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}},{"stage":{"nin":[7,9]}}]}}';

            $scope.todoCall3 = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}},{"stage":{"nin":[7,9]}}]}}';

            $scope.todoCall4 = 'reportincidents?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"currentstatus":{"nin":[5,9]}}]}}';

        } else {

            $scope.todoCall1 = 'conditionheaders?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"status":{"nin":[7,5]}},{"caseClosed":{"inq":[false]}}]}}';

            $scope.todoCall2 = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[false]}},{"stage":{"nin":[7,9]}}]}}';

            $scope.todoCall3 = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}},{"stage":{"nin":[7,9]}}]}}';

            $scope.todoCall4 = 'reportincidents?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"currentstatus":{"nin":[5,9]}}]}}';

        }

        Restangular.all($scope.todoCall1).getList().then(function (resp) {

            $scope.conArray = $filter('orderBy')(resp, 'followupdate');

            $scope.overDueCount = 0;

            var conArrayCount = 0;

            if ($scope.conArray.length == 0) {
                $scope.schemeOdFunc();
            } else {

                angular.forEach($scope.conArray, function (data, index) {
                    data.index = index;

                    if (data.followupdate == null || data.followupdate == '') {
                        conArrayCount++;
                    } else {

                        var date1 = new Date();
                        var date2 = new Date(data.followupdate);
                        var diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));

                        if (diffDays < 0) {
                            $scope.overDueCount++;
                            conArrayCount++;
                        } else {
                            conArrayCount++;
                        }
                    }

                    if (conArrayCount == $scope.conArray.length) {
                        $scope.schemeOdFunc();
                    }
                });
            }
        });

        $scope.schemeOdFunc = function () {

            Restangular.all($scope.todoCall2).getList().then(function (resp3) {

                $scope.schArray = $filter('orderBy')(resp3, 'datetime');

                var schArrayCount = 0;

                if ($scope.schArray.length == 0) {
                    $scope.docOdFunc();
                } else {

                    angular.forEach($scope.schArray, function (data, index) {

                        data.index = index;

                        if (data.datetime == null || data.datetime == '') {
                            schArrayCount++;
                        } else {

                            var date1 = new Date();
                            var date2 = new Date(data.datetime);
                            var diffDays1 = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));

                            if (diffDays1 < 0) {
                                $scope.overDueCount++;
                                schArrayCount++;
                            } else {
                                schArrayCount++;
                            }
                        }

                        if (schArrayCount == $scope.schArray.length) {
                            $scope.docOdFunc();
                        }
                    });
                }
            });
        };

        $scope.docOdFunc = function () {

            Restangular.all($scope.todoCall3).getList().then(function (resp4) {

                $scope.docArray = $filter('orderBy')(resp4, 'datetime');

                var docArrayCount = 0;

                if ($scope.docArray.length == 0) {
                    $scope.incOdFunc();
                } else {

                    angular.forEach($scope.docArray, function (data, index) {

                        data.index = index;

                        if (data.datetime == null || data.datetime == '') {
                            docArrayCount++;
                        } else {

                            var date1 = new Date();
                            var date2 = new Date(data.datetime);
                            var diffDays2 = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));

                            if (diffDays2 < 0) {
                                $scope.overDueCount++;
                                docArrayCount++;
                            } else {
                                docArrayCount++;
                            }
                        }

                        if (docArrayCount == $scope.docArray.length) {
                            $scope.incOdFunc();
                        }
                    });
                }
            });
        };

        $scope.incOdFunc = function () {

            Restangular.all($scope.todoCall4).getList().then(function (resp5) {

                $scope.incArray = $filter('orderBy')(resp5, 'followupdate');

                if ($scope.incArray.length == 0) {
                    $scope.todoCount = $scope.overDueCount;
                } else {

                    angular.forEach($scope.incArray, function (data, index) {

                        data.index = index;

                        if (data.followupdate == null || data.followupdate == '') {
                            return
                        } else {

                            var date1 = new Date();
                            var date2 = new Date(data.followupdate);
                            var diffDays3 = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));

                            if (diffDays3 < 0) {
                                $scope.overDueCount++;
                                $scope.todoCount = $scope.overDueCount;
                            } else {
                                $scope.todoCount = $scope.overDueCount;
                            }
                        }
                    });
                }
            });
        };

        /***************** End ********************************/

        /**************** Quick add ***************************/
        //var valueone = 'true';

        $scope.quickAdd = function () {
            //$scope.$broadcast('quickAdd', true);
            if ($location.path() == '/') {
                $scope.$broadcast('quickAdd', true);
                $window.sessionStorage.quickAddClick = '';
            } else if ($location.path() != '/') {
                $window.sessionStorage.quickAddClick = 'clicked';
                $scope.$broadcast('quickAdd', true);
                $location.path('/');
            }

        };

        var value = 'middle';

        $scope.gotoElement = function () {
            if ($location.path() == '/') {
                $scope.$broadcast('toDo', value);
                $window.sessionStorage.todoClick = '';
            } else if ($location.path() != '/') {
                $window.sessionStorage.todoClick = 'clicked';
                $scope.$broadcast('toDo', value);
                $location.path('/');
            }
        };

        if ($window.sessionStorage.userId) {
            $scope.roles = Restangular.one('roles', $window.sessionStorage.roleId).get().$object;
        }
        /****   for sidebar menu icon change *****/
        $scope.imageHome = "images/piclinks/home-blue.png";
        $scope.imageGeneral = "images/piclinks/facility-grey.png";
        $scope.imageFW = "images/piclinks/members-grey.png";
        $scope.imageWash = "images/piclinks/facility-grey.png";
        $scope.imageFacility = "images/piclinks/facility-admin-grey.png";
        $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
        $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
        $scope.imageNOBudget = "images/piclinks/budgetdark.png";
        $scope.imageLanguage = "images/piclinks/language-grey.png";

        $scope.colorHome = "4px solid #004891";
        $scope.colorGeneral = "4px solid #ffffff";
        $scope.colorFW = "4px solid #ffffff";
        $scope.colorFacility = "4px solid #ffffff";
        $scope.colorDashboard = "4px solid #ffffff";
        $scope.colorSPMBudget = "4px solid #ffffff";
        $scope.colorNOBudget = "4px solid #ffffff";
        $scope.colorLanguage = "4px solid #ffffff";

        $scope.clickHome = function () {

            $window.sessionStorage.todoClick = '';
            $window.sessionStorage.quickAddClick = '';

            $scope.imageHome = "images/piclinks/home-blue.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageFW = "images/piclinks/members-grey.png";
            $scope.imageWash = "images/piclinks/facility-grey.png";
            $scope.imageFacility = "images/piclinks/facility-admin-grey.png";
            $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";
            $scope.imageLanguage = "images/piclinks/language-grey.png";

            $scope.colorHome = "4px solid #004891";
            $scope.colorGeneral = "4px solid #ffffff";
            $scope.colorFW = "4px solid #ffffff";
            $scope.colorFW = "4px solid #ffffff";
            $scope.colorFacility = "4px solid #ffffff";
            $scope.colorDashboard = "4px solid #ffffff";
            $scope.colorSPMBudget = "4px solid #ffffff";
            $scope.colorNOBudget = "4px solid #ffffff";
            $scope.colorLanguage = "4px solid #ffffff";
            $scope.colorWash = "4px solid #ffffff";
            $location.path("/");
        };

        $scope.clickSPMBudget = function () {
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageFW = "images/piclinks/members-grey.png";
            $scope.imageFacility = "images/piclinks/facility-admin-grey.png";
            $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
            $scope.imageSPMBudget = "images/piclinks/budgetwhite.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";
            $scope.imageLanguage = "images/piclinks/language-grey.png";

            $scope.colorHome = "4px solid #ffffff";
            $scope.colorGeneral = "4px solid #ffffff";
            $scope.colorFW = "4px solid #ffffff";
            $scope.colorFacility = "4px solid #ffffff";
            $scope.colorDashboard = "4px solid #ffffff";
            $scope.colorSPMBudget = "4px solid #004891";
            $scope.colorNOBudget = "4px solid #ffffff";
            $scope.colorLanguage = "4px solid #ffffff";
            $location.path("/");

        };

        $scope.clickNOBudget = function () {
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageFW = "images/piclinks/members-grey.png";
            $scope.imageFacility = "images/piclinks/facility-admin-grey.png";
            $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetwhite.png";
            $scope.imageLanguage = "images/piclinks/language-grey.png";

            $scope.colorHome = "4px solid #ffffff";
            $scope.colorGeneral = "4px solid #ffffff";
            $scope.colorFW = "4px solid #ffffff";
            $scope.colorFacility = "4px solid #ffffff";
            $scope.colorDashboard = "4px solid #ffffff";
            $scope.colorSPMBudget = "4px solid #ffffff";
            $scope.colorNOBudget = "4px solid #004891";
            $scope.colorLanguage = "4px solid #ffffff";
            $location.path("/");

        };

        $rootScope.clickDashboard = function () {
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageFW = "images/piclinks/members-grey.png";
            $scope.imageWash = "images/piclinks/facility-grey.png";
            $scope.imageFacility = "images/piclinks/facility-admin-grey.png";
            $scope.imageDashboard = "images/piclinks/anlytics-blue.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";
            $scope.imageLanguage = "images/piclinks/language-grey.png";

            $scope.colorHome = "4px solid #ffffff";
            $scope.colorGeneral = "4px solid #ffffff";
            $scope.colorFW = "4px solid #ffffff";
            $scope.colorWash = "4px solid #ffffff";
            $scope.colorFacility = "4px solid #ffffff";
            $scope.colorDashboard = "4px solid #004891";
            $scope.colorSPMBudget = "4px solid #ffffff";
            $scope.colorNOBudget = "4px solid #ffffff";
            $scope.colorLanguage = "4px solid #ffffff";

            if ($scope.roles.dashboardfacility) {
                $location.path("/");
            } else if ($scope.roles.nobudget) {
                $location.path("/");
            } else if ($scope.roles.rodashboard) {
                $location.path("/");
            } else if ($scope.roles.sodashboard) {
                $location.path("/");
            } else if ($scope.roles.fsmentordashboard) {
                $location.path("/");
            }

        };

        $scope.clickGeneral = function () {
            //            $scope.imageHome = "images/piclinks/homedark.png";
            //            $scope.imageGeneral = "images/piclinks/facilitywhite.png";
            //            $scope.imageFW = "images/piclinks/admindark.png";
            //            $scope.imageFacility = "images/piclinks/dashboarddark.png";
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-blue.png";
            $scope.imageFW = "images/piclinks/members-grey.png";
            $scope.imageWash = "images/piclinks/facility-grey.png";
            $scope.imageFacility = "images/piclinks/facility-admin-grey.png";
            $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";
            $scope.imageLanguage = "images/piclinks/language-grey.png";

            $scope.colorHome = "4px solid #ffffff";
            $scope.colorGeneral = "4px solid #004891";
            $scope.colorFW = "4px solid #ffffff";
            $scope.colorFacility = "4px solid #ffffff";
            $scope.colorDashboard = "4px solid #ffffff";
            $scope.colorSPMBudget = "4px solid #ffffff";
            $scope.colorNOBudget = "4px solid #ffffff";
            $scope.colorLanguage = "4px solid #ffffff";
        };

        $rootScope.clickFW = function () {
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageFW = "images/piclinks/members-blue.png";
            $scope.imageWash = "images/piclinks/facility-grey.png";
            $scope.imageFacility = "images/piclinks/facility-admin-grey.png";
            $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";
            $scope.imageLanguage = "images/piclinks/language-grey.png";

            $scope.colorHome = "4px solid #ffffff";
            $scope.colorGeneral = "4px solid #ffffff";
            $scope.colorFW = "4px solid #004891";
            $scope.colorWash = "4px solid #ffffff";
            $scope.colorFacility = "4px solid #ffffff";
            $scope.colorDashboard = "4px solid #ffffff";
            $scope.colorSPMBudget = "4px solid #ffffff";
            $scope.colorNOBudget = "4px solid #ffffff";
            $scope.colorLanguage = "4px solid #ffffff";
        };

        $rootScope.clickWash = function () {
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-blue.png";
            $scope.imageFW = "images/piclinks/members-grey.png";
            $scope.imageWash = "images/piclinks/facility-blue.png";
            $scope.imageFacility = "images/piclinks/facility-admin-grey.png";
            $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";
            $scope.imageLanguage = "images/piclinks/language-grey.png";

            $scope.colorHome = "4px solid #ffffff";
            $scope.colorGeneral = "4px solid #ffffff";
            $scope.colorFW = "4px solid #ffffff";
            $scope.colorWash = "4px solid #004891";
            $scope.colorFacility = "4px solid #ffffff";
            $scope.colorDashboard = "4px solid #ffffff";
            $scope.colorSPMBudget = "4px solid #ffffff";
            $scope.colorNOBudget = "4px solid #ffffff";
            $scope.colorLanguage = "4px solid #ffffff";
        };

        $scope.clickFacility = function () {
            //            $scope.imageHome = "images/piclinks/homedark.png";
            //            $scope.imageGeneral = "images/piclinks/facilitydark.png";
            //            $scope.imageFW = "images/piclinks/admindark.png";
            //            $scope.imageFacility = "images/piclinks/dashboardwhite.png";
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageLanguage = "images/piclinks/language-grey.png";
            $scope.imageFW = "images/piclinks/members-grey.png";
            $scope.imageFacility = "images/piclinks/facility-admin-blue.png";
            $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";

            $scope.colorHome = "4px solid #ffffff";
            $scope.colorGeneral = "4px solid #ffffff";
            $scope.colorFW = "4px solid #ffffff";
            $scope.colorFacility = "4px solid #004891";
            $scope.colorDashboard = "4px solid #ffffff";
            $scope.colorSPMBudget = "4px solid #ffffff";
            $scope.colorNOBudget = "4px solid #ffffff";
            $scope.colorLanguage = "4px solid #ffffff";
        };

        $scope.clickLanguage = function () {
            //            $scope.imageHome = "images/piclinks/homedark.png";
            //            $scope.imageGeneral = "images/piclinks/facilitydark.png";
            //            $scope.imageFW = "images/piclinks/admindark.png";
            //            $scope.imageFacility = "images/piclinks/dashboardwhite.png";
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageLanguage = "images/piclinks/language-blue.png";
            $scope.imageFW = "images/piclinks/members-grey.png";
            $scope.imageFacility = "images/piclinks/facility-admin-grey.png";
            $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";

            $scope.colorHome = "4px solid #ffffff";
            $scope.colorGeneral = "4px solid #ffffff";
            $scope.colorFW = "4px solid #ffffff";
            $scope.colorFacility = "4px solid #ffffff";
            $scope.colorLanguage = "4px solid #004891";
            $scope.colorDashboard = "4px solid #ffffff";
            $scope.colorSPMBudget = "4px solid #ffffff";
            $scope.colorNOBudget = "4px solid #ffffff";
        };
        /**** Above code  for sidebar menu icon change *****/

        if ($window.sessionStorage.roleId == 1) {
            $scope.clickGeneral();
        }

        Restangular.one('users?filter[where][username]=' + $window.sessionStorage.userName).get().then(function (user) {
            $scope.user = user[0];
        });

        $scope.rolesconfiguration = true;
        $scope.rolesconfiguration1 = true;
        if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 10 || $window.sessionStorage.roleId == 11 || $window.sessionStorage.roleId == 12 || $window.sessionStorage.roleId == 13 || $window.sessionStorage.roleId == 14) {
            $scope.rolesconfiguration = false;
        } else {
            $scope.rolesconfiguration1 = false;
        }
        $scope.UserLanguage = $window.sessionStorage.language;
        $scope.KnowledgeBaseName = "Knowledge Base";
        if ($scope.UserLanguage == 1) {
            $scope.KnowledgeBaseName = "Knowledge Base";
        } else if ($scope.UserLanguage == 2) {
            $scope.KnowledgeBaseName = "ज्ञानधार";
        } else if ($scope.UserLanguage == 3) {
            $scope.KnowledgeBaseName = "ಜ್ಞಾನದ ತಳಹದಿ";
        } else if ($scope.UserLanguage == 4) {
            $scope.KnowledgeBaseName = "அறிவு சார்ந்த";
        } else if ($scope.UserLanguage == 5) {
            $scope.KnowledgeBaseName = "నాలెడ్జ్ బేస్";
        } else if ($scope.UserLanguage == 6) {
            $scope.KnowledgeBaseName = "पायाभूत माहिती";
        }

        /*
		       $scope.menu = [{
		         'title': 'Home',
		         'route': '/?(navbar)?',
		         'link': '/'
		       },
		       {
		         'title': 'hh',
		         'route': '/hh',
		         'link': '/hh'
		       }];
		       */
        /*************************************************************************************
			if ($window.sessionStorage.roleId == 5) {
				Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
					$scope.comemberid = comember.id;
					$scope.partners1 = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false' + '&filter[where][facilityId]=' + $scope.comemberid + '&filter={"where":{"lastmodifiedbyrole":{"inq":[5,6]}}}').getList().then(function (resPartner1) {
						$scope.partners = resPartner1;
					});
				});

			} else {
				$scope.partners2 = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false' + '&filter[where][fieldworker]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][lastmodifiedbyrole]=6').getList().then(function (resPartner2) {
					$scope.partners = resPartner2;
				});
			}
		/**************************************** Member *******************************************/

        if ($window.sessionStorage.roleId == 5) {
            $scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {
                $scope.partners = part1;
                angular.forEach($scope.partners, function (member, index) {
                    member.index = index + 1;
                });
            });
        } else {
            Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;

                    });
                });
            });
        }

        $scope.hideGeneral = true;
        $scope.hideHF = true;
        $scope.hideFacility = true;
        $scope.hideLanguage = true;
        $scope.hideBreaks = false;
        $scope.hideWash = true;
        $scope.hideAnalytics = true;

        /***********new Changes**********/
        $scope.HideShg = true;
        $scope.HideHouseHold = true;
        $scope.HideCondition = true;
        $scope.HidePatient = true;
        $scope.HideShgmeeting = true;
        $scope.HideIntervention = true;
        $scope.HideApplyforschemes = true;
        $scope.HideApplyfordocument = true;
        $scope.HideReportincident = true;
        /***********new Changes**********/

        if ($window.sessionStorage.roleId == 1) {
            $scope.hideHF = false;
            $scope.hideAnalytics = false;
            $scope.hideWash = false;

        } else if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 5 || $window.sessionStorage.roleId == 6) {
            $scope.hideAnalytics = true;
            $scope.hideGeneral = false;
            $scope.hideFacility = false;
            $scope.hideLanguage = false;
            $scope.hideHF = false;
            $scope.hideWash = false;

        } else if ($window.sessionStorage.roleId == 3 || $window.sessionStorage.roleId == 8) {
            $scope.hideGeneral = false;
            $scope.hideFacility = false;
            $scope.hideLanguage = false;

        } else if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 5 || $window.sessionStorage.roleId == 6) {
            $scope.hideAnalytics = true;
            $scope.hideGeneral = false;
            $scope.hideFacility = false;
            $scope.hideLanguage = false;
            $scope.hideHF = false;
            $scope.hideWash = false;

        } else if ($window.sessionStorage.roleId == 7) {

            $scope.hideHF = true;
            $scope.hideGeneral = false;
            $scope.hideFacility = false;
            $scope.hideLanguage = false;
            $scope.hideWash = true;
            $scope.hideAnalytics = true;
            $scope.HideShg = false;
            $scope.HideHouseHold = true;
            $scope.HideCondition = true;
            $scope.HidePatient = true;
            $scope.HideShgmeeting = false;
            $scope.HideIntervention = false;
            $scope.HideApplyforschemes = false;
            $scope.HideApplyfordocument = false;
            $scope.HideReportincident = false;

        } else if ($window.sessionStorage.roleId == 9) {
            $scope.hideHF = false;
            $scope.hideGeneral = false;
            $scope.hideFacility = false;
            $scope.hideLanguage = true;
            $scope.hideBreaks = true;
            $scope.hideWash = false;
            $scope.hideAnalytics = false;
            $scope.HideShg = false;
            $scope.HideHouseHold = false;
            $scope.HideCondition = false;
            $scope.HidePatient = false;
            $scope.HideShgmeeting = false;
            $scope.HideIntervention = false;
            $scope.HideApplyforschemes = false;
            $scope.HideApplyfordocument = false;
            $scope.HideReportincident = false;
        }


        /***********************************************************************/




        $scope.searchbulkupdate = '';



        //        $scope.openOnetoOne = function () {
        //            $scope.modalInstance1 = $modal.open({
        //                animation: true,
        //                templateUrl: 'template/OnetooOne.html',
        //                scope: $scope,
        //                backdrop: 'static'
        //
        //            });
        //        };

        $scope.okOnetooOne = function (fullname) {
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/onetoone/" + fullname);
            $route.reload();
        };

        $scope.cancelOnetooOne = function () {
            $scope.modalInstance1.close();
        };

        $scope.loading = true;
        //$scope.LoadingMOdal = false;
        $scope.toggleLoading = function () {
            $scope.modalInstanceLoad = $modal.open({
                animation: true,
                templateUrl: 'template/LodingModal.html',
                scope: $scope,
                backdrop: 'static',
                size: 'sm'

            });
            //$scope.LoadingMOdal = !$scope.LoadingMOdal;
        };



        $scope.openMainReportIncident = function () {
            $scope.modalMainReport = $modal.open({
                animation: true,
                templateUrl: 'template/MainReportIncident.html',
                scope: $scope,
                backdrop: 'static'

            });

        };

        $scope.okMainReportIncident = function (fullname) {
            $scope.modalMainReport.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okReportIncident', $rootScope.fullname);
            $location.path("/reportincident");
            $route.reload();
        };

        $scope.cancelMainReportIncident = function () {
            $scope.modalMainReport.close();
        };
        /***********************************************/
        $scope.openApplyforScheme = function () {
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/ApplyforScheme.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okApplyforScheme = function (fullname) {
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/applyforschemes");
            $route.reload();
        };

        $scope.cancelApplyforScheme = function () {
            $scope.modalInstance1.close();
        };

        $scope.openApplyforDocument = function () {
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/ApplyforDocument.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okApplyforDocument = function (fullname) {
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/applyfordocuments");
            $route.reload();
        };

        $scope.cancelApplyforDocument = function () {
            $scope.modalInstance1.close();
        };


        $scope.openfPlaning = function () {
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/fPlaning.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okfPlaning = function (fullname) {
            $scope.modalInstance1.close();
            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.fullname', $rootScope.fullname);
            $location.path("/financialPlanning");
            $route.reload();
        };

        $scope.cancelfPlaning = function () {
            $scope.modalInstance1.close();
        };
        /****************************************/

        $scope.menu = [{
                'title': 'Home',
                'link': '/'
    },
            {
                'title': 'States',
                'link': '/states'
    },
            {
                'title': 'Contact',
                'link': '#'
    }];

        $scope.fwprofileedit = true;
        $scope.coprofileedit = true;
        //        if ($window.sessionStorage.roleId == 5) {
        //            $scope.coprofileedit = false;
        //            $scope.UserId = $window.sessionStorage.UserEmployeeId;
        //        } else {
        //            //$scope.UserId = $window.sessionStorage.UserEmployeeId;
        //            $scope.fwprofileedit = false;
        //            $scope.UserId = $window.sessionStorage.UserEmployeeId;
        //        }

        //  console.log($window.sessionStorage.userId + 'sddsf');
        $scope.showNav = function () {
            //console.log('$window.sessionStorage.userId', $window.sessionStorage.userId);
            if ($window.sessionStorage.userId) {
                return true;
            }
        };
        $scope.isActive = function (route) {
            return route === $location.path();
        };

        $scope.getClass = function (path) {
            if ($location.path().substr(0, path.length) === path) {
                return 'active';
            } else {
                return '';
            }
        }

        $scope.logout = function () {
            //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
            Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
                $window.sessionStorage.userId = '';
                console.log('Logout');
            }).then(function (redirect) {

                $location.path("/login");
                $idle.unwatch();

            });
        };


        /*************************** Language *******************************/
        $scope.languages = Restangular.all('languages').getList().$object;



    });
