'use strict';

angular.module('secondarySalesApp')
    .controller('ShgMigrationCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $rootScope) {

        $scope.obj = {};
     $scope.confirmSave = false;


        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.hideforHf = true;
            $scope.Showforsite = false;

            $scope.Showforsite1 = false;
            $scope.hideAddBtn = true;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.hhfilterCall = 'households?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.ShgfilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}}]}}';

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.modalInstanceLoad.close();
            });

        } else if ($window.sessionStorage.roleId + "" === "7") {
            $scope.hideAddBtn = false;
            $scope.Showforsite = false;
            $scope.Showforsite1 = true;
            $scope.hideforHf = false;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.hhfilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.ShgfilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}}]}}';

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.modalInstanceLoad.close();
            });
        } else {

            $scope.hideforHf = false;
            $scope.Showforsite = true;
            $scope.Showforsite1 = true;
            $scope.hideAddBtn = true;
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.hhfilterCall = 'households?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            $scope.ShgfilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}}]}}';

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.modalInstanceLoad.close();
            });
        }

        Restangular.all('shgs').getList().then(function (shg) {
            $scope.shgdisplay = shg;
        });

        $scope.disableShg = true;

        $scope.changeCheck = function (value) {
            var data = $scope.members.filter(function (arr) {
                return arr.checkbox == true
            });
            // console.log(data);

            if (data == 0) {
                $scope.disableShg = true;
            } else {
                $scope.disableShg = false;
            }
        };

        $scope.$watch('obj.HfId', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {

                if ($window.sessionStorage.roleId + "" === "3") {
                    $scope.shgs = Restangular.all('shgs?filter={"where":{"and":[{"associatedHF":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().$object;
                } else {
                    $scope.shgs = Restangular.all('shgs?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"associatedHF":{"inq":[' + newValue + ']}}]}}').getList().$object;
                }

                Restangular.all('members?filter[where][deleteFlag]=false' + '&filter[where][associatedHF]=' + newValue).getList().then(function (urs) {
                    $scope.members = urs;

                    angular.forEach($scope.members, function (member, index) {
                        member.index = index + 1;
                        if (member.isHeadOfFamily == true) {
                            member.hhormember = 'HH';
                        } else {
                            member.hhormember = 'Member';
                        }

                        var data = $scope.users.filter(function (arr) {
                            return arr.id == member.associatedHF
                        })[0];

                        if (data != undefined) {
                            member.assignedTo = data.name;
                        }

                        var data1 = $scope.shgdisplay.filter(function (arr) {
                            return arr.id == member.shgId
                        })[0];

                        if (data1 != undefined) {
                            member.shgName = data1.groupName;
                        }
                    });
                });

            }
        });
    
    $scope.$watch('obj.shgId', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {
                
                        Restangular.one('shgs', newValue).get().then(function (shg) {
            $scope.shgName = shg.groupName;
        });
                
            }
    });
    
    /**************confirmation before save*********/
    $scope.membermigrates = [];
     $scope.SaveConfirmPopUp = function () {
     
          for(var i =0; i < $scope.members.length; i++){ 

                if ($scope.members[i].checkbox == true) {

                    $scope.members[i].newid = $scope.members[i].id;
                    $scope.members[i].migratedate = new Date();
                    $scope.members[i].migrateuser = $window.sessionStorage.userId;
                   $scope.members[i].newShgId = $scope.obj.shgId;
                   $scope.members[i].newShgname = $scope.shgName;
                    $scope.membermigrates.push($scope.members[i]);
                }
          }
          $scope.confirmSave = true;
     };
    
    $scope.cancelMigrate = function(){
         $scope.membermigrates = [];
         $scope.confirmSave = false;
        
    }
    
     /**************final saave*********/

        $scope.migcount = 0;
       $scope.memupdate = {};

        $scope.Migrate = function () {
           // console.log('$scope.obj.shgId', $scope.obj.shgId);
           if ($scope.migcount < $scope.members.length) {

                if ($scope.members[$scope.migcount].checkbox == true) {

                    $scope.members[$scope.migcount].newid = $scope.members[$scope.migcount].id;
                    $scope.members[$scope.migcount].migratedate = new Date();
                    $scope.members[$scope.migcount].migrateuser = $window.sessionStorage.userId;

                    delete $scope.members[$scope.migcount]['id'];

                    Restangular.all('membermigrate').post($scope.members[$scope.migcount]).then(function (resp) {


                         if($scope.obj.shgId == '' || $scope.obj.shgId == null || $scope.obj.shgId == undefined){
                      $scope.memupdate.shgId = null;
                      }else{
                       $scope.memupdate.shgId = $scope.obj.shgId;
                      }
                        

                        Restangular.one('members', $scope.members[$scope.migcount].newid).customPUT($scope.memupdate).then(function (subResponse) {
                            console.log(subResponse.id);
                            $scope.migcount++;
                            $scope.Migrate();
                        });
                    });
                } else {
                    $scope.migcount++;
                    $scope.Migrate();
                }
            } else {
                $route.reload();
                $scope.confirmSave = true;
            }
        };
   
     
    

    });
