'use strict';

angular.module('secondarySalesApp')
	.controller('TypCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/typologies/create' || $location.path() === '/typologies/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/typologies/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/typologies/create' || $location.path() === '/typologies/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/typologies/create' || $location.path() === '/typologies/' + $routeParams.id;
			return visible;
		};



		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			//console.log('$scope.countryId From Landing', $scope.pageSize);
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			console.log('mypage', mypage);
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};


		console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
		if ($window.sessionStorage.prviousLocation != "partials/typology") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
			$scope.currentpage = 1;
			$scope.pageSize = 25;
		}

		/**********************************/
		if ($routeParams.id) {
			$scope.message = 'Typology has been Updated!';
			Restangular.one('typologies', $routeParams.id).get().then(function (typology) {
				$scope.original = typology;
				$scope.typology = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Typology has been Created!';	
		}
		$scope.Search = $scope.name;

		/*********************************** INDEX *******************************************/
		//?filter[where][deleteflag]=false
		$scope.zn = Restangular.all('typologies?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.typologies = zn;
			angular.forEach($scope.typologies, function (member, index) {
				member.index = index + 1;
			});
		});

		/****************************** SAVE *******************************************/
	
	$scope.typology = {
		"name": '',
		"deleteflag": false
	};
	
		$scope.submitDisable = false;
		$scope.validatestring = '';
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.typology.name == '' || $scope.typology.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter typology name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.typology.hnname == '' || $scope.typology.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter typology name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.typology.knname == '' || $scope.typology.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter typology name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.typology.taname == '' || $scope.typology.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter typology name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.typology.tename == '' || $scope.typology.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter typology name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.typology.mrname == '' || $scope.typology.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter typology name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.typologies.post($scope.typology).then(function () {
					console.log('Typology Saved');
					window.location = '/typologies';
				});
			}
		};
		/******************** UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.typology.name == '' || $scope.typology.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter typology name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.typology.hnname == '' || $scope.typology.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter typology name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.typology.knname == '' || $scope.typology.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter typology name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.typology.taname == '' || $scope.typology.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter typology name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.typology.tename == '' || $scope.typology.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter typology name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.typology.mrname == '' || $scope.typology.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Typology name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.typologies.customPUT($scope.typology).then(function () {
					console.log('typology Saved');
					window.location = '/typologies';
				});
			}
		};


		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

		/************************************ DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('typologies/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

	});
