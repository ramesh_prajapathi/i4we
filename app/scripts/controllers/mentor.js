'use strict';

angular.module('secondarySalesApp')
	.controller('MentorCtrl', function ($scope, Restangular, $route, $window) {
			if ($window.sessionStorage.roleId != 1) {
				window.location = "/";
			}

			if ($window.sessionStorage.Mentor == null || $window.sessionStorage.Mentor == undefined) {
				$window.sessionStorage.Mentor = null;
				$window.sessionStorage.Mentor_currentPage = 1;
				$window.sessionStorage.Mentor_currentPagesize = 25;
			} else {
				$scope.countryId = $window.sessionStorage.co_zoneId;
				$scope.currentpage = $window.sessionStorage.Mentor_currentPage;
				$scope.pageSize = $window.sessionStorage.Mentor_currentPagesize;
			}


			$scope.currentPage = $window.sessionStorage.Mentor_currentPage;
			$scope.PageChanged = function (newPage, oldPage) {
				$scope.currentpage = newPage;
				$window.sessionStorage.Mentor_currentPage = newPage;
			};

			if ($window.sessionStorage.prviousLocation != "partials/mentor-form") {
				$window.sessionStorage.Mentor = '';
				$window.sessionStorage.Mentor_currentPage = 1;
				$window.sessionStorage.Mentor_currentPagesize = 25;
				//$scope.currentpage = 1;
				//$scope.pageSize = 25;


				$scope.tds = Restangular.all('mentors?filter[where][role]=16' + '&filter[where][deleteflag]=false').getList().then(function (response) {
					console.log('response', response);
					$scope.mentors = response;
					angular.forEach($scope.mentors, function (member, index) {
						member.index = index + 1;
					});
				});
		}

		$scope.pageSize = $window.sessionStorage.Mentor_currentPagesize; $scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.Mentor_currentPagesize = mypage;
		};
		/*$scope.zn = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
			$scope.zones = znes;
			$scope.countryId = $window.sessionStorage.co_zoneId;
		$scope.part = Restangular.all('mentors?filter[where][deleteflag]=false').getList().then(function (part1) {
					$scope.mentors = part1;
					angular.forEach($scope.mentors, function (member, index) {
						member.index = index + 1;
						
						for (var n = 0; n < $scope.zones.length; n++) {
						if (member.state == $scope.zones[n].id) {
							member.StateName = $scope.zones[n];
							break;
						}
					}
						$scope.TotalData = [];
						$scope.TotalData.push(member);
					});
				});
		});*/

		$scope.countryId = null; $scope.znes = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
			$scope.zones = znes;
			$scope.countryId = $window.sessionStorage.Mentor;
		});


		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('mentors/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

		$scope.countryId = ''; $scope.mentors = {}; $scope.$watch('countryId', function (newValue, oldValue) {
			if (newValue === oldValue) {
				return;
			} else {
				$window.sessionStorage.Mentor = newValue;
				$scope.sal = Restangular.all('mentors?filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (sal) {
					//console.log('sal', sal);
					$scope.mentors = sal;
					angular.forEach($scope.mentors, function (member, index) {
						member.index = index + 1;

						for (var n = 0; n < $scope.zones.length; n++) {
							if (member.state == $scope.zones[n].id) {
								member.StateName = $scope.zones[n];
								break;
							}
						}
						$scope.TotalData = [];
						$scope.TotalData.push(member);
					});
				});
			}
		}); $scope.currentpage = $window.sessionStorage.Mentor_currentPage; $scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.Mentor_currentPage = newPage;
		};



	});
