'use strict';

angular.module('secondarySalesApp')
	.controller('PillarsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/pillars/create' || $location.path() === '/pillars/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/pillars/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/pillars/create' || $location.path() === '/pillars/' + $routeParams.id;
			return visible;
		};

		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/pillars/create' || $location.path() === '/pillars/' + $routeParams.id;
			return visible;
		};


		/*************************************INDEX****************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}
		/*else {
				$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
				$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			}
			*/

		if ($window.sessionStorage.prviousLocation != "partials/pillars") {
			//$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
			//$scope.currentpage = 1;
			//$scope.pageSize = 5;
		}
		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};


		$scope.searchCon = $scope.name;

		$scope.con = Restangular.all('pillars?filter[where][deleteflag]=false').getList().then(function (con) {
			$scope.pillars = con;
			angular.forEach($scope.pillars, function (member, index) {
				member.index = index + 1;
			});
		});
		$scope.pillar = {};

		/*******************************CREATE************************************/
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.pillar.name == '' || $scope.pillar.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.pillar.hnname == '' || $scope.pillar.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.pillar.knname == '' || $scope.pillar.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.pillar.taname == '' || $scope.pillar.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.pillar.tename == '' || $scope.pillar.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.pillar.mrname == '' || $scope.pillar.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.pillars.post($scope.pillar).then(function () {
					console.log('$scope.pillar', $scope.pillar);
					window.location = '/pillars';
				});
			}
		};

		/************************************************************UPDATE****************************/

		$scope.validatestring = '';
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.pillar.name == '' || $scope.pillar.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.pillar.hnname == '' || $scope.pillar.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.pillar.knname == '' || $scope.pillar.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.pillar.taname == '' || $scope.pillar.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.pillar.tename == '' || $scope.pillar.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.pillar.mrname == '' || $scope.pillar.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter pillar in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.pillars.customPUT($scope.pillar).then(function () {
					console.log('$scope.pillar', $scope.pillar);
					window.location = '/pillars';
				});
			}
		};


		if ($routeParams.id) {
			$scope.message = 'Pillar has been Updated!';
			Restangular.one('pillars', $routeParams.id).get().then(function (pillar) {
				$scope.original = pillar;
				$scope.pillar = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Pillar has been Updated!';
		}

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/*---------------------------Delete---------------------------------------------------*/

		$scope.Delete = function (id) {
				$scope.item = [{
					deleteflag: true
            }]
				Restangular.one('pillars/' + id).customPUT($scope.item[0]).then(function () {
					$route.reload();
				});
			}
			/*******************************************DELETE*************************/


	});
