'use strict';

angular.module('secondarySalesApp')
	.controller('EnglishCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
		$scope.isCreateSave = true;
		$scope.heading = 'Multi-Language';

	$scope.multilanguage = {
			languageId: 1
		};

		$scope.submitmultilanguages = Restangular.all('multilanguages').getList().$object;

		$scope.SaveLanguage = function () {
			$scope.submitmultilanguages.customPUT($scope.multilanguage).then(function (response) {
				console.log('Response', response);
				window.location = '/';
			});
		};

		Restangular.one('multilanguages', 1).get().then(function (zone) {
			$scope.original = zone;
			$scope.multilanguage = Restangular.copy($scope.original);
			$scope.modalInstanceLoad.close();
		});

		$scope.LanguageName = 'English';
		$scope.Language = 'Language';
	});
