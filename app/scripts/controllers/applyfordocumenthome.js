'use strict';

angular.module('secondarySalesApp')
    .controller('ApplyForDocumentHomeCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {

        $rootScope.clickFW();
        $scope.UserLanguage = $window.sessionStorage.language;
        $scope.DocumentLanguage = {};
        $scope.multiLang = Restangular.one('documentlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.DocumentLanguage = langResponse[0];

        });


        $scope.someFocusVariable = true;

        //        if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        //            window.location = "/";
        //        }
        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/applyfordocuments") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
        /*************************************************************************************************/
        $scope.auditlog = {
            modifiedbyroleid: $window.sessionStorage.roleId,
            modifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            entityroleid: 49,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,

        };

        $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;
        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;
        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;
        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;
        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;
        $scope.roledsply = Restangular.all('roles').getList().$object;
        $scope.responcedreceived = Restangular.all('responsereceived?filter[where][deleteFlag]=false').getList().$object;
        $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteFlag]=false').getList().$object;
        $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteFlag]=false').getList().$object;
        $scope.finalDocsNew = Restangular.all('documenttypes?filter[where][deleteFlag]=false').getList().$object;
        $scope.finalSchemeNew = Restangular.all('schemestages?filter[where][deleteFlag]=false').getList().$object;

        $scope.ssgThisHHBelongs = [];

        $scope.schemeMasters = [];

        $scope.filterFields = ['memberName', 'HHId', 'mobile'];
        $scope.searchInput = '';

        Restangular.all('documenttypes?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (schemes) {
            $scope.finalSchemes = schemes;
            Restangular.all('schemestages?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (schemestages) {
                $scope.finalSchemeStages = schemestages;

               
                    if ($window.sessionStorage.roleId + "" === "3") {
                        $scope.filterCall = 'schememasters?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';
                        
                         $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}}]}}';
                        
                    } else {
                        $scope.filterCall = 'schememasters?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"documentflag":{"inq":[true]}}]}}';
                        
                         $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}}]}}';
                    }
                    
                    Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
                        
                    $scope.finalMembers = mems;
                    
                    Restangular.all($scope.filterCall).getList().then(function (household) {
                        $scope.schememstrs = household;
                        
                        $scope.modalInstanceLoad.close();
                        
                        angular.forEach($scope.schememstrs, function (member, index) {
                            member.index = index + 1;

                            member.datedisply = $filter('date')(member.datetime, 'dd-MMM-yyyy');
                            member.datetime = $filter('date')(member.datetime, 'dd/MM/yyyy');
                            member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                            member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                            if (member.datetime == null) {
                                member.datetime = '';
                            }
                            if (member.responserecieve == null) {
                                member.responserecieve = '';
                            }
                            if (member.lastModifiedDate == null) {
                                member.lastModifiedDate = '';
                            }
                            if (member.rejection == null) {
                                member.rejection = '';
                            }
                            if (member.delay == null) {
                                member.delay = '';
                            }
                            if (member.collectedrequired == null) {
                                member.collectedrequired = '';
                            }
                            if (member.servicefee == null) {
                                member.servicefee = 0;
                            }

                            for (var i = 0; i < $scope.finalSchemes.length; i++) {
                                if ($window.sessionStorage.language == 1) {
                                    if ($scope.finalSchemes[i].id + "" === member.schemeId + "") {
                                        member.schemeName = $scope.finalSchemes[i].name;
                                        break;
                                    }
                                } else if ($window.sessionStorage.language != 1) {
                                    if ($scope.finalSchemes[i].parentId + "" === member.schemeId + "") {
                                        member.schemeName = $scope.finalSchemes[i].name;
                                        break;
                                    }
                                }
                            }

                            for (var i = 0; i < $scope.finalDocsNew.length; i++) {
                                if ($scope.finalDocsNew[i].id + "" === member.schemeId + "") {
                                    member.schemeEngName = $scope.finalDocsNew[i].name;
                                    break;
                                }
                            }

                            for (var j = 0; j < $scope.finalSchemeStages.length; j++) {
                                if ($window.sessionStorage.language == 1) {
                                    if ($scope.finalSchemeStages[j].id + "" === member.stage + "") {
                                        member.stageName = $scope.finalSchemeStages[j].name;
                                        break;
                                    }
                                } else if ($window.sessionStorage.language != 1) {
                                    if ($scope.finalSchemeStages[j].parentId + "" === member.stage + "") {
                                        member.stageName = $scope.finalSchemeStages[j].name;
                                        break;
                                    }
                                }
                            }

                            for (var j = 0; j < $scope.finalSchemeNew.length; j++) {
                                if ($scope.finalSchemeNew[j].id + "" === member.stage + "") {
                                    member.stageEngName = $scope.finalSchemeNew[j].name;
                                    break;
                                }
                            }

                            for (var k = 0; k < $scope.finalMembers.length; k++) {
                                if ($scope.finalMembers[k].id + "" === member.memberId + "") {
                                    member.memberName = $scope.finalMembers[k].name;
                                    member.individualId = $scope.finalMembers[k].individualId;
                                    member.mobile = $scope.finalMembers[k].mobile;
                                    member.HHId = $scope.finalMembers[k].HHId;
                                    break;
                                }
                            }

                            for (var l = 0; l < $scope.responcedreceived.length; l++) {
                                if ($scope.responcedreceived[l].id + "" === member.responserecieve + "") {
                                    member.responsereceivedName = $scope.responcedreceived[l].name;
                                    break;
                                }
                            }

                            for (var m = 0; m < $scope.reasonforrejections.length; m++) {
                                if ($scope.reasonforrejections[m].id + "" === member.rejection + "") {
                                    member.rejectionName = $scope.reasonforrejections[m].name;
                                    break;
                                }
                            }

                            for (var n = 0; n < $scope.reasonfordelayed.length; n++) {
                                if ($scope.reasonfordelayed[n].id + "" === member.delay + "") {
                                    member.delayName = $scope.reasonfordelayed[n].name;
                                    break;
                                }
                            }

                            for (var np = 0; np < $scope.usersdisplay.length; np++) {
                                if (member.associatedHF == $scope.usersdisplay[np].id) {
                                    member.assignedTo = $scope.usersdisplay[np].name;
                                    break;
                                }
                            }

                            for (var p = 0; p < $scope.countrydisplay.length; p++) {
                                if (member.countryId == $scope.countrydisplay[p].id) {
                                    member.countryname = $scope.countrydisplay[p].name;
                                    break;
                                }
                            }
                            for (var q = 0; q < $scope.statedisplay.length; q++) {
                                if (member.stateId == $scope.statedisplay[q].id) {
                                    member.statename = $scope.statedisplay[q].name;
                                    break;
                                }
                            }
                            for (var r = 0; r < $scope.districtdsply.length; r++) {
                                if (member.districtId == $scope.districtdsply[r].id) {
                                    member.districtname = $scope.districtdsply[r].name;
                                    break;
                                }
                            }
                            for (var s = 0; s < $scope.sitedsply.length; s++) {
                                if (member.siteId == $scope.sitedsply[s].id) {
                                    member.sitename = $scope.sitedsply[s].name;
                                    break;
                                }
                            }
                            for (var t = 0; t < $scope.usersdisplay.length; t++) {
                                if (member.createdBy == $scope.usersdisplay[t].id) {
                                    member.createdByname = $scope.usersdisplay[t].username;
                                    break;
                                }
                            }
                            for (var u = 0; u < $scope.usersdisplay.length; u++) {
                                if (member.lastModifiedBy == $scope.usersdisplay[u].id) {
                                    member.lastModifiedByname = $scope.usersdisplay[u].username;
                                    break;
                                }
                            }
                            for (var v = 0; v < $scope.roledsply.length; v++) {
                                if (member.lastModifiedByRole == $scope.roledsply[v].id) {
                                    member.lastModifiedByRolename = $scope.roledsply[v].name;
                                    break;
                                }
                            }
                            for (var w = 0; w < $scope.roledsply.length; w++) {
                                if (member.createdByRole == $scope.roledsply[w].id) {
                                    member.createdByRolename = $scope.roledsply[w].name;
                                    break;
                                }
                            }

                        });
                        //  console.log("$scope.schememstrs", $scope.schememstrs);
                    });
                });
            });
        });

        $scope.status = 'active';

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }



        /**************************Export data to excel sheet ***************/

        $scope.valASCount = 0;

        $scope.SchemeMastersArray = [];

        $scope.exportData = function () {
            $scope.SchemeMastersArray = [];
            $scope.valASCount = 0;

            if ($scope.schememstrs.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.schememstrs.length; c++) {
                    $scope.SchemeMastersArray.push({
                        'Sr No': $scope.schememstrs[c].index,
                        'COUNTRY': $scope.schememstrs[c].countryname,
                        'STATE': $scope.schememstrs[c].statename,
                        'DISTRICT': $scope.schememstrs[c].districtname,
                        'SITE': $scope.schememstrs[c].sitename,
                        'MEMBER': $scope.schememstrs[c].memberName,
                        'INDIVIDUAL ID': $scope.schememstrs[c].individualId,
                        'DOCUMENT': $scope.schememstrs[c].schemeEngName,
                        'STAGE': $scope.schememstrs[c].stageEngName,
                        'RESPONSE RECEIVED': $scope.schememstrs[c].responsereceivedName,
                        'REASON FOR REJECTION': $scope.schememstrs[c].rejectionName,
                        'REASON FOR DELAY': $scope.schememstrs[c].delayName,
                        'FOLLOW UP DATE': $scope.schememstrs[c].datetime,
                        'COLLECTED REQUIRED': $scope.schememstrs[c].collectedrequired,
                        'ASSIGNED TO': $scope.schememstrs[c].assignedTo,
                        'SERVICE FEE CHARGED': $scope.schememstrs[c].servicefee,
                        'CREATED BY': $scope.schememstrs[c].createdByname,
                        'CREATED DATE': $scope.schememstrs[c].createdDate,
                        'CREATED ROLE': $scope.schememstrs[c].createdByRolename,
                        'LAST MODIFIED BY': $scope.schememstrs[c].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.schememstrs[c].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.schememstrs[c].lastModifiedByRolename,
                        'DELETE FLAG': $scope.schememstrs[c].deleteFlag
                    });

                    $scope.valASCount++;

                    if ($scope.schememstrs.length == $scope.valASCount) {
                        alasql('SELECT * INTO XLSX("applyfordocument.xlsx",{headers:true}) FROM ?', [$scope.SchemeMastersArray]);
                    }
                }
            }
        };


        /************** Delete Function *************************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('schememasters/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }



        $scope.beneficiary = {
            "reasonforarchive": false
        }
        //console.log('$scope.beneficiary.reasonforarchive', $scope.reasonforarchive)
        $scope.modalTitle = 'Reason For Archive Member'
        $scope.memberdataModal = false;
        $scope.documentdataModal = false;



        $scope.Delete11 = function (id) {
            //console.log('i m delete1');
            $scope.archivMemId = id;
            $scope.documentdataModal = !$scope.documentdataModal;
        }

        $scope.Delete1 = function (id) {
            $scope.archivMemId = id;
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/option.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.Cancelmodal = function () {
            $scope.modalInstance1.close();
        };


        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };
        $scope.okAlert = function () {
            $scope.modalOneAlert.close();
        };

        //$scope.reasonforarchive = '';
        //  console.log('$scope.documentdataModal', $scope.documentdataModal);
        $scope.okdone = false;
        $scope.validatestring = '';
        $scope.OK = function () {
            //        console.log('I m Ok');
            //        //console.log('$scope.documentdataModal',$scope.documentdataModal);
            //        if ($scope.beneficiary.reasonforarchive == false) {
            //            /*	$scope.validatestring = $scope.validatestring + 'Please Enter State Count';
            //            }
            //            if ($scope.validatestring != '') {
            //            	$scope.toggleValidation();
            //            	$scope.validatestring1 = $scope.validatestring;
            //            	$scope.validatestring = '';
            //            	
            //            	*/
            //            //alert('Choose an Option');
            //
            //            $scope.modalOneAlert = $modal.open({
            //                animation: true,
            //                templateUrl: 'template/AlertModal.html',
            //                scope: $scope,
            //                backdrop: 'static',
            //                keyboard: false,
            //                size: 'sm',
            //                windowClass: 'modal-danger'
            //
            //            });
            //
            //        } else {
            //            $scope.modalInstance1.close();
            //            //$scope.documentdataModal = !$scope.documentdataModal;
            //            $scope.item2 = [{
            //                reasonforarchive: $scope.beneficiary.reasonforarchive
            //            }]
            //            Restangular.one('beneficiaries/' + $scope.archivMemId).customPUT($scope.item2[0]).then(function (reasonforarchive) {
            //                console.log('reasonforarchive', reasonforarchive);
            //            });
            //            $scope.okdone = true;
            //            console.log('$scope.reasonforarchive', $scope.beneficiary.reasonforarchive);
            //            console.log('$scope.archivMemId', $scope.archivMemId);
            //            if ($scope.okdone == true) {
            //                $scope.Delete2($scope.archivMemId);
            //            }
            //        }
        }
        $scope.OK1 = function () {
            //        console.log('$scope.reasonforarchive', $scope.beneficiary.reasonforarchive);
            //        console.log('$scope.archivMemId', $scope.archivMemId);
            //        var id = $scope.archivMemId;
            //        $scope.item = [{
            //            partiallydeleted: true,
            //            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            //            lastmodifiedtime: new Date(),
            //            lastmodifiedbyrole: $window.sessionStorage.roleId,
            //            reasonforarchive: $scope.beneficiary.reasonforarchive
            //            }]
            //        Restangular.one('beneficiaries/' + $scope.archivMemId).customPUT($scope.item[0]).then(function () {
            //            //$route.reload();
            //            Restangular.all('todos?filter[where][beneficiaryid]=' + $scope.archivMemId + '&filter[where][deleteflag]=false').getList().then(function (tds) {
            //                if (tds.length > 0) {
            //                    $scope.todosToDelete = tds;
            //                    $scope.deleteTodo($scope.archivMemId);
            //                } else {
            //                    Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $scope.archivMemId + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
            //                        if (rprtincdnts.length > 0) {
            //                            $scope.reportsToDelete = rprtincdnts;
            //                            $scope.deleteReportIncident($scope.archivMemId);
            //                        } else {
            //                            $scope.itemben = [{
            //                                partiallydeleted: false,
            //                                deleteflag: true,
            //                                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            //                                lastmodifiedtime: new Date(),
            //                                lastmodifiedbyrole: $window.sessionStorage.roleId,
            //                                reasonforarchive: $scope.beneficiary.reasonforarchive
            //            }]
            //                            Restangular.one('beneficiaries/' + $scope.archivMemId).customPUT($scope.itemben[0]).then(function () {
            //                                $scope.modalInstanceLoad.close();
            //                                //$route.reload();
            //                                window.location = "/members";
            //                            });
            //                        }
            //                    });
            //                }
            //            });
            //        });


        }

        $scope.deleteTodo = function (benId) {
            //        $scope.itemtodo = [{
            //            deleteflag: true,
            //            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            //            lastmodifiedtime: new Date(),
            //            roleId: $window.sessionStorage.roleId
            //            }]
            //        Restangular.one('todos/' + $scope.todosToDelete[$scope.deleteCount].id).customPUT($scope.itemtodo[0]).then(function () {
            //            $scope.deleteCount++;
            //            if ($scope.deleteCount < $scope.todosToDelete.length) {
            //                $scope.deleteTodo(benId);
            //            } else {
            //                Restangular.all('reportincidents?filter[where][beneficiaryid]=' + benId + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
            //                    if (rprtincdnts.length > 0) {
            //                        $scope.reportsToDelete = rprtincdnts;
            //                        $scope.deleteReportIncident(benId);
            //                    } else {
            //                        $scope.itemben = [{
            //                            partiallydeleted: false,
            //                            deleteflag: true,
            //                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            //                            lastmodifiedtime: new Date(),
            //                            lastmodifiedbyrole: $window.sessionStorage.roleId
            //            }]
            //                        Restangular.one('beneficiaries/' + benId).customPUT($scope.itemben[0]).then(function () {
            //                            $scope.modalInstanceLoad.close();
            //                            //$route.reload();
            //                            window.location = "/members";
            //                        });
            //                    }
            //                });
            //            }
            //        });
        }

        $scope.deleteReportIncident = function (benId) {
            //        $scope.itemreport = [{
            //            deleteflag: true,
            //            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            //            lastmodifiedtime: new Date(),
            //            lastmodifiedbyrole: $window.sessionStorage.roleId
            //            }]
            //        Restangular.one('reportincidents/' + $scope.reportsToDelete[$scope.deleteReportCount].id).customPUT($scope.itemreport[0]).then(function () {
            //            $scope.deleteReportCount++;
            //            if ($scope.deleteReportCount < $scope.reportsToDelete.length) {
            //                $scope.deleteReportIncident(benId);
            //            } else {
            //                $scope.itemben = [{
            //                    partiallydeleted: false,
            //                    deleteflag: true,
            //                    lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            //                    lastmodifiedtime: new Date(),
            //                    lastmodifiedbyrole: $window.sessionStorage.roleId
            //            }]
            //                Restangular.one('beneficiaries/' + benId).customPUT($scope.itemben[0]).then(function () {
            //                    //$route.reload();
            //                    $scope.modalInstanceLoad.close();
            //                    window.location = "/members";
            //                });
            //            }
            //        });
        }

    });
