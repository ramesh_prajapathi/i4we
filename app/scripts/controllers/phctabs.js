'use strict';

angular.module('secondarySalesApp')
    .controller('PHCTABSCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {

        $scope.TotalRoles = [];

        Restangular.all('roles?filter[where][deleteFlag]=false').getList().then(function (role) {
            $scope.roles = role;

            angular.forEach($scope.roles, function (member, index) {
                member.index = index + 1;

                $scope.TotalRoles = [];
                $scope.TotalRoles.push(member);
            });
        });

    });