'use strict';

angular.module('secondarySalesApp')
	.controller('EventTypesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/eventtypes/create' || $location.path() === '/eventtypes/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/eventtypes/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/eventtypes/create' || $location.path() === '/eventtypes/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/eventtypes/create' || $location.path() === '/eventtypes/' + $routeParams.id;
			return visible;
		};

		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			$scope.mypillarId = $window.sessionStorage.myRoute;
		}

		if ($window.sessionStorage.prviousLocation != "partials/eventtypes") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};

		
		/*********/
		if ($routeParams.id) {
			$scope.message = 'Event type has been Updated!';
			Restangular.one('eventtypes', $routeParams.id).get().then(function (eventtype) {
				$scope.original = eventtype;
				$scope.eventtype = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Event type has been Created!';
		}

		$scope.searchEventType = $scope.name;
		//$scope.mypillars = Restangular.all('pillars').getList().$object;
		$scope.zn = Restangular.all('pillars?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.pillars = zn;
			$scope.mypillarId = $window.sessionStorage.myRoute;
		});
		
		$scope.getPillar = function (pillarId) {
			return Restangular.one('pillars', pillarId).get().$object;
		};


		/**************************************** INDEX *******************************************
		$scope.zn = Restangular.all('eventtypes?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.eventtypes = zn;
			angular.forEach($scope.eventtypes, function (member, index) {
				member.index = index + 1;
			});
		});
		*/
		$scope.eventtypes = {};
		$scope.mypillarId = '';
		$scope.zonalid = '';
		$scope.$watch('mypillarId', function (newValue, oldValue) {
			if (newValue === oldValue) {
				return;
			} else {
				$window.sessionStorage.myRoute = newValue;
				$scope.mypillarId = $window.sessionStorage.myRoute;
				$scope.sal = Restangular.all('eventtypes?filter[where][pillarid]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (sal) {
					$scope.eventtypes = sal;
					angular.forEach($scope.eventtypes, function (member, index) {
						member.index = index + 1;
					});
				});
			}
		});
		/*************************** SAVE *******************************************/
		$scope.eventtype = {
			name: '',
			deleteflag: false
		};

		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.SaveEventType = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.eventtype.name == '' || $scope.eventtype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.eventtype.pillarid == '' || $scope.eventtype.pillarid == null) {
				$scope.validatestring = $scope.validatestring + 'Please select a pillar';

			} else if ($scope.eventtype.hnname == '' || $scope.eventtype.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.eventtype.knname == '' || $scope.eventtype.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.eventtype.taname == '' || $scope.eventtype.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.eventtype.tename == '' || $scope.eventtype.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.eventtype.mrname == '' || $scope.eventtype.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.all('eventtypes').post($scope.eventtype).then(function () {
					window.location = '/eventtypes';
				});
			}
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

		/****************************** UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.UpdateEventType = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.eventtype.name == '' || $scope.eventtype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.eventtype.pillarid == '' || $scope.eventtype.pillarid == null) {
				$scope.validatestring = $scope.validatestring + 'Please select a pillar';

			} else if ($scope.eventtype.hnname == '' || $scope.eventtype.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.eventtype.knname == '' || $scope.eventtype.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.eventtype.taname == '' || $scope.eventtype.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.eventtype.tename == '' || $scope.eventtype.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.eventtype.mrname == '' || $scope.eventtype.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter eventtype name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.one('eventtypes').customPUT($scope.eventtype).then(function () {
					console.log('EventType Saved');
					window.location = '/eventtypes';
				});
			}
		};
		/************************************* DELETE *******************************************/

		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('eventtypes/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}
	});
