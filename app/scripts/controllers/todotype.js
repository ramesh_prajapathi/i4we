'use strict';

angular.module('secondarySalesApp')
	.controller('ToDoTypesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/todotypes/create' || $location.path() === '/todotypes/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/todotypes/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/todotypes/create' || $location.path() === '/todotypes/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/todotypes/create' || $location.path() === '/todotypes/' + $routeParams.id;
			return visible;
		};

		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
		}

		if ($window.sessionStorage.prviousLocation != "partials/todotype") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}
	
		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};

		
		/*********/

		//  $scope.todotypes = Restangular.all('todotypes').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'Todo type has been Updated!';
			Restangular.one('todotypes', $routeParams.id).get().then(function (todotype) {
				$scope.original = todotype;
				$scope.todotype = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'todo type has been Updated!';
		}
		$scope.searchToDoType = $scope.name;
		//$scope.pillars = Restangular.all('pillars').getList().$object;
		$scope.zn = Restangular.all('pillars?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.pillars = zn;
			$scope.mypillarId = $window.sessionStorage.myRoute;
		});

		$scope.getPillar = function (pillarId) {
			return Restangular.one('pillars', pillarId).get().$object;
		};

		/************************* INDEX *******************************************/
		/*$scope.zn = Restangular.all('todotypes?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.todotypes = zn;
			angular.forEach($scope.todotypes, function (member, index) {
				member.index = index + 1;
			});
		});*/
		$scope.todotypes = {};
		$scope.mypillarId = '';
		$scope.zonalid = '';
		$scope.$watch('mypillarId', function (newValue, oldValue) {
			console.log('newValue',newValue);
			if (newValue === oldValue) {
				return;
			} else if (newValue === 'Others') {
				$scope.sal = Restangular.all('todotypes?filter[where][pillarid]=null' + '&filter[where][deleteflag]=false').getList().then(function (sal) {
					$scope.todotypes = sal;
					angular.forEach($scope.todotypes, function (member, index) {
						member.index = index + 1;
					});
				});
			} else {
				$window.sessionStorage.myRoute = newValue;
				$scope.mypillarId = $window.sessionStorage.myRoute;
				$scope.sal = Restangular.all('todotypes?filter[where][pillarid]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (sal) {
					$scope.todotypes = sal;
					angular.forEach($scope.todotypes, function (member, index) {
						member.index = index + 1;
					});
				});
			}
		});

		/************************** SAVE *******************************************/
		$scope.todotype = {
			name: '',
			deleteflag: false
		};

		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.SaveToDoType = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.todotype.name == '' || $scope.todotype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.todotype.pillarid == '' || $scope.todotype.pillarid == null) {
				$scope.validatestring = $scope.validatestring + 'Please select a pillar';

			} else if ($scope.todotype.hnname == '' || $scope.todotype.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.todotype.knname == '' || $scope.todotype.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.todotype.taname == '' || $scope.todotype.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.todotype.tename == '' || $scope.todotype.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.todotype.mrname == '' || $scope.todotype.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.todotypes.post($scope.todotype).then(function () {
					console.log('ToDoType Saved');
					window.location = '/todotypes';
				});
			}
		};
		/************************ UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.UpdateToDoType = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.todotype.name == '' || $scope.todotype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.todotype.pillarid == '' || $scope.todotype.pillarid == null) {
				$scope.validatestring = $scope.validatestring + 'Please select a pillar';

			} else if ($scope.todotype.hnname == '' || $scope.todotype.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.todotype.knname == '' || $scope.todotype.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.todotype.taname == '' || $scope.todotype.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.todotype.tename == '' || $scope.todotype.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.todotype.mrname == '' || $scope.todotype.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter event type name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.todotypes.customPUT($scope.todotype).then(function () {
					console.log('ToDoType Saved');
					window.location = '/todotypes';
				});
			}
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/************************* DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('todotypes/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

	});
