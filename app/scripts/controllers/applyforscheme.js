'use strict';
angular.module('secondarySalesApp').directive('validationmodal', function () {
    return {
         
        template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-danger modal-sm" style="position:absolute;left:25px;bottom:0px;" >' + '<div class="modal-content">' + '<div class="row" style="margin:0px;background:#b30d55;width:370px;" >' + '<div class="col-xs-2">' + '<img src="images/piclinks/alert.png" style="padding-top: 2em;padding-left: .5em;">' + '</div>' + '<div class="col-xs-8" style="padding:0px;" >' + '<div class="modal-header" style="padding-left:15px;padding-bottom:0px;" >' + '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="modal-body" style="padding-top:5px"  ng-transclude>' + '</div>' + '</div>' + '<div class="col-xs-1 " style="float:right" >' + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title1 = attrs.title1;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });
            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });
            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
}).controller('ApplyForSchemeCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $timeout, $modal) {
    //    if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
    //        window.location = "/";
    //    }

    $scope.UserLanguage = $window.sessionStorage.language;

    $scope.SchemeLanguage = {};

    Restangular.one('schemelanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.SchemeLanguage = langResponse[0];
        $scope.AFSHeading = $scope.SchemeLanguage.schemeCreate;
        // $scope.modalTitle = langResponse[0].thankYou;
    });

    if ($window.sessionStorage.language == 1) {
        $scope.modalTitle = 'Thank You';
    } else if ($window.sessionStorage.language == 2) {
        $scope.modalTitle = 'धन्यवाद';
    } else if ($window.sessionStorage.language == 3) {
        $scope.modalTitle = 'ಧನ್ಯವಾದ';
    } else if ($window.sessionStorage.language == 4) {
        $scope.modalTitle = 'நன்றி';
    }

    $scope.HideCreateButton = false;

    $scope.schememaster = {
        createdDate: new Date(),
        createdBy: $window.sessionStorage.userId,
        createdByRole: $window.sessionStorage.roleId,
        lastModifiedDate: new Date(),
        lastModifiedBy: $window.sessionStorage.userId,
        lastModifiedByRole: $window.sessionStorage.roleId,
        countryId: $window.sessionStorage.countryId,
        stateId: $window.sessionStorage.stateId,
        districtId: $window.sessionStorage.districtId,
        siteId: $window.sessionStorage.siteId.split(",")[0],
        deleteFlag: false,
        servicefee: 0,
        memberId: []
    };

    $scope.schememaster.memberId.push($window.sessionStorage.fullName);

    $scope.auditlog = {
        action: 'Insert',
        module: 'Apply for Scheme',
        owner: $window.sessionStorage.userId,
        datetime: new Date(),
        details: 'Apply for Scheme Inserted',
        countryId: $window.sessionStorage.countryId,
        stateId: $window.sessionStorage.stateId,
        districtId: $window.sessionStorage.districtId,
        siteId: $window.sessionStorage.siteId.split(",")[0]
    };

    /****************************************** Member *******************************************/

    if ($window.sessionStorage.roleId + "" === "3") {

        $scope.hideAssigned = true;

        Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
            $scope.users = urs;
            $scope.schememaster.associatedHF = $window.sessionStorage.userId;

        });
    } else {

        $scope.hideAssigned = false;

        Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
            $scope.users = urs;
            $scope.schememaster.associatedHF = urs[0].id;

        });

    }

    // $scope.beneficiaries = Restangular.all('members').getList().$object;
    if ($window.sessionStorage.roleId + "" === "3") {
        $scope.memberFilterCall = 'members?filter[where][deleteFlag]=false' + '&filter[where][associatedHF]=' + $window.sessionStorage.userId;
    } else {
        $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
    }
    Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
        $scope.beneficiaries = mems;

        angular.forEach($scope.beneficiaries, function (member, index) {
            member.index = index;
        });
    });

    /***********************************************************************/
    $scope.schemestages = Restangular.all('schemestages?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

    $scope.printschemes = Restangular.all('schemes?filter[where][deleteFlag]=false&filter[where][schemeState]=' + $window.sessionStorage.stateId + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

    $scope.responcedreceived = Restangular.all('responsereceived?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

    $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

    $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
    //$scope.noofmonthrepaid = Restangular.all('noofmonthrepay?filter[where][deleteFlag]=false').getList().$object;



    $scope.toggleValidation = function () {
        console.log('toggleValidation');
        $scope.showValidation = !$scope.showValidation;
    };
    //$scope.message = 'Scheme has been applied!';
    //$scope.loanapplied = false;

    /**********************************************************************************/

    $scope.mygenderstatus = false;
    $scope.myoccupationstatus = false;
    $scope.mysocialstatus = false;
    $scope.myagestatus = false;
    $scope.myhealthstatus = false;
    $scope.loanapplied = false;

    /******************************************************NEW *****************************************/



    $scope.$watch('schememaster.memberId', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            Restangular.one('members/findOne?filter[where][id]=' + newValue).get().then(function (mem) {
                $scope.selectedMember = mem;
            });
        }
    });

    $scope.$watch('schememaster.schemeId', function (newValue, oldValue) {
        console.log('schemeId', newValue);
        if (newValue == oldValue || newValue == undefined) {
            return;
        } else {

        }
    });


    /******************************************************* Save ************************/
    $scope.validatestring = '';
    $scope.applyforschemedataModal = false;
    $scope.documentdataModal = false;
    $scope.CreateClicked = false;

    $scope.Save = function () {

        if ($scope.schememaster.memberId == '' || $scope.schememaster.memberId == null) {
            $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectMember; //'Please Select a Member';

        } else if ($scope.schememaster.associatedHF == '' || $scope.schememaster.associatedHF == null) {
            $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectAssignedTo;

        } else if ($scope.schememaster.schemeId == '' || $scope.schememaster.schemeId == null) {
            $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectScheme;

        } else if ($scope.schememaster.stage == '' || $scope.schememaster.stage == null) {
            $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectStage;

        } else if ($scope.schememaster.datetime == '' || $scope.schememaster.datetime == null) {
            $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectFollowUp;

        } else if ($scope.schememaster.stage == 4) {
            if ($scope.schememaster.responserecieve == '' || $scope.schememaster.responserecieve == null) {
                $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectResponseReceived;
            } else if ($scope.schememaster.responserecieve == 2) {
                if ($scope.schememaster.rejection == '' || $scope.schememaster.rejection == null) {
                    $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectReasonForRejection;
                }
            }

        } else if ($scope.schememaster.stage == 5) {
            if ($scope.schememaster.delay == '' || $scope.schememaster.delay == null) {
                $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectReasonForDelay;
            }
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            $scope.toggleLoading();
            $scope.message = $scope.SchemeLanguage.thankYouCreated;
            //  $scope.schememaster.associatedHF = $scope.selectedMember.associatedHF;
            Restangular.all('schememasters').post($scope.schememaster).then(function (resp) {

                $scope.auditlog.rowId = resp.id;
                $scope.auditlog.details = $scope.selectedMember.individualId;
                $scope.auditlog.memberId = $scope.selectedMember.id;
                $scope.auditlog.conditionId = resp.schemeId;
                $scope.auditlog.associatedHF = resp.associatedHF;

                Restangular.all('audittrials').post($scope.auditlog).then(function (respAudit) {
                    $scope.modalInstanceLoad.close();
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('reloading...');

                    setInterval(function () {
                        window.location = '/applyforschemes';
                    }, 1500);
                });
            }, function (error) {
                console.log("error", error);
                if (error.data.error.constraint === "unique_constraint_memberid_schemeid") {
                    $scope.modalInstanceLoad.close();
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.SchemeLanguage.schemeAlreadyAdded;
                }
            });
        }
    };


    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };

    var sevendays = new Date();
    // sevendays.setDate(sevendays.getDate() + 7);
    $scope.schememaster.datetime = sevendays
    //Datepicker settings start
    $scope.today = function () {};
    $scope.today();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.dtmin = new Date();
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.openstart = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerstart' + index).focus();
        });
        $scope.start.openedstart = true;
    };
    $scope.openstart1 = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerstart' + index).focus();
        });
        $scope.start.openedstart = true;
    };

    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };

    $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end
    $scope.datetimehide = true;
    $scope.reponserec = true;
    $scope.delaydis = true;
    $scope.collectedrequired = true;
    $scope.purposeofloanhide = true;

    $scope.$watch('schememaster.stage', function (newValue, oldValue) {
        console.log('schememaster.stage', newValue);
        //console.log('$scope.loanapplied', $scope.loanapplied);
        var fifteendays = new Date();
        fifteendays.setDate(fifteendays.getDate() + 15);
        var sevendays = new Date();

        sevendays.setDate(sevendays.getDate() + 7);
        if (newValue === oldValue) {
            return;
        } else if (newValue === '4') {
            $scope.reponserec = false;
            $scope.delaydis = true;
            $scope.datetimehide = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '5') {
            $scope.delaydis = false;
            $scope.reponserec = true;
            $scope.datetimehide = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.datetime = fifteendays;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '2') {
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.datetimehide = true;
            $scope.collectedrequired = false;
            $scope.rejectdis = true;
            $scope.schememaster.datetime = fifteendays;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '3') {
            $scope.schememaster.datetime = fifteendays;
            $scope.datetimehide = true;
            $scope.collectedrequired = true;
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '7') {
            var sixmonth = new Date();
            sixmonth.setDate(sixmonth.getDate() + 180);
            $scope.schememaster.datetime = sixmonth;
            $scope.datetimehide = false;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.delaydis = true;
            $scope.reponserec = true;
        } else if (newValue === '1') {
            $scope.schememaster.datetime = fifteendays;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '8') {
            $scope.schememaster.datetime = sevendays;
            $scope.datetimehide = true;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '6') {
            $scope.datetimehide = false;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '9') {
            //$scope.datetimehide = true;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.schememaster.datetime = new Date();
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else {
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.purposeofloanhide = true;
            $scope.schememaster.responserecieve = null;
            $scope.schememaster.delay = null;
            //$scope.schememaster.datetime = '';
            $scope.datetimehide = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        }
    });

    $scope.rejectdis = true;

    $scope.$watch('schememaster.responserecieve', function (newValue, oldValue) {
        console.log('responserecieve', newValue);
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        if (newValue === oldValue || newValue === null) {
            return;
        } else if (newValue === '1') {
            var sixmonth = new Date();
            sixmonth.setDate(sixmonth.getDate() + 180);
            $scope.schememaster.datetime = sixmonth;
            $scope.rejectdis = true;
        } else if (newValue === '2') {
            $scope.rejectdis = false;
            var fifteendays = new Date();
            fifteendays.setDate(fifteendays.getDate() + 15);
            var sevendays = new Date();
            $scope.schememaster.datetime = fifteendays;
            $scope.datetimehide = true;
        } else {
            $scope.schememaster.datetime = sevendays;
            $scope.rejectdis = true;
            $scope.schememaster.rejection = null;
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.schememaster.responserecieve = null;
            $scope.schememaster.delay = null;
            //$scope.schememaster.datetime = '';
            $scope.datetimehide = false;
        }
    });

    $scope.TestamountTaken = function (id) {
        $scope.schememaster.noofmonthrepay = '';
    }


});
