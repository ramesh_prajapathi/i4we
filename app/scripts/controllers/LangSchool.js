'use strict';

angular.module('secondarySalesApp')
    .controller('LangSchoolCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
    
    
    $scope.HideCreateButton = true;
        $scope.langdisable = false;

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        $scope.$watch('school.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                
                
                Restangular.one('schoollanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.school = Restangular.copy($scope.original);
                    });
                
                Restangular.all('schoollanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                        $scope.HideCreateButton = true;
                    } else {
                        // $scope.LangId = response[0].id;
                        //$scope.HideSubmitButton = false;
                        //$scope.langdisable = true;

                        //$scope.reportincident = response[0];
                        // console.log('$scope.reportincident', $scope.reportincident);
                        $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';
                    }
                });

            }
        });
    
                    /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/Langschool-list';

        };
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/

        $scope.school = {
            deleteFlag: false,
            lastModifiedDate: new Date()
        };

        $scope.Save = function () {
            Restangular.all('schoollanguages').post($scope.school).then(function (schoolResponse) {
                console.log('schoolResponse', schoolResponse);
                window.location = '/Langschool-list';
            });

        };

        $scope.Update = function () {
            Restangular.one('schoollanguages', $routeParams.id).customPUT($scope.school).then(function (school) {
                console.log('school', school);
                window.location = '/Langschool-list';
            });
        };

        if ($routeParams.id) {

            $scope.HideCreateButton = false;
            $scope.langdisable = true;
            Restangular.one('schoollanguages', $routeParams.id).get().then(function (school) {
                $scope.original = school;
                $scope.school = Restangular.copy($scope.original);
            });
        }
    
});