'use strict';

angular.module('secondarySalesApp')
    .controller('LangAFDo1Ctrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/


        $scope.HideCreateButton = true;
        $scope.langdisable = false;

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        $scope.$watch('afdoc.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                
                 Restangular.one('documentLanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.afdoc = Restangular.copy($scope.original);
                    });
                Restangular.all('documentLanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                        $scope.HideCreateButton = true;
                    } else {
                        //$scope.LangId = response[0].id;
                       // $scope.HideCreateButton = false;
                      //  $scope.langdisable = true;

                        //  $scope.afdoc = response[0];
                        //  console.log('$scope.afdoc', $scope.afdoc);
                        $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';

                    }
                });

            }
        });
    
        
    /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/Langapplyfordoclist';

        };
    
    
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/

        $scope.afdoc = {
            deleteFlag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.UserEmployeeId
        };




        $scope.Save = function () {
            Restangular.all('documentLanguages').post($scope.afdoc).then(function (docResponse) {
                console.log('docResponse', docResponse);
                window.location = '/Langapplyfordoclist';
            });

        };

        $scope.Update = function () {
            Restangular.one('documentLanguages', $routeParams.id).customPUT($scope.afdoc).then(function (docResponse) {
                console.log('docResponse', docResponse);
                window.location = '/Langapplyfordoclist';
            });
        };

        if ($routeParams.id) {

            $scope.HideCreateButton = false;
            $scope.langdisable = true;
            Restangular.one('documentLanguages', $routeParams.id).get().then(function (afdoc) {
                $scope.original = afdoc;
                $scope.afdoc = Restangular.copy($scope.original);
            });
        }


    });
