'use strict';

angular.module('secondarySalesApp')
    .controller('MyToDoStatusesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}
        $scope.showForm = function () {
            var visible = $location.path() === '/todofilters/create' || $location.path() === '/todofilters/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/todofilters/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/todofilters/create' || $location.path() === '/todofilters/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/todofilters/create' || $location.path() === '/todofilters/' + $routeParams.id;
            return visible;
        };


        /*********/
/*********************************** Pagination *******************************************/

		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}

		
		if ($window.sessionStorage.prviousLocation != "partials/mytodostatuses") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}
	
		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};


        if ($routeParams.id) {
            Restangular.one('duetodostatuses', $routeParams.id).get().then(function (duetodostatus) {
                $scope.original = duetodostatus;
                $scope.duetodostatus = Restangular.copy($scope.original);
            });
        }
        $scope.searchduetodostatus = $scope.name;
        $scope.pillars = Restangular.all('pillars').getList().$object;

        /************************************************************************** INDEX *******************************************/
        $scope.zn = Restangular.all('duetodostatuses').getList().then(function (zn) {
            $scope.duetodostatuses = zn;
            angular.forEach($scope.duetodostatuses, function (member, index) {
                member.index = index + 1;
            });
        });

        /*************************************************************************** SAVE *******************************************/
        $scope.validatestring = '';
        $scope.Saveduetodostatus = function () {

            $scope.duetodostatuses.post($scope.duetodostatus).then(function () {
                console.log('duetodostatus Saved');
                window.location = '/todofilters';
            });
        };
        /*************************************************************************** UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.Updateduetodostatus = function () {
            document.getElementById('name').style.border = "";
            if ($scope.duetodostatus.name == '' || $scope.duetodostatus.name == null) {
                $scope.duetodostatus.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your duetodostatus name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.duetodostatuses.customPUT($scope.duetodostatus).then(function () {
                    console.log('duetodostatus Saved');
                    window.location = '/todofilters';
                });
            }


        };
        /********************************* DELETE *******************************************/
   
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('duetodostatuses/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}


        $scope.getPillar = function (pillarId) {
            return Restangular.one('pillars', pillarId).get().$object;
        };

    });