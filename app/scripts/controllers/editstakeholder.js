'use strict';
angular.module('secondarySalesApp').controller('EditStakeHolderCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter, $modal) {
    /*********/
    if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        window.location = "/";
    }
    $scope.showForm = function () {
        var visible = $location.path() === '/stakeholdermeetings/create' || $location.path() === '/stakeholdermeetings/' + $routeParams.id;
        return visible;
    };
    $scope.isCreateView = function () {
        if ($scope.showForm()) {
            var visible = $location.path() === '/stakeholdermeetings/create';
            return visible;
        }
    };
    $scope.hideCreateButton = function () {
        var visible = $location.path() === '/stakeholdermeetings/create' || $location.path() === '/stakeholdermeetings/' + $routeParams.id;
        return visible;
    };
    $scope.hideSearchFilter = function () {
        var visible = $location.path() === '/stakeholdermeetings/create' || $location.path() === '/stakeholdermeetings/' + $routeParams.id;
        return visible;
    };
    /*************************************************************/
    $scope.modalTitle = 'Thank You';
    $scope.message = 'Stake-Holder Meeting has been updated!';
    $scope.auditlog = {
        modifiedbyroleid: $window.sessionStorage.roleId
        , modifiedby: $window.sessionStorage.UserEmployeeId
        , lastmodifiedtime: new Date()
        , entityroleid: 42
        , state: $window.sessionStorage.zoneId
        , district: $window.sessionStorage.salesAreaId
        , facility: $window.sessionStorage.coorgId
    };
    $scope.stakeholder = {
        follow: []
        , state: $window.sessionStorage.zoneId
        , //district: $window.sessionStorage.salesAreaId,
        facility: $window.sessionStorage.coorgId
        , lastmodifiedby: $window.sessionStorage.UserEmployeeId
        , lastmodifiedtime: new Date()
        , deleteflag: false
    };
    $scope.UserLanguage = $window.sessionStorage.language;
    $scope.organisebystakeholders = Restangular.all('organisebystakeholders?filter[where][deleteflag]=false').getList().$object;
    $scope.todostatuses1 = Restangular.all('todostatuses?filter={"where": {"id": {"inq": [1,3]}}}').getList().then(function (responseseservity) {
        $scope.todostatuses = responseseservity;
        $scope.newtodo.status = 1;
    });
    $scope.districts = Restangular.all('sales-areas?filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][deleteflag]=false').getList().$object;
    $scope.stakeholdertypes = Restangular.all('stakeholdertypes').getList().$object;
    $scope.purposeofmeetings = Restangular.all('purposeofmeetings').getList().$object;
    $scope.followupstacks = Restangular.all('followupstacks').getList().$object;
    $scope.validatestring = '';
    $scope.Updatestakeholder = function () {
        //$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.stakeholder.meetingpurpose = null;
        $scope.stakeholder.stakeholdertype = null;
        
        if ($scope.stakeholder.datetime == '' || $scope.stakeholder.datetime == null) {
            $scope.validatestring = $scope.validatestring + $scope.seldate;
        }
        else if ($scope.stakeholder.stakeholdertyp == '' || $scope.stakeholder.stakeholdertyp == null || $scope.stakeholder.stakeholdertyp == undefined) {
            $scope.validatestring = $scope.validatestring + $scope.pstakeholdtype;
        }
        if ($scope.stakeholder.stakeholdertyp != undefined) {
            for (var i = 0; i < $scope.stakeholder.stakeholdertyp.length; i++) {
                if (i == 0) {
                    $scope.stakeholder.stakeholdertype = $scope.stakeholder.stakeholdertyp[i];
                }
                else {
                    $scope.stakeholder.stakeholdertype = $scope.stakeholder.stakeholdertype + ',' + $scope.stakeholder.stakeholdertyp[i];
                }
            }
        }
        if ($scope.stakeholder.meeting != undefined) {
            for (var i = 0; i < $scope.stakeholder.meeting.length; i++) {
                if (i == 0) {
                    $scope.stakeholder.meetingpurpose = $scope.stakeholder.meeting[i];
                }
                else {
                    $scope.stakeholder.meetingpurpose = $scope.stakeholder.meetingpurpose + ',' + $scope.stakeholder.meeting[i];
                }
            }
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
            //$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        }
        else {
            $scope.openOneAlert();
            Restangular.one('stakeholders', $routeParams.id).customPUT($scope.stakeholder).then(function (resp) {
                $scope.auditlog.entityid = resp.id;
                var respdate = $filter('date')(resp.datetime, 'dd-MMMM-yyyy');
                $scope.auditlog.description = 'Stake Holder Updated With Following Details:' + 'Stakeholder Type - ' + resp.stakeholdertype + ' , ' + 'Purpose of meeting - ' + resp.meetingpurpose + ' , ' + 'Site - ' + resp.site + ' , ' + 'Organized By - ' + resp.organisedby + ' , ' + 'Follow Up - ' + resp.follow + ' , ' + 'Date - ' + respdate;
                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                    window.location = '/stakeholdermeetings';
                });
            });
        }
    };
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    $scope.DisableFollowup = true;
    Restangular.one('stakeholders', $routeParams.id).get().then(function (stakeholder) {
        $scope.original = stakeholder;
        if ($window.sessionStorage.roleId == 5) {
            $scope.disableapprove = false;
            $scope.cosite = false;
            $scope.fieldworkersite = true;
            //Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
            Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {
                //$scope.routes = Restangular.all('distribution-routes?filter[where][partnerId]=' + employee.id).getList().$object;
            });
            setTimeout(function () {
                $scope.stakeholder = Restangular.copy($scope.original);
                if (stakeholder.stakeholdertype != null) {
                    $scope.stakeholder.stakeholdertyp = stakeholder.stakeholdertype.split(",");
                }
                if (stakeholder.meetingpurpose != null) {
                    $scope.stakeholder.meeting = stakeholder.meetingpurpose.split(",");
                }
                if (stakeholder.followup != null) {
                    $scope.stakeholder.follow = stakeholder.followup.split(",");
                }
                else {
                    $scope.stakeholder.follow = [];
                }
                $scope.modalInstanceLoad.close();
            }, 2000);
        }
        else {
            $scope.fieldworkersite = false;
            $scope.disableapprove = true;
            $scope.cosite = true;
            Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                $scope.getsiteid = fw.id;
                console.log('$scope.getsiteid', $scope.getsiteid);
                Restangular.all('routelinks?filter[where][partnerId]=' + $scope.getsiteid).getList().then(function (routeResponse) {
                    $scope.routelinksget = routeResponse;
                    angular.forEach($scope.routelinksget, function (member, index) {
                        member.index = index + 1;
                        member.routeName = Restangular.one('distribution-routes', member.distributionRouteId).get().$object;
                    });
                    console.log('routeResponse', routeResponse);
                    $scope.getrouteid = routeResponse.distributionRouteId;
                    console.log('$scope.getrouteid', $scope.getrouteid);
                    $scope.modalInstanceLoad.close();
                });
                setTimeout(function () {
                    $scope.stakeholder = Restangular.copy($scope.original);
                    if (stakeholder.stakeholdertype != null) {
                        $scope.stakeholder.stakeholdertyp = stakeholder.stakeholdertype.split(",");
                    }
                    if (stakeholder.meetingpurpose != null) {
                        $scope.stakeholder.meeting = stakeholder.meetingpurpose.split(",");
                    }
                    if (stakeholder.followup != null) {
                        $scope.stakeholder.follow = stakeholder.followup.split(",");
                    }
                    else {
                        $scope.stakeholder.follow = [];
                    }
                    $scope.modalInstanceLoad.close();
                }, 2000);
            });
        }
    });
    $scope.$watch('stakeholder.district', function (newValue, oldValue) {
        //console.log('newValue', newValue);
        if (newValue === oldValue || newValue == '') {
            return;
        }
        else {
            //$scope.towns = Restangular.all('cities?filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').$object;
            $scope.towns1 = Restangular.all('cities?filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (resp) {
                //console.log('resp', resp);
                $scope.towns = resp;
            });
        }
    });
    //Datepicker settings start
    $scope.stakeholder.datetime = new Date();
    //$scope.newtodo.datetime = $filter('date')(new Date(), 'dd-MMMM-yyyy');
    $scope.today = function () {
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        $scope.newtodo = {
            datetime: sevendays
        };
    };
    $scope.today();
    $scope.dtmax = new Date();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.CurrentPast = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickercurpast' + index).focus();
        });
        $scope.picker.currpastopened = true;
    };
    $scope.followdt = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerfollowup' + index).focus();
        });
        $scope.followupopened = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy'
        , 'starting-day': 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end
    
      $scope.openOneAlert = function () {
         console.log('Me Calling')
         $scope.modalOneAlert = $modal.open({
             animation: true
             , templateUrl: 'template/AlertModal.html'
             , scope: $scope
             , backdrop: 'static'
             , keyboard: false
             , size: 'sm'
             , windowClass: 'modal-sucess '
         });
     };
    /***************************** Language *******************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.date = langResponse.date;
        $scope.name = langResponse.name;
        $scope.action = langResponse.action;
        $scope.searchmember = langResponse.searchmember;
        $scope.organizedby = langResponse.organizedby;
        $scope.followuprequired = langResponse.followuprequired;
        $scope.membername = langResponse.membername;
        $scope.status = langResponse.status;
        $scope.followupdate = langResponse.followupdate;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.site = langResponse.site;
        $scope.stakeholdertype = langResponse.stakeholdertype;
        $scope.stakeholdermeeting = langResponse.stakeholdermeeting;
        $scope.show = langResponse.show;
        $scope.entry = langResponse.entry;
        $scope.searchfor = langResponse.searchfor;
        $scope.addnew = langResponse.addnew;
        $scope.purposeofmeeting = langResponse.purposeofmeeting;
        $scope.district = langResponse.district;
        $scope.town = langResponse.town;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
        
        //$scope.stakeholdcreated = langResponse.stakeholdcreated;
        //$scope.stakeholdupdated = langResponse.stakeholdupdated;
        $scope.modalTitle = langResponse.thankyou;
        $scope.message = langResponse.stakeholdupdated;
        $scope.pstakeholdtype = langResponse.stakeholdtype;
        $scope.seldisctict = langResponse.seldisctict;
        $scope.selorgby = langResponse.selorgby;
        $scope.seldate = langResponse.seldate;
    });
});
/************************************ Not in Use ***********************************
		$scope.getSite = function (site) {
			return Restangular.one('distribution-routes', site).get().$object;
		};

		$scope.submitdtakeholders = Restangular.all('stakeholders').getList().$object;
		$scope.submittodos = Restangular.all('todos').getList().$object;
		
		
		
		$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;


			$scope.$watch('stakeholder.stakeholdertype', function (newValue, oldValue) {
			$scope.newFollow = newValue;
			if (newValue === oldValue) {
				return;
			} else {
				$scope.newtodo.stakeholdertype = newValue;
			}
		});
	
				$scope.$watch('stakeholder.meeting', function (newValue, oldValue) {
					$scope.newFollow = newValue;
					if (newValue === oldValue) {
						return;
					} else {
						$scope.newtodo.purpose = newValue;
						console.log('$scope.newtodo.purpose', $scope.newtodo.purpose);
					}
				});
			


		$scope.getStake = function (stakeholdertype) {
			return Restangular.one('stakeholdertypes', stakeholdertype).get().$object;
		};

	$scope.searchstakeholder = $scope.name;

	
		
	*********************************************************************************/