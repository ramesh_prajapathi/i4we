'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {

        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 9) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/screentreatlist/create' || $location.path() === '/screentreatlist/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/screentreatlist/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/screentreatlist/create' || $location.path() === '/screentreatlist/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/screentreatlist/create' || $location.path() === '/screentreatlist/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);

        if ($window.sessionStorage.prviousLocation != "partials/condition") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.showenglishLang = true;
        $scope.OtherLang = true;

        $scope.$watch('condition.name', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id) {
                return;
            } else {
                angular.forEach($scope.rowArray, function (member, index) {
                    member.index = index;
                    member.name = newValue;
                });
            }
        });

        $scope.$watch('condition.language', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }
                //                Restangular.one('genders?filter[where][deleteFlag]=false&filter[where][language]='+newValue).get().then(function(gen){
                //                    if(gen.lenth>0){
                //                        alert("Already Exists");
                //                    }
                //                    
                //                })
            }
        });

        $scope.$watch('condition.parentId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.one('conditions', newValue).get().then(function (sts) {
                    $scope.condition.orderNo = sts.orderNo;
                    $scope.condition.enabledFor = sts.enabledFor;
                    $scope.condition.enabled = sts.enabled.split(',');
                });
            }
        });
        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {

            $scope.message = 'Condition has been Updated!';

            Restangular.one('conditions', $routeParams.id).get().then(function (condition) {
                $scope.original = condition;
                $scope.condition = Restangular.copy($scope.original);
                $scope.condition.enabled = $scope.condition.enabled.split(',');
            });

            Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablangs) {
                $scope.tablanguages1 = tablangs;
                $scope.tablanguages1.splice(0, 1);
                $scope.currValue = 0;

                Restangular.all('conditions?filter[where][deleteFlag]=false&filter[where][parentId]=' + $routeParams.id + '&filter[where][parentFlag]=false').getList().then(function (intvls1) {
                    $scope.mtlangs1 = intvls1;

                    angular.forEach($scope.tablanguages1, function (member, index) {
                        member.index = index;

                        var data = $scope.mtlangs1.filter(function (arr) {
                            return arr.language == member.id
                        })[0];

                        if (data == undefined) {
                            $scope.rowArray.push({
                                language: member.id,
                                id: 0
                            });
                        } else {
                            $scope.rowArray.push(data);
                        }
                    });
                });
            });

        } else {
            $scope.message = 'Condition has been Created!';
        }

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];

        Restangular.all('conditionsteps?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (cstp) {
            $scope.conditionsteps = cstp;
        });

        /******************************** INDEX *******************************************/
        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (zn) {
            $scope.languages = zn;
        });

        Restangular.all('conditions?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (cns) {
            $scope.conditions = cns;
            angular.forEach($scope.conditions, function (member, index) {
                member.index = index + 1;

                Restangular.one('languages', member.language).get().then(function (lng) {
                    member.langname = lng.name;
                });
            });
        });

        Restangular.all('conditions?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.englishconditions = zn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        $scope.rowArray = [];

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablang) {
            $scope.tablanguages = tablang;
            $scope.tablanguages.splice(0, 1);
            $scope.currValue = 0;

            angular.forEach($scope.tablanguages, function (member, index) {
                member.index = index;

                if (!$routeParams.id) {
                    $scope.rowArray.push({
                        language: member.id,
                        name: ''
                    });
                }

                if (member.index == 0) {
                    member.active = true;
                    member.visible = true;
                } else {
                    member.active = false;
                    member.visible = false;
                }
            });
        });

        $scope.tabClick = function (row) {
            $scope.tablanguages[row.index].visible = true;
            if (row.index != 0) {
                $scope.tablanguages[0].visible = false;
            }
            $scope.currValue = row.index;
        };

        /********************************************* SAVE *******************************************/

        $scope.condition = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteFlag: false,
            language: 1
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {

            document.getElementById('name').style.border = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.condition.language == 1) {

                if ($scope.condition.name == '' || $scope.condition.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Condition';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.condition.orderNo == '' || $scope.condition.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                }

            } else if ($scope.condition.language != 1) {

                if ($scope.condition.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition English';
                } else if ($scope.condition.name == '' || $scope.condition.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Condition';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.condition.orderNo == '' || $scope.condition.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.condition.parentId === '') {
                    delete $scope.condition['parentId'];
                }

                $scope.submitDisable = true;

                $scope.condition.parentFlag = $scope.showenglishLang;

                Restangular.all('conditions').post($scope.condition).then(function (response) {
                    $scope.langFunc(response.id, response.orderNo, response.enabledFor, response.enabled);
                }, function (error) {
                    if (error.data.error.constraint === 'condition_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.langcount = 0;

        $scope.langFunc = function (intervalId, orderNo, enabledFor, enabled) {

            if ($scope.langcount < $scope.rowArray.length) {

                $scope.rowArray[$scope.langcount].parentFlag = false;
                $scope.rowArray[$scope.langcount].parentId = intervalId;
                $scope.rowArray[$scope.langcount].orderNo = orderNo;
                $scope.rowArray[$scope.langcount].enabledFor = enabledFor;
                $scope.rowArray[$scope.langcount].enabled = enabled;
                $scope.rowArray[$scope.langcount].deleteFlag = false;
                $scope.rowArray[$scope.langcount].createdDate = new Date();
                $scope.rowArray[$scope.langcount].createdBy = $window.sessionStorage.userId;
                $scope.rowArray[$scope.langcount].createdByRole = $window.sessionStorage.roleId;
                $scope.rowArray[$scope.langcount].lastModifiedDate = new Date();
                $scope.rowArray[$scope.langcount].lastModifiedBy = $window.sessionStorage.userId;
                $scope.rowArray[$scope.langcount].lastModifiedByRole = $window.sessionStorage.roleId;

                Restangular.all('conditions').post($scope.rowArray[$scope.langcount]).then(function () {
                    $scope.langcount++;
                    $scope.langFunc(intervalId, orderNo, enabledFor, enabled);
                });

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/screentreat-list';
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {

            document.getElementById('name').style.border = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.condition.name == '' || $scope.condition.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Condition';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.condition.orderNo == '' || $scope.condition.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                document.getElementById('order').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.condition.parentId === '') {
                    delete $scope.condition['parentId'];
                }

                $scope.submitDisable = true;

                $scope.condition.lastModifiedDate = new Date();
                $scope.condition.lastModifiedBy = $window.sessionStorage.userId;
                $scope.condition.lastModifiedByRole = $window.sessionStorage.roleId;

                Restangular.one('conditions', $routeParams.id).customPUT($scope.condition).then(function (resp) {
                    $scope.langUpdateFunc(resp.id, resp.orderNo, resp.enabledFor, resp.enabled);
                }, function (error) {
                    if (error.data.error.constraint === 'condition_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        $scope.langUpdatecount = 0;

        $scope.langUpdateFunc = function (intervalId, orderNo, enabledFor, enabled) {

            if ($scope.langUpdatecount < $scope.rowArray.length) {

                if ($scope.rowArray[$scope.langUpdatecount].id == 0 || $scope.rowArray[$scope.langUpdatecount].id == "0") {
                    $scope.rowArray[$scope.langUpdatecount].parentFlag = false;
                    $scope.rowArray[$scope.langUpdatecount].parentId = intervalId;
                    $scope.rowArray[$scope.langUpdatecount].orderNo = orderNo;
                    $scope.rowArray[$scope.langUpdatecount].enabledFor = enabledFor;
                    $scope.rowArray[$scope.langUpdatecount].enabled = enabled;
                    $scope.rowArray[$scope.langUpdatecount].deleteFlag = false;
                    $scope.rowArray[$scope.langUpdatecount].createdDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].createdBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].createdByRole = $window.sessionStorage.roleId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedByRole = $window.sessionStorage.roleId;
                    delete $scope.rowArray[$scope.langUpdatecount]['id'];

                    Restangular.all('conditions').post($scope.rowArray[$scope.langUpdatecount]).then(function () {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId);
                    });

                } else {

                    $scope.rowArray[$scope.langUpdatecount].orderNo = orderNo;
                    $scope.rowArray[$scope.langUpdatecount].enabledFor = enabledFor;
                    $scope.rowArray[$scope.langUpdatecount].enabled = enabled;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedByRole = $window.sessionStorage.roleId;

                    Restangular.one('conditions', $scope.rowArray[$scope.langUpdatecount].id).customPUT($scope.rowArray[$scope.langUpdatecount]).then(function (resp1) {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId, orderNo, enabledFor, enabled);
                    });
                }

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/screentreat-list';
            }
        };


        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

       /******************************************************** DELETE *******************************************/
        
        $scope.Delete = function (id) {
            
            $scope.item = [{
                deleteFlag: true
            }]

            Restangular.one('conditions/' + id).customPUT($scope.item[0]).then(function () {
                Restangular.all('conditions?filter[where][parentId]=' + id).getList().then(function (stypes) {
                    $scope.deleteRows = stypes;
                    $scope.DeleteFunc();
                });
            });
        };

        $scope.updateFlag = {
            deleteFlag: true
        };

        $scope.DeleteCount = 0;

        $scope.DeleteFunc = function (id) {

            if ($scope.DeleteCount < $scope.deleteRows.length) {

                Restangular.one('conditions/' + $scope.deleteRows[$scope.DeleteCount].id).customPUT($scope.updateFlag).then(function () {
                    $scope.DeleteCount++;
                    $scope.DeleteFunc();
                });

            } else {
                $route.reload();
            }
        }; 

    });
