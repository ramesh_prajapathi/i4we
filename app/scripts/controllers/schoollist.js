'use strict';

angular.module('secondarySalesApp')
    .controller('SchoolListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {


        /************************ Language ************************/

        Restangular.one('schoollanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.schoolLanguage = langResponse[0];
            $scope.headingName = $scope.schoolLanguage.houseHoldCreate;
            $scope.modalTitle = $scope.schoolLanguage.thankYou;
            $scope.message = $scope.schoolLanguage.thankYouCreated;
        });

        /************* Focus ***************************************/

        /************* Focus ***************************************/

        $scope.someFocusVariable = true;
        $scope.FocusMe = true;

        $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;

        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;

        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;

        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;

        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;

        $scope.roledsply = Restangular.all('roles').getList().$object;
        $scope.areasply = Restangular.all('areas?filter[where][deleteFlag]=false').getList().$object;
        $scope.schooltypedsply = Restangular.all('schooltypes?filter[where][deleteFlag]=false').getList().$object;


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/school-list") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            // console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************** List **********/

        $scope.obj = {};

        $scope.obj.status = 'active';

        $scope.$watch('obj.status', function (newValue, oldValue) {
            // console.log('newValue', newValue);
            // console.log('oldValue', oldValue);
            if (newValue === '' || newValue === null) {
                return;
            } else {

                Restangular.all('users?filter[where][deleteFlag]=false').getList().then(function (user) {

                    $scope.users = user;

                    Restangular.all('areas?filter[where][deleteFlag]=false').getList().then(function (area) {
                        $scope.areas = area;
                        Restangular.all('schooltypes?filter[where][deleteFlag]=false').getList().then(function (school) {
                            $scope.schooltypes = school;
                            Restangular.all('schoolrecords?filter[where][deleteFlag]=false').getList().then(function (schoolRespose) {
                                //  console.log('schoolRespose', schoolRespose);

                                $scope.schoolrecords = schoolRespose;

                                $scope.modalInstanceLoad.close();

                                angular.forEach($scope.schoolrecords, function (member, index) {

                                    member.totalStudent = '';
                                    member.totalStudent = parseInt(member.noofChildrenMale) + parseInt(member.noofChildrenFemale);
                                    // console.log('member.totalStudent', member.totalStudent);


                                    var data2 = $scope.users.filter(function (arr) {
                                        return arr.id == member.associatedHF
                                    })[0];

                                    if (data2 != undefined) {
                                        member.associateDisp = data2.name;
                                    }

                                    var data3 = $scope.areas.filter(function (arr) {
                                        return arr.id == member.area
                                    })[0];

                                    if (data3 != undefined) {
                                        member.areaname = data3.name;
                                    }

                                    var data4 = $scope.schooltypes.filter(function (arr) {
                                        return arr.id == member.typeofSchool
                                    })[0];

                                    if (data4 != undefined) {
                                        member.schoolTypeName = data4.name;
                                    }

                                    var data13 = $scope.countrydisplay.filter(function (arr) {
                                        return arr.id == member.countryId
                                    })[0];

                                    if (data13 != undefined) {
                                        member.countryname = data13.name;
                                    }

                                    var data14 = $scope.statedisplay.filter(function (arr) {
                                        return arr.id == member.stateId
                                    })[0];

                                    if (data14 != undefined) {
                                        member.statename = data14.name;
                                    }

                                    var data15 = $scope.districtdsply.filter(function (arr) {
                                        return arr.id == member.districtId
                                    })[0];

                                    if (data15 != undefined) {
                                        member.districtname = data15.name;
                                    }

                                    var data16 = $scope.sitedsply.filter(function (arr) {
                                        return arr.id == member.siteId
                                    })[0];

                                    if (data16 != undefined) {
                                        member.sitename = data16.name;
                                    }

                                    var data17 = $scope.usersdisplay.filter(function (arr) {
                                        return arr.id == member.associatedHF
                                    })[0];

                                    if (data17 != undefined) {
                                        member.assignedTo = data17.name;
                                    }

                                    var data18 = $scope.usersdisplay.filter(function (arr) {
                                        return arr.id == member.lastModifiedBy
                                    })[0];

                                    if (data18 != undefined) {
                                        member.lastModifiedByname = data18.username;
                                    } 

                                    var data19 = $scope.roledsply.filter(function (arr) {
                                        return arr.id == member.lastModifiedByRole
                                    })[0];

                                    if (data19 != undefined) {
                                        member.lastModifiedByRolename = data19.name;
                                    } 
                                    var data20 = $scope.roledsply.filter(function (arr) {
                                        return arr.id == member.createdByRole
                                    })[0];

                                    if (data20 != undefined) {
                                        member.createdByRolename = data20.name;
                                    }

                                    var data21 = $scope.usersdisplay.filter(function (arr) {
                                        return arr.id == member.createdBy
                                    })[0];

                                    if (data21 != undefined) {
                                        member.createdByname = data21.username;
                                    }


                                    member.datedisply = $filter('date')(member.dateofAdding, 'dd-MMM-yyyy');
                                    member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                                    member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');
                                    member.loaction = member.latitude + ' , ' + member.longitude;

                                    member.index = index + 1;
                                    // console.log('member', member);
                                });
                            });
                        });
                    });
                });
            }
        });

        /**************************Export data to excel sheet ***************/

        $scope.DisableExport = false;
        $scope.valschoolCount = 0;

        $scope.exportData = function () {
            $scope.schoolArray = [];
            $scope.valschoolCount = 0;
            if ($scope.schoolrecords.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.schoolrecords.length; c++) {
                    $scope.schoolArray.push({
                        'Sr No': $scope.schoolrecords[c].index,
                        'COUNTRY': $scope.schoolrecords[c].countryname,
                        'STATE': $scope.schoolrecords[c].statename,
                        'DISTRICT': $scope.schoolrecords[c].districtname,
                        'SITE': $scope.schoolrecords[c].sitename,
                        'NAME OF THE SCHOOL': $scope.schoolrecords[c].schoolname,
                        'ASSIGNED TO': $scope.schoolrecords[c].associateDisp,
                        'LOCATION': $scope.schoolrecords[c].loaction,
                        'TYPE OF SCHOOL': $scope.schoolrecords[c].schoolTypeName,
                        'NAME OF MALE CHILDREN': $scope.schoolrecords[c].noofChildrenMale,
                        'DATE OF ADDING': $scope.schoolrecords[c].datedisply,
                        'AREA NAME': $scope.schoolrecords[c].areaname,
                        'NO OF TEACHERS': $scope.schoolrecords[c].noofTeacher,
                        'NAME OF FEMALE CHILDREN': $scope.schoolrecords[c].noofChildrenFemale,
                        'CREATED BY': $scope.schoolrecords[c].createdByname,
                        'CREATED DATE': $scope.schoolrecords[c].createdDate,
                        'CREATED ROLE': $scope.schoolrecords[c].createdByRolename,
                        'LAST MODIFIED BY': $scope.schoolrecords[c].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.schoolrecords[c].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.schoolrecords[c].lastModifiedByRolename,
                        'DELETE FLAG': $scope.schoolrecords[c].deleteFlag
                    });

                    $scope.valschoolCount++;
                    if ($scope.schoolrecords.length == $scope.valschoolCount) {
                        alasql('SELECT * INTO XLSX("schoolrecords.xlsx",{headers:true}) FROM ?', [$scope.schoolArray]);
                    }
                }
            }
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

    });
