'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionDiagnosisCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {

        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 9) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/screentreatdiagnosis/create' || $location.path() === '/screentreatdiagnosis/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/screentreatdiagnosis/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/screentreatdiagnosis/create' || $location.path() === '/screentreatdiagnosis/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/screentreatdiagnosis/create' || $location.path() === '/screentreatdiagnosis/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/conditiondiagnosis") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.showenglishLang = true;
        $scope.OtherLang = true;

        $scope.$watch('conditiondiagnos.name', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id) {
                return;
            } else {
                angular.forEach($scope.rowArray, function (member, index) {
                    member.index = index;
                    member.name = newValue;
                });
            }
        });

        $scope.$watch('conditiondiagnos.language', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;

                    Restangular.all('conditions?filter[where][deleteFlag]=false').getList().then(function (cns) {
                        $scope.conditions = cns;
                    });

                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;

                    Restangular.all('conditions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (cns) {
                        $scope.conditions = cns;
                    });
                }
            }
        });

        $scope.$watch('conditiondiagnos.parentId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.one('conditiondiagnosis', newValue).get().then(function (sts) {
                    $scope.conditiondiagnos.orderNo = sts.orderNo;
                    $scope.conditiondiagnos.condition = sts.condition;
                    $scope.conditiondiagnos.metric = sts.metric;
                    // console.log($scope.conditiondiagnos);
                });
            }
        });

        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {

            $scope.message = 'Diagnosis has been Updated!';

            Restangular.one('conditiondiagnosis', $routeParams.id).get().then(function (conditiondiagnos) {
                $scope.original = conditiondiagnos;
                $scope.conditiondiagnos = Restangular.copy($scope.original);
            });

            Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablangs) {
                $scope.tablanguages1 = tablangs;
                $scope.tablanguages1.splice(0, 1);
                $scope.currValue = 0;

                Restangular.all('conditiondiagnosis?filter[where][deleteFlag]=false&filter[where][parentId]=' + $routeParams.id + '&filter[where][parentFlag]=false').getList().then(function (intvls1) {
                    $scope.mtlangs1 = intvls1;

                    angular.forEach($scope.tablanguages1, function (member, index) {
                        member.index = index;

                        var data = $scope.mtlangs1.filter(function (arr) {
                            return arr.language == member.id
                        })[0];

                        if (data == undefined) {
                            $scope.rowArray.push({
                                language: member.id,
                                id: 0
                            });
                        } else {
                            $scope.rowArray.push(data);
                        }
                    });
                });
            });

        } else {
            $scope.message = 'Diagnosis has been Created!';
        }

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];

        /******************************** INDEX *******************************************/
        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (zn) {
            $scope.cdlanguages = zn;
        });

        Restangular.all('conditiondiagnosis?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (cfs) {
            $scope.conditiondiagnosis = cfs;
            angular.forEach($scope.conditiondiagnosis, function (member, index) {
                member.index = index + 1;

                Restangular.one('languages', member.language).get().then(function (lng) {
                    Restangular.one('conditions', member.condition).get().then(function (cnts) {
                        member.langname = lng.name;
                        member.conditionName = cnts.name;
                    });
                });
            });
        });

        Restangular.all('conditiondiagnosis?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (cn) {
            $scope.englishconditiondiagnosis = cn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        $scope.rowArray = [];

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablang) {
            $scope.tablanguages = tablang;
            $scope.tablanguages.splice(0, 1);
            $scope.currValue = 0;

            angular.forEach($scope.tablanguages, function (member, index) {
                member.index = index;

                if (!$routeParams.id) {
                    $scope.rowArray.push({
                        language: member.id,
                        name: ''
                    });
                }

                if (member.index == 0) {
                    member.active = true;
                    member.visible = true;
                } else {
                    member.active = false;
                    member.visible = false;
                }
            });
        });

        $scope.tabClick = function (row) {
            $scope.tablanguages[row.index].visible = true;
            if (row.index != 0) {
                $scope.tablanguages[0].visible = false;
            }
            $scope.currValue = row.index;
        };

        /********************************************* SAVE *******************************************/

        $scope.conditiondiagnos = {
            name: '',
            language: 1,
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteFlag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {

            document.getElementById('name').style.border = "";
            document.getElementById('metric').style.borderColor = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.conditiondiagnos.condition == '' || $scope.conditiondiagnos.condition == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Condition';

            } else if ($scope.conditiondiagnos.name == '' || $scope.conditiondiagnos.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Diagnosis';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.conditiondiagnos.metric == '' || $scope.conditiondiagnos.metric == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Metric';
                document.getElementById('metric').style.borderColor = "#FF0000";

            } else if ($scope.conditiondiagnos.orderNo == '' || $scope.conditiondiagnos.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                document.getElementById('order').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.conditiondiagnos.parentId === '') {
                    delete $scope.conditiondiagnos['parentId'];
                }

                $scope.submitDisable = true;
                
                $scope.conditiondiagnos.parentFlag = $scope.showenglishLang;

                Restangular.all('conditiondiagnosis').post($scope.conditiondiagnos).then(function (response) {
                    $scope.langFunc(response.id, response.condition, response.metric, response.orderNo);
                }, function (error) {
                    if (error.data.error.constraint === 'conditiondiagnos_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.langcount = 0;

        $scope.langFunc = function (intervalId, condition, metric, orderNo) {

            if ($scope.langcount < $scope.rowArray.length) {

                $scope.rowArray[$scope.langcount].parentFlag = false;
                $scope.rowArray[$scope.langcount].parentId = intervalId;
                $scope.rowArray[$scope.langcount].condition = condition;
                $scope.rowArray[$scope.langcount].metric = metric;
                $scope.rowArray[$scope.langcount].orderNo = orderNo;
                $scope.rowArray[$scope.langcount].deleteFlag = false;
                $scope.rowArray[$scope.langcount].createdDate = new Date();
                $scope.rowArray[$scope.langcount].createdBy = $window.sessionStorage.userId;
                $scope.rowArray[$scope.langcount].createdByRole = $window.sessionStorage.roleId;
                $scope.rowArray[$scope.langcount].lastModifiedDate = new Date();
                $scope.rowArray[$scope.langcount].lastModifiedBy = $window.sessionStorage.userId;
                $scope.rowArray[$scope.langcount].lastModifiedByRole = $window.sessionStorage.roleId;

                Restangular.all('conditiondiagnosis').post($scope.rowArray[$scope.langcount]).then(function () {
                    $scope.langcount++;
                    $scope.langFunc(intervalId, condition, metric, orderNo);
                });

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/screentreatdiagnosis-list';
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {

            document.getElementById('name').style.border = "";
            document.getElementById('metric').style.borderColor = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.conditiondiagnos.condition == '' || $scope.conditiondiagnos.condition == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Condition';

            } else if ($scope.conditiondiagnos.name == '' || $scope.conditiondiagnos.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Step';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.conditiondiagnos.metric == '' || $scope.conditiondiagnos.metric == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Metric';
                document.getElementById('metric').style.borderColor = "#FF0000";

            } else if ($scope.conditiondiagnos.orderNo == '' || $scope.conditiondiagnos.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                document.getElementById('order').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                
                if ($scope.conditiondiagnos.parentId === '') {
                    delete $scope.conditiondiagnos['parentId'];
                }
                
                $scope.submitDisable = true;
                
                Restangular.one('conditiondiagnosis', $routeParams.id).customPUT($scope.conditiondiagnos).then(function (resp) {
                    $scope.langUpdateFunc(resp.id, resp.condition, resp.metric, resp.orderNo);
                }, function (error) {
                    if (error.data.error.constraint === 'conditiondiagnos_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };
    
       $scope.langUpdatecount = 0;

        $scope.langUpdateFunc = function (intervalId, condition, metric, orderNo) {

            if ($scope.langUpdatecount < $scope.rowArray.length) {

                if ($scope.rowArray[$scope.langUpdatecount].id == 0 || $scope.rowArray[$scope.langUpdatecount].id == "0") {
                    $scope.rowArray[$scope.langUpdatecount].parentFlag = false;
                    $scope.rowArray[$scope.langUpdatecount].parentId = intervalId;
                    $scope.rowArray[$scope.langUpdatecount].condition = condition;
                    $scope.rowArray[$scope.langUpdatecount].metric = metric;
                    $scope.rowArray[$scope.langUpdatecount].orderNo = orderNo;
                    $scope.rowArray[$scope.langUpdatecount].deleteFlag = false;
                    $scope.rowArray[$scope.langUpdatecount].createdDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].createdBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].createdByRole = $window.sessionStorage.roleId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedByRole = $window.sessionStorage.roleId;
                    delete $scope.rowArray[$scope.langUpdatecount]['id'];

                    Restangular.all('conditiondiagnosis').post($scope.rowArray[$scope.langUpdatecount]).then(function () {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId, condition, metric, orderNo);
                    });

                } else {

                    $scope.rowArray[$scope.langUpdatecount].condition = condition;
                    $scope.rowArray[$scope.langUpdatecount].metric = metric;
                    $scope.rowArray[$scope.langUpdatecount].orderNo = orderNo;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedByRole = $window.sessionStorage.roleId;

                    Restangular.one('conditiondiagnosis', $scope.rowArray[$scope.langUpdatecount].id).customPUT($scope.rowArray[$scope.langUpdatecount]).then(function (resp1) {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(intervalId, condition, metric, orderNo);
                    });
                }

            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/screentreatdiagnosis-list';
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

/******************************************************** DELETE *******************************************/
        
        $scope.Delete = function (id) {
            
            $scope.item = [{
                deleteFlag: true
            }]

            Restangular.one('conditiondiagnosis/' + id).customPUT($scope.item[0]).then(function () {
                Restangular.all('conditiondiagnosis?filter[where][parentId]=' + id).getList().then(function (stypes) {
                    $scope.deleteRows = stypes;
                    $scope.DeleteFunc();
                });
            });
        };

        $scope.updateFlag = {
            deleteFlag: true
        };

        $scope.DeleteCount = 0;

        $scope.DeleteFunc = function (id) {

            if ($scope.DeleteCount < $scope.deleteRows.length) {

                Restangular.one('conditiondiagnosis/' + $scope.deleteRows[$scope.DeleteCount].id).customPUT($scope.updateFlag).then(function () {
                    $scope.DeleteCount++;
                    $scope.DeleteFunc();
                });

            } else {
                $route.reload();
            }
        };

    });
