'use strict';
angular.module('secondarySalesApp').controller('OneToOneCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $modal, $filter) {
    /*********/
    //console.log('Loading');
Restangular.one('beneficiaries?filter[where][id]=' + $routeParams.id).get().then(function (benResp) {
                if (benResp.length > 0) {
                    $scope.selectedBeneficiary = benResp[0];
                } else {
                    $scope.selectedBeneficiary = {
                        stress_data: false
                    };
                }
            });

$scope.PresentCompletedPillar = 0;
$scope.PresentCompletedQuestion = 0;

    $scope.todos = [{
        name: 'Referral-ANC',
        status: 'Opened',
        date: new Date()
        }, {
        name: 'Referral-ANC',
        status: 'Opened',
        date: new Date()
        }, {
        name: 'Referral-ANC',
        status: 'Opened',
        date: new Date()
        }, {
        name: 'Referral-ANC',
        status: 'Opened',
        date: new Date()
        }, {
        name: 'Referral-ANC',
        status: 'Opened',
        date: new Date()
        }, {
        name: 'Referral-ANC',
        status: 'Opened',
        date: new Date()
        }, {
        name: 'Referral-ANC',
        status: 'Opened',
        date: new Date()
        }];
    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    if ($window.sessionStorage.prviousLocation != "partials/zones") {
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
    };
    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    /*********************************** INDEX *******************************************/
    $scope.openPillarStarted = function () {
        $scope.modalPillarStarted = $modal.open({
            animation: true,
            templateUrl: 'template/PillarStarted.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.okPillarStarted = function () {
        window.location = "/";
    };
    $scope.SavePillarStarted = function () {
        $scope.modalPillarStarted.close();
        $scope.beneficiaryConsent = {
            consent: true
        }
        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryConsent).then(function (beneficiary) {
            // console.log('beneficiary', beneficiary.consent)
            $scope.goToPillarQuestion();
        });
    }
    $scope.UserLanguage = $window.sessionStorage.language;
    if ($routeParams.id != undefined) {
        $scope.DisplayBenef = Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
            $scope.DisplayBeneficiary = beneficiary;
            $scope.DisplayBeneficiary.dob = $scope.DisplayBeneficiary.dob.split('T')[0];
            //$scope.DisplayBeneficiary.condomsasked = $scope.DisplayBeneficiary.condomsasked.split(':')[1];
            //$scope.DisplayBeneficiary.condomsprovided = $scope.DisplayBeneficiary.condomsprovided.split(':')[1];
            if (beneficiary.consent == false) {
                $scope.openPillarStarted();
            }
            if (beneficiary.dynamicmember == true) {
                $scope.StringActiveMember = 'YES';
            } else {
                $scope.StringActiveMember = 'NO';
            }
            if (beneficiary.typology == true) {
                $scope.StringTypology = beneficiary.typology;
            } else {
                $scope.StringTypology = beneficiary.typology;
            }
            if (beneficiary.paidmember == true) {
                $scope.StringPaidMember = 'YES';
            } else {
                $scope.StringPaidMember = 'NO';
            }
            if (beneficiary.plhiv == true) {
                $scope.StringPlhiv = 'YES';
            } else {
                $scope.StringPlhiv = 'NO';
            }

            if(beneficiary.consent == true){
            	$scope.benconsent = 'YES';
            } else {
            	$scope.benconsent = 'NO';
            }
            Restangular.one('employees', beneficiary.facility).get().then(function (comember) {
                $scope.Cohelpline = comember.mobile;
            });
            $scope.remainingschemes = Restangular.all('schemes?filter[where][state]=' + $scope.DisplayBeneficiary.state + '&filter[where][topscheme]=null').getList().$object;
            $scope.documenttypes = Restangular.all('documenttypes').getList().$object;
        });
    }
    $scope.DisableHealthPrev = false;
    $scope.DisableSSJPrev = false;
    $scope.DisableSPPrev = false;
    $scope.DisableFSPrev = false;
    $scope.DisableIDSPrev = false;
    $scope.showMoreDetails = true;
    $scope.showMoreDetails = true;
    $scope.DisableFollowupRI = false;
    $scope.displayHealthPrev = false;
    $scope.displaySSJPrev = false;
    $scope.displaySPPrev = false;
    $scope.displayFSPrev = false;
    $scope.displayIDSPrev = false;
    $scope.MoreDetails = function () {
        $scope.showMoreDetails = !$scope.showMoreDetails;
    };
    $scope.getSchemeDocName = function (docId) {
        return Restangular.one('documenttypes', docId).get().$object;
    };
    $scope.getSchemeName = function (scId) {
        console.log('scId', scId);
        return Restangular.one('schemes', scId).get().$object;
    };
    $scope.todo = {
        status: 1,
        stateid: $window.sessionStorage.zoneId,
        state: $window.sessionStorage.zoneId,
        districtid: $window.sessionStorage.salesAreaId,
        district: $window.sessionStorage.salesAreaId,
        coid: $window.sessionStorage.coorgId,
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        roleId: $window.sessionStorage.roleId
    };
    $scope.TotalTodos = [];
    var sevendays = new Date();
    sevendays.setDate(sevendays.getDate() + 7);
    $scope.todo.datetime = sevendays;
    $scope.fs = {};
    $scope.health = {};
    $scope.sp = {};
    $scope.phonetype = {};

    $scope.reportincident = {
        // "referredby":false,
        "dtmin": new Date(),
        "dtmax": new Date(),
        "currentstatus": 'Needs Follow Up',
        "physical": false,
        "emotional": false,
        "sexual": false,
        "propertyrelated": false,
        "childrelated": false, //"other": false,
        "police": false,
        "goons": false,
        "clients": false,
        "partners": false,
        "husband": false,
        "serviceprovider": false,
        "familymember": false,
        "otherkp": false,
        "perpetratorother": false,
        "neighbour": false,
        "authorities": false,
        "co": false,
        "timetorespond": false,
        "severity": "Moderate",
        "resolved": false,
        "reported": false,
        "reportedpolice": false,
        "reportedngos": false,
        "reportedfriends": false,
        "reportedplv": false,
        "reportedcoteam": false,
        "reportedchampions": false,
        "reportedotherkp": false,
        "reportedlegalaid": false,
        "referredcouncelling": false,
        "referredmedicalcare": false,
        "referredcomanager": false,
        "referredplv": false,
        "referredlegalaid": false,
        "referredboardmember": false,
        "referredboardmember": false,
        "noactionreqmember": false,
        "multiplemembers": ['1', '2'],
        "stateid": $window.sessionStorage.zoneId,
        "state": $window.sessionStorage.zoneId,
        "districtid": $window.sessionStorage.salesAreaId,
        "district": $window.sessionStorage.salesAreaId,
        "coid": $window.sessionStorage.coorgId,
        "facility": $window.sessionStorage.coorgId,
        "beneficiaryid": null,
        "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
        "lastmodifiedtime": new Date(),
        "lastmodifiedbyrole": $window.sessionStorage.roleId
    };
    $scope.follow = {};
    $scope.picker = {};
    $scope.cdids = {};
    $scope.schememaster = {};
    $scope.plhiv = {};
    $scope.condom = {};
    $scope.finliteracy = {};
    $scope.finplanning = {};
    $scope.increment = {};
    $scope.beneficiarycondom = {};
    //////////////////////////////////////////////////////////////////////////////////////////
    $scope.todotyp = Restangular.all('todotypes').getList().then(function (todotyp) {
        $scope.maintodotypes = todotyp;
    });
    $scope.todostat = Restangular.all('todostatuses').getList().then(function (todostat) {
        $scope.maintodostatuses = todostat;
    });
    $scope.todoplrs = Restangular.all('pillars').getList().then(function (todoplrs) {
        $scope.maintodopillars = todoplrs;
    });
    $scope.todorprtstat = Restangular.all('currentstatusofcases').getList().then(function (todorprtstat) {
        $scope.mainreporttodostatuses = todorprtstat;
    });
    //$scope.noofmonthrepaid = Restangular.all('noofmonthrepay?filter[where][deleteflag]=false').getList().$object;
    Restangular.all('noofmonthrepay').getList().then(function (response) {
        var str = response[0].name;
        var arr = str.split('-');
        var a1 = parseInt(arr[0]);
        var a2 = parseInt(arr[1]);
        $scope.newnoofmonthrepay = [];
        for (var i = a1; i <= a2; i++) {
            $scope.newnoofmonthrepay.push(i);
            // console.log(' $scope.newntimesinyears', $scope.newntimesinyears);
        }

    });
    $scope.financialgoals = Restangular.all('financialgoals').getList().$object;
    ///////////////////////All Tasks//////////////////////////
    $scope.CallAllTasks = function () {
        console.log('CallAllTasks');
        $scope.displayHealthPrev = false;
        $scope.displaySSJPrev = false;
        $scope.displaySPPrev = false;
        $scope.displayFSPrev = false;
        $scope.displayIDSPrev = false;
        $scope.TotalTodos = [];
        $scope.pillarId = "";
        $scope.pillars = Restangular.one('pillars').getList().$object;
        $scope.duetodostatuses = Restangular.one('duetodostatuses').getList().$object;
        $scope.zn = Restangular.all('todos?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (zn) {
            $scope.maintodos = zn;
            angular.forEach($scope.maintodos, function (member, index) {
                member.index = index + 1;
                //member.purpose = $scope.getTodopurpose(member.purpose);
                for (var m = 0; m < $scope.maintodotypes.length; m++) {
                    if (member.todotype == $scope.maintodotypes[m].id) {
                        //member.TodoType = $scope.maintodotypes[m].name;
                        member.TodoType = $scope.maintodotypes[m];
                        //console.log('member.TodoType', $scope.maintodotypes[m]);
                        break;
                    }
                }
                for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                    if (member.status == $scope.maintodostatuses[n].id) {
                        member.TodoStatus = $scope.maintodostatuses[n];
                        break;
                    }
                }
                for (var o = 0; o < $scope.maintodopillars.length; o++) {
                    if (member.pillarid == $scope.maintodopillars[o].id) {
                        member.TodoPillar = $scope.maintodopillars[o];
                        break;
                    }
                }
                $scope.TotalTodos.push(member);
            });
        });
        $scope.rprt = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (report) {
            $scope.displayreportincidents = report;
            angular.forEach($scope.displayreportincidents, function (member, index) {
                member.index = index + 1;
                member.pillarid = 2;
                member.todotype = 27;
                member.datetime = member.followupdate;
                //member.purpose = $scope.getTodopurpose(member.purpose);
                for (var m = 0; m < $scope.maintodotypes.length; m++) {
                    if (member.todotype == $scope.maintodotypes[m].id) {
                        //member.TodoType = $scope.maintodotypes[m].name;
                        member.TodoType = $scope.maintodotypes[m];
                        //console.log('member.TodoType', $scope.maintodotypes[m]);
                        break;
                    }
                }
                for (var n = 0; n < $scope.mainreporttodostatuses.length; n++) {
                    if (member.currentstatus == $scope.mainreporttodostatuses[n].name) {
                        member.TodoStatus = $scope.mainreporttodostatuses[n];
                        break;
                    }
                }
                for (var o = 0; o < $scope.maintodopillars.length; o++) {
                    if (member.pillarid == $scope.maintodopillars[o].id) {
                        member.TodoPillar = $scope.maintodopillars[o];
                        break;
                    }
                }
                $scope.TotalTodos.push(member);
            });
        });
    }

    /*****************************************************************Warning Message ************************/
    $scope.healthpillar_hide = true;
    $scope.ssjpillar_hide = true;
    $scope.sppillar_hide = true;
    $scope.fspillar_hide = true;
    $scope.idspillar_hide = true;
    var allhealth_answer = [];
    var allssj_answer = [];
    var allsp_answer = [];
    var allfs_answer = [];
    var allids_answer = [];
    Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (allreportincidents) {
        console.log('allreportincidents', allreportincidents);
        for (var i = 0; i < allreportincidents.length; i++) {
            allssj_answer.push(allreportincidents[i]);
        }
        if (allreportincidents.length == 0) {
            $scope.ssjpillar_hide = false;
        } else {
            for (var c = 0; c < allssj_answer.length; c++) {
                var today_date = new Date();
                var old_date = new Date(allssj_answer[c].lastmodifiedtime);
                var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                var total_round_days = Math.trunc(total_days);
                console.log('total_round_days',total_round_days);
                if (total_round_days > 180) {
                    $scope.ssjpillar_hide = true; 
                    return;
                } /*else {
                   $scope.ssjpillar_hide = false; 
                }*/
            }
        }

    });

    Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (allSurveyAns) {
        //$scope.Survey_Answers = allSurveyAns;
        //console.log('allSurveyAns', allSurveyAns);
       // console.log('allSurveyAns.length', allSurveyAns.length);
        if (allSurveyAns.length == 0) {
            $scope.healthpillar_hide = false;
            $scope.ssjpillar_hide = false;
            //$scope.sppillar_hide = false;
            $scope.fspillar_hide = false;
            $scope.idspillar_hide = false;

        }
        for (var i = 0; i < allSurveyAns.length; i++) {
            if (allSurveyAns[i].pillarid == 5) {
                allhealth_answer.push(allSurveyAns[i])
            }

            /*if (allSurveyAns[i].pillarid == 2) {
                allssj_answer.push(allSurveyAns[i])
            }*/

            if (allSurveyAns[i].pillarid == 1) {
                allsp_answer.push(allSurveyAns[i])
            }
            if (allSurveyAns[i].pillarid == 3) {
                allfs_answer.push(allSurveyAns[i])
            }
            if (allSurveyAns[i].pillarid == 4) {
                allids_answer.push(allSurveyAns[i])
            }
        }

        if (allhealth_answer.length == 0) {
            $scope.healthpillar_hide = false;
        } else {
            //var health_lastans = allhealth_answer[allhealth_answer.length - 1];
            for (var d = 0; d < allhealth_answer.length; d++) {
                var today_date = new Date();
                var old_date = new Date(allhealth_answer[d].lastupdatedtime);
                var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                var total_round_days = Math.trunc(total_days);
                if (total_round_days > 180) {
                    $scope.healthpillar_hide = true;
                    return;
                }
            }
        }

        if (allsp_answer.length == 0) {
            $scope.sppillar_hide = false;
        } else {
            for (var b = 0; b < allsp_answer.length; b++) {
                var today_date = new Date();
                var old_date = new Date(allsp_answer[b].lastupdatedtime);
                var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                var total_round_days = Math.trunc(total_days);
                if (total_round_days > 180) {
                    $scope.sppillar_hide = true;
                    return;
                }
            }
        }

        if (allfs_answer.length == 0) {
            $scope.fspillar_hide = false;
        } else {
            for (var a = 0; a < allfs_answer.length; a++) {
                var today_date = new Date();
                var old_date = new Date(allfs_answer[a].lastupdatedtime);
                var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                var total_round_days = Math.trunc(total_days);
                if (total_round_days > 180) {
                    $scope.fspillar_hide = true;
                    return;
                }
            }
        }
        
        if (allids_answer.length == 0) {
            $scope.idspillar_hide = false;
        } else {
            /* var ids_lastans = allids_answer[allids_answer.length - 1];
              var today_date = new Date();
             var old_date = new Date(ids_lastans.lastupdatedtime);
             var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
             var total_round_days = Math.trunc(total_days);
             //console.log('total_round_days', total_round_days);
             if (total_round_days > 180) {
                 $scope.idspillar_hide = false;
             }*/

            for (var n = 0; n < allids_answer.length; n++) {
               // console.log(allids_answer[n], 'allids_answer');
                var today_date = new Date();
                var old_date = new Date(allids_answer[n].lastupdatedtime);
                var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                var total_round_days = Math.trunc(total_days);
                // console.log('total_round_days', total_round_days);
                if (total_round_days > 180) {
                    $scope.idspillar_hide = true;
                    return;
                }
            }
        }

    });

    //////////////////////////////////All Tasks End///////////////////////
    $scope.TodayDate = $filter('date')(new Date(), 'yyyy-MM-dd') + 'T00:00:00.000Z';
    $scope.healthquestions = [];
    $scope.healthquestioncount = 0;
    $scope.ssjquestions = [];
    $scope.ssjquestioncount = 0;
    $scope.spquestions = [];
    $scope.spquestioncount = 0;
    $scope.fsquestions = [];
    $scope.fsquestioncount = 0;
    $scope.idsquestions = [];
    $scope.idsquestioncount = 0;
    $scope.healthLoaded = false;
    $scope.SSJLoaded = false;
    $scope.SPLoaded = false;
    $scope.FSLoaded = false;
    $scope.IDSLoaded = false;
    /**************************************************Warning*************************************/

    // $scope.WarningMsg = function(){
    //     console.log('Warning Msg');
    // }


    ///////////////////////HEALTH START/////////////////////////////
    $scope.healthques = Restangular.all('surveyquestions?filter[where][pillarid]=5&filter[order]=serialno%20ASC').getList().then(function (surveyques) {
        //console.log('partner',partner);
        for (var i = 0; i < surveyques.length; i++) {
            $scope.healthdata = {};
            /****************************Suman Changes Start****************************************/
            if ($window.sessionStorage.language == 1) {
                $scope.healthdata['question'] = surveyques[i].question;
                $scope.healthdata['answers'] = surveyques[i].answeroptions;
                $scope.healthdata['yesprovideinfo'] = surveyques[i].yesprovideinfo;
                $scope.healthdata['noprovideinfo'] = surveyques[i].noprovideinfo;
            } else if ($window.sessionStorage.language == 2) {
                $scope.healthdata['question'] = surveyques[i].hnquestion;
                $scope.healthdata['answers'] = surveyques[i].answeroptions;
                $scope.healthdata['yesprovideinfo'] = surveyques[i].hnyesprovideinfo;
                $scope.healthdata['noprovideinfo'] = surveyques[i].hnnoprovideinfo;
            } else if ($window.sessionStorage.language == 3) {
                $scope.healthdata['question'] = surveyques[i].kaquestion;
                $scope.healthdata['answers'] = surveyques[i].answeroptions;
                $scope.healthdata['yesprovideinfo'] = surveyques[i].kayesprovideinfo;
                $scope.healthdata['noprovideinfo'] = surveyques[i].kanoprovideinfo;
            } else if ($window.sessionStorage.language == 4) {
                $scope.healthdata['question'] = surveyques[i].taquestion;
                $scope.healthdata['answers'] = surveyques[i].answeroptions;
                $scope.healthdata['yesprovideinfo'] = surveyques[i].tayesprovideinfo;
                $scope.healthdata['noprovideinfo'] = surveyques[i].tanoprovideinfo;
            } else if ($window.sessionStorage.language == 5) {
                $scope.healthdata['question'] = surveyques[i].tequestion;
                $scope.healthdata['answers'] = surveyques[i].answeroptions;
                $scope.healthdata['yesprovideinfo'] = surveyques[i].teyesprovideinfo;
                $scope.healthdata['noprovideinfo'] = surveyques[i].tenoprovideinfo;
            } else if ($window.sessionStorage.language == 6) {
                $scope.healthdata['question'] = surveyques[i].maquestion;
                $scope.healthdata['answers'] = surveyques[i].answeroptions;
                $scope.healthdata['yesprovideinfo'] = surveyques[i].mayesprovideinfo;
                $scope.healthdata['noprovideinfo'] = surveyques[i].manoprovideinfo;
            }
            /****************************Suman Changes End****************************************/
            //$scope.healthdata['question'] = surveyques[i].question;
            $scope.healthdata['pillarid'] = surveyques[i].pillarid;
            $scope.healthdata['questiontype'] = surveyques[i].questiontype;
            $scope.healthdata['noofdropdown'] = surveyques[i].noofoptions;
            $scope.healthdata['id'] = surveyques[i].id;
            //$scope.healthdata['answers'] = surveyques[i].answeroptions;
            $scope.healthdata['yesaction'] = surveyques[i].yesaction;
            $scope.healthdata['noaction'] = surveyques[i].noaction;
            $scope.healthdata['yespopup'] = surveyques[i].yespopup;
            $scope.healthdata['nopopup'] = surveyques[i].nopopup;
            $scope.healthdata['yesskipto'] = surveyques[i].yesskipto;
            $scope.healthdata['noskipto'] = surveyques[i].noskipto;
            $scope.healthdata['schemeslno'] = surveyques[i].schemeslno;
            $scope.healthdata['serialno'] = surveyques[i].serialno;
            $scope.healthdata['yesincrement'] = surveyques[i].yesincrement;
            $scope.healthdata['noincrement'] = surveyques[i].noincrement;
            //$scope.healthdata['yesprovideinfo'] = surveyques[i].yesprovideinfo;
            //$scope.healthdata['noprovideinfo'] = surveyques[i].noprovideinfo;
            $scope.healthdata['yestodotype'] = surveyques[i].yestodotype;
            $scope.healthdata['notodotype'] = surveyques[i].notodotype;
            $scope.healthdata['yesdocumentflag'] = surveyques[i].yesdocumentflag;
            $scope.healthdata['nodocumentflag'] = surveyques[i].nodocumentflag;
            $scope.healthdata['yesdocumentid'] = surveyques[i].yesdocumentid;
            $scope.healthdata['nodocumentid'] = surveyques[i].nodocumentid;
            /******************************New Changes *******************************/
            $scope.healthdata['teansweroptions'] = surveyques[i].teansweroptions;
            $scope.healthdata['hnansweroptions'] = surveyques[i].hnansweroptions;
            $scope.healthdata['kaansweroptions'] = surveyques[i].kaansweroptions;
            $scope.healthdata['taansweroptions'] = surveyques[i].taansweroptions;
            $scope.healthdata['maansweroptions'] = surveyques[i].maansweroptions;
            $scope.healthdata['skipquestion'] = surveyques[i].skipquestion;
            $scope.healthdata['skipon'] = surveyques[i].skipon;
            $scope.healthdata['skipondate'] = surveyques[i].skipondate;
            $scope.healthdata['skipfield'] = surveyques[i].skipfield;
            $scope.healthdata['skiponfield'] = surveyques[i].skiponfield;
            $scope.healthdata['skipdate'] = surveyques[i].skipdate;
            $scope.healthdata['skiponfieldvalue'] = surveyques[i].skiponfieldvalue;
            $scope.healthquestions.push($scope.healthdata);
            if (i == surveyques.length - 1) {
                $scope.healthsurveyquestion = $scope.healthquestions[$scope.healthquestioncount];
                $scope.healthLoaded = true;
                if ($scope.healthsurveyquestion.skipfield == true || $scope.healthsurveyquestion.skiponfield === 'gender') {
                    if ($scope.healthsurveyquestion.skiponfieldvalue != $scope.DisplayBeneficiary.gender) {
                        $scope.healthnextquestion();
                    }
                }
                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).getList().then(function (answered) {
                    //console.log('health answered', answered);
                    if (answered.length > 0) {

                        if ($scope.healthsurveyquestion.skipdate == true) {
                            if (answered[0].answer === 'yes') {
                                var today_date = new Date();
                                var old_date = new Date(answered[0].lastupdatedtime);
                                var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                                var total_round_days = Math.trunc(total_days);
                                console.log('total_round_days', total_round_days);
                                if (total_round_days < $scope.healthsurveyquestion.skipondate) {
                                    $scope.healthnextquestion();
                                }
                            }
                        } else if ($scope.healthsurveyquestion.skipquestion == true && $scope.healthsurveyquestion.skipon == answered[0].answer) {
                            $scope.healthnextquestion();
                        } else {
                            /******************************New Changes *******************************/
                            if ($window.sessionStorage.language == 1) {
                                var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.healthsurveyanswer = enganswers[ansindex];
                                //console.log('$scope.healthsurveyanswer1', $scope.healthsurveyanswer)
                            } else if ($window.sessionStorage.language == 2) {
                                var hindianswers = [];
                                if ($scope.healthquestions[$scope.healthquestioncount].hnansweroptions == null) {
                                    hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                } else {
                                    hindianswers = $scope.healthquestions[$scope.healthquestioncount].hnansweroptions.split(',');
                                }
                                var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.healthsurveyanswer = hindianswers[ansindex];
                                //console.log('$scope.healthsurveyanswer2', $scope.healthsurveyanswer);
                            } else if ($window.sessionStorage.language == 3) {
                                //$scope.ssjdata['answers'] = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions;
                                //var kannadanswers = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions.split(',');
                                var kannadanswers = [];
                                if ($scope.healthquestions[$scope.healthquestioncount].kaansweroptions == null) {
                                    kannadanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                } else {
                                    kannadanswers = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions.split(',');
                                }
                                var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.healthsurveyanswer = kannadanswers[ansindex];
                            } else if ($window.sessionStorage.language == 4) {
                                //$scope.ssjdata['answers'] = $scope.healthquestions[$scope.healthquestioncount].taansweroptions;
                                //var tamilanswers = $scope.healthquestions[$scope.healthquestioncount].taansweroptions.split(',');
                                var tamilanswers = [];
                                if ($scope.healthquestions[$scope.healthquestioncount].taansweroptions == null) {
                                    tamilanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                } else {
                                    tamilanswers = $scope.healthquestions[$scope.healthquestioncount].taansweroptions.split(',');
                                }
                                var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.healthsurveyanswer = tamilanswers[ansindex];
                            } else if ($window.sessionStorage.language == 5) {
                                //var teluguanswers = $scope.healthquestions[$scope.healthquestioncount].teansweroptions.split(',');
                                var teluguanswers = [];
                                if ($scope.healthquestions[$scope.healthquestioncount].teansweroptions == null) {
                                    teluguanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                } else {
                                    teluguanswers = $scope.healthquestions[$scope.healthquestioncount].teansweroptions.split(',');
                                }
                                var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.healthsurveyanswer = teluguanswers[ansindex];
                            } else if ($window.sessionStorage.language == 6) {
                                //$scope.ssjdata['answers'] = surveyques[i].maansweroptions;
                                //var maratianswers = $scope.healthquestions[$scope.healthquestioncount].maansweroptions.split(',');
                                var maratianswers = [];
                                if ($scope.healthquestions[$scope.healthquestioncount].maansweroptions == null) {
                                    maratianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                } else {
                                    maratianswers = $scope.healthquestions[$scope.healthquestioncount].maansweroptions.split(',');
                                }
                                var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.healthsurveyanswer = maratianswers[ansindex];
                            }
                        }
                    }
                });
                if ($scope.healthLoaded == true && $scope.SSJLoaded == true && $scope.FSLoaded == true && $scope.SPLoaded == true && $scope.IDSLoaded == true) {
                    if ($routeParams.id != undefined) {
                        Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                            if (beneficiary.completedpillar != 0) {
                                //console.log(beneficiary.completedpillar, beneficiary.completedquestion);
                                $scope.goToPillarQuestion(beneficiary.completedpillar, beneficiary.completedquestion);
                            } else {
                                $scope.modalInstanceLoad.close();
                            }
                        });
                    }
                }
                /*  CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

                	      // console.log('answercount', answercount);
                	      if (answercount.length > 0) {
                	          $scope.DisableSubmit = true;
                	          $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
                	      } else {*/
                $scope.DisableSubmit = false;
                $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, '', "healthanswers", "healthradio", $scope.healthquestions[$scope.healthquestioncount].pillarid, $scope.healthquestions[$scope.healthquestioncount].yesaction, $scope.healthquestions[$scope.healthquestioncount].noaction, $scope.healthquestions[$scope.healthquestioncount].yespopup, $scope.healthquestions[$scope.healthquestioncount].nopopup, $scope.healthquestions[$scope.healthquestioncount].yesskipto, $scope.healthquestions[$scope.healthquestioncount].noskipto, $scope.healthquestions[$scope.healthquestioncount].schemeslno, $scope.healthquestions[$scope.healthquestioncount].serialno, $scope.healthquestions[$scope.healthquestioncount].yesincrement, $scope.healthquestions[$scope.healthquestioncount].noincrement, $scope.healthquestions[$scope.healthquestioncount].yesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].noprovideinfo, $scope.healthquestions[$scope.healthquestioncount].question, $scope.healthquestions[$scope.healthquestioncount].yestodotype, $scope.healthquestions[$scope.healthquestioncount].notodotype, $scope.healthquestions[$scope.healthquestioncount].yesdocumentflag, $scope.healthquestions[$scope.healthquestioncount].nodocumentflag, $scope.healthquestions[$scope.healthquestioncount].yesdocumentid, $scope.healthquestions[$scope.healthquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.healthquestions[$scope.healthquestioncount].hnyesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnnoprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnquestion
                    /****************************Suman Changes End****************************************/
                );
                //  }
                //  });
            }
        }
    });
    /************************************************** healthnextquestion *************************/
    $scope.healthnextquestion = function () {
        //console.log('$scope.healthnextquestion',$scope.prevquestionClick,$scope.healthquestioncount)
        $scope.DisableHealthPrev = false;
        $scope.healthquestioncount++;
        if ($scope.healthquestioncount < $scope.healthquestions.length) {
            $scope.healthsurveyquestion = $scope.healthquestions[$scope.healthquestioncount];
            // console.log('health nextques1', $scope.healthsurveyquestion);
            if ($scope.healthsurveyquestion.skipfield == true || $scope.healthsurveyquestion.skiponfield === 'gender') {
                if ($scope.healthsurveyquestion.skiponfieldvalue != $scope.DisplayBeneficiary.gender) {
                    $scope.healthnextquestion();
                }
            }
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).getList().then(function (answered) {
                //console.log('$scope.healthnextquestion',answered);
                if (answered.length > 0) {
                    if ($scope.prevquestionClick == true) {
                        $scope.healthquestioncount--;
                        $scope.prevquestionClick = false
                    } else if ($scope.healthsurveyquestion.skipdate == true) {
                        //console.log('SKIP ON DATES SHI H');
                        //console.log('answered.answer',answered[0].answer);
                        if (answered[0].answer === 'yes') {
                            //console.log('Previous Answer', answered);
                            var today_date = new Date();
                            var old_date = new Date(answered[0].lastupdatedtime);
                            var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                            var total_round_days = Math.trunc(total_days);
                            if (total_round_days < $scope.healthsurveyquestion.skipondate) {
                                $scope.healthnextquestion();
                                //console.log('Skip on Days1 Done');
                            }
                        }
                    } else if ($scope.healthsurveyquestion.skipquestion == true && $scope.healthsurveyquestion.skipon == answered[0].answer) {
                        $scope.healthnextquestion();
                    } else {
                        /******************************New Changes *******************************/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = enganswers[ansindex];
                            console.log('$scope.healthsurveyanswer11Prev', $scope.healthsurveyanswer)
                        } else if ($window.sessionStorage.language == 2) {
                            //$scope.ssjdata['answers'] = $scope.healthquestions[$scope.healthquestioncount].hnansweroptions;
                            //var hindianswers = $scope.healthquestions[$scope.healthquestioncount].hnansweroptions.split(',');
                            var hindianswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = hindianswers[ansindex];
                            //console.log('$scope.healthsurveyanswer2', $scope.healthsurveyanswer);
                        } else if ($window.sessionStorage.language == 3) {
                            //$scope.ssjdata['answers'] = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions;
                            //var kannadanswers = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions.split(',');
                            var kannadanswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            //$scope.ssjdata['answers'] = $scope.healthquestions[$scope.healthquestioncount].taansweroptions;
                            //var tamilanswers = $scope.healthquestions[$scope.healthquestioncount].taansweroptions.split(',');
                            var tamilanswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.healthquestions[$scope.healthquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            //var teluguanswers = $scope.healthquestions[$scope.healthquestioncount].teansweroptions.split(',');
                            var teluguanswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.healthquestions[$scope.healthquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            //$scope.ssjdata['answers'] = surveyques[i].maansweroptions;
                            //var maratianswers = $scope.healthquestions[$scope.healthquestioncount].maansweroptions.split(',');
                            var maratianswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].maansweroptions == null) {
                                maratianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.healthquestions[$scope.healthquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            //});
            var mydiv = document.getElementById("healthanswers");
            mydiv.innerHTML = '';
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, '', "healthanswers", "healthradio", $scope.healthquestions[$scope.healthquestioncount].pillarid, $scope.healthquestions[$scope.healthquestioncount].yesaction, $scope.healthquestions[$scope.healthquestioncount].noaction, $scope.healthquestions[$scope.healthquestioncount].yespopup, $scope.healthquestions[$scope.healthquestioncount].nopopup, $scope.healthquestions[$scope.healthquestioncount].yesskipto, $scope.healthquestions[$scope.healthquestioncount].noskipto, $scope.healthquestions[$scope.healthquestioncount].schemeslno, $scope.healthquestions[$scope.healthquestioncount].serialno, $scope.healthquestions[$scope.healthquestioncount].yesincrement, $scope.healthquestions[$scope.healthquestioncount].noincrement, $scope.healthquestions[$scope.healthquestioncount].yesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].noprovideinfo, $scope.healthquestions[$scope.healthquestioncount].question, $scope.healthquestions[$scope.healthquestioncount].yestodotype, $scope.healthquestions[$scope.healthquestioncount].notodotype, $scope.healthquestions[$scope.healthquestioncount].yesdocumentflag, $scope.healthquestions[$scope.healthquestioncount].nodocumentflag, $scope.healthquestions[$scope.healthquestioncount].yesdocumentid, $scope.healthquestions[$scope.healthquestioncount].nodocumentid, $scope.healthquestions[$scope.healthquestioncount].hnyesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnnoprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnquestion);
        } else {
            $scope.healthquestioncount = $scope.healthquestions.length - 1;
            $scope.healthsurveyquestion = $scope.healthquestions[$scope.healthquestioncount];
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    /*if ($scope.healthsurveyquestion.skipfield == true || $scope.healthsurveyquestion.skiponfield === 'gender') {
                    		//console.log('Gender Match3');
                    		Restangular.one('beneficiaries', $routeParams.id).get().then(function (ResponseGender) {
                    			if (ResponseGender.gender != 2) {
                    				$scope.healthnextquestion();
                    				console.log('Pregnent Skip2');
                    			}
                    		});
                    		
                    	} else*/
                    if ($scope.healthsurveyquestion.skipquestion == true && $scope.healthsurveyquestion.skipon == answered[0].answer) {
                        $scope.healthnextquestion();
                    } else {
                        /******************************New Changes *******************************/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = enganswers[ansindex];
                            // console.log('$scope.healthsurveyanswer111', $scope.healthsurveyanswer)
                        } else if ($window.sessionStorage.language == 2) {
                            //$scope.ssjdata['answers'] = $scope.healthquestions[$scope.healthquestioncount].hnansweroptions;
                            //var hindianswers = $scope.healthquestions[$scope.healthquestioncount].hnansweroptions.split(',');
                            var hindianswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = hindianswers[ansindex];
                            //console.log('$scope.healthsurveyanswer3', $scope.healthsurveyanswer);
                        } else if ($window.sessionStorage.language == 3) {
                            //$scope.ssjdata['answers'] = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions;
                            //var kannadanswers = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions.split(',');
                            var kannadanswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            //$scope.ssjdata['answers'] = $scope.healthquestions[$scope.healthquestioncount].taansweroptions;
                            //var tamilanswers = $scope.healthquestions[$scope.healthquestioncount].taansweroptions.split(',');
                            var tamilanswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.healthquestions[$scope.healthquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            //var teluguanswers = $scope.healthquestions[$scope.healthquestioncount].teansweroptions.split(',');
                            var teluguanswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.healthquestions[$scope.healthquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            //$scope.ssjdata['answers'] = surveyques[i].maansweroptions;
                            //var maratianswers = $scope.healthquestions[$scope.healthquestioncount].maansweroptions.split(',');
                            var maratianswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].maansweroptions == null) {
                                maratianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.healthquestions[$scope.healthquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("healthanswers");
            mydiv.innerHTML = '';
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, '', "healthanswers", "healthradio", $scope.healthquestions[$scope.healthquestioncount].pillarid, $scope.healthquestions[$scope.healthquestioncount].yesaction, $scope.healthquestions[$scope.healthquestioncount].noaction, $scope.healthquestions[$scope.healthquestioncount].yespopup, $scope.healthquestions[$scope.healthquestioncount].nopopup, $scope.healthquestions[$scope.healthquestioncount].yesskipto, $scope.healthquestions[$scope.healthquestioncount].noskipto, $scope.healthquestions[$scope.healthquestioncount].schemeslno, $scope.healthquestions[$scope.healthquestioncount].serialno, $scope.healthquestions[$scope.healthquestioncount].yesincrement, $scope.healthquestions[$scope.healthquestioncount].noincrement, $scope.healthquestions[$scope.healthquestioncount].yesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].noprovideinfo, $scope.healthquestions[$scope.healthquestioncount].question, $scope.healthquestions[$scope.healthquestioncount].yestodotype, $scope.healthquestions[$scope.healthquestioncount].notodotype, $scope.healthquestions[$scope.healthquestioncount].yesdocumentflag, $scope.healthquestions[$scope.healthquestioncount].nodocumentflag, $scope.healthquestions[$scope.healthquestioncount].yesdocumentid, $scope.healthquestions[$scope.healthquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.healthquestions[$scope.healthquestioncount].hnyesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnnoprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnquestion
                /****************************Suman Changes End****************************************/
            );
            $scope.goToSSJ();
        }
    };
    /****************************************************** healthprevquestion ***************/
    $scope.prevquestionClick = false;
    $scope.AllpreviousQuestion = function () {
        $scope.prevquestionClick = true;
        //console.log('AllpreviousQuestion', $scope.prevquestionClick);
        // Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
        //     if (beneficiary.completedpillar != 0) {
        //         $scope.goToPillarQuestionPrev(beneficiary.completedpillar, beneficiary.completedquestion);
        //     }
        // });
        console.log("$scope.PresentCompletedPillar",$scope.PresentCompletedPillar);
        console.log("$scope.PresentCompletedQuestion",$scope.PresentCompletedQuestion);
        $scope.goToPillarQuestionPrev($scope.PresentCompletedPillar, $scope.PresentCompletedQuestion);
    }
    /*  $scope.goToPillarQuestionPrev1 = function (pillarid, questionslno) {
          console.log('goToPillarQuestionPrev1', pillarid, questionslno);
          var skpcnt = +questionslno - 1;
          if (pillarid == 1) {
              $scope.spquestioncount = -1;
              $scope.spquestioncount = +$scope.spquestioncount + (+skpcnt);
              Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' +questionslno).getList().then(function (answered) {
                  console.log('answered', answered);
                  if (answered.length > 0) {
                      if ($window.sessionStorage.language == 1) {       
                          $scope.healthsurveyanswer = answered[0].answer;
                          console.log('healthsurveyanswerPrev_SP', $scope.healthsurveyanswer)
                      }
                  }
              });
              $scope.modalInstanceLoad.close();
          }
          else if (pillarid == 2) {
              $scope.ssjquestioncount = -1;
              $scope.ssjquestioncount = +$scope.ssjquestioncount + (+skpcnt);
              $scope.goToSSJ();
              $scope.ssjnextquestion();
              $scope.modalInstanceLoad.close();
          }
          else if (pillarid == 3) {
              $scope.fsquestioncount = -1;
              $scope.fsquestioncount = +$scope.fsquestioncount + (+skpcnt);
              $scope.goToFS();
              $scope.fsnextquestion();
              $scope.modalInstanceLoad.close();
          }
          else if (pillarid == 4) {
              $scope.idsquestioncount = -1;
              $scope.idsquestioncount = +$scope.idsquestioncount + (+skpcnt);
              $scope.goToIDS();
              $scope.idsnextquestion();
              $scope.modalInstanceLoad.close();
          }
          else if (pillarid == 5) {
              $scope.healthquestioncount = -1;
              $scope.healthquestioncount = +$scope.healthquestioncount + (+skpcnt);
              
              Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' +questionslno).getList().then(function (answered) {
                  console.log('answered', answered);
                  if (answered.length > 0) {
                      if ($window.sessionStorage.language == 1) {       
                          $scope.healthsurveyanswer = answered[0].answer;
                          console.log('$scope.healthsurveyanswer111', $scope.healthsurveyanswer)
                      }
                  }
              });
              $scope.modalInstanceLoad.close();
          }
      };*/
    $scope.healthprevquestion = function () {
        $scope.healthquestioncount--;
        if ($scope.healthquestioncount >= 0) {
            $scope.DisableHealthPrev = true;
            //console.log('healthprevquestion', $scope.healthquestioncount);
            $scope.healthsurveyquestion = $scope.healthquestions[$scope.healthquestioncount];
            if ($scope.healthsurveyquestion.skipfield == true || $scope.healthsurveyquestion.skiponfield === 'gender') {
                if ($scope.healthsurveyquestion.skiponfieldvalue != $scope.DisplayBeneficiary.gender) {
                    $scope.healthnextquestion();
                }
            }
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).getList().then(function (answered) {
                //console.log('healthprevquestion',answered);          
                if (answered.length > 0) {
                    if ($scope.healthsurveyquestion.skipdate == true) {
                        if (answered[0].answer === 'yes') {
                            var today_date = new Date();
                            var old_date = new Date(answered[0].lastupdatedtime);
                            var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                            var total_round_days = Math.round(total_days);

                            //console.log('total_round_days',total_round_days);
                            //console.log('$scope.healthsurveyquestion.skipondate',$scope.healthsurveyquestion.skipondate);
                            if (total_round_days < $scope.healthsurveyquestion.skipondate) {
                                $scope.healthnextquestion();
                            }
                        }
                    } else if ($scope.healthsurveyquestion.skipquestion == true && $scope.healthsurveyquestion.skipon == answered[0].answer) {
                        $scope.healthnextquestion();
                    } else {
                        /******************************New Changes *******************************/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = enganswers[ansindex];
                            console.log('$scope.healthsurveyanswer1111', $scope.healthsurveyanswer)
                        } else if ($window.sessionStorage.language == 2) {
                            //$scope.ssjdata['answers'] = $scope.healthquestions[$scope.healthquestioncount].hnansweroptions;
                            //var hindianswers = $scope.healthquestions[$scope.healthquestioncount].hnansweroptions.split(',');
                            var hindianswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = hindianswers[ansindex];
                            console.log('$scope.healthsurveyanswer4', $scope.healthsurveyanswer);
                        } else if ($window.sessionStorage.language == 3) {
                            //$scope.ssjdata['answers'] = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions;
                            //var kannadanswers = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions.split(',');
                            var kannadanswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.healthquestions[$scope.healthquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            //$scope.ssjdata['answers'] = $scope.healthquestions[$scope.healthquestioncount].taansweroptions;
                            //var tamilanswers = $scope.healthquestions[$scope.healthquestioncount].taansweroptions.split(',');
                            var tamilanswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.healthquestions[$scope.healthquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            //var teluguanswers = $scope.healthquestions[$scope.healthquestioncount].teansweroptions.split(',');
                            var teluguanswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.healthquestions[$scope.healthquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            //$scope.ssjdata['answers'] = surveyques[i].maansweroptions;
                            //var maratianswers = $scope.healthquestions[$scope.healthquestioncount].maansweroptions.split(',');
                            var maratianswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].maansweroptions == null) {
                                maratianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.healthquestions[$scope.healthquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("healthanswers");
            mydiv.innerHTML = '';
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, '', "healthanswers", "healthradio", $scope.healthquestions[$scope.healthquestioncount].pillarid, $scope.healthquestions[$scope.healthquestioncount].yesaction, $scope.healthquestions[$scope.healthquestioncount].noaction, $scope.healthquestions[$scope.healthquestioncount].yespopup, $scope.healthquestions[$scope.healthquestioncount].nopopup, $scope.healthquestions[$scope.healthquestioncount].yesskipto, $scope.healthquestions[$scope.healthquestioncount].noskipto, $scope.healthquestions[$scope.healthquestioncount].schemeslno, $scope.healthquestions[$scope.healthquestioncount].serialno, $scope.healthquestions[$scope.healthquestioncount].yesincrement, $scope.healthquestions[$scope.healthquestioncount].noincrement, $scope.healthquestions[$scope.healthquestioncount].yesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].noprovideinfo, $scope.healthquestions[$scope.healthquestioncount].question, $scope.healthquestions[$scope.healthquestioncount].yestodotype, $scope.healthquestions[$scope.healthquestioncount].notodotype, $scope.healthquestions[$scope.healthquestioncount].yesdocumentflag, $scope.healthquestions[$scope.healthquestioncount].nodocumentflag, $scope.healthquestions[$scope.healthquestioncount].yesdocumentid, $scope.healthquestions[$scope.healthquestioncount].nodocumentid
                /****************************Suman Changes Start****************************************                            
                               $scope.healthquestions[$scope.healthquestioncount].hnyesprovideinfo,
                               $scope.healthquestions[$scope.healthquestioncount].hnnoprovideinfo,
                               $scope.healthquestions[$scope.healthquestioncount].hnquestion   
                       /****************************Suman Changes End****************************************/
            );
        } else {
            $scope.healthquestioncount++;
        }
    };
    $scope.CallHealth = function () {
        //console.log('sumanchanges CallHealth')
        $scope.displayHealthPrev = true;
        $scope.displaySSJPrev = false;
        $scope.displaySPPrev = false;
        $scope.displayFSPrev = false;
        $scope.displayIDSPrev = false;
        $scope.hlthtodos = Restangular.all('todos?filter[where][pillarid]=5&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (hlthtodos) {
            $scope.healthtodos = hlthtodos;
            angular.forEach($scope.healthtodos, function (member, index) {
                member.index = index + 1;
                for (var m = 0; m < $scope.maintodotypes.length; m++) {
                    if (member.todotype == $scope.maintodotypes[m].id) {
                        //member.TodoType = $scope.maintodotypes[m].name;
                        member.TodoType = $scope.maintodotypes[m];
                        //console.log('member.TodoType', $scope.maintodotypes[m]);
                        break;
                    }
                }
                for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                    if (member.status == $scope.maintodostatuses[n].id) {
                        //if ($scope.UserLanguage == 1) {
                        member.TodoStatus = $scope.maintodostatuses[n];
                        /*     break;
                        	 }
                        	 if ($scope.UserLanguage == 2) {
                        	     member.TodoStatus = $scope.maintodostatuses[n].hnname;
                        	 }*/
                        break;
                    }
                }
            });
        });
    }
    ///////////////////////HEALTH END/////////////////////////////
    ///////////////////////SSJ START/////////////////////////////
    $scope.ssjques = Restangular.all('surveyquestions?filter[where][pillarid]=2&filter[order]=serialno%20ASC').getList().then(function (ssjsurveyques) {
        //console.log('partner',partner);
        for (var i = 0; i < ssjsurveyques.length; i++) {
            $scope.ssjdata = {};
            /****************************Suman Changes Start****************************************/
            if ($window.sessionStorage.language == 1) {
                $scope.ssjdata['question'] = ssjsurveyques[i].question;
                $scope.ssjdata['answers'] = ssjsurveyques[i].answeroptions;
                $scope.ssjdata['yesprovideinfo'] = ssjsurveyques[i].yesprovideinfo;
                $scope.ssjdata['noprovideinfo'] = ssjsurveyques[i].noprovideinfo;
            } else if ($window.sessionStorage.language == 2) {
                $scope.ssjdata['question'] = ssjsurveyques[i].hnquestion;
                $scope.ssjdata['answers'] = ssjsurveyques[i].answeroptions;
                $scope.ssjdata['yesprovideinfo'] = ssjsurveyques[i].hnyesprovideinfo;
                $scope.ssjdata['noprovideinfo'] = ssjsurveyques[i].hnnoprovideinfo;
            } else if ($window.sessionStorage.language == 3) {
                $scope.ssjdata['question'] = ssjsurveyques[i].kaquestion;
                $scope.ssjdata['answers'] = ssjsurveyques[i].answeroptions;
                $scope.ssjdata['yesprovideinfo'] = ssjsurveyques[i].kayesprovideinfo;
                $scope.ssjdata['noprovideinfo'] = ssjsurveyques[i].kanoprovideinfo;
            } else if ($window.sessionStorage.language == 4) {
                $scope.ssjdata['question'] = ssjsurveyques[i].taquestion;
                $scope.ssjdata['answers'] = ssjsurveyques[i].answeroptions;
                $scope.ssjdata['yesprovideinfo'] = ssjsurveyques[i].tayesprovideinfo;
                $scope.ssjdata['noprovideinfo'] = ssjsurveyques[i].tanoprovideinfo;
            } else if ($window.sessionStorage.language == 5) {
                $scope.ssjdata['question'] = ssjsurveyques[i].tequestion;
                $scope.ssjdata['answers'] = ssjsurveyques[i].answeroptions;
                $scope.ssjdata['yesprovideinfo'] = ssjsurveyques[i].teyesprovideinfo;
                $scope.ssjdata['noprovideinfo'] = ssjsurveyques[i].tenoprovideinfo;
            } else if ($window.sessionStorage.language == 6) {
                $scope.ssjdata['question'] = ssjsurveyques[i].maquestion;
                $scope.ssjdata['answers'] = ssjsurveyques[i].answeroptions;
                $scope.ssjdata['yesprovideinfo'] = ssjsurveyques[i].mayesprovideinfo;
                $scope.ssjdata['noprovideinfo'] = ssjsurveyques[i].manoprovideinfo;
            }
            /****************************Suman Changes End****************************************/
            //$scope.ssjdata['question'] = ssjsurveyques[i].question;
            $scope.ssjdata['pillarid'] = ssjsurveyques[i].pillarid;
            $scope.ssjdata['questiontype'] = ssjsurveyques[i].questiontype;
            $scope.ssjdata['noofdropdown'] = ssjsurveyques[i].noofoptions;
            $scope.ssjdata['id'] = ssjsurveyques[i].id;
            //$scope.ssjdata['answers'] = ssjsurveyques[i].answeroptions;
            $scope.ssjdata['yesaction'] = ssjsurveyques[i].yesaction;
            $scope.ssjdata['noaction'] = ssjsurveyques[i].noaction;
            $scope.ssjdata['yespopup'] = ssjsurveyques[i].yespopup;
            $scope.ssjdata['nopopup'] = ssjsurveyques[i].nopopup;
            $scope.ssjdata['yesskipto'] = ssjsurveyques[i].yesskipto;
            $scope.ssjdata['noskipto'] = ssjsurveyques[i].noskipto;
            $scope.ssjdata['schemeslno'] = ssjsurveyques[i].schemeslno;
            $scope.ssjdata['serialno'] = ssjsurveyques[i].serialno;
            $scope.ssjdata['yesincrement'] = ssjsurveyques[i].yesincrement;
            $scope.ssjdata['noincrement'] = ssjsurveyques[i].noincrement;
            //$scope.ssjdata['yesprovideinfo'] = ssjsurveyques[i].yesprovideinfo;
            //$scope.ssjdata['noprovideinfo'] = ssjsurveyques[i].noprovideinfo;
            $scope.ssjdata['yestodotype'] = ssjsurveyques[i].yestodotype;
            $scope.ssjdata['notodotype'] = ssjsurveyques[i].notodotype;
            $scope.ssjdata['yesdocumentflag'] = ssjsurveyques[i].yesdocumentflag;
            $scope.ssjdata['nodocumentflag'] = ssjsurveyques[i].nodocumentflag;
            $scope.ssjdata['yesdocumentid'] = ssjsurveyques[i].yesdocumentid;
            $scope.ssjdata['nodocumentid'] = ssjsurveyques[i].nodocumentid;
            /************************ new changes ***************/
            $scope.ssjdata['teansweroptions'] = ssjsurveyques[i].teansweroptions;
            $scope.ssjdata['hnansweroptions'] = ssjsurveyques[i].hnansweroptions;
            $scope.ssjdata['kaansweroptions'] = ssjsurveyques[i].kaansweroptions;
            $scope.ssjdata['taansweroptions'] = ssjsurveyques[i].taansweroptions;
            $scope.ssjdata['maansweroptions'] = ssjsurveyques[i].maansweroptions;
            $scope.ssjdata['skipquestion'] = ssjsurveyques[i].skipquestion;
            $scope.ssjdata['skipon'] = ssjsurveyques[i].skipon;
            $scope.ssjquestions.push($scope.ssjdata);
            if (i == ssjsurveyques.length - 1) {
                $scope.ssjsurveyquestion = $scope.ssjquestions[$scope.ssjquestioncount];
                $scope.SSJLoaded = true;
                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.ssjquestions[$scope.ssjquestioncount].id).getList().then(function (answered) {
                    //console.log('ssj-surveyanswers', answered);
                    if (answered.length > 0) {
                        if ($scope.ssjsurveyquestion.skipquestion == true && $scope.ssjsurveyquestion.skipon == answered[0].answer) {
                            $scope.ssjnextquestion();
                        } else {
                            //$scope.ssjsurveyanswer = answered[0].answer;
                            if ($window.sessionStorage.language == 1) {
                                var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.ssjsurveyanswer = enganswers[ansindex];
                            } else if ($window.sessionStorage.language == 2) {
                                //var hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions.split(',');
                                var hindianswers = [];
                                if ($scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions == null) {
                                    hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                                } else {
                                    hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions.split(',');
                                }
                                var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.ssjsurveyanswer = hindianswers[ansindex];
                            } else if ($window.sessionStorage.language == 3) {
                                //var kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions.split(',');
                                var kannadanswers = [];
                                if ($scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions == null) {
                                    kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                                } else {
                                    kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions.split(',');
                                }
                                var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.ssjsurveyanswer = kannadanswers[ansindex];
                            } else if ($window.sessionStorage.language == 4) {
                                //var tamilanswers = $scope.ssjquestions[$scope.ssjquestioncount].taansweroptions.split(',');
                                var tamilanswers = [];
                                if ($scope.ssjquestions[$scope.ssjquestioncount].taansweroptions == null) {
                                    tamilanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                                } else {
                                    tamilanswers = $scope.ssjquestions[$scope.ssjquestioncount].taansweroptions.split(',');
                                }
                                var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.ssjsurveyanswer = tamilanswers[ansindex];
                            } else if ($window.sessionStorage.language == 5) {
                                //var teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].teansweroptions.split(',');
                                var teluguanswers = [];
                                if ($scope.ssjquestions[$scope.ssjquestioncount].teansweroptions == null) {
                                    teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                                } else {
                                    teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].teansweroptions.split(',');
                                }
                                var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.ssjsurveyanswer = teluguanswers[ansindex];
                            } else if ($window.sessionStorage.language == 6) {
                                //var maratianswers = $scope.ssjquestions[$scope.ssjquestioncount].maansweroptions.split(',');
                                var maratianswers = [];
                                if ($scope.ssjquestions[$scope.ssjquestioncount].maansweroptions == null) {
                                    teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                                } else {
                                    maratianswers = $scope.ssjquestions[$scope.ssjquestioncount].maansweroptions.split(',');
                                }
                                var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.ssjsurveyanswer = maratianswers[ansindex];
                            }
                        }
                    }
                });
                if ($scope.healthLoaded == true && $scope.SSJLoaded == true && $scope.FSLoaded == true && $scope.SPLoaded == true && $scope.IDSLoaded == true) {
                    if ($routeParams.id != undefined) {
                        Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                            if (beneficiary.completedpillar != 0) {
                                //console.log(beneficiary.completedpillar, beneficiary.completedquestion);
                                $scope.goToPillarQuestion(beneficiary.completedpillar, beneficiary.completedquestion);
                            } else {
                                $scope.modalInstanceLoad.close();
                            }
                        });
                    }
                }
                /*  CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

                	      // console.log('answercount', answercount);
                	      if (answercount.length > 0) {
                	          $scope.DisableSubmit = true;
                	          $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
                	      } else {*/
                $scope.DisableSubmit = false;
                $scope.ChangeClient($scope.ssjquestions[$scope.ssjquestioncount].id, '', "ssjanswers", "ssjradio", $scope.ssjquestions[$scope.ssjquestioncount].pillarid, $scope.ssjquestions[$scope.ssjquestioncount].yesaction, $scope.ssjquestions[$scope.ssjquestioncount].noaction, $scope.ssjquestions[$scope.ssjquestioncount].yespopup, $scope.ssjquestions[$scope.ssjquestioncount].nopopup, $scope.ssjquestions[$scope.ssjquestioncount].yesskipto, $scope.ssjquestions[$scope.ssjquestioncount].noskipto, $scope.ssjquestions[$scope.ssjquestioncount].schemeslno, $scope.ssjquestions[$scope.ssjquestioncount].serialno, $scope.ssjquestions[$scope.ssjquestioncount].yesincrement, $scope.ssjquestions[$scope.ssjquestioncount].noincrement, $scope.ssjquestions[$scope.ssjquestioncount].yesprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].noprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].question, $scope.ssjquestions[$scope.ssjquestioncount].yestodotype, $scope.ssjquestions[$scope.ssjquestioncount].notodotype, $scope.ssjquestions[$scope.ssjquestioncount].yesdocumentflag, $scope.ssjquestions[$scope.ssjquestioncount].nodocumentflag, $scope.ssjquestions[$scope.ssjquestioncount].yesdocumentid, $scope.ssjquestions[$scope.ssjquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.ssjquestions[$scope.ssjquestioncount].hnyesprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].hnnoprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].hnquestion
                    /****************************Suman Changes End****************************************/
                );
                //  }
                //  });
            }
        }
    });
    $scope.ssjnextquestion = function () {
        $scope.DisableSSJPrev = false;
        $scope.ssjquestioncount++;
        if ($scope.ssjquestioncount < $scope.ssjquestions.length) {
            //console.log('nextquestion', $scope.healthquestioncount);
            $scope.ssjsurveyquestion = $scope.ssjquestions[$scope.ssjquestioncount];
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.ssjquestions[$scope.ssjquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    if ($scope.prevquestionClick == true) {
                        $scope.ssjquestioncount--;
                        $scope.prevquestionClick = false
                    } else
                    if ($scope.ssjsurveyquestion.skipquestion == true && $scope.ssjsurveyquestion.skipon == answered[0].answer) {
                        $scope.ssjnextquestion();
                    } else {
                        //$scope.ssjsurveyanswer = answered[0].answer;
                        /**************** new Changes **************/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {
                            //var hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions.split(',');
                            var hindianswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = hindianswers[ansindex];
                        } else if ($window.sessionStorage.language == 3) {
                            //var kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions.split(',');
                            var kannadanswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            //var tamilanswers = $scope.ssjquestions[$scope.ssjquestioncount].taansweroptions.split(',');
                            var tamilanswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.ssjquestions[$scope.ssjquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            //var teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].teansweroptions.split(',');
                            var teluguanswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            //var maratianswers = $scope.ssjquestions[$scope.ssjquestioncount].maansweroptions.split(',');
                            var maratianswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].maansweroptions == null) {
                                maratianswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.ssjquestions[$scope.ssjquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("ssjanswers");
            mydiv.innerHTML = '';
            // $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id);
            /*CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

            	    //console.log('answercount', answercount.count);
            	    if (answercount.length > 0) {
            	        $scope.DisableSubmit = true;
            	        $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
            	    } else {*/
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.ssjquestions[$scope.ssjquestioncount].id, '', "ssjanswers", "ssjradio", $scope.ssjquestions[$scope.ssjquestioncount].pillarid, $scope.ssjquestions[$scope.ssjquestioncount].yesaction, $scope.ssjquestions[$scope.ssjquestioncount].noaction, $scope.ssjquestions[$scope.ssjquestioncount].yespopup, $scope.ssjquestions[$scope.ssjquestioncount].nopopup, $scope.ssjquestions[$scope.ssjquestioncount].yesskipto, $scope.ssjquestions[$scope.ssjquestioncount].noskipto, $scope.ssjquestions[$scope.ssjquestioncount].schemeslno, $scope.ssjquestions[$scope.ssjquestioncount].serialno, $scope.ssjquestions[$scope.ssjquestioncount].yesincrement, $scope.ssjquestions[$scope.ssjquestioncount].noincrement, $scope.ssjquestions[$scope.ssjquestioncount].yesprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].noprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].question, $scope.ssjquestions[$scope.ssjquestioncount].yestodotype, $scope.ssjquestions[$scope.ssjquestioncount].notodotype, $scope.ssjquestions[$scope.ssjquestioncount].yesdocumentflag, $scope.ssjquestions[$scope.ssjquestioncount].nodocumentflag, $scope.ssjquestions[$scope.ssjquestioncount].yesdocumentid, $scope.ssjquestions[$scope.ssjquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.ssjquestions[$scope.ssjquestioncount].hnyesprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].hnnoprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].hnquestion
                /****************************Suman Changes End****************************************/
            );
            //}
            //  });
        } else {
            $scope.ssjquestioncount = $scope.ssjquestions.length - 1;
            $scope.ssjsurveyquestion = $scope.ssjquestions[$scope.ssjquestioncount];
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.ssjquestions[$scope.ssjquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    if ($scope.ssjsurveyquestion.skipquestion == true && $scope.ssjsurveyquestion.skipon == answered[0].answer) {
                        $scope.ssjnextquestion();
                    } else {
                        //$scope.ssjsurveyanswer = answered[0].answer;
                        /******************************New Changes *******************************/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {
                            //var hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions.split(',');
                            var hindianswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = hindianswers[ansindex];
                            console.log('$scope.ssjsurveyanswer3', $scope.ssjsurveyanswer);
                        } else if ($window.sessionStorage.language == 3) {
                            //var kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions.split(',');
                            var kannadanswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            //var tamilanswers = $scope.ssjquestions[$scope.ssjquestioncount].taansweroptions.split(',');
                            var tamilanswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.ssjquestions[$scope.ssjquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            //var teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].teansweroptions.split(',');
                            var teluguanswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            //var maratianswers = $scope.ssjquestions[$scope.ssjquestioncount].maansweroptions.split(',');
                            var maratianswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].maansweroptions == null) {
                                maratianswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.ssjquestions[$scope.ssjquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("ssjanswers");
            mydiv.innerHTML = '';
            // $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id);
            /*CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

            	    //console.log('answercount', answercount.count);
            	    if (answercount.length > 0) {
            	        $scope.DisableSubmit = true;
            	        $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
            	    } else {*/
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.ssjquestions[$scope.ssjquestioncount].id, '', "ssjanswers", "ssjradio", $scope.ssjquestions[$scope.ssjquestioncount].pillarid, $scope.ssjquestions[$scope.ssjquestioncount].yesaction, $scope.ssjquestions[$scope.ssjquestioncount].noaction, $scope.ssjquestions[$scope.ssjquestioncount].yespopup, $scope.ssjquestions[$scope.ssjquestioncount].nopopup, $scope.ssjquestions[$scope.ssjquestioncount].yesskipto, $scope.ssjquestions[$scope.ssjquestioncount].noskipto, $scope.ssjquestions[$scope.ssjquestioncount].schemeslno, $scope.ssjquestions[$scope.ssjquestioncount].serialno, $scope.ssjquestions[$scope.ssjquestioncount].yesincrement, $scope.ssjquestions[$scope.ssjquestioncount].noincrement, $scope.ssjquestions[$scope.ssjquestioncount].yesprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].noprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].question, $scope.ssjquestions[$scope.ssjquestioncount].yestodotype, $scope.ssjquestions[$scope.ssjquestioncount].notodotype, $scope.ssjquestions[$scope.ssjquestioncount].yesdocumentflag, $scope.ssjquestions[$scope.ssjquestioncount].nodocumentflag, $scope.ssjquestions[$scope.ssjquestioncount].yesdocumentid, $scope.ssjquestions[$scope.ssjquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.ssjquestions[$scope.ssjquestioncount].hnyesprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].hnnoprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].hnquestion
                /****************************Suman Changes End****************************************/
            );
            $scope.goToSP();
        }
    };
    $scope.ssjprevquestion = function () {
        $scope.ssjquestioncount--;
        if ($scope.ssjquestioncount >= 0) {
            $scope.DisableSSJPrev = true;
            //console.log('nextquestion', $scope.healthquestioncount);
            $scope.ssjsurveyquestion = $scope.ssjquestions[$scope.ssjquestioncount];
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.ssjquestions[$scope.ssjquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    if ($scope.ssjsurveyquestion.skipquestion == true && $scope.ssjsurveyquestion.skipon == answered[0].answer) {
                        $scope.ssjnextquestion();
                    } else {
                        //$scope.ssjsurveyanswer = answered[0].answer;
                        /**************** new Changes ***********/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {
                            //var hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions.split(',');
                            var hindianswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.ssjquestions[$scope.ssjquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = hindianswers[ansindex];
                            console.log('$scope.ssjsurveyanswer4', $scope.ssjsurveyanswer);
                        } else if ($window.sessionStorage.language == 3) {
                            //var kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions.split(',');
                            var kannadanswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            //var tamilanswers = $scope.ssjquestions[$scope.ssjquestioncount].taansweroptions.split(',');
                            var tamilanswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].taansweroptions == null) {
                                kannadanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.ssjquestions[$scope.ssjquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            //var teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].teansweroptions.split(',');
                            var teluguanswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.ssjquestions[$scope.ssjquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            //var maratianswers = $scope.ssjquestions[$scope.ssjquestioncount].maansweroptions.split(',');
                            var maratianswers = [];
                            if ($scope.ssjquestions[$scope.ssjquestioncount].maansweroptions == null) {
                                maratianswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.ssjquestions[$scope.ssjquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.ssjquestions[$scope.ssjquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.ssjsurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("ssjanswers");
            mydiv.innerHTML = '';
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.ssjquestions[$scope.ssjquestioncount].id, '', "ssjanswers", "ssjradio", $scope.ssjquestions[$scope.ssjquestioncount].pillarid, $scope.ssjquestions[$scope.ssjquestioncount].yesaction, $scope.ssjquestions[$scope.ssjquestioncount].noaction, $scope.ssjquestions[$scope.ssjquestioncount].yespopup, $scope.ssjquestions[$scope.ssjquestioncount].nopopup, $scope.ssjquestions[$scope.ssjquestioncount].yesskipto, $scope.ssjquestions[$scope.ssjquestioncount].noskipto, $scope.ssjquestions[$scope.ssjquestioncount].schemeslno, $scope.ssjquestions[$scope.ssjquestioncount].serialno, $scope.ssjquestions[$scope.ssjquestioncount].yesincrement, $scope.ssjquestions[$scope.ssjquestioncount].noincrement, $scope.ssjquestions[$scope.ssjquestioncount].yesprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].noprovideinfo, $scope.ssjquestions[$scope.ssjquestioncount].question, $scope.ssjquestions[$scope.ssjquestioncount].yestodotype, $scope.ssjquestions[$scope.ssjquestioncount].notodotype, $scope.ssjquestions[$scope.ssjquestioncount].yesdocumentflag, $scope.ssjquestions[$scope.ssjquestioncount].nodocumentflag, $scope.ssjquestions[$scope.ssjquestioncount].yesdocumentid, $scope.ssjquestions[$scope.ssjquestioncount].nodocumentid);
        } else {
            $scope.ssjquestioncount++;
        }
    };
    $scope.CallSSJ = function () {
        $scope.displayHealthPrev = false;
        $scope.displaySSJPrev = true;
        $scope.displaySPPrev = false;
        $scope.displayFSPrev = false;
        $scope.displayIDSPrev = false;
        Restangular.all('currentstatusofcases').getList().then(function (ssjRes1) {
            $scope.currentstatusofcases = ssjRes1
            $scope.ssjtodos1 = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][currentstatus][nlike]=Closed%').getList().then(function (ssjRes) {
                $scope.ssjtodos = ssjRes
                angular.forEach($scope.ssjtodos, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.currentstatusofcases.length; m++) {
                        if (member.currentstatus == $scope.currentstatusofcases[m].name) {
                            member.TodoType = $scope.currentstatusofcases[m];
                            break;
                        }
                    }
                });
            });
        });
        //$scope.ssjtodos = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][currentstatus][nlike]=Closed%').getList().$object;
        $scope.ssjevents = Restangular.all('events?filter[where][pillarid]=2&filter[where][deleteflag]=false&filter[where][order]=datetime%20DESC&filter[limit]=5&filter[where][datetime][gte]=' + $scope.TodayDate + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().$object;
        Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (reports) {
            $scope.noofIncidents = reports.length;
        });
    }
    ///////////////////////SSJ END/////////////////////////////
    ///////////////////////SP START/////////////////////////////
    $scope.spques = Restangular.all('surveyquestions?filter[where][pillarid]=1&filter[order]=serialno%20ASC').getList().then(function (spsurveyques) {
        //console.log('partner',partner);
        for (var i = 0; i < spsurveyques.length; i++) {
            $scope.spdata = {};
            /****************************Suman Changes Start****************************************/
            if ($window.sessionStorage.language == 1) {
                $scope.spdata['question'] = spsurveyques[i].question;
                $scope.spdata['answers'] = spsurveyques[i].answeroptions;
                $scope.spdata['yesprovideinfo'] = spsurveyques[i].yesprovideinfo;
                $scope.spdata['noprovideinfo'] = spsurveyques[i].noprovideinfo;
            } else if ($window.sessionStorage.language == 2) {
                $scope.spdata['question'] = spsurveyques[i].hnquestion;
                $scope.spdata['answers'] = spsurveyques[i].answeroptions;
                $scope.spdata['yesprovideinfo'] = spsurveyques[i].hnyesprovideinfo;
                $scope.spdata['noprovideinfo'] = spsurveyques[i].hnnoprovideinfo;
            } else if ($window.sessionStorage.language == 3) {
                $scope.spdata['question'] = spsurveyques[i].kaquestion;
                $scope.spdata['answers'] = spsurveyques[i].answeroptions;
                $scope.spdata['yesprovideinfo'] = spsurveyques[i].kayesprovideinfo;
                $scope.spdata['noprovideinfo'] = spsurveyques[i].kanoprovideinfo;
            } else if ($window.sessionStorage.language == 4) {
                $scope.spdata['question'] = spsurveyques[i].taquestion;
                $scope.spdata['answers'] = spsurveyques[i].answeroptions;
                $scope.spdata['yesprovideinfo'] = spsurveyques[i].tayesprovideinfo;
                $scope.spdata['noprovideinfo'] = spsurveyques[i].tanoprovideinfo;
            } else if ($window.sessionStorage.language == 5) {
                $scope.spdata['question'] = spsurveyques[i].tequestion;
                $scope.spdata['answers'] = spsurveyques[i].answeroptions;
                $scope.spdata['yesprovideinfo'] = spsurveyques[i].teyesprovideinfo;
                $scope.spdata['noprovideinfo'] = spsurveyques[i].tenoprovideinfo;
            } else if ($window.sessionStorage.language == 6) {
                $scope.spdata['question'] = spsurveyques[i].maquestion;
                $scope.spdata['answers'] = spsurveyques[i].answeroptions;
                $scope.spdata['yesprovideinfo'] = spsurveyques[i].mayesprovideinfo;
                $scope.spdata['noprovideinfo'] = spsurveyques[i].manoprovideinfo;
            }
            /****************************Suman Changes End****************************************/
            //$scope.spdata['question'] = spsurveyques[i].question;
            $scope.spdata['pillarid'] = spsurveyques[i].pillarid;
            $scope.spdata['questiontype'] = spsurveyques[i].questiontype;
            $scope.spdata['noofdropdown'] = spsurveyques[i].noofoptions;
            $scope.spdata['id'] = spsurveyques[i].id;
            //$scope.spdata['answers'] = spsurveyques[i].answeroptions;
            $scope.spdata['yesaction'] = spsurveyques[i].yesaction;
            $scope.spdata['noaction'] = spsurveyques[i].noaction;
            $scope.spdata['yespopup'] = spsurveyques[i].yespopup;
            $scope.spdata['nopopup'] = spsurveyques[i].nopopup;
            $scope.spdata['yesskipto'] = spsurveyques[i].yesskipto;
            $scope.spdata['noskipto'] = spsurveyques[i].noskipto;
            $scope.spdata['schemeslno'] = spsurveyques[i].schemeslno;
            $scope.spdata['serialno'] = spsurveyques[i].serialno;
            $scope.spdata['yesincrement'] = spsurveyques[i].yesincrement;
            $scope.spdata['noincrement'] = spsurveyques[i].noincrement;
            //$scope.spdata['yesprovideinfo'] = spsurveyques[i].yesprovideinfo;
            //$scope.spdata['noprovideinfo'] = spsurveyques[i].noprovideinfo;
            $scope.spdata['yestodotype'] = spsurveyques[i].yestodotype;
            $scope.spdata['notodotype'] = spsurveyques[i].notodotype;
            $scope.spdata['yesdocumentflag'] = spsurveyques[i].yesdocumentflag;
            $scope.spdata['nodocumentflag'] = spsurveyques[i].nodocumentflag;
            $scope.spdata['yesdocumentid'] = spsurveyques[i].yesdocumentid;
            $scope.spdata['nodocumentid'] = spsurveyques[i].nodocumentid;
            $scope.spdata['teansweroptions'] = spsurveyques[i].teansweroptions;
            $scope.spdata['hnansweroptions'] = spsurveyques[i].hnansweroptions;
            $scope.spdata['kaansweroptions'] = spsurveyques[i].kaansweroptions;
            $scope.spdata['taansweroptions'] = spsurveyques[i].taansweroptions;
            $scope.spdata['maansweroptions'] = spsurveyques[i].maansweroptions;
            $scope.spdata['skipquestion'] = spsurveyques[i].skipquestion;
            $scope.spdata['skipon'] = spsurveyques[i].skipon;
            $scope.spdata['skipfield'] = spsurveyques[i].skipfield;
            $scope.spdata['skiponfield'] = spsurveyques[i].skiponfield;
            $scope.spdata['skiponfieldvalue'] = spsurveyques[i].skiponfieldvalue;
            $scope.spdata['schemenameadded'] = false;
            $scope.spquestions.push($scope.spdata);
            if (i == spsurveyques.length - 1) {
                $scope.spsurveyquestion = $scope.spquestions[$scope.spquestioncount];
                $scope.SPLoaded = true;
                if ($scope.spsurveyquestion.skipfield == true || $scope.spsurveyquestion.skiponfield === 'gender') {
                    if ($scope.spsurveyquestion.skiponfieldvalue != $scope.DisplayBeneficiary.gender) {
                        $scope.spnextquestion();
                        console.log('TG Welfare Skip1');
                    }
                }
                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.spquestions[$scope.spquestioncount].id).getList().then(function (answered) {
                    if (answered.length > 0) {
                        if ($scope.spsurveyquestion.skipquestion == true && $scope.spsurveyquestion.skipon == answered[0].answer) {
                            if ($scope.spsurveyquestion.yespopup = 'dateforid') {
                                $scope.spquestioncount++;
                                $scope.spnextquestion();
                            } else {
                                $scope.spnextquestion();
                            }
                        } else {
                            //$scope.spsurveyanswer = answered[0].answer;
                            /*************** sp new change ************/
                            if ($window.sessionStorage.language == 1) {
                                var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = enganswers[ansindex];
                            } else if ($window.sessionStorage.language == 2) {
                                //var hindianswers = $scope.spquestions[$scope.spquestioncount].hnansweroptions.split(',');
                                var hindianswers = [];
                                if ($scope.spquestions[$scope.spquestioncount].hnansweroptions == null) {
                                    hindianswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                } else {
                                    hindianswers = $scope.spquestions[$scope.spquestioncount].hnansweroptions.split(',');
                                }
                                var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = hindianswers[ansindex];
                            } else if ($window.sessionStorage.language == 3) {
                                var kannadanswers = [];
                                if ($scope.spquestions[$scope.spquestioncount].kaansweroptions == null) {
                                    kannadanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                } else {
                                    kannadanswers = $scope.spquestions[$scope.spquestioncount].kaansweroptions.split(',');
                                }
                                var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = kannadanswers[ansindex];
                            } else if ($window.sessionStorage.language == 4) {
                                var tamilanswers = [];
                                if ($scope.spquestions[$scope.spquestioncount].taansweroptions == null) {
                                    tamilanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                } else {
                                    tamilanswers = $scope.spquestions[$scope.spquestioncount].taansweroptions.split(',');
                                }
                                var enganswers = $scope.ssjquestions[$scope.spquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = tamilanswers[ansindex];
                            } else if ($window.sessionStorage.language == 5) {
                                var teluguanswers = [];
                                if ($scope.spquestions[$scope.spquestioncount].teansweroptions == null) {
                                    teluguanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                } else {
                                    teluguanswers = $scope.spquestions[$scope.spquestioncount].teansweroptions.split(',');
                                }
                                var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = teluguanswers[ansindex];
                            } else if ($window.sessionStorage.language == 6) {
                                var maratianswers = [];
                                if ($scope.spquestions[$scope.spquestioncount].maansweroptions == null) {
                                    maratianswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                } else {
                                    maratianswers = $scope.spquestions[$scope.spquestioncount].maansweroptions.split(',');
                                }
                                var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = maratianswers[ansindex];
                            }
                        }
                    }
                });
                if ($scope.healthLoaded == true && $scope.SSJLoaded == true && $scope.FSLoaded == true && $scope.SPLoaded == true && $scope.IDSLoaded == true) {
                    if ($routeParams.id != undefined) {
                        Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                            if (beneficiary.completedpillar != 0) {
                                //console.log(beneficiary.completedpillar, beneficiary.completedquestion);
                                $scope.goToPillarQuestion(beneficiary.completedpillar, beneficiary.completedquestion);
                            } else {
                                $scope.modalInstanceLoad.close();
                            }
                        });
                    }
                }
                /*  CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

                	      // console.log('answercount', answercount);
                	      if (answercount.length > 0) {
                	          $scope.DisableSubmit = true;
                	          $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
                	      } else {*/
                $scope.DisableSubmit = false;
                $scope.ChangeClient($scope.spquestions[$scope.spquestioncount].id, '', "spanswers", "spradio", $scope.spquestions[$scope.spquestioncount].pillarid, $scope.spquestions[$scope.spquestioncount].yesaction, $scope.spquestions[$scope.spquestioncount].noaction, $scope.spquestions[$scope.spquestioncount].yespopup, $scope.spquestions[$scope.spquestioncount].nopopup, $scope.spquestions[$scope.spquestioncount].yesskipto, $scope.spquestions[$scope.spquestioncount].noskipto, $scope.spquestions[$scope.spquestioncount].schemeslno, $scope.spquestions[$scope.spquestioncount].serialno, $scope.spquestions[$scope.spquestioncount].yesincrement, $scope.spquestions[$scope.spquestioncount].noincrement, $scope.spquestions[$scope.spquestioncount].yesprovideinfo, $scope.spquestions[$scope.spquestioncount].noprovideinfo, $scope.spquestions[$scope.spquestioncount].question, $scope.spquestions[$scope.spquestioncount].yestodotype, $scope.spquestions[$scope.spquestioncount].notodotype, $scope.spquestions[$scope.spquestioncount].yesdocumentflag, $scope.spquestions[$scope.spquestioncount].nodocumentflag, $scope.spquestions[$scope.spquestioncount].yesdocumentid, $scope.spquestions[$scope.spquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.spquestions[$scope.spquestioncount].hnyesprovideinfo, $scope.spquestions[$scope.spquestioncount].hnnoprovideinfo, $scope.spquestions[$scope.spquestioncount].hnquestion
                    /****************************Suman Changes End****************************************/
                );
                //  }
                //  });
            }
        }
    });
    $scope.spnextquestion = function () {
        $scope.DisableSPPrev = false;
        $scope.spquestioncount++;
        if ($scope.spquestioncount < $scope.spquestions.length) {
            //console.log('spnextques', $scope.healthquestioncount);
            $scope.spsurveyquestion = $scope.spquestions[$scope.spquestioncount];
            //console.log('spnextques', $scope.spsurveyquestion);
            if ($scope.spsurveyquestion.questiontype == 'yespopup') {
                $scope.ValidateApplication();
            }
            if ($scope.spsurveyquestion.skipfield == true || $scope.spsurveyquestion.skiponfield === 'gender') {
                if ($scope.spsurveyquestion.skiponfieldvalue != $scope.DisplayBeneficiary.gender) {
                    $scope.spnextquestion();
                    //console.log('TG Welfare Skip2');
                }
            }
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.spquestions[$scope.spquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    /*if ($scope.spsurveyquestion.skipfield == true || $scope.spsurveyquestion.skiponfield === 'gender') {
                    		console.log('TG Match2');
                    		Restangular.one('beneficiaries', $routeParams.id).get().then(function (ResponseTG) {
                    			if (ResponseTG.gender != 3) {
                    				$scope.spquestioncount++;
                    				$scope.spnextquestion();
                    			} else {
                    				$scope.spnextquestion();
                    			}
                    		});
                    	} else*/
                    if ($scope.prevquestionClick == true) {
                        //  $scope.spquestioncount--;
                        //  $scope.prevquestionClick = false
                        // // $scope.spnextquestion();
                        // $scope.spquestioncount++;
                    } else
                        /* {
                              $scope.spquestioncount++;
                              $scope.spnextquestion();
                         }*/
                        if ($scope.spsurveyquestion.skipquestion == true && $scope.spsurveyquestion.skipon == answered[0].answer) {
                            if ($scope.spsurveyquestion.yespopup = 'dateforid') {
                                $scope.spquestioncount++;
                                $scope.spnextquestion();
                            } else {
                                $scope.spnextquestion();
                            }
                        } else {
                            //$scope.spsurveyanswer = answered[0].answer;
                            /**************** sp new Changes **************/
                            if ($window.sessionStorage.language == 1) {
                                var enganswers = $scope.spquestions[$scope.ssjquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = enganswers[ansindex];
                            } else if ($window.sessionStorage.language == 2) {
                                var hindianswers = [];
                                if ($scope.spquestions[$scope.spquestioncount].hnansweroptions == null) {
                                    hindianswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                } else {
                                    hindianswers = $scope.spquestions[$scope.spquestioncount].hnansweroptions.split(',');
                                }
                                var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = hindianswers[ansindex];
                            } else if ($window.sessionStorage.language == 3) {
                                var kannadanswers = [];
                                if ($scope.spquestions[$scope.spquestioncount].kaansweroptions == null) {
                                    kannadanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                } else {
                                    kannadanswers = $scope.spquestions[$scope.spquestioncount].kaansweroptions.split(',');
                                }
                                var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = kannadanswers[ansindex];
                            } else if ($window.sessionStorage.language == 4) {
                                var tamilanswers = [];
                                if ($scope.spquestions[$scope.spquestioncount].taansweroptions == null) {
                                    tamilanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                } else {
                                    tamilanswers = $scope.spquestions[$scope.spquestioncount].taansweroptions.split(',');
                                }
                                var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = tamilanswers[ansindex];
                            } else if ($window.sessionStorage.language == 5) {
                                var teluguanswers = [];
                                if ($scope.spquestions[$scope.spquestioncount].teansweroptions == null) {
                                    teluguanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                } else {
                                    teluguanswers = $scope.spquestions[$scope.spquestioncount].teansweroptions.split(',');
                                }
                                var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = teluguanswers[ansindex];
                            } else if ($window.sessionStorage.language == 6) {
                                var maratianswers = [];
                                if ($scope.spquestions[$scope.spquestioncount].maansweroptions == null) {
                                    maratianswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                } else {
                                    maratianswers = $scope.spquestions[$scope.spquestioncount].maansweroptions.split(',');
                                }
                                var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.spsurveyanswer = maratianswers[ansindex];
                            }
                        }
                }
            });
            var mydiv = document.getElementById("spanswers");
            mydiv.innerHTML = '';
            // $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id);
            /*CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

            	    //console.log('answercount', answercount.count);
            	    if (answercount.length > 0) {
            	        $scope.DisableSubmit = true;
            	        $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
            	    } else {*/
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.spquestions[$scope.spquestioncount].id, '', "spanswers", "spradio", $scope.spquestions[$scope.spquestioncount].pillarid, $scope.spquestions[$scope.spquestioncount].yesaction, $scope.spquestions[$scope.spquestioncount].noaction, $scope.spquestions[$scope.spquestioncount].yespopup, $scope.spquestions[$scope.spquestioncount].nopopup, $scope.spquestions[$scope.spquestioncount].yesskipto, $scope.spquestions[$scope.spquestioncount].noskipto, $scope.spquestions[$scope.spquestioncount].schemeslno, $scope.spquestions[$scope.spquestioncount].serialno, $scope.spquestions[$scope.spquestioncount].yesincrement, $scope.spquestions[$scope.spquestioncount].noincrement, $scope.spquestions[$scope.spquestioncount].yesprovideinfo, $scope.spquestions[$scope.spquestioncount].noprovideinfo, $scope.spquestions[$scope.spquestioncount].question, $scope.spquestions[$scope.spquestioncount].yestodotype, $scope.spquestions[$scope.spquestioncount].notodotype, $scope.spquestions[$scope.spquestioncount].yesdocumentflag, $scope.spquestions[$scope.spquestioncount].nodocumentflag, $scope.spquestions[$scope.spquestioncount].yesdocumentid, $scope.spquestions[$scope.spquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.spquestions[$scope.spquestioncount].hnyesprovideinfo, $scope.spquestions[$scope.spquestioncount].hnnoprovideinfo, $scope.spquestions[$scope.spquestioncount].hnquestion
                /****************************Suman Changes End****************************************/
            );
            //}
            //  });
        } else {
            $scope.spquestioncount = $scope.spquestions.length - 1;
            $scope.spsurveyquestion = $scope.spquestions[$scope.spquestioncount];
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.spquestions[$scope.spquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    //$scope.spsurveyanswer = answered[0].answer;
                    if ($scope.spsurveyquestion.skipfield == true || $scope.spsurveyquestion.skiponfield === 'gender') {
                        console.log('TG Match3');
                        Restangular.one('beneficiaries', $routeParams.id).get().then(function (ResponseTG) {
                            if (ResponseTG.gender != 3) {
                                $scope.spnextquestion();
                                console.log('TG Welfare Skip3');
                            }
                        });
                    } else
                    if ($scope.spsurveyquestion.skipquestion == true && $scope.spsurveyquestion.skipon == answered[0].answer) {
                        if ($scope.spsurveyquestion.yespopup = 'dateforid') {
                            $scope.spquestioncount++;
                            $scope.spnextquestion();
                        } else {
                            $scope.spnextquestion();
                        }
                    } else {
                        /******************************sp New Changes *******************************/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {
                            var hindianswers = [];
                            if ($scope.spquestions[$scope.spquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.spquestions[$scope.spquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = hindianswers[ansindex];
                        } else if ($window.sessionStorage.language == 3) {
                            var kannadanswers = [];
                            if ($scope.spquestions[$scope.spquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.spquestions[$scope.spquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            var tamilanswers = [];
                            if ($scope.spquestions[$scope.spquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.spquestions[$scope.spquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            var teluguanswers = [];
                            if ($scope.spquestions[$scope.spquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.spquestions[$scope.spquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            var maratianswers = [];
                            if ($scope.spquestions[$scope.spquestioncount].maansweroptions == null) {
                                maratianswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.spquestions[$scope.spquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("spanswers");
            mydiv.innerHTML = '';
            // $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id);
            /*CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

            	    //console.log('answercount', answercount.count);
            	    if (answercount.length > 0) {
            	        $scope.DisableSubmit = true;
            	        $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
            	    } else {*/
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.spquestions[$scope.spquestioncount].id, '', "spanswers", "spradio", $scope.spquestions[$scope.spquestioncount].pillarid, $scope.spquestions[$scope.spquestioncount].yesaction, $scope.spquestions[$scope.spquestioncount].noaction, $scope.spquestions[$scope.spquestioncount].yespopup, $scope.spquestions[$scope.spquestioncount].nopopup, $scope.spquestions[$scope.spquestioncount].yesskipto, $scope.spquestions[$scope.spquestioncount].noskipto, $scope.spquestions[$scope.spquestioncount].schemeslno, $scope.spquestions[$scope.spquestioncount].serialno, $scope.spquestions[$scope.spquestioncount].yesincrement, $scope.spquestions[$scope.spquestioncount].noincrement, $scope.spquestions[$scope.spquestioncount].yesprovideinfo, $scope.spquestions[$scope.spquestioncount].noprovideinfo, $scope.spquestions[$scope.spquestioncount].question, $scope.spquestions[$scope.spquestioncount].yestodotype, $scope.spquestions[$scope.spquestioncount].notodotype, $scope.spquestions[$scope.spquestioncount].yesdocumentflag, $scope.spquestions[$scope.spquestioncount].nodocumentflag, $scope.spquestions[$scope.spquestioncount].yesdocumentid, $scope.spquestions[$scope.spquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.spquestions[$scope.spquestioncount].hnyesprovideinfo, $scope.spquestions[$scope.spquestioncount].hnnoprovideinfo, $scope.spquestions[$scope.spquestioncount].hnquestion
                /****************************Suman Changes End****************************************/
            );
            $scope.goToFS();
        }
    };
    $scope.spprevquestion = function () {
        $scope.spquestioncount--;
        if ($scope.spquestioncount >= 0) {
            $scope.DisableSPPrev = true;
            //console.log('nextquestion', $scope.healthquestioncount);
            $scope.spsurveyquestion = $scope.spquestions[$scope.spquestioncount];
            if ($scope.spsurveyquestion.skipfield == true || $scope.spsurveyquestion.skiponfield === 'gender') {
                if ($scope.spsurveyquestion.skiponfieldvalue != $scope.DisplayBeneficiary.gender) {
                    $scope.spnextquestion();
                    console.log('TG Welfare Skip3');
                }
            }
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.spquestions[$scope.spquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    /*	if ($scope.spsurveyquestion.skipfield == true || $scope.spsurveyquestion.skiponfield === 'gender') {
                    			console.log('TG Match4');
                    			Restangular.one('beneficiaries', $routeParams.id).get().then(function (ResponseTG) {
                    				if (ResponseTG.gender != 3) {
                    					$scope.spnextquestion();
                    					console.log('TG Welfare Skip4');
                    				}
                    			});
                    		} else*/
                    if ($scope.spsurveyquestion.skipquestion == true && $scope.spsurveyquestion.skipon == answered[0].answer) {
                        if ($scope.spsurveyquestion.yespopup = 'dateforid') {
                            $scope.spquestioncount++;
                            $scope.spnextquestion();
                        } else {
                            $scope.spnextquestion();
                        }
                    } else {
                        //$scope.spsurveyanswer = answered[0].answer;
                        /**************** new Changes ***********/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {
                            var hindianswers = [];
                            if ($scope.spquestions[$scope.spquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.spquestions[$scope.spquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = hindianswers[ansindex];
                        } else if ($window.sessionStorage.language == 3) {
                            var kannadanswers = [];
                            if ($scope.spquestions[$scope.spquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.spquestions[$scope.spquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            var tamilanswers = [];
                            if ($scope.spquestions[$scope.spquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.spquestions[$scope.spquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            var teluguanswers = [];
                            if ($scope.spquestions[$scope.spquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.spquestions[$scope.spquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            var maratianswers = [];
                            if ($scope.spquestions[$scope.spquestioncount].maansweroptions == null) {
                                maratianswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.spquestions[$scope.spquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.spquestions[$scope.spquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.spsurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("spanswers");
            mydiv.innerHTML = '';
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.spquestions[$scope.spquestioncount].id, '', "spanswers", "spradio", $scope.spquestions[$scope.spquestioncount].pillarid, $scope.spquestions[$scope.spquestioncount].yesaction, $scope.spquestions[$scope.spquestioncount].noaction, $scope.spquestions[$scope.spquestioncount].yespopup, $scope.spquestions[$scope.spquestioncount].nopopup, $scope.spquestions[$scope.spquestioncount].yesskipto, $scope.spquestions[$scope.spquestioncount].noskipto, $scope.spquestions[$scope.spquestioncount].schemeslno, $scope.spquestions[$scope.spquestioncount].serialno, $scope.spquestions[$scope.spquestioncount].yesincrement, $scope.spquestions[$scope.spquestioncount].noincrement, $scope.spquestions[$scope.spquestioncount].yesprovideinfo, $scope.spquestions[$scope.spquestioncount].noprovideinfo, $scope.spquestions[$scope.spquestioncount].question, $scope.spquestions[$scope.spquestioncount].yestodotype, $scope.spquestions[$scope.spquestioncount].notodotype, $scope.spquestions[$scope.spquestioncount].yesdocumentflag, $scope.spquestions[$scope.spquestioncount].nodocumentflag, $scope.spquestions[$scope.spquestioncount].yesdocumentid, $scope.spquestions[$scope.spquestioncount].nodocumentid);
        } else {
            $scope.spquestioncount++;
        }
    };
    $scope.CallSP = function () {
        $scope.displayHealthPrev = false;
        $scope.displaySSJPrev = false;
        $scope.displaySPPrev = true;
        $scope.displayFSPrev = false;
        $scope.displayIDSPrev = false;
        $scope.sptodos = Restangular.all('todos?filter[where][pillarid]=1&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
        $scope.applieddocuments = Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][pillarid]=1&filter[where][answer]=yes&filter[where][availeddate][nlike]=null%').getList().$object;
    }
    ///////////////////////SP END/////////////////////////////
    ///////////////////////FS START/////////////////////////////
    $scope.fsques = Restangular.all('surveyquestions?filter[where][pillarid]=3&filter[order]=serialno%20ASC').getList().then(function (fssurveyques) {
        // console.log('fssurveyques',fssurveyques);
        for (var i = 0; i < fssurveyques.length; i++) {
            $scope.fsdata = {};
            /****************************Suman Changes Start****************************************/
            if ($window.sessionStorage.language == 1) {
                $scope.fsdata['question'] = fssurveyques[i].question;
                $scope.fsdata['answers'] = fssurveyques[i].answeroptions;
                $scope.fsdata['yesprovideinfo'] = fssurveyques[i].yesprovideinfo;
                $scope.fsdata['noprovideinfo'] = fssurveyques[i].noprovideinfo;
            } else if ($window.sessionStorage.language == 2) {
                $scope.fsdata['question'] = fssurveyques[i].hnquestion;
                $scope.fsdata['answers'] = fssurveyques[i].answeroptions;
                $scope.fsdata['yesprovideinfo'] = fssurveyques[i].hnyesprovideinfo;
                $scope.fsdata['noprovideinfo'] = fssurveyques[i].hnnoprovideinfo;
            } else if ($window.sessionStorage.language == 3) {
                $scope.fsdata['question'] = fssurveyques[i].kaquestion;
                $scope.fsdata['answers'] = fssurveyques[i].answeroptions;
                $scope.fsdata['yesprovideinfo'] = fssurveyques[i].kayesprovideinfo;
                $scope.fsdata['noprovideinfo'] = fssurveyques[i].kanoprovideinfo;
            } else if ($window.sessionStorage.language == 4) {
                $scope.fsdata['question'] = fssurveyques[i].taquestion;
                $scope.fsdata['answers'] = fssurveyques[i].answeroptions;
                $scope.fsdata['yesprovideinfo'] = fssurveyques[i].tayesprovideinfo;
                $scope.fsdata['noprovideinfo'] = fssurveyques[i].tanoprovideinfo;
            } else if ($window.sessionStorage.language == 5) {
                $scope.fsdata['question'] = fssurveyques[i].tequestion;
                $scope.fsdata['answers'] = fssurveyques[i].answeroptions;
                $scope.fsdata['yesprovideinfo'] = fssurveyques[i].teyesprovideinfo;
                $scope.fsdata['noprovideinfo'] = fssurveyques[i].tenoprovideinfo;
            } else if ($window.sessionStorage.language == 6) {
                $scope.fsdata['question'] = fssurveyques[i].maquestion;
                $scope.fsdata['answers'] = fssurveyques[i].answeroptions;
                $scope.fsdata['yesprovideinfo'] = fssurveyques[i].mayesprovideinfo;
                $scope.fsdata['noprovideinfo'] = fssurveyques[i].manoprovideinfo;
            }
            /****************************Suman Changes End****************************************/
            //$scope.fsdata['question'] = fssurveyques[i].question;
            $scope.fsdata['pillarid'] = fssurveyques[i].pillarid;
            $scope.fsdata['questiontype'] = fssurveyques[i].questiontype;
            $scope.fsdata['noofdropdown'] = fssurveyques[i].noofoptions;
            $scope.fsdata['id'] = fssurveyques[i].id;
            //$scope.fsdata['answers'] = fssurveyques[i].answeroptions;
            $scope.fsdata['yesaction'] = fssurveyques[i].yesaction;
            $scope.fsdata['noaction'] = fssurveyques[i].noaction;
            $scope.fsdata['yespopup'] = fssurveyques[i].yespopup;
            $scope.fsdata['nopopup'] = fssurveyques[i].nopopup;
            $scope.fsdata['yesskipto'] = fssurveyques[i].yesskipto;
            $scope.fsdata['noskipto'] = fssurveyques[i].noskipto;
            $scope.fsdata['schemeslno'] = fssurveyques[i].schemeslno;
            $scope.fsdata['serialno'] = fssurveyques[i].serialno;
            $scope.fsdata['yesincrement'] = fssurveyques[i].yesincrement;
            $scope.fsdata['noincrement'] = fssurveyques[i].noincrement;
            //$scope.fsdata['yesprovideinfo'] = fssurveyques[i].yesprovideinfo;
            //$scope.fsdata['noprovideinfo'] = fssurveyques[i].noprovideinfo;
            $scope.fsdata['yestodotype'] = fssurveyques[i].yestodotype;
            $scope.fsdata['notodotype'] = fssurveyques[i].notodotype;
            $scope.fsdata['yesdocumentflag'] = fssurveyques[i].yesdocumentflag;
            $scope.fsdata['nodocumentflag'] = fssurveyques[i].nodocumentflag;
            $scope.fsdata['yesdocumentid'] = fssurveyques[i].yesdocumentid;
            $scope.fsdata['nodocumentid'] = fssurveyques[i].nodocumentid;
            $scope.fsdata['teansweroptions'] = fssurveyques[i].teansweroptions;
            $scope.fsdata['hnansweroptions'] = fssurveyques[i].hnansweroptions;
            $scope.fsdata['kaansweroptions'] = fssurveyques[i].kaansweroptions;
            $scope.fsdata['taansweroptions'] = fssurveyques[i].taansweroptions;
            $scope.fsdata['maansweroptions'] = fssurveyques[i].maansweroptions;
            $scope.fsdata['skipquestion'] = fssurveyques[i].skipquestion;
            $scope.fsdata['skipon'] = fssurveyques[i].skipon;
            $scope.fsdata['skipondocument'] = fssurveyques[i].skipondocument;
            $scope.fsdata['skipdocument'] = fssurveyques[i].skipdocument;
            $scope.fsdata['stage'] = fssurveyques[i].stage;
            $scope.fsquestions.push($scope.fsdata);
            if (i == fssurveyques.length - 1) {
                $scope.fssurveyquestion = $scope.fsquestions[$scope.fsquestioncount];
                //console.log('sumanfs fssurveyquestion1', $scope.fssurveyquestion);
                $scope.FSLoaded = true;
                /*if ($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialliteracy') {
                			console.log('SHI H');
                			Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                				if (beneficiary.savings == true && beneficiary.insurance == true || beneficiary.pension == true && beneficiary.credit == true) {
                					$scope.fsnextquestion();
                					console.log('skipfinancialliteracy');
                				}
                			});
                		} else if($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialplanning') {
                			console.log('SHI H');
                			Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                				if (beneficiary.finplansavings == true && beneficiary.finplaninsurance == true || beneficiary.finplanpension == true && beneficiary.finplancredit == true) {
                					$scope.fsnextquestion();
                					console.log('skipfinancialplanning');
                				}
                			});
                			
                		}*/
                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.fsquestions[$scope.fsquestioncount].id).getList().then(function (answered) {
                    //console.log('FS answered', answered)
                    if (answered.length > 0) {
                        //console.log('$scope.fssurveyquestion.yesaction', $scope.fssurveyquestion.yesaction);
                        //console.log('$scope.DisplayBeneficiary.savings', $scope.DisplayBeneficiary.savings);
                        /*	if ($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialliteracy') {
                        			console.log('SHI H');
                        			Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                        				if (beneficiary.savings == true && beneficiary.insurance == true || beneficiary.pension == true && beneficiary.credit == true) {
                        					$scope.fsnextquestion();
                        					console.log('skipfinancialliteracy');
                        				}
                        			});
                        		} else if ($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialplanning') {
                        			console.log('SHI H');
                        			Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                        				if (beneficiary.finplansavings == true && beneficiary.finplaninsurance == true || beneficiary.finplanpension == true && beneficiary.finplancredit == true) {
                        					$scope.fsnextquestion();
                        					console.log('skipfinancialplanning');
                        				}
                        			});
                        		} else*/
                        if ($scope.fssurveyquestion.skipquestion == true && $scope.fssurveyquestion.skipon == answered[0].answer) {
                            if ($scope.fssurveyquestion.skipdocument == true && $scope.fssurveyquestion.stage == true) {
                                $scope.fsquestioncount++;
                            }
                            $scope.fsnextquestion();
                        } else if ($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialliteracy') {
                            //console.log('SHI H');
                            //Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                            if ($scope.DisplayBeneficiary.savings == true && $scope.DisplayBeneficiary.insurance == true && $scope.DisplayBeneficiary.pension == true && $scope.DisplayBeneficiary.credit == true) {
                                $scope.fsnextquestion();
                                //console.log('skipfinancialliteracy');
                            }
                            //});
                        } else if ($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialplanning') {
                            //console.log('SHI H');
                            //Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                            if ($scope.DisplayBeneficiary.finplansavings == true && $scope.DisplayBeneficiary.finplaninsurance == true && $scope.DisplayBeneficiary.finplanpension == true && $scope.DisplayBeneficiary.finplancredit == true) {
                                $scope.fsnextquestion();
                                //console.log('skipfinancialplanning1');
                            }
                            //});
                        } else if ($scope.fssurveyquestion.skipdocument == true && $scope.fssurveyquestion.stage == true) {
                            Restangular.all('schememasters?filter[where][memberId]=' + $routeParams.id + '&filter[where][schemeId]=' + $scope.fssurveyquestion.skipondocument).getList().then(function (schemstrs) {
                                console.log("schemstrs_disp", schemstrs);
                                if (schemstrs.length > 0 && (schemstrs[0].stage == 9 || (schemstrs[0] == 4 && schemstrs[0].responserecieve === 'Rejected'))) {
                                    $scope.fsnextquestion();
                                } else if (schemstrs.length > 0 && schemstrs[0].stage == 7) {
                                    $scope.fsquestioncount++;
                                    $scope.fsnextquestion();
                                } else {
                                    if ($window.sessionStorage.language == 1) {
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = enganswers[ansindex];
                                    } else if ($window.sessionStorage.language == 2) {
                                        var hindianswers = [];
                                        if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                            hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        } else {
                                            hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                                        }
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = hindianswers[ansindex];
                                    } else if ($window.sessionStorage.language == 3) {
                                        var kannadanswers = [];
                                        if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                            kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        } else {
                                            kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                                        }
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = kannadanswers[ansindex];
                                    } else if ($window.sessionStorage.language == 4) {
                                        var tamilanswers = [];
                                        if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                            tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        } else {
                                            tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                                        }
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = tamilanswers[ansindex];
                                    } else if ($window.sessionStorage.language == 5) {
                                        var teluguanswers = [];
                                        if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                            teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        } else {
                                            teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                                        }
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = teluguanswers[ansindex];
                                    } else if ($window.sessionStorage.language == 6) {
                                        var maratianswers = [];
                                        if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                            maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        } else {
                                            maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                                        }
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = maratianswers[ansindex];
                                    }
                                }
                            });
                        } else if ($scope.fssurveyquestion.skipdocument == true && $scope.fssurveyquestion.stage != true) {
                            Restangular.all('schememasters?filter[where][memberId]=' + $routeParams.id + '&filter[where][schemeId]=' + $scope.fssurveyquestion.skipondocument).getList().then(function (schemstrs) {
                                if (schemstrs.length > 0) {
                                    $scope.fsnextquestion();
                                } else {
                                    //$scope.fsquestioncount--;
                                    //$scope.fsnextquestion();
                                    if ($window.sessionStorage.language == 1) {
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = enganswers[ansindex];
                                    } else if ($window.sessionStorage.language == 2) {
                                        var hindianswers = [];
                                        if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                            hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        } else {
                                            hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                                        }
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = hindianswers[ansindex];
                                    } else if ($window.sessionStorage.language == 3) {
                                        var kannadanswers = [];
                                        if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                            kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        } else {
                                            kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                                        }
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = kannadanswers[ansindex];
                                    } else if ($window.sessionStorage.language == 4) {
                                        var tamilanswers = [];
                                        if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                            tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        } else {
                                            tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                                        }
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = tamilanswers[ansindex];
                                    } else if ($window.sessionStorage.language == 5) {
                                        var teluguanswers = [];
                                        if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                            teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        } else {
                                            teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                                        }
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = teluguanswers[ansindex];
                                    } else if ($window.sessionStorage.language == 6) {
                                        var maratianswers = [];
                                        if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                            maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        } else {
                                            maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                                        }
                                        var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                        var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                        $scope.fssurveyanswer = maratianswers[ansindex];
                                    }
                                }
                            });
                        } else {
                            //$scope.fssurveyanswer = answered[0].answer;
                            /*************** fs new change ************/
                            if ($window.sessionStorage.language == 1) {
                                var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.fssurveyanswer = enganswers[ansindex];
                            } else if ($window.sessionStorage.language == 2) {
                                var hindianswers = [];
                                if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                    hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                } else {
                                    hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                                }
                                var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.fssurveyanswer = hindianswers[ansindex];
                            } else if ($window.sessionStorage.language == 3) {
                                var kannadanswers = [];
                                if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                    kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                } else {
                                    kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                                }
                                var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.fssurveyanswer = kannadanswers[ansindex];
                            } else if ($window.sessionStorage.language == 4) {
                                var tamilanswers = [];
                                if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                    tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                } else {
                                    tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                                }
                                var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.fssurveyanswer = tamilanswers[ansindex];
                            } else if ($window.sessionStorage.language == 5) {
                                var teluguanswers = [];
                                if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                    teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                } else {
                                    teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                                }
                                var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.fssurveyanswer = teluguanswers[ansindex];
                            } else if ($window.sessionStorage.language == 6) {
                                var maratianswers = [];
                                if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                    maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                } else {
                                    maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                                }
                                var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.fssurveyanswer = maratianswers[ansindex];
                            }
                        } ///////////
                    }
                });
                if ($scope.healthLoaded == true && $scope.SSJLoaded == true && $scope.FSLoaded == true && $scope.SPLoaded == true && $scope.IDSLoaded == true) {
                    if ($routeParams.id != undefined) {
                        Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                            if (beneficiary.completedpillar != 0) {
                                //console.log(beneficiary.completedpillar, beneficiary.completedquestion);
                                $scope.goToPillarQuestion(beneficiary.completedpillar, beneficiary.completedquestion);
                            } else {
                                $scope.modalInstanceLoad.close();
                            }
                        });
                    }
                }
                $scope.DisableSubmit = false;
                $scope.ChangeClient($scope.fsquestions[$scope.fsquestioncount].id, '', "fsanswers", "fsradio", $scope.fsquestions[$scope.fsquestioncount].pillarid, $scope.fsquestions[$scope.fsquestioncount].yesaction, $scope.fsquestions[$scope.fsquestioncount].noaction, $scope.fsquestions[$scope.fsquestioncount].yespopup, $scope.fsquestions[$scope.fsquestioncount].nopopup, $scope.fsquestions[$scope.fsquestioncount].yesskipto, $scope.fsquestions[$scope.fsquestioncount].noskipto, $scope.fsquestions[$scope.fsquestioncount].schemeslno, $scope.fsquestions[$scope.fsquestioncount].serialno, $scope.fsquestions[$scope.fsquestioncount].yesincrement, $scope.fsquestions[$scope.fsquestioncount].noincrement, $scope.fsquestions[$scope.fsquestioncount].yesprovideinfo, $scope.fsquestions[$scope.fsquestioncount].noprovideinfo, $scope.fsquestions[$scope.fsquestioncount].question, $scope.fsquestions[$scope.fsquestioncount].yestodotype, $scope.fsquestions[$scope.fsquestioncount].notodotype, $scope.fsquestions[$scope.fsquestioncount].yesdocumentflag, $scope.fsquestions[$scope.fsquestioncount].nodocumentflag, $scope.fsquestions[$scope.fsquestioncount].yesdocumentid, $scope.fsquestions[$scope.fsquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.fsquestions[$scope.fsquestioncount].hnyesprovideinfo, $scope.fsquestions[$scope.fsquestioncount].hnnoprovideinfo, $scope.fsquestions[$scope.fsquestioncount].hnquestion
                    /****************************Suman Changes End****************************************/
                );
                //  }
                //  });
            }
        }
    });
    $scope.fsnextquestion = function () {
        $scope.DisableFSPrev = false;
        $scope.fsquestioncount++;
        if ($scope.fsquestioncount < $scope.fsquestions.length) {
            $scope.fssurveyquestion = $scope.fsquestions[$scope.fsquestioncount];
            //console.log('sumanfs fsnextquestion', $scope.fssurveyquestion);
            /*Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
            		if (beneficiary.savings == true || beneficiary.insurance == true || beneficiary.pension == true || beneficiary.credit == true || beneficiary.finplansavings == true || beneficiary.finplaninsurance == true || beneficiary.finplanpension == true || beneficiary.finplancredit == true) {
            			$scope.fsnextquestion();
            			console.log('savinginsurancecreditpension');
            		}
            	});*/
            /*if ($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialplanning') {
            		console.log('SHI H');
            		Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
            			if (beneficiary.finplansavings == true && beneficiary.finplaninsurance == true || beneficiary.finplanpension == true && beneficiary.finplancredit == true) {
            				$scope.fsnextquestion();
            				console.log('savinginsurancecreditpension');
            			}
            		});
            	}*/
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.fsquestions[$scope.fsquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    if ($scope.prevquestionClick == true) {
                        $scope.fsquestioncount--;
                        $scope.prevquestionClick = false
                    } else
                    if ($scope.fssurveyquestion.skipquestion == true && $scope.fssurveyquestion.skipon == answered[0].answer) {
                        if ($scope.fssurveyquestion.skipdocument == true && $scope.fssurveyquestion.stage == true) {
                            $scope.fsquestioncount++;
                        }
                        $scope.fsnextquestion();
                        //console.log("if1", "if");
                    } else if ($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialliteracy') {
                        //console.log('SHI H');
                        //Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                        if ($scope.DisplayBeneficiary.savings == true && $scope.DisplayBeneficiary.insurance == true && $scope.DisplayBeneficiary.pension == true && $scope.DisplayBeneficiary.credit == true) {
                            $scope.fsnextquestion();
                            console.log('skipfinancialliteracy');
                        }
                        //});
                    } else if ($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialplanning') {
                        //console.log('SHI H financialplanning');
                        //Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                        //console.log('beneficiary.finplansavings', beneficiary.finplansavings);
                        if ($scope.DisplayBeneficiary.finplansavings == true && $scope.DisplayBeneficiary.finplaninsurance == true && $scope.DisplayBeneficiary.finplanpension == true && $scope.DisplayBeneficiary.finplancredit == true) {
                            $scope.fsnextquestion();
                            console.log('skipfinancialplanning2');
                        }
                        //});
                    } else if ($scope.fssurveyquestion.skipdocument == true && $scope.fssurveyquestion.stage == true) {
                        console.log("skipdocument", "stage");
                        Restangular.all('schememasters?filter[where][memberId]=' + $routeParams.id + '&filter[where][schemeId]=' + $scope.fssurveyquestion.skipondocument).getList().then(function (schemstrs) {
                            if (schemstrs.length > 0 && (schemstrs[0].stage == 9 || (schemstrs[0] == 4 && schemstrs[0].responserecieve === 'Rejected'))) {
                                $scope.fsnextquestion();
                            } else if (schemstrs.length > 0 && schemstrs[0].stage == 7) {
                                $scope.fsquestioncount++;
                                $scope.fsnextquestion();
                            } else {
                                //$scope.fsquestioncount--;
                                // $scope.fsnextquestion();
                                if ($window.sessionStorage.language == 1) {
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = enganswers[ansindex];
                                } else if ($window.sessionStorage.language == 2) {
                                    var hindianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = hindianswers[ansindex];
                                } else if ($window.sessionStorage.language == 3) {
                                    var kannadanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = kannadanswers[ansindex];
                                } else if ($window.sessionStorage.language == 4) {
                                    var tamilanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = tamilanswers[ansindex];
                                } else if ($window.sessionStorage.language == 5) {
                                    var teluguanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = teluguanswers[ansindex];
                                } else if ($window.sessionStorage.language == 6) {
                                    var maratianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = maratianswers[ansindex];
                                }
                            }
                        });
                    } else if ($scope.fssurveyquestion.skipdocument == true && $scope.fssurveyquestion.stage != true) {
                        console.log("skipdocument", "stage false");
                        Restangular.all('schememasters?filter[where][memberId]=' + $routeParams.id + '&filter[where][schemeId]=' + $scope.fssurveyquestion.skipondocument).getList().then(function (schemstrs) {
                            if (schemstrs.length > 0) {
                                $scope.fsnextquestion();
                            } else {
                                //$scope.fsquestioncount--;
                                //$scope.fsnextquestion();
                                if ($window.sessionStorage.language == 1) {
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = enganswers[ansindex];
                                } else if ($window.sessionStorage.language == 2) {
                                    var hindianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = hindianswers[ansindex];
                                } else if ($window.sessionStorage.language == 3) {
                                    var kannadanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = kannadanswers[ansindex];
                                } else if ($window.sessionStorage.language == 4) {
                                    var tamilanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = tamilanswers[ansindex];
                                } else if ($window.sessionStorage.language == 5) {
                                    var teluguanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = teluguanswers[ansindex];
                                } else if ($window.sessionStorage.language == 6) {
                                    var maratianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = maratianswers[ansindex];
                                }
                            }
                        });
                    } else {
                        console.log("else", "else");
                        //$scope.fssurveyanswer = answered[0].answer;
                        /**************** sp new Changes **************/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {
                            var hindianswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = hindianswers[ansindex];
                        } else if ($window.sessionStorage.language == 3) {
                            var kannadanswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            var tamilanswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            var teluguanswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            var maratianswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = maratianswers[ansindex];
                        }
                    }
                    //});
                }
            });
            var mydiv = document.getElementById("fsanswers");
            mydiv.innerHTML = '';
            // $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id);
            /*CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

            	    //console.log('answercount', answercount.count);
            	    if (answercount.length > 0) {
            	        $scope.DisableSubmit = true;
            	        $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
            	    } else {*/
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.fsquestions[$scope.fsquestioncount].id, '', "fsanswers", "fsradio", $scope.fsquestions[$scope.fsquestioncount].pillarid, $scope.fsquestions[$scope.fsquestioncount].yesaction, $scope.fsquestions[$scope.fsquestioncount].noaction, $scope.fsquestions[$scope.fsquestioncount].yespopup, $scope.fsquestions[$scope.fsquestioncount].nopopup, $scope.fsquestions[$scope.fsquestioncount].yesskipto, $scope.fsquestions[$scope.fsquestioncount].noskipto, $scope.fsquestions[$scope.fsquestioncount].schemeslno, $scope.fsquestions[$scope.fsquestioncount].serialno, $scope.fsquestions[$scope.fsquestioncount].yesincrement, $scope.fsquestions[$scope.fsquestioncount].noincrement, $scope.fsquestions[$scope.fsquestioncount].yesprovideinfo, $scope.fsquestions[$scope.fsquestioncount].noprovideinfo, $scope.fsquestions[$scope.fsquestioncount].question, $scope.fsquestions[$scope.fsquestioncount].yestodotype, $scope.fsquestions[$scope.fsquestioncount].notodotype, $scope.fsquestions[$scope.fsquestioncount].yesdocumentflag, $scope.fsquestions[$scope.fsquestioncount].nodocumentflag, $scope.fsquestions[$scope.fsquestioncount].yesdocumentid, $scope.fsquestions[$scope.fsquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.fsquestions[$scope.fsquestioncount].hnyesprovideinfo, $scope.fsquestions[$scope.fsquestioncount].hnnoprovideinfo, $scope.fsquestions[$scope.fsquestioncount].hnquestion
                /****************************Suman Changes End****************************************/
            );
            //}
            //  });
        } else {
            $scope.fsquestioncount = $scope.fsquestions.length - 1;
            $scope.fssurveyquestion = $scope.fsquestions[$scope.fsquestioncount];
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.fsquestions[$scope.fsquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    if ($scope.fssurveyquestion.skipquestion == true && $scope.fssurveyquestion.skipon == answered[0].answer) {
                        if ($scope.fssurveyquestion.skipdocument == true && $scope.fssurveyquestion.stage == true) {
                            $scope.fsquestioncount++;
                        }
                        $scope.fsnextquestion();
                        console.log("no answer check if", "if");
                    } else if ($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialliteracy') {
                        //console.log('SHI H');
                        //Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                        if ($scope.DisplayBeneficiary.savings == true && $scope.DisplayBeneficiary.insurance == true && $scope.DisplayBeneficiary.pension == true && $scope.DisplayBeneficiary.credit == true) {
                            $scope.fsnextquestion();
                            console.log('skipfinancialliteracy');
                        }
                        //});
                    } else if ($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialplanning') {
                        //console.log('SHI H');
                        //Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                        if ($scope.DisplayBeneficiary.finplansavings == true && $scope.DisplayBeneficiary.finplaninsurance == true && $scope.DisplayBeneficiary.finplanpension == true && $scope.DisplayBeneficiary.finplancredit == true) {
                            $scope.fsnextquestion();
                            console.log('skipfinancialplanning3');
                        }
                        //});
                    } else if ($scope.fssurveyquestion.skipdocument == true && $scope.fssurveyquestion.stage == true) {
                        console.log("no answer skipdocument", "stage");
                        Restangular.all('schememasters?filter[where][memberId]=' + $routeParams.id + '&filter[where][schemeId]=' + $scope.fssurveyquestion.skipondocument).getList().then(function (schemstrs) {
                            if (schemstrs.length > 0 && (schemstrs[0].stage == 9 || (schemstrs[0] == 4 && schemstrs[0].responserecieve === 'Rejected'))) {
                                $scope.fsnextquestion();
                            } else if (schemstrs.length > 0 && schemstrs[0].stage == 7) {
                                $scope.fsquestioncount++;
                                $scope.fsnextquestion();
                            } else {
                                //$scope.fsquestioncount--;
                                //$scope.fsnextquestion();
                                if ($window.sessionStorage.language == 1) {
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = enganswers[ansindex];
                                } else if ($window.sessionStorage.language == 2) {
                                    var hindianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = hindianswers[ansindex];
                                } else if ($window.sessionStorage.language == 3) {
                                    var kannadanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = kannadanswers[ansindex];
                                } else if ($window.sessionStorage.language == 4) {
                                    var tamilanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = tamilanswers[ansindex];
                                } else if ($window.sessionStorage.language == 5) {
                                    var teluguanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = teluguanswers[ansindex];
                                } else if ($window.sessionStorage.language == 6) {
                                    var maratianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = maratianswers[ansindex];
                                }
                            }
                        });
                    } else if ($scope.fssurveyquestion.skipdocument == true && $scope.fssurveyquestion.stage != true) {
                        console.log("no answer skipdocument", "stage false");
                        Restangular.all('schememasters?filter[where][memberId]=' + $routeParams.id + '&filter[where][schemeId]=' + $scope.fssurveyquestion.skipondocument).getList().then(function (schemstrs) {
                            if (schemstrs.length > 0) {
                                $scope.fsnextquestion();
                            } else {
                                // $scope.fsquestioncount--;
                                //$scope.fsnextquestion();
                                if ($window.sessionStorage.language == 1) {
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = enganswers[ansindex];
                                } else if ($window.sessionStorage.language == 2) {
                                    var hindianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = hindianswers[ansindex];
                                } else if ($window.sessionStorage.language == 3) {
                                    var kannadanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = kannadanswers[ansindex];
                                } else if ($window.sessionStorage.language == 4) {
                                    var tamilanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = tamilanswers[ansindex];
                                } else if ($window.sessionStorage.language == 5) {
                                    var teluguanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = teluguanswers[ansindex];
                                } else if ($window.sessionStorage.language == 6) {
                                    var maratianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = maratianswers[ansindex];
                                }
                            }
                        });
                    } else {
                        console.log("no answer else", "else");
                        //$scope.fssurveyanswer = answered[0].answer;
                        /******************************sp New Changes *******************************/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {
                            var hindianswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = hindianswers[ansindex];
                        } else if ($window.sessionStorage.language == 3) {
                            var kannadanswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            var tamilanswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            var teluguanswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            var maratianswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("fsanswers");
            mydiv.innerHTML = '';
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.fsquestions[$scope.fsquestioncount].id, '', "fsanswers", "fsradio", $scope.fsquestions[$scope.fsquestioncount].pillarid, $scope.fsquestions[$scope.fsquestioncount].yesaction, $scope.fsquestions[$scope.fsquestioncount].noaction, $scope.fsquestions[$scope.fsquestioncount].yespopup, $scope.fsquestions[$scope.fsquestioncount].nopopup, $scope.fsquestions[$scope.fsquestioncount].yesskipto, $scope.fsquestions[$scope.fsquestioncount].noskipto, $scope.fsquestions[$scope.fsquestioncount].schemeslno, $scope.fsquestions[$scope.fsquestioncount].serialno, $scope.fsquestions[$scope.fsquestioncount].yesincrement, $scope.fsquestions[$scope.fsquestioncount].noincrement, $scope.fsquestions[$scope.fsquestioncount].yesprovideinfo, $scope.fsquestions[$scope.fsquestioncount].noprovideinfo, $scope.fsquestions[$scope.fsquestioncount].question, $scope.fsquestions[$scope.fsquestioncount].yestodotype, $scope.fsquestions[$scope.fsquestioncount].notodotype, $scope.fsquestions[$scope.fsquestioncount].yesdocumentflag, $scope.fsquestions[$scope.fsquestioncount].nodocumentflag, $scope.fsquestions[$scope.fsquestioncount].yesdocumentid, $scope.fsquestions[$scope.fsquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.fsquestions[$scope.fsquestioncount].hnyesprovideinfo, $scope.fsquestions[$scope.fsquestioncount].hnnoprovideinfo, $scope.fsquestions[$scope.fsquestioncount].hnquestion
                /****************************Suman Changes End****************************************/
            );
            $scope.goToIDS();
        }
    };
    $scope.fsprevquestion = function () {
        $scope.fsquestioncount--;
        if ($scope.fsquestioncount >= 0) {
            $scope.DisableFSPrev = true;
            //console.log('nextquestion', $scope.healthquestioncount);
            $scope.fssurveyquestion = $scope.fsquestions[$scope.fsquestioncount];
            //console.log('sumanfs fsprevquestion', $scope.fssurveyquestion);
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.fsquestions[$scope.fsquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    /*if ($scope.fssurveyquestion.yesaction === 'popup' && $scope.fssurveyquestion.yespopup === 'financialliteracy' && $scope.DisplayBeneficiary.savings == true && $scope.DisplayBeneficiary.insurance == true && $scope.DisplayBeneficiary.pension == true && $scope.DisplayBeneficiary.credit == true) {
                    		console.log('Condition True');
                    		$scope.fsnextquestion();
                    	} else if ($scope.fssurveyquestion.yesaction == 'popup' && $scope.fssurveyquestion.yespopup == 'financialplanning' && $scope.DisplayBeneficiary.finplansavings == true && $scope.DisplayBeneficiary.finplaninsurance == true && $scope.DisplayBeneficiary.finplanpension == true && $scope.DisplayBeneficiary.finplancredit == true) {
                    		console.log('Condition True2');
                    		$scope.fsnextquestion();
                    	} else*/
                    if ($scope.fssurveyquestion.skipdocument == true && $scope.fssurveyquestion.stage == true) {
                        Restangular.all('schememasters?filter[where][memberId]=' + $routeParams.id + '&filter[where][schemeId]=' + $scope.fssurveyquestion.skipondocument).getList().then(function (schemstrs) {
                            if (schemstrs.length > 0 && (schemstrs[0].stage == 9 || schemstrs[0].stage == 7 || (schemstrs[0] == 4 && schemstrs[0].responserecieve === 'Rejected'))) {
                                $scope.fsnextquestion();
                            } else {
                                //$scope.fsquestioncount--;
                                //$scope.fsnextquestion();
                                if ($window.sessionStorage.language == 1) {
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = enganswers[ansindex];
                                } else if ($window.sessionStorage.language == 2) {
                                    var hindianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = hindianswers[ansindex];
                                } else if ($window.sessionStorage.language == 3) {
                                    var kannadanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = kannadanswers[ansindex];
                                } else if ($window.sessionStorage.language == 4) {
                                    var tamilanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = tamilanswers[ansindex];
                                } else if ($window.sessionStorage.language == 5) {
                                    var teluguanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = teluguanswers[ansindex];
                                } else if ($window.sessionStorage.language == 6) {
                                    var maratianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = maratianswers[ansindex];
                                }
                            }
                        });
                    } else if ($scope.fssurveyquestion.skipdocument == true && $scope.fssurveyquestion.stage != true) {
                        Restangular.all('schememasters?filter[where][memberId]=' + $routeParams.id + '&filter[where][schemeId]=' + $scope.fssurveyquestion.skipondocument).getList().then(function (schemstrs) {
                            if (schemstrs.length > 0) {
                                $scope.fsnextquestion();
                            } else {
                                //$scope.fsquestioncount--;
                                //$scope.fsnextquestion();
                                if ($window.sessionStorage.language == 1) {
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = enganswers[ansindex];
                                } else if ($window.sessionStorage.language == 2) {
                                    var hindianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = hindianswers[ansindex];
                                } else if ($window.sessionStorage.language == 3) {
                                    var kannadanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = kannadanswers[ansindex];
                                } else if ($window.sessionStorage.language == 4) {
                                    var tamilanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = tamilanswers[ansindex];
                                } else if ($window.sessionStorage.language == 5) {
                                    var teluguanswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = teluguanswers[ansindex];
                                } else if ($window.sessionStorage.language == 6) {
                                    var maratianswers = [];
                                    if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    } else {
                                        maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                                    }
                                    var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                                    var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                    $scope.fssurveyanswer = maratianswers[ansindex];
                                }
                            }
                        });
                    } else {
                        //$scope.fssurveyanswer = answered[0].answer;
                        /**************** new Changes ***********/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {
                            var hindianswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.fsquestions[$scope.fsquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = hindianswers[ansindex];
                        } else if ($window.sessionStorage.language == 3) {
                            var kannadanswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.fsquestions[$scope.fsquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            var tamilanswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.fsquestions[$scope.fsquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            var teluguanswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.fsquestions[$scope.fsquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            var maratianswers = [];
                            if ($scope.fsquestions[$scope.fsquestioncount].maansweroptions == null) {
                                maratianswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.fsquestions[$scope.fsquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.fsquestions[$scope.fsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.fssurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("fsanswers");
            mydiv.innerHTML = '';
            // $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id);
            /*CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

            	    //console.log('answercount', answercount.count);
            	    if (answercount.length > 0) {
            	        $scope.DisableSubmit = true;
            	        $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
            	    } else {*/
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.fsquestions[$scope.fsquestioncount].id, '', "fsanswers", "fsradio", $scope.fsquestions[$scope.fsquestioncount].pillarid, $scope.fsquestions[$scope.fsquestioncount].yesaction, $scope.fsquestions[$scope.fsquestioncount].noaction, $scope.fsquestions[$scope.fsquestioncount].yespopup, $scope.fsquestions[$scope.fsquestioncount].nopopup, $scope.fsquestions[$scope.fsquestioncount].yesskipto, $scope.fsquestions[$scope.fsquestioncount].noskipto, $scope.fsquestions[$scope.fsquestioncount].schemeslno, $scope.fsquestions[$scope.fsquestioncount].serialno, $scope.fsquestions[$scope.fsquestioncount].yesincrement, $scope.fsquestions[$scope.fsquestioncount].noincrement, $scope.fsquestions[$scope.fsquestioncount].yesprovideinfo, $scope.fsquestions[$scope.fsquestioncount].noprovideinfo, $scope.fsquestions[$scope.fsquestioncount].question, $scope.fsquestions[$scope.fsquestioncount].yestodotype, $scope.fsquestions[$scope.fsquestioncount].notodotype, $scope.fsquestions[$scope.fsquestioncount].yesdocumentflag, $scope.fsquestions[$scope.fsquestioncount].nodocumentflag, $scope.fsquestions[$scope.fsquestioncount].yesdocumentid, $scope.fsquestions[$scope.fsquestioncount].nodocumentid);
            //}
            //  });
        } else {
            $scope.fsquestioncount++;
        }
    };
    $scope.CallFS = function () {
        $scope.displayHealthPrev = false;
        $scope.displaySSJPrev = false;
        $scope.displaySPPrev = false;
        $scope.displayFSPrev = true;
        $scope.displayIDSPrev = false;
        $scope.fstdos = Restangular.all('todos?filter[where][pillarid]=3&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (fstdos) {
            $scope.fstodos = fstdos;
            angular.forEach($scope.fstodos, function (member, index) {
                member.index = index + 1;
                for (var m = 0; m < $scope.maintodotypes.length; m++) {
                    if (member.todotype == $scope.maintodotypes[m].id) {
                        member.TodoType = $scope.maintodotypes[m];
                        break;
                    }
                }
                for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                    if (member.status == $scope.maintodostatuses[n].id) {
                        member.TodoStatus = $scope.maintodostatuses[n];
                        break;
                    }
                }
            });
        });
        $scope.fsevents = Restangular.all('events?filter[where][pillarid]=3&filter[where][deleteflag]=false&filter[where][order]=datetime%20DESC&filter[limit]=5&filter[where][datetime][gte]=' + $scope.TodayDate + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().$object;
    }
    ///////////////////////FS END/////////////////////////////
    ///////////////////////IDS START/////////////////////////////
    $scope.idsques = Restangular.all('surveyquestions?filter[where][pillarid]=4&filter[order]=serialno%20ASC').getList().then(function (idssurveyques) {
        //console.log('partner',partner);
        for (var i = 0; i < idssurveyques.length; i++) {
            $scope.idsdata = {};
            /****************************Suman Changes Start****************************************/
            if ($window.sessionStorage.language == 1) {
                $scope.idsdata['question'] = idssurveyques[i].question;
                $scope.idsdata['answers'] = idssurveyques[i].answeroptions;
                $scope.idsdata['yesprovideinfo'] = idssurveyques[i].yesprovideinfo;
                $scope.idsdata['noprovideinfo'] = idssurveyques[i].noprovideinfo;
            } else if ($window.sessionStorage.language == 2) {
                $scope.idsdata['question'] = idssurveyques[i].hnquestion;
                $scope.idsdata['answers'] = idssurveyques[i].answeroptions;
                $scope.idsdata['yesprovideinfo'] = idssurveyques[i].hnyesprovideinfo;
                $scope.idsdata['noprovideinfo'] = idssurveyques[i].hnnoprovideinfo;
            } else if ($window.sessionStorage.language == 3) {
                $scope.idsdata['question'] = idssurveyques[i].kaquestion;
                $scope.idsdata['answers'] = idssurveyques[i].answeroptions;
                $scope.idsdata['yesprovideinfo'] = idssurveyques[i].kayesprovideinfo;
                $scope.idsdata['noprovideinfo'] = idssurveyques[i].kanoprovideinfo;
            } else if ($window.sessionStorage.language == 4) {
                $scope.idsdata['question'] = idssurveyques[i].taquestion;
                $scope.idsdata['answers'] = idssurveyques[i].answeroptions;
                $scope.idsdata['yesprovideinfo'] = idssurveyques[i].tayesprovideinfo;
                $scope.idsdata['noprovideinfo'] = idssurveyques[i].tanoprovideinfo;
            } else if ($window.sessionStorage.language == 5) {
                $scope.idsdata['question'] = idssurveyques[i].tequestion;
                $scope.idsdata['answers'] = idssurveyques[i].answeroptions;
                $scope.idsdata['yesprovideinfo'] = idssurveyques[i].teyesprovideinfo;
                $scope.idsdata['noprovideinfo'] = idssurveyques[i].tenoprovideinfo;
            } else if ($window.sessionStorage.language == 6) {
                $scope.idsdata['question'] = idssurveyques[i].maquestion;
                $scope.idsdata['answers'] = idssurveyques[i].answeroptions;
                $scope.idsdata['yesprovideinfo'] = idssurveyques[i].mayesprovideinfo;
                $scope.idsdata['noprovideinfo'] = idssurveyques[i].manoprovideinfo;
            }
            /****************************Suman Changes End****************************************/
            //$scope.idsdata['question'] = idssurveyques[i].question;
            $scope.idsdata['pillarid'] = idssurveyques[i].pillarid;
            $scope.idsdata['questiontype'] = idssurveyques[i].questiontype;
            $scope.idsdata['noofdropdown'] = idssurveyques[i].noofoptions;
            $scope.idsdata['id'] = idssurveyques[i].id;
            //$scope.idsdata['answers'] = idssurveyques[i].answeroptions;
            $scope.idsdata['yesaction'] = idssurveyques[i].yesaction;
            $scope.idsdata['noaction'] = idssurveyques[i].noaction;
            $scope.idsdata['yespopup'] = idssurveyques[i].yespopup;
            $scope.idsdata['nopopup'] = idssurveyques[i].nopopup;
            $scope.idsdata['yesskipto'] = idssurveyques[i].yesskipto;
            $scope.idsdata['noskipto'] = idssurveyques[i].noskipto;
            $scope.idsdata['schemeslno'] = idssurveyques[i].schemeslno;
            $scope.idsdata['serialno'] = idssurveyques[i].serialno;
            $scope.idsdata['yesincrement'] = idssurveyques[i].yesincrement;
            $scope.idsdata['noincrement'] = idssurveyques[i].noincrement;
            //$scope.idsdata['yesprovideinfo'] = idssurveyques[i].yesprovideinfo;
            //$scope.idsdata['noprovideinfo'] = idssurveyques[i].noprovideinfo;
            $scope.idsdata['yestodotype'] = idssurveyques[i].yestodotype;
            $scope.idsdata['notodotype'] = idssurveyques[i].notodotype;
            $scope.idsdata['yesdocumentflag'] = idssurveyques[i].yesdocumentflag;
            $scope.idsdata['nodocumentflag'] = idssurveyques[i].nodocumentflag;
            $scope.idsdata['yesdocumentid'] = idssurveyques[i].yesdocumentid;
            $scope.idsdata['nodocumentid'] = idssurveyques[i].nodocumentid;
            $scope.idsdata['teansweroptions'] = idssurveyques[i].teansweroptions;
            $scope.idsdata['hnansweroptions'] = idssurveyques[i].hnansweroptions;
            $scope.idsdata['kaansweroptions'] = idssurveyques[i].kaansweroptions;
            $scope.idsdata['taansweroptions'] = idssurveyques[i].taansweroptions;
            $scope.idsdata['maansweroptions'] = idssurveyques[i].maansweroptions;
            $scope.idsdata['skipquestion'] = idssurveyques[i].skipquestion;
            $scope.idsdata['skipon'] = idssurveyques[i].skipon;
            $scope.idsquestions.push($scope.idsdata);
            if (i == idssurveyques.length - 1) {
                $scope.idssurveyquestion = $scope.idsquestions[$scope.idsquestioncount];
                $scope.IDSLoaded = true;
                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.idsquestions[$scope.idsquestioncount].id).getList().then(function (answered) {
                    if (answered.length > 0) {
                        if ($scope.idssurveyquestion.skipquestion == true && $scope.idssurveyquestion.skipon == answered[0].answer) {
                            $scope.idsnextquestion();
                        } else {
                            //$scope.idssurveyanswer = answered[0].answer;
                            /*************** fs new change ************/
                            if ($window.sessionStorage.language == 1) {
                                var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.idssurveyanswer = enganswers[ansindex];
                            } else if ($window.sessionStorage.language == 2) {
                                var hindianswers = [];
                                if ($scope.idsquestions[$scope.idsquestioncount].hnansweroptions == null) {
                                    hindianswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                                } else {
                                    hindianswers = $scope.idsquestions[$scope.idsquestioncount].hnansweroptions.split(',');
                                }
                                var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.idssurveyanswer = hindianswers[ansindex];
                                console.log('$scope.idssurveyanswer', $scope.idssurveyanswer);
                            } else if ($window.sessionStorage.language == 3) {
                                var kannadanswers = [];
                                if ($scope.idsquestions[$scope.idsquestioncount].kaansweroptions == null) {
                                    kannadanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                                } else {
                                    kannadanswers = $scope.idsquestions[$scope.idsquestioncount].kaansweroptions.split(',');
                                }
                                var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.idssurveyanswer = kannadanswers[ansindex];
                            } else if ($window.sessionStorage.language == 4) {
                                var tamilanswers = [];
                                if ($scope.idsquestions[$scope.idsquestioncount].taansweroptions == null) {
                                    tamilanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                                } else {
                                    tamilanswers = $scope.idsquestions[$scope.idsquestioncount].taansweroptions.split(',');
                                }
                                var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.idssurveyanswer = tamilanswers[ansindex];
                            } else if ($window.sessionStorage.language == 5) {
                                var teluguanswers = [];
                                if ($scope.idsquestions[$scope.idsquestioncount].teansweroptions == null) {
                                    teluguanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                                } else {
                                    teluguanswers = $scope.idsquestions[$scope.idsquestioncount].teansweroptions.split(',');
                                }
                                var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.idssurveyanswer = teluguanswers[ansindex];
                            } else if ($window.sessionStorage.language == 6) {
                                var maratianswers = [];
                                if ($scope.idsquestions[$scope.idsquestioncount].maansweroptions == null) {
                                    maratianswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                                } else {
                                    maratianswers = $scope.idsquestions[$scope.idsquestioncount].maansweroptions.split(',');
                                }
                                var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.idssurveyanswer = maratianswers[ansindex];
                            }
                        }
                    }
                });
                if ($scope.healthLoaded == true && $scope.SSJLoaded == true && $scope.FSLoaded == true && $scope.SPLoaded == true && $scope.IDSLoaded == true) {
                    if ($routeParams.id != undefined) {
                        Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
                            if (beneficiary.completedpillar != 0) {
                                //console.log(beneficiary.completedpillar, beneficiary.completedquestion);
                                $scope.goToPillarQuestion(beneficiary.completedpillar, beneficiary.completedquestion);
                            } else {
                                $scope.modalInstanceLoad.close();
                            }
                        });
                    }
                }
                /*  CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

                	      // console.log('answercount', answercount);
                	      if (answercount.length > 0) {
                	          $scope.DisableSubmit = true;
                	          $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
                	      } else {*/
                $scope.DisableSubmit = false;
                $scope.ChangeClient($scope.idsquestions[$scope.idsquestioncount].id, '', "idsanswers", "idsradio", $scope.idsquestions[$scope.idsquestioncount].pillarid, $scope.idsquestions[$scope.idsquestioncount].yesaction, $scope.idsquestions[$scope.idsquestioncount].noaction, $scope.idsquestions[$scope.idsquestioncount].yespopup, $scope.idsquestions[$scope.idsquestioncount].nopopup, $scope.idsquestions[$scope.idsquestioncount].yesskipto, $scope.idsquestions[$scope.idsquestioncount].noskipto, $scope.idsquestions[$scope.idsquestioncount].schemeslno, $scope.idsquestions[$scope.idsquestioncount].serialno, $scope.idsquestions[$scope.idsquestioncount].yesincrement, $scope.idsquestions[$scope.idsquestioncount].noincrement, $scope.idsquestions[$scope.idsquestioncount].yesprovideinfo, $scope.idsquestions[$scope.idsquestioncount].noprovideinfo, $scope.idsquestions[$scope.idsquestioncount].question, $scope.idsquestions[$scope.idsquestioncount].yestodotype, $scope.idsquestions[$scope.idsquestioncount].notodotype, $scope.idsquestions[$scope.idsquestioncount].yesdocumentflag, $scope.idsquestions[$scope.idsquestioncount].nodocumentflag, $scope.idsquestions[$scope.idsquestioncount].yesdocumentid, $scope.idsquestions[$scope.idsquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.idsquestions[$scope.idsquestioncount].hnyesprovideinfo, $scope.idsquestions[$scope.idsquestioncount].hnnoprovideinfo, $scope.idsquestions[$scope.idsquestioncount].hnquestion
                    /****************************Suman Changes End****************************************/
                );
                //  }
                //  });
            }
        }
    });
    $scope.idsnextquestion = function () {
        //console.log("idsnextquestion", "idsnextquestion");
        $scope.DisableIDSPrev = false;
        $scope.idsquestioncount++;
        if ($scope.idsquestioncount < $scope.idsquestions.length) {
            //console.log('nextquestion', $scope.healthquestioncount);
            $scope.idssurveyquestion = $scope.idsquestions[$scope.idsquestioncount];
            //console.log('$scope.idssurveyquestion', $scope.idssurveyquestion);
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.idsquestions[$scope.idsquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    //$scope.idssurveyanswer = answered[0].answer;
                    /**************** sp new Changes **************/
                    if ($scope.prevquestionClick == true) {
                        $scope.idsquestioncount--;
                        $scope.prevquestionClick = false
                    } else
                    if ($scope.idssurveyquestion.skipquestion == true && $scope.idssurveyquestion.skipon == answered[0].answer) {
                        $scope.idsnextquestion();
                    } else {
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {
                            var hindianswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.idsquestions[$scope.idsquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = hindianswers[ansindex];
                        } else if ($window.sessionStorage.language == 3) {
                            var kannadanswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.idsquestions[$scope.idsquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            var tamilanswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.idsquestions[$scope.idsquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            var teluguanswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.idsquestions[$scope.idsquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            //var maratianswers = $scope.idsquestions[$scope.idsquestioncount].maansweroptions.split(',');
                            var maratianswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].maansweroptions == null) {
                                maratianswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.idsquestions[$scope.idsquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("idsanswers");
            mydiv.innerHTML = '';
            // $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id);
            /*CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

            	    //console.log('answercount', answercount.count);
            	    if (answercount.length > 0) {
            	        $scope.DisableSubmit = true;
            	        $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
            	    } else {*/
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.idsquestions[$scope.idsquestioncount].id, '', "idsanswers", "idsradio", $scope.idsquestions[$scope.idsquestioncount].pillarid, $scope.idsquestions[$scope.idsquestioncount].yesaction, $scope.idsquestions[$scope.idsquestioncount].noaction, $scope.idsquestions[$scope.idsquestioncount].yespopup, $scope.idsquestions[$scope.idsquestioncount].nopopup, $scope.idsquestions[$scope.idsquestioncount].yesskipto, $scope.idsquestions[$scope.idsquestioncount].noskipto, $scope.idsquestions[$scope.idsquestioncount].schemeslno, $scope.idsquestions[$scope.idsquestioncount].serialno, $scope.idsquestions[$scope.idsquestioncount].yesincrement, $scope.idsquestions[$scope.idsquestioncount].noincrement, $scope.idsquestions[$scope.idsquestioncount].yesprovideinfo, $scope.idsquestions[$scope.idsquestioncount].noprovideinfo, $scope.idsquestions[$scope.idsquestioncount].question, $scope.idsquestions[$scope.idsquestioncount].yestodotype, $scope.idsquestions[$scope.idsquestioncount].notodotype, $scope.idsquestions[$scope.idsquestioncount].yesdocumentflag, $scope.idsquestions[$scope.idsquestioncount].nodocumentflag, $scope.idsquestions[$scope.idsquestioncount].yesdocumentid, $scope.idsquestions[$scope.idsquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.idsquestions[$scope.idsquestioncount].hnyesprovideinfo, $scope.idsquestions[$scope.idsquestioncount].hnnoprovideinfo, $scope.idsquestions[$scope.idsquestioncount].hnquestion
                /****************************Suman Changes End****************************************/
            );
            //}
            //  });
        } else {
            //console.log("$scope.idsquestioncount", $scope.idsquestioncount);
            //console.log("$scope.idsquestions.length", $scope.idsquestions.length);
            // console.log('$scope.idsquestioncount', $scope.idsquestioncount);
            //console.log('$scope.idssurveyquestion', '$scope.idssurveyquestion');
            $scope.idsquestioncount = $scope.idsquestions.length - 1;
            $scope.idssurveyquestion = $scope.idsquestions[$scope.idsquestioncount];
            // console.log('$scope.idssurveyquestion', $scope.idssurveyquestion);
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.idsquestions[$scope.idsquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    if ($scope.idssurveyquestion.skipquestion == true && $scope.idssurveyquestion.skipon == answered[0].answer) {
                        $scope.openPillarCompleted();
                    } else {
                        //$scope.idssurveyanswer = answered[0].answer;
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {
                            var hindianswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.idsquestions[$scope.idsquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = hindianswers[ansindex];
                        } else if ($window.sessionStorage.language == 3) {
                            var kannadanswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.idsquestions[$scope.idsquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            var tamilanswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.idsquestions[$scope.idsquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            var teluguanswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.idsquestions[$scope.idsquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            var maratianswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].maansweroptions == null) {
                                maratianswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.idsquestions[$scope.idsquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("idsanswers");
            mydiv.innerHTML = '';
            // $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id);
            /*CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

            	    //console.log('answercount', answercount.count);
            	    if (answercount.length > 0) {
            	        $scope.DisableSubmit = true;
            	        $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
            	    } else {*/
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.idsquestions[$scope.idsquestioncount].id, '', "idsanswers", "idsradio", $scope.idsquestions[$scope.idsquestioncount].pillarid, $scope.idsquestions[$scope.idsquestioncount].yesaction, $scope.idsquestions[$scope.idsquestioncount].noaction, $scope.idsquestions[$scope.idsquestioncount].yespopup, $scope.idsquestions[$scope.idsquestioncount].nopopup, $scope.idsquestions[$scope.idsquestioncount].yesskipto, $scope.idsquestions[$scope.idsquestioncount].noskipto, $scope.idsquestions[$scope.idsquestioncount].schemeslno, $scope.idsquestions[$scope.idsquestioncount].serialno, $scope.idsquestions[$scope.idsquestioncount].yesincrement, $scope.idsquestions[$scope.idsquestioncount].noincrement, $scope.idsquestions[$scope.idsquestioncount].yesprovideinfo, $scope.idsquestions[$scope.idsquestioncount].noprovideinfo, $scope.idsquestions[$scope.idsquestioncount].question, $scope.idsquestions[$scope.idsquestioncount].yestodotype, $scope.idsquestions[$scope.idsquestioncount].notodotype, $scope.idsquestions[$scope.idsquestioncount].yesdocumentflag, $scope.idsquestions[$scope.idsquestioncount].nodocumentflag, $scope.idsquestions[$scope.idsquestioncount].yesdocumentid, $scope.idsquestions[$scope.idsquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.idsquestions[$scope.idsquestioncount].hnyesprovideinfo, $scope.idsquestions[$scope.idsquestioncount].hnnoprovideinfo, $scope.idsquestions[$scope.idsquestioncount].hnquestion
                /****************************Suman Changes End****************************************/
            );
            //$scope.goToHealth();
            $scope.openPillarCompleted();
        }
    };
    $scope.idsprevquestion = function () {
        $scope.idsquestioncount--;
        if ($scope.idsquestioncount >= 0) {
            $scope.DisableIDSPrev = true;
            //console.log('nextquestion', $scope.healthquestioncount);
            $scope.idssurveyquestion = $scope.idsquestions[$scope.idsquestioncount];
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.idsquestions[$scope.idsquestioncount].id).getList().then(function (answered) {
                if (answered.length > 0) {
                    if ($scope.idssurveyquestion.skipquestion == true && $scope.idssurveyquestion.skipon == answered[0].answer) {
                        $scope.idsnextquestion();
                    } else {
                        //$scope.idssurveyanswer = answered[0].answer;
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {
                            var hindianswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].hnansweroptions == null) {
                                hindianswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.idsquestions[$scope.idsquestioncount].hnansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = hindianswers[ansindex];
                        } else if ($window.sessionStorage.language == 3) {
                            var kannadanswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].kaansweroptions == null) {
                                kannadanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                kannadanswers = $scope.idsquestions[$scope.idsquestioncount].kaansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = kannadanswers[ansindex];
                        } else if ($window.sessionStorage.language == 4) {
                            //var tamilanswers = $scope.idsquestions[$scope.idsquestioncount].taansweroptions.split(',');
                            var tamilanswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].taansweroptions == null) {
                                tamilanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                tamilanswers = $scope.idsquestions[$scope.idsquestioncount].taansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = tamilanswers[ansindex];
                        } else if ($window.sessionStorage.language == 5) {
                            var teluguanswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].teansweroptions == null) {
                                teluguanswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                teluguanswers = $scope.idsquestions[$scope.idsquestioncount].teansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = teluguanswers[ansindex];
                        } else if ($window.sessionStorage.language == 6) {
                            var maratianswers = [];
                            if ($scope.idsquestions[$scope.idsquestioncount].maansweroptions == null) {
                                maratianswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            } else {
                                maratianswers = $scope.idsquestions[$scope.idsquestioncount].maansweroptions.split(',');
                            }
                            var enganswers = $scope.idsquestions[$scope.idsquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.idssurveyanswer = maratianswers[ansindex];
                        }
                    }
                }
            });
            var mydiv = document.getElementById("idsanswers");
            mydiv.innerHTML = '';
            // $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id);
            /*CampRestangular.one('surveyanswers?filter[where][surveysentid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).get().then(function (answercount) {

            	    //console.log('answercount', answercount.count);
            	    if (answercount.length > 0) {
            	        $scope.DisableSubmit = true;
            	        $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, answercount[0].answers);
            	    } else {*/
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.idsquestions[$scope.idsquestioncount].id, '', "idsanswers", "idsradio", $scope.idsquestions[$scope.idsquestioncount].pillarid, $scope.idsquestions[$scope.idsquestioncount].yesaction, $scope.idsquestions[$scope.idsquestioncount].noaction, $scope.idsquestions[$scope.idsquestioncount].yespopup, $scope.idsquestions[$scope.idsquestioncount].nopopup, $scope.idsquestions[$scope.idsquestioncount].yesskipto, $scope.idsquestions[$scope.idsquestioncount].noskipto, $scope.idsquestions[$scope.idsquestioncount].schemeslno, $scope.idsquestions[$scope.idsquestioncount].serialno, $scope.idsquestions[$scope.idsquestioncount].yesincrement, $scope.idsquestions[$scope.idsquestioncount].noincrement, $scope.idsquestions[$scope.idsquestioncount].yesprovideinfo, $scope.idsquestions[$scope.idsquestioncount].noprovideinfo, $scope.idsquestions[$scope.idsquestioncount].question, $scope.idsquestions[$scope.idsquestioncount].yestodotype, $scope.idsquestions[$scope.idsquestioncount].notodotype, $scope.idsquestions[$scope.idsquestioncount].yesdocumentflag, $scope.idsquestions[$scope.idsquestioncount].nodocumentflag, $scope.idsquestions[$scope.idsquestioncount].yesdocumentid, $scope.idsquestions[$scope.idsquestioncount].nodocumentid);
            //}
            //  });
        } else {
            $scope.idsquestioncount++;
        }
    };
    $scope.CallIDS = function () {
        $scope.displayHealthPrev = false;
        $scope.displaySSJPrev = false;
        $scope.displaySPPrev = false;
        $scope.displayFSPrev = false;
        $scope.displayIDSPrev = true;
        $scope.idstdos = Restangular.all('todos?filter[where][pillarid]=4&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (idstdos) {
            $scope.idstodos = idstdos;
            angular.forEach($scope.idstodos, function (member, index) {
                member.index = index + 1;
                for (var m = 0; m < $scope.maintodotypes.length; m++) {
                    if (member.todotype == $scope.maintodotypes[m].id) {
                        member.TodoType = $scope.maintodotypes[m];
                        break;
                    }
                }
                for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                    if (member.status == $scope.maintodostatuses[n].id) {
                        member.TodoStatus = $scope.maintodostatuses[n];
                        break;
                    }
                }
            });
        });
        $scope.idsevents = Restangular.all('events?filter[where][pillarid]=4&filter[where][deleteflag]=false&filter[where][order]=datetime%20DESC&filter[limit]=5&filter[where][datetime][gte]=' + $scope.TodayDate + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().$object;
    }
    ///////////////////////IDS END/////////////////////////////
    $scope.submitsurveyanswers = Restangular.all('surveyanswers');
    $scope.submitallsurveyanswers = Restangular.all('allsurveyanswers');
    $scope.submittodos = Restangular.all('todos');
    $scope.submitreportincidents = Restangular.all('reportincidents');
    $scope.surveyanswer = {};
    $scope.beneficiaryupdate = {};
    //$scope.sourceofinfections = Restangular.all('sourceofinfections').getList().$object;
    //$scope.alltodos = Restangular.all('todos?filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
    $scope.submitdocumentmasters = Restangular.all('schememasters');
    $scope.$watch('pillarId', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '' || newValue == undefined) {
            return;
        } else {
            $scope.TotalTodos = [];
            if (newValue != 2) {
                $scope.displayreportincidents = [];
                $scope.RemainingPillars = 5;
                $scope.zn = Restangular.all('todos?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][pillarid]=' + newValue).getList().then(function (zn) {
                    $scope.todos = zn;
                    angular.forEach($scope.todos, function (member, index) {
                        member.index = index + 1;
                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                            if (member.todotype == $scope.maintodotypes[m].id) {
                                //member.TodoType = $scope.maintodotypes[m].name;
                                member.TodoType = $scope.maintodotypes[m];
                                //console.log('member.TodoType', $scope.maintodotypes[m]);
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                            if (member.status == $scope.maintodostatuses[n].id) {
                                member.TodoStatus = $scope.maintodostatuses[n];
                                break;
                            }
                        }
                        for (var o = 0; o < $scope.maintodopillars.length; o++) {
                            if (member.pillarid == $scope.maintodopillars[o].id) {
                                member.TodoPillar = $scope.maintodopillars[o];
                                break;
                            }
                        }
                        $scope.TotalTodos.push(member);
                        //member.purpose = $scope.getTodopurpose(member.purpose);
                    });
                });
            } else {
                $scope.todos = [];
                $scope.SSJPillar = 5;
                $scope.rprt = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (report) {
                    $scope.displayreportincidents = report;
                    angular.forEach($scope.displayreportincidents, function (member, index) {
                        member.index = index + 1;
                        member.pillarid = 2;
                        member.todotype = 27;
                        member.datetime = member.followupdate;
                        //member.purpose = $scope.getTodopurpose(member.purpose);
                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                            if (member.todotype == $scope.maintodotypes[m].id) {
                                //member.TodoType = $scope.maintodotypes[m].name;
                                member.TodoType = $scope.maintodotypes[m];
                                //console.log('member.TodoType', $scope.maintodotypes[m]);
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.mainreporttodostatuses.length; n++) {
                            if (member.currentstatus == $scope.mainreporttodostatuses[n].name) {
                                member.TodoStatus = $scope.mainreporttodostatuses[n];
                                break;
                            }
                        }
                        for (var o = 0; o < $scope.maintodopillars.length; o++) {
                            if (member.pillarid == $scope.maintodopillars[o].id) {
                                member.TodoPillar = $scope.maintodopillars[o];
                                break;
                            }
                        }
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        }
    });
    $scope.$watch('statusDate', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '' || newValue == undefined) {
            return;
        } else {
            $scope.TotalTodos = [];
            $scope.todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            if (newValue == 'closed') {
                $scope.RemainingPillars = 3;
                $scope.SSJPillar = 2;
                $scope.zn = Restangular.all('todos?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][status]=3').getList().then(function (zn) {
                    $scope.todos = zn;
                    angular.forEach($scope.todos, function (member, index) {
                        member.index = index + 1;
                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                            if (member.todotype == $scope.maintodotypes[m].id) {
                                //member.TodoType = $scope.maintodotypes[m].name;
                                member.TodoType = $scope.maintodotypes[m];
                                //console.log('member.TodoType', $scope.maintodotypes[m]);
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                            if (member.status == $scope.maintodostatuses[n].id) {
                                member.TodoStatus = $scope.maintodostatuses[n];
                                break;
                            }
                        }
                        for (var o = 0; o < $scope.maintodopillars.length; o++) {
                            if (member.pillarid == $scope.maintodopillars[o].id) {
                                member.TodoPillar = $scope.maintodopillars[o];
                                break;
                            }
                        }
                        $scope.TotalTodos.push(member);
                        //member.purpose = $scope.getTodopurpose(member.purpose);
                    });
                });
                $scope.rprt = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][currentstatus]=Closed').getList().then(function (report) {
                    $scope.displayreportincidents = report;
                    angular.forEach($scope.displayreportincidents, function (member, index) {
                        member.index = index + 1;
                        member.pillarid = 2;
                        member.todotype = 27;
                        member.datetime = member.followupdate;
                        //member.purpose = $scope.getTodopurpose(member.purpose);
                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                            if (member.todotype == $scope.maintodotypes[m].id) {
                                //member.TodoType = $scope.maintodotypes[m].name;
                                member.TodoType = $scope.maintodotypes[m];
                                //console.log('member.TodoType', $scope.maintodotypes[m]);
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.mainreporttodostatuses.length; n++) {
                            if (member.currentstatus == $scope.mainreporttodostatuses[n].name) {
                                member.TodoStatus = $scope.mainreporttodostatuses[n];
                                break;
                            }
                        }
                        for (var o = 0; o < $scope.maintodopillars.length; o++) {
                            if (member.pillarid == $scope.maintodopillars[o].id) {
                                member.TodoPillar = $scope.maintodopillars[o];
                                break;
                            }
                        }
                        $scope.TotalTodos.push(member);
                    });
                });
            }
            if (newValue == 'Overdue') {
                $scope.RemainingPillars = 3;
                $scope.SSJPillar = 2;
                $scope.zn = Restangular.all('todos?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][datetime][lt]=' + $scope.todayDate + 'T00:00:00.000Z&filter[where][status][lt]=3').getList().then(function (zn) {
                    $scope.todos = zn;
                    angular.forEach($scope.todos, function (member, index) {
                        member.index = index + 1;
                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                            if (member.todotype == $scope.maintodotypes[m].id) {
                                //member.TodoType = $scope.maintodotypes[m].name;
                                member.TodoType = $scope.maintodotypes[m];
                                //console.log('member.TodoType', $scope.maintodotypes[m]);
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                            if (member.status == $scope.maintodostatuses[n].id) {
                                member.TodoStatus = $scope.maintodostatuses[n];
                                break;
                            }
                        }
                        for (var o = 0; o < $scope.maintodopillars.length; o++) {
                            if (member.pillarid == $scope.maintodopillars[o].id) {
                                member.TodoPillar = $scope.maintodopillars[o];
                                break;
                            }
                        }
                        $scope.TotalTodos.push(member);
                        //member.purpose = $scope.getTodopurpose(member.purpose);
                    });
                });
                $scope.rprt = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][followupdate][lt]=' + $scope.todayDate + 'T00:00:00.000Z&filter[where][currentstatus][nlike]=Closed%').getList().then(function (report) {
                    $scope.displayreportincidents = report;
                    angular.forEach($scope.displayreportincidents, function (member, index) {
                        member.index = index + 1;
                        member.pillarid = 2;
                        member.todotype = 27;
                        member.datetime = member.followupdate;
                        //member.purpose = $scope.getTodopurpose(member.purpose);
                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                            if (member.todotype == $scope.maintodotypes[m].id) {
                                //member.TodoType = $scope.maintodotypes[m].name;
                                member.TodoType = $scope.maintodotypes[m];
                                //console.log('member.TodoType', $scope.maintodotypes[m]);
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.mainreporttodostatuses.length; n++) {
                            if (member.currentstatus == $scope.mainreporttodostatuses[n].name) {
                                member.TodoStatus = $scope.mainreporttodostatuses[n];
                                break;
                            }
                        }
                        for (var o = 0; o < $scope.maintodopillars.length; o++) {
                            if (member.pillarid == $scope.maintodopillars[o].id) {
                                member.TodoPillar = $scope.maintodopillars[o];
                                break;
                            }
                        }
                        $scope.TotalTodos.push(member);
                    });
                });
            }
            if (newValue == 'Due') {
                $scope.RemainingPillars = 3;
                $scope.SSJPillar = 2;
                $scope.zn = Restangular.all('todos?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z&filter[where][status][lt]=3').getList().then(function (zn) {
                    $scope.todos = zn;
                    angular.forEach($scope.todos, function (member, index) {
                        member.index = index + 1;
                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                            if (member.todotype == $scope.maintodotypes[m].id) {
                                //member.TodoType = $scope.maintodotypes[m].name;
                                member.TodoType = $scope.maintodotypes[m];
                                //console.log('member.TodoType', $scope.maintodotypes[m]);
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                            if (member.status == $scope.maintodostatuses[n].id) {
                                member.TodoStatus = $scope.maintodostatuses[n];
                                break;
                            }
                        }
                        for (var o = 0; o < $scope.maintodopillars.length; o++) {
                            if (member.pillarid == $scope.maintodopillars[o].id) {
                                member.TodoPillar = $scope.maintodopillars[o];
                                break;
                            }
                        }
                        $scope.TotalTodos.push(member);
                        //member.purpose = $scope.getTodopurpose(member.purpose);
                    });
                });
                $scope.rprt = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][followupdate][lt]=' + $scope.todayDate + 'T00:00:00.000Z&filter[where][currentstatus][nlike]=Closed%').getList().then(function (report) {
                    $scope.displayreportincidents = report;
                    angular.forEach($scope.displayreportincidents, function (member, index) {
                        member.index = index + 1;
                        member.pillarid = 2;
                        member.todotype = 27;
                        member.datetime = member.followupdate;
                        //member.purpose = $scope.getTodopurpose(member.purpose);
                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                            if (member.todotype == $scope.maintodotypes[m].id) {
                                //member.TodoType = $scope.maintodotypes[m].name;
                                member.TodoType = $scope.maintodotypes[m];
                                //console.log('member.TodoType', $scope.maintodotypes[m]);
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.mainreporttodostatuses.length; n++) {
                            if (member.currentstatus == $scope.mainreporttodostatuses[n].name) {
                                member.TodoStatus = $scope.mainreporttodostatuses[n];
                                break;
                            }
                        }
                        for (var o = 0; o < $scope.maintodopillars.length; o++) {
                            if (member.pillarid == $scope.maintodopillars[o].id) {
                                member.TodoPillar = $scope.maintodopillars[o];
                                break;
                            }
                        }
                        $scope.TotalTodos.push(member);
                    });
                });
            }
        }
    });
    //$scope.memberbeneficiaries = Restangular.all('beneficiaries').getList().$object;
    if ($window.sessionStorage.fullName != undefined) {
        $scope.mem = Restangular.one('beneficiaries', $window.sessionStorage.fullName).get().then(function (member) {
            //console.log('$window.sessionStorage.fullName', $window.sessionStorage.fullName);
            $scope.memberfullname = member;
            $scope.membername = member.fullname;
        });
    }
    /*	$scope.reportincident.co = false;
    		$scope.member = true;
    		$scope.$watch('reportincident.co', function (newValue, oldValue) {
    			console.log('newValue1234',newValue);
    			if (newValue === oldValue) {
    				return;
    			} else if ($scope.reportincident.co === true) {
    				$scope.member = false;
    			} else {
    				$scope.member = true;
    			}

    		});*/
    $scope.getSurveyanswers = function (beneficiaryid, questionid) {
        return Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + questionid).getList().$object;
    };
    /*
    	        $scope.reporttoo = true;
    	        $scope.$watch('reportincident.reported', function (newValue, oldValue) {
    	            //  console.log('Report Too');
    	            if (newValue === oldValue) {
    	                return;
    	            } else if ($scope.reportincident.reported === true) {
    	                $scope.reporttoo = false;
    	            } else {
    	                $scope.reporttoo = true;
    	            }
    	        });*/
    $scope.getTodoType = function (roleId) {
        return Restangular.one('todotypes', roleId).get().$object;
    };
    $scope.getStatus = function (roleId) {
        return Restangular.one('todostatuses', roleId).get().$object;
    };
    $scope.getCurrentStatusCase = function (roleId) {
        return Restangular.one('currentstatusofcases?filter[where][name]=' + roleId).get().$object;
    };
    $scope.getPillar = function (roleId) {
        return Restangular.one('pillars', roleId).get().$object;
    };
    $scope.getSurQues = function (quesId) {
        return Restangular.one('surveyquestions', quesId).get().$object;
    };
    $scope.getDocumentName = function (quesId) {
        return Restangular.one('documenttypes', quesId).get().$object;
    };
    $scope.getSchemeName = function (quesId) {
        return Restangular.one('schemes', quesId).get().$object;
    };
    $scope.getSchemeDocumentStage = function (stid) {
        return Restangular.one('schemestages', stid).get().$object;
    };
    /****************************************************** Save Answer *********************************/
    $scope.SaveAnswers = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, availeddate, referenceno) {
        /*console.log('answer',answer);
        	console.log('beneficiaryid',beneficiaryid);
        	console.log('pillarid',pillarid);
        	console.log('questionid',questionid);
        	console.log('serialno',serialno);
        	        
        	console.log('yesincrement',yesincrement);
        	console.log('noincrement',noincrement);
        	console.log('yesdocumentflag',yesdocumentflag);
        	console.log('nodocumentflag',nodocumentflag);
        	console.log('yesdocumentid',yesdocumentid);
        	        
        	console.log('nodocumentid',nodocumentid);
        	console.log('availeddate',availeddate);
        	console.log('referenceno',referenceno);
        	console.log('modifieddate',modifieddate);
        	console.log('modifiedby',modifiedby);
        	
        	*/
        $scope.surveyanswer = {};
        if (answer == 'yes') {
            $scope.surveyanswer.documentflag = yesdocumentflag;
            $scope.surveyanswer.documentid = yesdocumentid;
            //console.log('$scope.surveyanswer.documentid1', $scope.surveyanswer.documentid);
            $scope.surveyanswer.schemeid = yesdocumentid;
        } else {
            $scope.surveyanswer.documentflag = nodocumentflag;
            $scope.surveyanswer.documentid = nodocumentid;
            $scope.surveyanswer.schemeid = nodocumentid;
            //console.log('$scope.surveyanswer.documentid2', $scope.surveyanswer.documentid);
        }
        $scope.surveyanswer.beneficiaryid = beneficiaryid;
        $scope.surveyanswer.pillarid = pillarid;
        $scope.surveyanswer.questionid = questionid;
        $scope.surveyanswer.answer = answer;
        $scope.surveyanswer.lastupdatedtime = modifieddate;
        $scope.surveyanswer.lastupdatedby = modifiedby;
        $scope.surveyanswer.availeddate = availeddate;
        $scope.surveyanswer.referenceno = referenceno;
        $scope.surveyanswer.state = $window.sessionStorage.zoneId;
        $scope.surveyanswer.district = $window.sessionStorage.salesAreaId;
        $scope.surveyanswer.facility = $window.sessionStorage.coorgId;
        $scope.surveyanswer.lastmodifiedbyrole = $window.sessionStorage.roleId;
        $scope.beneficiaryupdate.completedflag = 'yes';
        $scope.beneficiaryupdate.completedpillar = pillarid;
        $scope.beneficiaryupdate.completedquestion = serialno;
        $scope.beneficiaryupdate.lastmeetingdate = new Date();
        $scope.surveyanswer.stress_data = $scope.selectedBeneficiary.stress_data;
         console.log("$scope.PresentCompletedPillar Save",$scope.PresentCompletedPillar);
        console.log("$scope.PresentCompletedQuestion Save",$scope.PresentCompletedQuestion);
        $scope.submitsurveyanswers.post($scope.surveyanswer).then(function (resp) {
            $scope.submitallsurveyanswers.post($scope.surveyanswer).then(function (resp) {
                //console.log('submitallsurveyanswers',resp);
            });
            console.log('answer Saved1', resp);
            // $scope.healthsurveyanswer = resp.answer;
            //  console.log('$scope.healthsurveyanswer Saved1', $scope.healthsurveyanswer)
            $scope.cdids = {};
            Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryupdate).then(function (respo) {
                // console.log('answer Saved', respo);
                $scope.DisplayBeneficiary = respo;
                $scope.alltodos = Restangular.all('todos?filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
                //$scope.healthtodos = Restangular.all('todos?filter[where][pillarid]=5&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
                $scope.hlthtodos = Restangular.all('todos?filter[where][pillarid]=5&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (hlthtodos) {
                    $scope.healthtodos = hlthtodos;
                    angular.forEach($scope.healthtodos, function (member, index) {
                        member.index = index + 1;
                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                            if (member.todotype == $scope.maintodotypes[m].id) {
                                //member.TodoType = $scope.maintodotypes[m].name;
                                member.TodoType = $scope.maintodotypes[m];
                                //console.log('member.TodoType', $scope.maintodotypes[m]);
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                            if (member.status == $scope.maintodostatuses[n].id) {
                                //if ($scope.UserLanguage == 1) {
                                member.TodoStatus = $scope.maintodostatuses[n];
                                /*     break;
                                	 }
                                	 if ($scope.UserLanguage == 2) {
                                	     member.TodoStatus = $scope.maintodostatuses[n].hnname;
                                	 }*/
                                break;
                            }
                        }
                    });
                });
                Restangular.all('currentstatusofcases').getList().then(function (ssjRes1) {
                    $scope.currentstatusofcases = ssjRes1
                    $scope.ssjtodos1 = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][currentstatus][nlike]=Closed%').getList().then(function (ssjRes) {
                        $scope.ssjtodos = ssjRes
                        angular.forEach($scope.ssjtodos, function (member, index) {
                            member.index = index + 1;
                            for (var m = 0; m < $scope.currentstatusofcases.length; m++) {
                                if (member.currentstatus == $scope.currentstatusofcases[m].name) {
                                    member.TodoType = $scope.currentstatusofcases[m];
                                    break;
                                }
                            }
                        });
                    });
                });
                //$scope.ssjtodos = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][currentstatus][nlike]=Closed%').getList().$object;
                $scope.sptodos = Restangular.all('todos?filter[where][pillarid]=1&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
                //$scope.fstodos = Restangular.all('todos?filter[where][pillarid]=3&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
                $scope.fstdos = Restangular.all('todos?filter[where][pillarid]=3&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (fstdos) {
                    $scope.fstodos = fstdos;
                    angular.forEach($scope.fstodos, function (member, index) {
                        member.index = index + 1;
                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                            if (member.todotype == $scope.maintodotypes[m].id) {
                                member.TodoType = $scope.maintodotypes[m];
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                            if (member.status == $scope.maintodostatuses[n].id) {
                                member.TodoStatus = $scope.maintodostatuses[n];
                                break;
                            }
                        }
                    });
                });
                //$scope.idstodos = Restangular.all('todos?filter[where][pillarid]=4&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
                $scope.idstdos = Restangular.all('todos?filter[where][pillarid]=4&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (idstdos) {
                    $scope.idstodos = idstdos;
                    angular.forEach($scope.idstodos, function (member, index) {
                        member.index = index + 1;
                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                            if (member.todotype == $scope.maintodotypes[m].id) {
                                member.TodoType = $scope.maintodotypes[m];
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                            if (member.status == $scope.maintodostatuses[n].id) {
                                member.TodoStatus = $scope.maintodostatuses[n];
                                break;
                            }
                        }
                    });
                });
                $scope.applieddocuments = Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][pillarid]=1&filter[where][answer]=yes&filter[where][availeddate][nlike]=null%').getList().$object;
            });
        }, function (error) {
            if (error.data.error.constraint == "pillar_question_beneficiary") {
                // console.log('error', error);
                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + questionid + '&filter[where][pillarid]=' + pillarid).getList().then(function (answered) {
                    if (answered.length > 0) {
                        $scope.surveyanswer.id = answered[0].id;
                        $scope.submitsurveyanswers.customPUT($scope.surveyanswer, answered[0].id).then(function (resp) {
                            // console.log('answer updated', resp);
                            $scope.cdids = {};
                            Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryupdate).then(function (respo) {
                                // console.log('member updated', respo);
                                $scope.DisplayBeneficiary = respo;
                                $scope.alltodos = Restangular.all('todos?filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
                                //$scope.healthtodos = Restangular.all('todos?filter[where][pillarid]=5&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
                                $scope.hlthtodos = Restangular.all('todos?filter[where][pillarid]=5&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (hlthtodos) {
                                    $scope.healthtodos = hlthtodos;
                                    angular.forEach($scope.healthtodos, function (member, index) {
                                        member.index = index + 1;
                                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                                            if (member.todotype == $scope.maintodotypes[m].id) {
                                                //member.TodoType = $scope.maintodotypes[m].name;
                                                member.TodoType = $scope.maintodotypes[m];
                                                //console.log('member.TodoType', $scope.maintodotypes[m]);
                                                break;
                                            }
                                        }
                                        for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                                            if (member.status == $scope.maintodostatuses[n].id) {
                                                //if ($scope.UserLanguage == 1) {
                                                member.TodoStatus = $scope.maintodostatuses[n];
                                                /*     break;
                                                	 }
                                                	 if ($scope.UserLanguage == 2) {
                                                	     member.TodoStatus = $scope.maintodostatuses[n].hnname;
                                                	 }*/
                                                break;
                                            }
                                        }
                                    });
                                });
                                Restangular.all('currentstatusofcases').getList().then(function (ssjRes1) {
                                    $scope.currentstatusofcases = ssjRes1
                                    $scope.ssjtodos1 = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][currentstatus][nlike]=Closed%').getList().then(function (ssjRes) {
                                        $scope.ssjtodos = ssjRes
                                        angular.forEach($scope.ssjtodos, function (member, index) {
                                            member.index = index + 1;
                                            for (var m = 0; m < $scope.currentstatusofcases.length; m++) {
                                                if (member.currentstatus == $scope.currentstatusofcases[m].name) {
                                                    member.TodoType = $scope.currentstatusofcases[m];
                                                    break;
                                                }
                                            }
                                        });
                                    });
                                });
                                //$scope.ssjtodos = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][currentstatus][nlike]=Closed%').getList().$object;
                                $scope.sptodos = Restangular.all('todos?filter[where][pillarid]=1&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
                                //$scope.fstodos = Restangular.all('todos?filter[where][pillarid]=3&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
                                $scope.fstdos = Restangular.all('todos?filter[where][pillarid]=3&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (fstdos) {
                                    $scope.fstodos = fstdos;
                                    angular.forEach($scope.fstodos, function (member, index) {
                                        member.index = index + 1;
                                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                                            if (member.todotype == $scope.maintodotypes[m].id) {
                                                member.TodoType = $scope.maintodotypes[m];
                                                break;
                                            }
                                        }
                                        for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                                            if (member.status == $scope.maintodostatuses[n].id) {
                                                member.TodoStatus = $scope.maintodostatuses[n];
                                                break;
                                            }
                                        }
                                    });
                                });
                                //$scope.idstodos = Restangular.all('todos?filter[where][pillarid]=4&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
                                $scope.idstdos = Restangular.all('todos?filter[where][pillarid]=4&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (idstdos) {
                                    $scope.idstodos = idstdos;
                                    angular.forEach($scope.idstodos, function (member, index) {
                                        member.index = index + 1;
                                        for (var m = 0; m < $scope.maintodotypes.length; m++) {
                                            if (member.todotype == $scope.maintodotypes[m].id) {
                                                member.TodoType = $scope.maintodotypes[m];
                                                break;
                                            }
                                        }
                                        for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                                            if (member.status == $scope.maintodostatuses[n].id) {
                                                member.TodoStatus = $scope.maintodostatuses[n];
                                                break;
                                            }
                                        }
                                    });
                                });
                                $scope.applieddocuments = Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][pillarid]=1&filter[where][answer]=yes&filter[where][availeddate][nlike]=null%').getList().$object;
                            });
                        });
                    }
                });
            }
        });
    };
    //////////////////////////////////////////////////////////////////////////////////////////
    $scope.openPrevAns = function () {
        if ($window.sessionStorage.language == 1) {
            $scope.printNotapplicable = 'N/A';
            $scope.printFormalSaving = 'Formal Saving';
            $scope.printInFormalSaving = 'Informal Saving';
        } else if ($window.sessionStorage.language == 2) {
            $scope.printNotapplicable = '???? ????';
            $scope.printFormalSaving = '??????? ???';
            $scope.printInFormalSaving = '????????? ???';
        } else if ($window.sessionStorage.language == 3) {
            $scope.printNotapplicable = '???????????????';
            $scope.printFormalSaving = '??????? ??????';
            $scope.printInFormalSaving = '????????? ??????';
        } else if ($window.sessionStorage.language == 4) {
            $scope.printNotapplicable = '??????????';
            $scope.printFormalSaving = '??????? ????????';
            $scope.printInFormalSaving = '???????? ????????';
        } else if ($window.sessionStorage.language == 5) {
            $scope.printNotapplicable = '?????????';
            $scope.printFormalSaving = '???????? ??????';
            $scope.printInFormalSaving = '????????? ??????';
        } else if ($window.sessionStorage.language == 6) {
            $scope.printNotapplicable = '???? ????';
            $scope.printFormalSaving = '??????? ???';
            $scope.printInFormalSaving = '????????? ???';
        }
        Restangular.all('surveyquestions').getList().then(function (surveyquestions) {
            $scope.AllSurveyQuestions = surveyquestions;
            Restangular.all('surveyanswers?filter[where][pillarid]=5&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (HRes) {
                $scope.HealthAnswers = HRes;
                angular.forEach($scope.HealthAnswers, function (member, index) {
                    member.index = index + 1;
                    if (member.questionid == 2 || member.questionid == 3 || member.questionid == 4 || member.questionid == 64) {
                        member.colour = "#3498DB";
                    } else {
                        member.colour = "";
                    }
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
            Restangular.all('surveyanswers?filter[where][pillarid]=2&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (ssjRes) {
                $scope.SSJAnswers = ssjRes;
                // console.log('$scope.SSJAnswers',$scope.SSJAnswers);
                angular.forEach($scope.SSJAnswers, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
            Restangular.all('surveyanswers?filter[where][pillarid]=1&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (spRes) {
                $scope.SPAnswers = spRes;
                angular.forEach($scope.SPAnswers, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
            Restangular.all('surveyanswers?filter[where][pillarid]=3&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (fsRes) {
                $scope.FSAnswers = fsRes;
                //console.log('$scope.FSAnswers',$scope.FSAnswers)
                angular.forEach($scope.FSAnswers, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
            Restangular.all('surveyanswers?filter[where][pillarid]=4&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (idsRes) {
                $scope.IDSAnswers = idsRes;
                angular.forEach($scope.IDSAnswers, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][pillarid]=5' + '&filter[where][questionid]=64').getList().then(function (idsRes) {
                $scope.RemainingAnswers = idsRes;
                angular.forEach($scope.RemainingAnswers, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
            $scope.modalPrevAns = $modal.open({
                animation: true,
                templateUrl: 'template/Previousanswers.html',
                scope: $scope,
                backdrop: 'static',
                size: 'lg'
            });
        });
    };
    $scope.HealthViewAnswer = function (ansId, quesId) {
        console.log('quesId', quesId);
        Restangular.one('surveyanswers', ansId).get().then(function (HealthAnsRes) {
            //console.log('HealthAnsRes', HealthAnsRes);
            $scope.AnsDate = HealthAnsRes.lastupdatedtime;
            Restangular.one('surveyquestions', HealthAnsRes.questionid).get().then(function (HealthQuesRes) {
                // console.log('HealthQuesRes', HealthQuesRes);
                var pillarid = HealthQuesRes.pillarid;
                console.log('pillarid', pillarid);
                /*  if (pillarid == 1) {
                      $scope.DisplayClickOption = languagedata;
                      if ($window.sessionStorage.language == 1) {
                          $scope.PillarName = 'SP';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 2) {
                          $scope.PillarName = 'एस पी';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 3) {
                          $scope.PillarName = 'ಆರೋಗ್ಯ';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 4) {
                          $scope.PillarName = 'ஆரோக்கியம்';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 5) {
                          $scope.PillarName = 'ఆరోగ్యం'
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 6) {
                          $scope.PillarName = 'एस पी'
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      }

                  } else if (pillarid == 2) {
                      $scope.DisplayClickOption = languagedata;
                      console.log('languagedata', languagedata);
                      if ($window.sessionStorage.language == 1) {
                          $scope.PillarName = 'SSJ';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 2) {
                          $scope.PillarName = 'एस एस जे';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 3) {
                          $scope.PillarName = 'ಆರೋಗ್ಯ';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 4) {
                          $scope.PillarName = 'ஆரோக்கியம்';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 5) {
                          $scope.PillarName = 'ఆరోగ్యం'
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 6) {
                          $scope.PillarName = 'एस एस जे';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      }

                  } else if (pillarid == 3) {
                      $scope.DisplayClickOption = languagedata;
                      if ($window.sessionStorage.language == 1) {
                          $scope.PillarName = 'FS';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 2) {
                          $scope.PillarName = 'एफ एस';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 3) {
                          $scope.PillarName = 'ಆರೋಗ್ಯ';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 4) {
                          $scope.PillarName = 'ஆரோக்கியம்';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 5) {
                          $scope.PillarName = 'ఆరోగ్యం'
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 6) {
                          $scope.PillarName = 'एफ एस'
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      }

                  } else if (pillarid == 4) {
                      $scope.DisplayClickOption = languagedata;
                      if ($window.sessionStorage.language == 1) {
                          $scope.PillarName = 'IDS';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 2) {
                          $scope.PillarName = 'आईडीएस';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 3) {
                          $scope.PillarName = 'ಆರೋಗ್ಯ';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 4) {
                          $scope.PillarName = 'ஆரோக்கியம்';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 5) {
                          $scope.PillarName = 'ఆరోగ్యం'
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      } else if ($window.sessionStorage.language == 6) {
                          $scope.PillarName = 'आईडीएस';
                          $scope.QuestionName = HealthQuesRes.question;
                          $scope.DisplayClickOption = HealthAnsRes.answer;
                      }

                  } else */
                if (pillarid == 5) {
                    //$scope.DisplayClickOption = languagedata;
                    if ($window.sessionStorage.language == 1) {
                        $scope.PillarName = 'HEALTH';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 2) {
                        $scope.PillarName = 'स्वास्थ्य';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 3) {
                        $scope.PillarName = 'ಆರೋಗ್ಯ';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 4) {
                        $scope.PillarName = 'ஆரோக்கியம்';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 5) {
                        $scope.PillarName = 'ఆరోగ్యం'
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 6) {
                        $scope.PillarName = 'आरोग्य'
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    }
                };
                if (quesId == 64) {
                    $scope.pickerselectdate = $scope.DisplayBeneficiary.monthoflaststitest;
                    $scope.modalToDo = $modal.open({
                        animation: true,
                        templateUrl: 'template/DatePickerView.html',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false
                    });
                } else if (quesId == 2) {
                    //$scope.printcondomasked = $scope.DisplayBeneficiary.condomsasked.split(':')[1];
                    $scope.printcondomasked1 = $scope.DisplayBeneficiary.condomsasked.split(',');
                    $scope.printcondomasked2 = $scope.printcondomasked1[$scope.printcondomasked1.length - 1];
                    $scope.printcondomprovided1 = $scope.DisplayBeneficiary.condomsprovided.split(',');
                    $scope.printcondomprovided2 = $scope.printcondomprovided1[$scope.printcondomprovided1.length - 1];
                    $scope.printcondomasked = $scope.printcondomasked2.split(':')[3];
                    $scope.printcondomprovided = $scope.printcondomprovided2.split(':')[3];
                    $scope.printcondomdateasked = $scope.DisplayBeneficiary.dateprovided;
                    $scope.modalToDo = $modal.open({
                        animation: true,
                        templateUrl: 'template/condomdetailsView.html',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false
                    });
                } else if (quesId == 3) {
                    $scope.pickerselectdate = $scope.DisplayBeneficiary.monthoflasthivtest;
                    $scope.modalToDo = $modal.open({
                        animation: true,
                        templateUrl: 'template/DatePickerView.html',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false
                    });
                } else if (quesId == 4) {
                    $scope.pickerselectdate = $scope.DisplayBeneficiary.monthofdetection;
                    //console.log('');
                    $scope.modalToDo = $modal.open({
                        animation: true,
                        templateUrl: 'template/DatePickerHIVView.html',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false
                    });
                } else if (quesId == 83) {
                    $scope.pickerselectdate = HealthAnsRes.lastupdatedtime;
                    //console.log('');
                    $scope.modalToDo = $modal.open({
                        animation: true,
                        templateUrl: 'template/DatePickerHIVView.html',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            });
        });
    };
    $scope.FSViewAnswer = function (ansId, quesId) {
        // console.log('quesId', quesId);
        Restangular.one('surveyanswers', ansId).get().then(function (HealthAnsRes) {
            //console.log('HealthAnsRes', HealthAnsRes.answer);
            //$scope.HealthAnsRes = HealthAnsRes;
            $scope.AnsDate = HealthAnsRes.lastupdatedtime;
            Restangular.one('surveyquestions', HealthAnsRes.questionid).get().then(function (HealthQuesRes) {
                // console.log('HealthQuesRes', HealthQuesRes);
                var pillarid = HealthQuesRes.pillarid;
                if (pillarid == 3) {
                    if ($window.sessionStorage.language == 1) {
                        $scope.PillarName = 'FS';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 2) {
                        $scope.PillarName = 'एफ एस';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 3) {
                        $scope.PillarName = 'ಆರೋಗ್ಯ';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 4) {
                        $scope.PillarName = 'ஆரோக்கியம்';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 5) {
                        $scope.PillarName = 'ఆరోగ్యం'
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 6) {
                        $scope.PillarName = 'एफ एस'
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    }
                }
                // });
                if (quesId == 17) {
                    if ($scope.DisplayBeneficiary.savings == true) {
                        $scope.Printsavings = 'YES';
                    } else {
                        $scope.Printsavings = 'NO';
                    }
                    if ($scope.DisplayBeneficiary.insurance == true) {
                        $scope.Printinsurance = 'YES';
                    } else {
                        $scope.Printinsurance = 'NO';
                    }
                    if ($scope.DisplayBeneficiary.pension == true) {
                        $scope.Printpension = 'YES';
                    } else {
                        $scope.Printpension = 'NO';
                    }
                    if ($scope.DisplayBeneficiary.credit == true) {
                        $scope.Printcredit = 'YES';
                    } else {
                        $scope.Printcredit = 'NO';
                    }
                    $scope.modalToDo = $modal.open({
                        animation: true,
                        templateUrl: 'template/FinancialLiteracyView.html',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false
                    });
                } else if (quesId == 34) {
                    if ($scope.DisplayBeneficiary.finplansavings == true) {
                        $scope.Printsavings = 'YES';
                    } else {
                        $scope.Printsavings = 'NO';
                    }
                    if ($scope.DisplayBeneficiary.finplaninsurance == true) {
                        $scope.Printinsurance = 'YES';
                    } else {
                        $scope.Printinsurance = 'NO';
                    }
                    if ($scope.DisplayBeneficiary.finplanpension == true) {
                        $scope.Printpension = 'YES';
                    } else {
                        $scope.Printpension = 'NO';
                    }
                    if ($scope.DisplayBeneficiary.finplancredit == true) {
                        $scope.Printcredit = 'YES';
                    } else {
                        $scope.Printcredit = 'NO';
                    }
                    $scope.modalToDo = $modal.open({
                        animation: true,
                        templateUrl: 'template/FinancialPlaningView.html',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false
                    });
                } else if (quesId == 19) {
                    $scope.pickerselectdate = $scope.DisplayBeneficiary.monthoflastsaving;
                    $scope.modalToDo = $modal.open({
                        animation: true,
                        templateUrl: 'template/DatePickerView.html',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false
                    });
                } else if (quesId == 20) {
                    $scope.pickerselectdate = $scope.DisplayBeneficiary.monthoflastsaving;
                    $scope.modalToDo = $modal.open({
                        animation: true,
                        templateUrl: 'template/DatePickerView.html',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            });
        });
    }

    $scope.IDSViewAnswer = function (ansId, quesId) {
        console.log('quesId', quesId);
        $scope.phonetypes = Restangular.all('phonetypes').getList().$object;
        Restangular.one('surveyanswers', ansId).get().then(function (HealthAnsRes) {
            $scope.AnsDate = HealthAnsRes.lastupdatedtime;
            Restangular.one('surveyquestions', HealthAnsRes.questionid).get().then(function (HealthQuesRes) {
                Restangular.one('beneficiaries', HealthAnsRes.beneficiaryid).get().then(function (BenRes) {
                    $scope.phonetyp = BenRes.phonetype;
                    if ($scope.phonetyp == 1) {
                        $scope.phonetype.phonetp = 1;
                    } else if ($scope.phonetyp == 2) {
                        $scope.phonetype.phonetp = 2;
                    } else if ($scope.phonetyp == 3) {
                        $scope.phonetype.phonetp = 3;
                    }

                });

                if ($window.sessionStorage.language == 1) {
                    $scope.PillarName = 'IDS';
                    $scope.QuestionName = HealthQuesRes.question;
                    $scope.DisplayClickOption = HealthAnsRes.answer;
                } else if ($window.sessionStorage.language == 2) {
                    $scope.PillarName = 'आईडीएस';
                    $scope.QuestionName = HealthQuesRes.question;
                    $scope.DisplayClickOption = HealthAnsRes.answer;
                } else if ($window.sessionStorage.language == 3) {
                    $scope.PillarName = 'ಆರೋಗ್ಯ';
                    $scope.QuestionName = HealthQuesRes.question;
                    $scope.DisplayClickOption = HealthAnsRes.answer;
                } else if ($window.sessionStorage.language == 4) {
                    $scope.PillarName = 'ஆரோக்கியம்';
                    $scope.QuestionName = HealthQuesRes.question;
                    $scope.DisplayClickOption = HealthAnsRes.answer;
                } else if ($window.sessionStorage.language == 5) {
                    $scope.PillarName = 'ఆరోగ్యం'
                    $scope.QuestionName = HealthQuesRes.question;
                    $scope.DisplayClickOption = HealthAnsRes.answer;
                } else if ($window.sessionStorage.language == 6) {
                    $scope.PillarName = 'आईडीएस'
                    $scope.QuestionName = HealthQuesRes.question;
                    $scope.DisplayClickOption = HealthAnsRes.answer;
                }
                //}
                $scope.modalINFO = $modal.open({
                    animation: true,
                    templateUrl: 'template/PhoneTypeView.html',
                    scope: $scope,
                    backdrop: 'static',
                    keyboard: false
                });
            });
        });

    }

    $scope.OkDpView = function () {
        $scope.modalToDo.close();
    }
    $scope.okPrevAns = function () {
        $scope.modalPrevAns.close();
    };
    $scope.openFs = function () {
        $scope.modalFS = $modal.open({
            animation: true,
            templateUrl: 'template/fs.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.SaveFS = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);

        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        
        $scope.modalFS.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okFs = function () {
        $scope.modalFS.close();
    };
    $scope.openHealth = function () {
        $scope.modalHealth = $modal.open({
            animation: true,
            templateUrl: 'template/health.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.SaveHealth = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        $scope.modalHealth.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okHealth = function () {
        $scope.modalHealth.close();
    };
    $scope.openSp = function () {
        $scope.modalSP = $modal.open({
            animation: true,
            templateUrl: 'template/sp.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.SaveSP = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        $scope.modalSP.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okSp = function () {
        $scope.modalSP.close();
    };
    $scope.openToDo = function () {
        $scope.toggleLoading();
        Restangular.all('todostatuses').getList().then(function (todostats) {
            $scope.todostatuses = todostats;
            $scope.modalInstanceLoad.close();
            $scope.modalToDo = $modal.open({
                animation: true,
                templateUrl: 'template/ToDo.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false
            });
        });
    };
    $scope.openOneAlert = function () {
        $scope.modalOneAlert = $modal.open({
            animation: true,
            templateUrl: 'template/AlertModal.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            windowClass: 'modal-danger'
        });
    };
    $scope.okAlert = function () {
        $scope.modalOneAlert.close();
    };
    $scope.openToDoPhoneName = function () {
        $scope.modalToDo = $modal.open({
            animation: true,
            templateUrl: 'template/ToDoPhoneName.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false
        });
    };
    $scope.SaveToDo = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.todo.pillarid = pillarid;
        $scope.todo.beneficiaryid = beneficiaryid;
        $scope.todo.fieldworkerid = $window.sessionStorage.UserEmployeeId;
        $scope.todo.facility = $window.sessionStorage.coorgId;
        $scope.todo.district = $window.sessionStorage.salesAreaId;
        $scope.todo.state = $window.sessionStorage.zoneId;
        $scope.todo.questionid = questionid;
        $scope.todo.roleId = $window.sessionStorage.roleId;
        $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
        $scope.submittodos.post($scope.todo).then(function (resp) {
            console.log('submittodos1', resp);
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
            $scope.todo = {};
            $scope.todo = {
                status: 1,
                stateid: $window.sessionStorage.zoneId,
                state: $window.sessionStorage.zoneId,
                districtid: $window.sessionStorage.salesAreaId,
                district: $window.sessionStorage.salesAreaId,
                coid: $window.sessionStorage.coorgId,
                facility: $window.sessionStorage.coorgId,
                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                lastmodifiedtime: new Date(),
                roleId: $window.sessionStorage.roleId
            };
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            $scope.todo.datetime = sevendays;
        }, function (error) {
            console.log('error', error);
        });
        $scope.modalToDo.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okToDo = function () {
        $scope.modalToDo.close();
    };
    $scope.openReportFollowup = function () {
        $scope.modalReportFollowup = $modal.open({
            animation: true,
            templateUrl: 'template/ReportFollowUp.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.SaveReportFollowup = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.modalReportFollowup.close();
    };
    $scope.okReportFollowup = function () {
        $scope.modalReportFollowup.close();
    };
    $scope.phonename = {};
    $scope.multiplephonename = [];
    $scope.openPhoneName = function () {
        $scope.modalPhoneName = $modal.open({
            animation: true,
            templateUrl: 'template/PhoneName.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.okPhoneName = function () {
        $scope.modalPhoneName.close();
    };
    $scope.AddPhoneName = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        if ($scope.phonename.name != undefined && $scope.phonename.phone != undefined) {
            if ($scope.phonename.phone.length == 10) {
                $scope.todophonename = {};
                $scope.todophonename.pillarid = pillarid;
                $scope.todophonename.beneficiaryid = beneficiaryid;
                $scope.todophonename.fieldworkerid = $window.sessionStorage.UserEmployeeId;
                $scope.todophonename.facility = $window.sessionStorage.coorgId;
                $scope.todophonename.district = $window.sessionStorage.salesAreaId;
                $scope.todophonename.state = $window.sessionStorage.zoneId;
                $scope.todophonename.questionid = questionid;
                $scope.todophonename.name = $scope.phonename.name;
                $scope.todophonename.phone = $scope.phonename.phone;
                $scope.todophonename.datetime = new Date();
                $scope.todophonename.status = 1;
                $scope.todophonename.todotype = 35;
                $scope.todophonename.questionid = questionid;
                $scope.todophonename.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.todophonename.lastmodifiedtime = new Date();
                $scope.todophonename.roleId = $window.sessionStorage.roleId;
                $scope.multiplephonename.push($scope.todophonename);
                $scope.phonename = {};
            } else {
                // alert('Enter 10 digit mobile number');
                $scope.AlertMessage = 'Enter 10 digit mobile number';
                $scope.openOneAlert();
            }
        }
        //$scope.modalPhoneName.close();
    };
    $scope.SavePhoneName = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        if ($scope.phonename.name != undefined && $scope.phonename.phone != undefined) {
            if ($scope.phonename.phone.length == 10) {
                $scope.todophonename = {};
                $scope.todophonename.pillarid = pillarid;
                $scope.todophonename.beneficiaryid = beneficiaryid;
                $scope.todophonename.fieldworkerid = $window.sessionStorage.UserEmployeeId;
                $scope.todophonename.facility = $window.sessionStorage.coorgId;
                $scope.todophonename.district = $window.sessionStorage.salesAreaId;
                $scope.todophonename.state = $window.sessionStorage.zoneId;
                $scope.todophonename.questionid = questionid;
                $scope.todophonename.name = $scope.phonename.name;
                $scope.todophonename.phone = $scope.phonename.phone;
                $scope.todophonename.datetime = new Date();
                $scope.todophonename.status = 1;
                $scope.todophonename.todotype = 35;
                $scope.todophonename.questionid = questionid;
                $scope.todophonename.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.todophonename.lastmodifiedtime = new Date();
                $scope.todophonename.roleId = $window.sessionStorage.roleId;
                $scope.todophonename.stress_data = $scope.selectedBeneficiary.stress_data;
                $scope.multiplephonename.push($scope.todophonename);
                $scope.phonename = {};
            } else {
                //alert('Enter 10 digit mobile number');
                $scope.AlertMessage = 'Enter 10 digit mobile number';
                $scope.openOneAlert();
            }
        }
        var phonenamecount = 0;
        for (var i = 0; i < $scope.multiplephonename.length; i++) {
            $scope.submittodos.post($scope.multiplephonename[i]).then(function (resp) {
                console.log('submittodos2', resp);
                phonenamecount++;
                if (phonenamecount >= $scope.multiplephonename.length) {
                    // console.log('phonename', 'phonename');
                    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
                    $scope.todo = {};
                    $scope.todo = {
                        status: 1,
                        stateid: $window.sessionStorage.zoneId,
                        state: $window.sessionStorage.zoneId,
                        districtid: $window.sessionStorage.salesAreaId,
                        district: $window.sessionStorage.salesAreaId,
                        coid: $window.sessionStorage.coorgId,
                        facility: $window.sessionStorage.coorgId,
                        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                        lastmodifiedtime: new Date(),
                        roleId: $window.sessionStorage.roleId
                    };
                    $scope.multiplephonename = [];
                    $scope.modalPhoneName.close();
                    if (pillarid == 1) {
                        $scope.spnextquestion();
                    } else if (pillarid == 2) {
                        $scope.ssjnextquestion();
                    } else if (pillarid == 3) {
                        $scope.fsnextquestion();
                    } else if (pillarid == 4) {
                        $scope.idsnextquestion();
                    } else if (pillarid == 5) {
                        $scope.healthnextquestion();
                    }
                }
            });
        }
        /* $scope.submittodos.post($scope.todophonename).then(function (resp) {
        	     $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        	     $scope.todo = {
        	         status: 1
        	     };
        	 }, function (error) {
        	     console.log('error', error);
        	 });*/
        /* $scope.modalPhoneName.close();
        	 if (pillarid == 1) {
        	     $scope.spnextquestion();
        	 } else if (pillarid == 2) {
        	     $scope.ssjnextquestion();
        	 } else if (pillarid == 3) {
        	     $scope.fsnextquestion();
        	 } else if (pillarid == 4) {
        	     $scope.idsnextquestion();
        	 } else if (pillarid == 5) {
        	     $scope.healthnextquestion();
        	 }*/
    };
    $scope.openRI = function (size) {
        Restangular.all('servityofincidents?filter[where][deleteflag]=false').getList().then(function (responseseservity) {
            $scope.servityofincidents = responseseservity;
            $scope.reportincident.severity = 'Moderate';
        });
        Restangular.all('currentstatusofcases?filter[where][deleteflag]=false').getList().then(function (response) {
            $scope.currentstatusofcases = response;
            $scope.reportincident.currentstatus = 'Needs Follow Up';
        });
        $scope.reportincidentfollowups = Restangular.all('reportincidentfollowups').getList().$object;
        if ($window.sessionStorage.roleId == 5) {
            $scope.partners1 = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][deleteflag]=false').getList().then(function (resPartner1) {
                $scope.multiplebeneficiaries = resPartner1;
            });
        } else {
            $scope.partners2 = Restangular.all('beneficiaries?filter[where][fieldworker]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=false').getList().then(function (resPartner2) {
                $scope.multiplebeneficiaries = resPartner2;
            });
        }
        $scope.modalRI = $modal.open({
            animation: true,
            templateUrl: 'template/ReportIncident.html',
            scope: $scope,
            backdrop: 'static',
            size: size,
        });
    };
    $scope.openRIOthers = function (size) {
        Restangular.all('servityofincidents?filter[where][deleteflag]=false').getList().then(function (responseseservity) {
            $scope.servityofincidents = responseseservity;
            $scope.reportincident.severity = 'Moderate';
        });
        Restangular.all('currentstatusofcases?filter[where][deleteflag]=false').getList().then(function (response) {
            $scope.currentstatusofcases = response;
            $scope.reportincident.currentstatus = 'Needs Follow Up';
        });
        $scope.reportincidentfollowups = Restangular.all('reportincidentfollowups').getList().$object;
        if ($window.sessionStorage.roleId == 5) {
            $scope.partners1 = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][deleteflag]=false').getList().then(function (resPartner1) {
                $scope.multiplebeneficiaries = resPartner1;
            });
        } else {
            $scope.partners2 = Restangular.all('beneficiaries?filter[where][fieldworker]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=false').getList().then(function (resPartner2) {
                $scope.multiplebeneficiaries = resPartner2;
            });
        }
        $scope.OthersReport = true;
        $scope.reportincident.multiplemembers = [];
        $scope.modalRI = $modal.open({
            animation: true,
            templateUrl: 'template/ReportIncidentOthers.html',
            scope: $scope,
            backdrop: 'static',
            size: size,
        });
    };
    $scope.reportincident.follow = [];
    $scope.addedtodos = [];
    /* $scope.$watch('reportincident.follow', function (newValue, oldValue) {
    	     if (newValue === oldValue || newValue == '' || oldValue.length > newValue.length) {
    	         return;
    	     } else {
    	         $scope.newtodo = {};
    	         $scope.newtodo.status = 1;
    	         $scope.newtodo.todotype = 27;
    	         var oneday = new Date();
    	         oneday.setDate(oneday.getDate() + 1);
    	         $scope.newtodo.datetime = oneday;
    	         $scope.newtodo.pillarid = 2;

    	         $scope.newtodo.state = $window.sessionStorage.zoneId;
    	         $scope.newtodo.district = $window.sessionStorage.salesAreaId;
    	         $scope.newtodo.facility = $window.sessionStorage.coorgId
    	         $scope.newtodo.lastmodifiedby = $window.sessionStorage.UserEmployeeId
    	         $scope.newtodo.lastmodifiedtime = new Date();


    	         $scope.openReportFollowup();
    	         $scope.oldFollowUp = oldValue;
    	         $scope.newFollow = newValue;

    	         if (oldValue != undefined) {
    	             console.log('reportincident.follow newvalue', newValue.length);
    	             console.log('reportincident.follow oldValue', oldValue.length);
    	             if (oldValue.length < newValue.length) {
    	                 $scope.okReportFollowup();
    	             }
    	         } else {
    	             $scope.okReportFollowup();
    	         }

    	         if (newValue === oldValue) {
    	             return;
    	         }
    	         /*else if ($scope.reportincident.follow === 'none') {
    	            console.log('$scope.reportincident.follow', $scope.reportincident.follow);
    	            $scope.showfollowupModal = !$scope.showfollowupModal;
    	         } */
    /* else {

    	     $scope.SavetodoFollow = function () {
    	         $scope.attendeespurpose = newValue;
    	         $scope.addedtodos.push($scope.newtodo);
    	         $scope.okReportFollowup()
    	         console.log('$scope.addedtodos', $scope.addedtodos);
    	     };*/
    /*if ($scope.reportincident.follow === 'none') {
    	    console.log('$scope.reportincident.follow', $scope.reportincident.follow);
    	    $scope.showfollowupModal = !$scope.showfollowupModal;
    	}*/
    /*  var array3 = newValue.filter(function (obj) {
    	      return oldValue.indexOf(obj) == -1;
    	  });
    	  if (array3.length > 1) {
    	      $scope.okReportFollowup();
    	  }
    	  if (array3.length > 0) {
    	      console.log('unique', array3);
    	      for (var i = 0; i < $scope.reportincidentfollowups.length; i++) {
    	          if ($scope.reportincidentfollowups[i].id == array3[0]) {
    	              $scope.printpurpose = $scope.reportincidentfollowups[i].name;
    	              $scope.newtodo.purpose = $scope.reportincidentfollowups[i].id;
    	          }
    	      }
    	  }*/
    /*if ($scope.reportincident.follow === 'none') {
    	    console.log('$scope.reportincident.follow', newValue);
    	    $scope.showfollowupModal = !$scope.showfollowupModal;
    	}*/
    /* };
    	    }
    	});*/
    $scope.CancelFollow = function () {
        $scope.reportincident.follow = $scope.oldFollowUp;
        //console.log('$scope.groupmeeting.follow', $scope.groupmeeting.follow);
        $scope.okReportFollowup();
    };
    /**************SaveReportIncident ****************/
    $scope.SaveRI = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.reportincident.beneficiaryid = beneficiaryid;
        $scope.reportincident.fieldworkerid = $window.sessionStorage.UserEmployeeId;
        $scope.reportincident.coid = $window.sessionStorage.coorgId;
        $scope.reportincident.districtid = $window.sessionStorage.salesAreaId;
        $scope.reportincident.stateid = $window.sessionStorage.zoneId;
        $scope.reportincident.questionid = questionid;
        if ($scope.reportincident.physical == true || $scope.reportincident.sexual == true || $scope.reportincident.childrelated == true || $scope.reportincident.emotional == true || $scope.reportincident.propertyrelated == true) {
            if ($scope.reportincident.police == true || $scope.reportincident.clients == true || $scope.reportincident.serviceprovider == true || $scope.reportincident.otherkp == true || $scope.reportincident.neighbour == true || $scope.reportincident.goons == true || $scope.reportincident.partners == true || $scope.reportincident.familymember == true || $scope.reportincident.husband == true) {
                if ($scope.reportincident.reported == false || $scope.reportincident.reportedpolice == true || $scope.reportincident.reportedngos == true || $scope.reportincident.reportedfriends == true || $scope.reportincident.reportedplv == true || $scope.reportincident.reportedcoteam == true || $scope.reportincident.reportedchampions == true || $scope.reportincident.reportedotherkp == true || $scope.reportincident.reportedlegalaid == true) {
                    if ($scope.reportincident.referredcouncelling == true || $scope.reportincident.referredmedicalcare == true || $scope.reportincident.referredcomanager == true || $scope.reportincident.referredplv == true || $scope.reportincident.referredlegalaid == true || $scope.reportincident.referredboardmember == true || $scope.reportincident.noactionreqmember == true || $scope.reportincident.noactiontaken == true) {
                        //$scope.SaveReportIncident1();
                        if ($scope.dateofclosure == true) {
                            if ($scope.reportincident.follow != undefined && $scope.reportincident.follow.length > 0) {
                                $scope.reportcount = 0;
                                for (var i = 0; i < $scope.reportincident.follow.length; i++) {
                                    if (i == 0) {
                                        $scope.reportincident.followupneeded = $scope.reportincident.follow[i];
                                    } else {
                                        $scope.reportincident.followupneeded = $scope.reportincident.followupneeded + ',' + $scope.reportincident.follow[i];
                                    }
                                }
                                $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
                                $scope.SaveIncident(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
                                console.log('Save');
                            } else {
                                $scope.AlertMessage = 'Followup is mandatory';
                                $scope.openOneAlert();
                            }
                        } else {
                            $scope.reportcount = 0;
                            for (var i = 0; i < $scope.reportincident.follow.length; i++) {
                                if (i == 0) {
                                    $scope.reportincident.followupneeded = $scope.reportincident.follow[i];
                                } else {
                                    $scope.reportincident.followupneeded = $scope.reportincident.followupneeded + ',' + $scope.reportincident.follow[i];
                                }
                            }
                            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
                            $scope.SaveIncident(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
                            console.log('Save');
                        }
                    } else {
                        $scope.AlertMessage = $scope.optactiontaken; //'At least one option must be selected in Action Taken and Followup';
                        $scope.openOneAlert();
                    }
                } else {
                    $scope.AlertMessage = $scope.optreportedto; //'At least one option must be selected in Reported To';
                    $scope.openOneAlert();
                }
            } else {
                $scope.AlertMessage = $scope.optpreperator; //'At least one option must be selected in  Perpetrator Details';
                $scope.openOneAlert();
            }
        } else {
            $scope.AlertMessage = $scope.optincidenttype; //'At least one option must be selected in Incident Type';
            $scope.openOneAlert();
        }
    };
    $scope.okRI = function () {
        $scope.modalRI.close();
    };
    $scope.newtodocount = 0;
    $scope.reportadd = 0;
    $scope.SaveIncident = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        if ($scope.reportincident.co === false) {
            $scope.reportincident.beneficiaryid = $window.sessionStorage.fullName;
            $scope.reportincident.stress_data = $scope.selectedBeneficiary.stress_data;
            $scope.submitreportincidents.post($scope.reportincident).then(function (onemember) {
                $scope.reportincident = {
                    // "referredby":false,
                    "dtmin": new Date(),
                    "dtmax": new Date(),
                    "currentstatus": 'Needs Follow Up',
                    "physical": false,
                    "emotional": false,
                    "sexual": false,
                    "propertyrelated": false,
                    "childrelated": false, //"other": false,
                    "police": false,
                    "goons": false,
                    "clients": false,
                    "partners": false,
                    "husband": false,
                    "serviceprovider": false,
                    "familymember": false,
                    "otherkp": false,
                    "perpetratorother": false,
                    "neighbour": false,
                    "authorities": false,
                    "co": false,
                    "timetorespond": false,
                    "severity": "Moderate",
                    "resolved": false,
                    "reported": false,
                    "reportedpolice": false,
                    "reportedngos": false,
                    "reportedfriends": false,
                    "reportedplv": false,
                    "reportedcoteam": false,
                    "reportedchampions": false,
                    "reportedotherkp": false,
                    "reportedlegalaid": false,
                    "referredcouncelling": false,
                    "referredmedicalcare": false,
                    "referredcomanager": false,
                    "referredplv": false,
                    "referredlegalaid": false,
                    "referredboardmember": false,
                    "noactionreqmember": false,
                    "noactiontaken": false,
                    "multiplemembers": ['1', '2'],
                    "stateid": $window.sessionStorage.zoneId,
                    "state": $window.sessionStorage.zoneId,
                    "districtid": $window.sessionStorage.salesAreaId,
                    "district": $window.sessionStorage.salesAreaId,
                    "coid": $window.sessionStorage.coorgId,
                    "facility": $window.sessionStorage.coorgId,
                    "beneficiaryid": null,
                    "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
                    "lastmodifiedtime": new Date(),
                    "lastmodifiedbyrole": $window.sessionStorage.roleId
                };
                var sevendays = new Date();
                sevendays.setDate(sevendays.getDate() + 7);
                $scope.reportincident.followupdate = sevendays;
                $scope.modalRI.close();
                if (pillarid == 1) {
                    $scope.spnextquestion();
                } else if (pillarid == 2) {
                    $scope.ssjnextquestion();
                } else if (pillarid == 3) {
                    $scope.fsnextquestion();
                } else if (pillarid == 4) {
                    $scope.idsnextquestion();
                } else if (pillarid == 5) {
                    $scope.healthnextquestion();
                }
            });
        } else if ($scope.reportincident.co === true) {
            if ($scope.reportincident.multiplemembers.length == 0) {
                $scope.AlertMessage = 'At least one member must be selected';
                $scope.openOneAlert();
            } else {
                $scope.CreateClicked = true;
                $scope.reportincident.beneficiaryid = $scope.reportincident.multiplemembers[$scope.reportadd];
                $scope.reportincident.stress_data = $scope.selectedBeneficiary.stress_data;
                $scope.submitreportincidents.post($scope.reportincident).then(function (allmember) {
                    $scope.reportadd++;
                    if ($scope.reportadd < $scope.reportincident.multiplemembers.length) {
                        $scope.SaveIncident(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
                    } else {
                        $scope.reportincident = {
                            // "referredby":false,
                            "dtmin": new Date(),
                            "dtmax": new Date(),
                            "currentstatus": 'Needs Follow Up',
                            "physical": false,
                            "emotional": false,
                            "sexual": false,
                            "propertyrelated": false,
                            "childrelated": false, //"other": false,
                            "police": false,
                            "goons": false,
                            "clients": false,
                            "partners": false,
                            "husband": false,
                            "serviceprovider": false,
                            "familymember": false,
                            "otherkp": false,
                            "perpetratorother": false,
                            "neighbour": false,
                            "authorities": false,
                            "co": false,
                            "timetorespond": false,
                            "severity": "Moderate",
                            "resolved": false,
                            "reported": false,
                            "reportedpolice": false,
                            "reportedngos": false,
                            "reportedfriends": false,
                            "reportedplv": false,
                            "reportedcoteam": false,
                            "reportedchampions": false,
                            "reportedotherkp": false,
                            "reportedlegalaid": false,
                            "referredcouncelling": false,
                            "referredmedicalcare": false,
                            "referredcomanager": false,
                            "referredplv": false,
                            "referredlegalaid": false,
                            "referredboardmember": false,
                            "noactionreqmember": false,
                            "noactiontaken": false,
                            "multiplemembers": ['1', '2'],
                            "stateid": $window.sessionStorage.zoneId,
                            "state": $window.sessionStorage.zoneId,
                            "districtid": $window.sessionStorage.salesAreaId,
                            "district": $window.sessionStorage.salesAreaId,
                            "coid": $window.sessionStorage.coorgId,
                            "facility": $window.sessionStorage.coorgId,
                            "beneficiaryid": null,
                            "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
                            "lastmodifiedtime": new Date(),
                            "lastmodifiedbyrole": $window.sessionStorage.roleId
                        };
                        var sevendays = new Date();
                        sevendays.setDate(sevendays.getDate() + 7);
                        $scope.reportincident.followupdate = sevendays;
                        $scope.modalRI.close();
                        if (pillarid == 1) {
                            $scope.spnextquestion();
                        } else if (pillarid == 2) {
                            $scope.ssjnextquestion();
                        } else if (pillarid == 3) {
                            $scope.fsnextquestion();
                        } else if (pillarid == 4) {
                            $scope.idsnextquestion();
                        } else if (pillarid == 5) {
                            $scope.healthnextquestion();
                        }
                    }
                });
            }
        };
    };
    $scope.openCL = function () {
        $scope.modalCL = $modal.open({
            animation: true,
            templateUrl: 'template/ChampionsList.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.SaveCL = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        $scope.modalCL.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okCL = function () {
        $scope.modalCL.close();
    };
    $scope.openDP = function () {
        $scope.modalDP = $modal.open({
            animation: true,
            templateUrl: 'template/DatePicker.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.beneficiarylastdate = {};
    $scope.SaveDP = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        if (questionid == 3) {
            $scope.beneficiarylastdate.monthoflasthivtest = $scope.picker.selectdate;
        } else if (questionid == 51 && answer == 'yes') {
            $scope.beneficiarylastdate.paidmember = 'yes';
        } else {
            $scope.beneficiarylastdate.monthoflaststitest = $scope.picker.selectdate;
        }
        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiarylastdate).then(function (respo) {
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        });
        $scope.modalDP.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okDP = function () {
        $scope.modalDP.close();
    };
    $scope.openIDP = function () {
        $scope.modalIDP = $modal.open({
            animation: true,
            templateUrl: 'template/IncrementDatePicker.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.beneficiaryincrement = {};
    $scope.SaveIDP = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        // console.log('Button Clicked', beneficiaryid + '::' + pillarid + ',questionid::' + questionid + ',answer::' + answer + ',modifieddate::' + modifieddate + ',modifiedby::' + modifiedby + ',serialno::' + serialno + ',yesincrement::' + yesincrement + ',noincrement::' + noincrement);
        if ($routeParams.id != undefined) {
            Restangular.one('beneficiaries', $routeParams.id).get().then(function (kp) {
                var d1 = Date.parse($scope.increment.selectdate);
                var d2 = Date.parse($scope.DisplayBeneficiary.monthoflastsaving);
                // console.log('d1', d1);
                // console.log('d2', d2);
                if (yesincrement == 'savings') {
                    if (kp.noofsavingaccounts == null) {
                        kp.noofsavingaccounts = 0;
                    }
                    $scope.beneficiaryincrement.noofsavingaccounts = +kp.noofsavingaccounts + 1;
                    if (d1 > d2) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                    }
                } else if (yesincrement == 'investmentproducts') {
                    if (kp.noofinvestmentproducts == null) {
                        kp.noofinvestmentproducts = 0;
                    }
                    $scope.beneficiaryincrement.noofinvestmentproducts = +kp.noofinvestmentproducts + 1;
                    if (d1 > d2) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                    }
                } else if (yesincrement == 'savingsources') {
                    if (kp.noofinformalsavingsources == null) {
                        kp.noofinformalsavingsources = 0;
                    }
                    $scope.beneficiaryincrement.noofinformalsavingsources = +kp.noofinformalsavingsources + 1;
                    if (d1 > d2) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                    }
                } else if (yesincrement == 'insuranceproducts') {
                    if (kp.noofinsuranceproducts == null) {
                        kp.noofinsuranceproducts = 0;
                    }
                    $scope.beneficiaryincrement.noofinsuranceproducts = +kp.noofinsuranceproducts + 1;
                    if (d1 > d2) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                    }
                } else if (yesincrement == 'pensionproducts') {
                    if (kp.noofpensionproducts == null) {
                        kp.noofpensionproducts = 0;
                    }
                    $scope.beneficiaryincrement.noofpensionproducts = +kp.noofpensionproducts + 1;
                    if (d1 > d2) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                    }
                } else if (yesincrement == 'informalcreditsources') {
                    if (kp.noofinformalcreditsources == null) {
                        kp.noofinformalcreditsources = 0;
                    }
                    $scope.beneficiaryincrement.noofinformalcreditsources = +kp.noofinformalcreditsources + 1;
                    if (d1 > d2) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                    }
                } else if (yesincrement == 'formalcreditsources') {
                    if (kp.noofformalcreditsources == null) {
                        kp.noofformalcreditsources = 0;
                    }
                    $scope.beneficiaryincrement.noofformalcreditsources = +kp.noofformalcreditsources + 1;
                    if (d1 > d2) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                    }
                } else if (yesincrement == 'reciept') {
                    $scope.beneficiaryincrement.paidmember = 'yes';
                    if (d1 > d2) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                    } else {
                        $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                    }
                } else if (yesincrement == 'dynamicmember') {
                    $scope.beneficiaryincrement.dynamicmember = true;
                }
                console.log('$scope.beneficiaryincrement', $scope.beneficiaryincrement);
                Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryincrement).then(function (respo) {
                    $scope.beneficiaryincrement = {};
                    $scope.increment = {};
                    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
                }, function (error) {
                    console.log('error', error);
                });
            });
        }
        $scope.modalIDP.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okIDP = function () {
        $scope.modalIDP.close();
    };
    $scope.openCDIDS = function () {
        $scope.modalCDIDS = $modal.open({
            animation: true,
            templateUrl: 'template/CDIDS.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.SaveCDIDS = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
        $scope.availeddate = $scope.cdids.availeddate;
        $scope.referenceno = $scope.cdids.referenceno;
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, $scope.availeddate, $scope.referenceno);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        $scope.modalCDIDS.close();
        if (pillarid == 1) {
            $scope.spquestioncount++;
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjquestioncount;
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsquestioncount;
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsquestioncount;
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthquestioncount;
            $scope.healthnextquestion();
        }
    };
    $scope.okCDIDS = function () {
        $scope.modalCDIDS.close();
    };
    $scope.schemestages = Restangular.all('schemestages?filter[where][deleteflag]=false').getList().$object;
    $scope.printschemes = Restangular.all('schemes?filter[where][deleteflag]=false').getList().$object;
    $scope.responcedreceived = Restangular.all('responcedreceived?filter[where][deleteflag]=false').getList().$object;
    $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteflag]=false').getList().$object;
    $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteflag]=false').getList().$object;
    $scope.openAW = function (size) {
        //console.log('I am Open');
        $scope.schememaster.memberId = $scope.DisplayBeneficiary.id;
        //console.log('schememaster', $scope.schememaster);
        $scope.modalAW = $modal.open({
            animation: true,
            templateUrl: 'template/ApplicationWorkflow.html',
            scope: $scope,
            backdrop: 'static',
            size: size
        });
    };
    $scope.SaveAW = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        console.log('SaveAW', beneficiaryid)
        if (answer == 'yes') {
            $scope.todo.documentid = yesdocumentid;
            $scope.todo.documentflag = yesdocumentflag;
            $scope.schememaster.documentflag = yesdocumentflag;
            $scope.schememaster.schemeId = yesdocumentid;
            // console.log('yesdocumentflag',yesdocumentflag);
            if (yesdocumentflag === true) {
                // console.log('ifyesdocumentflag',yesdocumentflag);
                $scope.todo.todotype = 29;
                $scope.todo.pillarid = pillarid;
                $scope.todo.questionid = questionid;
                $scope.todo.beneficiaryid = beneficiaryid;
                $scope.todo.datetime = $scope.schememaster.datetime;
                $scope.todo.stage = $scope.schememaster.stage;
                $scope.todo.facility = $window.sessionStorage.coorgId;
                $scope.todo.district = $window.sessionStorage.salesAreaId;
                $scope.todo.state = $window.sessionStorage.zoneId;
                $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.todo.lastmodifiedtime = new Date();
                $scope.todo.site = $scope.DisplayBeneficiary.site;
                $scope.todo.roleId = $window.sessionStorage.roleId;
                $scope.schememaster.memberId = beneficiaryid;
                $scope.schememaster.facility = $window.sessionStorage.coorgId;
                $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                $scope.schememaster.state = $window.sessionStorage.zoneId;
                $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.schememaster.lastmodifiedtime = new Date();
                $scope.schememaster.stress_data = $scope.selectedBeneficiary.stress_data;
                $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                    // console.log('$scope.documentmasters', $scope.schememaster);
                    $scope.todo.reportincidentid = resp.id;
                    if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                        $scope.todo.status = 3;
                    } else
                    if ($scope.schememaster.stage == "8") {
                        $scope.schememaster.stage = "3";
                    } else if (resp.stage == "9" || resp.stage == "7") {
                        $scope.todo.status = 3;
                    } else {
                        $scope.todo.status = 1;
                    }
                    $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
                    $scope.submittodos.post($scope.todo).then(function (resp) {
                        console.log('submittodos3', resp);
                        $scope.todo = {};
                        $scope.todo = {
                            status: 1,
                            stateid: $window.sessionStorage.zoneId,
                            state: $window.sessionStorage.zoneId,
                            districtid: $window.sessionStorage.salesAreaId,
                            district: $window.sessionStorage.salesAreaId,
                            coid: $window.sessionStorage.coorgId,
                            facility: $window.sessionStorage.coorgId,
                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                            lastmodifiedtime: new Date(),
                            roleId: $window.sessionStorage.roleId
                        };
                        //console.log('$scope.todo', $scope.todo);
                        $scope.SchemeOrDocumentname = null;
                        $scope.schememaster.stage = null;
                    });
                });
            } else {
                $scope.todo.todotype = 28;
                Restangular.one('schemes?filter[where][state]=' + $scope.DisplayBeneficiary.state + '&filter[where][topscheme]=' + yesdocumentid).get().then(function (schemes) {
                    if (schemes.length > 0) {
                        console.log('$scope.loanapplied1', $scope.loanapplied)
                        if ($scope.loanapplied == true || $scope.schememaster.stage == 7) {
                            console.log('check in if');
                            if ($scope.schememaster.purposeofloan != 5) {
                                $scope.todo.status = 3;
                                console.log('$scope.todo.status2', $scope.todo.status);
                            } else if ($scope.schememaster.purposeofloan == 5 && $scope.schememaster.amount < 10000) {
                                $scope.todo.status = 3;
                                console.log('$scope.todo.status1', $scope.todo.status);
                            } else if ($scope.paidamounts == $scope.schememaster.amount) {
                                $scope.todo.status = 3;
                                console.log('$scope.todo.status3', $scope.todo.status);
                            } else {
                                $scope.todo.status = 1;
                                console.log('$scope.todo.status0', $scope.todo.status);
                            }
                        } else {
                            console.log('check in else');
                            if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                                $scope.todo.status = 3;
                                //console.log('$scope.todo.status6', $scope.todo.status);
                            } else if ($scope.schememaster.stage == 9) {
                                $scope.todo.status = 3;
                                //console.log('$scope.todo.status5', $scope.todo.status);
                            } else {
                                $scope.todo.status = 1;
                                console.log('$scope.todo.status4', $scope.todo.status);
                            }
                        }
                        $scope.todo.documentid = schemes[0].id;
                        $scope.schememaster.schemeId = schemes[0].id;
                        $scope.todo.pillarid = pillarid;
                        $scope.todo.questionid = questionid;
                        $scope.todo.beneficiaryid = beneficiaryid;
                        $scope.todo.datetime = $scope.schememaster.datetime;
                        $scope.todo.stage = $scope.schememaster.stage;
                        $scope.todo.facility = $window.sessionStorage.coorgId;
                        $scope.todo.district = $window.sessionStorage.salesAreaId;
                        $scope.todo.state = $window.sessionStorage.zoneId;
                        $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                        $scope.todo.lastmodifiedtime = new Date();
                        $scope.todo.site = $scope.DisplayBeneficiary.site;
                        $scope.todo.roleId = $window.sessionStorage.roleId;
                        $scope.schememaster.memberId = beneficiaryid;
                        $scope.schememaster.facility = $window.sessionStorage.coorgId;
                        $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                        $scope.schememaster.state = $window.sessionStorage.zoneId;
                        $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                        $scope.schememaster.lastmodifiedtime = new Date();
                        $scope.schememaster.stress_data = $scope.selectedBeneficiary.stress_data;
                        $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                            console.log('$scope.documentmasters', $scope.schememaster);
                            $scope.todo.reportincidentid = resp.id;
                            /* if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                                 $scope.todo.status = 3;
                             }
                             else
                             if ($scope.schememaster.stage == "8") {
                                 $scope.schememaster.stage = "3";
                             }
                             else if (resp.stage == "9" || resp.stage == "7") {
                                 $scope.todo.status = 3;
                             }
                             else {
                                 $scope.todo.status = 1;
                             }*/
                            console.log('$scope.loanapplied', $scope.loanapplied)
                            if ($scope.loanapplied == true || $scope.schememaster.stage == 7) {
                                console.log('check in if');
                                if ($scope.schememaster.purposeofloan != 5) {
                                    $scope.todo.status = 3;
                                    console.log('$scope.todo.status2', $scope.todo.status);
                                } else if ($scope.schememaster.purposeofloan == 5 && $scope.schememaster.amount < 10000) {
                                    $scope.todo.status = 3;
                                    console.log('$scope.todo.status1', $scope.todo.status);
                                } else if ($scope.paidamounts == $scope.schememaster.amount) {
                                    $scope.todo.status = 3;
                                    console.log('$scope.todo.status3', $scope.todo.status);
                                } else {
                                    $scope.todo.status = 1;
                                    console.log('$scope.todo.status0', $scope.todo.status);
                                }
                            } else {
                                console.log('check in else');
                                if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                                    $scope.todo.status = 3;
                                    //console.log('$scope.todo.status6', $scope.todo.status);
                                } else if ($scope.schememaster.stage == 9) {
                                    $scope.todo.status = 3;
                                    //console.log('$scope.todo.status5', $scope.todo.status);
                                } else {
                                    $scope.todo.status = 1;
                                    console.log('$scope.todo.status4', $scope.todo.status);
                                }
                            }
                            $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
                            $scope.submittodos.post($scope.todo).then(function (resp) {
                                console.log('submittodos4', resp);
                                $scope.todo = {};
                                $scope.todo = {
                                    status: 1,
                                    stateid: $window.sessionStorage.zoneId,
                                    state: $window.sessionStorage.zoneId,
                                    districtid: $window.sessionStorage.salesAreaId,
                                    district: $window.sessionStorage.salesAreaId,
                                    coid: $window.sessionStorage.coorgId,
                                    facility: $window.sessionStorage.coorgId,
                                    lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                    lastmodifiedtime: new Date(),
                                    roleId: $window.sessionStorage.roleId
                                };
                                // console.log('$scope.todo', $scope.todo);
                                $scope.SchemeOrDocumentname = null;
                                $scope.schememaster.stage = null;
                            });
                        });
                    }
                });
            }
        } else {
            $scope.todo.documentid = nodocumentid;
            $scope.todo.documentflag = nodocumentflag;
            $scope.schememaster.documentflag = nodocumentflag;
            $scope.schememaster.schemeId = nodocumentid;
            if (yesdocumentflag == true) {
                $scope.todo.todotype = 29;
                $scope.todo.pillarid = pillarid;
                $scope.todo.questionid = questionid;
                $scope.todo.beneficiaryid = beneficiaryid;
                $scope.todo.datetime = $scope.schememaster.datetime;
                $scope.todo.stage = $scope.schememaster.stage;
                $scope.todo.facility = $window.sessionStorage.coorgId;
                $scope.todo.district = $window.sessionStorage.salesAreaId;
                $scope.todo.state = $window.sessionStorage.zoneId;
                $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.todo.lastmodifiedtime = new Date();
                $scope.todo.site = $scope.DisplayBeneficiary.site;
                $scope.todo.roleId = $window.sessionStorage.roleId;
                $scope.schememaster.memberId = beneficiaryid;
                $scope.schememaster.facility = $window.sessionStorage.coorgId;
                $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                $scope.schememaster.state = $window.sessionStorage.zoneId;
                $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.schememaster.lastmodifiedtime = new Date();
                $scope.schememaster.stress_data = $scope.selectedBeneficiary.stress_data;
                $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                    // console.log('$scope.documentmasters', $scope.schememaster);
                    $scope.todo.reportincidentid = resp.id;
                    if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                        $scope.todo.status = 3;
                    } else
                    if ($scope.schememaster.stage == "8") {
                        $scope.schememaster.stage = "3";
                    } else if (resp.stage == "9" || resp.stage == "7") {
                        $scope.todo.status = 3;
                    } else {
                        $scope.todo.status = 1;
                    }
                    $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
                    $scope.submittodos.post($scope.todo).then(function () {
                        //console.log('$scope.todo', $scope.todo);
                        $scope.todo = {};
                        $scope.todo = {
                            status: 1,
                            stateid: $window.sessionStorage.zoneId,
                            state: $window.sessionStorage.zoneId,
                            districtid: $window.sessionStorage.salesAreaId,
                            district: $window.sessionStorage.salesAreaId,
                            coid: $window.sessionStorage.coorgId,
                            facility: $window.sessionStorage.coorgId,
                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                            lastmodifiedtime: new Date(),
                            roleId: $window.sessionStorage.roleId
                        };
                        $scope.SchemeOrDocumentname = null;
                        $scope.schememaster.stage = null;
                    });
                });
            } else {
                $scope.todo.todotype = 28;
                Restangular.one('schemes?filter[where][state]=' + $scope.DisplayBeneficiary.state + '&filter[where][topscheme]=' + nodocumentid).get().then(function (schemes) {
                    if (schemes.length > 0) {
                        $scope.todo.documentid = schemes[0].id;
                        $scope.schememaster.schemeId = schemes[0].id;
                        $scope.todo.pillarid = pillarid;
                        $scope.todo.questionid = questionid;
                        $scope.todo.beneficiaryid = beneficiaryid;
                        $scope.todo.datetime = $scope.schememaster.datetime;
                        $scope.todo.stage = $scope.schememaster.stage;
                        $scope.todo.facility = $window.sessionStorage.coorgId;
                        $scope.todo.district = $window.sessionStorage.salesAreaId;
                        $scope.todo.state = $window.sessionStorage.zoneId;
                        $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                        $scope.todo.lastmodifiedtime = new Date();
                        $scope.todo.site = $scope.DisplayBeneficiary.site;
                        $scope.todo.roleId = $window.sessionStorage.roleId;
                        $scope.schememaster.memberId = beneficiaryid;
                        $scope.schememaster.facility = $window.sessionStorage.coorgId;
                        $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                        $scope.schememaster.state = $window.sessionStorage.zoneId;
                        $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                        $scope.schememaster.lastmodifiedtime = new Date();
                        $scope.schememaster.stress_data = $scope.selectedBeneficiary.stress_data;
                        $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                            //console.log('$scope.documentmasters', $scope.schememaster);
                            $scope.todo.reportincidentid = resp.id;
                            if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                                $scope.todo.status = 3;
                            } else
                            if ($scope.schememaster.stage == "8") {
                                $scope.schememaster.stage = "3";
                            } else if (resp.stage == "9" || resp.stage == "7") {
                                $scope.todo.status = 3;
                            } else {
                                $scope.todo.status = 1;
                            }
                            $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
                            $scope.submittodos.post($scope.todo).then(function (resp) {
                                console.log('submittodos5', resp);
                                $scope.todo = {};
                                $scope.todo = {
                                    status: 1,
                                    stateid: $window.sessionStorage.zoneId,
                                    state: $window.sessionStorage.zoneId,
                                    districtid: $window.sessionStorage.salesAreaId,
                                    district: $window.sessionStorage.salesAreaId,
                                    coid: $window.sessionStorage.coorgId,
                                    facility: $window.sessionStorage.coorgId,
                                    lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                    lastmodifiedtime: new Date(),
                                    roleId: $window.sessionStorage.roleId
                                };
                                //console.log('$scope.todo', $scope.todo);
                                $scope.SchemeOrDocumentname = null;
                                $scope.schememaster.stage = null;
                            });
                        });
                    }
                });
            }
        }
        $scope.modalAW.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okAW = function () {
        $scope.todo = {
            status: 1,
            stateid: $window.sessionStorage.zoneId,
            state: $window.sessionStorage.zoneId,
            districtid: $window.sessionStorage.salesAreaId,
            district: $window.sessionStorage.salesAreaId,
            coid: $window.sessionStorage.coorgId,
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            roleId: $window.sessionStorage.roleId
        };
        //$scope.schememaster={};
        $scope.modalAW.close();
    };
    $scope.openAWBoth = function (size) {
        //console.log('I am also Open');
        //$scope.schemestages = Restangular.all('schemestages').getList().$object;
        //$scope.printschemes = Restangular.all('schemes?filter[where][deleteflag]=false').getList().$object;
        //$scope.responcedreceived = Restangular.all('responcedreceived?filter[where][deleteflag]=false').getList().$object;
        //$scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteflag]=false').getList().$object;
        //$scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteflag]=false').getList().$object;
        $scope.schememaster.memberId = $scope.DisplayBeneficiary.id;
        $scope.schememaster.documentflag = true;
        $scope.modalAWBoth = $modal.open({
            animation: true,
            templateUrl: 'template/ApplicationWorkflowAll.html',
            scope: $scope,
            backdrop: 'static',
            size: size
        });
    };
    $scope.SaveAWBoth = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        console.log('SaveAWBoth', pillarid)
        if (answer == 'yes') {
            $scope.todo.documentid = $scope.schememaster.schemeId;
            $scope.todo.documentflag = $scope.schememaster.documentflag;
            if (yesdocumentflag == true) {
                $scope.todo.todotype = 29;
                $scope.todo.pillarid = pillarid;
                $scope.todo.questionid = questionid;
                $scope.todo.beneficiaryid = beneficiaryid;
                $scope.todo.datetime = $scope.schememaster.datetime;
                $scope.todo.stage = $scope.schememaster.stage;
                $scope.todo.facility = $window.sessionStorage.coorgId;
                $scope.todo.district = $window.sessionStorage.salesAreaId;
                $scope.todo.state = $window.sessionStorage.zoneId;
                $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.todo.lastmodifiedtime = new Date();
                $scope.todo.site = $scope.DisplayBeneficiary.site;
                $scope.todo.roleId = $window.sessionStorage.roleId;
                $scope.schememaster.memberId = beneficiaryid;
                $scope.schememaster.facility = $window.sessionStorage.coorgId;
                $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                $scope.schememaster.state = $window.sessionStorage.zoneId;
                $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                // $scope.schememaster.roleId = $window.sessionStorage.roleId;
                $scope.schememaster.lastmodifiedtime = new Date();
                $scope.schememaster.stress_data = $scope.selectedBeneficiary.stress_data;
                $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                    console.log('$scope.documentmasters1', $scope.schememaster);
                    $scope.todo.reportincidentid = resp.id;
                    if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                        $scope.todo.status = 3;
                    } else
                    if ($scope.schememaster.stage == "8") {
                        $scope.schememaster.stage = "3";
                    } else if (resp.stage == "9" || resp.stage == "7") {
                        $scope.todo.status = 3;
                    } else {
                        $scope.todo.status = 1;
                    }
                    $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
                    $scope.submittodos.post($scope.todo).then(function () {
                        $scope.todo = {};
                        $scope.todo = {
                            status: 1,
                            stateid: $window.sessionStorage.zoneId,
                            state: $window.sessionStorage.zoneId,
                            districtid: $window.sessionStorage.salesAreaId,
                            district: $window.sessionStorage.salesAreaId,
                            coid: $window.sessionStorage.coorgId,
                            facility: $window.sessionStorage.coorgId,
                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                            lastmodifiedtime: new Date(),
                            roleId: $window.sessionStorage.roleId
                        };
                        //console.log('$scope.todo', $scope.todo);
                        $scope.SchemeOrDocumentname = null;
                        $scope.schememaster.stage = null;
                    });
                });
            } else {
                console.log('Working1');
                $scope.todo.todotype = 28;
                $scope.todo.pillarid = pillarid;
                $scope.todo.questionid = questionid;
                $scope.todo.beneficiaryid = beneficiaryid;
                $scope.todo.datetime = $scope.schememaster.datetime;
                $scope.todo.stage = $scope.schememaster.stage;
                $scope.todo.facility = $window.sessionStorage.coorgId;
                $scope.todo.district = $window.sessionStorage.salesAreaId;
                $scope.todo.state = $window.sessionStorage.zoneId;
                $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.todo.lastmodifiedtime = new Date();
                $scope.todo.site = $scope.DisplayBeneficiary.site;
                $scope.todo.roleId = $window.sessionStorage.roleId;
                $scope.schememaster.memberId = beneficiaryid;
                $scope.schememaster.facility = $window.sessionStorage.coorgId;
                $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                $scope.schememaster.state = $window.sessionStorage.zoneId;
                $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.schememaster.lastmodifiedtime = new Date();
                console.log('$scope.mygenderstatus', $scope.mygenderstatus);
                /* if ($scope.mygenderstatus == false) {
                     $scope.AlertMessage = $scope.gendernotmatch; //'Gender is not Matching for This Scheme';
                     $scope.openOneAlert();
                 } else */
                if ($scope.myoccupationstatus == false) {

                    $scope.AlertMessage = $scope.typologynotmatch; //'Typology is not Matching for This Scheme';
                    $scope.openOneAlert();
                } else if ($scope.myagestatus == false) {
                    //$scope.validatestring =  $scope.validatestring + $scope.agenotmatch; 

                    $scope.AlertMessage = $scope.agenotmatch; //'Age is not Matching for This Scheme';
                    $scope.openOneAlert();
                } else if ($scope.myhealthstatus == false) {
                    $scope.AlertMessage = $scope.healthstnotmatch; //'Health Status is not Matching for This Scheme';
                    $scope.openOneAlert();
                } else if ($scope.mysocialstatus == false) {
                    $scope.AlertMessage = $scope.socialstnotmatch; //'Social Status is not Matching for This Scheme';
                    $scope.openOneAlert();
                } else {
                    $scope.schememaster.stress_data = $scope.selectedBeneficiary.stress_data;
                    $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                        console.log('$scope.schememasters2', resp);

                        if ($scope.loanapplied == true && $scope.schememaster.stage == 7) {
                            //console.log('check in if');
                            if ($scope.schememaster.purposeofloan != 5) {
                                $scope.todo.status = 3;
                                //console.log('$scope.todo.status2', $scope.todo.status);
                            } else if ($scope.schememaster.purposeofloan == 5 && $scope.schememaster.amount < 10000) {
                                $scope.todo.status = 3;
                                //console.log('$scope.todo.status1', $scope.todo.status);
                            } else if ($scope.paidamounts == $scope.schememaster.amount) {
                                $scope.todo.status = 3;
                                //console.log('$scope.todo.status3', $scope.todo.status);
                            } else {
                                $scope.todo.status = 1;
                                //console.log('$scope.todo.status0', $scope.todo.status);
                            }
                        } else {
                            //console.log('check in else');
                            if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                                $scope.todo.status = 3;
                                //console.log('$scope.todo.status6', $scope.todo.status);
                            } else if ($scope.schememaster.stage == 9) {
                                $scope.todo.status = 3;
                                //console.log('$scope.todo.status5', $scope.todo.status);
                            } else {
                                $scope.todo.status = 1;
                                //console.log('$scope.todo.status4', $scope.todo.status);
                            }
                        }
                        $scope.todo.reportincidentid = resp.id;
                        $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
                        $scope.submittodos.post($scope.todo).then(function (resp) {
                            console.log('submittodos6', resp);
                            $scope.todo = {};
                            $scope.todo = {
                                status: 1,
                                stateid: $window.sessionStorage.zoneId,
                                state: $window.sessionStorage.zoneId,
                                districtid: $window.sessionStorage.salesAreaId,
                                district: $window.sessionStorage.salesAreaId,
                                coid: $window.sessionStorage.coorgId,
                                facility: $window.sessionStorage.coorgId,
                                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                lastmodifiedtime: new Date(),
                                roleId: $window.sessionStorage.roleId
                            };
                            //console.log('$scope.todo', $scope.todo);
                            $scope.SchemeOrDocumentname = null;
                            $scope.schememaster.stage = null;
                        });
                    });
                    $scope.modalAWBoth.close();
                }
            }

        } else {
            $scope.todo.documentid = $scope.schememaster.schemeId;
            $scope.todo.documentflag = $scope.schememaster.documentflag;
            if (yesdocumentflag == true) {
                $scope.todo.todotype = 29;
                $scope.todo.pillarid = pillarid;
                $scope.todo.questionid = questionid;
                $scope.todo.beneficiaryid = beneficiaryid;
                $scope.todo.datetime = $scope.schememaster.datetime;
                $scope.todo.stage = $scope.schememaster.stage;
                $scope.todo.facility = $window.sessionStorage.coorgId;
                $scope.todo.district = $window.sessionStorage.salesAreaId;
                $scope.todo.state = $window.sessionStorage.zoneId;
                $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.todo.lastmodifiedtime = new Date();
                $scope.todo.site = $scope.DisplayBeneficiary.site;
                $scope.todo.roleId = $window.sessionStorage.roleId;
                $scope.schememaster.memberId = beneficiaryid;
                $scope.schememaster.facility = $window.sessionStorage.coorgId;
                $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                $scope.schememaster.state = $window.sessionStorage.zoneId;
                $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.schememaster.lastmodifiedtime = new Date();
                $scope.schememaster.stress_data = $scope.selectedBeneficiary.stress_data;
                $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                    //console.log('$scope.documentmasters', $scope.schememaster);
                    if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                        $scope.todo.status = 3;
                    } else
                    if ($scope.schememaster.stage == "8") {
                        $scope.schememaster.stage = "3";
                    } else if (resp.stage == "9" || resp.stage == "7") {
                        $scope.todo.status = 3;
                    } else {
                        $scope.todo.status = 1;
                    }
                    $scope.todo.reportincidentid = resp.id;
                    $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
                    $scope.submittodos.post($scope.todo).then(function (resp) {
                        console.log('submittodos7', resp);
                        $scope.todo = {};
                        $scope.todo = {
                            status: 1,
                            stateid: $window.sessionStorage.zoneId,
                            state: $window.sessionStorage.zoneId,
                            districtid: $window.sessionStorage.salesAreaId,
                            district: $window.sessionStorage.salesAreaId,
                            coid: $window.sessionStorage.coorgId,
                            facility: $window.sessionStorage.coorgId,
                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                            lastmodifiedtime: new Date(),
                            roleId: $window.sessionStorage.roleId
                        };
                        // console.log('$scope.todo', $scope.todo);
                        $scope.SchemeOrDocumentname = null;
                        $scope.schememaster.stage = null;
                    });
                });
            } else {
                console.log('Working2');
                $scope.todo.todotype = 28;
                $scope.todo.pillarid = pillarid;
                $scope.todo.questionid = questionid;
                $scope.todo.beneficiaryid = beneficiaryid;
                $scope.todo.datetime = $scope.schememaster.datetime;
                $scope.todo.stage = $scope.schememaster.stage;
                $scope.todo.facility = $window.sessionStorage.coorgId;
                $scope.todo.district = $window.sessionStorage.salesAreaId;
                $scope.todo.state = $window.sessionStorage.zoneId;
                $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.todo.roleId = $window.sessionStorage.roleId;
                $scope.todo.lastmodifiedtime = new Date();
                $scope.todo.site = $scope.DisplayBeneficiary.site;
                $scope.schememaster.memberId = beneficiaryid;
                $scope.schememaster.facility = $window.sessionStorage.coorgId;
                $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                $scope.schememaster.state = $window.sessionStorage.zoneId;
                $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.schememaster.lastmodifiedtime = new Date();
                /*if ($scope.mygenderstatus == false) {
                    $scope.AlertMessage = $scope.gendernotmatch; //'Gender is not Matching for This Scheme';
                    $scope.openOneAlert();
                } else */
                if ($scope.myoccupationstatus == false) {

                    $scope.AlertMessage = $scope.typologynotmatch; //'Typology is not Matching for This Scheme';
                    $scope.openOneAlert();
                } else if ($scope.myagestatus == false) {
                    //$scope.validatestring =  $scope.validatestring + $scope.agenotmatch; 

                    $scope.AlertMessage = $scope.agenotmatch; //'Age is not Matching for This Scheme';
                    $scope.openOneAlert();
                } else if ($scope.myhealthstatus == false) {
                    $scope.AlertMessage = $scope.healthstnotmatch; //'Health Status is not Matching for This Scheme';
                    $scope.openOneAlert();
                } else if ($scope.mysocialstatus == false) {
                    $scope.AlertMessage = $scope.socialstnotmatch; //'Social Status is not Matching for This Scheme';
                    $scope.openOneAlert();
                } else {
                    $scope.schememaster.stress_data = $scope.selectedBeneficiary.stress_data;
                    $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                        if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                            $scope.todo.status = 3;
                        } else
                        if ($scope.schememaster.stage == "8") {
                            $scope.schememaster.stage = "3";
                        } else if (resp.stage == "9" || resp.stage == "7") {
                            $scope.todo.status = 3;
                        } else {
                            $scope.todo.status = 1;
                        }
                        //console.log('$scope.schememasters', resp);
                        $scope.todo.reportincidentid = resp.id;
                        $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
                        $scope.submittodos.post($scope.todo).then(function (resp) {
                            console.log('submittodos8', resp);
                            $scope.todo = {};
                            $scope.todo = {
                                status: 1,
                                stateid: $window.sessionStorage.zoneId,
                                state: $window.sessionStorage.zoneId,
                                districtid: $window.sessionStorage.salesAreaId,
                                district: $window.sessionStorage.salesAreaId,
                                coid: $window.sessionStorage.coorgId,
                                facility: $window.sessionStorage.coorgId,
                                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                lastmodifiedtime: new Date(),
                                roleId: $window.sessionStorage.roleId
                            };
                            // console.log('$scope.todo', $scope.todo);
                            $scope.SchemeOrDocumentname = null;
                            $scope.schememaster.stage = null;
                        });
                    });
                }
            }
            $scope.modalAWBoth.close();
        }
        //$scope.modalAWBoth.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okAWBoth = function () {
        $scope.todo = {
            status: 1,
            stateid: $window.sessionStorage.zoneId,
            state: $window.sessionStorage.zoneId,
            districtid: $window.sessionStorage.salesAreaId,
            district: $window.sessionStorage.salesAreaId,
            coid: $window.sessionStorage.coorgId,
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            roleId: $window.sessionStorage.roleId
        };
        $scope.schememaster = {};
        $scope.modalAWBoth.close();
    };
    $scope.openPLHIV = function () {
        $scope.plhiv.source = $scope.DisplayBeneficiary.sourceofinfection;
        $scope.plhiv.month = $scope.DisplayBeneficiary.monthofdetection;
        $scope.modalPLHIV = $modal.open({
            animation: true,
            templateUrl: 'template/PLHIV.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.beneficiaryplhiv = {};
    $scope.SavePLHIV = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.beneficiaryplhiv.plhiv = 'yes';
        $scope.beneficiaryplhiv.sourceofinfection = $scope.plhiv.source;
        $scope.beneficiaryplhiv.monthofdetection = $scope.plhiv.month;
        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryplhiv).then(function (respo) {
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        });
        $scope.modalPLHIV.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okPLHIV = function () {
        $scope.modalPLHIV.close();
    };
    $scope.openCD = function () {
        if ($scope.DisplayBeneficiary.condomsasked != null) {
            var CondomsAsked = $scope.DisplayBeneficiary.condomsasked.split(',');
            var CondomsProvided = $scope.DisplayBeneficiary.condomsprovided.split(',');
            if (CondomsAsked.length > 0) {
                var AskedNum = CondomsAsked[CondomsAsked.length - 1].split(':');
                var ProvidedNum = CondomsProvided[CondomsProvided.length - 1].split(':');
                $scope.condom.asked = AskedNum[AskedNum.length - 1];
                $scope.condom.provided = ProvidedNum[ProvidedNum.length - 1];
            }
        }
        $scope.modalCD = $modal.open({
            animation: true,
            templateUrl: 'template/condomdetails.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.SaveCD = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        //console.log('$scope.condom.asked', $scope.condom.asked);
        //console.log('$scope.condom.provided', $scope.condom.provided);
        if ($scope.condom.asked == '' || $scope.condom.asked == null || $scope.condom.asked == undefined) {
            $scope.AlertMessage = $scope.condomaskcantempty; //'Condoms Asked cannot be Empty';
            $scope.openOneAlert();
        } else if ($scope.condom.provided == '' || $scope.condom.provided == null || $scope.condom.provided == undefined) {
            $scope.AlertMessage = $scope.condomprovidecantempty //'Condoms Provided cannot be Empty';
            $scope.openOneAlert();
        } else if ($scope.condom.dateasked == '' || $scope.condom.dateasked == null || $scope.condom.dateasked == undefined) {
            $scope.AlertMessage = $scope.dateprovidedcantempty; //'Date Provided cannot be Empty';
            $scope.openOneAlert();
        } else {
            //$scope.askeddate = $filter('date')($scope.condom.dateasked, 'dd/MM/yyyy');
            $scope.askeddate = $filter('date')(new Date(), 'yyyy-MM-dd') + 'T00:00:00.000Z'; //$filter('date')($scope.condom.dateasked, 'yyyy/MM/dd');
            //['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate']);
            //console.log('new Date()', new Date());
            if ($scope.DisplayBeneficiary.condomsasked != null) {
                $scope.beneficiarycondom.dateprovided = $scope.condom.dateasked;
                $scope.beneficiarycondom.condomsasked = $scope.DisplayBeneficiary.condomsasked + ',' + $scope.askeddate + ':' + $scope.condom.asked;
                $scope.beneficiarycondom.condomsprovided = $scope.DisplayBeneficiary.condomsprovided + ',' + $scope.askeddate + ':' + $scope.condom.provided;
            } else {
                $scope.beneficiarycondom.dateprovided = $scope.condom.dateasked;
                $scope.beneficiarycondom.condomsasked = $scope.askeddate + ':' + $scope.condom.asked;
                $scope.beneficiarycondom.condomsprovided = $scope.askeddate + ':' + $scope.condom.provided;
            }
            Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiarycondom).then(function (respo) {
                $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
            });
            $scope.modalCD.close();
            if (pillarid == 1) {
                $scope.spnextquestion();
            } else if (pillarid == 2) {
                $scope.ssjnextquestion();
            } else if (pillarid == 3) {
                $scope.fsnextquestion();
            } else if (pillarid == 4) {
                $scope.idsnextquestion();
            } else if (pillarid == 5) {
                $scope.healthnextquestion();
            }
        }
    };
    $scope.okCD = function () {
        $scope.modalCD.close();
    };
    $scope.openFL = function () {
        $scope.finliteracy.savings = $scope.DisplayBeneficiary.savings;
        $scope.finliteracy.insurance = $scope.DisplayBeneficiary.insurance;
        $scope.finliteracy.pension = $scope.DisplayBeneficiary.pension;
        $scope.finliteracy.credit = $scope.DisplayBeneficiary.credit;
        //$scope.finliteracy.remitance = $scope.DisplayBeneficiary.remitance;
        $scope.modalFL = $modal.open({
            animation: true,
            templateUrl: 'template/FinancialLiteracy.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.beneficiaryfl = {};
    $scope.SaveFL = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.beneficiaryfl.savings = $scope.finliteracy.savings;
        $scope.beneficiaryfl.insurance = $scope.finliteracy.insurance;
        $scope.beneficiaryfl.pension = $scope.finliteracy.pension;
        $scope.beneficiaryfl.credit = $scope.finliteracy.credit;
        //$scope.beneficiaryfl.remitance = $scope.finliteracy.remitance;
        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryfl).then(function (respo) {
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        });
        $scope.modalFL.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okFL = function () {
        $scope.modalFL.close();
    };
    $scope.openFP = function () {
        $scope.finplanning.savings = $scope.DisplayBeneficiary.finplansavings;
        $scope.finplanning.insurance = $scope.DisplayBeneficiary.finplaninsurance;
        $scope.finplanning.pension = $scope.DisplayBeneficiary.finplanpension;
        $scope.finplanning.credit = $scope.DisplayBeneficiary.finplancredit;
        //$scope.finplanning.remitance = $scope.DisplayBeneficiary.finplanremitance;
        $scope.modalFP = $modal.open({
            animation: true,
            templateUrl: 'template/FinancialPlanning.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.openINFO = function () {
        $scope.modalINFO = $modal.open({
            animation: true,
            templateUrl: 'template/ProvideInfo.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.openPT = function () {
        $scope.modalINFO = $modal.open({
            animation: true,
            templateUrl: 'template/PhoneType.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    //  $scope.okPT = function () {
    //     $scope.modalINFO.close();
    // };
    $scope.SaveINFO = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        $scope.modalINFO.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };
    $scope.okINFO = function () {
        $scope.modalINFO.close();
    };
    $scope.pillaropened = false;
    $scope.openPillarCompleted = function () {
        if ($scope.pillaropened == true) {
            $scope.cancelPillarCompleted();
        }
        $scope.pillaropened = true;
        $scope.modalPillarCompleted = $modal.open({
            animation: true,
            templateUrl: 'template/PillarCompleted.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.PillarCompleted = {
        completedpillar: 0,
        completedquestion: 0
    }
    $scope.SavePillarCompleted = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.PillarCompleted).then(function (respo) {
            $scope.modalPillarCompleted.close();
            //$route.reload();
            window.location = "/onetoone/" + $routeParams.id;
        });
    };
    $scope.okPillarCompleted = function () {
        $scope.pillaropened = false;
        $scope.modalPillarCompleted.close();
        //window.location = "/";
        $scope.goToAllTasks();
    };
    $scope.cancelPillarCompleted = function () {
        $scope.pillaropened = false;
        $scope.modalPillarCompleted.close();
    };
    $scope.beneficiaryfp = {};
    $scope.SaveFP = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.beneficiaryfp.finplansavings = $scope.finplanning.savings;
        $scope.beneficiaryfp.finplaninsurance = $scope.finplanning.insurance;
        $scope.beneficiaryfp.finplanpension = $scope.finplanning.pension;
        $scope.beneficiaryfp.finplancredit = $scope.finplanning.credit;
        //$scope.beneficiaryfp.finplanremitance = $scope.finplanning.remitance;
        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryfp).then(function (respo) {
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        });
        $scope.modalFP.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };

    $scope.beneficiarypt = {};
    $scope.SavePT = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        $scope.beneficiarypt.phonetype = $scope.phonetype.phonetp;
        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiarypt).then(function (respo) {
            //console.log('respo',respo);
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
        });
        $scope.modalINFO.close();
        if (pillarid == 1) {
            $scope.spnextquestion();
        } else if (pillarid == 2) {
            $scope.ssjnextquestion();
        } else if (pillarid == 3) {
            $scope.fsnextquestion();
        } else if (pillarid == 4) {
            $scope.idsnextquestion();
        } else if (pillarid == 5) {
            $scope.healthnextquestion();
        }
    };

    $scope.okPT = function () {
        $scope.modalINFO.close();
    };
    $scope.goToHealth = function () {
        $scope.CallHealth();
        document.getElementById('lihealth').classList.add('active');
        document.getElementById('health').classList.add('active');
        document.getElementById('lissj').classList.remove('active');
        document.getElementById('ssj').classList.remove('active');
        document.getElementById('lisp').classList.remove('active');
        document.getElementById('sp').classList.remove('active');
        document.getElementById('lifs').classList.remove('active');
        document.getElementById('fs').classList.remove('active');
        document.getElementById('liids').classList.remove('active');
        document.getElementById('ids').classList.remove('active');
        document.getElementById('lialltasks').classList.remove('active');
        document.getElementById('alltasks').classList.remove('active');
    };
    $scope.goToSSJ = function () {
        $scope.CallSSJ();
        document.getElementById('lihealth').classList.remove('active');
        document.getElementById('health').classList.remove('active');
        document.getElementById('lissj').classList.add('active');
        document.getElementById('ssj').classList.add('active');
        document.getElementById('lisp').classList.remove('active');
        document.getElementById('sp').classList.remove('active');
        document.getElementById('lifs').classList.remove('active');
        document.getElementById('fs').classList.remove('active');
        document.getElementById('liids').classList.remove('active');
        document.getElementById('ids').classList.remove('active');
        document.getElementById('lialltasks').classList.remove('active');
        document.getElementById('alltasks').classList.remove('active');
    };
    $scope.goToSP = function () {
        $scope.CallSP();
        document.getElementById('lihealth').classList.remove('active');
        document.getElementById('health').classList.remove('active');
        document.getElementById('lissj').classList.remove('active');
        document.getElementById('ssj').classList.remove('active');
        document.getElementById('lisp').classList.add('active');
        document.getElementById('sp').classList.add('active');
        document.getElementById('lifs').classList.remove('active');
        document.getElementById('fs').classList.remove('active');
        document.getElementById('liids').classList.remove('active');
        document.getElementById('ids').classList.remove('active');
        document.getElementById('lialltasks').classList.remove('active');
        document.getElementById('alltasks').classList.remove('active');
    };
    $scope.goToFS = function () {
        $scope.CallFS();
        document.getElementById('lihealth').classList.remove('active');
        document.getElementById('health').classList.remove('active');
        document.getElementById('lissj').classList.remove('active');
        document.getElementById('ssj').classList.remove('active');
        document.getElementById('lisp').classList.remove('active');
        document.getElementById('sp').classList.remove('active');
        document.getElementById('lifs').classList.add('active');
        document.getElementById('fs').classList.add('active');
        document.getElementById('liids').classList.remove('active');
        document.getElementById('ids').classList.remove('active');
        document.getElementById('lialltasks').classList.remove('active');
        document.getElementById('alltasks').classList.remove('active');
    };
    $scope.goToIDS = function () {
        $scope.CallIDS();
        document.getElementById('lihealth').classList.remove('active');
        document.getElementById('health').classList.remove('active');
        document.getElementById('lissj').classList.remove('active');
        document.getElementById('ssj').classList.remove('active');
        document.getElementById('lisp').classList.remove('active');
        document.getElementById('sp').classList.remove('active');
        document.getElementById('lifs').classList.remove('active');
        document.getElementById('fs').classList.remove('active');
        document.getElementById('liids').classList.add('active');
        document.getElementById('ids').classList.add('active');
        document.getElementById('lialltasks').classList.remove('active');
        document.getElementById('alltasks').classList.remove('active');
    };
    $scope.goToAllTasks = function () {
        $scope.CallAllTasks();
        document.getElementById('lihealth').classList.remove('active');
        document.getElementById('health').classList.remove('active');
        document.getElementById('lissj').classList.remove('active');
        document.getElementById('ssj').classList.remove('active');
        document.getElementById('lisp').classList.remove('active');
        document.getElementById('sp').classList.remove('active');
        document.getElementById('lifs').classList.remove('active');
        document.getElementById('fs').classList.remove('active');
        document.getElementById('liids').classList.remove('active');
        document.getElementById('ids').classList.remove('active');
        document.getElementById('lialltasks').classList.add('active');
        document.getElementById('alltasks').classList.add('active');
    };
    $scope.goToPillarQuestion = function (pillarid, questionslno) {
        var skpcnt = +questionslno - 1;
        if (pillarid == 1) {
            $scope.spquestioncount = 0;
            $scope.spquestioncount = +$scope.spquestioncount + (+skpcnt);
            $scope.goToSP();
            $scope.spnextquestion();
            $scope.modalInstanceLoad.close();
        } else if (pillarid == 2) {
            $scope.ssjquestioncount = 0;
            $scope.ssjquestioncount = +$scope.ssjquestioncount + (+skpcnt);
            $scope.goToSSJ();
            $scope.ssjnextquestion();
            $scope.modalInstanceLoad.close();
        } else if (pillarid == 3) {
            $scope.fsquestioncount = 0;
            $scope.fsquestioncount = +$scope.fsquestioncount + (+skpcnt);
            $scope.goToFS();
            $scope.fsnextquestion();
            $scope.modalInstanceLoad.close();
        } else if (pillarid == 4) {
            $scope.idsquestioncount = 0;
            $scope.idsquestioncount = +$scope.idsquestioncount + (+skpcnt);
            $scope.goToIDS();
            $scope.idsnextquestion();
            $scope.modalInstanceLoad.close();
        } else if (pillarid == 5) {
            $scope.healthquestioncount = 0;
            $scope.healthquestioncount = +$scope.healthquestioncount + (+skpcnt);
            $scope.goToHealth();
            $scope.healthnextquestion();
            $scope.modalInstanceLoad.close();
        }
    };
    $scope.goToPillarQuestionPrev = function (pillarid, questionslno) {
        //console.log('goToPillarQuestionPrev', pillarid, questionslno);
        var skpcnt = +questionslno - 1;
        if (pillarid == 1) {
            $scope.spquestioncount = -1;
            $scope.spquestioncount = +$scope.spquestioncount + (+skpcnt);
            $scope.goToSP();
            $scope.spnextquestion();
            $scope.modalInstanceLoad.close();
        } else if (pillarid == 2) {
            $scope.ssjquestioncount = -1;
            $scope.ssjquestioncount = +$scope.ssjquestioncount + (+skpcnt);
            $scope.goToSSJ();
            $scope.ssjnextquestion();
            $scope.modalInstanceLoad.close();
        } else if (pillarid == 3) {
            $scope.fsquestioncount = -1;
            $scope.fsquestioncount = +$scope.fsquestioncount + (+skpcnt);
            $scope.goToFS();
            $scope.fsnextquestion();
            $scope.modalInstanceLoad.close();
        } else if (pillarid == 4) {
            $scope.idsquestioncount = -1;
            $scope.idsquestioncount = +$scope.idsquestioncount + (+skpcnt);
            $scope.goToIDS();
            $scope.idsnextquestion();
            $scope.modalInstanceLoad.close();
        } else if (pillarid == 5) {
            $scope.healthquestioncount = -1;
            $scope.healthquestioncount = +$scope.healthquestioncount + (+skpcnt);
            $scope.goToHealth();
            $scope.healthnextquestion();
            // console.log('goToHealth')
            $scope.modalInstanceLoad.close();
        }
    };
    $scope.Champions = Restangular.all('beneficiaries?filter[where][championapproved]=true&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().$object;
    $scope.ButtonClicked = function (data, pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, questionid, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, languagedata) {
        console.log('ButtonClicked', data)
        console.log('languagedata', languagedata);
        console.log('displayquestion', displayquestion);
        console.log('data', data);
        console.log('pillarid', pillarid);
        console.log('yesdocumentid', yesdocumentid);
        console.log('noincrement', noincrement);
        // console.log('yesdocumentflag', yesdocumentflag);
        //console.log('$window.sessionStorage.language', $window.sessionStorage.language);
        //console.log('$scope.DisplayBeneficiary.state', $scope.DisplayBeneficiary.state);
        $scope.hideUpdateCDIDS = true;
        $scope.hideUpdateTodo = true;
        $scope.hideUpdateRI = true;
        $scope.hideUpdateAW = true;
        $scope.OthersReport = false;
        var oneday = new Date();
        oneday.setDate(oneday.getDate() + 1);
        $scope.reportincident.dtmin = oneday;
        $scope.today();
        $scope.condom.dt = new Date();
        $scope.picker.dt = new Date();
        $scope.picker.selectdate = new Date();
        $scope.condom.dateasked = new Date();
        $scope.plhiv.month = new Date();
        $scope.plhiv.maxdate = new Date();
        $scope.cdids.availeddate = new Date();
        //$scope.cdids.maxdate = new Date();
        $scope.schememaster.datetime = new Date();
        $scope.increment.selectdate = new Date();
        $scope.reportincident.incidentdate = new Date();
        $scope.reportincident.dtmax = new Date();
        $scope.condom.dateasked = new Date();
        if (data == 'Yes') {
            if (yesdocumentflag == true) {
                //console.log('yesdocumentid2', yesdocumentid);
                Restangular.one('documenttypes', yesdocumentid).get().then(function (doc) {
                    //$scope.SchemeOrDocumentname = doc.name;
                    $scope.SchemeOrDocumentname = doc;
                    //console.log(' $scope.SchemeOrDocumentname1', $scope.SchemeOrDocumentname);
                    //sumanchanges
                    //console.log('Document Loan', doc);
                });
            } else {
                Restangular.one('schemes?filter[where][state]=' + $scope.DisplayBeneficiary.state + '&filter[where][topscheme]=' + yesdocumentid).get().then(function (schemes) {
                    if (schemes.length > 0) {
                        $scope.SchemeOrDocumentname = schemes[0];
                        var Array = [];
                        var Array2 = [];
                        Array = schemes[0].category;
                        Array2 = Array.split(',');
                        //console.log('Array2', Array2.length);
                        for (var i = 0; i < Array2.length; i++) {
                            //console.log('Array.length',Array.length);
                            //console.log('Array', Array2[i]);
                            if (Array2[i] == 15) {
                                $scope.loanapplied = true;
                                //console.log('Loan Applied');
                            } else {
                                $scope.loanapplied = false;
                                //console.log('Not Applied');
                            }
                        }
                    }
                });
            }
        } else {
            if (nodocumentflag == true) {
                Restangular.one('documenttypes', nodocumentid).get().then(function (doc) {
                    $scope.SchemeOrDocumentname = doc;
                    //console.log('$scope.SchemeOrDocumentname2', $scope.SchemeOrDocumentname);
                    //console.log('Document type  Loan', doc);
                });
            } else {
                Restangular.one('schemes?filter[where][state]=' + $scope.DisplayBeneficiary.state + '&filter[where][topscheme]=' + nodocumentid).get().then(function (schemes) {
                    if (schemes.length > 0) {
                        $scope.SchemeOrDocumentname = schemes[0];
                        //console.log('$scope.SchemeOrDocumentname', $scope.SchemeOrDocumentname);
                        //console.log('Scheme  Loan2', schemes[0]);
                    }
                });
            }
        }
        // console.log(pillarid);
        if (pillarid == 1) {
            $scope.DisplayClickOption = languagedata;
            if ($window.sessionStorage.language == 1) {
                $scope.PillarName = 'SP';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 2) {
                $scope.PillarName = 'एस पी';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 3) {
                $scope.PillarName = 'ಆರೋಗ್ಯ';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 4) {
                $scope.PillarName = 'ஆரோக்கியம்';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 5) {
                $scope.PillarName = 'ఆరోగ్యం'
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 6) {
                $scope.PillarName = 'एस पी'
                $scope.QuestionName = displayquestion;
            }
        } else if (pillarid == 2) {
            $scope.DisplayClickOption = languagedata;
            console.log('languagedata', languagedata);
            if ($window.sessionStorage.language == 1) {
                $scope.PillarName = 'SSJ';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 2) {
                $scope.PillarName = 'एस एस जे';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 3) {
                $scope.PillarName = 'ಆರೋಗ್ಯ';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 4) {
                $scope.PillarName = 'ஆரோக்கியம்';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 5) {
                $scope.PillarName = 'ఆరోగ్యం'
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 6) {
                $scope.PillarName = 'एस एस जे';
                $scope.QuestionName = displayquestion;
            }
        } else if (pillarid == 3) {
            $scope.DisplayClickOption = languagedata;
            if ($window.sessionStorage.language == 1) {
                $scope.PillarName = 'FS';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 2) {
                $scope.PillarName = 'एफ एस';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 3) {
                $scope.PillarName = 'ಆರೋಗ್ಯ';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 4) {
                $scope.PillarName = 'ஆரோக்கியம்';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 5) {
                $scope.PillarName = 'ఆరోగ్యం'
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 6) {
                $scope.PillarName = 'एफ एस'
                $scope.QuestionName = displayquestion;
            }
        } else if (pillarid == 4) {
            $scope.DisplayClickOption = languagedata;
            if ($window.sessionStorage.language == 1) {
                $scope.PillarName = 'IDS';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 2) {
                $scope.PillarName = 'आईडीएस';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 3) {
                $scope.PillarName = 'ಆರೋಗ್ಯ';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 4) {
                $scope.PillarName = 'ஆரோக்கியம்';
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 5) {
                $scope.PillarName = 'ఆరోగ్యం'
                $scope.QuestionName = displayquestion;
            } else if ($window.sessionStorage.language == 6) {
                $scope.PillarName = 'आईडीएस';
                $scope.QuestionName = displayquestion;
            }
        } else if (pillarid == 5) {
            $scope.DisplayClickOption = languagedata;
            if ($window.sessionStorage.language == 1) {
                $scope.PillarName = 'HEALTH';
                $scope.QuestionName = displayquestion;
                //$scope.DisplayClickOption = data;
            } else if ($window.sessionStorage.language == 2) {
                $scope.PillarName = 'स्वास्थ्य';
                $scope.QuestionName = displayquestion;
                if (data == 'Yes') {
                    //$scope.DisplayClickOption = 'हाँ';
                } else {
                    //$scope.DisplayClickOption = 'नहीं';
                }
            } else if ($window.sessionStorage.language == 3) {
                $scope.PillarName = 'ಆರೋಗ್ಯ';
                $scope.QuestionName = displayquestion;
                if (data == 'Yes') {
                    //$scope.DisplayClickOption = 'ಹೌದು';
                } else {
                    //$scope.DisplayClickOption = 'ಇಲ್ಲ';
                }
            } else if ($window.sessionStorage.language == 4) {
                $scope.PillarName = 'ஆரோக்கியம்';
                $scope.QuestionName = displayquestion;
                if (data == 'Yes') {
                    //$scope.DisplayClickOption = 'ஆம்';
                } else {
                    //$scope.DisplayClickOption = 'இல்லை';
                }
            } else if ($window.sessionStorage.language == 5) {
                $scope.PillarName = 'ఆరోగ్యం'
                $scope.QuestionName = displayquestion;
                if (data == 'Yes') {
                    //$scope.DisplayClickOption = 'అవును';
                } else {
                    //$scope.DisplayClickOption = 'లేదు';
                }
            } else if ($window.sessionStorage.language == 6) {
                $scope.PillarName = 'आरोग्य'
                $scope.QuestionName = displayquestion;
                console.log('$scope.QuestionName', $scope.QuestionName);
                if (data == 'Yes') {
                    //$scope.DisplayClickOption = 'होय'
                } else {
                    //$scope.DisplayClickOption = 'नाही'
                }
            }
        };
        if (data == 'Yes') {
            $scope.todotyes = Restangular.all('todotypes?filter[where][id]=' + yestodotype).getList().then(function (todotyes) {
                $scope.todotypes = todotyes;
                //console.log('$scope.todotypes', $scope.todotypes);
                $scope.todo.todotype = yestodotype;
            });
            if (yesaction == 'nextquestion') {
                if (pillarid == 1) {
                    $scope.spnextquestion();
                } else if (pillarid == 2) {
                    $scope.ssjnextquestion();
                } else if (pillarid == 3) {
                    $scope.fsnextquestion();
                } else if (pillarid == 4) {
                    $scope.idsnextquestion();
                } else if (pillarid == 5) {
                    $scope.healthnextquestion();
                }
                $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'yes', new Date(), $window.sessionStorage.UserEmployeeId, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
            } else if (yesaction == 'nextmodule') {
                if (pillarid == 1) {
                    $scope.goToFS();
                } else if (pillarid == 2) {
                    $scope.goToSP();
                } else if (pillarid == 3) {
                    $scope.goToIDS();
                } else if (pillarid == 4) {
                    $scope.goToHealth();
                } else if (pillarid == 5) {
                    $scope.goToSSJ();
                }
                $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'yes', new Date(), $window.sessionStorage.UserEmployeeId, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
            } else if (yesaction == 'popup') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupPillarid = pillarid;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'yes';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupYesDocumentflag = yesdocumentflag;
                $scope.PopupNoDocumentFlag = nodocumentflag;
                $scope.PopupYesDocumentid = yesdocumentid;
                $scope.PopupNoDocumentid = nodocumentid;
                if (yespopup == 'todos') {
                    $scope.openToDo();
                } else if (yespopup == 'datepicker') {
                    $scope.openDP();
                } else if (yespopup == 'plhiv') {
                    $scope.openPLHIV();
                } else if (yespopup == 'reportincident') {
                    var sevendays = new Date();
                    sevendays.setDate(sevendays.getDate() + 7);
                    $scope.reportincident = {
                        // "referredby":false,
                        "dtmin": new Date(),
                        "dtmax": new Date(),
                        "followupdate": sevendays,
                        "incidentdate": new Date(),
                        "currentstatus": 'Needs Follow Up',
                        "physical": false,
                        "husband": false,
                        "emotional": false,
                        "sexual": false,
                        "propertyrelated": false,
                        "childrelated": false, //"other": false,
                        "police": false,
                        "goons": false,
                        "clients": false,
                        "partners": false,
                        "serviceprovider": false,
                        "familymember": false,
                        "otherkp": false,
                        "perpetratorother": false,
                        "neighbour": false,
                        "authorities": false,
                        "co": false,
                        "timetorespond": false,
                        "severity": "Moderate",
                        "resolved": false,
                        "reported": false,
                        "reportedpolice": false,
                        "reportedngos": false,
                        "reportedfriends": false,
                        "reportedplv": false,
                        "reportedcoteam": false,
                        "reportedchampions": false,
                        "reportedotherkp": false,
                        "reportedlegalaid": false,
                        "referredcouncelling": false,
                        "referredmedicalcare": false,
                        "referredcomanager": false,
                        "referredplv": false,
                        "referredlegalaid": false,
                        "referredboardmember": false,
                        "noactionreqmember": false,
                        "noactiontaken": false,
                        "multiplemembers": ['1', '2'],
                        "stateid": $window.sessionStorage.zoneId,
                        "state": $window.sessionStorage.zoneId,
                        "districtid": $window.sessionStorage.salesAreaId,
                        "district": $window.sessionStorage.salesAreaId,
                        "coid": $window.sessionStorage.coorgId,
                        "facility": $window.sessionStorage.coorgId,
                        "beneficiaryid": null,
                        "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
                        "lastmodifiedtime": new Date(),
                        "lastmodifiedbyrole": $window.sessionStorage.roleId
                    };
                    $scope.openRI('lg');
                } else if (yespopup == 'reportincidentothers') {
                    var sevendays = new Date();
                    sevendays.setDate(sevendays.getDate() + 7);
                    $scope.reportincident = {
                        // "referredby":false,
                        "dtmin": new Date(),
                        "dtmax": new Date(),
                        "followupdate": sevendays,
                        "incidentdate": new Date(),
                        "currentstatus": 'Needs Follow Up',
                        "physical": false,
                        "emotional": false,
                        "sexual": false,
                        "propertyrelated": false,
                        "childrelated": false, //"other": false,
                        "police": false,
                        "goons": false,
                        "clients": false,
                        "partners": false,
                        "husband": false,
                        "serviceprovider": false,
                        "familymember": false,
                        "otherkp": false,
                        "perpetratorother": false,
                        "neighbour": false,
                        "authorities": false,
                        "co": false,
                        "timetorespond": false,
                        "severity": "Moderate",
                        "resolved": false,
                        "reported": false,
                        "reportedpolice": false,
                        "reportedngos": false,
                        "reportedfriends": false,
                        "reportedplv": false,
                        "reportedcoteam": false,
                        "reportedchampions": false,
                        "reportedotherkp": false,
                        "reportedlegalaid": false,
                        "referredcouncelling": false,
                        "referredmedicalcare": false,
                        "referredcomanager": false,
                        "referredplv": false,
                        "referredlegalaid": false,
                        "referredboardmember": false,
                        "noactionreqmember": false,
                        "noactiontaken": false,
                        "multiplemembers": ['1', '2'],
                        "stateid": $window.sessionStorage.zoneId,
                        "state": $window.sessionStorage.zoneId,
                        "districtid": $window.sessionStorage.salesAreaId,
                        "district": $window.sessionStorage.salesAreaId,
                        "coid": $window.sessionStorage.coorgId,
                        "facility": $window.sessionStorage.coorgId,
                        "beneficiaryid": null,
                        "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
                        "lastmodifiedtime": new Date(),
                        "lastmodifiedbyrole": $window.sessionStorage.roleId
                    };
                    $scope.openRIOthers('lg');
                } else if (yespopup == 'champions') {
                    $scope.openCL();
                } else if (yespopup == 'dateforid') {
                    $scope.openCDIDS();
                } else if (yespopup == 'applicationflow') {
                    $scope.datetimehide = false;
                    $scope.reponserec = true;
                    $scope.delaydis = true;
                    $scope.collectedrequired = true;
                    $scope.rejectdis = true;
                    $scope.schememaster = {
                        datetime: new Date()
                    }
                    $scope.openAW('lg');
                } else if (yespopup == 'applicationflowboth') {
                    $scope.datetimehide = false;
                    $scope.reponserec = true;
                    $scope.delaydis = true;
                    $scope.collectedrequired = true;
                    $scope.rejectdis = true;
                    $scope.schememaster = {
                        datetime: new Date()
                    }
                    $scope.openAWBoth('lg');
                } else if (yespopup == 'condomdetails') {
                    $scope.openCD();
                } else if (yespopup == 'financialliteracy') {
                    $scope.openFL();
                } else if (yespopup == 'financialplanning') {
                    $scope.openFP();
                } else if (yespopup == 'phonetype') {
                    //$scope.phonetypes = Restangular.all('phonetypes').getList().$object;
                    $scope.phonetypes = [];
                    Restangular.all('phonetypes').getList().then(function (ptRes) {
                        for (var i = 0; i < ptRes.length; i++) {
                            if (ptRes[i].id != 3) {
                                $scope.phonetypes.push(ptRes[i]);
                            } else {
                                return;
                            }
                        }
                    });

                    Restangular.one('beneficiaries', $routeParams.id).get().then(function (phonetyp) {
                        $scope.phonetyp = phonetyp.phonetype;
                        if ($scope.phonetyp == 1) {
                            $scope.phonetype.phonetp = 1;
                        } else if ($scope.phonetyp == 2) {
                            $scope.phonetype.phonetp = 2;
                        } else if ($scope.phonetyp == 3) {
                            $scope.phonetype.phonetp = 3;
                        }
                    });
                    $scope.openPT();
                }
                //$scope.SaveAnswers($routeParams.id, pillarid, questionid, 'yes', new Date(), $window.sessionStorage.UserEmployeeId);
            } else if (yesaction == 'skp') {
                var skpcnt = +yesskipto - +serialno - 1;
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
                if (pillarid == 1) {
                    $scope.spquestioncount = +$scope.spquestioncount + (+skpcnt);
                    $scope.spnextquestion();
                    $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'yes', new Date(), $window.sessionStorage.UserEmployeeId, (+yesskipto) - 1, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                } else if (pillarid == 2) {
                    $scope.ssjquestioncount = +$scope.ssjquestioncount + (+skpcnt);
                    $scope.ssjnextquestion();
                    $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'yes', new Date(), $window.sessionStorage.UserEmployeeId, (+yesskipto) - 1, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                } else if (pillarid == 3) {
                    $scope.fsquestioncount = +$scope.fsquestioncount + (+skpcnt);
                    $scope.fsnextquestion();
                    $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'yes', new Date(), $window.sessionStorage.UserEmployeeId, (+yesskipto) - 1, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                } else if (pillarid == 4) {
                    $scope.idsquestioncount = +$scope.idsquestioncount + (+skpcnt);
                    $scope.idsnextquestion();
                    $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'yes', new Date(), $window.sessionStorage.UserEmployeeId, (+yesskipto) - 1, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                } else if (pillarid == 5) {
                    $scope.healthquestioncount = +$scope.healthquestioncount + (+skpcnt);
                    $scope.healthnextquestion();
                    $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'yes', new Date(), $window.sessionStorage.UserEmployeeId, (+yesskipto) - 1, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                }
            } else if (yesaction == 'increment') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupPillarid = pillarid;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'yes';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.openIDP();
            } else if (yesaction == 'incrementskip') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupPillarid = pillarid;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'yes';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.IncrementBy = 1;
                $scope.openIDP();
            } else if (yesaction == 'incrementonly') {
                $scope.beneficiaryincrementonly = {};
                if ($routeParams.id != undefined) {
                    Restangular.one('beneficiaries', $routeParams.id).get().then(function (kp) {
                        if (yesincrement == 'savings') {
                            if (kp.noofsavingaccounts == null) {
                                kp.noofsavingaccounts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofsavingaccounts = +kp.noofsavingaccounts + 1;
                        } else if (yesincrement == 'investmentproducts') {
                            if (kp.noofinvestmentproducts == null) {
                                kp.noofinvestmentproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinvestmentproducts = +kp.noofinvestmentproducts + 1;
                        } else if (yesincrement == 'savingsources') {
                            if (kp.noofinformalsavingsources == null) {
                                kp.noofinformalsavingsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinformalsavingsources = +kp.noofinformalsavingsources + 1;
                        } else if (yesincrement == 'insuranceproducts') {
                            if (kp.noofinsuranceproducts == null) {
                                kp.noofinsuranceproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinsuranceproducts = +kp.noofinsuranceproducts + 1;
                        } else if (yesincrement == 'pensionproducts') {
                            if (kp.noofpensionproducts == null) {
                                kp.noofpensionproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofpensionproducts = +kp.noofpensionproducts + 1;
                        } else if (yesincrement == 'informalcreditsources') {
                            if (kp.noofinformalcreditsources == null) {
                                kp.noofinformalcreditsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinformalcreditsources = +kp.noofinformalcreditsources + 1;
                        } else if (yesincrement == 'formalcreditsources') {
                            if (kp.noofformalcreditsources == null) {
                                kp.noofformalcreditsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofformalcreditsources = +kp.noofformalcreditsources + 1;
                        } else if (yesincrement == 'reciept') {
                            $scope.beneficiaryincrementonly.paidmember = 'yes';
                        } else if (yesincrement == 'dynamicmember') {
                            $scope.beneficiaryincrementonly.dynamicmember = true;
                        }
                        //console.log('$scope.beneficiaryincrement', $scope.beneficiaryincrement);
                        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryincrementonly).then(function (respo) {
                            $scope.beneficiaryincrement = {};
                            $scope.increment = {};
                            $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'yes', new Date(), $window.sessionStorage.UserEmployeeId, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
                        }, function (error) {
                            console.log('error', error);
                        });
                        if (pillarid == 1) {
                            $scope.spnextquestion();
                        } else if (pillarid == 2) {
                            $scope.ssjnextquestion();
                        } else if (pillarid == 3) {
                            $scope.fsnextquestion();
                        } else if (pillarid == 4) {
                            $scope.idsnextquestion();
                        } else if (pillarid == 5) {
                            $scope.healthnextquestion();
                        }
                    });
                }
            } else if (yesaction == 'incrementonlyskip') {
                $scope.beneficiaryincrementonly = {};
                if ($routeParams.id != undefined) {
                    Restangular.one('beneficiaries', $routeParams.id).get().then(function (kp) {
                        if (yesincrement == 'savings') {
                            if (kp.noofsavingaccounts == null) {
                                kp.noofsavingaccounts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofsavingaccounts = +kp.noofsavingaccounts + 1;
                        } else if (yesincrement == 'investmentproducts') {
                            if (kp.noofinvestmentproducts == null) {
                                kp.noofinvestmentproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinvestmentproducts = +kp.noofinvestmentproducts + 1;
                        } else if (yesincrement == 'savingsources') {
                            if (kp.noofinformalsavingsources == null) {
                                kp.noofinformalsavingsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinformalsavingsources = +kp.noofinformalsavingsources + 1;
                        } else if (yesincrement == 'insuranceproducts') {
                            if (kp.noofinsuranceproducts == null) {
                                kp.noofinsuranceproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinsuranceproducts = +kp.noofinsuranceproducts + 1;
                        } else if (yesincrement == 'pensionproducts') {
                            if (kp.noofpensionproducts == null) {
                                kp.noofpensionproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofpensionproducts = +kp.noofpensionproducts + 1;
                        } else if (yesincrement == 'informalcreditsources') {
                            if (kp.noofinformalcreditsources == null) {
                                kp.noofinformalcreditsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinformalcreditsources = +kp.noofinformalcreditsources + 1;
                        } else if (yesincrement == 'formalcreditsources') {
                            if (kp.noofformalcreditsources == null) {
                                kp.noofformalcreditsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofformalcreditsources = +kp.noofformalcreditsources + 1;
                        } else if (yesincrement == 'reciept') {
                            $scope.beneficiaryincrementonly.paidmember = 'yes';
                        } else if (yesincrement == 'dynamicmember') {
                            $scope.beneficiaryincrementonly.dynamicmember = true;
                        }
                        console.log('$scope.beneficiaryincrement', $scope.beneficiaryincrement);
                        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryincrementonly).then(function (respo) {
                            $scope.beneficiaryincrement = {};
                            $scope.increment = {};
                            $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'yes', new Date(), $window.sessionStorage.UserEmployeeId, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
                        }, function (error) {
                            console.log('error', error);
                        });
                        if (pillarid == 1) {
                            $scope.spquestioncount++;
                            $scope.spnextquestion();
                        } else if (pillarid == 2) {
                            $scope.ssjquestioncount++;
                            $scope.ssjnextquestion();
                        } else if (pillarid == 3) {
                            $scope.fsquestioncount++;
                            $scope.fsnextquestion();
                        } else if (pillarid == 4) {
                            $scope.idsquestioncount++;
                            $scope.idsnextquestion();
                        } else if (pillarid == 5) {
                            $scope.healthquestioncount++;
                            $scope.healthnextquestion();
                        }
                    });
                }
            } else if (yesaction == 'providenextquestion') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupPillarid = pillarid;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'yes';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupInfo = yesprovideinfo;
                $scope.openINFO();
            } else if (yesaction == 'freeflow') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupPillarid = pillarid;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'yes';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupInfo = yesprovideinfo;
                $scope.openPhoneName();
            }
            //console.log('Button Clicked', data + '::' + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',questionid::' + questionid);
        }
        if (data == 'No') {
            $scope.todotyes = Restangular.all('todotypes?filter[where][id]=' + notodotype).getList().then(function (todotyes) {
                $scope.todotypes = todotyes;
                $scope.todo.todotype = notodotype;
            });
            if (yespopup == 'plhiv') {
                $scope.beneficiaryplhivno = {
                    plhiv: false
                };
                Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryplhivno).then(function (respo) {
                    $scope.DisplayBeneficiary = respo;
                });
            } else if (yespopup == 'phonetype') {
                // $scope.beneficiaryincrementonly.phonetype = 3;
                $scope.beneficiaryphonetype = {
                    phonetype: 3
                };
                Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryphonetype).then(function (respoph) {
                    $scope.DisplayBeneficiary = respoph;
                    //console.log('respoph',respoph);
                });
            }
            if (noaction == 'nextquestion') {
                if (pillarid == 1) {
                    $scope.spnextquestion();
                } else if (pillarid == 2) {
                    $scope.ssjnextquestion();
                } else if (pillarid == 3) {
                    $scope.fsnextquestion();
                } else if (pillarid == 4) {
                    $scope.idsnextquestion();
                } else if (pillarid == 5) {
                    $scope.healthnextquestion();
                }
                $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
            } else if (noaction == 'nextmodule') {
                if (pillarid == 1) {
                    $scope.goToFS();
                } else if (pillarid == 2) {
                    $scope.goToSP();
                } else if (pillarid == 3) {
                    $scope.goToIDS();
                } else if (pillarid == 4) {
                    $scope.goToHealth();
                } else if (pillarid == 5) {
                    $scope.goToSSJ();
                }
                $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
            } else if (noaction == 'popup') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupPillarid = pillarid;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'no';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupYesDocumentflag = yesdocumentflag;
                $scope.PopupNoDocumentFlag = nodocumentflag;
                $scope.PopupYesDocumentid = yesdocumentid;
                $scope.PopupNoDocumentid = nodocumentid;
                if (nopopup == 'todos') {
                    $scope.openToDo();
                } else if (nopopup == 'datepicker') {
                    $scope.openDP();
                } else if (nopopup == 'plhiv') {
                    $scope.openPLHIV();
                } else if (nopopup == 'reportincident') {
                    var sevendays = new Date();
                    sevendays.setDate(sevendays.getDate() + 7);
                    $scope.reportincident = {
                        // "referredby":false,
                        "dtmin": new Date(),
                        "dtmax": new Date(),
                        "followupdate": sevendays,
                        "incidentdate": new Date(),
                        "currentstatus": 'Needs Follow Up',
                        "physical": false,
                        "emotional": false,
                        "sexual": false,
                        "propertyrelated": false,
                        "childrelated": false, //"other": false,
                        "police": false,
                        "goons": false,
                        "clients": false,
                        "partners": false,
                        "husband": false,
                        "serviceprovider": false,
                        "familymember": false,
                        "otherkp": false,
                        "perpetratorother": false,
                        "neighbour": false,
                        "authorities": false,
                        "co": false,
                        "timetorespond": false,
                        "severity": "Moderate",
                        "resolved": false,
                        "reported": false,
                        "reportedpolice": false,
                        "reportedngos": false,
                        "reportedfriends": false,
                        "reportedplv": false,
                        "reportedcoteam": false,
                        "reportedchampions": false,
                        "reportedotherkp": false,
                        "reportedlegalaid": false,
                        "referredcouncelling": false,
                        "referredmedicalcare": false,
                        "referredcomanager": false,
                        "referredplv": false,
                        "referredlegalaid": false,
                        "referredboardmember": false,
                        "noactionreqmember": false,
                        "noactiontaken": false,
                        "multiplemembers": ['1', '2'],
                        "stateid": $window.sessionStorage.zoneId,
                        "state": $window.sessionStorage.zoneId,
                        "districtid": $window.sessionStorage.salesAreaId,
                        "district": $window.sessionStorage.salesAreaId,
                        "coid": $window.sessionStorage.coorgId,
                        "facility": $window.sessionStorage.coorgId,
                        "beneficiaryid": null,
                        "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
                        "lastmodifiedtime": new Date(),
                        "lastmodifiedbyrole": $window.sessionStorage.roleId
                    };
                    $scope.openRI('lg');
                } else if (nopopup == 'reportincidentothers') {
                    var sevendays = new Date();
                    sevendays.setDate(sevendays.getDate() + 7);
                    $scope.reportincident = {
                        // "referredby":false,
                        "dtmin": new Date(),
                        "dtmax": new Date(),
                        "followupdate": sevendays,
                        "incidentdate": new Date(),
                        "currentstatus": 'Needs Follow Up',
                        "physical": false,
                        "emotional": false,
                        "sexual": false,
                        "propertyrelated": false,
                        "childrelated": false, //"other": false,
                        "police": false,
                        "goons": false,
                        "clients": false,
                        "partners": false,
                        "husband": false,
                        "serviceprovider": false,
                        "familymember": false,
                        "otherkp": false,
                        "perpetratorother": false,
                        "neighbour": false,
                        "authorities": false,
                        "co": false,
                        "timetorespond": false,
                        "severity": "Moderate",
                        "resolved": false,
                        "reported": false,
                        "reportedpolice": false,
                        "reportedngos": false,
                        "reportedfriends": false,
                        "reportedplv": false,
                        "reportedcoteam": false,
                        "reportedchampions": false,
                        "reportedotherkp": false,
                        "reportedlegalaid": false,
                        "referredcouncelling": false,
                        "referredmedicalcare": false,
                        "referredcomanager": false,
                        "referredplv": false,
                        "referredlegalaid": false,
                        "referredboardmember": false,
                        "noactionreqmember": false,
                        "noactiontaken": false,
                        "multiplemembers": ['1', '2'],
                        "stateid": $window.sessionStorage.zoneId,
                        "state": $window.sessionStorage.zoneId,
                        "districtid": $window.sessionStorage.salesAreaId,
                        "district": $window.sessionStorage.salesAreaId,
                        "coid": $window.sessionStorage.coorgId,
                        "facility": $window.sessionStorage.coorgId,
                        "beneficiaryid": null,
                        "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
                        "lastmodifiedtime": new Date(),
                        "lastmodifiedbyrole": $window.sessionStorage.roleId
                    };
                    $scope.openRIOthers('lg');
                } else if (nopopup == 'champions') {
                    $scope.openCL();
                } else if (nopopup == 'dateforid') {
                    $scope.openCDIDS();
                } else if (nopopup == 'applicationflow') {
                    $scope.datetimehide = false;
                    $scope.reponserec = true;
                    $scope.delaydis = true;
                    $scope.collectedrequired = true;
                    $scope.rejectdis = true;
                    $scope.schememaster = {
                        datetime: new Date()
                    }
                    $scope.openAW('lg');
                } else if (nopopup == 'applicationflowboth') {
                    $scope.datetimehide = false;
                    $scope.reponserec = true;
                    $scope.delaydis = true;
                    $scope.collectedrequired = true;
                    $scope.rejectdis = true;
                    $scope.schememaster = {
                        datetime: new Date()
                    }
                    $scope.openAWBoth('lg');
                } else if (nopopup == 'condomdetails') {
                    $scope.openCD();
                } else if (nopopup == 'financialliteracy') {
                    $scope.openFL();
                } else if (nopopup == 'financialplanning') {
                    $scope.openFP();
                }
                /* else if (yespopup == 'phonetype') {
                 	Restangular.one('beneficiaries',$routeParams.id).get().then(function(phonetyp){
                 		console.log('phonetyp',phonetyp);
                 	});
                     $scope.openPT();
                 }*/
                // $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId);
            } else if (noaction == 'skp') {
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
                var skpcnt = +noskipto - +serialno - 1;
                if (pillarid == 1) {
                    $scope.spquestioncount = +$scope.spquestioncount + (+skpcnt);
                    $scope.spnextquestion();
                    $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, (+noskipto) - 1, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                } else if (pillarid == 2) {
                    $scope.ssjquestioncount = +$scope.ssjquestioncount + (+skpcnt);
                    $scope.ssjnextquestion();
                    $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, (+noskipto) - 1, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                } else if (pillarid == 3) {
                    $scope.fsquestioncount = +$scope.fsquestioncount + (+skpcnt);
                    $scope.fsnextquestion();
                    $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, (+noskipto) - 1, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                } else if (pillarid == 4) {
                    $scope.idsquestioncount = +$scope.idsquestioncount + (+skpcnt);
                    $scope.idsnextquestion();
                    $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, (+noskipto) - 1, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                } else if (pillarid == 5) {
                    $scope.healthquestioncount = +$scope.healthquestioncount + (+skpcnt);
                    $scope.healthnextquestion();
                    $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, (+noskipto) - 1, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                }
            }

            /*else if (noaction == 'nophone') {
                $scope.beneficiary_phoneType = {
                    phonetype: 3
                };
                Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiary_phoneType).then(function (respo) {
                    $scope.beneficiaryincrement = {};
                    //$scope.increment = {};
                    $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                }, function (error) {
                    console.log('error', error);
                });

            } */
            else if (noaction == 'increment') {
                /*if (pillarid == 1) {
                	    $scope.spnextquestion();
                	} else if (pillarid == 2) {
                	    $scope.ssjnextquestion();
                	} else if (pillarid == 3) {
                	    $scope.fsnextquestion();
                	} else if (pillarid == 4) {
                	    $scope.idsnextquestion();
                	} else if (pillarid == 5) {
                	    $scope.healthnextquestion();
                	}
                	$scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, serialno);*/
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupPillarid = pillarid;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'no';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.openIDP();
            } else if (noaction == 'incrementskip') {
                /*if (pillarid == 1) {
                	    $scope.spnextquestion();
                	} else if (pillarid == 2) {
                	    $scope.ssjnextquestion();
                	} else if (pillarid == 3) {
                	    $scope.fsnextquestion();
                	} else if (pillarid == 4) {
                	    $scope.idsnextquestion();
                	} else if (pillarid == 5) {
                	    $scope.healthnextquestion();
                	}
                	$scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, serialno);*/
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupPillarid = pillarid;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'no';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.IncrementBy = 1;
                $scope.openIDP();
            } else if (noaction == 'incrementonly') {
                $scope.beneficiaryincrementonly = {};
                if ($routeParams.id != undefined) {
                    Restangular.one('beneficiaries', $routeParams.id).get().then(function (kp) {
                        if (noincrement == 'savings') {
                            if (kp.noofsavingaccounts == null) {
                                kp.noofsavingaccounts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofsavingaccounts = +kp.noofsavingaccounts + 1;
                        } else if (noincrement == 'investmentproducts') {
                            if (kp.noofinvestmentproducts == null) {
                                kp.noofinvestmentproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinvestmentproducts = +kp.noofinvestmentproducts + 1;
                        } else if (noincrement == 'savingsources') {
                            if (kp.noofinformalsavingsources == null) {
                                kp.noofinformalsavingsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinformalsavingsources = +kp.noofinformalsavingsources + 1;
                        } else if (noincrement == 'insuranceproducts') {
                            if (kp.noofinsuranceproducts == null) {
                                kp.noofinsuranceproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinsuranceproducts = +kp.noofinsuranceproducts + 1;
                        } else if (noincrement == 'pensionproducts') {
                            if (kp.noofpensionproducts == null) {
                                kp.noofpensionproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofpensionproducts = +kp.noofpensionproducts + 1;
                        } else if (noincrement == 'informalcreditsources') {
                            if (kp.noofinformalcreditsources == null) {
                                kp.noofinformalcreditsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinformalcreditsources = +kp.noofinformalcreditsources + 1;
                        } else if (noincrement == 'formalcreditsources') {
                            if (kp.noofformalcreditsources == null) {
                                kp.noofformalcreditsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofformalcreditsources = +kp.noofformalcreditsources + 1;
                        } else if (noincrement == 'reciept') {
                            $scope.beneficiaryincrementonly.paidmember = 'yes';
                        } else if (noincrement == 'dynamicmember') {
                            $scope.beneficiaryincrementonly.dynamicmember = true;
                        }
                        //console.log('$scope.beneficiaryincrement', $scope.beneficiaryincrement);
                        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryincrementonly).then(function (respo) {
                            //console.log('beneficiaryincrementonly',respo);
                            $scope.beneficiaryincrement = {};
                            $scope.increment = {};
                            $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
                        }, function (error) {
                            console.log('error', error);
                        });
                        if (pillarid == 1) {
                            $scope.spnextquestion();
                        } else if (pillarid == 2) {
                            $scope.ssjnextquestion();
                        } else if (pillarid == 3) {
                            $scope.fsnextquestion();
                        } else if (pillarid == 4) {
                            $scope.idsnextquestion();
                        } else if (pillarid == 5) {
                            $scope.healthnextquestion();
                        }
                    });
                }
            } else if (noaction == 'incrementonlyskip') {
                $scope.beneficiaryincrementonly = {};
                if ($routeParams.id != undefined) {
                    Restangular.one('beneficiaries', $routeParams.id).get().then(function (kp) {
                        if (noincrement == 'savings') {
                            if (kp.noofsavingaccounts == null) {
                                kp.noofsavingaccounts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofsavingaccounts = +kp.noofsavingaccounts + 1;
                        } else if (noincrement == 'investmentproducts') {
                            if (kp.noofinvestmentproducts == null) {
                                kp.noofinvestmentproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinvestmentproducts = +kp.noofinvestmentproducts + 1;
                        } else if (noincrement == 'savingsources') {
                            if (kp.noofinformalsavingsources == null) {
                                kp.noofinformalsavingsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinformalsavingsources = +kp.noofinformalsavingsources + 1;
                        } else if (noincrement == 'insuranceproducts') {
                            if (kp.noofinsuranceproducts == null) {
                                kp.noofinsuranceproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinsuranceproducts = +kp.noofinsuranceproducts + 1;
                        } else if (noincrement == 'pensionproducts') {
                            if (kp.noofpensionproducts == null) {
                                kp.noofpensionproducts = 0;
                            }
                            $scope.beneficiaryincrementonly.noofpensionproducts = +kp.noofpensionproducts + 1;
                        } else if (noincrement == 'informalcreditsources') {
                            if (kp.noofinformalcreditsources == null) {
                                kp.noofinformalcreditsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofinformalcreditsources = +kp.noofinformalcreditsources + 1;
                        } else if (noincrement == 'formalcreditsources') {
                            if (kp.noofformalcreditsources == null) {
                                kp.noofformalcreditsources = 0;
                            }
                            $scope.beneficiaryincrementonly.noofformalcreditsources = +kp.noofformalcreditsources + 1;
                        } else if (noincrement == 'reciept') {
                            $scope.beneficiaryincrementonly.paidmember = 'yes';
                        } else if (noincrement == 'dynamicmember') {
                            $scope.beneficiaryincrementonly.dynamicmember = true;
                        }
                        console.log('$scope.beneficiaryincrement', $scope.beneficiaryincrement);
                        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryincrementonly).then(function (respo) {
                            $scope.beneficiaryincrement = {};
                            $scope.increment = {};
                            $scope.SaveAnswers($routeParams.id, pillarid, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
        $scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;
                        }, function (error) {
                            console.log('error', error);
                        });
                        if (pillarid == 1) {
                            $scope.spquestioncount++;
                            $scope.spnextquestion();
                        } else if (pillarid == 2) {
                            $scope.ssjquestioncount++;
                            $scope.ssjnextquestion();
                        } else if (pillarid == 3) {
                            $scope.fsquestioncount++;
                            $scope.fsnextquestion();
                        } else if (pillarid == 4) {
                            $scope.idsquestioncount++;
                            $scope.idsnextquestion();
                        } else if (pillarid == 5) {
                            $scope.healthquestioncount++;
                            $scope.healthnextquestion();
                        }
                    });
                }
            } else if (noaction == 'providenextquestion') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupPillarid = pillarid;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'no';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupInfo = noprovideinfo;
                $scope.openINFO();
            } else if (noaction == 'freeflow') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupPillarid = pillarid;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'no';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupInfo = yesprovideinfo;
                $scope.openPhoneName();
            }
            // console.log('Button Clicked', data + '::' + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',questionid::' + questionid);
        }
    };
    ////////////////////////////////////////////////////////SP Start////////////////////////////////////////////////
    ////////////////////////////////////////////////////////SP End////////////////////////////////////////////////
    ////////////////////////////////////////////////////////SSJ Start////////////////////////////////////////////////
    ////////////////////////////////////////////////////////SSJ End////////////////////////////////////////////////
    ////////////////////////////////////////////////////////FS Start////////////////////////////////////////////////
    ////////////////////////////////////////////////////////FS End////////////////////////////////////////////////
    ////////////////////////////////////////////////////////IDS Start////////////////////////////////////////////////
    ///////////////////////////////////IDS End////////////////////////////////////////////////
    ///////////////////////////////Health Start////////////////////////////////////////////////
    ///////////////////////////////////Health End////////////////////////////////////////////////
    $scope.ChangeClient = function (id, answered, Elementid, radioid, pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
        $scope.route = id;
        $rootScope.surveyquestions = Restangular.one('surveyquestions/' + id).get().then(function (surveyquestions) {
            $rootScope.routeid = surveyquestions.surveytypeid;
            $scope.surveyquestion = surveyquestions;
            //console.log('surveyquestions.questiontype', surveyquestions.questiontype);
            //console.log('$scope.DisplayBeneficiary.state', $scope.DisplayBeneficiary.state);
            //console.log('schemeslno', schemeslno);
            if (surveyquestions.questiontype == 'scheme') {
                $scope.schemename = Restangular.one('schemes?filter[where][state]=' + $scope.DisplayBeneficiary.state + '&filter[where][topscheme]=' + schemeslno).get().then(function (schemes) {
                    /* var Array = [];
                     var Array2 = [];
                     Array = schemes[0].category;
                     Array2 = Array.split(',');
                     //console.log('Array2', Array2.length);
                     for (var i = 0; i < Array2.length; i++) {
                         //console.log('Array.length',Array.length);
                         //console.log('Array', Array2[i]);
                         if (Array2[i] == 15) {
                             $scope.loanapplied = true;
                             //console.log('Loan Applied');
                         }
                         else {
                             $scope.loanapplied = false;
                             //  console.log('Not Applied');
                         }
                     }*/
                    Restangular.all('schememasters?filter[where][schemeId]=' + schemes[0].id + '&filter[where][memberId]=' + $scope.DisplayBeneficiary.id).getList().then(function (schememstrs) {
                        if (schememstrs.length == 0) {
                            if ($window.sessionStorage.language == 1) {
                                $scope.mySchemeName = schemes[0].name;
                            } else if ($window.sessionStorage.language == 2) {
                                $scope.mySchemeName = schemes[0].hnname;
                            } else if ($window.sessionStorage.language == 3) {
                                $scope.mySchemeName = schemes[0].knname;
                            } else if ($window.sessionStorage.language == 4) {
                                $scope.mySchemeName = schemes[0].taname;
                            } else if ($window.sessionStorage.language == 5) {
                                $scope.mySchemeName = schemes[0].tename;
                            } else if ($window.sessionStorage.language == 6) {
                                $scope.mySchemeName = schemes[0].mrname;
                            }
                            if (schemes.length > 0) {
                                if ($scope.spsurveyquestion.schemenameadded == false) {
                                    console.log("surveyname", $scope.spsurveyquestion.schemenameadded);
                                    $scope.tempsursurveyquestion = $scope.spsurveyquestion.question + ' ' + $scope.mySchemeName + ' ?';
                                    var tempschemeques = $scope.tempsursurveyquestion.split('?');
                                    $scope.spsurveyquestion.question = tempschemeques[0] + tempschemeques[1] + '?';
                                    $scope.spsurveyquestion.schemenameadded = true;
                                }
                                $scope.CreateForm(surveyquestions.id, surveyquestions.noofoptions, surveyquestions.answeroptions, surveyquestions.questiontype, answered, Elementid, radioid, pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, null, null, schemes[0].id, schemes[0].id, surveyquestions.teansweroptions, surveyquestions.hnansweroptions, surveyquestions.kaansweroptions, surveyquestions.taansweroptions, surveyquestions.maansweroptions);
                            } else {
                                $scope.spnextquestion();
                            }
                        } else {
                            $scope.spquestioncount++;
                            $scope.spnextquestion();
                        }
                    });
                });
            } else {
                $scope.CreateForm(surveyquestions.id, surveyquestions.noofoptions, surveyquestions.answeroptions, surveyquestions.questiontype, answered, Elementid, radioid, pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, surveyquestions.teansweroptions, surveyquestions.hnansweroptions, surveyquestions.kaansweroptions, surveyquestions.taansweroptions, surveyquestions.maansweroptions);
            }
        });
    }
    $scope.CreateForm = function (id, noofdropdowns, answers, questiontype, answered, ElementId, radioid, pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, teansweroptions, hnansweroptions, kaansweroptions, taansweroptions, maansweroptions) {
        var mydiv = document.getElementById(ElementId);
        var myForm = document.createElement("form");
        while (mydiv.firstChild) {
            mydiv.removeChild(mydiv.firstChild);
        }
        myForm.method = "post";
        if (questiontype == 't') {
            //camera_status
            /* var label = document.createElement("label");
            	 label.for = "text";
            	 label.innerHTML = '<br><div style="color: #333; display:block; width:250px;word-wrap: break-word;">Enter Text: </div>';
            	 myForm.appendChild(label);*/
            var myTextElement;
            try {
                myTextElement = document.createElement('<input type="text" name="camera_status" />On');
            } catch (err) {
                myTextElement = document.createElement('input');
            }
            myTextElement.type = "text";
            myTextElement.name = "camera_status";
            myTextElement.label = "Text";
            myForm.appendChild(myTextElement);
            mydiv.appendChild(myForm);
        }
        if (questiontype == 'td') {
            //camera_status
            /*var label = document.createElement("label");
            	label.for = "text";
            	label.innerHTML = '<br><div style="color: #333; display:block; width:250px;word-wrap: break-word;">' + $scope.selectanswer + '</div>';
            	myForm.appendChild(label);*/
            var array = answers.split(',');
            for (var i = 0; i < noofdropdowns; i++) {
                var myTextDisplayElement;
                try {
                    myTextDisplayElement = document.createElement('div');
                    myTextDisplayElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <p style='color: white;'>" + '-' + array[i] + "</p></div";
                } catch (err) {
                    myTextDisplayElement = document.createElement('div');
                    myTextDisplayElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <p style='color: white;'>" + '-' + array[i] + "</p></div";
                }
                // myRadioElement.type = "radio";
                // myRadioElement.name = "camera_status" +''+ i;
                // myRadioElement.value = "camera_status" +''+ i;
                myForm.appendChild(myTextDisplayElement);
            };
            mydiv.appendChild(myForm);
        }
        /*if (questiontype == 'r') {
        	    //camera_status
        	    var array = answers.split(',');
        	    var displayarray = [];
        	    //console.log('$window.sessionStorage.language',$window.sessionStorage.language);
        	    //console.log('id',id);
        	    if ($window.sessionStorage.language == 1) {
        	        displayarray = answers.split(',');

        	    } else if ($window.sessionStorage.language == 2) {
        	        if (hnansweroptions == null) {
        	            displayarray = answers.split(',');
        	        } else {
        	            displayarray = hnansweroptions.split(',');
        	        }

        	    } else if ($window.sessionStorage.language == 3) {
        	        if (kaansweroptions == null) {
        	            displayarray = answers.split(',');
        	        } else {
        	            displayarray = kaansweroptions.split(',');
        	        }

        	    } else if ($window.sessionStorage.language == 4) {
        	        if (taansweroptions == null) {
        	            displayarray = answers.split(',');
        	        } else {
        	            displayarray = taansweroptions.split(',');
        	        }

        	    } else if ($window.sessionStorage.language == 5) {
        	        if (teansweroptions == null) {
        	            displayarray = answers.split(',');
        	        } else {
        	            displayarray = teansweroptions.split(',');
        	        }

        	    } else if ($window.sessionStorage.language == 6) {
        	        if (maansweroptions == null) {
        	            displayarray = answers.split(',');
        	        } else {
        	            displayarray = maansweroptions.split(',');
        	        }

        	    }
        	    for (var i = 0; i < noofdropdowns; i++) {
        	        var myRadioElement;
        	        try {
        	            myRadioElement = document.createElement('<input type="radio" name="camera_status" />On');
        	        } catch (err) {
        	            if (answered == array[i]) {
        	                myRadioElement = document.createElement('div');
        	                //myRadioElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input  style='color: white;' type='radio' checked='true' name='myInputs[]' value='" + array[i] + "' id=radio" + radioid + i + ">" + '  ' + array[i] + "</div";
        	                var radioYes = document.createElement("input");
        	                radioYes.setAttribute("type", "radio");
        	                radioYes.setAttribute("id", radioid + i);
        	                radioYes.setAttribute("name", "myInputs[]");
        	                radioYes.setAttribute("value", array[i]);
        	                radioYes.setAttribute("checked", "true");
        	                if (array[i].toLowerCase() == 'yes') {
        	                    radioYes.addEventListener("click", function (event) {
        	                        $scope.ButtonClicked('Yes', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[0]);
        	                        //event.preventDefault();
        	                    });
        	                }
        	                if (array[i].toLowerCase() == 'no') {
        	                    radioYes.addEventListener("click", function (event) {
        	                        $scope.ButtonClicked('No', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[1]);
        	                        //event.preventDefault();
        	                    });
        	                }
        	                var lblYes = document.createElement("label");
        	                var textYes = document.createTextNode(displayarray[i]);
        	                lblYes.appendChild(textYes);
        	                myRadioElement.appendChild(radioYes);
        	                myRadioElement.appendChild(lblYes);
        	            } else {
        	                myRadioElement = document.createElement('div');
        	                var radioYes = document.createElement("input");
        	                radioYes.setAttribute("type", "radio");
        	                radioYes.setAttribute("id", radioid + i);
        	                radioYes.setAttribute("name", "myInputs[]");
        	                radioYes.setAttribute("value", array[i]);
        	                if (array[i].toLowerCase() == 'yes') {
        	                    // console.log('Button Clicked Yes',   + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',serialno::' + serialno);
        	                    radioYes.addEventListener("click", function (event) {
        	                        $scope.ButtonClicked('Yes', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[0]);
        	                        event.preventDefault();
        	                    });
        	                }
        	                if (array[i].toLowerCase() == 'no') {
        	                    //console.log('Button Clicked No',   + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',serialno::' + serialno);
        	                    radioYes.addEventListener("click", function (event) {
        	                        $scope.ButtonClicked('No', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[1]);
        	                        event.preventDefault();
        	                    });
        	                }
        	                var lblYes = document.createElement("label");
        	                var textYes = document.createTextNode(displayarray[i]);
        	                lblYes.appendChild(textYes);
        	                myRadioElement.appendChild(radioYes);
        	                myRadioElement.appendChild(lblYes);
        	                // myRadioElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input  style='color: white;' type='radio' name='myInputs[]' value='" + array[i] + "' id=radio" + radioid + i + " onclick='ButtonClicked()'>" + '  ' + array[i] + "</div";

        	            }
        	        }

        	        // myRadioElement.type = "radio";
        	        // myRadioElement.name = "camera_status" +''+ i;
        	        // myRadioElement.value = "camera_status" +''+ i;

        	        myForm.appendChild(myRadioElement);

        	    };

        	    mydiv.appendChild(myForm);
        	    
        	}*/
        if (questiontype == 'r') {
            //camera_status
            var array = answers.split(',');
            var displayarray = [];
            //console.log('$window.sessionStorage.language',$window.sessionStorage.language);
            //console.log('id',id);
            if ($window.sessionStorage.language == 1) {
                displayarray = answers.split(',');
            } else if ($window.sessionStorage.language == 2) {
                if (hnansweroptions == null) {
                    displayarray = answers.split(',');
                } else {
                    displayarray = hnansweroptions.split(',');
                }
            } else if ($window.sessionStorage.language == 3) {
                if (kaansweroptions == null) {
                    displayarray = answers.split(',');
                } else {
                    displayarray = kaansweroptions.split(',');
                }
            } else if ($window.sessionStorage.language == 4) {
                if (taansweroptions == null) {
                    displayarray = answers.split(',');
                } else {
                    displayarray = taansweroptions.split(',');
                }
            } else if ($window.sessionStorage.language == 5) {
                if (teansweroptions == null) {
                    displayarray = answers.split(',');
                } else {
                    displayarray = teansweroptions.split(',');
                }
            } else if ($window.sessionStorage.language == 6) {
                if (maansweroptions == null) {
                    displayarray = answers.split(',');
                } else {
                    displayarray = maansweroptions.split(',');
                }
            }
            for (var i = 0; i < noofdropdowns; i++) {
                var myRadioElement;
                try {
                    myRadioElement = document.createElement('<input type="radio" name="camera_status" />On');
                } catch (err) {
                    if (answered == array[i]) {
                        myRadioElement = document.createElement('div');
                        //myRadioElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input  style='color: white;' type='radio' checked='true' name='myInputs[]' value='" + array[i] + "' id=radio" + radioid + i + ">" + '  ' + array[i] + "</div";
                        var radioYes = document.createElement("input");
                        radioYes.setAttribute("type", "button");
                        radioYes.setAttribute("id", radioid + i);
                        radioYes.setAttribute("name", "myInputs[]");
                        radioYes.setAttribute("value", array[i]);
                        radioYes.setAttribute("checked", "true");
                        if (array[i].toLowerCase() == 'yes') {
                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('Yes', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[0]);
                                //event.preventDefault();
                            });
                        }
                        if (array[i].toLowerCase() == 'no') {
                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('No', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[1]);
                                //event.preventDefault();
                            });
                        }
                        var lblYes = document.createElement("label");
                        var textYes = document.createTextNode(displayarray[i]);
                        lblYes.appendChild(textYes);
                        mydiv.appendChild(radioYes);
                        mydiv.appendChild(lblYes);
                    } else {
                        myRadioElement = document.createElement('div');
                        var radioYes = document.createElement("input");
                        radioYes.setAttribute("type", "button");
                        radioYes.setAttribute("id", radioid + i);
                        radioYes.setAttribute("name", "myInputs[]");
                        radioYes.setAttribute("value", displayarray[i]);
                        radioYes.setAttribute("class", "btn btn-success");
                        radioYes.style.width = "60px";
                        radioYes.style.height = "25px";
                        radioYes.style["font-size"] = "12px";
                        radioYes.style["align"] = "center";
                        radioYes.style["line-height"] = "0.7em";
                        if (array[i].toLowerCase() == 'yes') {
                            // console.log('Button Clicked Yes',   + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',serialno::' + serialno);
                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('Yes', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[0]);
                                event.preventDefault();
                            });
                        }
                        if (array[i].toLowerCase() == 'no') {
                            radioYes.style.marginLeft = "5px";
                            radioYes.setAttribute("class", "btn btn-red");
                            //console.log('Button Clicked No',   + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',serialno::' + serialno);
                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('No', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[1]);
                                event.preventDefault();
                            });
                        }
                        var lblYes = document.createElement("label");
                        var textYes = document.createTextNode(displayarray[i]);
                        lblYes.appendChild(textYes);
                        mydiv.appendChild(radioYes);
                        //mydiv.appendChild(lblYes);
                        // myRadioElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input  style='color: white;' type='radio' name='myInputs[]' value='" + array[i] + "' id=radio" + radioid + i + " onclick='ButtonClicked()'>" + '  ' + array[i] + "</div";
                    }
                }
                // myRadioElement.type = "radio";
                // myRadioElement.name = "camera_status" +''+ i;
                // myRadioElement.value = "camera_status" +''+ i;
                // myForm.appendChild(myRadioElement);
            };
            //mydiv.appendChild(myForm);
        }
        /* if (questiontype == 'scheme') {
        	     //camera_status
        	     
        	     var array = answers.split(',');
        	     var displayarray = [];
        	     //console.log('$window.sessionStorage.language',$window.sessionStorage.language);
        	     //console.log('id',id);
        	     if ($window.sessionStorage.language == 1) {
        	         displayarray = answers.split(',');

        	     } else if ($window.sessionStorage.language == 2) {
        	         if (hnansweroptions == null) {
        	             displayarray = answers.split(',');
        	         } else {
        	             displayarray = hnansweroptions.split(',');
        	         }

        	     } else if ($window.sessionStorage.language == 3) {
        	         if (kaansweroptions == null) {
        	             displayarray = answers.split(',');
        	         } else {
        	             displayarray = kaansweroptions.split(',');
        	         }

        	     } else if ($window.sessionStorage.language == 4) {
        	         if (taansweroptions == null) {
        	             displayarray = answers.split(',');
        	         } else {
        	             displayarray = taansweroptions.split(',');
        	         }

        	     } else if ($window.sessionStorage.language == 5) {
        	         if (teansweroptions == null) {
        	             displayarray = answers.split(',');
        	         } else {
        	             displayarray = teansweroptions.split(',');
        	         }

        	     } else if ($window.sessionStorage.language == 6) {
        	         if (maansweroptions == null) {
        	             displayarray = answers.split(',');
        	         } else {
        	             displayarray = maansweroptions.split(',');
        	         }

        	     }
        	     for (var i = 0; i < noofdropdowns; i++) {
        	         var myRadioElement;
        	         try {
        	             myRadioElement = document.createElement('<input type="radio" name="camera_status" />On');
        	         } catch (err) {
        	             if (answered == array[i]) {
        	                 myRadioElement = document.createElement('div');
        	                 //myRadioElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input  style='color: white;' type='radio' checked='true' name='myInputs[]' value='" + array[i] + "' id=radio" + radioid + i + ">" + '  ' + array[i] + "</div";
        	                 var radioYes = document.createElement("input");
        	                 radioYes.setAttribute("type", "radio");
        	                 radioYes.setAttribute("id", radioid + i);
        	                 radioYes.setAttribute("name", "myInputs[]");
        	                 radioYes.setAttribute("value", array[i]);
        	                 radioYes.setAttribute("checked", "true");
        	                 if (array[i].toLowerCase() == 'yes') {
        	                     radioYes.addEventListener("click", function (event) {
        	                         $scope.ButtonClicked('Yes', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[0]);
        	                         //event.preventDefault();
        	                     });
        	                 }
        	                 if (array[i].toLowerCase() == 'no') {
        	                     radioYes.addEventListener("click", function (event) {
        	                         $scope.ButtonClicked('No', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[1]);
        	                         //event.preventDefault();
        	                     });
        	                 }
        	                 var lblYes = document.createElement("label");
        	                 var textYes = document.createTextNode(array[i]);
        	                 lblYes.appendChild(textYes);
        	                 myRadioElement.appendChild(radioYes);
        	                 myRadioElement.appendChild(lblYes);
        	             } else {
        	                 myRadioElement = document.createElement('div');
        	                 var radioYes = document.createElement("input");
        	                 radioYes.setAttribute("type", "radio");
        	                 radioYes.setAttribute("id", radioid + i);
        	                 radioYes.setAttribute("name", "myInputs[]");
        	                 radioYes.setAttribute("value", array[i]);
        	                 if (array[i].toLowerCase() == 'yes') {
        	                     // console.log('Button Clicked Yes',   + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',serialno::' + serialno);
        	                     radioYes.addEventListener("click", function (event) {
        	                         $scope.ButtonClicked('Yes', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[0]);
        	                         event.preventDefault();
        	                     });
        	                 }
        	                 if (array[i].toLowerCase() == 'no') {
        	                     //console.log('Button Clicked No',   + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',serialno::' + serialno);
        	                     radioYes.addEventListener("click", function (event) {
        	                         $scope.ButtonClicked('No', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[1]);
        	                         event.preventDefault();
        	                     });
        	                 }
        	                 var lblYes = document.createElement("label");
        	                 var textYes = document.createTextNode(array[i]);
        	                 lblYes.appendChild(textYes);
        	                 myRadioElement.appendChild(radioYes);
        	                 myRadioElement.appendChild(lblYes);
        	                 // myRadioElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input  style='color: white;' type='radio' name='myInputs[]' value='" + array[i] + "' id=radio" + radioid + i + " onclick='ButtonClicked()'>" + '  ' + array[i] + "</div";

        	             }
        	         }

        	         // myRadioElement.type = "radio";
        	         // myRadioElement.name = "camera_status" +''+ i;
        	         // myRadioElement.value = "camera_status" +''+ i;

        	         myForm.appendChild(myRadioElement);

        	     };
        	     mydiv.appendChild(myForm);
        	 }*/
        if (questiontype == 'scheme') {
            //camera_status
            var array = answers.split(',');
            var displayarray = [];
            //console.log('$window.sessionStorage.language',$window.sessionStorage.language);
            //console.log('id',id);
            if ($window.sessionStorage.language == 1) {
                displayarray = answers.split(',');
            } else if ($window.sessionStorage.language == 2) {
                if (hnansweroptions == null) {
                    displayarray = answers.split(',');
                } else {
                    displayarray = hnansweroptions.split(',');
                }
            } else if ($window.sessionStorage.language == 3) {
                if (kaansweroptions == null) {
                    displayarray = answers.split(',');
                } else {
                    displayarray = kaansweroptions.split(',');
                }
            } else if ($window.sessionStorage.language == 4) {
                if (taansweroptions == null) {
                    displayarray = answers.split(',');
                } else {
                    displayarray = taansweroptions.split(',');
                }
            } else if ($window.sessionStorage.language == 5) {
                if (teansweroptions == null) {
                    displayarray = answers.split(',');
                } else {
                    displayarray = teansweroptions.split(',');
                }
            } else if ($window.sessionStorage.language == 6) {
                if (maansweroptions == null) {
                    displayarray = answers.split(',');
                } else {
                    displayarray = maansweroptions.split(',');
                }
            }
            for (var i = 0; i < noofdropdowns; i++) {
                var myRadioElement;
                try {
                    myRadioElement = document.createElement('<input type="radio" name="camera_status" />On');
                } catch (err) {
                    if (answered == array[i]) {
                        myRadioElement = document.createElement('div');
                        //myRadioElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input  style='color: white;' type='radio' checked='true' name='myInputs[]' value='" + array[i] + "' id=radio" + radioid + i + ">" + '  ' + array[i] + "</div";
                        var radioYes = document.createElement("input");
                        radioYes.setAttribute("type", "radio");
                        radioYes.setAttribute("id", radioid + i);
                        radioYes.setAttribute("name", "myInputs[]");
                        radioYes.setAttribute("value", array[i]);
                        radioYes.setAttribute("checked", "true");
                        if (array[i].toLowerCase() == 'yes') {
                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('Yes', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[0]);
                                //event.preventDefault();
                            });
                        }
                        if (array[i].toLowerCase() == 'no') {
                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('No', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[1]);
                                //event.preventDefault();
                            });
                        }
                        var lblYes = document.createElement("label");
                        var textYes = document.createTextNode(array[i]);
                        lblYes.appendChild(textYes);
                        myRadioElement.appendChild(radioYes);
                        myRadioElement.appendChild(lblYes);
                    } else {
                        var radioYes = document.createElement("input");
                        radioYes.setAttribute("type", "button");
                        radioYes.setAttribute("id", radioid + i);
                        radioYes.setAttribute("name", "myInputs[]");
                        radioYes.setAttribute("value", displayarray[i]);
                        radioYes.setAttribute("class", "btn btn-success");
                        radioYes.style.width = "60px";
                        radioYes.style.height = "25px";
                        radioYes.style["font-size"] = "12px";
                        radioYes.style["align"] = "center";
                        radioYes.style["line-height"] = "0.7em";
                        if (array[i].toLowerCase() == 'yes') {
                            // console.log('Button Clicked Yes',   + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',serialno::' + serialno);
                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('Yes', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[0]);
                                event.preventDefault();
                            });
                        }
                        if (array[i].toLowerCase() == 'no') {
                            radioYes.style.marginLeft = "5px";
                            radioYes.setAttribute("class", "btn btn-red");
                            //console.log('Button Clicked No',   + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',serialno::' + serialno);
                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('No', pillarid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[1]);
                                event.preventDefault();
                            });
                        }
                        var lblYes = document.createElement("label");
                        var textYes = document.createTextNode(array[i]);
                        lblYes.appendChild(textYes);
                        mydiv.appendChild(radioYes);
                        // mydiv.appendChild(lblYes);
                        // myRadioElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input  style='color: white;' type='radio' name='myInputs[]' value='" + array[i] + "' id=radio" + radioid + i + " onclick='ButtonClicked()'>" + '  ' + array[i] + "</div";
                    }
                }
                // myRadioElement.type = "radio";
                // myRadioElement.name = "camera_status" +''+ i;
                // myRadioElement.value = "camera_status" +''+ i;
                //myForm.appendChild(myRadioElement);
            };
            // mydiv.appendChild(myForm);
        }
        if (questiontype == 'c') {
            //camera_status
            /* var label = document.createElement("label");
            	 label.for = "text";
            	 label.innerHTML = '<br><div style="color: #333; display:block; width:250px;word-wrap: break-word;">' + $scope.selectanswer + '</div>';
            	 myForm.appendChild(label);*/
            var array = answers.split(',');
            var answeredarray = answered.split(',');
            for (var i = 0; i < noofdropdowns; i++) {
                var myCheckElement;
                try {
                    if (answeredarray.indexOf(array[i]) == -1) {
                        myCheckElement = document.createElement('div');
                        myCheckElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='checkbox' name='myInputs[] value='" + i + "'>" + '  ' + array[i] + "</div";
                    } else {
                        myCheckElement = document.createElement('div');
                        myCheckElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='checkbox' name='myInputs[] value='" + i + "' checked>" + '  ' + array[i] + "</div";
                    }
                } catch (err) {
                    if (answeredarray.indexOf(array[i]) == -1) {
                        myCheckElement = document.createElement('div');
                        myCheckElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='checkbox' name='myInputs[] value='" + i + "'>" + '  ' + array[i] + "</div";
                    } else {
                        myCheckElement = document.createElement('div');
                        myCheckElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='checkbox' name='myInputs[] value='" + i + "' checked>" + '  ' + array[i] + "</div";
                    }
                    //myCheckElement = document.createElement('input');
                }
                /// myCheckElement.type = "checkbox";
                // myCheckElement.name = "camera_status" + i;
                // myCheckElement.value = "camera_status" + i;
                myForm.appendChild(myCheckElement);
            }
            mydiv.appendChild(myForm);
        }
    };
    //$scope.goToPillarQuestion(2, 2);
    //Datepicker settings start
    $scope.today = function () {
        $scope.dt = new Date();
        //$scope.todo.datetime = $filter('date')(new Date(), 'y-MM-dd');
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        $scope.reportincident.followupdate = sevendays;
        $scope.todo.datetime = sevendays;
    };
    // var nextmonth = new Date();
    // nextmonth.setMonth(sevendays.getMonth + 1);
    // $scope.cdids.maxdate = nextmonth;
    var oneday = new Date();
    oneday.setDate(oneday.getDate() + 1);
    $scope.reportincident.dtmin = oneday;
    $scope.today();
    $scope.condom.dt = new Date();
    $scope.picker.dt = new Date();
    $scope.picker.selectdate = new Date();
    $scope.condom.dateasked = new Date();
    $scope.plhiv.month = new Date();
    $scope.plhiv.maxdate = new Date();
    $scope.cdids.availeddate = new Date();
    $scope.cdidsmaxdate = new Date();
    $scope.schememaster.datetime = new Date();
    $scope.increment.selectdate = new Date();
    $scope.reportincident.incidentdate = new Date();
    $scope.reportincident.dtmax = new Date();
    $scope.condom.dateasked = new Date();
    $scope.todomin = new Date();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        //$scope.dt = null;
    };
    // Disable weekend selection
    $scope.disabled = function (date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.todoopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.todo.opened = true;
    };
    $scope.fsopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.fs.opened = true;
    };
    $scope.healthopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.health.opened = true;
    };
    $scope.spopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.sp.opened = true;
    };
    $scope.reportopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.reportincident.opened = true;
    };
    $scope.followopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.follow.opened = true;
    };
    $scope.pickeropen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.picker.opened = true;
    };
    $scope.cdidsopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.cdids.opened = true;
    };
    $scope.workflowopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.schememaster.opened = true;
    };
    $scope.plhivopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.plhiv.opened = true;
    };
    $scope.condomopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.condom.opened = true;
    };
    $scope.incrementopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.increment.opened = true;
    };
    $scope.folowupdateopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.schememaster.opened = true;
    };
    $scope.reportincidentopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.reportincident.opened = true;
    };
    $scope.reportincidentfollowupopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.reportincident.followupdatepick = true;
    };
    $scope.reportincidentcloseropen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.reportincidentcloser = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };
    $scope.monthOptions = {
        formatYear: 'yyyy',
        startingDay: 1,
        minMode: 'month'
    };
    $scope.mode = 'month';
    $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.monthformat = $scope.monthformats[0];
    //Datepicker settings end
    /******************************************* Suman's Changes ApplicationWorkflow *************/
    if ($window.sessionStorage.fullName != undefined) {
        $scope.mem = Restangular.one('beneficiaries', $window.sessionStorage.fullName).get().then(function (member) {
            $scope.memberfullname = member;
            $scope.benfid = $window.sessionStorage.fullName;
        });
    }
    $scope.member = true;
    $scope.memberone = true;
    $scope.$watch('reportincident.co', function (newValue, oldValue) {
        //console.log('reportincident.co', newValue);
        if (newValue === oldValue) {
            return;
        } else if ($scope.reportincident.co === false) {
            $scope.member = true;
            $scope.memberone = true;
        } else if ($scope.reportincident.co === true) {
            $scope.member = false;
            $scope.memberone = false;
            if ($scope.OthersReport == false) {
                $scope.reportincident.multiplemembers.push($routeParams.id);
                console.log('$scope.reportincident.multiplemembers', $scope.reportincident.multiplemembers);
            }
            /*else {
            		$scope.reportincident.multiplemembers.push($routeParams.id);
            		console.log('$scope.reportincident.multiplemembers2',$scope.reportincident.multiplemembers);
            	}*/
        } else {
            $scope.member = true;
            $scope.memberone = true;
        }
    });
    $scope.followupdatehide = false;
    $scope.followuphide = false;
    $scope.dateofclosure = true;
    $scope.$watch('reportincident.currentstatus', function (newValue, oldValue) {
        if ($scope.reportincident.currentstatus == null) {
            $scope.reportincident.currentstatus = 'Needs Follow Up';
        } else {
            if (newValue === oldValue) {
                return;
            } else if ($scope.reportincident.currentstatus === 'Needs Follow Up') {
                $scope.dateofclosure = true;
                $scope.followupdatehide = false;
                $scope.followuphide = false;
            } else if ($scope.reportincident.currentstatus === 'Resolved' || $scope.reportincident.currentstatus === 'Closed') {
                $scope.followuphide = true;
                $scope.dateofclosure = true;
                $scope.dateofclosure = false;
                $scope.followupdatehide = true;
                $scope.reportincident.dateofclosure = new Date();
            } else {
                $scope.dateofclosure = false;
            }
        }
    });
    $scope.reportincident.reported = false;
    $scope.report = true;
    $scope.$watch('reportincident.reported', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else if ($scope.reportincident.reported === true) {
            $scope.report = false;
        } else {
            $scope.report = true;
            $scope.timetorespond = true;
            $scope.reportincident.reportedcoteam = false;
            $scope.reportincident.timetorespond = false;
        }
    });
    $scope.reportincident.reportedcoteam = false;
    $scope.timetorespond = true;
    $scope.$watch('reportincident.reportedcoteam', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else if ($scope.reportincident.reportedcoteam === true) {
            $scope.timetorespond = false;
        } else {
            $scope.timetorespond = true;
            $scope.reportincident.timetorespond = false;
        }
    });

    $scope.$watch('reportincident.noactiontaken', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else if ($scope.reportincident.noactiontaken == true) {
            $scope.reportincident.currentstatus = 'Closed';
        } else {
            $scope.reportincident.currentstatus = 'Needs Follow Up';
        }
    });
    $scope.noofmonthrepay_Hide = true;
    $scope.$watch('schememaster.purposeofloan', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        }
        /* else if ($scope.current_LoanPurpose != 5) {
            // $scope.current_LoanPurpose = newValue;
             $scope.noofmonthrepay_Hide = true;
             //$scope.schememaster.amount = '';
             $scope.currMonthyear = '';
         }*/
        else {
            $scope.currMonthyear = '';
            $scope.current_LoanPurpose = newValue;
            $scope.schememaster.amount = '';
        }
    });
    $scope.$watch('schememaster.amount', function (newValue, oldValue) {
        //console.log('$scope.current_LoanPurpose', $scope.current_LoanPurpose);
        //console.log('$scope.amount', newValue);
        if (newValue === oldValue) {
            return;
        } else if ($scope.current_LoanPurpose == 5 && newValue >= 10000) {
            $scope.noofmonthrepay_Hide = false;
        } else {
            $scope.noofmonthrepay_Hide = true;
        }
    });
    $scope.studcheck = [];
    $scope.$watch('schememaster.noofmonthrepay', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            $scope.currMonthyear = [];
            for (var m = 1; m <= newValue; m++) {
                $scope.monthyear = new Date();
                $scope.monthyear.setDate($scope.monthyear.getDate() + 30 * m)
                $scope.monthyear = $filter('date')($scope.monthyear, 'MMMM-yyyy');
                //console.log('MonthYear',$scope.monthyear);
                $scope.currMonthyear.push($scope.monthyear);
                //console.log('$scope.currMonthyear', $scope.currMonthyear);
                //console.log('$scope.studcheck',$scope.studcheck.length)
                //sumanchanges
            }
            //console.log('$scope.currMonthyear.monthtopay', $scope.monthtopay);
            for (var k = 0; k < $scope.monthtopay; k++) {
                $scope.studcheck.push(true);
            }
        };
    });
    $scope.loanapplied = false;
    /* $scope.$watch('schememaster.schemeId', function (newValue, oldValue) {
         if (newValue == oldValue || newValue == '') {
             return;
         } else {
             //$scope.schememaster.stage = '';
             $scope.loanapplied = false;
             $scope.purposeofloanhide = true;
             Restangular.one('schemes', newValue).get().then(function (SchemeMaster) {
                 //  console.log('SchemeMaster', SchemeMaster);
                 var Array = [];
                 var Array2 = [];
                 Array = SchemeMaster.category;
                 Array2 = Array.split(',');
                 //console.log('Array2', Array2.length);
                 for (var i = 0; i < Array2.length; i++) {
                     //console.log('Array.length',Array.length);
                     //console.log('Array', Array2[i]);
                     if (Array2[i] == 15) {
                         $scope.loanapplied = true;
                         //console.log('Loan Applied');
                     } else {
                         $scope.loanapplied = false;
                         //  console.log('Not Applied');
                     }
                 }
             });
         }
     });*/
    $scope.mygenderstatus = false;
    $scope.myoccupationstatus = false;
    $scope.mysocialstatus = false;
    $scope.myagestatus = false;
    $scope.myhealthstatus = false;
    $scope.loanapplied = false;
    /* $scope.$watch('schememaster.schemeId', function (newValue, oldValue) {
         if (newValue == oldValue || newValue == undefined) {
             return;
         } else {
             $scope.schememaster.stage = '';
             $scope.toggleLoading();
             var Array = [];
             var Array2 = [];
             Restangular.one('beneficiaries', $scope.schememaster.memberId).get().then(function (brfcry) {
                 $scope.newoccupation = brfcry.typology.toUpperCase();
                 //console.log('$scope.newoccupation', $scope.newoccupation);
                 if ($scope.newoccupation === 'MIGRANT' || $scope.newoccupation === 'IDU' || $scope.newoccupation === 'TRUCKERS' || $scope.newoccupation === 'GENERAL') {
                     $scope.modalInstanceLoad.close();
                     $scope.myoccupationstatus = true;
                     return;
                 } else {
                     Restangular.one('occupationstatuses/findOne?filter[where][name]=' + $scope.newoccupation).get().then(function (occstatus) {
                         Restangular.one('schemes', newValue).get().then(function (schme) {
                             //console.log('occstatus', occstatus.id);
                             //console.log('schme', schme.healthstatus);
                             //console.log('brfcry', brfcry.gender);
                             Restangular.one('maritalstatuses', brfcry.maritalstatus).get().then(function (marstatus) {
                                 //console.log('marstatus', marstatus);
                                 Restangular.one('socialstatuses', schme.socialstatus).get().then(function (socstatus) {
                                     //console.log('socstatus', socstatus);
                                     //console.log('brfcry.maritalstatus', brfcry.maritalstatus);
                                     //console.log('socstatus.id', schme.socialstatus);
                                     if (socstatus.id == 1 || socstatus.id == 3 || marstatus.id == 6) {
                                         $scope.mysocialstatus = true;
                                     } else if (marstatus.id == 3 && socstatus.id == 2) {
                                         $scope.mysocialstatus = true;
                                     } else if (marstatus.name == socstatus.name) {
                                         $scope.mysocialstatus = true;
                                     } else {
                                         $scope.mysocialstatus = false;
                                     }
                                     $scope.modalInstanceLoad.close();
                                 });
                             });


                             if (occstatus.id == schme.occupationstatus) {
                                 $scope.myoccupationstatus = true;
                             }
                             if (brfcry.gender == schme.gender) {
                                 $scope.mygenderstatus = true;
                             }
                             if (schme.healthstatus == 1 || schme.healthstatus == 2) {
                                 $scope.myhealthstatus = true;

                             } else if (schme.healthstatus == 3) {
                                 console.log('schme.healthstatus', schme.healthstatus);
                                 if (brfcry.plhiv == true) {
                                     $scope.myhealthstatus = true;
                                 } else if (brfcry.plhiv == false) {
                                     $scope.myhealthstatus = false;
                                 }
                             } else if (schme.healthstatus == 5) {
                                 console.log('schme.healthstatus', schme.healthstatus);
                                 if (brfcry.physicallydisabled == true) {
                                     $scope.myhealthstatus = true;
                                 } else if (brfcry.physicallydisabled == false) {
                                     $scope.myhealthstatus = false;
                                 }
                             } else if (schme.healthstatus == 9) {
                                 console.log('schme.healthstatus', schme.healthstatus);
                                 if (brfcry.mentallydisabled == true) {
                                     $scope.myhealthstatus = true;
                                 } else if (brfcry.mentallydisabled == false) {
                                     $scope.myhealthstatus = false;
                                 }
                             } else if (schme.healthstatus == 4) {
                                 Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 6).getList().then(function (answeredquestions) {
                                     console.log('answeredquestions', answeredquestions);
                                     for (var o = 0; o < answeredquestions.length; o++) {

                                         if (answeredquestions[o].answer.toLowerCase() == 'yes') {
                                             $scope.myhealthstatus = true;
                                         }
                                     }
                                 });
                             } else if (schme.healthstatus == 6) {
                                 Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 8).getList().then(function (answeredquestions) {
                                     console.log('answeredquestions', answeredquestions);

                                     for (var m = 0; m < answeredquestions.length; m++) {
                                         if (answeredquestions[m].answer.toLowerCase() == 'yes') {
                                             $scope.myhealthstatus = true;
                                         }
                                     }
                                 });
                             }


                             if (schme.agegroup == 1) {
                                 $scope.myagestatus = true;

                             } else if (schme.agegroup == 3) {
                                 for (var s = 18; s <= 59; s++) {
                                     if (brfcry.age == [s][0]) {
                                         $scope.myagestatus = true;
                                         //  console.log('valid data'); 
                                     }
                                 }
                             } else if (schme.agegroup == 4) {
                                 for (var v = 60; v <= 79; v++) {
                                     if (brfcry.age == [v][0]) {
                                         $scope.myagestatus = true;
                                         //  console.log('valid data'); 
                                     }
                                 }
                             } else if (schme.agegroup == 5) {
                                 for (var w = 0; w <= 17; w++) {
                                     if (brfcry.age == [w][0]) {
                                         $scope.myagestatus = true;
                                         //  console.log('valid data'); 
                                     }
                                 }
                             } else if (schme.agegroup == 6) {
                                 for (var x = 80; x <= 99; x++) {
                                     if (brfcry.age == [x][0]) {
                                         $scope.myagestatus = true;
                                         //  console.log('valid data'); 
                                     }
                                 }
                             }
                             $scope.modalInstanceLoad.close();
                             //console.log('$scope.myoccupationstatus', $scope.myoccupationstatus);
                             //console.log('$scope.mygenderstatus', $scope.mygenderstatus);
                         });
                     });
                 }
             });
         }
     });*/

    $scope.$watch('schememaster.schemeId', function (newValue, oldValue) {
        //console.log('schemeId', newValue);
        if (newValue == oldValue || newValue == undefined) {
            return;
        } else {
            $scope.schememaster.stage = '';
            $scope.toggleLoading();
            var Array = [];
            var Array2 = [];
            Restangular.one('beneficiaries', $scope.schememaster.memberId).get().then(function (brfcry) {
                $scope.newoccupation = brfcry.typology.toUpperCase();
                //console.log('$scope.newoccupation', $scope.newoccupation);
                if ($scope.newoccupation === 'MIGRANT' || $scope.newoccupation === 'IDU' || $scope.newoccupation === 'TRUCKERS' || $scope.newoccupation === 'GENERAL') {
                    $scope.modalInstanceLoad.close();
                    console.log('i m IF');
                    Restangular.one('occupationstatuses', 1).get().then(function (occstatus) {
                        Restangular.one('schemes', newValue).get().then(function (schme) {
                            //console.log('occstatus', occstatus.id);
                            console.log('schme agegroup', schme.agegroup);
                            //console.log('brfcry', brfcry.gender);
                            Restangular.one('maritalstatuses', brfcry.maritalstatus).get().then(function (marstatus) {
                                //console.log('marstatus', marstatus);
                                Restangular.one('socialstatuses', schme.socialstatus).get().then(function (socstatus) {
                                    //console.log('socstatus', socstatus);
                                    //console.log('brfcry.maritalstatus', brfcry.maritalstatus);
                                    //console.log('socstatus.id', schme.socialstatus);
                                    if (socstatus.id == 1 || socstatus.id == 3 || marstatus.id == 6) {
                                        $scope.mysocialstatus = true;
                                    } else if (marstatus.id == 3 && socstatus.id == 2) {
                                        $scope.mysocialstatus = true;
                                    } else if (marstatus.name == socstatus.name) {
                                        $scope.mysocialstatus = true;
                                    } else {
                                        $scope.mysocialstatus = false;
                                    }
                                    $scope.modalInstanceLoad.close();
                                });
                            });

                            Array = schme.category;
                            Array2 = Array.split(',');
                            //console.log('Array2', Array2.length);
                            for (var i = 0; i < Array2.length; i++) {
                                //console.log('Array.length',Array.length);
                                //console.log('Array', Array2[i]);
                                if (Array2[i] == 15) {
                                    $scope.loanapplied = true;
                                    console.log('$scope.loanapplied', $scope.loanapplied);
                                } else {
                                    $scope.loanapplied = false;
                                    console.log('$scope.loanapplied', $scope.loanapplied);
                                }
                            }


                            if (occstatus.id == schme.occupationstatus || schme.occupationstatus == 1 || schme.occupationstatus == 8) {
                                $scope.myoccupationstatus = true;
                            }

                            if (schme.healthstatus == 1 || schme.healthstatus == 2) {
                                $scope.myhealthstatus = true;

                            } else if (schme.healthstatus == 3) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.plhiv == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.plhiv == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 5) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.physicallydisabled == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.physicallydisabled == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 9) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.mentallydisabled == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.mentallydisabled == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 4) {
                                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 6).getList().then(function (answeredquestions) {
                                    console.log('answeredquestions', answeredquestions);
                                    for (var o = 0; o < answeredquestions.length; o++) {

                                        if (answeredquestions[o].answer.toLowerCase() == 'yes') {
                                            $scope.myhealthstatus = true;
                                        }
                                    }
                                });
                            } else if (schme.healthstatus == 6) {
                                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 8).getList().then(function (answeredquestions) {
                                    console.log('answeredquestions', answeredquestions);

                                    for (var m = 0; m < answeredquestions.length; m++) {
                                        if (answeredquestions[m].answer.toLowerCase() == 'yes') {
                                            $scope.myhealthstatus = true;
                                        }
                                    }
                                });
                            }


                            if (schme.agegroup == 1) {
                                $scope.myagestatus = true;
                            } else if (schme.agegroup == 3) {
                                for (var s = 18; s <= 59; s++) {
                                    if (brfcry.age == [s][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 4) {
                                for (var v = 60; v <= 79; v++) {
                                    if (brfcry.age == [v][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 5) {
                                for (var w = 0; w <= 17; w++) {
                                    if (brfcry.age == [w][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 6) {
                                for (var x = 80; x <= 99; x++) {
                                    if (brfcry.age == [x][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            }
                            $scope.modalInstanceLoad.close();

                        });
                        $scope.modalInstanceLoad.close();
                    });
                    $scope.modalInstanceLoad.close();
                } else {
                    console.log('i m ELSE');
                    Restangular.one('occupationstatuses/findOne?filter[where][name]=' + $scope.newoccupation).get().then(function (occstatus) {
                        Restangular.one('schemes', newValue).get().then(function (schme) {
                            //console.log('occstatus', occstatus.id);
                            console.log('schme agegroup', schme.agegroup);
                            //console.log('brfcry', brfcry.gender);
                            Restangular.one('maritalstatuses', brfcry.maritalstatus).get().then(function (marstatus) {
                                //console.log('marstatus', marstatus);
                                Restangular.one('socialstatuses', schme.socialstatus).get().then(function (socstatus) {
                                    //console.log('socstatus', socstatus);
                                    //console.log('brfcry.maritalstatus', brfcry.maritalstatus);
                                    //console.log('socstatus.id', schme.socialstatus);
                                    if (socstatus.id == 1 || socstatus.id == 3 || marstatus.id == 6) {
                                        $scope.mysocialstatus = true;
                                    } else if (marstatus.id == 3 && socstatus.id == 2) {
                                        $scope.mysocialstatus = true;
                                    } else if (marstatus.name == socstatus.name) {
                                        $scope.mysocialstatus = true;
                                    } else {
                                        $scope.mysocialstatus = false;
                                    }
                                    $scope.modalInstanceLoad.close();
                                });
                            });

                            Array = schme.category;
                            Array2 = Array.split(',');
                            //console.log('Array2', Array2.length);
                            for (var i = 0; i < Array2.length; i++) {
                                //console.log('Array.length',Array.length);
                                //console.log('Array', Array2[i]);
                                if (Array2[i] == 15) {
                                    $scope.loanapplied = true;
                                    console.log('$scope.loanapplied', $scope.loanapplied);
                                } else {
                                    $scope.loanapplied = false;
                                    console.log('$scope.loanapplied', $scope.loanapplied);
                                }
                            }



                            if (occstatus.id == schme.occupationstatus || schme.occupationstatus == 1 || schme.occupationstatus == 8) {
                                $scope.myoccupationstatus = true;
                            }
                            if (schme.healthstatus == 1 || schme.healthstatus == 2) {
                                $scope.myhealthstatus = true;

                            } else if (schme.healthstatus == 3) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.plhiv == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.plhiv == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 5) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.physicallydisabled == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.physicallydisabled == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 9) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.mentallydisabled == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.mentallydisabled == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 4) {
                                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 6).getList().then(function (answeredquestions) {
                                    console.log('answeredquestions', answeredquestions);
                                    for (var o = 0; o < answeredquestions.length; o++) {

                                        if (answeredquestions[o].answer.toLowerCase() == 'yes') {
                                            $scope.myhealthstatus = true;
                                        }
                                    }
                                });
                            } else if (schme.healthstatus == 6) {
                                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 8).getList().then(function (answeredquestions) {
                                    console.log('answeredquestions', answeredquestions);

                                    for (var m = 0; m < answeredquestions.length; m++) {
                                        if (answeredquestions[m].answer.toLowerCase() == 'yes') {
                                            $scope.myhealthstatus = true;
                                        }
                                    }
                                });
                            }


                            if (schme.agegroup == 1) {
                                $scope.myagestatus = true;
                            } else if (schme.agegroup == 3) {
                                for (var s = 18; s <= 59; s++) {
                                    if (brfcry.age == [s][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 4) {
                                for (var v = 60; v <= 79; v++) {
                                    if (brfcry.age == [v][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 5) {
                                for (var w = 0; w <= 17; w++) {
                                    if (brfcry.age == [w][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 6) {
                                for (var x = 80; x <= 99; x++) {
                                    if (brfcry.age == [x][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            }
                            $scope.modalInstanceLoad.close();
                            //console.log('$scope.myoccupationstatus', $scope.myoccupationstatus);
                            //console.log('$scope.mygenderstatus', $scope.mygenderstatus);
                        });
                    });
                }
            });
        }
    });

    $scope.TestamountTaken = function () {
        $scope.schememaster.noofmonthrepay = '';
    }
    $scope.datetimehide = false;
    $scope.reponserec = true;
    $scope.delaydis = true;
    $scope.collectedrequired = true;
    $scope.rejectdis = true;
    $scope.purposeofloanhide = true;
    /***************************************************************/
    $scope.$watch('schememaster.stage', function (newValue, oldValue) {
        //sumanscheme
        //console.log('schememaster.stage suman', newValue)
        if (newValue === oldValue || newValue == '' || newValue == undefined) {
            return;
        } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
            if (newValue === oldValue) {
                return;
            } else
            if (newValue === '4') {
                $scope.reponserec = false;
                $scope.delaydis = true;
                $scope.datetimehide = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.purposeofloanhide = true;
            } else if (newValue === '5') {
                $scope.delaydis = false;
                $scope.reponserec = true;
                $scope.datetimehide = true;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                // $scope.schememaster.datetime = sevendays;
            } else if (newValue === '2') {
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.datetimehide = true;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                //$scope.schememaster.datetime = sevendays;
            } else if (newValue === '3') {
                //$scope.schememaster.datetime = sevendays;
                $scope.datetimehide = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            } else if (newValue === '7') {
                var sixmonth = new Date();
                sixmonth.setDate(sixmonth.getDate() + 180);
                // $scope.schememaster.datetime = sixmonth;
                $scope.schememaster.datetime = new Date();
                $scope.datetimehide = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.schememaster.responserecieve = null;
                console.log('$scope.loanapplied', $scope.loanapplied);
                if ($scope.loanapplied == true && newValue === '7') {
                    $scope.purposeofloanhide = false;
                    $scope.datetimehide = false;
                    var onemonth = new Date();
                    onemonth.setDate(onemonth.getDate() + 30);
                    $scope.schememaster.datetime = onemonth;
                }
            } else if (newValue === '1') {
                //$scope.schememaster.datetime = sevendays;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                $scope.datetimehide = true;
            } else if (newValue === '8') {
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            } else if (newValue === '6') {
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            } else if (newValue === '9') {
                $scope.datetimehide = false;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.datetime = new Date();
                $scope.purposeofloanhide = true;
            } else {
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.delay = null;
                //$scope.schememaster.datetime = '';
                $scope.datetimehide = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
        } else {
            // console.log('schememaster.stage', newValue);
            var fifteendays = new Date();
            fifteendays.setDate(fifteendays.getDate() + 15);
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            if (newValue === oldValue) {
                return;
            } else if (newValue === '4') {
                $scope.reponserec = false;
                $scope.delaydis = true;
                $scope.datetimehide = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.purposeofloanhide = true;
            } else if (newValue === '5') {
                $scope.delaydis = false;
                $scope.reponserec = true;
                $scope.datetimehide = true;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.datetime = sevendays;
                $scope.purposeofloanhide = true;
            } else if (newValue === '2') {
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.datetimehide = true;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.schememaster.datetime = sevendays;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            } else if (newValue === '3') {
                $scope.schememaster.datetime = sevendays;
                $scope.datetimehide = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            } else if (newValue === '7') {
                var sixmonth = new Date();
                sixmonth.setDate(sixmonth.getDate() + 180);
                $scope.schememaster.datetime = new Date();
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.schememaster.responserecieve = null;
                console.log('$scope.loanapplied1', $scope.loanapplied);
                if ($scope.loanapplied == true && newValue === '7') {
                    $scope.purposeofloanhide = false;
                    $scope.datetimehide = false;
                    var onemonth = new Date();
                    onemonth.setDate(onemonth.getDate() + 30);
                    $scope.schememaster.datetime = onemonth;
                }
            } else if (newValue === '1') {
                $scope.schememaster.datetime = fifteendays;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                $scope.datetimehide = true;
            } else if (newValue === '8') {
                $scope.schememaster.datetime = sevendays;
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            } else if (newValue === '6') {
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            } else if (newValue === '9') {
                $scope.datetimehide = false;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.datetime = new Date();
                $scope.purposeofloanhide = true;
            } else {
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.delay = null;
                //$scope.schememaster.datetime = '';
                $scope.datetimehide = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
        }
    });
    /****************************
    $scope.$watch('schememaster.stage', function (newValue, oldValue) {
        //sumanscheme
        if (newValue === oldValue || newValue == '' || newValue == undefined) {
            return;
        } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
         console.log('schememaster.stage suman', newValue)
            if (newValue === oldValue) {
                return;
            } else if (newValue === '4') {
                $scope.reponserec = false;
                $scope.delaydis = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '5') {
                $scope.delaydis = false;
                $scope.reponserec = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                // $scope.schememaster.datetime = sevendays;
            }
            else if (newValue === '2') {
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                //$scope.schememaster.datetime = sevendays;
            }
            else if (newValue === '3') {
                //$scope.schememaster.datetime = sevendays;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '7') {
                var sixmonth = new Date();
                sixmonth.setDate(sixmonth.getDate() + 180);
                // $scope.schememaster.datetime = sixmonth;
                $scope.schememaster.datetime = new Date();
                $scope.datetimehide = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.schememaster.responserecieve = null;
                console.log('$scope.loanapplied', $scope.loanapplied);
                if ($scope.loanapplied == true && newValue === '7') {
                    $scope.purposeofloanhide = false;
                    $scope.datetimehide = false;
                    var onemonth = new Date();
                    onemonth.setDate(onemonth.getDate() + 30);
                    $scope.schememaster.datetime = onemonth;
                }
            }
            else if (newValue === '1') {
                //$scope.schememaster.datetime = sevendays;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '8') {
                $scope.datetimehide = false;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '6') {
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '9') {
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.datetime = new Date();
                $scope.purposeofloanhide = true;
            }
            else {
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.delay = null;
                //$scope.schememaster.datetime = '';
                $scope.datetimehide = false;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
        }
        else {
            // console.log('schememaster.stage', newValue);
            var fifteendays = new Date();
            fifteendays.setDate(fifteendays.getDate() + 15);
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            if (newValue === oldValue) {
                return;
            }
            else if (newValue === '4') {
                $scope.reponserec = false;
                $scope.delaydis = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '5') {
                $scope.delaydis = false;
                $scope.reponserec = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.datetime = sevendays;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '2') {
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.datetimehide = false;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.schememaster.datetime = sevendays;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '3') {
                $scope.schememaster.datetime = sevendays;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '7') {
                var sixmonth = new Date();
                sixmonth.setDate(sixmonth.getDate() + 180);
                $scope.schememaster.datetime = new Date();
                $scope.datetimehide = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.schememaster.responserecieve = null;
                console.log('$scope.loanapplied1', $scope.loanapplied);
                if ($scope.loanapplied == true && newValue === '7') {
                    $scope.purposeofloanhide = false;
                    $scope.datetimehide = false;
                    var onemonth = new Date();
                    onemonth.setDate(onemonth.getDate() + 30);
                    $scope.schememaster.datetime = onemonth;
                }
            }
            else if (newValue === '1') {
                $scope.schememaster.datetime = fifteendays;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '8') {
                $scope.schememaster.datetime = sevendays;
                $scope.datetimehide = false;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '6') {
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
            else if (newValue === '9') {
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.datetime = new Date();
                $scope.purposeofloanhide = true;
            }
            else {
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.delay = null;
                //$scope.schememaster.datetime = '';
                $scope.datetimehide = false;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
            }
        }
    });
    
    
    
   /**************************************************************/
    $scope.rejectdis = true;
    $scope.$watch('schememaster.responserecieve', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '' || newValue == undefined) {
            return;
        } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
            return;
        } else {
            //console.log('responserecieve', newValue);
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 180);
            if (newValue === oldValue) {
                $scope.rejectdis = true;
                return;
            } else if (newValue === null) {
                $scope.rejectdis = true;
                return;
            } else if (newValue === 'Approved') {
                $scope.schememaster.datetime = sevendays;
                $scope.rejectdis = true;
            } else if (newValue === 'Rejected') {
                $scope.rejectdis = false;
                $scope.datetimehide = true;
                $scope.schememaster.datetime = new Date();
            } else {
                //$scope.schememaster.datetime = '';
                $scope.rejectdis = true;
                $scope.schememaster.rejection = null;
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.delay = null;
                //$scope.schememaster.datetime = '';
                $scope.datetimehide = false;
            }
        };
    });
    /********************************************************************************************/
    $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=false').getList().then(function (scheme) {
        $scope.printschemes = scheme;
        angular.forEach($scope.printschemes, function (member, index) {
            member.index = index;
            member.enabled = false;
        });
    });
    $scope.docm = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().then(function (docmt) {
        $scope.printdocuments = docmt;
        angular.forEach($scope.printdocuments, function (member, index) {
            member.index = index;
            member.enabled = false;
        });
    });
    $scope.getGender = function (genderId) {
        return Restangular.one('genders', genderId).get().$object;
    };
    $scope.schememaster = {
        attendees: []
    };
    $scope.schememaster.attendees = [];
    $scope.$watch('schememaster.attendees', function (newValue, oldValue) {
        // console.log('watch.attendees', newValue);
        if (newValue === oldValue || newValue == undefined) {
            return;
        } else {
            if (newValue.length > oldValue.length) {
                // something was added
                var Array1 = newValue;
                var Array2 = oldValue;
                for (var i = 0; i < Array2.length; i++) {
                    var arrlen = Array1.length;
                    for (var j = 0; j < arrlen; j++) {
                        if (Array2[i] == Array1[j]) {
                            Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                        }
                    }
                }
                $scope.printschemes[Array1[0]].enabled = true;
                // do stuff
            } else if (newValue.length < oldValue.length) {
                // something was removed
                var Array1 = oldValue;
                var Array2 = newValue;
                for (var i = 0; i < Array2.length; i++) {
                    var arrlen = Array1.length;
                    for (var j = 0; j < arrlen; j++) {
                        if (Array2[i] == Array1[j]) {
                            Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                        }
                    }
                }
                $scope.printschemes[Array1[0]].enabled = false;
            }
        }
    });
    /************************************************ Search On Modal *************************/
    $scope.schemename = $scope.name;
    $scope.$watch('schememaster.agegrp', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=false' + '&filter[where][agegroup]=' + newValue).getList().then(function (scheme) {
                $scope.schemes = scheme;
                angular.forEach($scope.schemes, function (member, index) {
                    member.index = index;
                    member.enabled = false;
                });
            });
        }
    });
    $scope.$watch('schememaster.gender', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=false' + '&filter[where][gender]=' + newValue).getList().then(function (scheme) {
                $scope.schemes = scheme;
                angular.forEach($scope.schemes, function (member, index) {
                    member.index = index;
                    member.enabled = false;
                });
            });
        }
    });
    $scope.$watch('schememaster.stateid', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=false' + '&filter[where][state]=' + newValue).getList().then(function (scheme) {
                $scope.schemes = scheme;
                angular.forEach($scope.schemes, function (member, index) {
                    member.index = index;
                    member.enabled = false;
                });
            });
        }
    });
    $scope.openSchemeFlow = function (size) {
        $scope.modalSchemeFlow = $modal.open({
            animation: true,
            templateUrl: 'template/showschemesModal.html',
            scope: $scope,
            backdrop: 'static',
            size: size
        });
    };
    $scope.SaveSchemeFlow = function () {
        $scope.newArray = [];
        $scope.schememaster.attendees = [];
        for (var i = 0; i < $scope.printschemes.length; i++) {
            if ($scope.printschemes[i].enabled == true) {
                $scope.newArray.push($scope.printschemes[i].index);
            }
        }
        $scope.schememaster.attendees = $scope.newArray
        $scope.modalSchemeFlow.close();
    };
    $scope.okSchemeFlow = function () {
        $scope.modalSchemeFlow.close();
    };
    $scope.showschemesModal = false;
    $scope.toggleschemesModal = function () {
        $scope.SaveScheme = function () {
            $scope.newArray = [];
            $scope.schememaster.attendees = [];
            for (var i = 0; i < $scope.printschemes.length; i++) {
                if ($scope.printschemes[i].enabled == true) {
                    $scope.newArray.push($scope.printschemes[i].index, $scope.printdocuments[j].index);
                }
            }
            $scope.schememaster.attendees = $scope.newArray
            //console.log('$scope.groupmeeting.attendees', $scope.schememaster.attendees.id);
            $scope.showschemesModal = !$scope.showschemesModal;
        };
        $scope.showschemesModal = !$scope.showschemesModal;
    };
    $scope.CancelScheme = function () {
        $scope.showschemesModal = !$scope.showschemesModal;
    };
    /******************************************/
    $scope.hideUpdateCDIDS = true;
    $scope.hideUpdateTodo = true;
    $scope.hideUpdateRI = true;
    $scope.hideUpdateAW = true;
    //Edit OnetoOne////////////
    $scope.EditoneToOne = function (todotype, id, data, pillarid) {
        console.log('EditoneToOne', id);
        $scope.UpdateQuestionId = data.questionid;
        if (pillarid == 1) {
            //$scope.Pillar = 'SP';
            if (data.questionid != null) {
                $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
                //$scope.UpdateDisplayClickOption = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + data.questionid).get().$object;
                $scope.UpdateDisplayClickOption12 = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + data.questionid).get().then(function (Sans) {
                    //console.log('Sans', Sans[0].answer);
                    if ($window.sessionStorage.language == 1) {
                        $scope.PillarName = 'SP';
                        if (Sans[0].answer == 'yes') {
                            $scope.UpdateDisplayClickOption = 'Yes';
                        } else {
                            $scope.UpdateDisplayClickOption = 'No';
                        }
                    } else if ($window.sessionStorage.language == 2) {
                        $scope.PillarName = '?? ??';
                        if (Sans[0].answer == 'yes') {
                            $scope.UpdateDisplayClickOption = '???';
                        } else {
                            $scope.UpdateDisplayClickOption = '????';
                        }
                    } else if ($window.sessionStorage.language == 3) {
                        $scope.PillarName = '??? ??';
                        if (Sans[0].answer == 'yes') {
                            $scope.UpdateDisplayClickOption = '????';
                        } else {
                            $scope.UpdateDisplayClickOption = '????';
                        }
                    } else if ($window.sessionStorage.language == 4) {
                        $scope.PillarName = '?????';
                        if (Sans[0].answer == 'yes') {
                            $scope.UpdateDisplayClickOption = '???';
                        } else {
                            $scope.UpdateDisplayClickOption = '?????';
                        }
                    } else if ($window.sessionStorage.language == 5) {
                        $scope.PillarName = '???.??.';
                        if (Sans[0].answer == 'yes') {
                            $scope.UpdateDisplayClickOption = '?????';
                        } else {
                            $scope.UpdateDisplayClickOption = '????';
                        }
                    } else if ($window.sessionStorage.language == 6) {
                        $scope.PillarName = '?? ??';
                        if (Sans[0].answer == 'yes') {
                            $scope.UpdateDisplayClickOption = '???'
                        } else {
                            $scope.UpdateDisplayClickOption = '????'
                        }
                    }
                });
            } else {
                $scope.UpdateQuestion = 'Scheme/Document';
                $scope.UpdateDisplayClickOption = '';
            }
        } else if (pillarid == 2) {
            //console.log('I am ssj');
            //$scope.Pillar = 'SSJ';
            if (data.questionid != null) {
                $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
                //$scope.UpdateDisplayClickOption = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + data.questionid).get().$object;
                $scope.UpdateDisplayClickOption = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + data.questionid).get().then(function (Sans) {
                    //console.log('Sans',Sans[0].answer);
                    if ($window.sessionStorage.language == 1) {
                        $scope.PillarName = 'SSJ';
                        if (Sans[0].answer == 'yes') {
                            $scope.UpdateDisplayClickOption = 'Yes';
                        } else {
                            $scope.UpdateDisplayClickOption = 'No';
                        }
                    } else if ($window.sessionStorage.language == 2) {
                        $scope.PillarName = '?? ?? ??';
                        if (Sans[0].answer == 'yes') {
                            $scope.UpdateDisplayClickOption = '???';
                        } else {
                            $scope.UpdateDisplayClickOption = '????';
                        }
                    } else if ($window.sessionStorage.language == 3) {
                        $scope.PillarName = '??? ??? ??';
                        if (Sans[0].answer == 'yes') {
                            $scope.UpdateDisplayClickOption = '????';
                        } else {
                            $scope.UpdateDisplayClickOption = '????';
                        }
                    } else if ($window.sessionStorage.language == 4) {
                        $scope.PillarName = '????????';
                        if (Sans[0].answer == 'yes') {
                            $scope.DisplayClickOption = '???';
                        } else {
                            $scope.UpdateDisplayClickOption = '?????';
                        }
                    } else if ($window.sessionStorage.language == 5) {
                        $scope.PillarName = '???.???.??.??'
                        if (Sans[0].answer == 'yes') {
                            $scope.UpdateDisplayClickOption = '?????';
                        } else {
                            $scope.UpdateDisplayClickOption = '????';
                        }
                    } else if ($window.sessionStorage.language == 6) {
                        $scope.PillarName = '?? ?? ??';
                        if (Sans[0].answer == 'yes') {
                            $scope.UpdateDisplayClickOption = '???'
                        } else {
                            $scope.UpdateDisplayClickOption = '????'
                        }
                    }
                });
            } else {
                $scope.UpdateQuestion = 'Report Incident';
                $scope.UpdateDisplayClickOption = '';
            }
        } else if (pillarid == 3) {
            //$scope.Pillar = 'FS';
            $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
            //$scope.UpdateDisplayClickOption = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + data.questionid).get().$object;
            $scope.UpdateDisplayClickOption12 = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + data.questionid).get().then(function (Sans) {
                //console.log('Sans',Sans[0].answer);
                if ($window.sessionStorage.language == 1) {
                    $scope.PillarName = 'FS';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = 'Yes';
                    } else {
                        $scope.UpdateDisplayClickOption = 'No';
                    }
                } else if ($window.sessionStorage.language == 2) {
                    $scope.PillarName = '?? ??';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '???';
                    } else {
                        $scope.UpdateDisplayClickOption = '????';
                    }
                } else if ($window.sessionStorage.language == 3) {
                    $scope.PillarName = '??? ???';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '????';
                    } else {
                        $scope.UpdateDisplayClickOption = '????';
                    }
                } else if ($window.sessionStorage.language == 4) {
                    $scope.PillarName = '???????';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '???';
                    } else {
                        $scope.UpdateDisplayClickOption = '?????';
                    }
                } else if ($window.sessionStorage.language == 5) {
                    $scope.PillarName = '???.???.';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '?????';
                    } else {
                        $scope.UpdateDisplayClickOption = '????';
                    }
                } else if ($window.sessionStorage.language == 6) {
                    $scope.PillarName = '?? ??';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '???'
                    } else {
                        $scope.UpdateDisplayClickOption = '????'
                    }
                }
            });
        } else if (pillarid == 4) {
            //$scope.Pillar = 'IDS';
            $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
            //$scope.UpdateDisplayClickOption = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + data.questionid).get().$object;
            $scope.UpdateDisplayClickOption12 = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + data.questionid).get().then(function (Sans) {
                //console.log('Sans',Sans[0].answer);
                if ($window.sessionStorage.language == 1) {
                    $scope.PillarName = 'IDS';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = 'Yes';
                    } else {
                        $scope.UpdateDisplayClickOption = 'No';
                    }
                } else if ($window.sessionStorage.language == 2) {
                    $scope.PillarName = '?? ?? ??';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '???';
                    } else {
                        $scope.UpdateDisplayClickOption = '????';
                    }
                } else if ($window.sessionStorage.language == 3) {
                    $scope.PillarName = '??????';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '????';
                    } else {
                        $scope.UpdateDisplayClickOption = '????';
                    }
                } else if ($window.sessionStorage.language == 4) {
                    $scope.PillarName = '??????';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '???';
                    } else {
                        $scope.UpdateDisplayClickOption = '?????';
                    }
                } else if ($window.sessionStorage.language == 5) {
                    $scope.PillarName = '?.??.???.';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '?????';
                    } else {
                        $scope.UpdateDisplayClickOption = '????';
                    }
                } else if ($window.sessionStorage.language == 6) {
                    $scope.PillarName = '?? ?? ??'
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '???'
                    } else {
                        $scope.UpdateDisplayClickOption = '????'
                    }
                }
            });
        } else if (pillarid == 5) {
            //  $scope.Pillar = 'HEALTH';
            $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
            $scope.UpdateDisplayClickOption12 = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + data.questionid).get().then(function (Sans) {
                //console.log('Sans',Sans[0].answer);
                if ($window.sessionStorage.language == 1) {
                    $scope.PillarName = 'HEALTH';
                    console.log('$scope.PillarName', $scope.PillarName);
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = 'Yes';
                    } else {
                        $scope.UpdateDisplayClickOption = 'No';
                    }
                } else if ($window.sessionStorage.language == 2) {
                    $scope.PillarName = '?????????';
                    console.log('$scope.PillarName', $scope.PillarName);
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '???';
                    } else {
                        $scope.UpdateDisplayClickOption = '????';
                    }
                } else if ($window.sessionStorage.language == 3) {
                    $scope.PillarName = '??????';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '????';
                    } else {
                        $scope.UpdateDisplayClickOption = '????';
                    }
                } else if ($window.sessionStorage.language == 4) {
                    $scope.PillarName = '????????';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '???';
                    } else {
                        $scope.UpdateDisplayClickOption = '?????';
                    }
                } else if ($window.sessionStorage.language == 5) {
                    $scope.PillarName = '???????';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '?????';
                    } else {
                        $scope.UpdateDisplayClickOption = '????';
                    }
                } else if ($window.sessionStorage.language == 6) {
                    $scope.PillarName = '??????';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '???'
                    } else {
                        $scope.UpdateDisplayClickOption = '????'
                    }
                }
            });
        }
        //console.log('todotype', pillarid);
        // console.log('id', id);
        if (pillarid == 5) {
            $scope.hideUpdateTodo = false;
            $scope.todotyes = Restangular.all('todotypes?filter[where][pillarid]=' + pillarid).getList().then(function (todotyes) {
                $scope.todotypes = todotyes;
                $scope.todo = data;
                $scope.openToDo();
            });
        }
        if (pillarid == 4 && todotype != null) {
            $scope.hideUpdateTodo = false;
            $scope.todotyes = Restangular.all('todotypes?filter[where][pillarid]=' + pillarid).getList().then(function (todotyes) {
                $scope.todotypes = todotyes;
                $scope.todo = data;
                if (todotype != 35) {
                    $scope.openToDo();
                } else {
                    $scope.openToDoPhoneName();
                }
            });
        }
        if (pillarid == 2) {
            $scope.hideUpdateRI = false;
            $scope.DisableFollowupRI = true;
            $scope.followuphideUpdate = false;
            if (data.id != undefined && data.id != '' && data.id != null) {
                $scope.report = Restangular.one('reportincidents', data.id).get().then(function (report) {
                    //console.log('report',report.followupneeded);
                    $scope.reportincident = report;
                    $scope.re1 = Restangular.all('reportincidentfollowups?filter[where][id]=' + report.followupneeded).getList().then(function (NeedRes) {
                        $scope.reportincidentfollowups = NeedRes;
                    });
                    if (report.followupneeded != null) {
                        $scope.reportincident.follow = report.followupneeded.split(',');
                        console.log('$scope.reportincident.follow', $scope.reportincident.follow);
                    } else {
                        $scope.reportincident.follow = [];
                    }
                    $scope.reportincident.multiplemembers = [];
                    $scope.reportincident.multiplemembers.push(report.beneficiaryid);
                    console.log('$scope.reportincident.multiplemembers2', $scope.reportincident.multiplemembers);
                    $scope.todo = data;
                    $scope.openRI('lg');
                });
            } else {
                $scope.todo = data;
                $scope.openRI('lg');
            }
        }
        if (pillarid == 1) {
            //console.log('Me working',data.documentflag)
            $scope.hideUpdateAW = false;
            if (data.documentflag == true) {
                $scope.responcedreceived = Restangular.all('responcedreceived?filter[where][deleteflag]=false').getList().$object;
                $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteflag]=false').getList().$object;
                $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteflag]=false').getList().$object;
                Restangular.one('documenttypes', data.documentid).get().then(function (doc) {
                    $scope.SchemeOrDocumentname = doc;
                });
                if (data.reportincidentid != undefined && data.reportincidentid != '' && data.reportincidentid != null) {
                    $scope.report = Restangular.one('schememasters', data.reportincidentid).get().then(function (report) {
                        console.log('schememasters me', report);
                        $scope.schemstage = Restangular.all('schemestages?filter[where][deleteflag]=false').getList().then(function (schemstage) {
                            $scope.schemestages = schemstage;
                            angular.forEach($scope.schemestages, function (member, index) {
                                member.index = index;
                                if (report.stage == member.id) {
                                    $scope.stageLevel = member.level;
                                }
                            });
                            if (report.stage == 4 && report.responserecieve === 'Rejected') {
                                $scope.reponserec = false;
                                $scope.rejectdis = false;
                                angular.forEach($scope.schemestages, function (member, index) {
                                    member.index = index;
                                    if (member.id == 4) {
                                        member.enabled = false;
                                    } else {
                                        member.enabled = true
                                    }
                                });
                            } else {
                                angular.forEach($scope.schemestages, function (member, index) {
                                    member.index = index;
                                    if ($scope.stageLevel < member.level) {
                                        member.enabled = false;
                                    } else if (member.id == report.stage && $scope.stageLevel == member.level) {
                                        member.enabled = false;
                                    } else {
                                        member.enabled = true
                                    }
                                });
                            }
                        });
                        $scope.datetimehide = false;
                        $scope.reponserec = true;
                        $scope.delaydis = true;
                        $scope.collectedrequired = true;
                        $scope.rejectdis = true;
                        //console.log('report', report);
                        $scope.schememaster = report;
                        $scope.todo = data;
                        $scope.openAW('lg');
                    });
                }
            } else {
                $scope.responcedreceived = Restangular.all('responcedreceived?filter[where][deleteflag]=false').getList().$object;
                $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteflag]=false').getList().$object;
                $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteflag]=false').getList().$object;
                //$scope.noofmonthrepaid = Restangular.all('noofmonthrepay?filter[where][deleteflag]=false').getList().$object;
                //$scope.financialgoals = Restangular.all('financialgoals').getList().$object;
                Restangular.one('schemes', data.documentid).get().then(function (sch) {
                    $scope.SchemeOrDocumentname = sch;
                    /*  console.log('Me2 working',data.documentflag)
                     var Array = [];
                        var Array2 = [];
                 Array = schemes[0].category;
                 Array2 = Array.split(',');
                 //console.log('Array2', Array2.length);
                 for (var i = 0; i < Array2.length; i++) {
                     //console.log('Array.length',Array.length);
                     //console.log('Array', Array2[i]);
                     if (Array2[i] == 15) {
                         $scope.loanapplied = true;
                         //console.log('Loan Applied');
                     }
                     else {
                         $scope.loanapplied = false;
                         //  console.log('Not Applied');
                     }
                 }*/
                });
                if (data.reportincidentid != undefined && data.reportincidentid != '' && data.reportincidentid != null) {
                    $scope.report = Restangular.one('schememasters', data.reportincidentid).get().then(function (report) {
                        $scope.installmentAmount = report.amount / report.noofmonthrepay;
                        $scope.remainingamount = report.amount - report.paidamount;
                        $scope.emipaid = $scope.remainingamount / $scope.installmentAmount;
                        $scope.monthtopay = report.paidamount / $scope.installmentAmount;
                        //console.log('$scope.remainingamount', $scope.remainingamount);
                        //console.log('$scope.monthtopay', $scope.monthtopay);
                        //console.log('$scope.installmentAmount', $scope.installmentAmount)
                        $scope.schemstage = Restangular.all('schemestages?filter[where][deleteflag]=false').getList().then(function (schemstage) {
                            $scope.schemestages = schemstage;
                            angular.forEach($scope.schemestages, function (member, index) {
                                member.index = index;
                                if (report.stage == member.id) {
                                    $scope.stageLevel = member.level;
                                }
                            });
                            if (report.stage == 4 && report.responserecieve === 'Rejected') {
                                $scope.reponserec = false;
                                $scope.rejectdis = false;
                                angular.forEach($scope.schemestages, function (member, index) {
                                    member.index = index;
                                    if (member.id == 4) {
                                        member.enabled = false;
                                    } else {
                                        member.enabled = true
                                    }
                                });
                            } else {
                                angular.forEach($scope.schemestages, function (member, index) {
                                    member.index = index;
                                    if ($scope.stageLevel < member.level) {
                                        member.enabled = false;
                                    } else if (member.id == report.stage && $scope.stageLevel == member.level) {
                                        member.enabled = false;
                                    } else {
                                        member.enabled = true
                                    }
                                });
                            }
                        });
                        $scope.datetimehide = false;
                        $scope.reponserec = true;
                        $scope.delaydis = true;
                        $scope.collectedrequired = true;
                        $scope.rejectdis = true;
                        //console.log('report', report);
                        $scope.schememaster = report;
                        $scope.todo = data;
                        $scope.openAW('lg');
                    });
                }
            }
        }
        if (pillarid == 3) {
            $scope.hideUpdateTodo = false;
            $scope.todotyes = Restangular.all('todotypes?filter[where][pillarid]=' + pillarid).getList().then(function (todotyes) {
                $scope.todotypes = todotyes;
                $scope.todo = data;
                $scope.openToDo();
            });
        }
    };
    $scope.updateToDo = function () {
        $scope.submittodos.customPUT($scope.todo, $scope.todo.id).then(function (resp) {
            console.log('submittodos9', resp);
            $scope.todo = {};
            $scope.todo = {
                status: 1,
                stateid: $window.sessionStorage.zoneId,
                state: $window.sessionStorage.zoneId,
                districtid: $window.sessionStorage.salesAreaId,
                district: $window.sessionStorage.salesAreaId,
                coid: $window.sessionStorage.coorgId,
                facility: $window.sessionStorage.coorgId,
                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                lastmodifiedtime: new Date(),
                roleId: $window.sessionStorage.roleId
            };
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            $scope.todo.datetime = sevendays;
            $scope.modalToDo.close();
            $scope.hideUpdateTodo = true;
            //$scope.healthtodos = Restangular.all('todos?filter[where][pillarid]=5&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
            $scope.hlthtodos = Restangular.all('todos?filter[where][pillarid]=5&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (hlthtodos) {
                $scope.healthtodos = hlthtodos;
                angular.forEach($scope.healthtodos, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.maintodotypes.length; m++) {
                        if (member.todotype == $scope.maintodotypes[m].id) {
                            //member.TodoType = $scope.maintodotypes[m].name;
                            member.TodoType = $scope.maintodotypes[m];
                            //console.log('member.TodoType', $scope.maintodotypes[m]);
                            break;
                        }
                    }
                    for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                        if (member.status == $scope.maintodostatuses[n].id) {
                            //if ($scope.UserLanguage == 1) {
                            member.TodoStatus = $scope.maintodostatuses[n];
                            /*     break;
                            	 }
                            	 if ($scope.UserLanguage == 2) {
                            	     member.TodoStatus = $scope.maintodostatuses[n].hnname;
                            	 }*/
                            break;
                        }
                    }
                });
            });
            //$scope.fstodos = Restangular.all('todos?filter[where][pillarid]=3&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
            $scope.fstdos = Restangular.all('todos?filter[where][pillarid]=3&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (fstdos) {
                $scope.fstodos = fstdos;
                angular.forEach($scope.fstodos, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.maintodotypes.length; m++) {
                        if (member.todotype == $scope.maintodotypes[m].id) {
                            member.TodoType = $scope.maintodotypes[m];
                            break;
                        }
                    }
                    for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                        if (member.status == $scope.maintodostatuses[n].id) {
                            member.TodoStatus = $scope.maintodostatuses[n];
                            break;
                        }
                    }
                });
            });
            //$scope.idstodos = Restangular.all('todos?filter[where][pillarid]=4&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
            $scope.idstdos = Restangular.all('todos?filter[where][pillarid]=4&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (idstdos) {
                $scope.idstodos = idstdos;
                console.log('$scope.idstodos', $scope.idstodos);
                angular.forEach($scope.idstodos, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.maintodotypes.length; m++) {
                        if (member.todotype == $scope.maintodotypes[m].id) {
                            member.TodoType = $scope.maintodotypes[m];
                            break;
                        }
                    }
                    for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                        if (member.status == $scope.maintodostatuses[n].id) {
                            member.TodoStatus = $scope.maintodostatuses[n];
                            break;
                        }
                    }
                });
            });
        }, function (error) {
            console.log('error', error);
        });
    };
    $scope.updateRI = function () {
        if ($scope.reportincident.physical == true || $scope.reportincident.sexual == true || $scope.reportincident.childrelated == true || $scope.reportincident.emotional == true || $scope.reportincident.propertyrelated == true) {
            if ($scope.reportincident.police == true || $scope.reportincident.clients == true || $scope.reportincident.serviceprovider == true || $scope.reportincident.otherkp == true || $scope.reportincident.neighbour == true || $scope.reportincident.goons == true || $scope.reportincident.partners == true || $scope.reportincident.familymember == true || $scope.reportincident.husband == true) {
                if ($scope.reportincident.reported == false || $scope.reportincident.reportedpolice == true || $scope.reportincident.reportedngos == true || $scope.reportincident.reportedfriends == true || $scope.reportincident.reportedplv == true || $scope.reportincident.reportedcoteam == true || $scope.reportincident.reportedchampions == true || $scope.reportincident.reportedotherkp == true || $scope.reportincident.reportedlegalaid == true) {
                    if ($scope.reportincident.referredcouncelling == true || $scope.reportincident.referredmedicalcare == true || $scope.reportincident.referredcomanager == true || $scope.reportincident.referredplv == true || $scope.reportincident.referredlegalaid == true || $scope.reportincident.referredboardmember == true || $scope.reportincident.noactionreqmember == true || $scope.reportincident.noactiontaken == true) {
                        $scope.updateREPORTINC();
                    } else {
                        $scope.AlertMessage = $scope.optactiontaken; //'At least one option must be selected in Action Taken and Followup';
                        $scope.openOneAlert();
                    }
                } else {
                    $scope.AlertMessage = $scope.optreportedto; //'At least one option must be selected in Reported To';
                    $scope.openOneAlert();
                }
            } else {
                $scope.AlertMessage = $scope.optpreperator; //'At least one option must be selected in  Perpetrator Details';
                $scope.openOneAlert();
            }
        } else {
            $scope.AlertMessage = $scope.optincidenttype; //'At least one option must be selected in Incident Type';
            $scope.openOneAlert();
        }
    };
    $scope.updateREPORTINC = function () {
        console.log('updateRI');
        //$scope.ValidateupdateRI();
        $scope.submitreportincidents.customPUT($scope.reportincident, $scope.reportincident.id).then(function (resp) {
            $scope.reportincident = {
                // "referredby":false,
                "dtmin": new Date(),
                "dtmax": new Date(),
                "physical": false,
                "emotional": false,
                "sexual": false,
                "propertyrelated": false,
                "childrelated": false, //"other": false,
                "police": false,
                "goons": false,
                "clients": false,
                "partners": false,
                "husband": false,
                "serviceprovider": false,
                "familymember": false,
                "otherkp": false,
                "perpetratorother": false,
                "neighbour": false,
                "authorities": false,
                "co": false,
                "timetorespond": false,
                "severity": "Moderate",
                "resolved": false,
                "reported": false,
                "reportedpolice": false,
                "reportedngos": false,
                "reportedfriends": false,
                "reportedplv": false,
                "reportedcoteam": false,
                "reportedchampions": false,
                "reportedotherkp": false,
                "reportedlegalaid": false,
                "referredcouncelling": false,
                "referredmedicalcare": false,
                "referredcomanager": false,
                "referredplv": false,
                "referredlegalaid": false,
                "referredboardmember": false,
                "noactionreqmember": false,
                "noactiontaken": false,
                "multiplemembers": ['1', '2'],
                "stateid": $window.sessionStorage.zoneId,
                "state": $window.sessionStorage.zoneId,
                "districtid": $window.sessionStorage.salesAreaId,
                "district": $window.sessionStorage.salesAreaId,
                "coid": $window.sessionStorage.coorgId,
                "facility": $window.sessionStorage.coorgId,
                "beneficiaryid": null,
                "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
                "lastmodifiedtime": new Date(),
                "lastmodifiedbyrole": $window.sessionStorage.roleId
            };
            $scope.submittodos.customPUT($scope.todo, $scope.todoid).then(function (resp) {
                console.log('submittodos10', resp);
                $scope.todo = {};
                $scope.todo = {
                    status: 1,
                    stateid: $window.sessionStorage.zoneId,
                    state: $window.sessionStorage.zoneId,
                    districtid: $window.sessionStorage.salesAreaId,
                    district: $window.sessionStorage.salesAreaId,
                    coid: $window.sessionStorage.coorgId,
                    facility: $window.sessionStorage.coorgId,
                    lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                    lastmodifiedtime: new Date(),
                    roleId: $window.sessionStorage.roleId
                };
                var sevendays = new Date();
                sevendays.setDate(sevendays.getDate() + 7);
                $scope.todo.datetime = sevendays;
                $scope.hideUpdateRI = true;
                $scope.okRI();
            }, function (error) {
                console.log('error', error);
            });
        }, function (error) {
            console.log('error', error);
        });
    };
    $scope.updateAW = function () {
        console.log('$scope.schememaster.paidamount', $scope.schememaster.paidamount);
        console.log('$scope.studcheck.length', $scope.studcheck.length);
        console.log('$scope.schememaster.amount', $scope.schememaster.amount);
        if ($scope.loanapplied == true && $scope.schememaster.stage == 7) {
            $scope.schememaster.paidamount = $scope.installmentAmount * $scope.studcheck.length;
            $scope.paidamounts = $scope.installmentAmount * $scope.studcheck.length;
        }
        $scope.submitdocumentmasters.customPUT($scope.schememaster, $scope.schememaster.id).then(function (resp) {
            //console.log('$scope.documentmasters', $scope.schememaster);
            $scope.todo.reportincidentid = resp.id;
            $scope.todo.datetime = resp.datetime;
            if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                $scope.todo.status = 3;
            } else
            if ($scope.schememaster.stage == "8") {
                $scope.schememaster.stage = "3";
            } else if (resp.stage == "9" || resp.stage == "7") {
                $scope.todo.status = 3;
            } else if ($scope.schememaster.purposeofloan != 5) {
                $scope.todo.status = 3;
                console.log('$scope.todo.status2', $scope.todo.status);
            } else if ($scope.schememaster.purposeofloan == 5 && $scope.schememaster.amount < 10000) {
                $scope.todo.status = 3;
                console.log('$scope.todo.status1', $scope.todo.status);
            } else if ($scope.paidamounts == $scope.schememaster.amount) {
                $scope.todo.status = 3;
                console.log('$scope.todo.status3', $scope.todo.status);
            } else {
                $scope.todo.status = 1;
            }
            $scope.submittodos.customPUT($scope.todo, $scope.todo.id).then(function (resp) {
                console.log('submittodos11', resp);
                $scope.todo = {};
                $scope.todo = {
                    status: 1,
                    stateid: $window.sessionStorage.zoneId,
                    state: $window.sessionStorage.zoneId,
                    districtid: $window.sessionStorage.salesAreaId,
                    district: $window.sessionStorage.salesAreaId,
                    coid: $window.sessionStorage.coorgId,
                    facility: $window.sessionStorage.coorgId,
                    lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                    lastmodifiedtime: new Date(),
                    roleId: $window.sessionStorage.roleId
                };
                //console.log('$scope.todo', $scope.todo);
                $scope.okAW();
                $scope.sptodos = Restangular.all('todos?filter[where][pillarid]=1&filter[where][beneficiaryid]=' + $routeParams.id).getList().$object;
            });
        });
    };
    $scope.surveyanswerrefer = {};
    $scope.UpdateCDIDS = function () {
        $scope.surveyanswerrefer.id = $scope.cdids.suransid;
        $scope.surveyanswerrefer.availeddate = $scope.cdids.availeddate;
        $scope.surveyanswerrefer.referenceno = $scope.cdids.referenceno;
        $scope.submitsurveyanswers.customPUT($scope.surveyanswerrefer, $scope.cdids.suransid).then(function () {
            $scope.okCDIDS();
            $scope.hideUpdateCDIDS = true;
            $scope.applieddocuments = Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][pillarid]=1&filter[where][answer]=yes&filter[where][availeddate][nlike]=null%').getList().$object;
        });
    };
    $scope.UpdateDocumentReference = function (surveyanswerid) {
        $scope.hideUpdateCDIDS = false;
        if (surveyanswerid != undefined && surveyanswerid != '' && surveyanswerid != null) {
            $scope.surveyanswer = Restangular.one('surveyanswers/findOne?filter[where][id]=' + surveyanswerid).get().then(function (surans) {
                //console.log('surans', surans);
                $scope.cdids.availeddate = surans.availeddate;
                $scope.cdids.referenceno = surans.referenceno;
                $scope.cdids.suransid = surans.id;
                $scope.openCDIDS();
            });
        } else {
            $scope.openCDIDS();
        }
    };
    /************************* Language *****************************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.onetoneheader = langResponse.onetoneheader;
        $scope.previousanswers = langResponse.previousanswers;
        $scope.printlastmeetingdate = langResponse.lastmeetingdate;
        $scope.printfullname = langResponse.fullname;
        $scope.printavahanid = langResponse.avahanid;
        $scope.moredetails = langResponse.moredetails;
        $scope.lessdetails = langResponse.lessdetails;
        $scope.printactivemember = langResponse.activemember;
        $scope.printtypology = langResponse.typology;
        $scope.printdob = langResponse.dob;
        $scope.printnickname = langResponse.nickname;
        $scope.printtiid = langResponse.tiid;
        $scope.printtypology = langResponse.typology;
        $scope.printplhiv = langResponse.plhivprint;
        $scope.printpaidmember = langResponse.paidmember;
        $scope.printage = langResponse.age;
        $scope.monthofhivtest = langResponse.monthofhivtest;
        $scope.monthofstitest = langResponse.monthofstitest;
        $scope.currenthivstatus = langResponse.currenthivstatus;
        $scope.facilityhelplineno = langResponse.facilityhelplineno;
        $scope.todolist = langResponse.todolist;
        $scope.tododisplay = langResponse.tododisplay;
        $scope.date = langResponse.date;
        $scope.status = langResponse.status;
        $scope.prevquestionlist = langResponse.prevquestionlist;
        $scope.yes = langResponse.yes;
        $scope.no = langResponse.no;
        $scope.noofincidentmonth = langResponse.noofincidentmonth;
        $scope.helplineno = langResponse.helplineno;
        $scope.headerreportincident = langResponse.headerreportincident;
        $scope.documentscheme = langResponse.documentscheme;
        $scope.printreferenceno = langResponse.referenceno;
        $scope.dateoflastfs = langResponse.dateoflastfs;
        $scope.monthoflastsaving = langResponse.monthoflastsaving;
        $scope.noofsavingaccount = langResponse.noofsavingaccount;
        $scope.noofinsuranceproduct = langResponse.noofinsuranceproduct;
        $scope.noofpensionproduct = langResponse.noofpensionproduct;
        $scope.noofinvestmentproduct = langResponse.noofinvestmentproduct;
        $scope.noofinformalsaving = langResponse.noofinformalsaving;
        $scope.noofinformalcredit = langResponse.noofinformalcredit;
        $scope.noofformalcredit = langResponse.noofformalcredit;
        $scope.lastmembershipfeespaid = langResponse.lastmembershipfeespaid;
        $scope.dateanddetailnextevent = langResponse.dateanddetailnextevent;
        $scope.whetherbordpast = langResponse.whetherbordpast;
        $scope.proposed = langResponse.proposed;
        $scope.approved = langResponse.approved;
        $scope.noofsavingaccount = langResponse.noofsavingaccount;
        $scope.noofinsuranceproduct = langResponse.noofinsuranceproduct;
        $scope.noofpensionproduct = langResponse.noofpensionproduct;
        $scope.noofinvestmentproduct = langResponse.noofinvestmentproduct;
        $scope.noofinformalsaving = langResponse.noofinformalsaving;
        $scope.noofinformalcredit = langResponse.noofinformalcredit;
        $scope.pillar = langResponse.pillar;
        $scope.healthpillar = langResponse.healthpillar;
        $scope.ssjpillar = langResponse.ssjpillar;
        $scope.sppillar = langResponse.sppillar;
        $scope.fspillar = langResponse.fspillar;
        $scope.idspillar = langResponse.idspillar;
        $scope.alltaskpillar = langResponse.alltaskpillar;
        $scope.selecttodos = langResponse.selecttodos;
        $scope.selectstatus = langResponse.selectstatus;
        $scope.followupdate = langResponse.followupdate;
        $scope.monthtimeavailed = langResponse.monthtimeavailed;
        $scope.referencenumber = langResponse.referencenumber;
        $scope.todosprint = langResponse.todosprint;
        $scope.savebutton = langResponse.savebutton;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.create = langResponse.create;
        $scope.headerreportincident = langResponse.headerreportincident;
        $scope.whendidhappen = langResponse.whendidhappen;
        $scope.multiplepeopleeffect = langResponse.multiplepeopleeffect;
        $scope.membername = langResponse.membername;
        $scope.printmember = langResponse.member;
        $scope.divincidenttype = langResponse.divincidenttype;
        $scope.printphysical = langResponse.physical;
        $scope.sexual = langResponse.sexual;
        $scope.chieldrelated = langResponse.chieldrelated;
        $scope.emotional = langResponse.emotional;
        $scope.propertyrelated = langResponse.propertyrelated;
        $scope.severityofincident = langResponse.severityofincident;
        $scope.divperpetratordetail = langResponse.divperpetratordetail;
        $scope.police = langResponse.police;
        $scope.client = langResponse.client;
        $scope.serviceprovider = langResponse.serviceprovider;
        $scope.otherkp = langResponse.otherkp;
        $scope.neighbour = langResponse.neighbour;
        $scope.goons = langResponse.goons;
        $scope.partners = langResponse.partners;
        $scope.PrintHusband = langResponse.husband;
        $scope.familiymember = langResponse.familiymember;
        $scope.othersexkp = langResponse.othersexkp;
        $scope.divreported = langResponse.divreported;
        $scope.reportedto = langResponse.reportedto;
        $scope.ngos = langResponse.ngos;
        $scope.friends = langResponse.friends;
        $scope.plv = langResponse.plv;
        $scope.coteam = langResponse.coteam;
        $scope.champion = langResponse.champion;
        $scope.otherkps = langResponse.otherkps;
        $scope.legalaidclinic = langResponse.legalaidclinic;
        $scope.divtimetorespond = langResponse.divtimetorespond;
        $scope.refferedcounselling = langResponse.refferedcounselling;
        $scope.refferedmedicalcare = langResponse.refferedmedicalcare;
        $scope.refferedcomanager = langResponse.refferedcomanager;
        $scope.refferedplv = langResponse.refferedplv;
        $scope.refferedaidclinic = langResponse.refferedaidclinic;
        $scope.refferedboardmember = langResponse.refferedboardmember;
        $scope.followupdate = langResponse.followupdate;
        $scope.divactiontaken = langResponse.divactiontaken;
        $scope.printdateofclosure = langResponse.dateofclosure;
        $scope.twohrs = langResponse.twohrs;
        $scope.twototwentyfourhrs = langResponse.twototwentyfourhrs;
        $scope.greatertwentyfourhrs = langResponse.greatertwentyfourhrs;
        $scope.currentstatuscase = langResponse.currentstatuscase;
        $scope.followuprequired = langResponse.followuprequired;
        $scope.name = langResponse.name;
        $scope.hotspot = langResponse.hotspot;
        $scope.phonenumber = langResponse.phonenumber;
        $scope.schemedocument = langResponse.schemedocument;
        $scope.applyforscheme = langResponse.applyforscheme;
        $scope.schemeordocument = langResponse.schemeordocument;
        $scope.scheme = langResponse.scheme;
        $scope.stage = langResponse.stage;
        $scope.responsereceive = langResponse.responsereceive;
        $scope.responserejection = langResponse.responserejection;
        $scope.responsedelay = langResponse.responsedelay;
        $scope.document = langResponse.document;
        $scope.monthyesrinfection = langResponse.monthyesrinfection;
        $scope.noofcondomask = langResponse.noofcondomask;
        $scope.noofcondomprovide = langResponse.noofcondomprovide;
        $scope.dateprovided = langResponse.dateprovided;
        $scope.saving = langResponse.saving;
        $scope.insurance = langResponse.insurance;
        $scope.pension = langResponse.pension;
        $scope.credit = langResponse.credit;
        $scope.remitance = langResponse.remitance;
        $scope.dateoflastsaving = langResponse.dateoflastsaving;
        $scope.state = langResponse.state;
        $scope.agegroup = langResponse.agegroup;
        $scope.gender = langResponse.gender;
        $scope.schemename = langResponse.schemename;
        $scope.schemelocalname = langResponse.schemelocalname;
        $scope.category = langResponse.category;
        $scope.select = langResponse.select;
        $scope.remitance = langResponse.remitance;
        $scope.clickyestorepeat = langResponse.clickyestorepeat;
        $scope.clicknotoview = langResponse.clicknotoview;
        $scope.searchfor = langResponse.searchfor;
        $scope.question = langResponse.question;
        $scope.answer = langResponse.answer;
        $scope.lastmodifytime = langResponse.lastmodifytime;
        $scope.monthavailed = langResponse.monthavailed;
        $scope.data = langResponse.data;
        $scope.ok = langResponse.ok;
        $scope.addbutton = langResponse.addbutton;
        $scope.questionprint = langResponse.question;
        $scope.selectprint = langResponse.select;
        $scope.monthofdetection = langResponse.monthofdetection;
        $scope.purposeofloan = langResponse.purposeofloan;
        $scope.amounttaken = langResponse.amounttaken;
        $scope.printnoofmonthrepay = langResponse.noofmonthrepay;
        $scope.printconsent = langResponse.consent;
        $scope.printconsentmsg = langResponse.consentmsg;
        $scope.optactiontaken = langResponse.optactiontaken;
        $scope.optreportedto = langResponse.optreportedto;
        $scope.optpreperator = langResponse.optpreperator;
        $scope.optincidenttype = langResponse.optincidenttype;
        $scope.atleastonemember = langResponse.atleastonemember;
        $scope.condomaskcantempty = langResponse.condomaskcantempty;
        $scope.condomprovidecantempty = langResponse.condomprovidecantempty;
        $scope.dateprovidedcantempty = langResponse.dateprovidedcantempty;
        $scope.gendernotmatch = langResponse.gendernotmatch;
        $scope.typologynotmatch = langResponse.typologynotmatch;
        $scope.agenotmatch = langResponse.agenotmatch;
        $scope.healthstnotmatch = langResponse.healthstnotmatch;
        $scope.socialstnotmatch = langResponse.socialstnotmatch;
        $scope.pnoactionreqmember = langResponse.noactionreqmember;
        $scope.noactiontaken = langResponse.noactiontaken;
        $scope.printaction = langResponse.action;
    });
});
