'use strict';
angular.module('secondarySalesApp').controller('EditEventsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter, $modal) {
    /*********/
    if ($window.sessionStorage.roleId != 5) {
        window.location = "/";
    }
    $scope.showForm = function () {
        var visible = $location.path() === '/events/create' || $location.path() === '/events/' + $routeParams.id;
        return visible;
    };
    $scope.isCreateView = function () {
        if ($scope.showForm()) {
            var visible = $location.path() === '/events/create';
            return visible;
        }
    };
    $scope.hideCreateButton = function () {
        var visible = $location.path() === '/events/create' || $location.path() === '/events/' + $routeParams.id;
        return visible;
    };
    $scope.hideSearchFilter = function () {
        var visible = $location.path() === '/events/create' || $location.path() === '/events/' + $routeParams.id;
        return visible;
    };
    /*
    	if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
    				$window.sessionStorage.myRoute = null;
    				$window.sessionStorage.myRoute_currentPage = 1;
    				$window.sessionStorage.myRoute_currentPagesize = 5;
    			} else {
    				$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    				$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
    				console.log('$scope.countryId From Landing', $scope.pageSize);
    			}

    			$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    			$scope.PageChanged = function (newPage, oldPage) {
    				$scope.currentpage = newPage;
    				console.log('newPage',newPage);
    				$window.sessionStorage.myRoute_currentPage = newPage;
    			};

    			if ($window.sessionStorage.prviousLocation != "partials/events") {
    				$window.sessionStorage.myRoute_currentPage = 1;
    				$scope.currentpage = 1;
    				$scope.pageSize = 5;
    			}

    			$scope.$watch('pageSize', function (newPageSize, oldPageSize) {
    				console.log('newPageSize', newPageSize);
    				$scope.pageSize = newPageSize;
    				$window.sessionStorage.myRoute_currentPagesize = newPageSize;
    			});
    */
    /***************************************** Pagination ********************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 5;
    }
    else {
        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        console.log('$scope.countryId From Landing', $scope.pageSize);
    }
    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
    };
    if ($window.sessionStorage.prviousLocation != "partials/events") {
        $window.sessionStorage.myRoute_currentPage = 1;
        //$window.sessionStorage.myRoute_currentPagesize = 5;
        $scope.currentpage = 1;
        $scope.pageSize = 5;
    }
    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
        console.log('mypage', mypage);
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    /*********/
    $scope.event = {
        lastmodifiedtime: new Date()
        , lastmodifiedby: $window.sessionStorage.UserEmployeeId
        , facility: $window.sessionStorage.coorgId
        , facilityId: $window.sessionStorage.UserEmployeeId
        , state: $window.sessionStorage.zoneId
        , district: $window.sessionStorage.salesAreaId
        , deleteflag: false
    };
    $scope.auditlog = {
        modifiedbyroleid: $window.sessionStorage.roleId
        , modifiedby: $window.sessionStorage.UserEmployeeId
        , lastmodifiedtime: new Date()
        , entityroleid: 54
        , state: $window.sessionStorage.zoneId
        , district: $window.sessionStorage.salesAreaId
        , facility: $window.sessionStorage.coorgId
        , facilityId: $window.sessionStorage.UserEmployeeId
        , lastmodifiedby: $window.sessionStorage.UserEmployeeId
    };
    $scope.UserLanguage = $window.sessionStorage.language;
    $scope.eventcreate = false;
    $scope.todate = new Date();
    $scope.documenthide = true;
    $scope.$watch('event.organisedby', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        }
        else {
            if (newValue == 41) {
                $scope.documenthide = false;
            }
            else {
                $scope.documenthide = true;
                $scope.event.others = null;
            }
        }
    });
    if ($routeParams.id) {
        $scope.pillars = Restangular.all('pillars').getList().$object;
        Restangular.one('events', $routeParams.id).get().then(function (event) {
            $scope.original = event;
            $scope.event = Restangular.copy($scope.original);
            $scope.modalInstanceLoad.close();
            $scope.eventtypes1 = Restangular.all('eventtypes').getList().then(function (etypResponse) {
                $scope.eventtypes = etypResponse;
            });
            $scope.PillarChanged = function (pillarid) {
                $scope.eventtypes = Restangular.all('eventtypes?filter[where][pillarid]=' + pillarid).getList().$object;
            };
            /*************************************************** Organised By **************************/
            if ($window.sessionStorage.roleId == 5) {
                Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fwrResponse) {
                    $scope.fieldworkers = fwrResponse;
                });
                $scope.Organisedcomembers = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + $window.sessionStorage.UserEmployeeId + ',41]}}}').getList().$object;
            }
            else {
                Restangular.all('fieldworkers?filter[where][id]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (fwrResponse2) {
                    $scope.fieldworkers = fwrResponse2;
                });
                Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fwrResponse3) {
                    $scope.Organisedcomembers = Restangular.all('comembers?filter={"where":{"id":{"inq":[' + fwrResponse3.lastmodifiedby + ',41]}}}').getList().$object;
                });
            }
        });
    }
    /***************************************** UPDATE *******************************************/
    $scope.validatestring = '';
    $scope.createDisabled = false;
    $scope.UpdateEvent = function () {
        //$scope.eventdataModal = !$scope.eventdataModal;
        if ($scope.event.datetime == '' || $scope.event.datetime == null) {
            $scope.validatestring = $scope.validatestring + $scope.seldate;
        }
        else
        if ($scope.event.name == '' || $scope.event.name == null) {
            $scope.validatestring = $scope.validatestring + $scope.entereventname;
        }
        else if ($scope.event.venue == '' || $scope.event.venue == null) {
            $scope.validatestring = $scope.validatestring + $scope.entervenue;
        }
        else if ($scope.event.pillarid == '' || $scope.event.pillarid == null) {
            $scope.validatestring = $scope.validatestring + $scope.selpillar;
        }
        else if ($scope.event.eventype == '' || $scope.event.eventype == null) {
            $scope.validatestring = $scope.validatestring + $scope.seleventtype;
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
            //$scope.eventdataModal = !$scope.eventdataModal;
        }
        else {
			$scope.openOneAlert();
            $scope.createDisabled = true;
            Restangular.one('events', $routeParams.id).customPUT($scope.event).then(function (response) {
                $scope.auditlog.entityid = response.id;
                var respdate = $filter('date')(response.datetime, 'dd-MMMM-yyyy');
                $scope.auditlog.description = 'Event Updated With Following Details: ' + 'Event Name  - ' + response.name + ' , ' + 'Venue - ' + response.venue + ' , ' + 'Pillar - ' + response.pillarid + ' , ' + 'Event Type - ' + response.eventype + ' , ' + 'Organized By - ' + response.organisedby + ' , ' + 'Date - ' + respdate;
                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                    window.location = '/events';
                });
            });
        };
    };
    $scope.openOneAlert = function () {
         console.log('Me Calling')
         $scope.modalOneAlert = $modal.open({
             animation: true
             , templateUrl: 'template/AlertModal.html'
             , scope: $scope
             , backdrop: 'static'
             , keyboard: false
             , size: 'sm'
             , windowClass: 'modal-sucess '
         });
     };
    $scope.eventdataModal = false;
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    /********************************************** Watch Others **************************/
    //Datepicker settings start
    $scope.today = function () {
        $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
    };
    $scope.today();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.dtmax = new Date();
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.open = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.opened = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy'
        , 'starting-day': 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end
    /******************* Language **********************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.eventheader = langResponse.eventheader;
        $scope.addnew = langResponse.addnew;
        $scope.date = langResponse.date;
        $scope.eventname = langResponse.eventname;
        $scope.show = langResponse.show;
        $scope.entry = langResponse.entry;
        $scope.searchfor = langResponse.searchfor;
        $scope.action = langResponse.action;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.venue = langResponse.venue;
        $scope.pillar = langResponse.pillar;
        $scope.eventtype = langResponse.eventtype;
        $scope.organizedby = langResponse.organizedby;
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
        $scope.selpillar = langResponse.selpillar;
        $scope.message = langResponse.eventupdate;
		$scope.entereventname = langResponse.entereventname;
		$scope.entervenue = langResponse.entervenue;
		$scope.seleventtype = langResponse.seleventtype;
		$scope.modalTitle = langResponse.thankyou;
    });
});