'use strict';
angular.module('secondarySalesApp').controller('StakeHolderCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter, $modal) {
    /*********/
    $scope.addedtodos = [];
    if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        window.location = "/";
    }
    $scope.showForm = function () {
        var visible = $location.path() === '/stakeholdermeetings/create' || $location.path() === '/stakeholdermeetings/' + $routeParams.id;
        return visible;
    };
    $scope.isCreateView = function () {
        if ($scope.showForm()) {
            var visible = $location.path() === '/stakeholdermeetings/create';
            return visible;
        }
    };
    $scope.hideCreateButton = function () {
        var visible = $location.path() === '/stakeholdermeetings/create' || $location.path() === '/stakeholdermeetings/' + $routeParams.id;
        return visible;
    };
    $scope.hideSearchFilter = function () {
        var visible = $location.path() === '/stakeholdermeetings/create' || $location.path() === '/stakeholdermeetings/' + $routeParams.id;
        return visible;
    };
    /***********************************************Pagination *********************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    if ($window.sessionStorage.prviousLocation != "partials/stakeholder") {
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
    };
    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    /***************************************************/
    $scope.stakeholder = {
        follow: [],
        state: $window.sessionStorage.zoneId, //district: $window.sessionStorage.salesAreaId,
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        deleteflag: false
    };
    $scope.auditlog = {
        modifiedbyroleid: $window.sessionStorage.roleId,
        modifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        entityroleid: 42,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
    };

    $scope.getOrg = function (site) {
        return Restangular.one('organisebystakeholders?filter[where][name]=' + site).get().$object;
    };
    /********************************************* INDEX *******************************************/
    /*  $scope.STypeFilter = function () {
          $scope.backgroundCol1 = "blue";
          $scope.backgroundCol2 = "";
          $scope.emps = Restangular.all('stakeholdertypes').getList().then(function (emps) {
              $scope.stakeholdertypes = emps;
              $scope.zn = Restangular.all('stakeholders?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=stakeholdertype%20ASC').getList().then(function (zn) {
                  $scope.stakeholders = zn;
                  $scope.modalInstanceLoad.close();
                  angular.forEach($scope.stakeholders, function (member, index) {
                      member.index = index + 1;
                      member.stakeholdertypes = $scope.getStakeholdertype(member.stakeholdertype);
                      for (var n = 0; n < $scope.districts.length; n++) {
                          if (member.district == $scope.districts[n].id) {
                              member.Sitename = $scope.districts[n];
                              break;
                          }
                      }
                      member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                      $scope.TotalTodos = [];
                      $scope.TotalTodos.push(member);
                  });
              });
          });
      }
      $scope.DateFilter = function () {
          $scope.backgroundCol1 = "";
          $scope.backgroundCol2 = "blue";
          $scope.emps = Restangular.all('stakeholdertypes').getList().then(function (emps) {
              $scope.stakeholdertypes = emps;
              $scope.zn = Restangular.all('stakeholders?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=datetime%20ASC').getList().then(function (zn) {
                  $scope.stakeholders = zn;
                  $scope.modalInstanceLoad.close();
                  angular.forEach($scope.stakeholders, function (member, index) {
                      member.index = index + 1;
                      member.stakeholdertypes = $scope.getStakeholdertype(member.stakeholdertype);
                      for (var n = 0; n < $scope.districts.length; n++) {
                          if (member.district == $scope.districts[n].id) {
                              member.Sitename = $scope.districts[n];
                              break;
                          }
                      }
                      member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                      $scope.TotalTodos = [];
                      $scope.TotalTodos.push(member);
                  });
              });
          });
      }*/
    $scope.UserLanguage = $window.sessionStorage.language;
    $scope.organisebystakeholders = Restangular.all('organisebystakeholders?filter[where][deleteflag]=false').getList().$object;
    $scope.todostatuses1 = Restangular.all('todostatuses?filter={"where": {"id": {"inq": [1,3]}}}').getList().then(function (responseseservity) {
        $scope.todostatuses = responseseservity;
        $scope.newtodo.status = 1;
    });
    $scope.emps = Restangular.all('stakeholdertypes').getList().then(function (emps) {
        $scope.stakeholdertypes = emps;
        $scope.zn = Restangular.all('stakeholders?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=datetime%20ASC').getList().then(function (zn) {
            $scope.stakeholders = zn;
            $scope.modalInstanceLoad.close();
            angular.forEach($scope.stakeholders, function (member, index) {
                member.index = index + 1;
                //member.stakeholdertypes = $scope.getStakeholdertype(member.stakeholdertype);
                for (var n = 0; n < $scope.districts.length; n++) {
                    if (member.district == $scope.districts[n].id) {
                        member.Sitename = $scope.districts[n];
                        member.filterDate = new Date(member.datetime.split("T")[0]);
                        break;
                    }
                }

                for (var n = 0; n < $scope.stakeholdertypes.length; n++) {
                    if (member.stakeholdertype == $scope.stakeholdertypes[n].id) {
                        member.StakeholdertypeName = $scope.stakeholdertypes[n].name;
                        break;
                    }
                }
                member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });
    });
    $scope.getStakeholdertype = function (typ) {
        return Restangular.all('stakeholdertypes?filter={"where":{"id":{"inq":[' + typ + ']}}}').getList().$object;
    }

    /********************/
    /* $scope.sortingOrder = sortingOrder;
     $scope.reverse = false;

     $scope.sort_by = function (newSortingOrder) {
         if ($scope.sortingOrder == newSortingOrder)
             $scope.reverse = !$scope.reverse;

         $scope.sortingOrder = newSortingOrder;

     };*/

    $scope.sort = {
        active: '',
        descending: undefined
    }

    $scope.changeSorting = function (column) {

        var sort = $scope.sort;

        if (sort.active == column) {
            sort.descending = !sort.descending;

        } else {
            sort.active = column;
            sort.descending = false;
        }
    };

    /*$scope.getIcon = function (column) {
        var sort = $scope.sort;
        if (sort.active == column) {
            return sort.descending ? 'glyphicon-chevron-up' : 'glyphicon-chevron-down';
        }
    }*/

    $scope.getIcon = function (column) {

        var sort = $scope.sort;

        if (sort.active == column) {
            return sort.descending ? 'fa-sort-up' : 'fa-sort-desc';
        }
    }


    /*****************/
   
    /************************************** Create ********************************************/
    $scope.purposeofmeetings = Restangular.all('purposeofmeetings').getList().$object;
    $scope.followupstacks = Restangular.all('followupstacks').getList().$object;
    $scope.partners = Restangular.all('followupstacks').getList().$object;
    $scope.DisableFollowup = false;
    if ($window.sessionStorage.roleId == 5) {
        $scope.fieldworkersite = true;
        $scope.cosite = false;
        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
            $scope.comemberid = comember.id;
            $scope.stakeholder.facilityId = comember.id;
            $scope.auditlog.facilityId = comember.id;
            $scope.getfacilityId = comember.id;
            Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {
                //$scope.routes = Restangular.all('distribution-routes?filter[where][partnerId]=' + employee.id + '&filter[where][deleteflag]=false').getList().$object;
            });
            //$scope.modalInstanceLoad.close();
        });
        $scope.districts = Restangular.all('sales-areas?filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][deleteflag]=false').getList().$object;
    } else {
        $scope.fieldworkersite = false;
        $scope.disableapprove = true;
        $scope.cosite = true;
        Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
            $scope.getsiteid = fw.id;
            Restangular.one('comembers', fw.facility).get().then(function (comember) {
                $scope.comemberid = comember.id;
                $scope.stakeholder.facilityId = comember.id;
                $scope.auditlog.facilityId = comember.id;
                $scope.getfacilityId = comember.id;
            });
            console.log('$scope.getsiteid', $scope.getsiteid);
            Restangular.all('routelinks?filter[where][partnerId]=' + $scope.getsiteid).getList().then(function (routeResponse) {
                $scope.routelinksget = routeResponse;
                angular.forEach($scope.routelinksget, function (member, index) {
                    member.index = index + 1;
                    member.routeName = Restangular.one('distribution-routes', member.distributionRouteId).get().$object;
                });
                $scope.getrouteid = routeResponse.distributionRouteId;
                //$scope.modalInstanceLoad.close();
            });
        });
    }
    $scope.$watch('stakeholder.district', function (newValue, oldValue) {
        //console.log('newValue', newValue);
        if (newValue === oldValue || newValue == '') {
            return;
        } else {
            //$scope.towns = Restangular.all('cities?filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').$object;
            $scope.towns1 = Restangular.all('cities?filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (resp) {
                //console.log('resp', resp);
                $scope.towns = resp;
            });
        }
    });
    /************************************* SAVE *******************************************/
    $scope.stakeholderdataModal = false;
    $scope.validatestring = '';
    //console.log('$scope.stakeholder.stakeholdertyp', $scope.stakeholder.stakeholdertyp);
    $scope.Savestakeholder = function () {
        //$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.stakeholder.meetingpurpose = null;
        $scope.stakeholder.followup = null;
        $scope.stakeholder.stakeholdertype = null;
        if ($scope.stakeholder.datetime == '' || $scope.stakeholder.datetime == null) {
            $scope.validatestring = $scope.validatestring + $scope.seldate;
        } else if ($scope.stakeholder.stakeholdertyp == '' || $scope.stakeholder.stakeholdertyp == null || $scope.stakeholder.stakeholdertyp == undefined) {
            $scope.validatestring = $scope.validatestring + $scope.pstakeholdtype;
        } else if ($scope.stakeholder.district == '' || $scope.stakeholder.district == null) {
            $scope.validatestring = $scope.validatestring + $scope.seldisctict;
            /*} else if ($scope.stakeholder.city == '' || $scope.stakeholder.city == null) {
				$scope.validatestring = $scope.validatestring + 'Please Select Town';
		      */
        } else if ($scope.stakeholder.organisedby == '' || $scope.stakeholder.organisedby == null) {
            $scope.validatestring = $scope.validatestring + $scope.selorgby;
        } else
        if ($scope.stakeholder.follow != 'None') {
            for (var i = 0; i < $scope.stakeholder.follow.length; i++) {
                if (i == 0) {
                    $scope.stakeholder.followup = $scope.stakeholder.follow[i];
                } else {
                    $scope.stakeholder.followup = $scope.stakeholder.followup + ',' + $scope.stakeholder.follow[i];
                }
            }
        } else {
            $scope.stakeholder.followup = null;
        }
        if ($scope.stakeholder.meeting != undefined) {
            for (var i = 0; i < $scope.stakeholder.meeting.length; i++) {
                if (i == 0) {
                    $scope.stakeholder.meetingpurpose = $scope.stakeholder.meeting[i];
                } else {
                    $scope.stakeholder.meetingpurpose = $scope.stakeholder.meetingpurpose + ',' + $scope.stakeholder.meeting[i];
                }
            }
        }
        if ($scope.stakeholder.stakeholdertyp != undefined) {
            for (var i = 0; i < $scope.stakeholder.stakeholdertyp.length; i++) {
                if (i == 0) {
                    $scope.stakeholder.stakeholdertype = $scope.stakeholder.stakeholdertyp[i];
                } else {
                    $scope.stakeholder.stakeholdertype = $scope.stakeholder.stakeholdertype + ',' + $scope.stakeholder.stakeholdertyp[i];
                }
            }
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
            //$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        } else {
            $scope.openOneAlert();
            Restangular.all('stakeholders').post($scope.stakeholder).then(function (resp) {
                console.log('resp', resp);
                $scope.auditlog.entityid = resp.id;
                var respdate = $filter('date')(resp.datetime, 'dd-MMMM-yyyy');
                $scope.auditlog.description = 'Stake Holder Created With Following Details:' + 'Stakeholder Type - ' + resp.stakeholdertype + ' , ' + 'Purpose of meeting - ' + resp.meetingpurpose + ' , ' + 'City - ' + resp.city + ' , ' + 'Organized By - ' + resp.organisedby + ' , ' + 'Follow Up - ' + resp.follow + ' , ' + 'Date - ' + respdate;
                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                    //console.log('responseaudit', responseaudit);
                    if ($scope.stakeholder.follow == 'None') {
                        //window.location = '/stakeholdermeetings';
                    }
                    $scope.SaveToDo();
                });
            }, function (response) {
                console.log('response', response);
            });
        }
    };
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    /*********************************************************************************/
    /*
		$scope.stakeholder.follow.push('None');
		$scope.showfollowupModal = false;
		$scope.$watch('stakeholder.follow', function (newValue, oldValue) {
			$scope.removeTodo = true;
			$scope.newtodo = {};
			$scope.newtodo.status = 1;
			var sevendays = new Date();
			sevendays.setDate(sevendays.getDate() + 7);
			$scope.mindt = sevendays;
			$scope.newtodo.datetime = sevendays;
			$scope.oldFollowUp = oldValue;
			$scope.newFollow = newValue;
			if (newValue === oldValue) {
				return;
			} else {
				$scope.SavetodoFollow = function () {
					$scope.newtodo.todotype = 31;
					$scope.newtodo.state = $window.sessionStorage.zoneId;
					$scope.newtodo.district = $window.sessionStorage.salesAreaId;
					$scope.newtodo.facility = $window.sessionStorage.coorgId;
					$scope.newtodo.facilityId = $scope.getfacilityId;
					$scope.newtodo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
					$scope.newtodo.lastmodifiedtime = new Date();
					$scope.newtodo.site = $scope.stakeholder.site;

					$scope.newtodo.purpose = $scope.printpurposeid;
					$scope.newtodo.stakeholdertype = $scope.sttype;
					$scope.newtodo.roleId = $window.sessionStorage.roleId;
					$scope.addedtodos.push($scope.newtodo);		
					$scope.showfollowupModal = !$scope.showfollowupModal;

					for (var index = 0; index < $scope.followupstacks.length; index++) {
						if ($scope.stakeholder.follow[index] === 'None') {
							$scope.stakeholder.follow.splice(index, 1);
						}
					}
				};

				var array3 = newValue.filter(function (obj) {
					return oldValue.indexOf(obj) == -1;
				});
				if (array3.length > 0) {
					//console.log('unique', array3);
					for (var i = 0; i < $scope.followupstacks.length; i++) {
						if ($scope.followupstacks[i].id == array3[0]) {
							$scope.printpurpose = $scope.followupstacks[i];
							$scope.printpurposeid = $scope.followupstacks[i].id;
						}
					}
				}
				if (oldValue != undefined) {
					if (oldValue.length < newValue.length) {
						$scope.showfollowupModal = !$scope.showfollowupModal;
					}
				} else {
					$scope.showfollowupModal = !$scope.showfollowupModal;
				}
			};
		});
*/
    $scope.stakeholder.follow.push('None');
    $scope.showfollowupModal = false;
    $scope.$watch('stakeholder.follow', function (newValue, oldValue) {
        $scope.removeTodo = true;
        $scope.newtodo = {};
        $scope.newtodo.status = 1;
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        $scope.mindt = sevendays;
        $scope.newtodo.datetime = sevendays;
        $scope.oldFollowUp = oldValue;
        $scope.newFollow = newValue;
        if (newValue === oldValue) {
            return;
        } else {
            $scope.SavetodoFollow = function () {
                $scope.newtodo.todotype = 31;
                $scope.newtodo.state = $window.sessionStorage.zoneId;
                $scope.newtodo.district = $window.sessionStorage.salesAreaId;
                $scope.newtodo.facility = $window.sessionStorage.coorgId;
                $scope.newtodo.facilityId = $scope.getfacilityId;
                $scope.newtodo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.newtodo.lastmodifiedtime = new Date();
                $scope.newtodo.site = $scope.stakeholder.site;
                $scope.newtodo.purpose = $scope.printpurposeid;
                $scope.newtodo.stakeholdertype = $scope.sttype;
                $scope.newtodo.roleId = $window.sessionStorage.roleId;
                $scope.addedtodos.push($scope.newtodo);
                //console.log('$scope.addedtodos', $scope.addedtodos);
                $scope.showfollowupModal = !$scope.showfollowupModal;
                for (var index = 0; index < $scope.followupstacks.length; index++) {
                    if ($scope.stakeholder.follow[index] === 'None') {
                        $scope.stakeholder.follow.splice(index, 1);
                    }
                }
            };
            var array3 = newValue.filter(function (obj) {
                return oldValue.indexOf(obj) == -1;
            });
            if (array3.length > 0) {
                //console.log('unique', array3);
                for (var i = 0; i < $scope.followupstacks.length; i++) {
                    if ($scope.followupstacks[i].id == array3[0]) {
                        $scope.printpurpose = $scope.followupstacks[i];
                        $scope.printpurposeid = $scope.followupstacks[i].id;
                    }
                }
            }
            if (oldValue != undefined) {
                if (oldValue.length < newValue.length) {
                    //console.log('$scope.addedtodos', $scope.addedtodos.length);
                    var Array1 = newValue;
                    var Array2 = oldValue;
                    for (var i = 0; i < Array2.length; i++) {
                        var arrlen = Array1.length;
                        for (var j = 0; j < arrlen; j++) {
                            if (Array2[i] == Array1[j]) {
                                //console.log('Match', Array2[i], Array1[j]);
                                Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                                //console.log('Array1', Array1[0]);
                                //console.log('Array2', Array2);
                            }
                        }
                    }
                    for (var i = 0; i < $scope.addedtodos.length; i++) {
                        if ($scope.addedtodos[i].purpose == Array1[0]) {
                            $scope.addedtodos.slice($scope.addedtodos[i], 1);
                            //console.log('$scope.addedtodos.splice(i, 1)', $scope.addedtodos.splice($scope.addedtodos[i], 1));
                            //console.log('$scope.addedtodos[i].purposed', $scope.addedtodos[i].purpose);
                            //console.log('Array1[0]', Array1[0]);
                        }
                    }
                    $scope.showfollowupModal = !$scope.showfollowupModal;
                } else if (newValue.length < oldValue.length) {
                    // something was removed
                    var Array1 = oldValue;
                    var Array2 = newValue;
                    for (var i = 0; i < Array2.length; i++) {
                        var arrlen = Array1.length;
                        for (var j = 0; j < arrlen; j++) {
                            if (Array2[i] == Array1[j]) {
                                Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                                //console.log('Array111', Array1[0]);
                            }
                        }
                    }
                    for (var i = 0; i < $scope.addedtodos.length; i++) {
                        if ($scope.addedtodos[i].id == Array1[0]) {
                            $scope.addedtodos.slice($scope.addedtodos[i], 1);
                        }
                    }
                }
            } else {
                $scope.showfollowupModal = !$scope.showfollowupModal;
            }
        };
    });
    /*****************************************************************/
    $scope.todocount = 0;
    /*if ($scope.stakeholder.follow == 'None') {
        $scope.SaveToDo = function () {
            for (var i = 0; i < $scope.addedtodos.length; i++) {
                //$scope.submittodos.post($scope.addedtodos[i]).then(function (resp) {
                Restangular.all('todos').post($scope.addedtodos[i]).then(function (resp) {
                    window.location = '/stakeholdermeetings';
                });
            };
        };
    } else {
        window.location = '/stakeholdermeetings';
    };*/
    //if ($scope.stakeholder.follow != 'None') {
    //	console.log('I m calling');
    $scope.totalStakeholderTodos = [];
    $scope.SaveToDo = function () {
        console.log("$scope.addedtodos", $scope.addedtodos);
        console.log("$scope.stakeholder.stakeholdertyp", $scope.stakeholder.stakeholdertyp);
        console.log("$scope.stakeholder.meeting", $scope.stakeholder.meeting);
        for (var i = 0; i < $scope.addedtodos.length; i++) {
            //console.log("$scope.addedtodos  "+i, $scope.addedtodos[i]);
            //$scope.totalTodo={};
            //$scope.totalTodo = $scope.addedtodos[i];
            if ($scope.stakeholder.meeting != undefined && $scope.stakeholder.meeting.length > 0) {
                for (var j = 0; j < $scope.stakeholder.stakeholdertyp.length; j++) {
                    //console.log("$scope.stakeholder.stakeholdertyp  "+j, $scope.stakeholder.stakeholdertyp[j]);
                    for (var k = 0; k < $scope.stakeholder.meeting.length; k++) {
                        //console.log("$scope.stakeholder.stakeholdertyp  " + j, $scope.stakeholder.stakeholdertyp[j]);
                        //console.log("$scope.stakeholder.meeting  " + k, $scope.stakeholder.meeting[k]);
                        $scope.totalTodo = {};
                        $scope.totalTodo = Restangular.copy($scope.addedtodos[i]);
                        $scope.totalTodo.stakeholdertype = $scope.stakeholder.stakeholdertyp[j];
                        $scope.totalTodo.meetingpurpose = $scope.stakeholder.meeting[k];
                        $scope.totalStakeholderTodos.push($scope.totalTodo);
                        //console.log("$scope.totalTodo", $scope.totalTodo);
                    }
                }
            } else {
                for (var j = 0; j < $scope.stakeholder.stakeholdertyp.length; j++) {
                    //console.log("$scope.stakeholder.stakeholdertyp  "+j, $scope.stakeholder.stakeholdertyp[j]);
                    //console.log("$scope.stakeholder.stakeholdertyp  " + j, $scope.stakeholder.stakeholdertyp[j]);
                    //console.log("$scope.stakeholder.meeting  " + k, $scope.stakeholder.meeting[k]);
                    $scope.totalTodo = {};
                    $scope.totalTodo = Restangular.copy($scope.addedtodos[i]);
                    $scope.totalTodo.stakeholdertype = $scope.stakeholder.stakeholdertyp[j];
                    $scope.totalTodo.meetingpurpose = "";
                    $scope.totalStakeholderTodos.push($scope.totalTodo);
                    //console.log("$scope.totalTodo", $scope.totalTodo);
                }
            }
        }
        //console.log("$scope.addedtodos  ", $scope.addedtodos);
        //console.log("$scope.totalStakeholderTodos", $scope.totalStakeholderTodos);
        if ($scope.totalStakeholderTodos.length > 0) {
            $scope.submitTodo();
        } else {
            window.location = '/stakeholdermeetings';
        }
    }
    $scope.submitTodoCount = 0;
    $scope.submitTodo = function () {
        console.log("submitTodo", "submitTodo");
        if ($scope.submitTodoCount < $scope.totalStakeholderTodos.length) {
            Restangular.all('todos').post($scope.totalStakeholderTodos[$scope.submitTodoCount]).then(function (resp) {
                console.log('resp', resp);
                $scope.submitTodoCount++;
                $scope.submitTodo();
            });
        } else {
            window.location = '/stakeholdermeetings';
        }
    }
    $scope.CancelFollow = function () {
        $scope.stakeholder.follow = $scope.oldFollowUp;
        $scope.showfollowupModal = !$scope.showfollowupModal;
    };
    //Datepicker settings start
    $scope.stakeholder.datetime = new Date();
    $scope.today = function () {
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
    };
    $scope.dtmin = new Date();
    $scope.today();
    $scope.dtmax = new Date();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.CurrentPast = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickercurpast' + index).focus();
        });
        $scope.picker.currpastopened = true;
    };
    $scope.followdt = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerfollowup' + index).focus();
        });
        $scope.picker.followupopened = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };
    $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end
    /***************************** Language *******************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.date = langResponse.date;
        $scope.name = langResponse.name;
        $scope.action = langResponse.action;
        $scope.searchmember = langResponse.searchmember;
        $scope.organizedby = langResponse.organizedby;
        $scope.followuprequired = langResponse.followuprequired;
        $scope.membername = langResponse.membername;
        $scope.status = langResponse.status;
        $scope.followupdate = langResponse.followupdate;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.site = langResponse.site;
        $scope.stakeholdertype = langResponse.stakeholdertype;
        $scope.stakeholdermeeting = langResponse.stakeholdermeeting;
        $scope.show = langResponse.show;
        $scope.entry = langResponse.entry;
        $scope.searchfor = langResponse.searchfor;
        $scope.addnew = langResponse.addnew;
        $scope.purposeofmeeting = langResponse.purposeofmeeting;
        $scope.savebutton = langResponse.savebutton;
        $scope.district = langResponse.district;
        $scope.town = langResponse.town;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;

        $scope.stakeholdcreated = langResponse.stakeholdcreated;
        //$scope.stakeholdupdated = langResponse.stakeholdupdated;
        $scope.modalTitle = langResponse.thankyou;
        $scope.message = langResponse.stakeholdcreated;
        $scope.pstakeholdtype = langResponse.stakeholdtype;
        $scope.seldisctict = langResponse.seldisctict;
        $scope.selorgby = langResponse.selorgby;
        $scope.seldate = langResponse.seldate;

    });
    $scope.openOneAlert = function () {
        console.log('Me Calling')
        $scope.modalOneAlert = $modal.open({
            animation: true,
            templateUrl: 'template/AlertModal.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            windowClass: 'modal-sucess '
        });
    };
    if ($window.sessionStorage.language == 1) {
        $scope.followupmemberTitle = 'Follow Up Member';
    } else if ($window.sessionStorage.language == 2) {
        $scope.followupmemberTitle = 'ऊपर का सदस्य का पालन करें';
    } else if ($window.sessionStorage.language == 3) {
        $scope.followupmemberTitle = 'ಮೆಂಬರ್  ಅನುಸರಣ';
    } else if ($window.sessionStorage.language == 4) {
        $scope.followupmemberTitle = 'மெம்பர் பின்தொடர்';
    } else if ($window.sessionStorage.language == 5) {
        $scope.followupmemberTitle = 'సభ్యుని అనుసరించండ';
    } else if ($window.sessionStorage.language == 6) {
        $scope.followupmemberTitle = 'प्रतिनिधी सदस्य अनुसरण करा';
    }
});
