'use strict';

angular.module('secondarySalesApp')
    .controller('SchoolCheckListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {


        /************************ Language ************************/

        Restangular.one('schoollanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.schoolLanguage = langResponse[0];
            $scope.headingName = $scope.schoolLanguage.houseHoldCreate;
            $scope.modalTitle = $scope.schoolLanguage.thankYou;
            $scope.message = $scope.schoolLanguage.thankYouCreated;
        });

        /************* Focus ***************************************/

        /************* Focus ***************************************/

        $scope.someFocusVariable = true;
        $scope.FocusMe = true;

        $scope.usersdisplay = Restangular.all('users?filter[where][deleteFlag]=false').getList().$object;

        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;

        $scope.statedisplay = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;

        $scope.districtdsply = Restangular.all('districts?filter[where][deleteFlag]=false').getList().$object;

        $scope.sitedsply = Restangular.all('sites?filter[where][deleteFlag]=false').getList().$object;

        $scope.roledsply = Restangular.all('roles').getList().$object;

        $scope.dynamiclistDis = Restangular.all('dynamiclists?filter[where][deleteFlag]=false').getList().$object;


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/schoolchecklist-list") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            // console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************** List **********/

        $scope.obj = {};

        $scope.obj.status = 'active';

        $scope.$watch('obj.status', function (newValue, oldValue) {
            // console.log('newValue', newValue);
            // console.log('oldValue', oldValue);
            if (newValue === '' || newValue === null) {
                return;
            } else {

                Restangular.all('users?filter[where][deleteFlag]=false').getList().then(function (user) {

                    $scope.users = user;
                    Restangular.all('schoolrecords?filter[where][deleteFlag]=false').getList().then(function (school) {
                        $scope.schoolrecordsply = school;
                        Restangular.all('schoolchecklists?filter[where][deleteFlag]=false').getList().then(function (schoolcheckRespose) {
                            //  console.log('schoolRespose', schoolRespose);

                            $scope.schoolchecklists = schoolcheckRespose;

                            Restangular.all('schoolchecklisttrailers?filter[where][deleteFlag]=false').getList().then(function (check) {

                                $scope.checklisttrailer = check;

                                $scope.modalInstanceLoad.close();

                                angular.forEach($scope.schoolchecklists, function (member, index) {

                                    $scope.myCheckArray = [];

                                    for (var mi = 0; mi < $scope.checklisttrailer.length; mi++) {
                                        if (member.id == $scope.checklisttrailer[mi].schoolheaderId) {
                                            $scope.myCheckArray.push($scope.checklisttrailer[mi]);
                                        }
                                    }

                                    var data11 = $scope.users.filter(function (arr) {
                                        return arr.id == member.associatedHF
                                    })[0];

                                    if (data11 != undefined) {
                                        member.associateDisp = data11.name;
                                    }

                                    var data12 = $scope.schoolrecordsply.filter(function (arr) {
                                        return arr.id == member.schoolName
                                    })[0];

                                    if (data12 != undefined) {
                                        member.schoolNameDis = data12.schoolname;
                                    }

                                    var data13 = $scope.countrydisplay.filter(function (arr) {
                                        return arr.id == member.countryId
                                    })[0];

                                    if (data13 != undefined) {
                                        member.countryname = data13.name;
                                    }

                                    var data14 = $scope.statedisplay.filter(function (arr) {
                                        return arr.id == member.stateId
                                    })[0];

                                    if (data14 != undefined) {
                                        member.statename = data14.name;
                                    }

                                    var data15 = $scope.districtdsply.filter(function (arr) {
                                        return arr.id == member.districtId
                                    })[0];

                                    if (data15 != undefined) {
                                        member.districtname = data15.name;
                                    }

                                    var data16 = $scope.sitedsply.filter(function (arr) {
                                        return arr.id == member.siteId
                                    })[0];

                                    if (data16 != undefined) {
                                        member.sitename = data16.name;
                                    }

                                    var data17 = $scope.usersdisplay.filter(function (arr) {
                                        return arr.id == member.associatedHF
                                    })[0];

                                    if (data17 != undefined) {
                                        member.assignedTo = data17.name;
                                    }

                                    var data18 = $scope.usersdisplay.filter(function (arr) {
                                        return arr.id == member.lastModifiedBy
                                    })[0];

                                    if (data18 != undefined) {
                                        member.lastModifiedByname = data18.username;
                                    }

                                    var data19 = $scope.roledsply.filter(function (arr) {
                                        return arr.id == member.lastModifiedByRole
                                    })[0];

                                    if (data19 != undefined) {
                                        member.lastModifiedByRolename = data19.name;
                                    }
                                    var data20 = $scope.roledsply.filter(function (arr) {
                                        return arr.id == member.createdByRole
                                    })[0];

                                    if (data20 != undefined) {
                                        member.createdByRolename = data20.name;
                                    }

                                    var data21 = $scope.usersdisplay.filter(function (arr) {
                                        return arr.id == member.createdBy
                                    })[0];

                                    if (data21 != undefined) {
                                        member.createdByname = data21.username;
                                    }
                                    member.toiletinschool = '';
                                    member.toiletforboygirl = '';
                                    member.drinkingwtrsource = '';
                                    member.qtyofwtravialabel = '';
                                    member.qualityofwater = '';
                                    member.middaymeal = '';


                                    for (var aa = 0; aa < $scope.myCheckArray.length; aa++) {

                                        if ($scope.myCheckArray[aa].checklistId == 3) {

                                            var data12 = $scope.dynamiclistDis.filter(function (arr) {
                                                return arr.id == 3
                                            })[0];

                                            if (data12 != undefined) {
                                                var mydata = data12.option.split(',');

                                                $scope.foo = [];
                                                var count = 0;

                                                for (var i = 0; i < mydata.length; i++) {

                                                    $scope.foo.push({
                                                        seq: i + 1,
                                                        value: mydata[i]
                                                    });

                                                    count++;

                                                    if (count == mydata.length) {
                                                        var data22 = $scope.foo.filter(function (arr) {
                                                            return arr.seq == $scope.myCheckArray[aa].option
                                                        })[0];

                                                        if (data22 != undefined) {
                                                            member.toiletinschool = data22.value;
                                                            //console.log(member.toiletinschool);
                                                        }
                                                    }
                                                }
                                            }
                                        } else if ($scope.myCheckArray[aa].checklistId == 4) {

                                            var data12 = $scope.dynamiclistDis.filter(function (arr) {
                                                return arr.id == 4
                                            })[0];

                                            if (data12 != undefined) {
                                                var mydata = data12.option.split(',');

                                                $scope.foo = [];
                                                var count = 0;

                                                for (var i = 0; i < mydata.length; i++) {

                                                    $scope.foo.push({
                                                        seq: i + 1,
                                                        value: mydata[i]
                                                    });

                                                    count++;

                                                    if (count == mydata.length) {
                                                        var data22 = $scope.foo.filter(function (arr) {
                                                            return arr.seq == $scope.myCheckArray[aa].option
                                                        })[0];

                                                        if (data22 != undefined) {
                                                            member.toiletforboygirl = data22.value;
                                                           // console.log(member.toiletforboygirl);
                                                        }
                                                    }
                                                }
                                            }
                                        } else if ($scope.myCheckArray[aa].checklistId == 5) {

                                            var data12 = $scope.dynamiclistDis.filter(function (arr) {
                                                return arr.id == 5
                                            })[0];

                                            if (data12 != undefined) {
                                                var mydata = data12.option.split(',');

                                                $scope.foo = [];
                                                var count = 0;

                                                for (var i = 0; i < mydata.length; i++) {

                                                    $scope.foo.push({
                                                        seq: i + 1,
                                                        value: mydata[i]
                                                    });

                                                    count++;

                                                    if (count == mydata.length) {
                                                        var data22 = $scope.foo.filter(function (arr) {
                                                            return arr.seq == $scope.myCheckArray[aa].option
                                                        })[0];

                                                        if (data22 != undefined) {
                                                            member.drinkingwtrsource = data22.value;
                                                           // console.log(member.drinkingwtrsource);
                                                        }
                                                    }
                                                }
                                            }
                                        } else if ($scope.myCheckArray[aa].checklistId == 6) {

                                            var data12 = $scope.dynamiclistDis.filter(function (arr) {
                                                return arr.id == 6
                                            })[0];

                                            if (data12 != undefined) {
                                                var mydata = data12.option.split(',');

                                                $scope.foo = [];
                                                var count = 0;

                                                for (var i = 0; i < mydata.length; i++) {

                                                    $scope.foo.push({
                                                        seq: i + 1,
                                                        value: mydata[i]
                                                    });

                                                    count++;

                                                    if (count == mydata.length) {
                                                        var data22 = $scope.foo.filter(function (arr) {
                                                            return arr.seq == $scope.myCheckArray[aa].option
                                                        })[0];

                                                        if (data22 != undefined) {
                                                            member.qtyofwtravialabel = data22.value;
                                                          //  console.log(member.qtyofwtravialabel);
                                                        }
                                                    }
                                                }
                                            }
                                        } else if ($scope.myCheckArray[aa].checklistId == 7) {

                                            var data12 = $scope.dynamiclistDis.filter(function (arr) {
                                                return arr.id == 7
                                            })[0];

                                            if (data12 != undefined) {
                                                var mydata = data12.option.split(',');

                                                $scope.foo = [];
                                                var count = 0;

                                                for (var i = 0; i < mydata.length; i++) {

                                                    $scope.foo.push({
                                                        seq: i + 1,
                                                        value: mydata[i]
                                                    });

                                                    count++;

                                                    if (count == mydata.length) {
                                                        var data22 = $scope.foo.filter(function (arr) {
                                                            return arr.seq == $scope.myCheckArray[aa].option
                                                        })[0];

                                                        if (data22 != undefined) {
                                                            member.qualityofwater = data22.value;
                                                           // console.log(member.qualityofwater);
                                                        }
                                                    }
                                                }
                                            }
                                        } else if ($scope.myCheckArray[aa].checklistId == 8) {

                                            var data12 = $scope.dynamiclistDis.filter(function (arr) {
                                                return arr.id == 8
                                            })[0];

                                            if (data12 != undefined) {
                                                var mydata = data12.option.split(',');

                                                $scope.foo = [];
                                                var count = 0;

                                                for (var i = 0; i < mydata.length; i++) {

                                                    $scope.foo.push({
                                                        seq: i + 1,
                                                        value: mydata[i]
                                                    });

                                                    count++;

                                                    if (count == mydata.length) {
                                                        var data22 = $scope.foo.filter(function (arr) {
                                                            return arr.seq == $scope.myCheckArray[aa].option
                                                        })[0];

                                                        if (data22 != undefined) {
                                                            member.middaymeal = data22.value;
                                                          //  console.log(member.middaymeal);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    member.dateofLastcheckDis = $filter('date')(member.dateofLastcheck, 'dd-MMM-yyyy');
                                    member.createdDate = $filter('date')(member.createdDate, 'dd/MM/yyyy');
                                    member.lastModifiedDate = $filter('date')(member.lastModifiedDate, 'dd/MM/yyyy');

                                    member.index = index + 1;
                                    // console.log('member', member);
                                });
                            });
                        });
                    });
                });
            }
        });

        /**************************Export data to excel sheet ***************/

        $scope.DisableExport = false;
        $scope.valschoolCount = 0;

        $scope.exportData = function () {
            $scope.schoolCheckArray = [];
            $scope.valschoolCount = 0;
            if ($scope.schoolchecklists.length == 0) {
                alert('No data found');
            } else {
                for (var c = 0; c < $scope.schoolchecklists.length; c++) {
                    $scope.schoolCheckArray.push({
                        'Sr No': $scope.schoolchecklists[c].index,
                        'COUNTRY': $scope.schoolchecklists[c].countryname,
                        'STATE': $scope.schoolchecklists[c].statename,
                        'DISTRICT': $scope.schoolchecklists[c].districtname,
                        'SITE': $scope.schoolchecklists[c].sitename,
                        'NAME OF THE SCHOOL': $scope.schoolchecklists[c].schoolNameDis,
                        'DATE': $scope.schoolchecklists[c].dateofLastcheckDis,
                        'ASSIGNED TO': $scope.schoolchecklists[c].associateDisp,
                        'TOILETS IN SCHOOL PERMISE': $scope.schoolchecklists[c].toiletinschool,
                        'SEPERATE TOILETS FOR BOYS AND GIRLS  ': $scope.schoolchecklists[c].toiletforboygirl,
                        'SOURCE OF DRINKING WATER': $scope.schoolchecklists[c].drinkingwtrsource,
                        'QUANTITY OF WATER AVAILABLE PER DAY': $scope.schoolchecklists[c].qtyofwtravialabel,
                        'QUALITY OF WATER': $scope.schoolchecklists[c].qualityofwater,
                        'SOURCE OF WATER FOR MID DAY MEAL': $scope.schoolchecklists[c].middaymeal,
                        'CREATED BY': $scope.schoolchecklists[c].createdByname,
                        'CREATED DATE': $scope.schoolchecklists[c].createdDate,
                        'CREATED ROLE': $scope.schoolchecklists[c].createdByRolename,
                        'LAST MODIFIED BY': $scope.schoolchecklists[c].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.schoolchecklists[c].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.schoolchecklists[c].lastModifiedByRolename,
                        'DELETE FLAG': $scope.schoolchecklists[c].deleteFlag
                    });

                    $scope.valschoolCount++;
                    if ($scope.schoolchecklists.length == $scope.valschoolCount) {
                        alasql('SELECT * INTO XLSX("schoolchecklists.xlsx",{headers:true}) FROM ?', [$scope.schoolCheckArray]);
                    }
                }
            }
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

    });
