'use strict';
angular.module('secondarySalesApp').controller('cobudgetsViewNew2Ctrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $modal, $timeout) {
    $scope.isCreateView = true;

    Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=false').getList().then(function (Allspmres) {
        $scope.yearArray = [];
        for (var b = 0; b < Allspmres.length; b++) {
            $scope.yearArray.push(Allspmres[b].year);
        }
        //console.log('$scope.yearArray',$scope.yearArray)
        //"{\"where\":{\"or\":[{\"and\":[{\"site\":{\"inq\":["+mFieldWorker.getSiteAssigned()+"]}},{\"deleteflag\":false}]},{\"stress_data\":true}]}}"
        //{"where":{"and":[{"id":{"inq":["3,2,1"]}},{"order":"name ASC"}]}}
        //"{"where":{"and":[{"id":{"inq":["+$scope.yearArray+"]}},{"order":"name ASC"}]}}"
        Restangular.all('spmbudgetyears?filter={"where":{"id":{"inq":[' + $scope.yearArray + ']}}}').getList().then(function (cobyearRes) {
            $scope.spmbudgetyears = cobyearRes;
            $scope.spmbudgetyears.sort(function (a, b) {
                var nameA = a.name.toLowerCase(),
                    nameB = b.name.toLowerCase()
                if (nameA < nameB) //sort string ascending
                    return -1
                if (nameA > nameB)
                    return 1
                return 0 //default return value (no sorting)
            });

        });
    });

    $scope.YearFunction = function (selyr) {
        //console.log("selyr", selyr);
        if (selyr === undefined || selyr === null || selyr === '') {
            return;
        } else {
            $scope.current_YEAR = selyr;
            console.log('selyr', selyr);
            //console.log('$window.sessionStorage.UserEmployeeId', $window.sessionStorage.UserEmployeeId)
            Restangular.one('spmbudgetyears', selyr).get().then(function (yearResa) {
                $scope.Current_Year = yearResa.name;
            });
            Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][year]=' + selyr).getList().then(function (res) {
                $scope.spmBudget = res[0].totalamount;
                $scope.spmBudgetAmount = res[0].balancetilldate;
                $scope.spmBudgetId = res[0].id;
                $scope.Balance_amount = $scope.spmBudget - $scope.Total_CO_HumanResource;
                //console.log('$scope.spmBudget', res[0].humanresources);

                $scope.spmhumanresources = res[0].humanresources;
                $scope.spmtravelandcommunication = res[0].travelandcommunication;
                $scope.spmgenericactivity = res[0].genericactivity;
                $scope.spmspecificactivity = res[0].specificactivity;
                $scope.spmmsgtgactivity = res[0].msgtgactivity;
                $scope.spmadministrativeexpense = res[0].administrativeexpense;
                $scope.spmmonitoringevaluation = res[0].monitoringevaluation;
                $scope.spmbudget_total = res[0].humanresources + res[0].travelandcommunication + res[0].genericactivity + res[0].specificactivity + res[0].msgtgactivity + res[0].administrativeexpense + res[0].monitoringevaluation;


            });
            // console.log('$scope.cobudgetsArray',$scope.cobudgetsArray)
            //$scope.cobudgetsArray = $scope.cobudgetsArray;
            angular.forEach($scope.cobudgetsArray, function (member, index) {
                member.index = index + 1;
                member.totalamounts = $scope.getTotalAmount(member.id, selyr);
                //        $scope.TotalData = [];
                //        $scope.TotalData.push(member);
            });
            //console.log("$scope.cobudgetsArray", $scope.cobudgetsArray);
        };
    };
    
//    Restangular.all('cobudgets?filter[where][month]=' + monthID + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
//                console.log('remaining cobudgets for month 4', resp)
//                $scope.remainingbudget = resp;
//                // }
//                $scope.remaininghumanresources = ;
//                $scope.remaininggenericactivity = ;
//                $scope.remainingtravelandcommunication = ;
//                $scope.remainingspecificactivity = ;
//                $scope.remainingmsgtgactivity = ;
//                $scope.remainingadministrativeexpense = ;
//                $scope.remainingmonitoringevaluation = ;
//
//
//                
//
//                $scope.Remaining_Sum = {{((spmhumanresources|number) - (cobud.totalamounts[0].humanresources|number)) + 
//                                        ((spmgenericactivity|number) - (cobud.totalamounts[0].genericactivity|number)) + 
//                                        ((spmtravelandcommunication|number) - (cobud.totalamounts[0].travelandcommunication|number)) + 
//                                        ((spmspecificactivity|number) - (cobud.totalamounts[0].specificactivity|number)) + 
//                                        ((spmmsgtgactivity|number) - (cobud.totalamounts[0].msgtgactivity|number)) + 
//                                        ((spmadministrativeexpense|number) - (cobud.totalamounts[0].administrativeexpense|number)) + 
//                                        ((spmmonitoringevaluation|number) - (cobud.totalamounts[0].monitoringevaluation|number))}};
//                
//            });
    
    $scope.getTotalAmount = function (id, selyr) {
        return Restangular.all('cobudgets?filter[where][month]=' + id + '&filter[where][year]=' + selyr + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().$object;
    };
    $scope.cobudgetsArray = [];
    $scope.cobudgetsArray.push({
        mymonth: 'APRIL',
        id: 4
    }, {
        mymonth: 'MAY',
        id: 5
    }, {
        mymonth: 'JUNE',
        id: 6
    }, {
        mymonth: 'JULY',
        id: 7
    }, {
        mymonth: 'AUGUST',
        id: 8
    }, {
        mymonth: 'SEPTEMBER',
        id: 9
    }, {
        mymonth: 'OCTOBER',
        id: 10
    }, {
        mymonth: 'NOVEMBER',
        id: 11
    }, {
        mymonth: 'DECEMBER',
        id: 12
    }, {
        mymonth: 'JANUARY',
        id: 1
    }, {
        mymonth: 'FEBRUARY',
        id: 2
    }, {
        mymonth: 'MARCH',
        id: 3
    });


    /******************************EDIT **************************/
    $scope.BudgetEdit = function (monthID, year) {
        //console.log('BudgetId', monthID);
        //console.log('year', year);
        $scope.currentBudgetId = null;
        $scope.original = null;
        $scope.cobudget = {
            year: $scope.current_YEAR,
            deleteflag: false,
            comember: $window.sessionStorage.UserEmployeeId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            lastmodifiedbyrole: $window.sessionStorage.roleId,
            facility: $window.sessionStorage.coorgId,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId
        };
        $scope.Displaymonth = null;
        $scope.Current_Year = null;
        $scope.Swasti_Total = null;
        $scope.Swasti_Total = null;
        $scope.Total_Sum = null;
        $scope.CO_Total = null;
        $scope.Remaining_Sum = null;


        $scope.isCreateView = false;

        Restangular.one('cobudgets?filter[where][month]=' + monthID + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).get().then(function (resp) {
            //console.log('cobudgets', resp)
            $scope.currentBudgetId = resp[0].id;
            $scope.currentBalAmount = resp[0].totalamount;

            Restangular.one('cobudgets', $scope.currentBudgetId).get().then(function (aprl) {
                //console.log('cobudgets', aprl);
                $scope.original = aprl;
                $scope.cobudget = Restangular.copy($scope.original);
                for (var i = 0; i < $scope.cobudgetsArray.length; i++) {
                    //console.log('scope.cobudgetsArray.length',$scope.cobudgetsArray.length)
                    if ($scope.cobudgetsArray[i].id == $scope.original.month) {
                        // console.log('$scope.cobudgetsArray[i].id',$scope.cobudgetsArray[i].mymonth)
                        $scope.Displaymonth = $scope.cobudgetsArray[i].mymonth;
                    }
                }
                Restangular.one('spmbudgetyears', $scope.original.year).get().then(function (yr) {
                    $scope.Current_Year = yr.name;
                });
                $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
                $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
                $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);

            });
        });
        if (monthID == 4) {
            Restangular.all('cobudgets?filter[where][month]=' + monthID + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 4', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                //for (var i = 0; i < $scope.remainingbudget.length; i++) {
                $scope.totalremainighumanresource = 0;
                $scope.totalremainiggenericactivity = 0;
                $scope.totalremainigtravelandcommunication = 0;
                $scope.totalremainingspecificactivity = 0;
                $scope.totalremainingmsgtgactivity = 0;
                $scope.totalremainingadministrativeexpense = 0;
                $scope.totalremainingmonitoringevaluation = 0;


                $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[0].humanresources);
                $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[0].genericactivity);
                $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[0].travelandcommunication);
                $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[0].specificactivity);
                $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[0].msgtgactivity);
                $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[0].administrativeexpense);
                $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[0].monitoringevaluation);
                // }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;


                $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                //console.log('$scope.remaininghumanresources', $scope.remaininghumanresources);
                console.log('$scope.totalremainig_humanresource', $scope.totalremainighumanresource);
                //console.log('$scope.totalremainig_travelandcommunication', $scope.totalremainig_travelandcommunication);
                $scope.dispalyhumanresources = $scope.remaininghumanresources;

            });
        } else if (monthID == 5) {
            //alert('Add Two Month');
            //Restangular.all('cobudgets?filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
            Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5' + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 5', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;


                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                //console.log('$scope.remaininghumanresources', $scope.remaininghumanresources);
                //console.log('$scope.totalremainig_humanresource', $scope.totalremainig_humanresource);
                //console.log('$scope.totalremainig_travelandcommunication', $scope.totalremainig_travelandcommunication);

            });
        } else if (monthID == 6) {
            Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6' + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 5', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            });
        } else if (monthID == 7) {
            Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7' + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 5', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            });
        } else if (monthID == 8) {
            Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8' + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 5', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            });
        } else if (monthID == 9) {
            Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9' + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 5', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            });
        } else if (monthID == 10) {
            Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10' + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 5', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            });
        } else if (monthID == 11) {
            Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10&filter[where][month][inq]=11' + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 5', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            });
        } else if (monthID == 12) {
            Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10&filter[where][month][inq]=11&filter[where][month][inq]=12' + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 5', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            });
        } else if (monthID == 1) {
            Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10&filter[where][month][inq]=11&filter[where][month][inq]=12&filter[where][month][inq]=1' + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 5', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            });
        } else if (monthID == 2) {
            Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10&filter[where][month][inq]=11&filter[where][month][inq]=12&filter[where][month][inq]=1&filter[where][month][inq]=2' + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 5', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            });
        } else if (monthID == 3) {
            Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10&filter[where][month][inq]=11&filter[where][month][inq]=12&filter[where][month][inq]=1&filter[where][month][inq]=2&filter[where][month][inq]=3' + '&filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                console.log('remaining cobudgets for month 5', resp)
                $scope.remainingbudget = resp;
                $scope.totalremainig_humanresource = 0;
                $scope.totalremainig_genericactivity = 0;
                $scope.totalremainig_travelandcommunication = 0;
                $scope.totalremainig_specificactivity = 0;
                $scope.totalremainig_msgtgactivity = 0;
                $scope.totalremainig_administrativeexpense = 0;
                $scope.totalremainig_monitoringevaluation = 0;
                for (var i = 0; i < $scope.remainingbudget.length; i++) {
                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                }
                $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            });
        } else {
            return;
        }
        /*Restangular.all('cobudgets?filter[where][year]=' + year + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
            //console.log('cobudgets', resp)
            $scope.remainingbudget = resp;
            $scope.totalremainig_humanresource = 0;
            $scope.totalremainig_genericactivity = 0;
            $scope.totalremainig_travelandcommunication = 0;
            $scope.totalremainig_specificactivity = 0;
            $scope.totalremainig_msgtgactivity = 0;
            $scope.totalremainig_administrativeexpense = 0;
            $scope.totalremainig_monitoringevaluation = 0;
            for (var i = 0; i < $scope.remainingbudget.length; i++) {
                $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
            }
            $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
            $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
            $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
            $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
            $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
            $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
            $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

            $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
            //console.log('$scope.remaininghumanresources', $scope.remaininghumanresources);
            console.log('$scope.totalremainig_humanresource', $scope.totalremainig_humanresource);
            console.log('$scope.totalremainig_travelandcommunication', $scope.totalremainig_travelandcommunication);

        });*/
        $scope.BudgetEditModal1();
    }

    /**********-----------------------------------------------------------------------------------------******/


    /**********************************************************************************************
    $scope.$watch('cobudget.swastihumanresources', function (newValue, oldValue) {
        //        console.log('newValue', newValue);
        //        console.log('oldValue', oldValue);
        if (newValue == oldValue || newValue == ' ' || newValue == undefined || oldValue == undefined) {
            return;
        } else {
            //            $scope.myArray = [];
            //            console.log(oldValue);
            //            $scope.myArray.push(newValue);
            //            console.log('$scope.myArray', $scope.myArray);
            //            $scope.finalmyvalue = $scope.myArray[$scope.myArray.length - 1]
            //            $scope.remaininghumanresources = $scope.remaininghumanresources - (-$scope.finalmyvalue);
            //            console.log('$scope.finalmyvalue', $scope.finalmyvalue);
            //            console.log('$scope.remaininghumanresources', $scope.remaininghumanresources);


            //            $scope.myvalue = $scope.remaininghumanresources;
            //            $scope.mynewvalue = $scope.myvalue - (-newValue);
            //            $scope.myArray.push(newValue);
            //            console.log('$scope.mynewvalue', $scope.mynewvalue);
            //            console.log('$scope.myArray', $scope.myArray);
            //            $scope.finalmyvalue = $scope.myArray[$scope.myArray.length - 1]
            //            console.log('$scope.finalmyvalue', $scope.finalmyvalue);
            //            $scope.MyEvent();

            //            if ($scope.original != undefined && $scope.original != null) {
            //                $scope.initialswastiamount = +$scope.original.swastihumanresources;
            //                $scope.enteredswastiamount = +$scope.cobudget.swastihumanresources;
            //                $scope.initialcoamount = +$scope.original.cohumanresources;
            //                $scope.enteredcoamount = +$scope.cobudget.cohumanresources;
            //                //$scope.presentremaining = +$scope.totalremainig_humanresource;
            //                $scope.presentremaining = +$scope.remaininghumanresources;
            //
            //                console.log('initialswastiamount', $scope.original.swastihumanresources);
            //                console.log('enteredswastiamount', $scope.cobudget.swastihumanresources);
            //                console.log('initialcoamount', $scope.original.swastihumanresources);
            //                console.log('enteredcoamount', $scope.original.swastihumanresources);
            //                if ($scope.initialswastiamount < $scope.enteredswastiamount) {
            //                    //$scope.actualremaining =  $scope.presentremaining - $scope.initialswastiamount + $scope.initialcoamount - $scope.enteredswastiamount - $scope.enteredcoamount; 
            //                    $scope.actualremaining = $scope.presentremaining - $scope.enteredswastiamount;
            //                    console.log('$scope.actualremaining if', $scope.actualremaining);
            //                    $scope.remaininghumanresources = $scope.actualremaining;
            //                } else if ($scope.initialswastiamount == $scope.enteredswastiamount) {
            //                    $scope.remaininghumanresources = $scope.remaininghumanresources;
            //
            //                } else {
            //                    $scope.actualremaining = $scope.presentremaining + $scope.enteredswastiamount
            //                    console.log('$scope.actualremaining else', $scope.actualremaining);
            //                    $scope.remaininghumanresources = $scope.actualremaining;
            //                }
            //
            //
            //
            //            } else {
            //                $scope.remaininghumanresources = (+$scope.totalremainig_humanresource) - ((+$scope.cobudget.swastihumanresources) + (+$scope.cobudget.cohumanresources));
            //            }
        }
    });

    /******************************************************************************
         $scope.HumanresourceTotal = function () {};
        $scope.HumanresourceTotal1 = function () {

            $scope.cobudget.humanresources = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.cohumanresources);
            //$scope.remaininghumanresources = $scope.totalremainig_humanresource + $scope.cobudget.humanresources;
            //init = 100,entered=200,remaining=500,actualremain=400
            //console.log('$scope.original',$scope.original);
            if ($scope.original != undefined && $scope.original != null) {
                $scope.initialswastiamount = +$scope.original.swastihumanresources;
                $scope.enteredswastiamount = +$scope.cobudget.swastihumanresources;
                $scope.initialcoamount = +$scope.original.cohumanresources;
                $scope.enteredcoamount = +$scope.cobudget.cohumanresources;
                //$scope.presentremaining = +$scope.totalremainig_humanresource;
                $scope.presentremaining = +$scope.remaininghumanresources;
                
                
                console.log('initialswastiamount',$scope.original.swastihumanresources);
                console.log('enteredswastiamount',$scope.cobudget.swastihumanresources);
                console.log('initialcoamount',$scope.original.swastihumanresources);
                console.log('enteredcoamount',$scope.original.swastihumanresources);
                console.log('$scope.totalremainig_humanresource bef', $scope.totalremainig_humanresource);
                
                $scope.actualremaining = $scope.presentremaining + $scope.initialswastiamount + $scope.initialcoamount - $scope.enteredswastiamount - $scope.enteredcoamount;
                
                 //$scope.actualremaining = $scope.presentremaining + $scope.initialswastiamount + $scope.initialcoamount - $scope.enteredswastiamount - $scope.enteredcoamount;
                
                $scope.remaininghumanresources = $scope.actualremaining;
                
                console.log('$scope.remaininghumanresources aft', $scope.remaininghumanresources);
            } else {
                //$scope.remaininghumanresources = $scope.totalremainig_humanresource;
                $scope.remaininghumanresources = (+$scope.totalremainig_humanresource) - ((+$scope.cobudget.swastihumanresources) + (+$scope.cobudget.cohumanresources));
            }
                //console.log('$scope.spmhumanresources', $scope.spmhumanresources);
        }
    /*--------------------------------------------------------------------------------------------------------------------------------------------*/
    $scope.spmbudgetupdate = {};

    $scope.Update = function () {
        document.getElementById('swastihumanresources').style.borderColor = "";
        document.getElementById('cohumanresources').style.borderColor = "";
        document.getElementById('swastitravelandcommunication').style.borderColor = "";
        document.getElementById('cotravelandcommunication').style.borderColor = "";
        document.getElementById('swastigenericactivity').style.borderColor = "";
        document.getElementById('cogenericactivity').style.borderColor = "";
        document.getElementById('swastispecificactivity').style.borderColor = "";
        document.getElementById('cospecificactivity').style.borderColor = "";
        document.getElementById('swastimsgtgactivity').style.borderColor = "";
        document.getElementById('comsgtgactivity').style.borderColor = "";
        document.getElementById('swastiadministrativeexpense').style.borderColor = "";
        document.getElementById('coadministrativeexpense').style.borderColor = "";
        document.getElementById('swastimonitoringevaluation').style.borderColor = "";
        document.getElementById('comonitoringevaluation').style.borderColor = "";
        if ($scope.cobudget.swastihumanresources === '' || $scope.cobudget.swastihumanresources === null) {
            $scope.validatestring = $scope.validatestring + $scope.enterhumanressw;
            document.getElementById('swastihumanresources').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cohumanresources === '' || $scope.cobudget.cohumanresources === null) {
            $scope.validatestring = $scope.validatestring + $scope.enterhumanresco;
            document.getElementById('cohumanresources').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastitravelandcommunication === '' || $scope.cobudget.swastitravelandcommunication === null) {
            $scope.validatestring = $scope.validatestring + $scope.entertravelsw;
            document.getElementById('swastitravelandcommunication').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cotravelandcommunication === '' || $scope.cobudget.cotravelandcommunication === null) {
            $scope.validatestring = $scope.validatestring + $scope.entertravelco;
            document.getElementById('cotravelandcommunication').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastigenericactivity === '' || $scope.cobudget.swastigenericactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.entergenericsw;
            document.getElementById('swastigenericactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cogenericactivity === '' || $scope.cobudget.cogenericactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.entergenericco;
            document.getElementById('cogenericactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastispecificactivity === '' || $scope.cobudget.swastispecificactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.enterspecificsw;
            document.getElementById('swastispecificactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.cospecificactivity === '' || $scope.cobudget.cospecificactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.enterspecificco;
            document.getElementById('cospecificactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastimsgtgactivity === '' || $scope.cobudget.swastimsgtgactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.entermsmsw;
            document.getElementById('swastimsgtgactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.comsgtgactivity === '' || $scope.cobudget.comsgtgactivity === null) {
            $scope.validatestring = $scope.validatestring + $scope.entermsmco;;
            document.getElementById('comsgtgactivity').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastiadministrativeexpense === '' || $scope.cobudget.swastiadministrativeexpense === null) {
            $scope.validatestring = $scope.validatestring + $scope.enteradministrativesw;
            document.getElementById('swastiadministrativeexpense').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.coadministrativeexpense === '' || $scope.cobudget.coadministrativeexpense === null) {
            $scope.validatestring = $scope.validatestring + $scope.enteradministrativeco;
            document.getElementById('coadministrativeexpense').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.swastimonitoringevaluation === '' || $scope.cobudget.swastimonitoringevaluation === null) {
            $scope.validatestring = $scope.validatestring + $scope.entermonitoringsw;
            document.getElementById('swastimonitoringevaluation').style.borderColor = "#FF0000";
        } else if ($scope.cobudget.comonitoringevaluation === '' || $scope.cobudget.comonitoringevaluation === null) {
            $scope.validatestring = $scope.validatestring + $scope.entermonitoringco;
            document.getElementById('comonitoringevaluation').style.borderColor = "#FF0000";
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            //console.log('SAVE', $scope.cobudget);
            // Restangular.one('spmbudgets', $scope.cobudget.year).get().then(function (res) {
            Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][year]=' + $scope.cobudget.year).getList().then(function (res) {
                $scope.spmBudget = res[0].totalamount;
                $scope.spmBudgetAmount = res[0].balancetilldate;

                //console.log('$scope.spmBudget',$scope.spmBudget)
                //   $scope.cobudget.balancetilldate = $scope.spmBudget - $scope.cobudget.totalamount;


                //     $scope.cobudget.balancetilldate = $scope.spmBudgetAmount - $scope.cobudget.totalamount;

                $scope.cobudget.balancetilldate = $scope.spmBudgetAmount + ($scope.currentBalAmount - $scope.cobudget.totalamount);
                $scope.spmbudgetupdate.balancetilldate = $scope.spmBudgetAmount + ($scope.currentBalAmount - $scope.cobudget.totalamount);

                $scope.cobudget.remaininghumanresources = $scope.remaininghumanresources;
                $scope.cobudget.remaininggenericactivity = $scope.remaininggenericactivity;
                $scope.cobudget.remainingtravelandcommunication = $scope.remainingtravelandcommunication;
                $scope.cobudget.remainingspecificactivity = $scope.remainingspecificactivity;
                $scope.cobudget.remainingmsgtgactivity = $scope.remainingmsgtgactivity;
                $scope.cobudget.remainingadministrativeexpense = $scope.remainingadministrativeexpense;
                $scope.cobudget.remainingmonitoringevaluation = $scope.remainingmonitoringevaluation;
                $scope.cobudget.remainingtotal = $scope.Remaining_Sum;

                //    console.log('$scope.spmbudgetupdate.balancetilldate', $scope.spmbudgetupdate.balancetilldate);                 
                //    console.log('$scope.cobudget.balancetilldate', $scope.cobudget.balancetilldate);

                //console.log('$scope.currentBudgetId', $scope.currentBudgetId);

                Restangular.one('cobudgets', $scope.currentBudgetId).customPUT($scope.cobudget).then(function (updateResponse) {
                    // console.log('updateResponse', updateResponse);
                    Restangular.one('spmbudgets', $scope.spmBudgetId).customPUT($scope.spmbudgetupdate).then(function (updateResponse) {
                        $scope.modalAW.close();
                        //location.reload();



                        Restangular.one('spmbudgetyears', $scope.current_YEAR).get().then(function (yearResa) {
                            $scope.Current_Year = yearResa.name;
                        });
                        Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][year]=' + $scope.current_YEAR).getList().then(function (res) {
                            $scope.spmBudget = res[0].totalamount;
                            $scope.spmBudgetAmount = res[0].balancetilldate;
                            $scope.spmBudgetId = res[0].id;
                            $scope.Balance_amount = $scope.spmBudget - $scope.Total_CO_HumanResource;
                            //console.log('$scope.spmBudget', $scope.spmBudget)
                            /* for(var b=0; b<res.length;b++){
                                 $
                             } */

                        });
                        // console.log('$scope.cobudgetsArray',$scope.cobudgetsArray)
                        //$scope.cobudgetsArray = $scope.cobudgetsArray;
                        angular.forEach($scope.cobudgetsArray, function (member, index) {
                            member.index = index + 1;
                            member.totalamounts = $scope.getTotalAmount(member.id, $scope.current_YEAR);
                            //        $scope.TotalData = [];
                            //        $scope.TotalData.push(member);
                        });
                    });
                });
            });
        }
    }
    $scope.BudgetEditModal1 = function () {
        $scope.modalAW = $modal.open({
            animation: true,
            templateUrl: 'template/spmbudgetView.html',
            scope: $scope,
            backdrop: 'static',
            size: 'lg'
        });
    };
    $scope.Cancel = function () {
            $scope.modalAW.close();
            //location.reload();
        }
        /****************ADD BUDGET*******************/
    $scope.cobudget = {
        deleteflag: false,
        comember: $window.sessionStorage.UserEmployeeId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        lastmodifiedbyrole: $window.sessionStorage.roleId,
        facility: $window.sessionStorage.coorgId,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId
    };
    $scope.callMsg = function () {
        alert('You have already created budget for this month');
    }

    //console.log(' $window.sessionStorage.UserEmployeeId', $window.sessionStorage.UserEmployeeId);
    $scope.validatestring = ''
    $scope.AddBudget = function (nam, monthid, year) {

        $scope.addyear = $scope.current_YEAR;
        var monthID = monthid;
        $scope.addmonthID = monthid;
        $scope.original = null;
        console.log('AddBudget', $scope.current_YEAR);
        $scope.isCreateView = true;
        $scope.cobudget = {
            year: $scope.current_YEAR,
            deleteflag: false,
            comember: $window.sessionStorage.UserEmployeeId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            lastmodifiedbyrole: $window.sessionStorage.roleId,
            facility: $window.sessionStorage.coorgId,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId
        };
        $scope.Swasti_Total = 0;
        $scope.CO_Total = 0;
        $scope.Total_Sum = 0;
        //$scope.Remaining_Sum = 0;
        $scope.remaininghumanresources = "";
        $scope.remaininggenericactivity = null;
        $scope.remainingtravelandcommunication = null;
        $scope.remainingspecificactivity = null;
        $scope.remainingmsgtgactivity = null;
        $scope.remainingadministrativeexpense = null;
        $scope.remainingmonitoringevaluation = null;
        $scope.Remaining_Sum = 0;
        //console.log('name', nam)
        $scope.AlreadyCreated = '';
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        } else {
            //Restangular.one('cobudgets?filter[where][year]=' + $scope.cobudget.year + '&filter[where][month]=' + monthid + '&filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.UserEmployeeId).get().then(function (coRes) {
            Restangular.one('cobudgets?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.UserEmployeeId).get().then(function (coRes) {
                //$scope.AlreadyCreated = coRes.month;
                //console.log('CoRes',coRes);
                $scope.AlreadyCreated = coRes.length;
                //console.log('coRes.length', $scope.AlreadyCreated)
                // console.log('coRes',coRes)
                if ($scope.AlreadyCreated == 1) {
                    $scope.modalAW.close();
                    console.log('Shi h')
                    $scope.toggleValidation2();
                    //$scope.validatestring = $scope.validatestring + 'You have already created budget for this month';
                }
                /* else {
                     console.log('Shi NHI h')
                 }*/
            });

            /* Restangular.all('cobudgets?filter[where][year]=' + $scope.current_YEAR + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                 console.log('cobudgets', resp)
                 $scope.remainingbudget = resp;
                 $scope.totalremainig_humanresource = 0;
                 $scope.totalremainig_genericactivity = 0;
                 $scope.totalremainig_travelandcommunication = 0;
                 $scope.totalremainig_specificactivity = 0;
                 $scope.totalremainig_msgtgactivity = 0;
                 $scope.totalremainig_administrativeexpense = 0;
                 $scope.totalremainig_monitoringevaluation = 0;
                 for (var i = 0; i < $scope.remainingbudget.length; i++) {
                     $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                     $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                     $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);

                     $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                     $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                     $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                     $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                 }
                 $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                 $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                 $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                 $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                 $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                 $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                 $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                 $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                 console.log('$scope.remaininghumanresources', $scope.remaininghumanresources);
                 console.log('$scope.totalremainig_travelandcommunication', $scope.totalremainig_travelandcommunication);

             });*/


            if (monthID == 4) {
                Restangular.all('cobudgets?filter[where][month]=' + monthID + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 4', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;
                    //for (var i = 0; i < $scope.remainingbudget.length; i++) {

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[0].humanresources);
                    $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[0].genericactivity);
                    $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[0].travelandcommunication);
                    $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[0].specificactivity);
                    $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[0].msgtgactivity);
                    $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[0].administrativeexpense);
                    $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[0].monitoringevaluation);
                    // }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                    //console.log('$scope.remaininghumanresources', $scope.remaininghumanresources);
                    //console.log('$scope.totalremainig_humanresource', $scope.totalremainig_humanresource);
                    //console.log('$scope.totalremainig_travelandcommunication', $scope.totalremainig_travelandcommunication);

                });
            } else if (monthID == 5) {
                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 5', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;

                });
            } else if (monthID == 6) {

                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 5', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                });
            } else if (monthID == 7) {
                //console.log('year',$scope.addyear);
                //console.log('currentYear', $scope.current_YEAR)
                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 7', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                });
            } else if (monthID == 8) {
                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 5', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                });
            } else if (monthID == 9) {
                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 5', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                });
            } else if (monthID == 10) {
                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 10', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;
                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                });
            } else if (monthID == 11) {
                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10&filter[where][month][inq]=11' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 5', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                });
            } else if (monthID == 12) {
                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10&filter[where][month][inq]=11&filter[where][month][inq]=12' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 5', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                });
            } else if (monthID == 1) {
                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10&filter[where][month][inq]=11&filter[where][month][inq]=12&filter[where][month][inq]=1' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 1', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                });
            } else if (monthID == 1) {
                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10&filter[where][month][inq]=11&filter[where][month][inq]=12&filter[where][month][inq]=1' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 1', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                });
            } else if (monthID == 2) {
                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10&filter[where][month][inq]=11&filter[where][month][inq]=12&filter[where][month][inq]=1&filter[where][month][inq]=2' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 1', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                });
            } else if (monthID == 3) {
                Restangular.all('cobudgets?filter[where][month][inq]=4&filter[where][month][inq]=5&filter[where][month][inq]=6&filter[where][month][inq]=7&filter[where][month][inq]=8&filter[where][month][inq]=9&filter[where][month][inq]=10&filter[where][month][inq]=11&filter[where][month][inq]=12&filter[where][month][inq]=1&filter[where][month][inq]=2&filter[where][month][inq]=1' + '&filter[where][year]=' + $scope.addyear + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (resp) {
                    console.log('remaining cobudgets for month 1', resp)
                    $scope.remainingbudget = resp;
                    $scope.totalremainig_humanresource = 0;
                    $scope.totalremainig_genericactivity = 0;
                    $scope.totalremainig_travelandcommunication = 0;
                    $scope.totalremainig_specificactivity = 0;
                    $scope.totalremainig_msgtgactivity = 0;
                    $scope.totalremainig_administrativeexpense = 0;
                    $scope.totalremainig_monitoringevaluation = 0;

                    $scope.totalremainighumanresource = 0;
                    $scope.totalremainiggenericactivity = 0;
                    $scope.totalremainigtravelandcommunication = 0;
                    $scope.totalremainingspecificactivity = 0;
                    $scope.totalremainingmsgtgactivity = 0;
                    $scope.totalremainingadministrativeexpense = 0;
                    $scope.totalremainingmonitoringevaluation = 0;

                    for (var i = 0; i < $scope.remainingbudget.length; i++) {
                        $scope.totalremainig_humanresource = $scope.totalremainig_humanresource + parseInt($scope.remainingbudget[i].humanresources);
                        $scope.totalremainig_genericactivity = $scope.totalremainig_genericactivity + parseInt($scope.remainingbudget[i].genericactivity);
                        $scope.totalremainig_travelandcommunication = $scope.totalremainig_travelandcommunication + parseInt($scope.remainingbudget[i].travelandcommunication);
                        $scope.totalremainig_specificactivity = $scope.totalremainig_specificactivity + parseInt($scope.remainingbudget[i].specificactivity);
                        $scope.totalremainig_msgtgactivity = $scope.totalremainig_msgtgactivity + parseInt($scope.remainingbudget[i].msgtgactivity);
                        $scope.totalremainig_administrativeexpense = $scope.totalremainig_administrativeexpense + parseInt($scope.remainingbudget[i].administrativeexpense);
                        $scope.totalremainig_monitoringevaluation = $scope.totalremainig_monitoringevaluation + parseInt($scope.remainingbudget[i].monitoringevaluation);
                    }
                    $scope.remaininghumanresources = $scope.spmhumanresources - $scope.totalremainig_humanresource;
                    $scope.remaininggenericactivity = $scope.spmgenericactivity - $scope.totalremainig_genericactivity;
                    $scope.remainingtravelandcommunication = $scope.spmtravelandcommunication - $scope.totalremainig_travelandcommunication;
                    $scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.totalremainig_specificactivity;
                    $scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.totalremainig_msgtgactivity;
                    $scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.totalremainig_administrativeexpense;
                    $scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.totalremainig_monitoringevaluation;

                    $scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
                    $scope.totalremainigtravelandcommunication = $scope.totalremainigtravelandcommunication + $scope.remainingtravelandcommunication;
                    $scope.totalremainiggenericactivity = $scope.totalremainiggenericactivity + $scope.remaininggenericactivity;
                    $scope.totalremainingspecificactivity = $scope.totalremainingspecificactivity + $scope.remainingspecificactivity;
                    $scope.totalremainingmsgtgactivity = $scope.totalremainingmsgtgactivity + $scope.remainingmsgtgactivity;
                    $scope.totalremainingadministrativeexpense = $scope.totalremainingadministrativeexpense + $scope.remainingadministrativeexpense;
                    $scope.totalremainingmonitoringevaluation = $scope.totalremainingmonitoringevaluation + $scope.remainingmonitoringevaluation;

                    $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
                });
            } else {
                return;
            }
        }



        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            $scope.Displaymonth = nam;
            $scope.cobudget.month = monthid;
            $scope.modalAW = $modal.open({
                animation: true,
                templateUrl: 'template/spmbudgetView.html',
                scope: $scope,
                backdrop: 'static',
                size: 'lg'
            });
        }
    }
    $scope.modalTitle = 'Thank You';
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.modalInstance1 = $modal.open({
            animation: true,
            templateUrl: 'template/validation.html',
            scope: $scope,
            backdrop: 'static',
            size: 'sm'
        });
    };
    $scope.toggleValidation2 = function () {
        $scope.modalInstance1 = $modal.open({
            animation: true,
            templateUrl: 'template/validation2.html',
            scope: $scope,
            backdrop: 'static',
            size: 'sm'
        });
    };
    $scope.OkValidation = function () {
        $scope.modalInstance1.close();
    };

    $scope.Save = function () {

            document.getElementById('swastihumanresources').style.borderColor = "";
            document.getElementById('cohumanresources').style.borderColor = "";
            document.getElementById('swastitravelandcommunication').style.borderColor = "";
            document.getElementById('cotravelandcommunication').style.borderColor = "";
            document.getElementById('swastigenericactivity').style.borderColor = "";
            document.getElementById('cogenericactivity').style.borderColor = "";
            document.getElementById('swastispecificactivity').style.borderColor = "";
            document.getElementById('cospecificactivity').style.borderColor = "";
            document.getElementById('swastimsgtgactivity').style.borderColor = "";
            document.getElementById('comsgtgactivity').style.borderColor = "";
            document.getElementById('swastiadministrativeexpense').style.borderColor = "";
            document.getElementById('coadministrativeexpense').style.borderColor = "";
            document.getElementById('swastimonitoringevaluation').style.borderColor = "";
            document.getElementById('comonitoringevaluation').style.borderColor = "";
            if ($scope.cobudget.swastihumanresources == '' || $scope.cobudget.swastihumanresources == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount By Swasti';
                document.getElementById('swastihumanresources').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.cohumanresources == '' || $scope.cobudget.cohumanresources == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount By CO';
                document.getElementById('cohumanresources').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.swastitravelandcommunication == '' || $scope.cobudget.swastitravelandcommunication == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount By Swasti';
                document.getElementById('swastitravelandcommunication').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.cotravelandcommunication == '' || $scope.cobudget.cotravelandcommunication == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount By CO';
                document.getElementById('cotravelandcommunication').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.swastigenericactivity == '' || $scope.cobudget.swastigenericactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount By Swasti';
                document.getElementById('swastigenericactivity').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.cogenericactivity == '' || $scope.cobudget.cogenericactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount By CO';
                document.getElementById('cogenericactivity').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.swastispecificactivity == '' || $scope.cobudget.swastispecificactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount By Swasti';
                document.getElementById('swastispecificactivity').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.cospecificactivity == '' || $scope.cobudget.cospecificactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount By CO';
                document.getElementById('cospecificactivity').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.swastimsgtgactivity == '' || $scope.cobudget.swastimsgtgactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter MSM/TG Activity Amount By Swasti';
                document.getElementById('swastimsgtgactivity').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.comsgtgactivity == '' || $scope.cobudget.comsgtgactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter MSM/TG Activity Amount By CO';
                document.getElementById('comsgtgactivity').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.swastiadministrativeexpense == '' || $scope.cobudget.swastiadministrativeexpense == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount By Swasti';
                document.getElementById('swastiadministrativeexpense').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.coadministrativeexpense == '' || $scope.cobudget.coadministrativeexpense == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount By CO';
                document.getElementById('coadministrativeexpense').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.swastimonitoringevaluation == '' || $scope.cobudget.swastimonitoringevaluation == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount By Swasti';
                document.getElementById('swastimonitoringevaluation').style.borderColor = "#FF0000";
            } else if ($scope.cobudget.comonitoringevaluation == '' || $scope.cobudget.comonitoringevaluation == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount By CO';
                document.getElementById('comonitoringevaluation').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                console.log('SAVE', $scope.cobudget);

                $scope.spmbudgetupdate.balancetilldate = $scope.spmBudgetAmount - $scope.cobudget.totalamount;
                $scope.cobudget.balancetilldate = $scope.spmBudgetAmount - $scope.cobudget.totalamount;

                $scope.cobudget.remaininghumanresources = $scope.remaininghumanresources;
                $scope.cobudget.remaininggenericactivity = $scope.remaininggenericactivity;
                $scope.cobudget.remainingtravelandcommunication = $scope.remainingtravelandcommunication;
                $scope.cobudget.remainingspecificactivity = $scope.remainingspecificactivity;
                $scope.cobudget.remainingmsgtgactivity = $scope.remainingmsgtgactivity;
                $scope.cobudget.remainingadministrativeexpense = $scope.remainingadministrativeexpense;
                $scope.cobudget.remainingmonitoringevaluation = $scope.remainingmonitoringevaluation;
                $scope.cobudget.remainingtotal = $scope.Remaining_Sum;
                // console.log('$scope.spmbudgetupdate.balancetilldate', $scope.spmbudgetupdate.balancetilldate);
                //console.log('$scope.cobudget.balancetilldate', $scope.cobudget.balancetilldate);

                Restangular.all('cobudgets').post($scope.cobudget).then(function (cobudgetsRes) {

                    Restangular.one('spmbudgets', $scope.spmBudgetId).customPUT($scope.spmbudgetupdate).then(function (updateResponse) {
                        // console.log('cobudgetsRes', cobudgetsRes);
                        $scope.cobudget.year = $scope.current_YEAR;
                        $scope.modalAW.close();
                        //$route.reload();

                        Restangular.one('spmbudgetyears', $scope.current_YEAR).get().then(function (yearResa) {
                            $scope.Current_Year = yearResa.name;
                        });
                        Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][year]=' + $scope.current_YEAR).getList().then(function (res) {
                            $scope.spmBudget = res[0].totalamount;
                            $scope.spmBudgetAmount = res[0].balancetilldate;
                            $scope.spmBudgetId = res[0].id;
                            $scope.Balance_amount = $scope.spmBudget - $scope.Total_CO_HumanResource;
                            //console.log('$scope.spmBudget', $scope.spmBudget)
                            /* for(var b=0; b<res.length;b++){
                                 $
                             } */

                        });
                        // console.log('$scope.cobudgetsArray',$scope.cobudgetsArray)
                        //$scope.cobudgetsArray = $scope.cobudgetsArray;
                        angular.forEach($scope.cobudgetsArray, function (member, index) {
                            member.index = index + 1;
                            member.totalamounts = $scope.getTotalAmount(member.id, $scope.current_YEAR);
                            //        $scope.TotalData = [];
                            //        $scope.TotalData.push(member);
                        });
                    });
                });
            }
        }
        /************************************GET TOTAL *******************************/
        //    $scope.myArray = [];
        //    $scope.value = '';
        //    var timeout, delay = 1250;
        //    $scope.changes = false;
    $scope.HumanresourceTotal1 = function () {

        $scope.cobudget.humanresources = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.cohumanresources);

        if ($scope.original != undefined && $scope.original != null) {
            $scope.initialswastiamount = +$scope.original.swastihumanresources;
            $scope.enteredswastiamount = +$scope.cobudget.swastihumanresources;
            $scope.initialcoamount = +$scope.original.cohumanresources;
            $scope.enteredcoamount = +$scope.cobudget.cohumanresources;
            $scope.presentremaining = +$scope.totalremainighumanresource;

            $scope.displayammount = +$scope.dispalyhumanresources;
            $scope.calculateammount = $scope.cobudget.humanresources;
            console.log('$scope.displayammount', $scope.displayammount);
            console.log('$scope.calculateammount', $scope.calculateammount);
            console.log('$scope.totalremainighumanresource', $scope.totalremainighumanresource);

            //$scope.totalremainighumanresource = $scope.totalremainighumanresource + $scope.remaininghumanresources;
            $scope.actualremaining = $scope.presentremaining + $scope.initialswastiamount + $scope.initialcoamount - $scope.enteredswastiamount - $scope.enteredcoamount;
            console.log('$scope.actualremaining', $scope.actualremaining);
            $scope.remaininghumanresources = $scope.actualremaining;

        } else {
            $scope.presentremaining = +$scope.totalremainighumanresource;
            $scope.remaininghumanresources = (+$scope.presentremaining) - ((+$scope.cobudget.swastihumanresources) + (+$scope.cobudget.cohumanresources));
            console.log('$scope.remaininghumanresources else', $scope.presentremaining);
        }
    };

    //        $scope.changes = true;
    //        console.log('Change', $scope.changes);
    //        if (timeout) {
    //            $timeout.cancel(timeout);
    //        }
    //        timeout = $timeout(function () {
    //            // do something
    //
    //            console.log('$scope.cobudget.swastihumanresources', $scope.cobudget.swastihumanresources);
    //            console.log('$scope.cobudget.cohumanresources', $scope.cobudget.cohumanresources);
    //            //$scope.valueone = $scope.cobudget.swastihumanresources;
    //            //$scope.valuetwo = $scope.cobudget.cohumanresources;
    //            if ($scope.changes == true) {
    //                //$scope.valueone = '';
    //                //$scope.valuetwo = '';
    //                $scope.myval = '';
    //                $scope.mycalval = '';
    //                $scope.valueone = $scope.cobudget.swastihumanresources;
    //                $scope.valuetwo = $scope.cobudget.cohumanresources;
    //            } else {
    //                $scope.valueone = $scope.cobudget.swastihumanresources;
    //                $scope.valuetwo = $scope.cobudget.cohumanresources;
    //            }
    //            if ($scope.valueone > $scope.valuetwo) {
    //
    //                $scope.myval = $scope.cobudget.swastihumanresources - ($scope.cobudget.cohumanresources);
    //                //console.log('$scope.myval', $scope.myval);
    //
    //                $scope.mycalval = $scope.remaininghumanresources - ($scope.myval);
    //                //console.log('$scope.mycalval', $scope.mycalval);
    //
    //                $scope.myevent();
    //
    //            } else if ($scope.valueone < $scope.valuetwo) {
    //
    //                $scope.myval = $scope.cobudget.cohumanresources - ($scope.cobudget.swastihumanresources);
    //                //console.log('myval', $scope.myval);
    //
    //                $scope.mycalval = $scope.remaininghumanresources - (-$scope.myval);
    //                //console.log('$scope.mycalval', $scope.mycalval);
    //
    //                $scope.myevent();
    //            }
    //
    //        }, delay);


    /*$scope.myevent = function () {
        $scope.remaininghumanresources = $scope.mycalval;
    };*/

    $scope.TravelandCommunication = function () {}
    $scope.TravelandCommunication1 = function () {
        $scope.cobudget.travelandcommunication = parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.cotravelandcommunication);
        if ($scope.original != undefined && $scope.original != null) {
            $scope.initialswastiamount2 = +$scope.original.swastitravelandcommunication;
            $scope.enteredswastiamount2 = +$scope.cobudget.swastitravelandcommunication;
            $scope.initialcoamount2 = +$scope.original.cotravelandcommunication;
            $scope.enteredcoamount2 = +$scope.cobudget.cotravelandcommunication;
            //$scope.presentremaining2 = +$scope.totalremainig_travelandcommunication;//+$scope.remainingtravelandcommunication; 
            $scope.presentremaining2 = $scope.totalremainigtravelandcommunication;



            //            console.log('$scope.initialswastiamount2 bef', $scope.initialswastiamount2);
            //            console.log('$scope.enteredswastiamount2 bef', $scope.enteredswastiamount2);
            //            console.log('$scope.initialcoamount2 bef', $scope.initialcoamount2);
            //            console.log('$scope.enteredcoamount2 bef', $scope.enteredcoamount2);
            //            console.log('$scope.presentremaining2 bef', $scope.presentremaining2);
            //            console.log('$scope.totalremainig_travelandcommunication bef', $scope.totalremainig_travelandcommunication);
            //            console.log('$scope.remainingtravelandcommunication bef', $scope.remainingtravelandcommunication);

            $scope.actualremaining2 = $scope.presentremaining2 + $scope.initialswastiamount2 + $scope.initialcoamount2 - $scope.enteredswastiamount2 - $scope.enteredcoamount2;
            $scope.remainingtravelandcommunication = $scope.actualremaining2;
            //console.log('$scope.remainingtravelandcommunication aft', $scope.remainingtravelandcommunication);
        } else {
            $scope.remainingtravelandcommunication = (+$scope.totalremainigtravelandcommunication) - ((+$scope.cobudget.swastitravelandcommunication) + (+$scope.cobudget.cotravelandcommunication));
            //console.log('$scope.remainingtravelandcommunication2', $scope.remainingtravelandcommunication);
        }
    }
    $scope.Genericactivity = function () {}
    $scope.Genericactivity1 = function () {
        $scope.cobudget.genericactivity = parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.cogenericactivity);
        if ($scope.original != undefined && $scope.original != null) {
            $scope.initialswastiamount3 = +$scope.original.swastigenericactivity;
            $scope.enteredswastiamount3 = +$scope.cobudget.swastigenericactivity;
            $scope.initialcoamount3 = +$scope.original.cogenericactivity;
            $scope.enteredcoamount3 = +$scope.cobudget.cogenericactivity;
            $scope.presentremaining3 = +$scope.totalremainiggenericactivity;

            $scope.actualremaining3 = $scope.presentremaining3 + $scope.initialswastiamount3 + $scope.initialcoamount3 - $scope.enteredswastiamount3 - $scope.enteredcoamount3;
            $scope.remaininggenericactivity = $scope.actualremaining3;
        } else {
            //$scope.remaininghumanresources = $scope.totalremainig_humanresource;
            $scope.remaininggenericactivity = (+$scope.totalremainiggenericactivity) - ((+$scope.cobudget.swastigenericactivity) + (+$scope.cobudget.cogenericactivity));
        }
    }
    $scope.Specificactivity = function () {}
    $scope.Specificactivity1 = function () {
        $scope.cobudget.specificactivity = parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.cospecificactivity);
        //$scope.remainingspecificactivity = $scope.spmspecificactivity - $scope.cobudget.specificactivity;
        if ($scope.original != undefined && $scope.original != null) {
            $scope.initialswastiamount4 = +$scope.original.swastispecificactivity;
            $scope.enteredswastiamount4 = +$scope.cobudget.swastispecificactivity;
            $scope.initialcoamount4 = +$scope.original.cospecificactivity;
            $scope.enteredcoamount4 = +$scope.cobudget.cospecificactivity;
            $scope.presentremaining4 = +$scope.totalremainingspecificactivity;

            $scope.actualremaining4 = $scope.presentremaining4 + $scope.initialswastiamount4 + $scope.initialcoamount4 - $scope.enteredswastiamount4 - $scope.enteredcoamount4;
            $scope.remainingspecificactivity = $scope.actualremaining4;
        } else {
            //$scope.remaininghumanresources = $scope.totalremainig_humanresource;
            $scope.remainingspecificactivity = (+$scope.totalremainingspecificactivity) - ((+$scope.cobudget.swastispecificactivity) + (+$scope.cobudget.cospecificactivity));
        }
    }
    $scope.Msgtgactivity = function () {}
    $scope.Msgtgactivity1 = function () {
        $scope.cobudget.msgtgactivity = parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.comsgtgactivity);
        //$scope.remainingmsgtgactivity = $scope.spmmsgtgactivity - $scope.cobudget.msgtgactivity;
        if ($scope.original != undefined && $scope.original != null) {
            $scope.initialswastiamount5 = +$scope.original.swastimsgtgactivity;
            $scope.enteredswastiamount5 = +$scope.cobudget.swastimsgtgactivity;
            $scope.initialcoamount5 = +$scope.original.comsgtgactivity;
            $scope.enteredcoamount5 = +$scope.cobudget.comsgtgactivity;
            $scope.presentremaining5 = +$scope.totalremainingmsgtgactivity;

            $scope.actualremaining5 = $scope.presentremaining5 + $scope.initialswastiamount5 + $scope.initialcoamount5 - $scope.enteredswastiamount5 - $scope.enteredcoamount5;
            $scope.remainingmsgtgactivity = $scope.actualremaining5;
        } else {
            $scope.remainingmsgtgactivity = (+$scope.totalremainingmsgtgactivity) - ((+$scope.cobudget.swastimsgtgactivity) + (+$scope.cobudget.comsgtgactivity));
        }
    }
    $scope.Administrativeexpense = function () {}
    $scope.Administrativeexpense1 = function () {
        $scope.cobudget.administrativeexpense = parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.coadministrativeexpense);
        //$scope.remainingadministrativeexpense = $scope.spmadministrativeexpense - $scope.cobudget.administrativeexpense;
        if ($scope.original != undefined && $scope.original != null) {
            $scope.initialswastiamount6 = +$scope.original.swastiadministrativeexpense;
            $scope.enteredswastiamount6 = +$scope.cobudget.swastiadministrativeexpense;
            $scope.initialcoamount6 = +$scope.original.coadministrativeexpense;
            $scope.enteredcoamount6 = +$scope.cobudget.coadministrativeexpense;
            $scope.presentremaining6 = +$scope.totalremainingadministrativeexpense;

            $scope.actualremaining6 = $scope.presentremaining6 + $scope.initialswastiamount6 + $scope.initialcoamount6 - $scope.enteredswastiamount6 - $scope.enteredcoamount6;
            $scope.remainingadministrativeexpense = $scope.actualremaining6;
        } else {
            $scope.remainingadministrativeexpense = (+$scope.totalremainingadministrativeexpense) - ((+$scope.cobudget.swastiadministrativeexpense) + (+$scope.cobudget.coadministrativeexpense));
        }
    }
    $scope.Monitoringevaluation = function () {}
    $scope.Monitoringevaluation1 = function () {
        $scope.cobudget.monitoringevaluation = parseInt($scope.cobudget.swastimonitoringevaluation) + parseInt($scope.cobudget.comonitoringevaluation);
        //$scope.remainingmonitoringevaluation = $scope.spmmonitoringevaluation - $scope.cobudget.monitoringevaluation;
        if ($scope.original != undefined && $scope.original != null) {
            $scope.initialswastiamount7 = +$scope.original.swastimonitoringevaluation;
            $scope.enteredswastiamount7 = +$scope.cobudget.swastimonitoringevaluation;
            $scope.initialcoamount7 = +$scope.original.comonitoringevaluation;
            $scope.enteredcoamount7 = +$scope.cobudget.comonitoringevaluation;
            $scope.presentremaining7 = +$scope.totalremainingmonitoringevaluation;

            $scope.actualremaining7 = $scope.presentremaining7 + $scope.initialswastiamount7 + $scope.initialcoamount7 - $scope.enteredswastiamount7 - $scope.enteredcoamount7;
            $scope.remainingmonitoringevaluation = $scope.actualremaining7;
        } else {
            $scope.remainingmonitoringevaluation = (+$scope.totalremainingmonitoringevaluation) - ((+$scope.cobudget.swastimonitoringevaluation) + (+$scope.cobudget.comonitoringevaluation));
        }
    }
    $scope.SwastiSum1 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum2 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum3 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum4 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum5 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum6 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.SwastiSum7 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    }
    $scope.COSum1 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum2 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum3 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum4 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum5 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum6 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };
    $scope.COSum7 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;

        //$scope.Remaining_Sum = $scope.remaininghumanresources;
        $scope.Remaining_Sum = $scope.remaininghumanresources + $scope.remaininggenericactivity + $scope.remainingtravelandcommunication + $scope.remainingspecificactivity + $scope.remainingmsgtgactivity + $scope.remainingadministrativeexpense + $scope.remainingmonitoringevaluation;
    };

    /************************************************************************************/

    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.selyear
        $scope.enterhumanressw = langResponse.enterhumanressw;
        $scope.enterhumanresco = langResponse.enterhumanresco;
        $scope.entertravelsw = langResponse.entertravelsw;
        $scope.entertravelco = langResponse.entertravelco;
        $scope.entergenericsw = langResponse.entergenericsw;
        $scope.entergenericco = langResponse.entergenericco;
        $scope.enterspecificsw = langResponse.enterspecificsw;
        $scope.enterspecificco = langResponse.enterspecificco;
        $scope.entermsmsw = langResponse.entermsmsw;
        $scope.entermsmco = langResponse.entermsmco;
        $scope.enteradministrativesw = langResponse.enteradministrativesw;
        $scope.enteradministrativeco = langResponse.enteradministrativeco;
        $scope.entermonitoringsw = langResponse.entermonitoringsw;
        $scope.entermonitoringco = langResponse.entermonitoringco;
    });

});