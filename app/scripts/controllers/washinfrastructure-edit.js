'use strict';

angular.module('secondarySalesApp')
    .controller('WashInfrastructureEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {

        $scope.SaveBtn = false;
        $scope.UpdateBtn = true;

        $scope.washLanguage = {};

        Restangular.one('washLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.washLanguage = langResponse[0];
            $scope.message = langResponse[0].infrastructureHasBeenUpdated;
            $scope.washHeading = langResponse[0].editInfrastructure;
        });

        $scope.UserLanguage = $window.sessionStorage.language;

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        $scope.wash = {};

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.hideAssigned = true;

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.wash.associatedHF = $window.sessionStorage.userId;

            });
        } else {

            $scope.hideAssigned = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.wash.associatedHF = $scope.wash.associatedHF;

            });

        }

        if ($routeParams.id) {
            Restangular.one('infrastructures', $routeParams.id).get().then(function (infrastructure) {
                $scope.original = infrastructure;
                $scope.wash = Restangular.copy($scope.original);

                Restangular.all('areas?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (ars) {
                    $scope.areas = ars;
                    $scope.wash.areaId = infrastructure.areaId;
                });

                Restangular.all('typeofinfrastructures?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (ifrs) {
                    $scope.typeofinfrastructures = ifrs;
                    $scope.wash.typeOfInfrastructure = infrastructure.typeOfInfrastructure;
                });

                Restangular.all('isnews?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (irs) {
                    $scope.isnews = irs;

                    if (infrastructure.isNew == false) {
                        $scope.wash.isNew = 1;
                    } else if (infrastructure.isNew == true) {
                        $scope.wash.isNew = 2;
                    }
                });
            });
        }

        $scope.infraModel = false;

        $scope.popupObj = {};

        $scope.validatestring = '';

        $scope.Confirm = function () {

            document.getElementById('number').style.border = "";

            if ($scope.wash.dateOfMapping == '' || $scope.wash.dateOfMapping == null) {
                $scope.validatestring = $scope.validatestring + $scope.washLanguage.pleaseSelectDateOfMapping;

            } else if ($scope.wash.associatedHF == '' || $scope.wash.associatedHF == null) {
                $scope.validatestring = $scope.validatestring + $scope.washLanguage.pleaseSelectAssignedTo;

            } else if ($scope.wash.areaId == '' || $scope.wash.areaId == null) {
                $scope.validatestring = $scope.validatestring + $scope.washLanguage.pleaseSelectArea;

            } else if ($scope.wash.latitude == '' || $scope.wash.latitude == null) {
                $scope.validatestring = $scope.validatestring + $scope.washLanguage.pleaseSelectLocation;

            } else if ($scope.wash.typeOfInfrastructure == '' || $scope.wash.typeOfInfrastructure == null) {
                $scope.validatestring = $scope.validatestring + $scope.washLanguage.pleaseSelectTypeOfInfrastructure;

            } else if ($scope.wash.noOfInfrastructure == '' || $scope.wash.noOfInfrastructure == null) {
                $scope.validatestring = $scope.validatestring + $scope.washLanguage.pleaseEnterNoOfInsfrastructure;
                document.getElementById('number').style.borderColor = "#FF0000";

            } else if ($scope.wash.isNew == '' || $scope.wash.isNew == null) {
                $scope.validatestring = $scope.validatestring + $scope.washLanguage.pleaseSelectNew;
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.popupObj.dateOfMapping = $filter('date')($scope.wash.dateOfMapping, 'dd-MMM-yyyy');
                $scope.popupObj.noOfInfrastructure = $scope.wash.noOfInfrastructure;

                $scope.infraModel = true;

                if ($window.sessionStorage.language == 1) {

                    Restangular.one('users', $scope.wash.associatedHF).get().then(function (udr) {
                        Restangular.one('areas', $scope.wash.areaId).get().then(function (ara) {
                            Restangular.one('typeofinfrastructures', $scope.wash.typeOfInfrastructure).get().then(function (ifra) {
                                Restangular.one('isnews', $scope.wash.isNew).get().then(function (inew) {
                                    $scope.popupObj.assignedTo = udr.name;
                                    $scope.popupObj.areaName = ara.name;
                                    $scope.popupObj.infraName = ifra.name;
                                    $scope.popupObj.isNew = inew.name;
                                });
                            });
                        });
                    });

                } else {

                    Restangular.one('users', $scope.wash.associatedHF).get().then(function (udr) {
                        Restangular.one('areas', $scope.wash.areaId).get().then(function (ara) {
                            Restangular.one('typeofinfrastructures/findOne?filter[where][parentId]=' + $scope.wash.typeOfInfrastructure + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (tdr) {
                                Restangular.one('isnews/findOne?filter[where][parentId]=' + $scope.wash.isNew + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (inew) {
                                    $scope.popupObj.assignedTo = udr.name;
                                    $scope.popupObj.areaName = ara.name;
                                    $scope.popupObj.infraName = tdr.name;
                                    $scope.popupObj.isNew = inew.name;
                                });
                            });
                        });
                    });
                }
            }
        };

        $scope.Save = function () {

            $scope.toggleLoading();

            $scope.wash.lastModifiedDate = new Date();
            $scope.wash.lastModifiedBy = $window.sessionStorage.userId;
            $scope.wash.lastModifiedByRole = $window.sessionStorage.roleId;

            if ($scope.wash.isNew == 1) {
                $scope.wash.isNew = false;
            } else if ($scope.wash.isNew == 2) {
                $scope.wash.isNew = true;
            }

            Restangular.all('infrastructures', $routeParams.id).customPUT($scope.wash).then(function (resp) {

                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setInterval(function () {
                    window.location = '/washinfrastructure-list';
                }, 1500);
            });
        };

        /******************************google map****************************/

        $scope.mapdataModalView = false;

        $scope.Locate = function () {
            $scope.mapdataModalView = true;

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 4,

                center: new google.maps.LatLng(12.9538477, 77.3507369),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            var marker, i;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(12.9538477, 77.3507369),
                map: map,
                html: ''
            });

            $scope.toggleMapModal();
        };

        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            ///////////////////////////////////////////////////////MAP//////////////////////////

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                //  console.log(latLng);
                $scope.wash.latitude = latLng.lat();
                $scope.wash.longitude = latLng.lng();

                //  console.log('$scope.updatepromotion', $scope.updatepromotion);

                document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
            }
            var map;

            function initialize() {

                $scope.latitude = 12.9538477;
                $scope.longitude = 77.3507369;
                navigator.geolocation.getCurrentPosition(function (location) {
                    //                    console.log(location.coords.latitude);
                    //                    console.log(location.coords.longitude);
                    //                    console.log(location.coords.accuracy);
                    $scope.latitude = location.coords.latitude;
                    $scope.longitude = location.coords.longitude;
                    //                });

                    // console.log('$scope.address', $scope.address);

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 4,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });
                });


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(10);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                //  console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start
        var sevendays = new Date();

        sevendays.setDate(sevendays.getDate() + 7);

        $scope.today = function () {};

        $scope.today();

        $scope.showWeeks = true;

        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dtmin = new Date();

        $scope.dtmax = new Date();

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };

        $scope.toggleMin();

        $scope.picker = {};

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end
    });