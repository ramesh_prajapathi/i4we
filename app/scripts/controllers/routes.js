'use strict';
angular.module('secondarySalesApp').controller('RoutesCtrl', function ($scope, Restangular, $route, $window) {
    if ($window.sessionStorage.roleId != 1) {
        window.location = "/";
    }
    if ($window.sessionStorage.site_zoneId == null || $window.sessionStorage.site_zoneId == undefined || $window.sessionStorage.site_stateId == null || $window.sessionStorage.site_stateId == undefined || $window.sessionStorage.site_facilityId == null || $window.sessionStorage.site_facilityId == undefined) {
        $window.sessionStorage.site_zoneId = null;
        $window.sessionStorage.site_stateId = null;
        $window.sessionStorage.site_facilityId == null;
        $window.sessionStorage.facility_currentPage = 1;
        $window.sessionStorage.facility_currentPageSize = 25;
    }
    else {
        $scope.countryId = $window.sessionStorage.site_zoneId;
        $scope.stateId = $window.sessionStorage.site_stateId;
        $scope.facilityId = $window.sessionStorage.site_facilityId;
        $scope.currentpage = $window.sessionStorage.facility_currentPage;
        $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
    }
    if ($window.sessionStorage.prviousLocation != "partials/routes-form") {
        $window.sessionStorage.site_zoneId = '';
        $window.sessionStorage.site_stateId = '';
        $window.sessionStorage.site_facilityId = '';
        $window.sessionStorage.facility_currentPage = 1;
        $window.sessionStorage.facility_currentPageSize = 25;
    }
    $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
    $scope.pageFunction = function (mypage) {
        $scope.pageSize = mypage;
        $window.sessionStorage.facility_currentPageSize = mypage;
    };
    $scope.currentpage = $window.sessionStorage.facility_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.facility_currentPage = newPage;
    };
    $scope.getZone = function (id) {
        return Restangular.one('zones', id).get().$object;
    };
    $scope.getSalesArea = function (id) {
        return Restangular.one('sales-areas', id).get().$object;
    };
    $scope.getPartner = function (partnerId) {
        return Restangular.one('employees', partnerId).get().$object;
    };
    $scope.getDistributionSubarea = function (distributionSubareaId) {
        return Restangular.one('distribution-subareas', distributionSubareaId).get().$object;
    };
    //$scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
    $scope.znes = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
        $scope.zones = znes;
        $scope.countryId = $window.sessionStorage.site_zoneId;
    });
    $scope.searchDA = $scope.name;
    $scope.showValidation = false;
    $scope.modalTitle = 'Alert';
    $scope.message = 'There are some assignments on this site,delete them and then proceed.';
    $scope.toggleValidation = function () {
        console.log('toggleValidation');
        $scope.showValidation = !$scope.showValidation;
    };
    /************************************* DELETE *******************************************/
    $scope.Delete = function (id) {
        $scope.item = [{
            deleteflag: true
            }]
        Restangular.all('routelinks?filter[where][distributionRouteId]=' + id).getList().then(function (rtelnks) {
            if (rtelnks.length == 0) {
                Restangular.one('distribution-routes/' + id).customPUT($scope.item[0]).then(function () {
                    $route.reload();
                });
            }
            else {
                $scope.toggleValidation();
                $scope.validatestring1 = "There are some assignments on this site,delete them and then proceed.";
            }
        });
    }
    $scope.UnDelete = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('distribution-routes/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        /*************************** Watch **************************************************/
        /*$scope.Member = function (siteId) {
        	Restangular.all('beneficiaries?filter[where][site]=' + siteId + '&filter[where][limit]=10').then(function (response) {
        		console.log('MemberLength', response.length);
        	});
        };
        $scope.Member = Restangular.all('beneficiaries??filter[where][site]=' + siteId + '&filter[where][limit]=10' + '&filter[where][deleteflag]=false').getList().then(function (me) {
        	$scope.members = me;
        	//   $scope.countryId = $window.sessionStorage.site_zoneId;
        });*/
    $scope.Member = function (siteId) {
        console.log('siteId', siteId);
        /* Restangular.one('beneficiaries??filter[where][site]=siteId').get().then(function(Res){
        	 console.log('Res',Res);
         });*/
        /*Restangular.all('beneficiaries?filter[where][site]=siteId' + '&filter[where][limit]=10' + '&filter[where][deleteflag]=false').getList().then(function(Res){
        	 console.log('Res',Res);
         });*/
    };
    /*$scope.MemberCount = function (benId) {
    	console.log('conId', benId);
    	Restangular.one('beneficiaries?filter[where][site]=' + benId + '&filter[where][deleteflag]=false' + '&filter[limit]=100').get().then(function (todores) {
    		$scope.alltodo = todores;
    		console.log('todores', todores[0]);
    	});
    };*/
    /*$scope.FetchMember = function (benId) {
    	//console.log('I m calling by', benId5);
    	$scope.deleteCount51 = 0;
    	Restangular.all('todos?filter[where][facility]=' + benId5 + '&filter[where][deleteflag]=false' + '&filter[limit]=1000').getList().then(function (todores) {
    		$scope.alltodo = todores;
    		if (todores.length > 0) {
    			$scope.mainTodosToDelete = todores;
    			$scope.deleteMainTodos(benId5);
    			//$scope.Fetchtodo(benId5);
    		} else {
    			$scope.FetchreportIncidents(benId5);

    		}
    	});
    }*/
    $scope.FetchMember = function (siteId) {
        //console.log('siteId', siteId);
        Restangular.all('beneficiaries?filter[where][site]=59' + '&filter[where][deleteflag]=false' + '&filter[limit]=1000').getList().then(function (memberres) {
            $scope.allmember = memberres;
            console.log('allmember', memberres.length);
        });
    };
    $scope.countryId = '';
    $scope.salesareaId = '';
    $scope.stateId = '';
    $scope.salesid = '';
    //$scope.partnerId;
    $scope.partid = '';
    $scope.zonalid = '';
    //$scope.distributionSubareaId = '';
    $scope.facilityId = '';
    $scope.displayroutes = {};
    $scope.$watch('countryId', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
            return;
        }
        else {
            $window.sessionStorage.site_zoneId = newValue;
            $scope.facilities = {};
            //Restangular.all('beneficiaries?filter[where][state]=' + newValue + '&filter[where][deleteflag]=false' + '&filter[where][limit]=100').getList().then(function (Members) {
            //	$scope.Members = Members;
            $scope.salesareas1 = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (SARes) {
                $scope.salesareas = SARes;
                $scope.stateId = $window.sessionStorage.site_stateId;
                Restangular.all('routelinks?filter[where][deleteflag]=false').getList().then(function (routelinks) {
                    $scope.con = Restangular.all('distribution-routes?filter[where][zoneId]=' + newValue).getList().then(function (con) {
                        $scope.displayroutes = con;
                        //member.id = $scope.MemberCount(con.id);
                        angular.forEach($scope.displayroutes, function (member, index) {
                            //console.log('member', member.id);
                            //$scope.FetchMember(member.id);
                            member.index = index + 1;
                            member.colour = "#29DF66";
                            /*
                            Restangular.all('routelinks?filter[where][distributionRouteId]=' +member.id).getList().then(function(routelinks){
                            	console.log('routelinks',routelinks);
                            });*/
                            if (member.deleteflag === true) {
                                member.colour = "#D53118";
                            }
                            /*for (var j = 0; j < Members.length; j++) {
                            	if (Members[j].site == member.id) {
                            		//member.colour = "";
                            		console.log('Members', Members[j].site);
                            		$scope.Membercount = Members[j].fullname;
                            		break;
                            	}
                            	else if (Members[j].site != member.id) {
                            		//member.colour = "#F33226";
                            		break;
                            	}
                            }*/
                            //$scope.count2 = 0;
                            //$scope.dummyarray = [];
                            //for (var j = 0; j < Members.length; j++) {
                            for (var i = 0; i < routelinks.length; i++) {
                                if (routelinks[i].distributionRouteId == member.id) {
                                           member.colour = "";
                                    //member.totalamounts = $scope.FetchMember(member.id);
                                    break;
                                }
                                else if (routelinks[i].distributionRouteId != member.id && member.deleteflag == true) {
                                    member.colour = "#F33226";
                                    break;
                                }
                            }
                            //	}
                            //});
                        });
                    });
                });
            });
            $scope.zonalid = +newValue;
        }
    });
    /***************************************************/
    /*$scope.FetchMember = function (benId) {
            return Restangular.all('beneficiaries?filter[where][site]=' + benId + '&filter[where][deleteflag]=false').getList().$object;
          }*/
        /***************************************************/
        //--------------------------------------------------------------------------    
    $scope.$watch('stateId', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
            return;
        }
        else {
            $window.sessionStorage.site_stateId = newValue;
            $scope.facilit = Restangular.all('employees?filter[where][deleteflag]=false' + '&filter[where][stateId]=' + $scope.zonalid + '&filter[where][district]=' + newValue).getList().then(function (FscRes) {
                $scope.facilities = FscRes;
                $scope.facilityId = $window.sessionStorage.site_facilityId;
                /*$scope.Rt = Restangular.all('distribution-routes?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (Rt) {
                    $scope.displayroutes = Rt;
                    angular.forEach($scope.displayroutes, function (member, index) {
                        member.index = index + 1;
                    });
                });*/
                Restangular.all('routelinks?filter[where][deleteflag]=false').getList().then(function (routelinks) {
                    $scope.con = Restangular.all('distribution-routes?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + newValue).getList().then(function (con) {
                        $scope.displayroutes = con;
                        angular.forEach($scope.displayroutes, function (member, index) {
                            member.index = index + 1;
                            member.colour = "#29DF66";
                            for (var i = 0; i < routelinks.length; i++) {
                                if (routelinks[i].distributionRouteId == member.id) {
                                    member.colour = "";
                                    //member.totalamounts = $scope.FetchMember(member.id);
                                    break;
                                }
                                else if (routelinks[i].distributionRouteId != member.id && member.deleteflag == true) {
                                    member.colour = "#F33226";
                                    break;
                                }
                            }
                        });
                    });
                });
            });
            $scope.salesid = +newValue;
        }
    });
    $scope.$watch('facilityId', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
            return;
        }
        else {
            $window.sessionStorage.site_facilityId = newValue;
            /*$scope.Rt = Restangular.all('distribution-routes?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + $scope.salesid + '&filter[where][partnerId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (Rt) {
                $scope.displayroutes = Rt;
                angular.forEach($scope.displayroutes, function (member, index) {
                    member.index = index + 1;
                });
            });*/
            Restangular.all('routelinks?filter[where][deleteflag]=false').getList().then(function (routelinks) {
                $scope.con = Restangular.all('distribution-routes?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + $scope.salesid + '&filter[where][partnerId]=' + newValue).getList().then(function (con) {
                    $scope.displayroutes = con;
                    angular.forEach($scope.displayroutes, function (member, index) {
                        member.index = index + 1;
                        member.colour = "#29DF66"; //Assigned
                        for (var i = 0; i < routelinks.length; i++) {
                            if (routelinks[i].distributionRouteId == member.id) {
                                member.colour = ""; //Assigned
                                //member.totalamounts = $scope.FetchMember(member.id);
                                break;
                            }
                            else if (routelinks[i].distributionRouteId != member.id && member.deleteflag == true) {
                                member.colour = "#F33226";
                                break;
                            }
                        }
                    });
                });
            });
            $scope.partid = +newValue;
        }
    });
});