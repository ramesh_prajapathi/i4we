'use strict';
angular.module('secondarySalesApp')
    .controller('ReportIncidentEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/

        if ($window.sessionStorage.roleId == 1) {
            window.location = "/";
        }

        $scope.RILanguage = {};

        $scope.HideSubmitButton = true;
        $scope.Violence = false;
        $scope.MedicalEmergency = false;
        $scope.SAWFIssue = false;
        $scope.Showmember = false;
        $scope.ShowmemberOne = false;
        $scope.hideReportedTo = false;
        $scope.dateofclosureHide = false;
        $scope.followupHide = false;

        $scope.reportincident = {};

        Restangular.one('riLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.RILanguage = langResponse[0];
            $scope.RiHeading = $scope.RILanguage.riEdit;
            // $scope.modalTitle = $scope.RILanguage.thankYou;
            $scope.message = $scope.RILanguage.reportIncidentUpdated;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        $scope.auditlog = {
            action: 'Update',
            module: 'Report Incident',
            owner: $window.sessionStorage.userId,
            datetime: new Date(),
            details: 'Report Incident Updated',
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        };
        /***********************************************************************/

        $scope.HideSubmitButton = true;

        /******** Get data using Routeparams *************************/

        if ($routeParams.id) {
            Restangular.one('reportincidents', $routeParams.id).get().then(function (ri) {
                $scope.original = ri;
                $scope.reportincident = Restangular.copy($scope.original);
                $scope.reportincident.multiplemembers = ri.multiplemembers.split(',');
            });
        }

        /**************************************** Member *******************************************/

        $scope.UserLanguage = $window.sessionStorage.language;

        Restangular.all('typeofincidents?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (incident) {
            $scope.typeofincidents = incident;
        });

        Restangular.all('severityofincidents?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (responseseservity) {
            $scope.severityofincidents = responseseservity;
        });

        Restangular.all('currentstatusofcases?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (response) {
            $scope.currentstatusofcases = response;
        });
    
        if ($window.sessionStorage.roleId + "" === "3") {
            
            $scope.hideAssigned = true;
            
            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.reportincident.associatedHF = $window.sessionStorage.userId;

            });
        } else {
            
            $scope.hideAssigned = false;
            
            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.reportincident.associatedHF = $scope.reportincident.associatedHF;
            });
        }
    
        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.memberFilterCall = 'members?filter[where][deleteFlag]=false&filter[where][associatedHF]=' + $window.sessionStorage.userId;
        } else {
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        }
        Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
            $scope.members = mems;

            angular.forEach($scope.members, function (member, index) {
                member.index = index;
            });
        });

        Restangular.all('reportincidentfollowups?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (follow) {
            $scope.reportincidentfollowups = follow;
        });


        $scope.$watch('reportincident.incidenttype', function (newValue, oldValue) {
            if (newValue === oldValue || newValue === '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                if (newValue == 1) {
                    $scope.Violence = true;
                    $scope.MedicalEmergency = false;
                    $scope.SAWFIssue = false;

                    // $scope.reportincident.reported = false;

                    $scope.reportincident.wound = false;
                    $scope.reportincident.cut = false;
                    $scope.reportincident.severepain = false;
                    $scope.reportincident.bleeding = false;
                    $scope.reportincident.immobile = false;
                    $scope.reportincident.passingout = false;
                    $scope.reportincident.uncoveredfood = false;
                    $scope.reportincident.waterstagnation = false;
                    $scope.reportincident.unclearedgarbage = false;

                    $scope.reportincident.emergencyactionclinic = false;
                    $scope.reportincident.referredhospital = false;

                    $scope.reportincident.complaintmade = false;
                    $scope.reportincident.communitymobilized = false;
                    $scope.reportincident.prioritizedflorsawfaction = false;

                    if ($scope.reportincident.co == false) {
                        $scope.Showmember = false;
                        $scope.ShowmemberOne = true;
                        // $scope.reportincident.multiplemembers = '';

                    } else if ($scope.reportincident.co == true) {
                        $scope.Showmember = true;
                        $scope.ShowmemberOne = false;
                        //$scope.reportincident.multiplemembers = '';
                    }

                } else if (newValue == 2) {
                    $scope.Violence = false;
                    $scope.MedicalEmergency = true;
                    $scope.SAWFIssue = false;

                    $scope.reportincident.reported = false;

                    $scope.reportincident.physical = false;
                    $scope.reportincident.sexual = false;
                    $scope.reportincident.childrelated = false;
                    $scope.reportincident.emotional = false;
                    $scope.reportincident.propertyrelated = false;
                    $scope.reportincident.mental = false;
                    $scope.reportincident.uncoveredfood = false;
                    $scope.reportincident.waterstagnation = false;
                    $scope.reportincident.unclearedgarbage = false;

                    $scope.reportincident.police = false;
                    $scope.reportincident.fatherinlaw = false;
                    $scope.reportincident.motherinlaw = false;
                    $scope.reportincident.sisbroinlaw = false;
                    $scope.reportincident.sondaughter = false;
                    $scope.reportincident.othermem = false;
                    $scope.reportincident.husfriends = false;
                    $scope.reportincident.goons = false;
                    $scope.reportincident.partners = false;
                    $scope.reportincident.husband = false;
                    $scope.reportincident.anyfamilymember = false;
                    $scope.reportincident.daughterinlaw = false;
                    $scope.reportincident.landlord = false;
                    $scope.reportincident.familymember = false;

                    $scope.reportincident.referredhf = false;
                    $scope.reportincident.referredmedicalcare = false;
                    $scope.reportincident.referredcomanager = false;
                    $scope.reportincident.referredheadofgrampanchayat = false;
                    $scope.reportincident.referredplv = false;
                    $scope.reportincident.referredlegalaid = false;
                    $scope.reportincident.referredpolice = false;
                    $scope.reportincident.referredsgshg = false;

                    $scope.reportincident.complaintmade = false;
                    $scope.reportincident.communitymobilized = false;
                    $scope.reportincident.prioritizedflorsawfaction = false;

                    if ($scope.reportincident.co == false) {
                        $scope.Showmember = false;
                        $scope.ShowmemberOne = true;
                        // $scope.reportincident.multiplemembers = '';
                    } else if ($scope.reportincident.co == true) {
                        $scope.Showmember = true;
                        $scope.ShowmemberOne = false;
                        // $scope.reportincident.multiplemembers = '';
                    }

                } else if (newValue == 3) {
                    $scope.Violence = false;
                    $scope.MedicalEmergency = false;
                    $scope.SAWFIssue = true;
                    $scope.Showmember = false;
                    $scope.ShowmemberOne = false;
                    $scope.reportincident.multiplemembers = '';

                    $scope.reportincident.reported = false;

                    $scope.reportincident.physical = false;
                    $scope.reportincident.sexual = false;
                    $scope.reportincident.childrelated = false;
                    $scope.reportincident.emotional = false;
                    $scope.reportincident.propertyrelated = false;
                    $scope.reportincident.mental = false;
                    $scope.reportincident.wound = false;
                    $scope.reportincident.cut = false;
                    $scope.reportincident.severepain = false;
                    $scope.reportincident.bleeding = false;
                    $scope.reportincident.immobile = false;
                    $scope.reportincident.passingout = false;

                    $scope.reportincident.police = false;
                    $scope.reportincident.fatherinlaw = false;
                    $scope.reportincident.motherinlaw = false;
                    $scope.reportincident.sisbroinlaw = false;
                    $scope.reportincident.sondaughter = false;
                    $scope.reportincident.othermem = false;
                    $scope.reportincident.husfriends = false;
                    $scope.reportincident.goons = false;
                    $scope.reportincident.partners = false;
                    $scope.reportincident.husband = false;
                    $scope.reportincident.anyfamilymember = false;
                    $scope.reportincident.daughterinlaw = false;
                    $scope.reportincident.landlord = false;
                    $scope.reportincident.familymember = false;

                    $scope.reportincident.referredhf = false;
                    $scope.reportincident.referredmedicalcare = false;
                    $scope.reportincident.referredcomanager = false;
                    $scope.reportincident.referredheadofgrampanchayat = false;
                    $scope.reportincident.referredplv = false;
                    $scope.reportincident.referredlegalaid = false;
                    $scope.reportincident.referredpolice = false;
                    $scope.reportincident.referredsgshg = false;

                    $scope.reportincident.emergencyactionclinic = false;
                    $scope.reportincident.referredhospital = false;
                }
            }
        });

        $scope.$watch('reportincident.co', function (newValue, oldValue) {
            if (newValue === oldValue || newValue === '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                if (newValue == false && ($scope.reportincident.incidenttype == 1 || $scope.reportincident.incidenttype == 2)) {
                    $scope.Showmember = false;
                    $scope.ShowmemberOne = true;
                    $scope.reportincident.multiplemembers = '';
                } else if (newValue == true && ($scope.reportincident.incidenttype == 1 || $scope.reportincident.incidenttype == 2)) {
                    $scope.Showmember = true;
                    $scope.ShowmemberOne = false;
                    $scope.reportincident.multiplemembers = '';
                }
            }
        });

        $scope.$watch('reportincident.reported', function (newValue, oldValue) {
            if (newValue === oldValue || newValue === '') {
                return;
            } else {
                if (newValue == false) {
                    $scope.hideReportedTo = false;
                    $scope.reportincident.reportedpolice = false;
                    $scope.reportincident.reportedngos = false;
                    $scope.reportincident.reportedfriends = false;
                    $scope.reportincident.reportedplv = false;
                    $scope.reportincident.shshg = false;
                    $scope.reportincident.hfs = false;
                    $scope.reportincident.reportedchampions = false;
                    $scope.reportincident.doctornurse = false;
                    $scope.reportincident.reportedlegalaid = false;
                    $scope.reportincident.grampanchayat = false;
                    $scope.reportincident.timetorespond = '';
                } else if (newValue == true) {
                    $scope.hideReportedTo = true;
                }
            }
        });

        $scope.$watch('reportincident.currentstatus', function (newValue, oldValue) {
            if (newValue === oldValue || newValue === '') {
                return;
            } else {
                if (newValue == 5 || newValue == 9) {
                    $scope.followupHide = false;
                    $scope.dateofclosureHide = true;
                    $scope.reportincident.followupneeded = null;
                    $scope.reportincident.followupdate = null;
                    $scope.reportincident.dateofclosure = new Date();
                } else {
                    $scope.followupHide = true;
                    $scope.dateofclosureHide = false;
                    $scope.reportincident.followupneeded = 1;
                    var sevendays = new Date();
                    sevendays.setDate(sevendays.getDate() + 7);
                    $scope.reportincident.followupdate = sevendays;
                }
            }
        });

        $scope.validatestring = '';

        $scope.UpdateReportIncident = function () {

            if ($scope.reportincident.incidenttype == '' || $scope.reportincident.incidenttype == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Incident Type';

            } else if ($scope.reportincident.severity == '' || $scope.reportincident.severity == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Severity of Incident';

            } else if ($scope.reportincident.incidenttype == 1 || $scope.reportincident.incidenttype == 2) {
                if ($scope.reportincident.multiplemembers == '' || $scope.reportincident.multiplemembers == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Members';
                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.reportincident.lastModifiedDate = new Date();
                $scope.reportincident.lastModifiedBy = $window.sessionStorage.userId;
                $scope.reportincident.lastModifiedByRole = $window.sessionStorage.roleId;

                $scope.toggleLoading();
                $scope.message = $scope.RILanguage.reportIncidentUpdated;
                $scope.submitDisable = true;

                Restangular.one('reportincidents', $routeParams.id).customPUT($scope.reportincident).then(function (Response) {

                    $scope.auditlog.rowId = Response.id;

                    Restangular.all('audittrials').post($scope.auditlog).then(function (respAudit) {
                        $scope.modalInstanceLoad.close();
                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        console.log('reloading...');

                        setInterval(function () {
                            window.location = '/reportincident-list';
                        }, 1500);
                    });
                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.reportincident.incidentdate = new Date();
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        $scope.reportincident.followupdate = sevendays;


        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.open2 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepickerfollowup' + index).focus();
            });
            $scope.reportincident.followupopened = true;
        };

        $scope.reportincidentfollowupopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.reportincident.followupdatepick = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end///


    });