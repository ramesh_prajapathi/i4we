'use strict';
angular.module('secondarySalesApp').controller('AgentRoutelinksCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter, $document) {
    /*********/
    $scope.showForm = function () {
        var visible = $location.path() === '/assignfwtosite/create' || $location.path() === '/assignfwtosite/' + $routeParams.id;
        return visible;
    };
    $scope.isCreateView = function () {
        if ($scope.showForm()) {
            var visible = $location.path() === '/assignfwtosite/create';
            return visible;
        }
    };
    $scope.hideCreateButton = function () {
        var visible = $location.path() === '/assignfwtosite/create' || $location.path() === '/assignfwtosite/' + $routeParams.id;
        return visible;
    };
    $scope.hideSearchFilter = function () {
        var visible = $location.path() === '/assignfwtosite/create' || $location.path() === '/assignfwtosite/' + $routeParams.id;
        return visible;
    };
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    if ($window.sessionStorage.prviousLocation != "partials/agentroutelinks") {
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
    };
    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    /************************************************************INDEX**************************/
    /*	$scope.routelink = {
				flag: 'A',
				lastmodifiedtime: new Date(),
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				facilityId: $window.sessionStorage.UserEmployeeId,
				facility: $window.sessionStorage.coorgId,
				state: $window.sessionStorage.zoneId,
				district: $window.sessionStorage.salesAreaId,
				deleteflag: false
			}*/
    $scope.auditlog = {
        modifiedbyroleid: $window.sessionStorage.roleId,
        modifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        entityroleid: 52,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        facility: $window.sessionStorage.coorgId
    };
    /************************************* Not In Use ******************************
				Restangular.one('zones', $window.sessionStorage.zoneId).get().then(function (zone) {
					$scope.routelink.zoneId = zone.id;
				});

				Restangular.one('sales-areas', $window.sessionStorage.salesAreaId).get().then(function (salesarea) {
					$scope.routelink.salesAreaId = salesarea.id;
				});

					$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;
				$scope.submitfieldworkers = Restangular.all('fieldworkers').getList().$object;


			

				//$scope.partners = Restangular.all('fieldworkers?filter[where][deleteflag]=null' + '&filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][facilityId]=' + $window.sessionStorage.coorgId).getList().$object;
				
				$scope.partners = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (response) {
					$scope.partners = response;
				});

				$scope.routes = Restangular.all('distribution-routes?filter[where][deleteflag]=false&filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().$object;


				//console.log('$window.sessionStorage.UserEmployeeId', $window.sessionStorage.UserEmployeeId);

				$scope.getPartner = function (partnerId) {
					return Restangular.one('fieldworkers', partnerId).get().$object;
				};

				$scope.getdistributionRoute = function (distributionRouteId) {
					return Restangular.one('distribution-routes', distributionRouteId).get().$object;
				};


				$("#Search").keyup(function () {
					var data = this.value.toUpperCase().split(" ");
					var js = $("#bnbody").find("tr");
					if (this.value == "") {
						js.show();
						return;
					}
					js.hide();
					js.filter(function (i, v) {
						var $t = $(this);
						for (var d = 0; d < data.length; ++d) {
							if ($t.text().toUpperCase().indexOf(data[d]) > -1) {
								return true;
							}
						}
						return false;
					}).show();
				});
			$scope.routelinks = Restangular.all('routelinks').getList().$object;
			
				$scope.con = Restangular.all('routelinks?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (con) {
			$scope.displayroutelinks = con;
			$scope.modalInstanceLoad.close();
			angular.forEach($scope.displayroutelinks, function (member, index) {
				member.index = index + 1;
			});
		});
		$scope.country = {};


		/*******************************************************************/
    /* $scope.FWFilter = function () {
             $scope.za = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (za) {
                 $scope.partners = za;
                 $scope.ds = Restangular.all('distribution-routes?filter[where][deleteflag]=false&filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().then(function (ds) {
                     $scope.routes = ds;
                     $scope.zn = Restangular.all('routelinks?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=distributionRouteId%20ASC').getList().then(function (zn) {
                         $scope.displayroutelinks = zn;
                         // console.log(' $scope.displayroutelinks', $scope.displayroutelinks);
                         $scope.modalInstanceLoad.close();
                         angular.forEach($scope.displayroutelinks, function (member, index) {
                             member.index = index + 1;
                             for (var m = 0; m < $scope.partners.length; m++) {
                                 if (member.partnerId == $scope.partners[m].id) {
                                     member.FieldworkerName = $scope.partners[m];
                                     break;

                                 }
                             }
                             for (var n = 0; n < $scope.routes.length; n++) {
                                 if (member.distributionRouteId == $scope.routes[n].id) {
                                     member.Sitename = $scope.routes[n];
                                     break;
                                 }
                             }
                             $scope.TotalData = [];
                             $scope.TotalData.push(member);

                         });
                     });
                 })
             });
         }*/
    //$scope.AssignedSitesMain = [];
    $scope.$watch('routelink.partnerId', function (newValue, oldValue) {
        $scope.currPartnerId = newValue;
        //console.log('$scope.currPartnerId', $scope.currPartnerId);
        if (newValue === oldValue || newValue == "") {
            return;
        } else if ($routeParams.id && (oldValue == null || oldValue == undefined)) {
            return;
        } else {
            $scope.exitingroutes = {};
            //console.log('newValue', newValue);
            Restangular.one('fieldworkers', newValue).get().then(function (fldworker) {
                $scope.AssignedSitesMain = fldworker.sitesassigned;
                console.log('$scope.AssignedSitesMain', $scope.AssignedSitesMain);
                var Array = [];
                Array.push(fldworker.sitesassigned);
                $scope.no = 275;
                //console.log('$scope.AssignedSitesMain', Array);
                //Restangular.all('distribution-routes?filter{where: {id: {inq: ['+$scope.no+']}}}').getList().then(function(dsRes){
                $scope.part = Restangular.all('distribution-routes?filter={"where":{"and":[{"id":{"inq":[' + Array + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (dsRes) {
                    $scope.exitingroutes = dsRes;
                    //console.log('dsRes', dsRes.length);
                });
            });
        }
    });
    /*********************************** iNDEX **************************************/

    $scope.za = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (za) {
        $scope.partners = za;
        $scope.ds = Restangular.all('distribution-routes?filter[where][deleteflag]=false&filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().then(function (ds) {
            $scope.routes = ds;
            $scope.zn = Restangular.all('routelinks?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=partnerId%20ASC').getList().then(function (zn) {
                //+ '&filter[order]=distributionRouteId%20ASC'
                $scope.displayroutelinks = zn;
                $scope.modalInstanceLoad.close();
                angular.forEach($scope.displayroutelinks, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.partners.length; m++) {
                        if (member.partnerId == $scope.partners[m].id) {
                            member.FieldworkerName = $scope.partners[m].firstname;
                            /* $scope.sampleArry.push(
                                 $scope.partners[m]
                             );*/


                            break;
                        }
                    }

                    for (var n = 0; n < $scope.routes.length; n++) {
                        if (member.distributionRouteId == $scope.routes[n].id) {
                            member.Sitename = $scope.routes[n].name;
                            member.SitenameId = $scope.routes[n].id;
                            break;
                        }
                    }
                    $scope.TotalData = [];
                    $scope.TotalData.push(member);

                });

                /* console.log('$scope.displayroutelinks', $scope.displayroutelinks);
                  $scope.sort_by = function (FieldworkerName) {
                      $scope.displayroutelinks.sort(function (a1, b1) {
                          var a = parseInt(a1.FieldworkerName.match(/\d+/g)[0], 10),
                              b = parseInt(b1.FieldworkerName.match(/\d+/g)[0], 10),
                              letterA = a1.FieldworkerName.charCodeAt(0),
                              letterB = b1.FieldworkerName.charCodeAt(0);
                          if (letterA > letterB) {
                              return 1;
                          } else if (letterB > letterA) {
                              return -1;
                          } else {
                              if (a < b) return -1;
                              if (a > b) return 1;
                              return 0;
                          }
                      });
                  }*/

            });

        })
    });


    /********************/
    //    $scope.sortingOrder = sortingOrder;
    /*  $scope.reverse = false;

      $scope.sort_by = function (newSortingOrder) {
          if ($scope.sortingOrder == newSortingOrder)
              $scope.reverse = !$scope.reverse;
          $scope.sortingOrder = newSortingOrder;
      };*/

   /*$scope.sort = {
        active: '',
        descending: undefined
    }

    $scope.changeSorting = function (column) {

        var sort = $scope.sort;

        if (sort.active == column) {
            sort.descending = !sort.descending;

        } else {
            sort.active = column;
            sort.descending = false;
        }
    };

    $scope.getIcon = function (column) {

        var sort = $scope.sort;

        if (sort.active == column) {
            return sort.descending ? 'glyphicon-chevron-up' : 'glyphicon-chevron-down';
        }
    }*/


    /**************************/
    var regex = /^([a-z]*)(\d*)/i;
    $scope.boolAsc = false;
    $scope.changeSorting = function (col) {
            if ($scope.boolAsc) {
                $scope.ascendingSorting(col)
            } else {
                $scope.descendingSorting(col)
            }
        }
        /***********Icon*********/
    $scope.getIcon = function (col) {
        if ($scope.boolAsc) {

            return 'fa-sort-up';
            //return 'glyphicon-chevron-down';
        } else {
            return 'fa-sort-desc';
        }
    }

    $scope.ascendingSorting = function (column) {
        var x = column;

        function sortFn(a, b) {
            var _a = a[x].match(regex);
            var _b = b[x].match(regex);

            // if the alphabetic part of a is less than that of b => -1
            if (_a[1] < _b[1]) return -1;
            // if the alphabetic part of a is greater than that of b => 1
            if (_a[1] > _b[1]) return 1;

            // if the alphabetic parts are equal, check the number parts
            var _n = parseInt(_a[2]) - parseInt(_b[2]);
            /* if (_n == 0) // if the number parts are equal start a recursive test on the rest
                 return sortFn(a[x](_a[0].length), b.substr(_b[0].length));*/
            // else, just sort using the numbers parts
            return _n;
        }
        $scope.boolAsc = !$scope.boolAsc;
        $scope.displayroutelinks.sort(sortFn);
    };

    $scope.descendingSorting = function (column) {
        var y = column;


        function sortFn(a, b) {
            var _a = a[y].match(regex);
            var _b = b[y].match(regex);

            // if the alphabetic part of a is less than that of b => -1
            if (_a[1] < _b[1]) return 1;
            // if the alphabetic part of a is greater than that of b => 1
            if (_a[1] > _b[1]) return -1;

            // if the alphabetic parts are equal, check the number parts
            var _n = parseInt(_b[2]) - parseInt(_a[2]);
            /* if (_n == 0) // if the number parts are equal start a recursive test on the rest
                 return sortFn(a[y](_a[0].length), b.substr(_b[0].length));*/
            // else, just sort using the numbers parts
            return _n;
        }

        $scope.boolAsc = !$scope.boolAsc;
        $scope.displayroutelinks.sort(sortFn);
    };




    /****************************** Try to get Unassigned Routes ************************/
    $scope.Equalvalue = [];
    $scope.UnEqualValue = [];
    $scope.totaldisplayroutelinksid = [];
    $scope.totalroutelinkid = [];
    $scope.ds = Restangular.all('distribution-routes?filter[where][deleteflag]=false&filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().then(function (ds) {
        $scope.dsroutes = ds;
        $scope.zn = Restangular.all('routelinks?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (zn) {
            $scope.dsroutelinks = zn;
            for (var i = 0; i < $scope.dsroutes.length; i++) {
                $scope.totaldisplayroutelinksid.push($scope.dsroutes[i].id);
                $scope.totaldisplayroutelinksid.toString();
                for (var j = 0; j < $scope.dsroutelinks.length; j++) {
                    $scope.totalroutelinkid.push($scope.dsroutelinks[j].distributionRouteId);
                    $scope.totalroutelinkid.toString();
                }
            }
            var index;
            for (var k = 0; k < $scope.totalroutelinkid.length; k++) {
                index = $scope.totaldisplayroutelinksid.indexOf($scope.totalroutelinkid[k]);
                if (index > -1) {
                    $scope.totaldisplayroutelinksid.splice(index, 1);
                }
            }
            //console.log('$scope.totaldisplayroutelinksid2', $scope.totaldisplayroutelinksid);
            $scope.part = Restangular.all('distribution-routes?filter={"where":{"and":[{"id":{"inq":[' + $scope.totaldisplayroutelinksid + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (dsRes) {
                $scope.availableroutes = dsRes;
                //console.log('dsRes', dsRes.length);
            });
        });
    });
    /*$scope.ds = Restangular.all('distribution-routes?filter[where][deleteflag]=false&filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().then(function (ds) {
			$scope.dsroutes = ds;
			$scope.zn = Restangular.all('routelinks?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (zn) {
				$scope.dsroutelinks = zn;
				angular.forEach($scope.dsroutelinks, function (member, index) {
					member.index = index + 1;
					for (var i = 0; i < $scope.dsroutes.length; i++) {
						if ($scope.dsroutes[i].id != member.distributionRouteId) {
							//$scope.Equalvalue.push($scope.dsroutes[i].id);
							$scope.Equalvalue.splice(i, 1);
							$scope.Equalvalue.toString();
							console.log('Equalvalue', $scope.Equalvalue);
						} else {
							$scope.UnEqualValue.push($scope.dsroutes[i].id);
							$scope.UnEqualValue.toString();
							console.log('$scope.UnEqualValue', $scope.UnEqualValue);
						}
					}
				});
			});
		});*/
    /***************************************** Old CREATE************************************
		$scope.agenttoutelinkModal = false;
		$scope.insertCount = 0;
		$scope.Save2 = function () {
			if ($scope.insertCount == 0) {
				if ($scope.routelink.partnerId != null && $scope.routelink.partnerId != "" && $scope.routelink.partnerId != undefined &&
					$scope.routelink.distributionRouteId != null && $scope.routelink.distributionRouteId != "" && $scope.routelink.partnerId != undefined) {
					$scope.insertCount++;
					//$scope.agenttoutelinkModal = !$scope.agenttoutelinkModal;
					//$scope.routelinks.post($scope.routelink).then(function (response) {
					Restangular.all('routelinks').post($scope.routelink).then(function (response) {
						$scope.auditlog.entityid = response.id;

						var respdate = $filter('date')(response.lastmodifiedtime, 'dd-MMMM-yyyy');
						$scope.auditlog.description = 'FW Site Link Created With Following Details: ' + 'FieldWorker Id - ' + response.partnerId + ', ' + 'SiteId - ' + response.distributionRouteId + ', ' + 'Modied Date - ' + respdate;


						if ($scope.AssignedSitesMain == null) {
							$scope.AssignedSitesMain = response.distributionRouteId;

						} else {
							if ($scope.AssignedSitesMain.split(',').indexOf(response.distributionRouteId + "") == -1) {
								$scope.AssignedSitesMain = $scope.AssignedSitesMain + ',' + response.distributionRouteId;
							} else {
								$scope.AssignedSitesMain = $scope.AssignedSitesMain;
							}
							//$scope.AssignedSitesMain = $scope.AssignedSitesMain + ',' + response.distributionRouteId;
						}
						$scope.AssignedFieldSite = {
							sitesassigned: $scope.AssignedSitesMain,
							id: $scope.routelink.partnerId
						};
						//$scope.submitfieldworkers.customPUT($scope.AssignedFieldSite).then(function (responsefield) {
						//$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
						Restangular.one('fieldworkers', response.partnerId).customPUT($scope.AssignedFieldSite).then(function (responsefield) {
							Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
								console.log('responseaudit', responseaudit);
								window.location = '/assignfwtosite';
							});
						});
					}, function (response) {
						//alert(response.data.error.detail);
						$scope.agenttoutelinkModal = !$scope.agenttoutelinkModal;
						$scope.message = 'Site Already Assigned';
						console.error(response);
						$scope.insertCount = 0;
					});
				} else {
					$scope.agenttoutelinkModal = !$scope.agenttoutelinkModal;
					$scope.message = 'Select All Fields';
					$scope.insertCount = 0;
				}
			}
		};*/
    $scope.Sitechange = function (cng) {
            $scope.getNewChanges = cng;
        }
        /*++++++++++++++++++++++++++++++++Routelink Save +++++++++++++++++++++++++*/
    $scope.agenttoutelinkModal = false;
    $scope.saveDisable = false;
    $scope.insertCount = 0;
    $scope.validatestring = '';
   
    $scope.thankyouModal = false;
    $scope.Save = function () {
        $scope.routelink.distributionRouteId = null;
        var inputElements = document.getElementsByClassName('messageCheckbox');
        var count = 0;
        if ($scope.routelink.partnerId == '' || $scope.routelink.partnerId == null) {
            $scope.validatestring = $scope.validatestring + $scope.selfielworker;
        }
        /*else if (myval.checked == undefined || myval.checked == false) {
				$scope.validatestring = $scope.validatestring + 'Please Select Available Site';
			}*/
        else if ($scope.getNewChanges == undefined || $scope.getNewChanges == false) {
            $scope.validatestring = $scope.validatestring + $scope.selavailablesite;
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            $scope.thankyouModal = !$scope.thankyouModal;
            $scope.saveDisable = true;
            for (var i = 0; inputElements[i]; ++i) {
                if (inputElements[i].checked) {
                    count++;
                    $scope.getcount = count;
                    for (var j = 0; j < count; j++) {
                        $scope.checkval = inputElements[i].value;
                        $scope.routelink = {
                            flag: 'A',
                            lastmodifiedtime: new Date(),
                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                            facilityId: $window.sessionStorage.UserEmployeeId,
                            facility: $window.sessionStorage.coorgId,
                            state: $window.sessionStorage.zoneId,
                            district: $window.sessionStorage.salesAreaId,
                            deleteflag: false,
                            distributionRouteId: $scope.checkval,
                            partnerId: $scope.currPartnerId
                        }
                        Restangular.all('routelinks').post($scope.routelink).then(function (response) {
                            //console.log('save routelinks', response);
                            $scope.auditlog.entityid = response.id;
                            var respdate = $filter('date')(response.lastmodifiedtime, 'dd-MMMM-yyyy');
                            $scope.auditlog.description = 'FW Site Link Created With Following Details: ' + 'FieldWorker Id - ' + response.partnerId + ', ' + 'SiteId - ' + response.distributionRouteId + ', ' + 'Modied Date - ' + respdate;
                            if ($scope.AssignedSitesMain == null) {
                                $scope.AssignedSitesMain = response.distributionRouteId;
                            } else {
                                $scope.AssignedSitesMain = $scope.AssignedSitesMain + ',' + response.distributionRouteId;
                            }
                            $scope.AssignedFieldSite = {
                                sitesassigned: $scope.AssignedSitesMain,
                                id: $scope.routelink.partnerId
                            };
                            Restangular.one('fieldworkers', response.partnerId).customPUT($scope.AssignedFieldSite).then(function (responsefield) {
                                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                                    window.location = '/assignfwtosite';
                                });
                            });
                        });
                    };
                    //};
                }
            }
        };
    };
    /*-------------------------------------------------------------------------*/
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    /*************************************UPDATE****************************

		$scope.updateCount = 0; $scope.Update = function () {
			if ($scope.updateCount == 0) {
				$scope.updateCount++;
				$scope.AssignedSites = null;
				console.log('$scope.routelink.partnerId', $scope.routelink.partnerId);
				console.log('$scope.original.partnerId', $scope.original.partnerId);
				if ($scope.routelink.partnerId != $scope.original.partnerId) {
					Restangular.one('fieldworkers', $scope.original.partnerId).get().then(function (fldwrkr) {
						$scope.Assignedarray = fldwrkr.sitesassigned.split(',');
						// console.log('$scope.Assignedarray Before', $scope.Assignedarray);
						$scope.Assignedarray.splice($scope.Assignedarray.indexOf('' + $scope.original.distributionRouteId), 1);
						// console.log('$scope.Assignedarray', $scope.Assignedarray);
						for (var i = 0; i < $scope.Assignedarray.length; i++) {
							if (i == 0) {
								$scope.AssignedSites = $scope.Assignedarray[i];
							} else {
								$scope.AssignedSites = $scope.AssignedSites + ',' + $scope.Assignedarray[i];
							}
							//console.log('$scope.AssignedSites',$scope.AssignedSites);
						}
						$scope.AssignedFieldSite = {
							sitesassigned: $scope.AssignedSites,
							id: $scope.original.partnerId
						};
						console.log('$scope.AssignedFieldSite', $scope.AssignedFieldSite);
						//$scope.submitfieldworkers.customPUT($scope.AssignedFieldSite).then(function (responsefield) {
						Restangular.one('fieldworkers', $scope.original.partnerId).customPUT($scope.AssignedFieldSite).then(function (responsefield) {
							Restangular.one('fieldworkers', $scope.routelink.partnerId).get().then(function (fldwrkr) {
								$scope.AssignedSitesone = fldwrkr.sitesassigned;
								if ($scope.AssignedSitesone == null) {
									$scope.AssignedSitesone = $scope.routelink.distributionRouteId;
								} else {
									$scope.AssignedSitesone = $scope.AssignedSitesone + ',' + $scope.routelink.distributionRouteId;
								}
								$scope.AssignedFieldSiteOne = {
									sitesassigned: $scope.AssignedSitesone,
									id: $scope.routelink.partnerId
								};
								//$scope.submitfieldworkers.customPUT($scope.AssignedFieldSiteOne).then(function (responsefield) {
								Restangular.one('fieldworkers', $scope.routelink.partnerId).customPUT($scope.AssignedFieldSiteOne).then(function (responsefield) {
									//$scope.routelinks.customPUT($scope.routelink).then(function (response) {
									Restangular.one('routelinks', $routeParams.id).customPUT($scope.routelink).then(function (response) {
										console.log('response', response);
										$scope.auditlog.entityid = response.id;
										//$scope.auditlog.description = 'FW Site Link Update';
										var respdate = $filter('date')(response.lastmodifiedtime, 'dd-MMMM-yyyy');
										$scope.auditlog.description = 'Field Worker Site Link Updated With Following Details: ' + 'Field Worker Id - ' + response.partnerId + ' , ' + 'Site - ' + response.distributionRouteId + 'Last Modied - ' + respdate;
										//$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
										Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
											console.log('responseaudit', responseaudit);
											window.location = '/assignfwtosite';
										});
									});
								});

							});
						});

					}, function (error) {
						Restangular.one('fieldworkers', $scope.routelink.partnerId).get().then(function (fldwrkr) {
							$scope.AssignedSitesone = fldwrkr.sitesassigned;
							if ($scope.AssignedSitesone == null) {
								$scope.AssignedSitesone = $scope.routelink.distributionRouteId;
							} else {
								$scope.AssignedSitesone = $scope.AssignedSitesone + ',' + $scope.routelink.distributionRouteId;
							}
							$scope.AssignedFieldSiteOne = {
								sitesassigned: $scope.AssignedSitesone,
								id: $scope.routelink.partnerId
							};
							//$scope.submitfieldworkers.customPUT($scope.AssignedFieldSiteOne).then(function (responsefield) {
							Restangular.one('fieldworkers', $scope.routelink.partnerId).customPUT($scope.AssignedFieldSiteOne).then(function (responsefield) {
								//$scope.routelinks.customPUT($scope.routelink).then(function (response) {
								Restangular.one('routelinks', $routeParams.id).customPUT($scope.routelink).then(function (response) {
									console.log('response', response);
									$scope.auditlog.entityid = response.id;
									//$scope.auditlog.description = 'FW Site Link Update';
									var respdate = $filter('date')(response.lastmodifiedtime, 'dd-MMMM-yyyy');
									$scope.auditlog.description = 'FW Site Link Updated With Following Details: ' + 'FieldWorker Id - ' + response.partnerId + ', ' + 'SiteId - ' + response.distributionRouteId + ', ' + 'Modied Date- ' + respdate;

									Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
										console.log('responseaudit', responseaudit);
										window.location = '/assignfwtosite';
									});
								});
							});

						});
					});
				} else if ($scope.routelink.distributionRouteId != $scope.original.distributionRouteId) {
					Restangular.one('fieldworkers', $scope.routelink.partnerId).get().then(function (fldwrkr) {
						$scope.Assignedarray = fldwrkr.sitesassigned.split(',');
						$scope.Assignedarray.splice($scope.Assignedarray.indexOf('' + $scope.original.distributionRouteId), 1);
						for (var i = 0; i < $scope.Assignedarray.length; i++) {
							if (i == 0) {
								$scope.AssignedSites = $scope.Assignedarray[i];
							} else {
								$scope.AssignedSites = $scope.AssignedSites + ',' + $scope.Assignedarray[i];
							}
						}
						if ($scope.Assignedarray.length == 0) {
							$scope.AssignedSites = $scope.routelink.distributionRouteId;
						}
						$scope.AssignedFieldSite = {
							sitesassigned: $scope.AssignedSites,
							id: $scope.routelink.partnerId
						};
						//$scope.submitfieldworkers.customPUT($scope.AssignedFieldSite).then(function (responsefield) {
						Restangular.one('fieldworkers', $scope.original.partnerId).customPUT($scope.AssignedFieldSite).then(function (responsefield) {
							//$scope.routelinks.customPUT($scope.routelink).then(function (response) {
							Restangular.one('routelinks', $routeParams.id).customPUT($scope.routelink).then(function (response) {
								console.log('response', response);
								$scope.auditlog.entityid = response.id;
								//$scope.auditlog.description = 'FW Site Link Update';
								//$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {

								var respdate = $filter('date')(response.lastmodifiedtime, 'dd-MMMM-yyyy');
								$scope.auditlog.description = 'FW Site Link Updated With Following Details: ' + 'FieldWorker Id - ' + response.partnerId + ', ' + 'SiteId - ' + response.distributionRouteId + ', ' + 'Modied Date - ' + respdate;

								Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
									console.log('responseaudit', responseaudit);
									window.location = '/assignfwtosite';
								});
							}, function (error) {
								$scope.agenttoutelinkModal = !$scope.agenttoutelinkModal;
								$scope.message = 'Site Already Assigned';
								console.log(error);
								$scope.updateCount = 0;
							});
						}, function (error) {
							$scope.agenttoutelinkModal = !$scope.agenttoutelinkModal;
							$scope.message = 'Site Already Assigned';
							$scope.updateCount = 0;
						});
					}, function (error) {
						Restangular.one('fieldworkers', $scope.routelink.partnerId).get().then(function (fldwrkr) {
							$scope.AssignedSitesone = fldwrkr.sitesassigned;
							if ($scope.AssignedSitesone == null) {
								$scope.AssignedSitesone = $scope.routelink.distributionRouteId;
							} else {
								$scope.AssignedSitesone = $scope.AssignedSitesone + ',' + $scope.routelink.distributionRouteId;
							}
							$scope.AssignedFieldSiteOne = {
								sitesassigned: $scope.AssignedSitesone,
								id: $scope.routelink.partnerId
							};
							//$scope.submitfieldworkers.customPUT($scope.AssignedFieldSiteOne).then(function (responsefield) {
							Restangular.one('fieldworkers', $scope.routelink.partnerId).customPUT($scope.AssignedFieldSiteOne).then(function (responsefield) {
								//$scope.routelinks.customPUT($scope.routelink).then(function (response) {
								Restangular.one('routelinks', $routeParams.id).customPUT($scope.routelink).then(function (response) {
									console.log('response', response);
									$scope.auditlog.entityid = response.id;
									//$scope.auditlog.description = 'FW Site Link Update';
									//$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
									var respdate = $filter('date')(response.lastmodifiedtime, 'dd-MMMM-yyyy');
									$scope.auditlog.description = 'FW Site Link Updated With Following Details: ' + 'FieldWorker Id - ' + response.partnerId + ', ' + 'SiteId - ' + response.distributionRouteId + ', ' + 'Modied Date - ' + respdate;

									Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
										console.log('responseaudit', responseaudit);
										window.location = '/assignfwtosite';
									});
								});
							});

						});
					});

				} else {
					//$scope.routelinks.customPUT($scope.routelink).then(function (response) {
					Restangular.one('routelinks', $routeParams.id).customPUT($scope.routelink).then(function (response) {
						console.log('response', response);
						$scope.auditlog.entityid = response.id;
						$scope.auditlog.description = 'FW Site Link Update';
						//$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
						Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
							console.log('responseaudit', responseaudit);
							window.location = '/assignfwtosite';
						});
					});
				}
			}
		};


		if ($routeParams.id) {
			Restangular.one('routelinks', $routeParams.id).get().then(function (routelink) {
				$scope.original = routelink;
				$scope.routelink = Restangular.copy($scope.original);
			});
		}


		/*****************************************************************DELETE*************************/
    $scope.Delete = function (id, fwid, routeid) {
            //console.log('Delete Id', id);
            //console.log('Delete fwid', fwid);
            //console.log('Delete routeid', routeid);
            $scope.auditlog.entityid = id;
            //$scope.auditlog.description = 'FW Site Link Delete';
            var respdate = $filter('date')(new Date(), 'dd-MMMM-yyyy');
            $scope.auditlog.description = 'FW Site Link Deleted With Following Details: ' + 'Deleted By UserId - ' + $window.sessionStorage.userId + ', ' + 'RoleId - ' + $window.sessionStorage.roleId + ', ' + 'FieldworkerId - ' + fwid + ', ' + 'SiteId - ' + routeid + ', ' + 'Date - ' + respdate;
            $scope.AssignedSites = null;
            console.log('feid', fwid);
            Restangular.one('fieldworkers', fwid).get().then(function (fldwrkr) {
                $scope.Assignedarray = fldwrkr.sitesassigned.split(',');
                $scope.Assignedarray.splice($scope.Assignedarray.indexOf('' + routeid), 1);
                for (var i = 0; i < $scope.Assignedarray.length; i++) {
                    if (i == 0) {
                        $scope.AssignedSites = $scope.Assignedarray[i];
                        console.log('$scope.AssignedSites0', $scope.AssignedSites);
                    } else {
                        $scope.AssignedSites = $scope.AssignedSites + ',' + $scope.Assignedarray[i];
                        //console.log('$scope.AssignedSites', $scope.AssignedSites);
                    }
                }
                $scope.AssignedFieldSite = [{
                    sitesassigned: $scope.AssignedSites,
                    id: fwid
    }];
                //$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                    //$scope.submitfieldworkers.customPUT($scope.AssignedFieldSite[0]).then(function (responsefield) {
                    Restangular.one('fieldworkers', fwid).customPUT($scope.AssignedFieldSite[0]).then(function (responsefield) {
                        Restangular.one('routelinks/' + id).remove($scope.routelink).then(function (routeResponse) {
                            $route.reload();
                        });
                    }, function (error) {
                        console.log('error', error);
                    });
                });
            });
        }
        /********************** Language *****************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.fwsitelink = langResponse.fwsitelink;
        $scope.addnew = langResponse.addnew;
        $scope.fieldworkerth = langResponse.fieldworkerth;
        $scope.site = langResponse.site;
        $scope.show = langResponse.show;
        $scope.entry = langResponse.entry;
        $scope.searchfor = langResponse.searchfor;
        $scope.action = langResponse.action;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
        $scope.passignedsites = langResponse.assignedsites;
        $scope.pillarstarted = langResponse.pillarstarted;
		$scope.selfielworker = langResponse.selfielworker;
		$scope.selavailablesite = langResponse.selavailablesite;
		$scope.message = langResponse.sitehascreated//'Site has been created!';
    	$scope.modalTitle = langResponse.thankyou;//'Thank You';
    });
});
