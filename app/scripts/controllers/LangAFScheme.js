'use strict';

angular.module('secondarySalesApp')
    .controller('LangAFSchemeCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
     
  
      /*********/

        $scope.isCreateSave = true;
        $scope.langdisable = false;
    

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        $scope.$watch('afscheme.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                
                 Restangular.one('schemeLanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.afscheme = Restangular.copy($scope.original);
                    });
                
                Restangular.all('schemeLanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                        $scope.isCreateSave = true;
                    } else {
                       // $scope.LangId = response[0].id;
                       // $scope.isCreateSave = false;
                       // $scope.langdisable = true;

                      // $scope.afscheme = response[0];
                       // console.log('$scope.afscheme', $scope.afscheme);
                        $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';

                    }
                });

            }
        });
    
                     /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/Langapplyforschmlist';

        };
    
    
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/

        $scope.afscheme = {
            deleteFlag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.UserEmployeeId
        };



       
        $scope.Save = function () {
            Restangular.all('schemeLanguages').post($scope.afscheme).then(function (afschemeResponse) {
                console.log('afschemeResponse', afschemeResponse);
                window.location = '/Langapplyforschmlist';
            });
           
        };

        $scope.Update = function () {
            Restangular.one('schemeLanguages', $routeParams.id).customPUT($scope.afscheme).then(function (afschemeResponse) {
                console.log('afschemeResponse', afschemeResponse);
                window.location = '/Langapplyforschmlist';
            });
        };
    
     if ($routeParams.id) {
          
             $scope.isCreateSave = false;
             $scope.langdisable = true;
            Restangular.one('schemeLanguages', $routeParams.id).get().then(function (afscheme) {
                $scope.original = afscheme;
                $scope.afscheme = Restangular.copy($scope.original);
            });
      }
    
});