'use strict';

angular.module('secondarySalesApp')
    .controller('UsersEditCtrl', function ($scope, Restangular, $routeParams, $window, AnalyticsRestangular) {
        //        if ($window.sessionStorage.roleId != 1) {
        //            window.location = "/";
        //        }
        $scope.heading = 'Edit User';
        $scope.Saved = true;
        $scope.Updated = false;
        $scope.roleDisable = false;
        $scope.userDisable = true;
        $scope.usernameDisable = true;
        $scope.hideTypeofDoctor = true;

        $scope.countryDisable = false;
        $scope.stateDisable = false;
        $scope.districtDisable = false;
        $scope.siteDisable = false;
        $scope.statusDisable = false;
        $scope.languageDisable = false;

        if ($window.sessionStorage.roleId == 1) {
            $scope.countryDisable = false;
            $scope.stateDisable = false;
            $scope.districtDisable = false;
            $scope.siteDisable = false;
            $scope.statusDisable = false;
            $scope.languageDisable = false;
            $scope.roleDisable = false;
        } else {
            $scope.countryDisable = true;
            $scope.stateDisable = true;
            $scope.districtDisable = true;
            $scope.siteDisable = true;
            $scope.statusDisable = true;
            $scope.languageDisable = true;
            $scope.roleDisable = true;
        }


        if ($window.sessionStorage.roleId != 1 && $routeParams.id != $window.sessionStorage.userId) {
            console.log("non user");
            window.location = '/';
        }

        $scope.roleId = $window.sessionStorage.roleId;

        Restangular.all('roles?filter[order]=order ASC').getList().then(function (rle) {
            $scope.roles = rle;
        });

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (lRas) {
            $scope.languages = lRas;
        });

        $scope.users = Restangular.all('users').getList().$object;
        $scope.employees = Restangular.all('employees').getList().$object;
        $scope.zones = Restangular.all('zones').getList().$object;
        //$scope.roles = Restangular.all('roles').getList().$object;
        $scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
        $scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;

        $scope.typeOfDoctors = Restangular.all('typeOfDoctors').getList().$object;

        $scope.zones = Restangular.all('zones?filter[where][deleteFlag]=false').getList().$object;

        $scope.countries = Restangular.all('countries?filter[where][deleteFlag]=false').getList().$object;

        //  $scope.states = Restangular.all('states?filter[where][deleteFlag]=false').getList().$object;

        $scope.user = {};

        $scope.user.stateId = '';
        $scope.user.districtId = '';
        $scope.user.siteId = '';
        $scope.zonalid = '';
        $scope.counId = '';

        $scope.$watch('user.countryId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                //  console.log('newValue', newValue);
                Restangular.all('states?filter={"where":{"and":[{"countryId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (dist) {
                    $scope.districts = dist;
                    // console.log('dist', dist);
                });
                $scope.counId = +newValue;
            }
        });

        $scope.$watch('user.stateId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                // console.log('newValue', newValue);
                Restangular.all('districts?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (dist) {
                    $scope.districts = dist;
                    // console.log('dist', dist);
                });
                $scope.zonalid = +newValue;
            }
        });

        $scope.$watch('user.districtId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                // console.log('newValue', newValue);      
                Restangular.all('sites?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (site) {
                    $scope.sites = site;
                    //$scope.user.siteId = $scope.user.siteId.split(",");
                    //console.log('site', site);
                });
            }
        });

        $scope.$watch('user.roleId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if (newValue == 4) {
                $scope.hideTypeofDoctor = false;
            } else if (newValue != 4) {
                $scope.hideTypeofDoctor = true;
                $scope.user.typeOfDoctor = '';
            }
        });

        $('#mobile').keypress(function (evt) {
            if (/^-?[0-9]\d*(\.\d+)?$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        if ($routeParams.id) {
            Restangular.one('users', $routeParams.id).get().then(function (user) {
                $scope.original = user;

                Restangular.all('states?filter={"where":{"and":[{"countryId":{"inq":[' + $scope.original.countryId + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (state) {
                    //$scope.districts = dist;

                    //Restangular.all('states?filter[where][deleteFlag]=false').getList().then(function (state) {
                    $scope.states = state;

                    Restangular.all('districts?filter={"where":{"and":[{"stateId":{"inq":[' + $scope.original.stateId + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (dist) {
                        $scope.districts = dist;
                        $scope.user = Restangular.copy($scope.original);
                        $scope.user.countryId = $scope.user.countryId.split(",");
                        $scope.user.stateId = $scope.user.stateId.split(",");
                        $scope.user.districtId = $scope.user.districtId.split(",");
                        $scope.user.siteId = $scope.user.siteId.split(",");
                    });
                    // });
                });

                $scope.salesArea = Restangular.all('sales-areas?filter[where][zoneId]=' + $scope.original.zoneId).getList().then(function (salearea) {
                    $scope.salesAreas = salearea;
                    $scope.copartner = Restangular.all('employees?filter[where][groupId]=5' + '&filter[where][deleteflag]=false').getList().then(function (copartner) {
                        $scope.copartners = copartner;


                        if (user.zoneId == undefined || user.zoneId == null || user.zoneId == '') {
                            return;
                        } else {
                            $scope.zones = Restangular.one('zones?filter[where][deleteflag]=false' + '&filter[where][id]=' + user.zoneId).get().then(function (responseZone) {
                                $scope.statename = responseZone[0].name;
                                $scope.user.zoneId = responseZone[0].id;
                                //		console.log('responseZone.name', $scope.user.zoneId);
                            });
                        }

                        $scope.salesAreas = Restangular.one('states?filter[where][deleteflag]=false' + '&filter[where][id]=' + user.salesAreaId).get().then(function (responsesalesAreas) {
                            if (responsesalesAreas.length > 0) {
                                $scope.districtname = responsesalesAreas[0].name;
                                $scope.user.salesAreaId = responsesalesAreas[0].id;
                                //			console.log('responseZone.salesAreaId', $scope.user.salesAreaId);
                            }
                        });
                    });
                });

                if (user.roleId == 6) {
                    Restangular.all('fieldworkers').getList().then(function (fldwkr) {
                        $scope.fieldworkers = fldwkr;
                        Restangular.one('fieldworkers', user.employeeid).get().then(function (fldwrkr) {
                            $scope.user.usrName = JSON.stringify(fldwrkr);
                        });
                    });

                }
                if (user.roleId == 5) {
                    Restangular.all('comembers').getList().then(function (comembr) {
                        $scope.comembers = comembr;
                        Restangular.one('comembers', user.employeeid).get().then(function (comem) {
                            $scope.user.usrName = JSON.stringify(comem);
                        });
                    });

                }
                if (user.roleId == 3 || user.roleId == 4 || user.roleId == 16 || user.roleId == 17) {
                    //$scope.mentors = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=17').getList().$object;

                    //$scope.spms = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=3').getList().$object;

                    //$scope.rios = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=4').getList().$object;

                    // $scope.nos = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=16').getList().$object;
                    Restangular.all('mentors').getList().then(function (otherusrs) {
                        $scope.mentors = otherusrs;
                        $scope.spms = otherusrs;
                        $scope.rios = otherusrs;
                        $scope.nos = otherusrs;
                        Restangular.one('mentors', user.employeeid).get().then(function (mentr) {
                            //console.log('mentr', mentr);
                            $scope.user.usrName = JSON.stringify(mentr);
                        });
                    });
                }
                if (user.roleId == 1) {
                    Restangular.all('employees?filter[where][groupId]=1').getList().then(function (amn) {
                        $scope.admins = amn;
                        Restangular.one('employees', user.employeeid).get().then(function (admn) {
                            $scope.user.usrName = JSON.stringify(admn);
                        });
                    });

                }

            });
        }

        $scope.hideState = false;
        $scope.hideDistrict = false;
        $scope.hideCo = false;

        /********************************************** UPDATE ****************************/

        Restangular.all('users?filter[where][id]=' + $routeParams.id).getList().then(function (submitcomem) {
            $scope.getUserId = submitcomem[0].employeeid;
        });

        $scope.comember = {};
        $scope.fwworker = {};

        $scope.validatestring = '';

        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('userName').style.borderColor = "";
            document.getElementById('password').style.border = "";
            document.getElementById('mobile').style.border = "";
            document.getElementById('email').style.border = "";

            if ($scope.user.name == '' || $scope.user.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.user.roleId == '' || $scope.user.roleId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Role';

            } else if (($scope.user.roleId != 1) && ($scope.user.countryId == '' || $scope.user.countryId == null)) {
                $scope.validatestring = $scope.validatestring + 'Please Select Country';

            } else if (($scope.user.roleId != 1 && $scope.user.roleId != 2) && ($scope.user.stateId == '' || $scope.user.stateId == null)) {
                $scope.validatestring = $scope.validatestring + 'Please Select State';

            } else if (($scope.user.roleId != 1 && $scope.user.roleId != 2 && $scope.user.roleId != 6) && ($scope.user.districtId == '' || $scope.user.districtId == null)) {
                $scope.validatestring = $scope.validatestring + 'Please Select District';

            } else if (($scope.user.roleId != 1 && $scope.user.roleId != 2 && $scope.user.roleId != 6 && $scope.user.roleId != 5) && ($scope.user.siteId == '' || $scope.user.siteId == null)) {
                $scope.validatestring = $scope.validatestring + 'Please Select Site';

            } else if ($scope.user.username == '' || $scope.user.username == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter User Name';
                document.getElementById('userName').style.borderColor = "#FF0000";

            }
            else if ($scope.user.languageId == '' || $scope.user.languageId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Your Language';

            } else if ($scope.user.mobile == '' || $scope.user.mobile == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Your Mobile Number';
                document.getElementById('mobile').style.borderColor = "#FF0000";

            } else if ($scope.user.mobile.length != 10) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Valid Mobile Number';
                document.getElementById('mobile').style.borderColor = "#FF0000";

            } else if ($scope.user.email == '' || $scope.user.email == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Email ID';
                document.getElementById('email').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring2 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.user.lastModifiedDate = new Date();
                $scope.user.lastModifiedBy = $window.sessionStorage.userId;
                $scope.user.lastModifiedByRole = $window.sessionStorage.roleId;
               // alert("Ok");
                 $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

                 Restangular.one('users/' + $routeParams.id).customPUT($scope.user).then(function (subResponse) {
                     // console.log('subResponse', subResponse);
                     $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                     if ($window.sessionStorage.roleId == 1) {
                         window.location = '/user-list';
                     } else {
                         window.location = '/';
                     }

                     //                    AnalyticsRestangular.one('users/' + $routeParams.id).customPUT($scope.user).then(function (analyticssubResponse) {
                     //                        if (subResponse.roleId == 5) {
                     //                            $scope.comember.helpline = subResponse.mobile;
                     //                            Restangular.one('comembers/' + $scope.getUserId).customPUT($scope.comember).then(function (submitcomember) {
                     //                                console.log('submitcomember', submitcomember);
                     //                                window.location = '/users';
                     //                            });
                     //                        } else if (subResponse.roleId == 6) {
                     //                            $scope.fwworker.mobile = subResponse.mobile;
                     //                            Restangular.one('fieldworkers/' + $scope.getUserId).customPUT($scope.fwworker).then(function (submitfwer) {
                     //                                console.log('submitfwer', submitfwer);
                     //                                window.location = '/users';
                     //                            });
                     //                        } else {
                     //                            window.location = '/users';
                     //                        }
                     //                    });
                 }, function (error) {
                     //console.log('error', error);
                     $scope.showValidation = !$scope.showValidation;
                     
                     $scope.validatestring1 = 'Email ID Already Exit';
                     //console.error('console.error', response);
                 });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.message = 'User has been Updated!';
        $scope.showValidation = false;
        $scope.showUniqueValidation = false;
        $scope.toggleValidation = function () {
            $scope.showUniqueValidation = !$scope.showUniqueValidation;
        };


    });