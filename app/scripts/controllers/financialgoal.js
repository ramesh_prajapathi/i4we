'use strict';

angular.module('secondarySalesApp')
    .controller('FinancialGoalCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/financialgoal/create' || $location.path() === '/financialgoal/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/financialgoal/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/financialgoal/create' || $location.path() === '/financialgoal/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/financialgoal/create' || $location.path() === '/financialgoal/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        }

        if ($window.sessionStorage.prviousLocation != "partials/financialgoal") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };



        /*********/

        //$scope.submitstakeholdertypes = Restangular.all('financialgoals').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'FinancialGoal type has been Updated!';
            Restangular.one('financialgoals', $routeParams.id).get().then(function (stakeholdertype) {
                $scope.original = stakeholdertype;
                $scope.financialgoal = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'FinancialGoal type has been Created!';
        }
        $scope.searchstakeholdertype = $scope.name;

        /***************************** INDEX *******************************************/
        $scope.zn = Restangular.all('financialgoals?filter[where][deleteflag]=false').getList().then(function (zn) {
            //
            $scope.financialgoals = zn;
            // console.log('$scope.financialgoals', $scope.financialgoals);
            angular.forEach($scope.financialgoals, function (member, index) {
                member.index = index + 1;
            });
        });

        $scope.financialgoal = {
            name: '',
            deleteflag: false
        };
        /************************************ SAVE *******************************************/
        $scope.validatestring = '';
        $scope.Savestakeholdertype = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.financialgoal.name == '' || $scope.financialgoal.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.financialgoal.hnname == '' || $scope.financialgoal.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name in hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.financialgoal.knname == '' || $scope.financialgoal.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name in kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.financialgoal.taname == '' || $scope.financialgoal.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name in tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.financialgoal.tename == '' || $scope.financialgoal.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name in telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.financialgoal.mrname == '' || $scope.financialgoal.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name in marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.financialgoals.post($scope.financialgoal).then(function () {
                    console.log('financial goal Saved');
                    window.location = '/financialgoal';
                });
            }
        };
        /********************************* UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.Updatestakeholdertype = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.financialgoal.name == '' || $scope.financialgoal.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.financialgoal.hnname == '' || $scope.financialgoal.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name in hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.financialgoal.knname == '' || $scope.financialgoal.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name in kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.financialgoal.taname == '' || $scope.financialgoal.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name in tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.financialgoal.tename == '' || $scope.financialgoal.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name in telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.financialgoal.mrname == '' || $scope.financialgoal.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter financial goal name in marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.financialgoals.customPUT($scope.financialgoal).then(function () {
                    console.log('financial goal Saved');
                    window.location = '/financialgoal';
                });
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };
        /********************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('financialgoals/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
