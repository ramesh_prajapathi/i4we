'use strict';

angular.module('secondarySalesApp')
    .controller('NonSystematicRecordCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 9) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/nonsystematicrecord/create' || $location.path() === '/nonsystematicrecord/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/nonsystematicrecord/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/nonsystematicrecord/create' || $location.path() === '/nonsystematicrecord/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/nonsystematicrecord/create' || $location.path() === '/nonsystematicrecord/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/nonsystematicrecord") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.showenglishLang = true;
        $scope.OtherLang = true;

        $scope.$watch('nonsysrecord.language', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });

        $scope.$watch('nonsysrecord.parentId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.one('nonsystematicrecords', newValue).get().then(function (sts) {
                    $scope.nonsysrecord.orderNo = sts.orderNo;
                    $scope.nonsysrecord.lowerBound = sts.lowerBound;
                    $scope.nonsysrecord.upperBound = sts.upperBound;
                    $scope.nonsysrecord.mandatoryFlag = sts.mandatoryFlag;
                    $scope.nonsysrecord.enabled = sts.enabled;
                });
            }
        });

        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Non Systematic Record has been Updated!';
            Restangular.one('nonsystematicrecords', $routeParams.id).get().then(function (nonsysrecord) {
                $scope.original = nonsysrecord;
                $scope.nonsysrecord = Restangular.copy($scope.original);

            });
            Restangular.all('nonsystematicrecords?filter[where][parentId]=' + $routeParams.id).getList().then(function (vitalResp) {
                $scope.Allnonsystematicrecords = vitalResp;
                //console.log('$scope.AllvitalRecord', $scope.AllvitalRecord);
            });

        } else {
            $scope.message = 'Non Systematic Record has been Created!';
        }

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];

        /******************************** INDEX *******************************************/

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (sev) {
            $scope.nsrlanguages = sev;

            Restangular.all('nonsystematicrecords?filter[where][deleteFlag]=false').getList().then(function (vital) {
                $scope.nonsystematicrecords = vital;

                angular.forEach($scope.nonsystematicrecords, function (member, index) {
                    member.index = index + 1;

                    var data = $scope.nsrlanguages.filter(function (arr) {
                        return arr.id == member.language
                    })[0];

                    if (data != undefined) {
                        member.langname = data.name;
                    }
                });
            });
        });

        Restangular.all('nonsystematicrecords?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.englishnonsystematicrecords = zn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        /********************************************* SAVE *******************************************/

        $scope.nonsysrecord = {
            "name": '',
            "deleteFlag": false,
            "mandatoryFlag": false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";

            if ($scope.nonsysrecord.language == '' || $scope.nonsysrecord.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.nonsysrecord.name == '' || $scope.nonsysrecord.name == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.nonsysrecord.orderNo == '' || $scope.nonsysrecord.orderNo == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Order No';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.nonsysrecord.lowerBound == '' || $scope.nonsysrecord.lowerBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Lower Bound';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.nonsysrecord.upperBound == '' || $scope.nonsysrecord.upperBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Upper Bound';
                document.getElementById('name').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.nonsysrecord.parentId === '') {
                    delete $scope.nonsysrecord['parentId'];
                }
                $scope.submitDisable = true;
                $scope.nonsysrecord.parentFlag = $scope.showenglishLang;

                Restangular.all('nonsystematicrecords').post($scope.nonsysrecord).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    window.location = '/nonsystematicrecords-list';
                }, function (error) {
                    if (error.data.error.constraint === 'nonsystematicrecord_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.nonsysrecord.language == '' || $scope.nonsysrecord.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.nonsysrecord.name == '' || $scope.nonsysrecord.name == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.nonsysrecord.orderNo == '' || $scope.nonsysrecord.orderNo == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Order No';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.nonsysrecord.lowerBound == '' || $scope.nonsysrecord.lowerBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Lower Bound';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.nonsysrecord.upperBound == '' || $scope.nonsysrecord.upperBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Upper Bound';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.nonsysrecord.parentId === '') {
                    delete $scope.nonsysrecord['parentId'];
                }
                $scope.submitDisable = true;

                Restangular.one('nonsystematicrecords', $routeParams.id).customPUT($scope.nonsysrecord).then(function (updateResp) {
                    console.log('Test Name Saved');
                    $scope.MandatoryUpdate(updateResp);

                }, function (error) {
                    if (error.data.error.constraint === 'nonsystematicrecord_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        /**************************Updating all palce flag value**************************/

        $scope.upgateFlagCount = 0;
        $scope.updateallRecord = {};

        $scope.MandatoryUpdate = function (myResponse) {
            if ($scope.upgateFlagCount < $scope.Allnonsystematicrecords.length) {

                $scope.updateallRecord.orderNo = myResponse.orderNo;
                $scope.updateallRecord.lowerBound = myResponse.lowerBound;
                $scope.updateallRecord.upperBound = myResponse.upperBound;
                $scope.updateallRecord.mandatoryFlag = myResponse.mandatoryFlag;

                Restangular.one('nonsystematicrecords', $scope.Allnonsystematicrecords[$scope.upgateFlagCount].id).customPUT($scope.updateallRecord).then(function (childResp) {
                    console.log('childResp', childResp);
                    $scope.upgateFlagCount++;
                    $scope.MandatoryUpdate(myResponse);

                });
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('Test Name Saved');
                window.location = '/nonsystematicrecords-list';
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('nonsystematicrecords/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
