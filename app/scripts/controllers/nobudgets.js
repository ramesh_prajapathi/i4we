'use strict';
angular.module('secondarySalesApp').controller('nobudgetsCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $modal) {

    $scope.SPM_Budget_Year = '';
    $scope.SPM_Budget_CO = '';
    $scope.Budget = 0;


    $scope.totalCo = 0;
    $scope.$watch('SPM_Budget_Year', function (newValue, oldValue) {
        //console.log('SPM_Budget_Year', newValue);
        if (newValue === oldValue || newValue == '') {
            return;
        } else {
            $scope.emps = Restangular.all('cobudgets?filter[where][year]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (emps) {
                $scope.cobudgets = emps;
                console.log('$scope.cobudgets ', $scope.cobudgets);
                $scope.emps = Restangular.all('spmbudgets?filter[where][year]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (emps) {
                    $scope.stakeholdertypes = emps;
                    $scope.zn = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (zn) {
                       // $scope.stakeholders = zn;
                         $scope.stakeholders = [];
                        
                        for(var i=0;i<zn.length;i++){
                            if(zn[i].id != 25){
                                $scope.stakeholders.push(zn[i]);
                            }
                        }
                        //$scope.modalInstanceLoad.close();
                        angular.forEach($scope.stakeholders, function (member, index) {
                            member.index = index + 1;
                            member.COTotal = 0;
                            member.Sitename = 0;
                            /// member.stakeholdertypes = $scope.getStakeholdertype(member.stakeholdertype);
                            for (var n = 0; n < $scope.stakeholdertypes.length; n++) {
                                if (member.id == $scope.stakeholdertypes[n].state) {
                                    //member.Sitename2 = $scope.stakeholdertypes[n];
                                    member.Sitename = member.Sitename + $scope.stakeholdertypes[n].totalamount;
                                    //$scope.Sitename_humanresources = $scope.stakeholdertypes[n].totalamount;
                                    //console.log('Sitename', member.Sitename.totalamount)
                                    //break;
                                }
                            }

                            for (var n = 0; n < $scope.stakeholdertypes.length; n++) {
                                if (member.id == $scope.stakeholdertypes[n].state) {
                                    member.Sitename2 = $scope.stakeholdertypes[n];
                                    // member.Sitename = member.Sitename + $scope.stakeholdertypes[n].totalamount;
                                    //$scope.Sitename_humanresources = $scope.stakeholdertypes[n].totalamount;
                                    //console.log('Sitename', member.Sitename.totalamount)
                                    break;
                                }
                            }

                            for (var o = 0; o < $scope.cobudgets.length; o++) {
                                if (member.id == $scope.cobudgets[o].state) {
                                    member.COTotal = member.COTotal + $scope.cobudgets[o].totalamount;
                                    //$scope.Sitename_humanresources = $scope.stakeholdertypes[n].totalamount;
                                    //console.log('Sitename', member.COTotal)
                                    //$scope.totalCo = $scope.totalCo + $scope.cobudgets[o].totalamount;
                                    //console.log(' $scope.totalCo', $scope.totalCo)
                                }
                            }
                            //member.datetime = $filter('date')(member.datetime, 'dd-MMMM-yyyy');
                            $scope.TotalTodos = [];
                            $scope.TotalTodos.push(member);
                        });
                    });
                });
            });
        }
    });

    /*******************************************************************************************/

    /* $scope.$watch('SPM_Budget_Year', function (newValue, oldValue) {
         console.log('SPM_Budget_Year', newValue);
         if (newValue === oldValue || newValue == '') {
             return;
         } else {
             $scope.SPM_Budget_CO = '';
             $scope.CO_total_Budget = 0;


             Restangular.all('spmbudgets??filter[where][year]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                 $scope.spmbudgets = response;
                 Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (states) {
                     $scope.states = states;
                     angular.forEach($scope.spmbudgets, function (member, index) {
                         member.index = index + 1;
                         for (var n = 0; n < $scope.spmbudgets.length; n++) {
                             if (member.id == $scope.spmbudgets[n].state) {
                                 member.FieldWorkerName = $scope.spmbudgets[n];
                                 console.log('member.FieldWorkerName',member.FieldWorkerName)
                                 break;
                             }
                         }

                         //$scope.Fetch_SPM_Budget(member.id);
                     });
                 });
             });

         }
     });*/

    $scope.Fetch_SPM_Budget = function (stid) {
        console.log('spmid', stid);
        return Restangular.all('spmbudgets?filter={"where":{"id":{"state":[' + stid + ']}}}').getList().$object;
        /* if (stid === 25) {
             Restangular.all('spmbudgets?filter[where][state]=25' + '&filter[where][deleteflag]=false').getList().then(function (response) {
                 $scope.spmbudgets = response;
                 //console.log('spm Total', response);
                // for (var j = 0; j < response.length; j++) {
                     $scope.Budget = $scope.Budget + response[0].totalamount;
                 //}
             });
         } else {
             $scope.Budget = 0;
             return;
         }*/

    }

    $scope.getStakeholdertype = function (typ) {
        console.log('typ', typ)
        return Restangular.all('spmbudgets?filter={"where":{"state":{"inq":[' + typ + ']}}}').getList().$object;
    }


    /* $scope.$watch('SPM_Budget_CO', function (newValue, oldValue) {
         // console.log('SPM_Budget_CO', newValue);
         if (newValue === oldValue || newValue == '') {
             return;
         } else {
             Restangular.all('comembers?filter[where][facility]=' + newValue + '&filter[where][deleteflag]=false&filter[where][hdfflag]=false').getList().then(function (comembers) {
                 console.log('comembers.length', comembers.length);
                 if (comembers.length == 0) {
                     $scope.spmbudgets = '';
                     return;
                 } else {
                     //if (comembers.length > 0) {
                     Restangular.all('spmbudgets?filter[where][comember]=' + comembers[0].id + '&filter[where][year]=' + $scope.yearid + '&filter[where][deleteflag]=false').getList().then(function (respon) {
                         $scope.spmbudgets = respon;
                         $scope.facility_Id = respon[0].facility;
                         angular.forEach($scope.spmbudgets, function (member, index) {
                             member.index = index + 1;
                         });
                     });
                     $scope.Your_Selected_Co = comembers[0].id;
                 }



             });

             Restangular.all('cobudgets?filter[where][year]=' + $scope.yearid + '&filter[where][facility]=' + $scope.facility_Id).getList().then(function (cobudget) {
                 console.log('cobudget', cobudget)
                 for (var i = 0; i < cobudget.length; i++) {
                     $scope.CO_total_Budget = $scope.CO_total_Budget + cobudget[i].totalamount;
                 }
                 console.log('$scope.CO_total_Budget', $scope.CO_total_Budget)
             });

         }

     });*/


    Restangular.all('spmbudgetyears?filter[order]=name%20ASC').getList().then(function (bud) {
        $scope.spmbudgetyears = bud;
    });



    //$scope.states = ['AP','KR','MH','TN','TS','Total'];

    //$scope.Actual = 500;


    /***********************************************************************************/
    $scope.BudgetFunction1 = function (year, state) {
        console.log('Year', year);
        console.log('state', state);
        Restangular.all('cobudgets?filter[where][year]=' + year).getList().then(function (cobudget) {
            console.log('cobudget', cobudget);
            $scope.cobudgets = cobudget;
            Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (zn) {
                $scope.zones = zn;
                angular.forEach($scope.zones, function (member, index) {
                    member.index = index + 1;

                    member.Humanresource = 0;
                    for (var n = 0; n < $scope.cobudgets.length; n++) {
                        if (state == $scope.cobudgets[n].state) {
                            member.Humanresource = $scope.cobudgets[n].humanresources;
                            console.log(' member.Humanresource', member.Humanresource);
                        }
                    }
                });
            });
        });
    }



    $scope.BudgetFunction2 = function (year, state) {
        console.log('Year', year);
        console.log('state', state);
        Restangular.all('spmbudgets?filter[where][year]=' + year + '&filter[where][state]=' + state).getList().then(function (spmbudget) {
            console.log('cobudget', spmbudget);
            $scope.spmbudgets = spmbudget;
            angular.forEach($scope.zones, function (member, index) {
                member.index = index + 1;
                member.Humanresource = 0;
                for (var n = 0; n < $scope.spmbudgets.length; n++) {
                    if (member.state == state) {
                        member.Humanresource = member.Humanresource + $scope.spmbudgets[n].humanresources;

                        $scope.SPM_Budget_TravelandCommunication = $scope.SPM_Budget_TravelandCommunication + $scope.spmbudgets[n].travelandcommunication;
                        console.log(' member.Humanresource', member.Humanresource);
                        console.log('$scope.SPM_Budget_TravelandCommunication ', $scope.SPM_Budget_TravelandCommunication);
                    }
                }
            });

        });
    }

    $scope.BudgetViewModal = function () {
        $scope.modalAW = $modal.open({
            animation: true,
            templateUrl: 'template/nobudgetView.html',
            scope: $scope,
            backdrop: 'static',
            size: 'lg'
        });
    };

    $scope.SPM_Budget_TravelandCommunication = 0
    $scope.SPM_Budget_HumanResources = 0;
    $scope.SPM_Budget_GenericActivity = 0;
    $scope.SPM_Budget_SpecificActivity = 0;
    $scope.SPM_Budget_MSGTGActivity = 0;
    $scope.SPM_Budget_AdminitrativeExpence = 0;
    $scope.SPM_Budget_MoniteringEvaluation = 0;

    $scope.CO_Budget_TravelandCommunication = 0
    $scope.CO_Budget_HumanResources = 0;
    $scope.CO_Budget_GenericActivity = 0;
    $scope.CO_Budget_SpecificActivity = 0;
    $scope.CO_Budget_MSGTGActivity = 0;
    $scope.CO_Budget_AdminitrativeExpence = 0;
    $scope.CO_Budget_MoniteringEvaluation = 0;
    $scope.Budget_Total = 0;
    $scope.Actual_Total = 0;

    $scope.BudgetFunction = function (year, state) {
        console.log('Year', year);
        console.log('state', state);
        Restangular.all('spmbudgets?filter[where][year]=' + year + '&filter[where][state]=' + state).getList().then(function (spmbudget) {
            console.log('spmbudget', spmbudget);
            $scope.spmbudgets = spmbudget;
            for (var n = 0; n < $scope.spmbudgets.length; n++) {
                $scope.SPM_Budget_HumanResources = $scope.SPM_Budget_HumanResources + $scope.spmbudgets[n].humanresources;
                $scope.SPM_Budget_TravelandCommunication = $scope.SPM_Budget_TravelandCommunication + $scope.spmbudgets[n].travelandcommunication;
                $scope.SPM_Budget_GenericActivity = $scope.SPM_Budget_GenericActivity + $scope.spmbudgets[n].genericactivity;
                $scope.SPM_Budget_SpecificActivity = $scope.SPM_Budget_SpecificActivity + $scope.spmbudgets[n].specificactivity;
                $scope.SPM_Budget_MSGTGActivity = $scope.SPM_Budget_MSGTGActivity + $scope.spmbudgets[n].msgtgactivity;
                $scope.SPM_Budget_AdminitrativeExpence = $scope.SPM_Budget_AdminitrativeExpence + $scope.spmbudgets[n].administrativeexpense;
                $scope.SPM_Budget_MoniteringEvaluation = $scope.SPM_Budget_MoniteringEvaluation + $scope.spmbudgets[n].monitoringevaluation;
                //console.log('$scope.SPM_Budget_TravelandCommunication ', $scope.SPM_Budget_TravelandCommunication);

                $scope.Budget_Total = $scope.Budget_Total + $scope.spmbudgets[n].totalamount;
            }
        });

        Restangular.all('cobudgets?filter[where][year]=' + year + '&filter[where][state]=' + state).getList().then(function (cobudget) {
            //console.log('cobudget', spmbudget);
            $scope.cobudgets = cobudget;
            for (var n = 0; n < $scope.cobudgets.length; n++) {
                $scope.CO_Budget_HumanResources = $scope.CO_Budget_HumanResources + $scope.cobudgets[n].humanresources;
                $scope.CO_Budget_TravelandCommunication = $scope.CO_Budget_TravelandCommunication + $scope.cobudgets[n].travelandcommunication;
                $scope.CO_Budget_GenericActivity = $scope.CO_Budget_GenericActivity + $scope.cobudgets[n].genericactivity;
                $scope.CO_Budget_SpecificActivity = $scope.CO_Budget_SpecificActivity + $scope.cobudgets[n].specificactivity;
                $scope.CO_Budget_MSGTGActivity = $scope.CO_Budget_MSGTGActivity + $scope.cobudgets[n].msgtgactivity;
                $scope.CO_Budget_AdminitrativeExpence = $scope.CO_Budget_AdminitrativeExpence + $scope.cobudgets[n].administrativeexpense;
                $scope.CO_Budget_MoniteringEvaluation = $scope.CO_Budget_MoniteringEvaluation + $scope.cobudgets[n].monitoringevaluation;
                ///console.log('$scope.SPM_Budget_TravelandCommunication ', $scope.SPM_Budget_TravelandCommunication);
                $scope.Actual_Total = $scope.Actual_Total + $scope.cobudgets[n].totalamount;
            }
        });
        $scope.BudgetViewModal();
    }

    $scope.Cancel = function () {
        $scope.SPM_Budget_TravelandCommunication = 0
        $scope.SPM_Budget_HumanResources = 0;
        $scope.SPM_Budget_GenericActivity = 0;
        $scope.SPM_Budget_SpecificActivity = 0;
        $scope.SPM_Budget_MSGTGActivity = 0;
        $scope.SPM_Budget_AdminitrativeExpence = 0;
        $scope.SPM_Budget_MoniteringEvaluation = 0;


        $scope.CO_Budget_TravelandCommunication = 0
        $scope.CO_Budget_HumanResources = 0;
        $scope.CO_Budget_GenericActivity = 0;
        $scope.CO_Budget_SpecificActivity = 0;
        $scope.CO_Budget_MSGTGActivity = 0;
        $scope.CO_Budget_AdminitrativeExpence = 0;
        $scope.CO_Budget_MoniteringEvaluation = 0;
        $scope.Budget_Total = 0;
        $scope.Actual_Total = 0;
        $scope.modalAW.close();
    };
    /************************************************888******************************/



});
