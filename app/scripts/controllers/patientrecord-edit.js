'use strict';

angular.module('secondarySalesApp')
    .controller('PatientRecordEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter, $timeout, $modal) {


        Restangular.one('prlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.PRLanguage = langResponse[0];
            $scope.PRHeading = $scope.PRLanguage.prEdit;
            // $scope.modalTitle = $scope.RILanguage.thankYou;
            $scope.message = $scope.PRLanguage.thankYouUpdated;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }


        $scope.patient = {};

        $scope.disabledAge = true;
        $scope.disableGender = true;
        $scope.disabledcontact = true;
        $scope.disableMigrant = true;
        $scope.HideSubmitButton = false;
        $scope.HideUpdateButton = false;
        $scope.disabledpatient = true;
        $scope.disableSHGmember = true;
        $scope.disableUpdate = false;


        $scope.TestArray = [];

        $scope.medicineList = [];

        /********************************* Watch ****************************/
        //  $scope.$watch('patient.memberofShgFlag', function (newValue, oldValue) {
        // console.log('newValue', newValue);


        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.disableAssigned = true;
            $scope.hideAddBtn = true;
            $scope.HideUpdateButton = true;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.patient.associatedHF = $scope.patient.associatedHF;

            });
        } else {

            $scope.disableAssigned = false;
            $scope.hideAddBtn = false;
            $scope.HideUpdateButton = false;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.patient.associatedHF = $scope.patient.associatedHF;

            });

        }
        /*************************Cancle Button***********/

        $scope.CancelButton = function () {

            $location.path('/patientrecord-list');

        };

        /****************/

        Restangular.all('genders?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (gender) {
            $scope.genders = gender;
            $scope.patient.gender = $scope.patient.gender;

        });

        Restangular.all('migrants?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (migrant) {
            $scope.migrants = migrant;
            $scope.patient.migrant = $scope.patient.migrant;

        });

        Restangular.all('symptoms?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (symptom) {
            $scope.symptoms = symptom;
            $scope.patient.symptom = $scope.patient.symptom;

        });

        Restangular.all('illnesses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (ill) {
            $scope.illnesses = ill;
            $scope.patient.illness = $scope.patient.illness;
        });

        Restangular.all('medicines?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (med) {
            $scope.medicines = med;
            $scope.patient.medicine = $scope.patient.medicine;

        });

        Restangular.all('seenbys?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (seen) {
            $scope.seenbys = seen;
            $scope.patient.seenby = $scope.patient.seenby;
        });

        $scope.refertos = Restangular.all('refertos?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        Restangular.all('testnames?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (Resp) {
            $scope.testnames = Resp;

        });
        Restangular.all('members?filter[where][deleteFlag]=false').getList().then(function (memberResp) {
            // console.log('memberResp', memberResp);
            $scope.members = memberResp;
            $scope.patient.memberId = $scope.patient.memberId;
        });

        Restangular.all('vitalrecords?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][enabled]=false').getList().then(function (Response) {
            $scope.vitalrecords = Response;
            angular.forEach($scope.vitalrecords, function (member, index) {
                member.existingFlag = false;
                if (member.mandatoryFlag == true) {
                    member.color = 'red';
                } else {
                    member.color = 'black';
                }
            });
        });

        Restangular.all('vitalrecords?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][enabled]=true').getList().then(function (Response) {
            $scope.nonsystematicrecords = Response;
            angular.forEach($scope.nonsystematicrecords, function (member, index) {
                member.existingFlag = false;
                if (member.mandatoryFlag == true) {
                    member.color = 'red';
                } else {
                    member.color = 'black';
                }
            });
        });

        if ($routeParams.id) {
            Restangular.one('patientrecords', $routeParams.id).get().then(function (patient) {
                $scope.original = patient;
                $scope.patient = Restangular.copy($scope.original);

                Restangular.all('vaitalrecordtrailers?filter[where][patientHeaderId]=' + $routeParams.id).getList().then(function (vitalValue) {
                    //console.log('vitalValue', vitalValue);
                    $scope.vaitalrecordtrailer = vitalValue;
                    // });
                    angular.forEach($scope.vitalrecords, function (member, index) {
                        // console.log('member', member);
                        //if(member.vitalrecordId == vitalValue. )
                        for (var i = 0; i < $scope.vaitalrecordtrailer.length; i++) {
                            if ($window.sessionStorage.language == 1) {

                                if (member.id == $scope.vaitalrecordtrailer[i].vitalrecordId) {
                                    member.TextValue = $scope.vaitalrecordtrailer[i].vitalValue;
                                    member.existingFlag = true;
                                    member.newId = $scope.vaitalrecordtrailer[i].id;
                                    delete member['id'];
                                    //console.log('member', member);
                                    break;
                                }
                            } else {
                                if (member.parentId == $scope.vaitalrecordtrailer[i].vitalrecordId) {
                                    member.TextValue = $scope.vaitalrecordtrailer[i].vitalValue;
                                    member.existingFlag = true;
                                    member.newId = $scope.vaitalrecordtrailer[i].id;
                                    delete member['id'];
                                    //console.log('member', member);
                                    break;
                                }

                            }
                        }


                    });
                });

                Restangular.all('nonsystematicrecordtrailers?filter[where][patientHeaderId]=' + $routeParams.id).getList().then(function (nonsys) {
                    $scope.nonsystematicrecordtrailer = nonsys;

                    angular.forEach($scope.nonsystematicrecords, function (member, index) {

                        for (var i = 0; i < $scope.nonsystematicrecordtrailer.length; i++) {

                            if ($window.sessionStorage.language == 1) {
                                if (member.id == $scope.nonsystematicrecordtrailer[i].nonSystematicrecordId) {
                                    // console.log(member);
                                    member.nonSystematicValue = $scope.nonsystematicrecordtrailer[i].nonSystematicValue;
                                    member.existingFlag = true;
                                    member.newId = $scope.nonsystematicrecordtrailer[i].id;
                                    delete member['id'];
                                    break;
                                }
                            } else {
                                if (member.parentId == $scope.nonsystematicrecordtrailer[i].nonSystematicrecordId) {
                                    member.nonSystematicValue = $scope.nonsystematicrecordtrailer[i].nonSystematicValue;
                                    member.existingFlag = true;
                                    member.newId = $scope.nonsystematicrecordtrailer[i].id;
                                    delete member['id'];
                                    break;
                                }
                            }
                        }
                    });
                });

                if ($scope.patient.memberofShgFlag == true) {
                    $scope.patient.memberofShgFlag = 'true';
                } else {
                    $scope.patient.memberofShgFlag = 'false';
                }

                $scope.patient.symptom = $scope.patient.symptom.split(',');

                Restangular.all('testresults?filter[where][patientId]=' + patient.id + '&filter[where][deleteFlag]=false').getList().then(function (testResponse) {

                    if (testResponse.length > 0) {

                        angular.forEach(testResponse, function (member, index) {
                            member.testnames = $scope.testnames;
                            member.dateoftest = member.dateoftest;
                            member.existingFlag = true;
                            // member.testnameId = member.testnameId.split(',');
                            $scope.TestArray.push(member);
                        });

                    } else {
                        $scope.TestArray = [{
                            result: '',
                            existingFlag: false,
                            deleteFlag: false,
                            testnames: $scope.testnames,
                            dateoftest: new Date()
                        }];
                    }
                });

                Restangular.all('medicinegivens?filter[where][patientrecordid]=' + patient.id + '&filter[where][deleteFlag]=false').getList().then(function (medResponse) {

                    if (medResponse.length > 0) {

                        angular.forEach(medResponse, function (member, index) {
                            member.medicineid = member.medicineid;
                            member.existingFlag = true;
                            $scope.medicineList.push(member);
                        });

                    } else {
                        $scope.medicineList = [{
                            medicineid: '',
                            noofmedicine: 0,
                            deleteFlag: false,
                            existingFlag: false
                        }];
                    }
                });

            });
        }

        $scope.$watch('checkMemberOfSHGpatient.memberofShgFlag', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == true) {

                $scope.disabledAge = true;
                $scope.disableGender = true;
                $scope.disabledcontact = true;
                $scope.disableMigrant = true;
            } else {

                $scope.disabledAge = false;
                $scope.disableGender = false;
                $scope.disabledcontact = false;
                $scope.disableMigrant = false;

            }
        });
        /**************************Members ************************************/
        /***   $scope.parentIdarray = [];
           if ($window.sessionStorage.roleId + "" === "3") {

               $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}},{"shgId":{"nin":[' + 0 + ']}}]}}';

               Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
                   for (var i = 0; i < mems.length; i++) {

                       if ($scope.parentIdarray.indexOf(mems[i].parentId) == -1) {
                           $scope.parentIdarray.push(mems[i].parentId);
                       }

                   }



                   Restangular.all('members?filter={"where": {"parentId": {"inq": [' + $scope.parentIdarray + ']}}}').getList().then(function (memberResp) {
                       // console.log('memberResp', memberResp);
                       $scope.members = memberResp;
                       $scope.patient.memberId = $scope.patient.memberId;
                   });
               });
           } else {
               $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"shgId":{"nin":[' + 0 + ']}}]}}';


               Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
                   for (var i = 0; i < mems.length; i++) {

                       if ($scope.parentIdarray.indexOf(mems[i].parentId) == -1) {
                           $scope.parentIdarray.push(mems[i].parentId);
                       }

                   }

                   Restangular.all('members?filter={"where": {"parentId": {"inq": [' + $scope.parentIdarray + ']}}}').getList().then(function (memberResp) {
                       // console.log('memberResp', memberResp);
                       $scope.members = memberResp;
                       $scope.patient.memberId = $scope.patient.memberId;
                   });
               });
           }***/
        /*****************************************************/
        /*****************************************************/



        $scope.$watch('patient.seenby', function (newValue, oldValue) {
            //console.log('newValue', newValue);
            if (newValue == 3) {
                $scope.disablePatientForm = false;
            } else {
                $scope.disablePatientForm = true;
            }
        });


        $scope.addTest = function (id, index) {
            $scope.TestArray.push({
                result: '',
                existingFlag: 'true',
                deleteFlag: false,
                testnames: $scope.testnames,
                dateoftest: new Date()
            });
        };

        $scope.removeTest = function (id, index) {
            $scope.TestArray[index].deleteFlag = true;
        };

        $scope.addMedicine = function () {
            $scope.medicineList.push({
                medicineid: '',
                noofmedicine: 0,
                deleteFlag: false,
                existingFlag: false
            });
        };

        $scope.removeMedicine = function (index) {
            $scope.medicineList[index].deleteFlag = true;
        };

        /************************Vital Related*******************/
        var timeoutPromise;
        var delayInMs = 1200;
        $scope.ChnageVital = function (value, index, name, id, orderno) {
            $scope.disableUpdate = true;

            setTimeout(function () {
                $scope.disabledVitalText = true;
            }, 1100);

            /** if (name == 'Temperature') {
                 $scope.patient.temperature = $scope.vitalrecords[index].TextValue;

             } else if (name == 'Pulse Rate') {
                 $scope.patient.pulserate = $scope.vitalrecords[index].TextValue;

             } else if (name == 'BP (Diastolic)') {
                 $scope.patient.bpdiastolic = $scope.vitalrecords[index].TextValue;
             } else if (name == 'BP (Systolic)') {
                 $scope.patient.bpsystolic = $scope.vitalrecords[index].TextValue;
             } else if (name == 'SPO2') {
                 $scope.patient.spo = $scope.vitalrecords[index].TextValue;
             } else if (name == 'Hemoglobin') {
                 $scope.patient.hemoglobin = $scope.vitalrecords[index].TextValue;
             } else if (name == 'Sugar Level') {
                 $scope.patient.sugarlevel = $scope.vitalrecords[index].TextValue;
             } else if (name == 'RR') {
                 $scope.patient.rr = $scope.vitalrecords[index].TextValue;
             } else if (name == 'Height') {
                 $scope.patient.height = $scope.vitalrecords[index].TextValue;
             } else if (name == 'Weight') {
                 $scope.patient.weight = $scope.vitalrecords[index].TextValue;
             } else if (name == 'BMI') {
                 $scope.patient.bmi = $scope.vitalrecords[index].TextValue;
                 //console.log('$scope.patient', $scope.patient);
             }*/


            $timeout.cancel(timeoutPromise);
            timeoutPromise = $timeout(function () {
                //$scope.disabledVitalText = false;
                $scope.disableUpdate = false;
                $scope.changeVitalOne(id, value, index, name, orderno);
            }, delayInMs);
        }

        $scope.changeVitalOne = function (id, value, index, name, orderno) {
            $scope.disabledVitalText = false;
            $scope.validatestring = '';
            Restangular.one('vitalrecords', id).get().then(function (Resp) {
                // console.log('Resp.lowerBound', Resp.lowerBound);
                // console.log('Resp.upperBound', Resp.upperBound);

                if (value < parseInt(Resp.lowerBound) || value > parseInt(Resp.upperBound)) {

                    $scope.validatestring = $scope.validatestring + ' Enter' + ' ' + name + ' ' + 'between' + ' ' + Resp.lowerBound + ' - ' + Resp.upperBound;
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.validatestring;
                    $scope.vitalrecords[index].TextValue = '';
                }
            });
        };

        $scope.changeNonSysRecordOne = function (id, value, index, name, orderno) {
            $scope.disableSave = false;
            $scope.validatestring = '';
            Restangular.one('vitalrecords', id).get().then(function (Resp) {
                if (value < parseInt(Resp.lowerBound) || value > parseInt(Resp.upperBound)) {
                    $scope.validatestring = $scope.validatestring + ' Enter' + ' ' + name + ' ' + 'between' + ' ' + Resp.lowerBound + ' - ' + Resp.upperBound;
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.validatestring;
                    $scope.nonsystematicrecords[index].nonSystematicValue = '';
                }
            });
        };

        $scope.ChnageNonSysRecord = function (value, index, name, id, orderno) {

            $scope.disableSave = true;

            setTimeout(function () {
                $scope.disabledNonSysText = true;
            }, 1100);

            $timeout.cancel(timeoutPromise);
            timeoutPromise = $timeout(function () {
                $scope.disabledNonSysText = false;
                $scope.changeNonSysRecordOne(id, value, index, name, orderno);
            }, delayInMs);
        };

        /*******************************/

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /*********************************SAVE*********************************/

        $scope.validatestring = '';

        $scope.UpdatePatientRecord = function () {

            document.getElementById('name').style.border = "";
            document.getElementById('age').style.border = "";
            document.getElementById('contact').style.border = "";
            document.getElementById('amount').style.border = "";

            if ($scope.patient.memberofShgFlag == '' || $scope.patient.memberofShgFlag == null) {
                $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectmemberOfSHG;

            } else if ($scope.patient.memberofShgFlag == 'true') {

                if ($scope.patient.memberId == '' || $scope.patient.memberId == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectpatientnameSHG;
                } else if ($scope.patient.visitdate == '' || $scope.patient.visitdate == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectVisitDate;
                } else if ($scope.patient.symptom == '' || $scope.patient.symptom == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectSymptom;
                }

            } else if ($scope.patient.memberofShgFlag == 'false') {

                if ($scope.patient.patientname == '' || $scope.patient.patientname == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.enterContact;
                } else if ($scope.patient.age == '' || $scope.patient.age == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.enterAge;
                } else if ($scope.patient.gender == '' || $scope.patient.gender == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectGender;
                } else if ($scope.patient.migrant == '' || $scope.patient.migrant == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectMigrant;
                } else if ($scope.patient.associatedHF == '' || $scope.patient.associatedHF == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectAssignedTo;
                } else if ($scope.patient.visitdate == '' || $scope.patient.visitdate == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectVisitDate;
                } else if ($scope.patient.symptom == '' || $scope.patient.symptom == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectSymptom;
                }

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.ValidateVitalRecord();
            }
        };

        $scope.VitalCount = 0;

        $scope.ValidateVitalRecord = function () {

            $scope.validatestring = '';

            if ($scope.VitalCount < $scope.vitalrecords.length) {
                if ($scope.vitalrecords[$scope.VitalCount].mandatoryFlag == true && $scope.vitalrecords[$scope.VitalCount].TextValue == '') {

                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.enter + ' ' + $scope.vitalrecords[$scope.VitalCount].name
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.validatestring;

                } else {
                    $scope.VitalCount++;
                    $scope.ValidateVitalRecord();
                }

            } else {

                if ($scope.patient.illness == '' || $scope.patient.illness == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectIllness;
                } else if ($scope.patient.seenby == '' || $scope.patient.seenby == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectSeenBy;
                } else if ($scope.patient.amountChagred == '' || $scope.patient.amountChagred == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.enterAmount;
                    document.getElementById('amount').style.borderColor = "#FF0000";
                }

                if ($scope.validatestring != '') {
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.validatestring;
                    $scope.validatestring = '';
                } else {
                    $scope.ValidateNonSysRecord();
                }
            }
        };

        /*********************** Validate Non Systematic ******************************/

        $scope.NonSysCount = 0;

        $scope.ValidateNonSysRecord = function () {

            $scope.validatestring = '';

            if ($scope.NonSysCount < $scope.nonsystematicrecords.length) {

                // console.log($scope.nonsystematicrecords[$scope.NonSysCount]);

                if ($scope.nonsystematicrecords[$scope.NonSysCount].mandatoryFlag == true && $scope.nonsystematicrecords[$scope.NonSysCount].nonSystematicValue == '') {

                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.enter + ' ' + $scope.nonsystematicrecords[$scope.NonSysCount].name;
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.validatestring;

                } else {
                    $scope.NonSysCount++;
                    $scope.ValidateNonSysRecord();
                }

            } else {

                document.getElementById('amount').style.borderColor = "";

                if ($scope.patient.illness == '' || $scope.patient.illness == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectIllness;

                } else if ($scope.patient.seenby == '' || $scope.patient.seenby == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectSeenBy;

                } else if ($scope.patient.amountChagred == '' || $scope.patient.amountChagred == null) {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.enterAmount;
                    document.getElementById('amount').style.borderColor = "#FF0000";
                }

                if ($scope.validatestring != '') {
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.validatestring;
                    $scope.validatestring = '';
                } else {
                    $scope.ValidateMedicine();
                }
            }
        };

        /********************* end **********************************************/

        /*********************** Validate Medicine ******************************/

        $scope.medCount = 0;

        $scope.ValidateMedicine = function () {

            $scope.validatestring = '';

            if ($scope.medCount < $scope.medicineList.length) {
                
               // console.log($scope.medicineList[$scope.medCount]);

                if ($scope.medicineList[$scope.medCount].medicineid == null || $scope.medicineList[$scope.medCount].medicineid == '') {
                    $scope.validatestring = $scope.validatestring + $scope.PRLanguage.selectMedicineName;
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.validatestring;
                } else {
                    $scope.medCount++;
                    $scope.ValidateMedicine();
                }
            } else {

                if ($scope.validatestring != '') {
                    $scope.toggleValidation();
                    $scope.validatestring1 = $scope.validatestring;
                    $scope.validatestring = '';
                } else {
                    $scope.FinalSaveFunction();
                }
            }
        };

        /******************************* end **********************************/


        $scope.FinalSaveFunction = function () {

            $scope.toggleLoading();

            if ($scope.patient.memberofShgFlag == 'true') {
                $scope.patient.memberofShgFlag = true;
            } else {
                $scope.patient.memberofShgFlag = false;
            }

            $scope.patient.lastModifiedDate = new Date();
            $scope.patient.lastModifiedBy = $window.sessionStorage.userId;
            $scope.patient.lastModifiedByRole = $window.sessionStorage.roleId;

            Restangular.one('patientrecords', $routeParams.id).customPUT($scope.patient).then(function (Response) {
                $scope.patientId = Response.id;
                $scope.saveVitaltextVAlue(Response.id);
            });
        };


        /*********************************VITal record Value********************/

        $scope.vitalCountNew = 0;

        $scope.saveVitaltextVAlue = function (patientId) {

            if ($scope.vitalCountNew < $scope.vitalrecords.length) {

                if ($scope.vitalrecords[$scope.vitalCountNew].existingFlag == true) {
                    $scope.vitalrecords[$scope.vitalCountNew].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.vitalrecords[$scope.vitalCountNew].lastModifiedDate = new Date();
                    $scope.vitalrecords[$scope.vitalCountNew].lastModifiedByRole = $window.sessionStorage.roleId;
                    $scope.vitalrecords[$scope.vitalCountNew].vitalValue = $scope.vitalrecords[$scope.vitalCountNew].TextValue;

                    Restangular.one('vaitalrecordtrailers', $scope.vitalrecords[$scope.vitalCountNew].newId).customPUT($scope.vitalrecords[$scope.vitalCountNew]).then(function (childResp) {

                       // console.log('$scope.vitalrecords[$scope.vitalCountNew]', $scope.vitalrecords[$scope.vitalCountNew]);
                        $scope.vitalCountNew++;
                        $scope.saveVitaltextVAlue(patientId);
                    });

                } else {

                    if ($scope.vitalrecords[$scope.vitalCountNew].TextValue == null || $scope.vitalrecords[$scope.vitalCountNew].TextValue == '') {
                        $scope.vitalCountNew++;
                        $scope.saveVitaltextVAlue(patientId);
                    } else {

                        $scope.vitalrecords[$scope.vitalCountNew].patientHeaderId = patientId;
                        $scope.vitalrecords[$scope.vitalCountNew].vitalValue = $scope.vitalrecords[$scope.vitalCountNew].TextValue;
                        if ($window.sessionStorage.language == 1) {
                            $scope.vitalrecords[$scope.vitalCountNew].vitalrecordId = $scope.vitalrecords[$scope.vitalCountNew].id;
                        } else {
                            $scope.vitalrecords[$scope.vitalCountNew].vitalrecordId = $scope.vitalrecords[$scope.vitalCountNew].parentId;
                        }
                        $scope.vitalrecords[$scope.vitalCountNew].deleteFlag = false;
                        $scope.vitalrecords[$scope.vitalCountNew].countryId = $window.sessionStorage.countryId;
                        $scope.vitalrecords[$scope.vitalCountNew].stateId = $window.sessionStorage.stateId;
                        $scope.vitalrecords[$scope.vitalCountNew].districtId = $window.sessionStorage.districtId;
                        $scope.vitalrecords[$scope.vitalCountNew].siteId = $window.sessionStorage.siteId.split(",")[0];
                        $scope.vitalrecords[$scope.vitalCountNew].createdBy = $window.sessionStorage.userId;
                        $scope.vitalrecords[$scope.vitalCountNew].createdDate = new Date();
                        $scope.vitalrecords[$scope.vitalCountNew].createdByRole = $window.sessionStorage.roleId;
                        $scope.vitalrecords[$scope.vitalCountNew].lastModifiedBy = $window.sessionStorage.userId;
                        $scope.vitalrecords[$scope.vitalCountNew].lastModifiedDate = new Date();
                        $scope.vitalrecords[$scope.vitalCountNew].lastModifiedByRole = $window.sessionStorage.roleId;
                        delete $scope.vitalrecords[$scope.vitalCountNew]['id'];
                        Restangular.all('vaitalrecordtrailers').post($scope.vitalrecords[$scope.vitalCountNew]).then(function (vitalResp) {
                            //console.log('vitalResp', vitalResp);
                            $scope.vitalCountNew++;
                            $scope.saveVitaltextVAlue(patientId);
                        });
                    }
                }
            } else {
                $scope.saveNonSysRecord(patientId);
            }
        };

        /*************************************************************/

        /*********************************** Non systematic record *********/

        $scope.nonSysCountNew = 0;

        $scope.saveNonSysRecord = function (patientId) {

            if ($scope.nonSysCountNew < $scope.nonsystematicrecords.length) {

                if ($scope.nonsystematicrecords[$scope.nonSysCountNew].existingFlag == true) {
                    $scope.nonsystematicrecords[$scope.nonSysCountNew].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.nonsystematicrecords[$scope.nonSysCountNew].lastModifiedDate = new Date();
                    $scope.nonsystematicrecords[$scope.nonSysCountNew].lastModifiedByRole = $window.sessionStorage.roleId;
                    $scope.nonsystematicrecords[$scope.nonSysCountNew].nonSystematicValue = $scope.nonsystematicrecords[$scope.nonSysCountNew].nonSystematicValue;

                    Restangular.one('nonsystematicrecordtrailers', $scope.nonsystematicrecords[$scope.nonSysCountNew].newId).customPUT($scope.nonsystematicrecords[$scope.nonSysCountNew]).then(function (nonSysResp1) {
                        $scope.nonSysCountNew++;
                        $scope.saveNonSysRecord(patientId);
                    });

                } else {

                    if ($scope.nonsystematicrecords[$scope.nonSysCountNew].nonSystematicValue == null || $scope.nonsystematicrecords[$scope.nonSysCountNew].nonSystematicValue == '') {
                        $scope.nonSysCountNew++;
                        $scope.saveNonSysRecord(patientId);
                    } else {

                        $scope.nonsystematicrecords[$scope.nonSysCountNew].patientHeaderId = patientId;

                        if ($window.sessionStorage.language == 1) {
                            $scope.nonsystematicrecords[$scope.nonSysCountNew].nonSystematicrecordId = $scope.nonsystematicrecords[$scope.nonSysCountNew].id;
                        } else {
                            $scope.nonsystematicrecords[$scope.nonSysCountNew].nonSystematicrecordId = $scope.nonsystematicrecords[$scope.nonSysCountNew].parentId;
                        }

                        $scope.nonsystematicrecords[$scope.nonSysCountNew].deleteFlag = false;
                        $scope.nonsystematicrecords[$scope.nonSysCountNew].countryId = $window.sessionStorage.countryId;
                        $scope.nonsystematicrecords[$scope.nonSysCountNew].stateId = $window.sessionStorage.stateId;
                        $scope.nonsystematicrecords[$scope.nonSysCountNew].districtId = $window.sessionStorage.districtId;
                        $scope.nonsystematicrecords[$scope.nonSysCountNew].siteId = $window.sessionStorage.siteId.split(",")[0];
                        $scope.nonsystematicrecords[$scope.nonSysCountNew].createdBy = $window.sessionStorage.userId;
                        $scope.nonsystematicrecords[$scope.nonSysCountNew].createdDate = new Date();
                        $scope.nonsystematicrecords[$scope.nonSysCountNew].createdByRole = $window.sessionStorage.roleId;
                        $scope.nonsystematicrecords[$scope.nonSysCountNew].lastModifiedBy = $window.sessionStorage.userId;
                        $scope.nonsystematicrecords[$scope.nonSysCountNew].lastModifiedDate = new Date();
                        $scope.nonsystematicrecords[$scope.nonSysCountNew].lastModifiedByRole = $window.sessionStorage.roleId;
                        delete $scope.nonsystematicrecords[$scope.nonSysCountNew]['id'];
                        Restangular.all('nonsystematicrecordtrailers').post($scope.nonsystematicrecords[$scope.nonSysCountNew]).then(function (nonSysResp) {
                            $scope.nonSysCountNew++;
                            $scope.saveNonSysRecord(patientId);
                        });
                    }
                }
            } else {
                console.log('patientId', patientId)
                $scope.medicineListSave(patientId);
            }
        };

        /**************** end ********************************/

        /************************* medicine list *******************/

        $scope.medicineCountNew = 0;

        $scope.medicineListSave = function (patientId) {

            if ($scope.medicineCountNew < $scope.medicineList.length) {

                if ($scope.medicineList[$scope.medicineCountNew].existingFlag == true) {

                    $scope.medicineList[$scope.medicineCountNew].lastModifiedDate = new Date();
                    $scope.medicineList[$scope.medicineCountNew].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.medicineList[$scope.medicineCountNew].lastModifiedByRole = $window.sessionStorage.roleId;

                    Restangular.one('medicinegivens', $scope.medicineList[$scope.medicineCountNew].id).customPUT($scope.medicineList[$scope.medicineCountNew]).then(function (medResp1) {
                        $scope.medicineCountNew++;
                        $scope.medicineListSave(patientId);
                    });

                } else {

                    if ($scope.medicineList[$scope.medicineCountNew].medicineid == null || $scope.medicineList[$scope.medicineCountNew].medicineid == '') {
                        $scope.medicineCountNew++;
                        $scope.medicineListSave(patientId);
                    } else {

                        $scope.medicineList[$scope.medicineCountNew].patientrecordid = patientId;
                        $scope.medicineList[$scope.medicineCountNew].deleteFlag = false;
                        $scope.medicineList[$scope.medicineCountNew].countryId = $window.sessionStorage.countryId;
                        $scope.medicineList[$scope.medicineCountNew].stateId = $window.sessionStorage.stateId;
                        $scope.medicineList[$scope.medicineCountNew].districtId = $window.sessionStorage.districtId;
                        $scope.medicineList[$scope.medicineCountNew].siteId = $window.sessionStorage.siteId.split(",")[0];
                        $scope.medicineList[$scope.medicineCountNew].createdBy = $window.sessionStorage.userId;
                        $scope.medicineList[$scope.medicineCountNew].createdDate = new Date();
                        $scope.medicineList[$scope.medicineCountNew].createdByRole = $window.sessionStorage.roleId;
                        $scope.medicineList[$scope.medicineCountNew].lastModifiedBy = $window.sessionStorage.userId;
                        $scope.medicineList[$scope.medicineCountNew].lastModifiedDate = new Date();
                        $scope.medicineList[$scope.medicineCountNew].lastModifiedByRole = $window.sessionStorage.roleId;
                        delete $scope.medicineList[$scope.medicineCountNew]['id'];

                        Restangular.all('medicinegivens').post($scope.medicineList[$scope.medicineCountNew]).then(function (nonSysResp) {
                           // console.log(nonSysResp);
                            $scope.medicineCountNew++;
                            $scope.medicineListSave(patientId);
                        });
                    }
                }
            } else {
                $scope.testResultSave(patientId);
            }
        };

        /************** end **********************/

        $scope.TestCount = 0;

        $scope.testResultSave = function (patientId) {

            if ($scope.TestCount < $scope.TestArray.length) {

                if ($scope.TestArray[$scope.TestCount].existingFlag == true) {

                    $scope.TestArray[$scope.TestCount].lastModifiedDate = new Date();
                    $scope.TestArray[$scope.TestCount].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.TestArray[$scope.TestCount].lastModifiedByRole = $window.sessionStorage.roleId;

                    Restangular.one('testResults', $scope.TestArray[$scope.TestCount].id).customPUT($scope.TestArray[$scope.TestCount]).then(function (childResp) {
                        $scope.TestCount++;
                        $scope.testResultSave(patientId);
                    });

                } else {

                    if ($scope.TestArray[$scope.TestCount].testnameId == null || $scope.TestArray[$scope.TestCount].testnameId == '') {
                        $scope.TestCount++;
                        $scope.testResultSave(patientId);
                    } else {

                        $scope.TestArray[$scope.TestCount].patientId = $scope.patientId;
                        $scope.TestArray[$scope.TestCount].deleteFlag = false;
                        $scope.TestArray[$scope.TestCount].countryId = $window.sessionStorage.countryId;
                        $scope.TestArray[$scope.TestCount].stateId = $window.sessionStorage.stateId;
                        $scope.TestArray[$scope.TestCount].districtId = $window.sessionStorage.districtId;
                        $scope.TestArray[$scope.TestCount].siteId = $window.sessionStorage.siteId.split(",")[0];
                        $scope.TestArray[$scope.TestCount].createdBy = $window.sessionStorage.userId;
                        $scope.TestArray[$scope.TestCount].createdDate = new Date();
                        $scope.TestArray[$scope.TestCount].createdByRole = $window.sessionStorage.roleId;
                        $scope.TestArray[$scope.TestCount].lastModifiedBy = $window.sessionStorage.userId;
                        $scope.TestArray[$scope.TestCount].lastModifiedDate = new Date();
                        $scope.TestArray[$scope.TestCount].lastModifiedByRole = $window.sessionStorage.roleId;

                        Restangular.all('testResults').post($scope.TestArray[$scope.TestCount]).then(function (childResp) {
                            //  console.log('childResp', childResp);
                            $scope.TestCount++;
                            $scope.testResultSave(patientId);
                        });
                    }
                }
            } else {
                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setInterval(function () {
                    window.location = '/patientrecord-list';
                }, 1500);
            }
        };

        /************************************************************/

        //Datepicker settings start
        $scope.patient.visitdate = new Date();
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};
        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };
        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };
        $scope.openfamilydob = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#familydobopen' + index).focus();
            });
            item.familydobopened = true;
        };

        $scope.opentestdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickertestdate' + index).focus();
            });
            $scope.TestArray[index].testdateopened = true;
        };

        $scope.openmod = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickermod' + index).focus();
            });
            $scope.mod.openedmod = true;
        };
        $scope.openstart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };
        $scope.incidentopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerincident' + index).focus();
            });
            $scope.incident.incidentopened = true;
        };
        $scope.healthopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerhealth' + index).focus();
            });
            $scope.hlth.healthopened = true;
        };
        $scope.opendatestartedatart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdatestartedatart' + index).focus();
            });
            $scope.datestartedart.openeddatestartedatart = true;
        };
        $scope.lasttestdateopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.lasttest.lasttestdateopened = true;
        };
        $scope.termstartdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termstartdate.dobopened = true;
        };
        $scope.termenddate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termenddate.dobopened = true;
        };
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];
        //Datepicker settings end

    });
