'use strict';

angular.module('secondarySalesApp')
    .controller('SchoolCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter, $timeout, $modal) {

        $scope.school = {
            deleteFlag: false,
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        }

        $scope.HideSubmitButton = true;
        /************************ Language ************************/

        Restangular.one('schoollanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.schoolLanguage = langResponse[0];
            $scope.schoolHeading = $scope.schoolLanguage.schoolCreate;
            $scope.modalTitle = $scope.schoolLanguage.thankYou;
            $scope.message = $scope.schoolLanguage.thankYouCreated;
            $scope.mandatory = $scope.schoolLanguage.mandatory;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.disableAssigned = true;
            $scope.hideAddBtn = true;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.school.associatedHF = $window.sessionStorage.userId;

            });
        } else {

            $scope.disableAssigned = false;
            $scope.hideAddBtn = false;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.school.associatedHF = urs[0].id;

            });

        }

        $scope.schooltypes = Restangular.all('schooltypes?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

      $scope.areas = Restangular.all('areas?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().$object;
        /******************************google map****************************/
        $scope.LocateMe = function () {
            $scope.mapdataModal = true;

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 4,

                center: new google.maps.LatLng(12.9538477, 77.3507369),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            var marker, i;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(12.9538477, 77.3507369),
                map: map,
                html: ''
            });

            $scope.toggleMapModal();
        };

        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            ///////////////////////////////////////////////////////MAP//////////////////////////

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                //  console.log(latLng);
                $scope.school.latitude = latLng.lat();
                $scope.school.longitude = latLng.lng();

                //  console.log('$scope.updatepromotion', $scope.updatepromotion);

                document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
            }
            var map;

            function initialize() {

                $scope.latitude = 12.9538477;
                $scope.longitude = 77.3507369;
                navigator.geolocation.getCurrentPosition(function (location) {
                    //                    console.log(location.coords.latitude);
                    //                    console.log(location.coords.longitude);
                    //                    console.log(location.coords.accuracy);
                    $scope.latitude = location.coords.latitude;
                    $scope.longitude = location.coords.longitude;
                    //                });

                    // console.log('$scope.address', $scope.address);

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 4,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });
                });


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(10);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                //  console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };
        /************************** MAP ************************/

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };



        $scope.interventionModel = false;

        $scope.popupObj = {};
        /*********************************SAVE*********************************/
        $scope.validatestring = '';
        $scope.SaveschoolRecord = function () {


            document.getElementById('name').style.border = "";
            document.getElementById('teacherNo').style.border = "";
            document.getElementById('male').style.border = "";
            document.getElementById('female').style.border = "";

            if ($scope.school.schoolname == '' || $scope.school.schoolname == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.enterSchool;
                document.getElementById('name').style.borderColor = "#FF0000";
            } else if ($scope.school.dateofAdding == '' || $scope.school.dateofAdding == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.selectdateofAdding;

            } else if ($scope.school.associatedHF == '' || $scope.school.associatedHF == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.selectAssignedTo;

            } else if ($scope.school.area == '' || $scope.school.area == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.selectarea;

            } else if ($scope.school.latitude == '' || $scope.school.latitude == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.enterLatitude;

            } else if ($scope.school.longitude == '' || $scope.school.longitude == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.enterLongitude;

            } else if ($scope.school.typeofSchool == '' || $scope.school.typeofSchool == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.selectschoolType;
            } else if ($scope.school.noofTeacher == '' || $scope.school.noofTeacher == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.enterTeacherNo;
                document.getElementById('teacherNo').style.borderColor = "#FF0000";
            } else if ($scope.school.noofChildrenMale == '' || $scope.school.noofChildrenMale == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.enterMaleStudent;
                document.getElementById('male').style.borderColor = "#FF0000";
            } else if ($scope.school.noofChildrenFemale == '' || $scope.school.noofChildrenFemale == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.enterFemaleStudent;
                document.getElementById('female').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';

            } else {
                $scope.popupObj.dateofAdding = $filter('date')($scope.school.dateofAdding, 'dd-MMM-yyyy');
                $scope.popupObj.schoolname = $scope.school.schoolname;
                $scope.popupObj.noofTeacher = $scope.school.noofTeacher;
                $scope.popupObj.noofChildrenMale = $scope.school.noofChildrenMale;
                $scope.popupObj.noofChildrenFemale = $scope.school.noofChildrenFemale;

                $scope.interventionModel = true;
                  if ($window.sessionStorage.language == 1) {

                    Restangular.one('users', $scope.school.associatedHF).get().then(function (udr) {
                        Restangular.one('schooltypes', $scope.school.typeofSchool).get().then(function (vdr) {
                            Restangular.one('areas', $scope.school.area).get().then(function (tdr) {
                                 $scope.popupObj.associatedHF = udr.name;
                                $scope.popupObj.area = vdr.name;
                                $scope.popupObj.typeofSchool = tdr.name;
                            });
                        });
                    });
                    
                } else {

                    Restangular.one('users', $scope.school.associatedHF).get().then(function (udr) {
                        Restangular.one('schooltypes/findOne?filter[where][parentId]=' + $scope.school.typeofSchool + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (vdr) {
                            Restangular.one('areas/findOne?filter[where][id]=' + $scope.school.area).get().then(function (tdr) {
                                $scope.popupObj.associatedHF = udr.name;
                                $scope.popupObj.area = vdr.name;
                                $scope.popupObj.typeofSchool = tdr.name;
                            });
                        });
                    });
                }
            }



        };
        $scope.SavePopUp = function () {

            $scope.toggleLoading();
            Restangular.all('schoolrecords').post($scope.school).then(function (Response) {
                // console.log('Response', Response);

                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setInterval(function () {
                    window.location = '/school-list';
                }, 1500);

            });

        };

        /************************************************************/

        //Datepicker settings start
        $scope.school.dateofAdding = new Date();
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};
        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };
        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };
        $scope.openfamilydob = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#familydobopen' + index).focus();
            });
            item.familydobopened = true;
        };
        $scope.openmod = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickermod' + index).focus();
            });
            $scope.mod.openedmod = true;
        };
        $scope.openstart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };
        $scope.incidentopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerincident' + index).focus();
            });
            $scope.incident.incidentopened = true;
        };
        $scope.healthopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerhealth' + index).focus();
            });
            $scope.hlth.healthopened = true;
        };
        $scope.opendatestartedatart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdatestartedatart' + index).focus();
            });
            $scope.datestartedart.openeddatestartedatart = true;
        };
        $scope.lasttestdateopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.lasttest.lasttestdateopened = true;
        };
        $scope.termstartdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termstartdate.dobopened = true;
        };
        $scope.termenddate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termenddate.dobopened = true;
        };
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];
        //Datepicker settings end

    });
