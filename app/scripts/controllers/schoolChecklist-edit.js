'use strict';

angular.module('secondarySalesApp')
    .controller('SchoolCheckListEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter, $timeout, $modal) {

        $scope.HideSubmitButton = false;
        $scope.school = {};
        /************************ Language ************************/

        Restangular.one('schoollanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.schoolLanguage = langResponse[0];
            $scope.schoolHeading = $scope.schoolLanguage.schoolchecklistEdit;
            $scope.modalTitle = $scope.schoolLanguage.thankYou;
            $scope.message = $scope.schoolLanguage.thankYouUpdatedchecklist;
            $scope.mandatory = $scope.schoolLanguage.mandatory;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.disableAssigned = true;
            $scope.hideAddBtn = true;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.school.associatedHF = $window.sessionStorage.userId;

            });
        } else {

            $scope.disableAssigned = false;
            $scope.hideAddBtn = false;

            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.school.associatedHF = urs[0].id;

            });

        }

        Restangular.all('schoolrecords?filter[where][deleteFlag]=false').getList().then(function (schoolResp) {
            $scope.schoolrecords = schoolResp;
            $scope.school.schoolName = $scope.school.schoolName;
        });

        Restangular.all('dynamiclists?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (Response) {

            $scope.dynamiclists = $filter('orderBy')(Response, 'orderNo');

            angular.forEach($scope.dynamiclists, function (member, index) {
                // console.log('member', member);
                member.optionName = '';
                member.optionValue = '';
                member.existingFlag = false;
                member.optionName = member.option.split(',');
                member.sequence = parseInt(member.orderNo);

                member.optionArray = [];
                for (var i = 0; i < member.optionName.length; i++) {

                    member.optionArray.push({
                        'seq': i + 1,
                        'value': member.optionName[i]
                    });
                }
                if (member.mandatoryFlag == true) {
                    member.color = 'red';
                } else {
                    member.color = 'black';
                }

            });
        });

        if ($routeParams.id) {
            Restangular.one('schoolchecklists', $routeParams.id).get().then(function (school) {
                $scope.original = school;
                $scope.school = Restangular.copy($scope.original);

                Restangular.all('schoolchecklisttrailers?filter[where][schoolheaderId]=' + $routeParams.id).getList().then(function (checkResp) {
                    //console.log('vitalValue', vitalValue);
                    $scope.schoolchecklisttrailers = checkResp;
                    // });
                    angular.forEach($scope.dynamiclists, function (member, index) {
                        // member.sequence = parseInt(member.orderNo);

                        for (var i = 0; i < $scope.schoolchecklisttrailers.length; i++) {
                            
                            if ($window.sessionStorage.language == 1) {

                                if (member.id == $scope.schoolchecklisttrailers[i].checklistId) {
                                    member.optionValue = $scope.schoolchecklisttrailers[i].option;
                                    member.existingFlag = true;
                                    member.newId = $scope.schoolchecklisttrailers[i].id;
                                    delete member['id'];
                                    //console.log('member.optionValue', member.optionValue);
                                    break;
                                }
                            } else {
                                if (member.parentId == $scope.schoolchecklisttrailers[i].checklistId) {
                                    member.optionValue = $scope.schoolchecklisttrailers[i].option;
                                    member.existingFlag = true;
                                    member.newId = $scope.schoolchecklisttrailers[i].id;
                                    delete member['id'];
                                   // console.log('member.optionValue', member.optionValue);
                                    break;
                                }
                            }
                        }



                    });
                });
            });
        }

        $scope.ChnageDropDown = function (option, index, question, id, sequence) {

            $scope.dynamiclists[index].optionValue = option;
        };


        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


        /*********************************SAVE*********************************/
        $scope.validatestring = '';

        $scope.UpdateschoolRecord = function () {

            if ($scope.school.schoolName == '' || $scope.school.schoolName == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.selectSchool;

            } else if ($scope.school.dateofLastcheck == '' || $scope.school.dateofLastcheck == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.selectDate;

            } else if ($scope.school.associatedHF == '' || $scope.school.associatedHF == null) {
                $scope.validatestring = $scope.validatestring + $scope.schoolLanguage.assignedTo;
            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.ValidateschoolCheckList();
            }
        };

        $scope.CheckListCount = 0;
        $scope.ValidateschoolCheckList = function () {


            $scope.validatestring = '';

            if ($scope.CheckListCount < $scope.dynamiclists.length) {


                if ($scope.dynamiclists[$scope.CheckListCount].color == 'red') {

                    if ($scope.dynamiclists[$scope.CheckListCount].optionValue == '') {

                        $scope.validatestring = $scope.validatestring + 'Select' + ' ' + $scope.dynamiclists[$scope.CheckListCount].question;
                        $scope.toggleValidation();
                        $scope.validatestring1 = $scope.validatestring;
                        $scope.validatestring = '';

                    } else {
                        $scope.CheckListCount++;
                        $scope.ValidateschoolCheckList();
                    }

                } else {
                    $scope.CheckListCount++;
                    $scope.ValidateschoolCheckList();
                }
            } else {
                $scope.FinalSaveFunction();
            }

        };

        $scope.FinalSaveFunction = function () {
            $scope.toggleLoading();
            //console.log('$scope.dynamiclists', $scope.dynamiclists);

            Restangular.all('schoolchecklists', $routeParams.id).customPUT($scope.school).then(function (Response) {
                // console.log('Response', Response);
                $scope.schoolHeaderId = Response.id;
                $scope.saveOptionValue($scope.schoolHeaderId);


            });


        };

        /*********************************VITal record Value********************/
        $scope.CheckCount = 0;
        $scope.saveOptionValue = function (schoolId) {

            if ($scope.CheckCount < $scope.dynamiclists.length) {

                if ($scope.dynamiclists[$scope.CheckCount].existingFlag == true) {
                    $scope.dynamiclists[$scope.CheckCount].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.dynamiclists[$scope.CheckCount].lastModifiedDate = new Date();
                    $scope.dynamiclists[$scope.CheckCount].lastModifiedByRole = $window.sessionStorage.roleId;
                    $scope.dynamiclists[$scope.CheckCount].option = $scope.dynamiclists[$scope.CheckCount].optionValue;

                    Restangular.one('schoolchecklisttrailers', $scope.dynamiclists[$scope.CheckCount].newId).customPUT($scope.dynamiclists[$scope.CheckCount]).then(function (childResp) {


                        $scope.CheckCount++;
                        $scope.saveOptionValue(schoolId);
                    });

                } else {

                    if ($scope.dynamiclists[$scope.CheckCount].optionValue == null || $scope.dynamiclists[$scope.CheckCount].optionValue == '') {

                        $scope.CheckCount++;
                        $scope.saveOptionValue(schoolId);

                    } else {

                        $scope.dynamiclists[$scope.CheckCount].schoolheaderId = schoolId;
                        $scope.dynamiclists[$scope.CheckCount].option = $scope.dynamiclists[$scope.CheckCount].optionValue;

                        if ($window.sessionStorage.language == 1) {
                            $scope.dynamiclists[$scope.CheckCount].checklistId = $scope.dynamiclists[$scope.CheckCount].id;
                        } else {
                            $scope.dynamiclists[$scope.CheckCount].checklistId = $scope.dynamiclists[$scope.CheckCount].parentId;
                        }

                        $scope.dynamiclists[$scope.CheckCount].deleteFlag = false;
                        $scope.dynamiclists[$scope.CheckCount].countryId = $window.sessionStorage.countryId;
                        $scope.dynamiclists[$scope.CheckCount].stateId = $window.sessionStorage.stateId;
                        $scope.dynamiclists[$scope.CheckCount].districtId = $window.sessionStorage.districtId;
                        $scope.dynamiclists[$scope.CheckCount].siteId = $window.sessionStorage.siteId.split(",")[0];
                        $scope.dynamiclists[$scope.CheckCount].createdBy = $window.sessionStorage.userId;
                        $scope.dynamiclists[$scope.CheckCount].createdDate = new Date();
                        $scope.dynamiclists[$scope.CheckCount].createdByRole = $window.sessionStorage.roleId;
                        $scope.dynamiclists[$scope.CheckCount].lastModifiedBy = $window.sessionStorage.userId;
                        $scope.dynamiclists[$scope.CheckCount].lastModifiedDate = new Date();
                        $scope.dynamiclists[$scope.CheckCount].lastModifiedByRole = $window.sessionStorage.roleId;
                        delete $scope.dynamiclists[$scope.CheckCount]['id'];
                        Restangular.all('schoolchecklisttrailers').post($scope.dynamiclists[$scope.CheckCount]).then(function (check) {

                            $scope.CheckCount++;
                            $scope.saveOptionValue(schoolId);
                        });
                    }
                }

            } else {
                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setInterval(function () {
                    window.location = '/schoolchecklist-list';
                }, 1500);
            }

        };

        /*************************************************************/

        /************************************************************/

        //Datepicker settings start
        //$scope.school.dateofLastcheck = new Date();
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};
        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };
        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };
        $scope.openfamilydob = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#familydobopen' + index).focus();
            });
            item.familydobopened = true;
        };
        $scope.openmod = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickermod' + index).focus();
            });
            $scope.mod.openedmod = true;
        };
        $scope.openstart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };
        $scope.incidentopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerincident' + index).focus();
            });
            $scope.incident.incidentopened = true;
        };
        $scope.healthopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerhealth' + index).focus();
            });
            $scope.hlth.healthopened = true;
        };
        $scope.opendatestartedatart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdatestartedatart' + index).focus();
            });
            $scope.datestartedart.openeddatestartedatart = true;
        };
        $scope.lasttestdateopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.lasttest.lasttestdateopened = true;
        };
        $scope.termstartdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termstartdate.dobopened = true;
        };
        $scope.termenddate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termenddate.dobopened = true;
        };
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];
        //Datepicker settings end

    });
