'use strict';

angular.module('secondarySalesApp')
	.controller('PhoneCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/phonetypes/create' || $location.path() === '/phonetypes/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/phonetypes/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/phonetypes/create' || $location.path() === '/phonetypes/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/phonetypes/create' || $location.path() === '/phonetypes/' + $routeParams.id;
			return visible;
		};


		/*********/
		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			//console.log('$scope.countryId From Landing', $scope.pageSize);
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			console.log('mypage', mypage);
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};


		console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
		if ($window.sessionStorage.prviousLocation != "partials/phonetypes") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
			$scope.currentpage = 1;
			$scope.pageSize = 25;
		}

		//  $scope.phonetypes = Restangular.all('phonetypes').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'phonetype has been Updated!';
			Restangular.one('phonetypes', $routeParams.id).get().then(function (phonetype) {
				$scope.original = phonetype;
				$scope.phonetype = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'phonetype has been Created!';
		}
		$scope.Search = $scope.name;

		/******************************** INDEX *******************************************/
		$scope.zn = Restangular.all('phonetypes?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.phonetypes = zn;
			angular.forEach($scope.phonetypes, function (member, index) {
				member.index = index + 1;
			});
		});

		/********************************************* SAVE *******************************************/
		$scope.phonetype = {
			"name": '',
			"deleteflag": false
		};
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.phonetype.name == '' || $scope.phonetype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.phonetype.hnname == '' || $scope.phonetype.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name in Hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.phonetype.knname == '' || $scope.phonetype.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name in Kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.phonetype.taname == '' || $scope.phonetype.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name in Tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.phonetype.tename == '' || $scope.phonetype.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name in Telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.phonetype.mrname == '' || $scope.phonetype.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name in Marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.phonetypes.post($scope.phonetype).then(function () {
					window.location = '/phonetypes';
				});
			};
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

		/***************************************************** UPDATE *******************************************/
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.phonetype.name == '' || $scope.phonetype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.phonetype.hnname == '' || $scope.phonetype.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name in Hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.phonetype.knname == '' || $scope.phonetype.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name in Kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.phonetype.taname == '' || $scope.phonetype.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name in Tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.phonetype.tename == '' || $scope.phonetype.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name in Telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.phonetype.mrname == '' || $scope.phonetype.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter phonetype Name in Marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				Restangular.one('phonetypes', $routeParams.id).customPUT($scope.phonetype).then(function () {
					console.log('phonetype Saved');
					window.location = '/phonetypes';
				});
			}
		};
		/******************************************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('phonetypes/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

	});
