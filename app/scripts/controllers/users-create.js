'use strict';

angular.module('secondarySalesApp')
    .controller('UsersCreateCtrl', function ($scope, Restangular, $http, $window, $route, $filter, AnalyticsRestangular) {

        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.roleId = $window.sessionStorage.roleId;

        $scope.heading = 'Add User';
        $scope.Saved = false;
        $scope.Updated = true;
        $scope.roleDisable = false;
        $scope.userDisable = false;
        $scope.usernameDisable = false;
        $scope.hideTypeofDoctor = true;

        $scope.countryDisable = false;
        $scope.stateDisable = false;
        $scope.districtDisable = false;
        $scope.siteDisable = false;
        $scope.statusDisable = false;
        $scope.languageDisable = false;

        $scope.user = {
            languageId: 1,
            status: 'active',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteFlag: false
        };

        $scope.users = Restangular.all('users').getList().$object;
        $scope.analyticsusers = AnalyticsRestangular.all('users').getList().$object;
        //$scope.employees = Restangular.all('employees').getList().$object;
        $scope.groups = Restangular.all('groups').getList().$object;

        Restangular.all('roles?filter[order]=order ASC').getList().then(function (rle) {
            $scope.roles = rle;
            $scope.user.roleId = 3;
            // console.log('$scope.user', $scope.user);
        });


        //$scope.salesAreas = Restangular.all('sales-areas').getList().$object;
        $scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
        $scope.distributionSubareas = Restangular.all('distribution-subareas').getList().$object;
        $scope.lastModifiedBy = $window.sessionStorage.UserEmployeeId;

        //console.log('$window.sessionStorage.State', $window.sessionStorage.zoneId);
        //console.log('$window.sessionStorage.Distric', $window.sessionStorage.salesAreaId);
        //console.log('$window.sessionStorage.Facility', $window.sessionStorage.coorgId);
        //console.log('$window.sessionStorage.LastModifyBy', $scope.lastmodifiedby);
        //console.log('$window.sessionStorage.Site', $window.sessionStorage.UserEmployeeId);
        /*
		"state": null,
		"district": null,
		"site": null,
		"facility": null,
		"lastmodifiedby": null,
		"lastmodifiedtime": null
*/
        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (lRas) {
            $scope.languages = lRas;
        });

        $scope.groups = Restangular.all('groups?filter[where][deleteFlag]=null').getList().$object;
        $scope.departments = Restangular.all('departments?filter[where][deleteflag]=null').getList().$object;
        $scope.ims = Restangular.all('ims?filter[where][deleteflag]=null').getList().$object;
        $scope.admins = Restangular.all('employees?filter[where][groupId]=1&filter[where][deleteflag]=false').getList().$object;
        //$scope.spms = Restangular.all('employees?filter[where][groupId]=3&filter[where][deleteflag]=false').getList().$object;
        //$scope.rios = Restangular.all('employees?filter[where][groupId]=4&filter[where][deleteflag]=null').getList().$object;
        //$scope.fieldworkers = Restangular.all('fieldworkers?filter[where][groupId]=4').getList().$object;

        $scope.fieldworkers = Restangular.all('fieldworkers?filter[where][deleteflag]=false&filter[where][usercreated]=false').getList().$object;


        $scope.comembers = Restangular.all('comembers?filter[where][deleteflag]=false&filter[where][usercreated]=false').getList().$object;

        $scope.mentors = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=17').getList().$object;

        $scope.spms = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=3').getList().$object;

        $scope.rios = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=4').getList().$object;

        $scope.nos = Restangular.all('mentors?filter[where][deleteflag]=false&filter[where][usercreated]=false&filter[where][role]=16').getList().$object;

        $scope.employees = Restangular.all('employees?filter[where][deleteflag]=false&filter[where][usercreated]=false').getList().$object;

        Restangular.all('typeOfDoctors').getList().then(function (dctrs) {
            $scope.typeOfDoctors = dctrs;
            // $scope.user.typeOfDoctor = 1;
        });


        Restangular.all('countries?filter[where][deleteFlag]=false').getList().then(function (cntry) {
            $scope.countries = cntry;
            $scope.user.countryId = cntry[0].id;
        });

        $scope.user.countryId = '';
        $scope.user.stateId = '';
        $scope.user.districtId = '';
        $scope.countryid = '';
        $scope.zonalid = '';

        $scope.$watch('user.countryId', function (newValue, oldVale) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.all('states?filter={"where":{"and":[{"countryId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (ste) {
                    $scope.states = ste;
                });
                $scope.countryid = +newValue;
            }
        });

        $scope.$watch('user.stateId', function (newValue, oldVale) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.all('districts?filter={"where":{"and":[{"stateId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (dist) {
                    $scope.districts = dist;
                });
                $scope.zonalid = +newValue;
            }
        });

        $scope.$watch('user.districtId', function (newValue, oldVale) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.all('sites?filter={"where":{"and":[{"districtId":{"inq":[' + newValue + ']}},{"deleteFlag":{"inq":[false]}}]}}').getList().then(function (site) {
                    $scope.sites = site;
                });
            }
        });

        $scope.$watch('user.roleId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else if (newValue == 4) {
                $scope.hideTypeofDoctor = false;
            } else if (newValue != 4) {
                $scope.hideTypeofDoctor = true;
                $scope.user.typeOfDoctor = '';
            }
        });

        //$scope.salesAreas = Restangular.all('sales-areas?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][deleteflag]=null').getList().$object;

        //$scope.zones = Restangular.all('zones').getList().$object;

        //$scope.employees = Restangular.all('employees').getList().$object;
        //$scope.comembers = Restangular.all('comembers').getList().$object;
        //$scope.languagemasters = Restangular.all('languagemasters').getList().$object;

        /************************************************** SAVE *******************************/
        $scope.showUniqueValidation = false;

        $scope.createduser = {
            "usercreated": true
        }

        $scope.Validname = function ($event) {
            var arr = $scope.user.name.split(/\s+/);
            // console.log('arr.length', arr.length);
            if (arr.length == 0 || arr.length == 1) {
                alert('Invalid name!, Please enter full name');
                //  $scope.user.Name = '';
            } else {
                return;
            }
        };

        $(function () {
            var txt = $("input#userName");
            var func = function () {
                txt.val(txt.val().replace(/\s/g, ''));
            }
            txt.keyup(func).blur(func);
        });

        $('#userName').keypress(function (evt) {
            if (/^[a-zA-Z\d\-_.,\s]+$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $('#mobile').keypress(function (evt) {
            if (/^-?[0-9]\d*(\.\d+)?$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $scope.validatestring = '';

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('userName').style.borderColor = "";
            document.getElementById('password').style.border = "";
            document.getElementById('mobile').style.border = "";
            document.getElementById('email').style.border = "";

            if ($scope.user.name == '' || $scope.user.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.user.roleId == '' || $scope.user.roleId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Role';

            } else if (($scope.user.roleId != 1) && ($scope.user.countryId == '' || $scope.user.countryId == null)) {
                $scope.validatestring = $scope.validatestring + 'Please Select Country';

            } else if (($scope.user.roleId != 1 && $scope.user.roleId != 2) && ($scope.user.stateId == '' || $scope.user.stateId == null)) {
                $scope.validatestring = $scope.validatestring + 'Please Select State';

            } else if (($scope.user.roleId != 1 && $scope.user.roleId != 2 && $scope.user.roleId != 6) && ($scope.user.districtId == '' || $scope.user.districtId == null)) {
                $scope.validatestring = $scope.validatestring + 'Please Select District';

            } else if (($scope.user.roleId != 1 && $scope.user.roleId != 2 && $scope.user.roleId != 6&& $scope.user.roleId != 5) && ($scope.user.siteId == '' || $scope.user.siteId == null)) {
                $scope.validatestring = $scope.validatestring + 'Please Select Site';

            } else if ($scope.user.username == '' || $scope.user.username == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter User Name';
                document.getElementById('userName').style.borderColor = "#FF0000";

            } else if ($scope.user.password == '' || $scope.user.password == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Your Password';
                document.getElementById('password').style.borderColor = "#FF0000";

            } else if ($scope.user.password.length != 4) {
                $scope.validatestring = $scope.validatestring + 'Please Enter (4-digit) Password';
                document.getElementById('password').style.borderColor = "#FF0000";

            }
            else if ($scope.user.languageId == '' || $scope.user.languageId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Your Language';

            } else if ($scope.user.mobile == '' || $scope.user.mobile == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Your Mobile Number';
                document.getElementById('mobile').style.borderColor = "#FF0000";

            } else if ($scope.user.mobile.length != 10) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Valid Mobile Number';
                document.getElementById('mobile').style.borderColor = "#FF0000";

            } else if ($scope.user.email == '' || $scope.user.email == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Email ID';
                document.getElementById('email').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring2 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                //alert("Ok");
                //$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.users.post($scope.user).then(function (PostResponce) {
                        console.log('$scope.user', PostResponce);
                        $scope.user.id = PostResponce.id;
                        $scope.user.roleid = PostResponce.roleId;
                        $scope.user.coorg_id = PostResponce.coorgId;
                        $scope.user.employee_id = PostResponce.employeeid;
                        $scope.user.zone_id = PostResponce.zoneId;
                        $scope.user.salesarea_id = PostResponce.salesAreaId;
                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        $scope.submitDisable = true;
                        window.location = '/user-list';
                    },
                    function (response) {
                        //alert(error.data.error.message);
                        $scope.showValidation = !$scope.showValidation;
                        
                        $scope.user.email = ''
                        $scope.validatestring1 = 'Email ID Already Exit';
                        //console.error('console.error', response);
                    });
            }
        };


        $scope.modalTitle = 'Thank You';
        $scope.message = 'User has been Created!';
        $scope.showValidation = false;
        /*$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};*/

        $scope.toggleValidation = function () {
            $scope.showUniqueValidation = !$scope.showUniqueValidation;
        };

        /******************************************** WATCH *************************/

        $scope.hideState = false;
        $scope.hideDistrict = false;
        $scope.hideCo = false;

        $scope.cofw = {
            data: 'co'
        };

        $scope.$watch('user.distributionAreaId', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.user.organizationId = newValue;
            }
        });

        $scope.$watch('user.salesAreaId', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.user.organizationId = newValue;
            }
        });

    });