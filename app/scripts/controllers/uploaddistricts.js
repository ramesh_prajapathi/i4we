'use strict';

angular.module('secondarySalesApp')
	.controller('DistReadXlsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $http, $window, $route) {

		console.log('$window.sessionStorage.organizationId', $window.sessionStorage.organizationId);
		$scope.hideSubmit = true;

		$scope.hideSubmit = true;
		$scope.DisableValidate = true;
		$scope.modalTitle = 'Wait';
		$scope.ValidationmessageTitle = 'Thank You';
		$scope.message = 'District Saving...';
		$scope.Validationmessage = 'Validated Sucessfully';
		$scope.validatestring1 = 'Some Of your data is not in proper format. Click on "Download Error Text" to download the error log.';
		$scope.validatestringxls = 'Your xls is not in specified format.';

		var X = XLSX;
		var XW = {
			/* worker message */
			msg: 'xlsx',
			/* worker scripts */
			rABS: 'scripts/services/xlsxworker2.js',
			norABS: 'scripts/services/xlsxworker1.js',
			noxfer: 'scripts/services/xlsxworker.js'
		};


		var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
		if (!rABS) {
			document.getElementsByName("userabs")[0].disabled = true;
			document.getElementsByName("userabs")[0].checked = false;
		}

		var use_worker = typeof Worker !== 'undefined';
		if (!use_worker) {
			document.getElementsByName("useworker")[0].disabled = true;
			document.getElementsByName("useworker")[0].checked = false;
		}

		function xw_noxfer(data, cb) {
			var worker = new Worker(XW.noxfer);
			worker.onmessage = function (e) {
				switch (e.data.t) {
					case 'ready':
						break;
					case 'e':
						console.error(e.data.d);
						break;
					case XW.msg:
						cb(JSON.parse(e.data.d));
						break;
				}
			};
			var arr = rABS ? data : btoa(fixdata(data));
			worker.postMessage({
				d: arr,
				b: rABS
			});
		}

		function xw_xfer(data, cb) {
			var worker = new Worker(rABS ? XW.rABS : XW.norABS);
			worker.onmessage = function (e) {
				switch (e.data.t) {
					case 'ready':
						break;
					case 'e':
						console.error(e.data.d);
						break;
					default:
						var xx = ab2str(e.data).replace(/\n/g, "\\n").replace(/\r/g, "\\r");
						console.log("done");
						$scope.DisableValidate = false;
						cb(JSON.parse(xx));
						break;
				}
			};
			if (rABS) {
				var val = s2ab(data);
				worker.postMessage(val[1], [val[1]]);
			} else {
				worker.postMessage(data, [data]);
			}
		}

		function xw(data, cb) {
			//transferable = document.getElementsByName("xferable")[0].checked;
			transferable = true;
			if (transferable) xw_xfer(data, cb);
			else xw_noxfer(data, cb);
		}

		var transferable = use_worker;
		if (!transferable) {
			document.getElementsByName("xferable")[0].disabled = true;
			document.getElementsByName("xferable")[0].checked = false;
		}

		function s2ab(s) {
			var b = new ArrayBuffer(s.length * 2),
				v = new Uint16Array(b);
			for (var i = 0; i != s.length; ++i) v[i] = s.charCodeAt(i);
			return [v, b];
		}

		function ab2str(data) {
			var o = "",
				l = 0,
				w = 10240;
			for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w, l * w + w)));
			o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w)));
			return o;
		}

		function to_json(workbook) {
			var result = {};
			workbook.SheetNames.forEach(function (sheetName) {
				var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
				if (roa.length > 0) {
					result[sheetName] = roa;
				}
			});
			return result;
		}

		function to_csv(workbook) {
			var result = [];
			workbook.SheetNames.forEach(function (sheetName) {
				var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
				if (csv.length > 0) {
					result.push("SHEET: " + sheetName);
					result.push("");
					result.push(csv);
				}
			});
			return result.join("\n");
		}

		function to_formulae(workbook) {
			var result = [];
			workbook.SheetNames.forEach(function (sheetName) {
				var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
				if (formulae.length > 0) {
					result.push("SHEET: " + sheetName);
					result.push("");
					result.push(formulae.join("\n"));
				}
			});
			return result.join("\n");
		}


		/*   $scope.xlsentries = {
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			deleteflag: false
		}; 
    
    console.log('$window.sessionStorage.UserEmployeeId',$window.sessionStorage.UserEmployeeId); */

		function process_wb(wb) {
			var output = "";
			switch (get_radio_value("format")) {
				case "csv":
					output = to_csv(wb);
					break;
				case "form":
					output = to_formulae(wb);
					break;
				default:
					output = JSON.stringify(to_json(wb), 2, 2);
					$scope.entries = to_json(wb);
					$scope.xlsentries = $scope.entries.Sheet1;

					angular.forEach($scope.xlsentries, function (member, index) {
						//Just add the index to your item
						member.index = index;
					});

					// console.log('output', $scope.entries.Sheet1);
			}
			/*if (out.innerText === undefined) out.textContent = output;
			else out.innerText = output;*/
			if (typeof console !== 'undefined') console.log("output", new Date());
		}

		function get_radio_value(radioName) {
			var radios = document.getElementsByName(radioName);
			for (var i = 0; i < radios.length; i++) {
				if (radios[i].checked || radios.length === 1) {
					return radios[i].value;
				}
			}
		}




		var xlf = document.getElementById('xlf');
		console.log('Event Start');

		function handleFile(e) {
			$scope.hideSubmit = true;
			$scope.hideValidate = false;
			console.log('Event');
			//rABS = document.getElementsByName("userabs")[0].checked;
			// use_worker = document.getElementsByName("useworker")[0].checked;
			rABS = true;
			use_worker = true;
			var files = e.target.files;
			var f = files[0]; {
				var reader = new FileReader();
				var name = f.name;
				reader.onload = function (e) {
					if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
					var data = e.target.result;
					if (use_worker) {
						xw(data, process_wb);
					} else {
						var wb;
						if (rABS) {
							wb = X.read(data, {
								type: 'binary'
							});
						} else {
							var arr = fixdata(data);
							wb = X.read(btoa(arr), {
								type: 'base64'
							});
						}
						process_wb(wb);
					}
				};
				if (rABS) reader.readAsBinaryString(f);
				else reader.readAsArrayBuffer(f);
			}
		}

		function validateFileType(e) {
			$scope.DisableSubmit = true;
			$scope.xlsentries = [];
			// $scope.offermanagement.imagename = this.value.split(/[\/\\]/).pop();
			// console.log('fileInput',this.value);

			var _validVideoFileExtensions = [".xlsx", ".xls"];
			var sFileName = this.value;
			if (sFileName.length > 0) {
				var blnValid = false;
				for (var j = 0; j < _validVideoFileExtensions.length; j++) {
					var sCurExtension = _validVideoFileExtensions[j];
					if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
						blnValid = true;
						$scope.CorrectFormat = true;
						handleFile(e);
						break;
					}
				}

				if (!blnValid) {
					alert("Sorry, " + sFileName.split(/[\/\\]/).pop() + " is invalid, allowed extensions are: " + _validVideoFileExtensions.join(", "));
					xlf.value = null;
					//$scope.uploader.queue[0].remove();
					// console.log('$scope.uploader',$scope.uploader.queue[0]);
					$scope.CorrectFormat = false;
				}
			}

			/* if (item.file.type != 'video/mp4') {
			     alert('Accepts Only .mp4 files');
			     item.remove();
			 }*/
		};

		xlf.addEventListener('change', validateFileType, false);

		/* var link = document.getElementById('download');
		link.href = 'http://flipkart-client.herokuapp.com/xlssheets/xlszone.xlsx'; */

		/* $scope.downloadfile = function () {
		    $http({
		        method: 'GET',
		        url: '/xlssheets/xlszone.xlsx'
		    }).
		    success(function (data, status, headers, config) {
		        var anchor = angular.element('<a/>');
		        anchor.attr({
		            href: '/xlssheets/xlszone.xlsx',
		            target: '_blank',
		            download: '/xlssheets/xlszone.xlsx'
		        })[0].click();

		    }).
		    error(function (data, status, headers, config) {
		        // if there's an error you should see it here
		    });

		} */




		$scope.partners = Restangular.all('districts').getList().$object;
    
		$scope.Save = function () {
			$scope.count = 0;
			$scope.salesareadataModal = !$scope.salesareadataModal;
			$scope.item = $scope.entries.Sheet1;
			for (var i = 0; i < $scope.item.length; i++) {
				/*    $scope.item[i].salesManagerId = $window.sessionStorage.EmployeeId;
				    $scope.item[i].organizationId = $window.sessionStorage.organizationId;  */
				$scope.item[i].lastModifiedDate = new Date();
				$scope.item[i].lastModifiedBy = $window.sessionStorage.UserEmployeeId;
				$scope.item[i].deleteFlag = false;
				$scope.partners.post($scope.item[i]).then(function (resp) {
					// console.log('districts saved', resp);
					$scope.count = $scope.count + 1;
					if ($scope.count === $scope.item.length) {
						console.log('output', $scope.entries.Sheet1);
						//$route.reload();
						window.location = "/district-list";
					}
				}, function (response) {
					// console.log("Error with status code", response.status);
					$scope.count = $scope.count + 1;
					if ($scope.count === $scope.item.length) {
						console.log('output', $scope.entries.Sheet1);
						//$route.reload();
						window.location = "/district-list";
					}
				});
			}
		};


		$scope.Validate = function () {
			$scope.errortext = '';
            var re = /^-?[0-9]\d*(\.\d+)?$/; 
			if ($scope.xlsentries[0]['name'] && $scope.xlsentries[0]['countryId'] && $scope.xlsentries[0]['stateId']) {
				// dataList["state"] exists. do stuff.
				console.log('presence');
				$scope.hideValidate = true;
				$scope.sheetValidation = !$scope.sheetValidation;
				setTimeout(function () {
					$scope.sheetValidation = !$scope.sheetValidation;
				}, 750);
				for (var i = 0; i < $scope.xlsentries.length; i++) {
					if ($scope.xlsentries[i].name == '' || $scope.xlsentries[i].name == null) {
						$scope.errortext = $scope.errortext + 'Invalid Name at Column Number::' + JSON.stringify($scope.xlsentries[i].index + 2) + '\r\n';
					} else if ($scope.xlsentries[i].countryId == '' || $scope.xlsentries[i].countryId == null || !re.test($scope.xlsentries[i].countryId)) {
						console.log(i + ':::' + $scope.xlsentries[i].countryId.length);
						$scope.errortext = $scope.errortext + 'Invalid Country Id at Column Number::' + JSON.stringify($scope.xlsentries[i].index + 2) + '\r\n';
					} else if ($scope.xlsentries[i].stateId == '' || $scope.xlsentries[i].stateId == null || !re.test($scope.xlsentries[i].stateId)) {
						console.log(i + ':::' + $scope.xlsentries[i].stateId.length);
						$scope.errortext = $scope.errortext + 'Invalid State Id at Column Number::' + JSON.stringify($scope.xlsentries[i].index + 2) + '\r\n';
					}
				}

				var textFile = null,
					makeTextFile = function (text) {
						var data = new Blob([text], {
							type: 'text/plain'
						});

						// If we are replacing a previously generated file we need to
						// manually revoke the object URL to avoid memory leaks.
						if (textFile !== null) {
							window.URL.revokeObjectURL(textFile);
						}

						textFile = window.URL.createObjectURL(data);

						return textFile;
					};

				if ($scope.errortext != '') {
				//	alert('Some Of your data is not in proper format. Click on "Download Error Text" to download the error log.');
                    $scope.showValidation = !$scope.showValidation;
					var link = document.getElementById('downloadlink');
					link.href = makeTextFile($scope.errortext);
					link.style.display = 'block';
				} else {
					$scope.hideSubmit = false;
				}
			} else {
                $scope.xlsValidation = !$scope.xlsValidation;
				// dataList["state"] does not exist
				// console.log('absence');
				//alert('Your xls is not in specified format.');
			}

		}
	});
