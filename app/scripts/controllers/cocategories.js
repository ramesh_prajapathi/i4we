'use strict';

angular.module('secondarySalesApp')
  .controller('COCategoriesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams,$timeout,baseUrl, $route, $window) {
    /*********/

    $scope.showForm = function(){
      var visible = $location.path() === '/cocategories/create' || $location.path() === '/cocategories/' + $routeParams.id;
      return visible;
    };
    
    $scope.isCreateView = function(){
      if($scope.showForm()){
        var visible = $location.path() === '/cocategories/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function (){
        var visible = $location.path() === '/cocategories/create'|| $location.path() === '/cocategories/' + $routeParams.id;
        return visible;
      };

    
    $scope.hideSearchFilter = function (){
        var visible = $location.path() === '/cocategories/create'|| $location.path() === '/cocategories/' + $routeParams.id;
        return visible;
      };

    
    /*********/

  //  $scope.cocategories = Restangular.all('cocategories').getList().$object;
    
    if($routeParams.id){
      Restangular.one('cocategories', $routeParams.id).get().then(function(cocategory){
				$scope.original = cocategory;
				$scope.cocategory = Restangular.copy($scope.original);
      });
    }
    $scope.searchZone = $scope.name;
    
/************************************************************************** INDEX *******************************************/
       $scope.zn = Restangular.all('cocategories').getList().then(function (zn) {
        $scope.cocategories = zn;
        angular.forEach($scope.cocategories, function (member, index) {
            member.index = index + 1;
        });
    });
    
/*************************************************************************** SAVE *******************************************/
     $scope.validatestring = '';
        $scope.SaveZone = function () {
          
                    $scope.cocategories.post($scope.cocategory).then(function () {
                        console.log('Zone Saved');
                        window.location = '/cocategories';
                    });
        };
/*************************************************************************** UPDATE *******************************************/ 
     $scope.validatestring = '';
        $scope.UpdateZone = function () {
                document.getElementById('name').style.border = "";
          if ($scope.cocategory.name == '' || $scope.cocategory.name == null) {
                $scope.cocategory.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your cocategory name';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } 
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.cocategories.customPUT($scope.cocategory).then(function () {
                        console.log('Zone Saved');
                        window.location = '/cocategories';
                    });
                }


        };
/*************************************************************************** DELETE *******************************************/ 
   $scope.Delete = function (id) {
        if (confirm("Are you sure want to delete..!") == true) {
            Restangular.one('cocategories/' + id).remove($scope.cocategory).then(function () {
                $route.reload();
            });

        } else {

        }

    }
    
  });


