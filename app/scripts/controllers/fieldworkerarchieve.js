'use strict';

angular.module('secondarySalesApp')
	.controller('FWArchieveCtrl', function ($scope, Restangular, $route, $window) {

		$scope.auditlog = {
			description: 'Unarchived Member',
			modifiedbyroleid: $window.sessionStorage.roleId,
			modifiedby: $window.sessionStorage.UserEmployeeId,
			lastmodifiedtime: new Date(),
			entityroleid: 56,
			state: $window.sessionStorage.zoneId,
			district: $window.sessionStorage.salesAreaId,
			facility: $window.sessionStorage.coorgId,
			lastmodifiedby: $window.sessionStorage.UserEmployeeId
		};


		$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;



		if ($window.sessionStorage.roleId == 5) {
			Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
				$scope.comemberid = comember.id;
				console.log('$scope.comemberid',$scope.comemberid);
				console.log('$window.sessionStorage.UserEmployeeId',$window.sessionStorage.coorgId);
				$scope.auditlog.facilityId = comember.id;
				$scope.part = Restangular.all('beneficiaries?filter[where][facility]=' +$window.sessionStorage.coorgId + '&filter[where][deleteflag]=true').getList().then(function (part) {
					$scope.beneficiaries = part;
					$scope.modalInstanceLoad.close();
					angular.forEach($scope.beneficiaries, function (member, index) {
						member.index = index + 1;
					});
				});

			});
		} else {
			Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
				$scope.getsiteid = fw.id;
				Restangular.one('comembers', fw.facility).get().then(function (comember) {
					$scope.comemberid = comember.id;
					$scope.auditlog.facilityId = comember.id;

					Restangular.all('beneficiaries?filter[where][facility]=' +$window.sessionStorage.coorgId + '&filter[where][deleteflag]=true').getList().then(function (part) {
						$scope.beneficiaries = part;
						$scope.modalInstanceLoad.close();
						angular.forEach($scope.beneficiaries, function (member, index) {
							member.index = index + 1;
						});
					});
				});
			});
		}

		$scope.searchPart = $scope.firstName;
		/**** DELETE *******************************************/


		$scope.Delete = function (id) {
			$scope.auditlog.entityid = id;
			$scope.submitauditlogs.post($scope.auditlog).then(function (responseaudit) {
				console.log('responseaudit', responseaudit);
				$scope.item = [{
					deleteflag: false
            }]
				Restangular.one('beneficiaries/' + id).customPUT($scope.item[0]).then(function () {
					$route.reload();
				});
			});
		}
		
	/************************** Language *******************************/
		$scope.languages = Restangular.all('languages').getList().$object;
		$scope.multiLang = Restangular.one('multilanguages?filter[where][id]=' +$window.sessionStorage.language).get().then(function (langResponse) {
			console.log('langResponse', langResponse);

			$scope.memberlist = langResponse[0].memberlist;
			$scope.show = langResponse[0].show;
			$scope.entry = langResponse[0].entry;
			$scope.membername = langResponse[0].membername;

			$scope.membercode = langResponse[0].membercode;
			$scope.membermobileno = langResponse[0].membermobileno;
			$scope.action = langResponse[0].action;
			$scope.searchfor = langResponse[0].searchfor;

		});

	});
