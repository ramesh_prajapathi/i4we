'use strict';

angular.module('secondarySalesApp')
	.controller('MentorAssignCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
		/*********/

		$scope.showForm = function () {
			var visible = $location.path() === '/mentorassign/create' || $location.path() === '/mentorassign/' + $routeParams.id;
			return visible;
		};
		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/mentorassign/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/mentorassign/create' || $location.path() === '/mentorassign/' + $routeParams.id;
			return visible;
		};
		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/mentorassign/create' || $location.path() === '/mentorassign/' + $routeParams.id;
			return visible;
		};
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		}
		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		if ($window.sessionStorage.prviousLocation != "partials/mentorassign") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
			// $scope.pageSize = 25;
		}

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};
		/**************************************************************************************/
		$scope.userroles = Restangular.all('roles?filter={"where":{"id":{"inq":[4,17]}}}').getList().$object;
		$scope.usermentors = Restangular.all('mentors?filter={"where":{"role":{"inq":[17]}}}').getList().$object;
		$scope.userrios = Restangular.all('mentors?filter={"where":{"role":{"inq":[4]}}}').getList().$object;


		$scope.$watch('routelink.name', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == "") {
				return;
			} else if ($routeParams.id && (oldValue == null || oldValue == undefined)) {
				return;
			} else {
				Restangular.one('mentors', newValue).get().then(function (mentr) {
					$scope.mentorId = mentr.id;
					$scope.AssignedSites = mentr.facilityassigned;
					console.log('mentr.facilityassigned', mentr.facilityassigned);
					$scope.employees = Restangular.all('employees?filter[where][stateId]=' + mentr.state + '&filter[where][deleteflag]=false').getList().$object;
				});
			}
		});
		$scope.disableSave = true;
		$scope.agenttoutelinkModal = false;
		$scope.$watch('routelink.facilityassigned', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == "") {
				return;
			} else if ($routeParams.id && (oldValue == null || oldValue == undefined)) {
				return;
			} else {
				console.log('newValue', newValue);
				console.log('$scope.AssignedSites', $scope.AssignedSites);
				Restangular.one('employees', newValue).get().then(function (mentr) {
					$scope.facilityid = mentr.id;
					/*var a = $scope.facilityid;
					if ($scope.AssignedSites.indexOf(a) != -1) {
						alert("Facility Already Assigned");
						$scope.routelink.facilityassigned = '';
					}*/
				});
				$scope.part = Restangular.all('mentors?filter[where][role]=' + $scope.routelink.role + '&filter[where][facilityassigned][like]=%' + newValue + '%').getList().then(function (part2) {
					//console.log('part2', part2);
					if (part2.length == 0) {
						$scope.disableSave = false;
					} else {
						// $scope.errorMessage = 'Facility Already Assigned for this Role';
						//$scope.agenttoutelinkModal = !$scope.agenttoutelinkModal;
						$scope.disableSave = false;
						// $scope.routelink.facilityassigned = "";
					}
				}, function (error) {
					console.log("error", error);
				});

				/* $scope.part = Restangular.all('mentors?filter={"where":{"and":[{"role":{"inq":[' + $scope.routelink.role + ']}},{"facilityassigned":{"inq":[' + newValue + ']}}]}}').getList().then(function (part2) {
				     console.log('part2', part2);
				     if (part2.length == 0) {
				         $scope.disableSave = false;
				     } else {
				         $scope.errorMessage = 'Facility Already Assigned for this Role';
				         $scope.agenttoutelinkModal = !$scope.agenttoutelinkModal;
				         $scope.disableSave = true;
				     }
				 }, function (error) {
				     console.log("error", error);
				 });*/
			}
		});

		/*****************************************CREATE************************************/
		$scope.validatestring = '';
		$scope.Save = function () {
			//$scope.AssignedSites = null;
			if ($scope.user.roleId == '' || $scope.user.roleId == null) {
				$scope.validatestring = $scope.validatestring + 'Please Select Your Role';
			}
			/*else if ($scope.user.usrName == '' || $scope.user.usrName == null) {
							$scope.validatestring = $scope.validatestring + 'Please Select Your User';
						} else if ($scope.user.username == '' || $scope.user.username == null) {
							$scope.validatestring = $scope.validatestring + 'Please Enter User Name';
							document.getElementById('Username').style.borderColor = "#FF0000";
						} else if ($scope.user.language == '' || $scope.user.language == null) {
							$scope.validatestring = $scope.validatestring + 'Please Select Your Language';
						}*/
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				Restangular.one('employees', $scope.facilityid).get().then(function (fwsResponse) {
					if ($scope.AssignedSites == null) {
						$scope.AssignedSites = fwsResponse.id;
					} else {
						if ($scope.AssignedSites.split(',').indexOf(fwsResponse.id + "") == -1) {
							$scope.AssignedSites = $scope.AssignedSites + ',' + fwsResponse.id;
						} else {
							$scope.AssignedSites = $scope.AssignedSites;
						}
					}
					$scope.AssignedFieldSite = {
						facilityassigned: $scope.AssignedSites,
						id: $scope.mentorId
					};

					$scope.AssignedFlagEmp = {
						assignedflag: true

					};
					Restangular.one('mentors', $scope.mentorId).customPUT($scope.AssignedFieldSite).then(function (responsefield) {
						//console.log('responsefield', responsefield);
						Restangular.one('employees', $scope.facilityid).customPUT($scope.AssignedFlagEmp).then(function (responseemp) {
							//console.log('responseemp', responseemp);
							window.location = '/mentorassign';
						});
					})
				});
			};
		};

		$scope.modalTitle = 'Thank You';
		$scope.message = 'Mentor has been Assigned!';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

		/*********************************** iNDEX **************************************/

		$scope.getFacility = function (id) {
			return Restangular.all('employees?filter={"where":{"id":{"inq":[' + id + ']}}}').getList().$object;
		};

		$scope.empl = Restangular.all('employees?filter[where][deleteflag]=false').getList().then(function (emply) {
			$scope.getemployees = emply;
			$scope.zn = Restangular.all('mentors?filter={"where":{"and":[{"role":{"inq":[4,17]}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (zn) {
				$scope.mentors = zn;
				angular.forEach($scope.mentors, function (member, index) {
					member.index = index + 1;

					member.employees = $scope.getFacility(member.facilityassigned);
					for (var n = 0; n < $scope.getemployees.length; n++) {
						if (member.facilityassigned == $scope.getemployees[n].id) {
							member.Sitename = $scope.getemployees[n];
							break;
						}
					}
					$scope.TotalData = [];
					$scope.TotalData.push(member);
				});
			});
		});

		/*****************************************************************DELETE*************************/

		$scope.Delete = function (id, empid, index) {
			// console.log('fwid', id);
			// console.log('empindex', empid);
			//console.log('index', index);
			$scope.AssignedSites = null;
			Restangular.one('mentors', id).get().then(function (fldwrkr) {
				$scope.Assignedarray = fldwrkr.facilityassigned.split(',');
				$scope.Assignedarray.splice($scope.Assignedarray.indexOf(empid + ''), 1);
				console.log('$scope.Assignedarray After', $scope.Assignedarray);
				for (var i = 0; i < $scope.Assignedarray.length; i++) {
					if (i == 0) {
						$scope.AssignedSites = $scope.Assignedarray[i];
						console.log('$scope.AssignedSites2', $scope.AssignedSites);
					} else {
						$scope.AssignedSites = $scope.AssignedSites + ',' + $scope.Assignedarray[i];
						console.log('$scope.AssignedSites3', $scope.AssignedSites);
					}
				}

				$scope.AssignedFieldSite = [{
					facilityassigned: $scope.AssignedSites
    }];

				$scope.AssignedFlagEmp = [{
					assignedflag: false
    }];

				Restangular.one('mentors', id).customPUT($scope.AssignedFieldSite[0]).then(function (responsefield) {
					Restangular.one('employees', empid).customPUT($scope.AssignedFlagEmp[0]).then(function (responseemp) {
						console.log('responseemp', responseemp);
						$route.reload();
					});
				});
			});
		}

		/**********************************Index 5 ************************
				$scope.zn = Restangular.all('mentors?filter[where][deleteflag]=false').getList().then(function (zn) {
					$scope.mentors3 = zn;
					var myValues = new Array();
					angular.forEach($scope.mentors3, function (member, index) {
						console.log('member',member.id);
					
						$scope.zn = Restangular.one('mentors', member.id).get().then(function (zn) {
							$scope.mentors12 = zn;
							$scope.Assignedarray2 = zn.facilityassigned.split(',');
							for (var i = 0; i < $scope.Assignedarray2.length; i++) {
								if (i == 0) {
									$scope.AssignedSites2 = $scope.Assignedarray2[i];
									console.log('$scope.AssignedSites1', $scope.AssignedSites2);
									Restangular.one('employees', $scope.AssignedSites2).get().then(function (oneEmp) {
										console.log('oneEmp', oneEmp);
										Restangular.all('mentors?filter[where][facilityassigned]=', oneEmp.id).getList().then(function (oneMentor) {
											console.log('oneMentor.name',oneMentor.name);
											console.log('oneMentor',oneMentor);
										});
									});
									
								} else {
									$scope.AssignedSites2 = $scope.AssignedSites2 + ',' + $scope.Assignedarray2[i];
									console.log('$scope.AssignedSites2', $scope.AssignedSites2);
									Restangular.all('employees?filter={"where":{"id":{"inq":[' + $scope.AssignedSites2 + ']}}}').getList().then(function (moreEmp) {
										console.log('moreEmp', moreEmp);
										$scope.getmoreEmp = moreEmp;
										//console.log('moreEmp', moreEmp);
										
										angular.forEach($scope.getmoreEmp, function (member2, index) {
										console.log('member2',member2.id);
										Restangular.all('mentors?filter={"where":{"facilityassigned":{"inq":[' + member2.id + ']}}}').getList().then(function (moreMentore) {
											$scope.TotalMentor = moreMentore;
											console.log('moreMentore',moreMentore);
										});
										});
										
										
									});
								}
							}
						});
					});
				});
		*/

	});
