'use strict';

angular.module('secondarySalesApp')
	.controller('COCreateCtrl', function ($scope, Restangular, $filter, $timeout, $window) {
		$scope.isCreateView = true;
		$scope.fscodedisable = false;
		$scope.fsnamedisable = false;
		$scope.hfdflagDisabled = false;
		$scope.heading = 'Facility/CO Create';

		$scope.partner = {
			hdfflag: true,
			deleteflag: false,
			usercreated: false,
			//state: '',
			latitude: '',
			longitude: '',
			address: '',
			//district: '',
			//state: $window.sessionStorage.zoneId,
			//district: $window.sessionStorage.salesAreaId,
			lastmodifiedtime: new Date(),
			//facility: $window.sessionStorage.coorgId,
			lastmodifiedby: $window.sessionStorage.UserEmployeeId
		};

		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}
		$scope.$watch('partner.facility', function (newValue, oldValue) {
			console.log('newValue', newValue);
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$scope.employees1 = Restangular.one('employees', newValue).get().then(function (response) {
					$scope.getStateId = response.stateId;
					$scope.gettDistricId = response.district;
					console.log('$scope.getStateId', response.stateId);


					//$scope.salesAreas = Restangular.all('sales-areas?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.gettDistricId).getList().$object;

					$scope.salesAreas = Restangular.one('sales-areas?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.gettDistricId).get().then(function (responsesalesAreas) {
						$scope.districtname = responsesalesAreas[0].name;
						$scope.partner.district = responsesalesAreas[0].id;
						console.log('responseZone.name', $scope.partner.district);
					});

					$scope.zones = Restangular.one('zones?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.getStateId).get().then(function (responseZone) {
						$scope.statename = responseZone[0].name;
						$scope.partner.state = responseZone[0].id;
						console.log('responseZone.name', $scope.partner.state);
					});
				});

				$scope.zonalid = newValue;

			}
		});

		$scope.modalTitle = 'Thank You';
		$scope.message = 'Facility Manager/HDF has been Created!';
		/******************************** SAVE *******************************************/
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			if ($scope.partner.facility == '' || $scope.partner.facility == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Select Facility Code';
			} else if ($scope.partner.name == '' || $scope.partner.name == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;

				//$scope.submitpartners.post($scope.partner).then(function () {
				Restangular.all('comembers').post($scope.partner).then(function () {
					console.log('$scope.partner', $scope.partner);
					window.location = '/co-organisation';
				});
			}
		};

		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

		//$scope.employees = Restangular.all('employees?filter[where][groupId]=4').getList().$object;
		$scope.employees = Restangular.all('employees?filter[deleteflag]=false').getList().$object;
		//$scope.retailers = Restangular.all('retailers').getList().$object;
		//$scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
		//$scope.submitpartners = Restangular.all('comembers').getList().$object;

		//$scope.groups = Restangular.all('groups').getList().$object;
		//$scope.states = Restangular.all('states').getList().$object;
		//$scope.cocategories = Restangular.all('cocategories').getList().$object;
		//$scope.ims = Restangular.all('ims').getList().$object;
		//$scope.retailercategories = Restangular.all('retailercategories').getList().$object;

		/******************** Default Call ************************************************/
		//$scope.partner = {};
		$scope.groups = Restangular.one('groups', 8).get().then(function (group) {
			console.log(group);
			$scope.groupname = group.name;
			$scope.partner.groupId = group.id;
		});


		/***************************************************************** WATCH ***********************************
	
	$scope.$watch('partner.state', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue).getList().$object;
			}
		});

		$scope.$watch('partner.salesAreaId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$scope.distributionAreas = Restangular.all('distribution-areas?filter[where][salesAreaId]=' + newValue).getList().$object;
			}
		});




		$scope.$watch('partner.stateId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$scope.cities = Restangular.all('cities?filter[where][stateId]=' + newValue).getList().$object;
			}
		});

		$scope.$watch('partner.groupId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				if (newValue == 8 || newValue == 9) {
					$scope.partner.sequence = 1;
				} else {
					$scope.partner.sequence = null;
				}

				if (newValue == 10) {
					$scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;
				} else {
					$scope.partners = Restangular.all('partners').getList().$object;
				}
			}
		});

		
		$scope.$watch('partner.salesManagerId', function (newValue, oldValue) {
			console.log('newValue: ' + newValue);
			console.log('oldValue: ' + oldValue);
			$scope.distributionRoutes = Restangular.all('routelinkviews?filter[where][partnerid]=' + newValue + '&filter[where][flag]=null').getList().$object;
		});

	*/

		$scope.anotherHelplinenumber = 0;

		$scope.AnotherHelpline = function () {
			if ($scope.anotherHelplinenumber < 3) {
				$scope.anotherHelplinenumber++;
				console.log('$scope.anotherphonenumber', $scope.anotherhelplinenumber);
			} else {
				alert('Only Three Numbers Are Allowed');
			}
		}

		$scope.removeHelpline = function (index) {
				if ($scope.anotherHelplinenumber > 0) {
					if (index == 'one') {
						$scope.partner.helplineone = ''
					}
					if (index == 'two') {
						$scope.partner.helplinetwo = ''
					}
					if (index == 'three') {
						$scope.partner.helplinethree = ''
					}
					$scope.anotherHelplinenumber--;
				} else {
					alert('No Numbers To Remove');
				}

			}
			/*
			$scope.anotherphonenumber = 0;

			$scope.AnotherPhone = function () {
				if ($scope.anotherphonenumber < 3) {
					$scope.anotherphonenumber++;
					// console.log('$scope.anotherphonenumber',$scope.anotherphonenumber);
				} else {
					alert('Only Three Numbers Are Allowed');
				}

			}*/


		$scope.documenthide = true;
		$scope.$watch('partner.nooftis', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				if (newValue < 0 || newValue > 5) {
					alert('Invalid Entry');
					$scope.partner.nooftis = null;

					if (newValue == 0) {
						$scope.partner.tionerating = null;
						$scope.partner.titworating = null;
						$scope.partner.tithreerating = null;
						$scope.partner.tifourrating = null;
						$scope.partner.tifiverating = null;
					}

					if (newValue == 1) {
						$scope.partner.titworating = null;
						$scope.partner.tithreerating = null;
						$scope.partner.tifourrating = null;
						$scope.partner.tifiverating = null;
					}

					if (newValue == 2) {
						$scope.partner.tithreerating = null;
						$scope.partner.tifourrating = null;
						$scope.partner.tifiverating = null;
					}

					if (newValue == 3) {
						$scope.partner.tifourrating = null;
						$scope.partner.tifiverating = null;
					}

					if (newValue == 4) {
						$scope.partner.tifiverating = null;
					}

				}
			}
		});

		//Datepicker settings start

		$scope.today = function () {
			$scope.dt = $filter('date')(new Date(), 'y-MM-dd');

		};
		//$scope.today();

		$scope.showWeeks = true;
		$scope.toggleWeeks = function () {
			$scope.showWeeks = !$scope.showWeeks;
		};

		$scope.clear = function () {
			$scope.dt = null;
		};

		// Disable weekend selection
		/*  $scope.disabled = function (date, mode) {
		      return (mode === 'day' && (date.getDay() === 0));
		  };*/

		$scope.toggleMin = function () {
			$scope.minDate = ($scope.minDate) ? null : new Date();
		};
		$scope.toggleMin();

		$scope.open = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();

			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.opened = true;
		};

		$scope.open2 = function ($event, index) {
			$event.preventDefault();
			$event.stopPropagation();

			$timeout(function () {
				$('#datepicker' + index).focus();
			});
			$scope.opened2 = true;
		};

		$scope.open3 = function ($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened3 = true;
		};

		$scope.open4 = function ($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened4 = true;
		};
		$scope.open5 = function ($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened5 = true;
		};
		$scope.open6 = function ($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened6 = true;
		};
		$scope.open7 = function ($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened7 = true;
		};

		$scope.dateOptions = {
			'year-format': 'yy',
			'starting-day': 1
		};

		$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
		$scope.format = $scope.formats[0];
		//Datepicker settings end


		$scope.showMapModal = false;
		$scope.toggleMapModal = function () {
			$scope.mapcount = 0;

			///////////////////////////////////////////////////////MAP//////////////////////////



			var geocoder = new google.maps.Geocoder();

			function geocodePosition(pos) {
				geocoder.geocode({
					latLng: pos
				}, function (responses) {
					if (responses && responses.length > 0) {
						updateMarkerAddress(responses[0].formatted_address);
					} else {
						updateMarkerAddress('Cannot determine address at this location.');
					}
				});
			}

			function updateMarkerStatus(str) {
				document.getElementById('markerStatus').innerHTML = str;
			}

			function updateMarkerPosition(latLng) {
				$scope.partner.latitude = latLng.lat() + ',' + latLng.lng();
				$scope.partner.longitude = latLng.lng();

				document.getElementById('info').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');
			}

			function updateMarkerAddress(str) {
				document.getElementById('mapaddress').innerHTML = str;
			}
			var map;

			function initialize() {

				$scope.address = $scope.partner.address;
				// console.log('$scope.address', $scope.address);
				$scope.latitude = 21.0000;
				$scope.longitude = 78.0000;
				if ($scope.address.length > 0) {
					var addressgeocoder = new google.maps.Geocoder();
					addressgeocoder.geocode({
						'address': $scope.address
					}, function (results, status) {

						if (status == google.maps.GeocoderStatus.OK) {
							$scope.latitude = parseInt(results[0].geometry.location.lat());
							$scope.longitude = parseInt(results[0].geometry.location.lng());
							//console.log($scope.latitude, $scope.longitude);

							var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
							map = new google.maps.Map(document.getElementById('mapCanvas'), {
								zoom: 4,
								center: new google.maps.LatLng($scope.latitude, $scope.longitude),
								mapTypeId: google.maps.MapTypeId.ROADMAP
							});
							var marker = new google.maps.Marker({
								position: latLng,
								title: 'Point A',
								map: map,
								draggable: true
							});

							// Update current position info.
							updateMarkerPosition(latLng);
							geocodePosition(latLng);

							// Add dragging event listeners.
							google.maps.event.addListener(marker, 'dragstart', function () {
								updateMarkerAddress('Dragging...');
							});

							google.maps.event.addListener(marker, 'drag', function () {
								updateMarkerStatus('Dragging...');
								updateMarkerPosition(marker.getPosition());
							});

							google.maps.event.addListener(marker, 'dragend', function () {
								updateMarkerStatus('Drag ended');
								geocodePosition(marker.getPosition());
							});
						}
					});
				} else {
					$scope.latitude = 21.0000;
					$scope.longitude = 78.0000;

					var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
					map = new google.maps.Map(document.getElementById('mapCanvas'), {
						zoom: 4,
						center: new google.maps.LatLng($scope.latitude, $scope.longitude),
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					var marker = new google.maps.Marker({
						position: latLng,
						title: 'Point A',
						map: map,
						draggable: true
					});

					// Update current position info.
					updateMarkerPosition(latLng);
					geocodePosition(latLng);

					// Add dragging event listeners.
					google.maps.event.addListener(marker, 'dragstart', function () {
						updateMarkerAddress('Dragging...');
					});

					google.maps.event.addListener(marker, 'drag', function () {
						updateMarkerStatus('Dragging...');
						updateMarkerPosition(marker.getPosition());
					});

					google.maps.event.addListener(marker, 'dragend', function () {
						updateMarkerStatus('Drag ended');
						geocodePosition(marker.getPosition());
					});

				}


			}

			// Onload handler to fire off the app.
			//google.maps.event.addDomListener(window, 'load', initialize);
			initialize();

			window.setTimeout(function () {
				google.maps.event.trigger(map, 'resize');
				map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
				map.setZoom(4);
			}, 1000);


			$scope.SaveMap = function () {
				$scope.showMapModal = !$scope.showMapModal;
				console.log($scope.reportincident);
			};

			//console.log('fdfd');
			$scope.showMapModal = !$scope.showMapModal;
		};

		$scope.CancelMap = function () {
			if ($scope.mapcount == 0) {
				$scope.showMapModal = !$scope.showMapModal;
				$scope.mapcount++;
			}
		};


	});
