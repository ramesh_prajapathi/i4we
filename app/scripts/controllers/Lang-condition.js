'use strict';

angular.module('secondarySalesApp')
    .controller('LangConditionCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/


        $scope.HideCreateButton = true;
        $scope.langdisable = false;

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        $scope.$watch('condition.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                
                 Restangular.one('conditionLanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.condition = Restangular.copy($scope.original);
                    });
                Restangular.all('conditionLanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                        $scope.HideCreateButton = true;
                    } else {
                        //$scope.LangId = response[0].id;
                        // $scope.HideCreateButton = false;
                        //  $scope.langdisable = true;

                        //  $scope.lhs = response[0];
                        //  console.log('$scope.lhs', $scope.lhs);
                        
                         $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';


                    }
                });

            }
        });
    
         /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/LangScreenTreat-list';

        };
    
    
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/

        $scope.condition = {
            deleteFlag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId
        };

        $scope.Save = function () {
            Restangular.all('conditionLanguages').post($scope.condition).then(function (conResponse) {
                console.log('conResponse', conResponse);
                window.location = '/LangScreenTreat-list';
            });

        };

        $scope.Update = function () {
            Restangular.one('conditionLanguages', $routeParams.id).customPUT($scope.condition).then(function (conResponse) {
                console.log('conResponse', conResponse);
                window.location = '/LangScreenTreat-list';
            });
        };

        if ($routeParams.id) {
            $scope.HideCreateButton = false;
            $scope.langdisable = true;
            Restangular.one('conditionLanguages', $routeParams.id).get().then(function (condition) {
                $scope.original = condition;
                $scope.condition = Restangular.copy($scope.original);
            });
        }
    });