'use strict';

angular.module('secondarySalesApp')
    .controller('LangShgCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/
           
  
   
        $scope.isCreateView = true;
        $scope.langdisable = false;

        $scope.languages = Restangular.all('languages?filter[where][deleteFlag]=false').getList().$object;

        $scope.$watch('shg.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                
                 Restangular.one('shgLanguages?filter[where][language]=' + 1 + '&filter[where][deleteFlag]=false').get().then(function (memblanguage) {
                        console.log('memblanguage', memblanguage);
                        $scope.original = memblanguage[0];
                         delete  $scope.original['language'];
                         delete  $scope.original['id'];
                         $scope.original.language = newValue;
                        $scope.shg = Restangular.copy($scope.original);
                    });
                
                Restangular.all('shgLanguages?filter[where][language]=' + newValue + '&filter[where][deleteFlag]=false').getList().then(function (response) {
                    if (response.length == 0) {
                       $scope.isCreateView = true;
                    } else {
                       // $scope.LangId = response[0].id;
                       // $scope.isCreateView = false;
                      //  $scope.langdisable = true;

                     //  $scope.shg = response[0];
                     //  console.log('$scope.shg', $scope.shg);
                        /***********new changes*****/
                         $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';
                       

                    }
                });
                
            }
        });
    
    /***********new changes*****/
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/LangShgList';

        };
    
    
    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

    /***********new changes*****/
    
    
    

        $scope.shg = {
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.UserEmployeeId,
            deleteFlag: false,
            formationDate: new Date()
        };



        // $scope.validatestring = '';
        $scope.Save = function () {

            /* document.getElementById('shglist').style.border = "";
            document.getElementById('shgcreate').style.border = "";
            document.getElementById('shgedit').style.border = "";
            document.getElementById('group').style.border = "";
            document.getElementById('hf').style.border = "";
            document.getElementById('formation').style.border = "";
            document.getElementById('search').style.border = "";
            document.getElementById('meeting').style.border = "";
            document.getElementById('name').style.border = "";
            document.getElementById('newhh').style.border = "";
            document.getElementById('age').style.border = "";
            document.getElementById('IId').style.border = "";
            document.getElementById('action').style.border = "";
            document.getElementById('phone').style.border = "";
            document.getElementById('amount').style.border = "";
            document.getElementById('assobank').style.border = "";
            document.getElementById('interestrate').style.border = "";
            document.getElementById('accno').style.border = "";
			
			if ($scope.shgLanguages.language == '' || $scope.shg.language == null) {
				$scope.validatestring = $scope.validatestring + 'Please Select Language';
				
				
			} else if ($scope.shg.shgList == '' || $scope.shg.shgList == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter SHG Registraion List';
				document.getElementById('state').style.borderColor = "#FF0000";
				
			}*/

            /*if ($scope.validatestring != '') {
            	$scope.toggleValidation();
            	$scope.validatestring1 = $scope.validatestring;
            	$scope.validatestring = '';
            } else {*/

            Restangular.all('shgLanguages').post($scope.shg).then(function (shgResponse) {
                console.log('shgResponse', shgResponse);
                window.location = '/LangShgList';
            });
            //}
        };

        $scope.Update = function () {
            Restangular.one('shgLanguages', $routeParams.id).customPUT($scope.shg).then(function (shgResponse) {
                console.log('shgResponse', shgResponse);
                window.location = '/LangShgList';
            });
        };
    
      if ($routeParams.id) {
          
             $scope.isCreateView = false;
           $scope.langdisable = true;
            Restangular.one('shgLanguages', $routeParams.id).get().then(function (shg) {
                $scope.original = shg;
                $scope.shg = Restangular.copy($scope.original);
            });
      }

    })



  .directive('checkmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-danger modal-sm">' + '<div class="modal-content">' + '<div class="modal-header">' + '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="modal-body" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });

