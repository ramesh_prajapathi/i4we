'use strict';

angular.module('secondarySalesApp')
	.controller('followupgroupsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/followupgroups/create' || $location.path() === '/followupgroups/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/followupgroups/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/followupgroups/create' || $location.path() === '/followupgroups/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/followupgroups/create' || $location.path() === '/followupgroups/' + $routeParams.id;
			return visible;
		};

		/*********************************** Pagination *******************************************/

		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;

		}

		if ($window.sessionStorage.prviousLocation != "partials/followupgroup") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};



		/*********/

		$scope.submitfollowupgroups = Restangular.all('followupgroups').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'Group Meeting Follow Up has been Updated!';
			Restangular.one('followupgroups', $routeParams.id).get().then(function (followupgroup) {
				$scope.original = followupgroup;
				$scope.followupgroup = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Group Meeting Follow Up has been Created!';
		}
		$scope.searchfollowupgroup = $scope.name;

		/********************************************************* INDEX *******************************************/
		$scope.zn = Restangular.all('followupgroups?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.followupgroups = zn;
			angular.forEach($scope.followupgroups, function (member, index) {
				member.index = index + 1;
			});
		});

		/************************************ SAVE *******************************************/
		$scope.followupgroup = {
			name: '',
			deleteflag: false
		};
		$scope.validatestring = '';
		$scope.Savefollowupgroup = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.followupgroup.name == '' || $scope.followupgroup.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.followupgroup.hnname == '' || $scope.followupgroup.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.followupgroup.knname == '' || $scope.followupgroup.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.followupgroup.taname == '' || $scope.followupgroup.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.followupgroup.tename == '' || $scope.followupgroup.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.followupgroup.mrname == '' || $scope.followupgroup.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitfollowupgroups.post($scope.followupgroup).then(function () {
					console.log('followupgroup Saved');
					window.location = '/followupgroups';
				});
			};
		};
		/********************************* UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Updatefollowupgroup = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.followupgroup.name == '' || $scope.followupgroup.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.followupgroup.hnname == '' || $scope.followupgroup.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.followupgroup.knname == '' || $scope.followupgroup.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.followupgroup.taname == '' || $scope.followupgroup.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.followupgroup.tename == '' || $scope.followupgroup.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.followupgroup.mrname == '' || $scope.followupgroup.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Name in Marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitfollowupgroups.customPUT($scope.followupgroup).then(function () {
					console.log('followupgroup Saved');
					window.location = '/followupgroups';
				});
			};
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/********************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('followupgroups/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

	});
