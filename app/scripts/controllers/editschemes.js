'use strict';

angular.module('secondarySalesApp')
    .controller('EditSchemeCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $timeout) {

        $scope.UserLanguage = $window.sessionStorage.language;

        $scope.SchemeLanguage = {};

        Restangular.one('schemelanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.SchemeLanguage = langResponse[0];
            $scope.AFSHeading = $scope.SchemeLanguage.schemeEdit;
            // $scope.modalTitle = langResponse[0].thankYou;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        $scope.HideCreateButton = true;
        /*******************************************************************************************************************/


        $scope.states = Restangular.all('zones').getList().$object;
        $scope.genders = Restangular.all('genders?filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.educations = Restangular.all('educations?filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.submitschememasters = Restangular.all('schememasters').getList().$object;
        //$scope.beneficiaries = Restangular.all('members').getList().$object;

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.hideAssigned = true;

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.schememaster.associatedHF = $window.sessionStorage.userId;

            });
        } else {

            $scope.hideAssigned = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.schememaster.associatedHF = $scope.schememaster.associatedHF;
            });
        }

        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.memberFilterCall = 'members?filter[where][deleteFlag]=false' + '&filter[where][associatedHF]=' + $window.sessionStorage.userId;
        } else {
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        }
        Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
            $scope.beneficiaries = mems;

            angular.forEach($scope.beneficiaries, function (member, index) {
                member.index = index;
            });
        });
        //
        //        $scope.schemestages = Restangular.all('schemestages?filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;
        $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.responcedreceived = Restangular.all('responsereceived?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
        $scope.printschemes = Restangular.all('schemes?filter[where][deleteFlag]=false&filter[where][schemeState]=' + $window.sessionStorage.stateId).getList().$object;

        $scope.auditlog = {
            action: 'Update',
            module: 'Apply for Scheme',
            owner: $window.sessionStorage.userId,
            datetime: new Date(),
            details: 'Apply for Scheme Updated',
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0]
        };

        $scope.$watch('schememaster.memberId', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                Restangular.one('members/findOne?filter[where][id]=' + newValue).get().then(function (mem) {
                    $scope.selectedMember = mem;
                });
            }
        });

        // $scope.modalschememasters = Restangular.all('schemes?filter[where][deleteflag]=null').getList().$object;
        $scope.submittodos = Restangular.all('todos').getList().$object;

        $scope.sm = Restangular.all('schemes?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][schemeState]=' + $window.sessionStorage.stateId).getList().then(function (scheme) {
            $scope.printschemes = scheme;
            angular.forEach($scope.printschemes, function (member, index) {
                member.index = index;
                member.enabled = 'no';
            });
        });


        $scope.getGender = function (genderId) {
            return Restangular.one('genders', genderId).get().$object;
        };

        if ($routeParams.id) {

            //            Restangular.all('schemestages?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (stges) {
            //                $scope.schemestages = stges;
            Restangular.one('schememasters', $routeParams.id).get().then(function (schememaster) {
                $scope.original = schememaster;
                $scope.schememaster = Restangular.copy($scope.original);

                if (schememaster.stage == 4) {
                    $scope.reponserec = false;
                }

                if (schememaster.stage == 5) {
                    $scope.delaydis = false;
                }

                $scope.schemstage = Restangular.all('schemestages?filter[where][deleteFlag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (schemstage) {
                    $scope.schemestages = schemstage;
                    angular.forEach($scope.schemestages, function (member, index) {
                        member.index = index;
                        if (schememaster.stage == member.id || schememaster.stage == member.parentId) {
                            $scope.stageLevel = member.level;
                        }
                    });
                    if (schememaster.stage == 4 && schememaster.responserecieve === '2') {
                        $scope.reponserec = false;
                        $scope.rejectdis = false;
                        angular.forEach($scope.schemestages, function (member, index) {
                            member.index = index;
                            if (member.id == 4 || member.parentId == 4 || member.id == 8 || member.parentId == 8) {
                                member.enabled = false;
                            } else {
                                member.enabled = true
                            }
                        });
                    } else {
                        angular.forEach($scope.schemestages, function (member, index) {
                            member.index = index;
                            if ($scope.stageLevel < member.level) {
                                member.enabled = false;
                            } else if ((member.id == schememaster.stage || member.parentId == schememaster.stage) && $scope.stageLevel == member.level) {
                                member.enabled = false;
                            } else {
                                member.enabled = true
                            }
                        });
                    }
                });
            });
            //            });
        }

        $scope.DeleteFlag = function (id) {
            if (confirm("Are you sure want to delete..!") == true) {
                Restangular.one('schemes/' + id).remove($scope.schememaster).then(function () {
                    $route.reload();
                });

            } else {

            }

        }
        /******************************************************* Save ************************/
        $scope.validatestring = '';

        $scope.Update = function () {
            if ($scope.schememaster.memberId == '' || $scope.schememaster.memberId == null) {
                $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectMember; //'Please Select a Member';

            } else if ($scope.schememaster.schemeId == '' || $scope.schememaster.schemeId == null) {
                $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectScheme;

            } else if ($scope.schememaster.stage == '' || $scope.schememaster.stage == null) {
                $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectStage;

            } else if ($scope.schememaster.datetime == '' || $scope.schememaster.datetime == null) {
                $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectFollowUp;

            } else if ($scope.schememaster.stage == 4) {
                if ($scope.schememaster.responserecieve == '' || $scope.schememaster.responserecieve == null) {
                    $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectResponseReceived;
                } else if ($scope.schememaster.responserecieve == 2) {
                    if ($scope.schememaster.rejection == '' || $scope.schememaster.rejection == null) {
                        $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectReasonForRejection;
                    }
                }

            } else if ($scope.schememaster.stage == 5) {
                if ($scope.schememaster.delay == '' || $scope.schememaster.delay == null) {
                    $scope.validatestring = $scope.validatestring + $scope.SchemeLanguage.selectReasonForDelay;
                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.schememaster.lastModifiedDate = new Date();
                $scope.schememaster.lastModifiedBy = $window.sessionStorage.userId;
                $scope.schememaster.lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.schememaster.associatedHF = $scope.selectedMember.associatedHF;

                $scope.toggleLoading();
                $scope.message = $scope.SchemeLanguage.thankYouUpdated;

                $scope.submitschememasters.customPUT($scope.schememaster).then(function (resp) {

                    $scope.auditlog.rowId = resp.id;
                    $scope.auditlog.details = $scope.selectedMember.individualId;
                    $scope.auditlog.memberId = $scope.selectedMember.id;
                    $scope.auditlog.conditionId = resp.schemeId;
                    $scope.auditlog.associatedHF = $scope.selectedMember.associatedHF;

                    Restangular.all('audittrials').post($scope.auditlog).then(function (respAudit) {
                        $scope.modalInstanceLoad.close();

                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        console.log('reloading...');

                        setInterval(function () {
                            window.location = '/applyforschemes';
                        }, 1500);
                    });
                });
            }

        };

        // console.log('$window.sessionStorage.UserEmployeeId,', $window.sessionStorage.UserEmployeeId);

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start//////

        $scope.today = function () {};
        $scope.today();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmin = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.openstart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };
        $scope.openstart1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];

        //Datepicker settings end////

        $scope.datetimehide = false;
        $scope.reponserec = true;
        $scope.delaydis = true;
        $scope.collectedrequired = true;

        $scope.$watch('schememaster.stage', function (newValue, oldValue) {
            console.log('schememaster.stage', newValue);
            //console.log('$scope.loanapplied', $scope.loanapplied);
            var fifteendays = new Date();
            fifteendays.setDate(fifteendays.getDate() + 15);
            var sevendays = new Date();

            sevendays.setDate(sevendays.getDate() + 7);
            if (newValue === oldValue) {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                $scope.schememaster.datetime = $scope.schememaster.datetime;
            } else if (newValue === '4') {
                $scope.reponserec = false;
                $scope.delaydis = true;
                $scope.datetimehide = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.purposeofloanhide = true;
                $scope.schememaster.purposeofloan = '';
                $scope.schememaster.amount = '';
            } else if (newValue === '5') {
                $scope.delaydis = false;
                $scope.reponserec = true;
                $scope.datetimehide = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.datetime = fifteendays;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                $scope.schememaster.purposeofloan = '';
                $scope.schememaster.amount = '';
            } else if (newValue === '2') {
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.datetimehide = true;
                $scope.collectedrequired = false;
                $scope.rejectdis = true;
                $scope.schememaster.datetime = fifteendays;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                $scope.schememaster.purposeofloan = '';
                $scope.schememaster.amount = '';
            } else if (newValue === '3') {
                $scope.schememaster.datetime = fifteendays;
                $scope.datetimehide = true;
                $scope.collectedrequired = true;
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                $scope.schememaster.purposeofloan = '';
                $scope.schememaster.amount = '';
            } else if (newValue === '7') {
                var sixmonth = new Date();
                sixmonth.setDate(sixmonth.getDate() + 180);
                $scope.schememaster.datetime = sixmonth;
                $scope.datetimehide = false;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.delaydis = true;
                $scope.reponserec = true;
            } else if (newValue === '1') {
                $scope.schememaster.datetime = fifteendays;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                $scope.schememaster.purposeofloan = '';
                $scope.schememaster.amount = '';
            } else if (newValue === '8') {
                $scope.schememaster.datetime = sevendays;
                $scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                $scope.schememaster.purposeofloan = '';
                $scope.schememaster.amount = '';
            } else if (newValue === '6') {
                $scope.datetimehide = false;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.purposeofloanhide = true;
                $scope.schememaster.purposeofloan = '';
                $scope.schememaster.amount = '';
            } else if (newValue === '9') {
                //$scope.datetimehide = true;
                $scope.delaydis = true;
                $scope.reponserec = true;
                $scope.collectedrequired = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.datetime = new Date();
                $scope.purposeofloanhide = true;
                $scope.schememaster.purposeofloan = '';
                $scope.schememaster.amount = '';
            } else {
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.purposeofloanhide = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.delay = null;
                //$scope.schememaster.datetime = '';
                $scope.datetimehide = true;
                $scope.rejectdis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.purposeofloan = '';
                $scope.schememaster.amount = '';
            }
        });

        $scope.rejectdis = true;

        $scope.$watch('schememaster.responserecieve', function (newValue, oldValue) {
            console.log('responserecieve', newValue);
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            if (newValue === oldValue || newValue === null) {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                $scope.schememaster.datetime = $scope.schememaster.datetime;
            } else if (newValue === '1') {
                var sixmonth = new Date();
                sixmonth.setDate(sixmonth.getDate() + 180);
                $scope.schememaster.datetime = sixmonth;
                $scope.rejectdis = true;
            } else if (newValue === '2') {
                $scope.rejectdis = false;
                var fifteendays = new Date();
                fifteendays.setDate(fifteendays.getDate() + 15);
                var sevendays = new Date();
                $scope.schememaster.datetime = fifteendays;
                $scope.datetimehide = true;
            } else {
                $scope.schememaster.datetime = sevendays;
                $scope.rejectdis = true;
                $scope.schememaster.rejection = null;
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.schememaster.responserecieve = null;
                $scope.schememaster.delay = null;
                //$scope.schememaster.datetime = '';
                $scope.datetimehide = false;
            }
        });

        /************************************************ PLHIV Modal *****************************************/
        // $scope.schememaster.attendees = [];

        $scope.$watch('schememaster.attendees', function (newValue, oldValue) {
            // console.log('watch.attendees', newValue);

            if (newValue === oldValue || newValue == undefined) {
                return;
            } else {
                if (newValue.length > oldValue.length) {
                    // something was added
                    var Array1 = newValue;
                    var Array2 = oldValue;

                    for (var i = 0; i < Array2.length; i++) {
                        var arrlen = Array1.length;
                        for (var j = 0; j < arrlen; j++) {
                            if (Array2[i] == Array1[j]) {
                                Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));

                            }
                        }
                    }
                    $scope.printschemes[Array1[0]].enabled = true;

                    // do stuff
                } else if (newValue.length < oldValue.length) {
                    // something was removed
                    var Array1 = oldValue;
                    var Array2 = newValue;

                    for (var i = 0; i < Array2.length; i++) {
                        var arrlen = Array1.length;
                        for (var j = 0; j < arrlen; j++) {
                            if (Array2[i] == Array1[j]) {
                                Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                            }
                        }
                    }
                    $scope.printschemes[Array1[0]].enabled = false;

                }
            }
        });


        $scope.showschemesModal = false;
        $scope.toggleschemesModal = function () {
            $scope.SaveScheme = function () {
                $scope.newArray = [];
                $scope.schememaster.attendees = [];
                for (var i = 0; i < $scope.printschemes.length; i++) {
                    if ($scope.printschemes[i].enabled == true) {
                        $scope.newArray.push($scope.printschemes[i].index);
                    }
                }
                $scope.schememaster.attendees = $scope.newArray
                console.log('$scope.groupmeeting.attendees', $scope.schememaster.attendees.id);
                $scope.showschemesModal = !$scope.showschemesModal;
            };

            $scope.showschemesModal = !$scope.showschemesModal;
        };


        $scope.CancelScheme = function () {
            $scope.showschemesModal = !$scope.showschemesModal;
        };

        /************************************************ Search On Modal *************************/

        $scope.schemename = $scope.name;
        $scope.$watch('schememaster.agegrp', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.sm = Restangular.all('schemes?filter[where][deleteFlag]=false' + '&filter[where][agegroup]=' + newValue + '&filter[where][schemeState]=' + $window.sessionStorage.stateId).getList().then(function (scheme) {
                    $scope.schemes = scheme;

                    angular.forEach($scope.schemes, function (member, index) {
                        member.index = index;
                        member.enabled = false;
                    });
                });
            }
        });

        $scope.$watch('schememaster.gender', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.sm = Restangular.all('schemes?filter[where][deleteFlag]=false' + '&filter[where][gender]=' + newValue + '&filter[where][schemeState]=' + $window.sessionStorage.stateId).getList().then(function (scheme) {
                    $scope.schemes = scheme;

                    angular.forEach($scope.schemes, function (member, index) {
                        member.index = index;
                        member.enabled = false;
                    });
                });
            }
        });

        $scope.$watch('schememaster.stateid', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.sm = Restangular.all('schemes?filter[where][deleteFlag]=false' + '&filter[where][state]=' + newValue + '&filter[where][schemeState]=' + $window.sessionStorage.stateId).getList().then(function (scheme) {
                    $scope.schemes = scheme;

                    angular.forEach($scope.schemes, function (member, index) {
                        member.index = index;
                        member.enabled = false;
                    });
                });
            }
        });



    });
