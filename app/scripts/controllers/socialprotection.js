'use strict';

angular.module('secondarySalesApp')
    .controller('SocialProtectionCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/



        /*********/

        $scope.partners = Restangular.all('partners?filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][salesAreaId]=' + $window.sessionStorage.salesAreaId + '&filter[where][groupId]=10&filter[where][coorgId]=' + $window.sessionStorage.coorgId).getList().$object;
    
    $scope.schemes = Restangular.all('schemes').getList().$object;
    $scope.applicationstages = Restangular.all('applicationstages').getList().$object;

        $scope.socialprotection = {
            "pan": null,
            "bpl": null,
            "aadhar": null,
            "voterid": null,
            "scheme1": null,
            "scheme2": null,
            "scheme3": null,
            "zoneId": $window.sessionStorage.zoneId,
            "salesAreaId": $window.sessionStorage.salesAreaId,
            "coorgId": $window.sessionStorage.coorgId,
            "partnerId": null
        };

        if ($routeParams.id) {
            Restangular.one('socialprotections', $routeParams.id).get().then(function (socialprotection) {
                $scope.original = socialprotection;
                $scope.socialprotection = Restangular.copy($scope.original);
            });
        }
        $scope.searchsocialprotection = $scope.name;

        /**************************************************** INDEX *******************************************/
        $scope.zn = Restangular.all('socialprotections').getList().then(function (zn) {
            $scope.socialprotections = zn;
            angular.forEach($scope.socialprotections, function (member, index) {
                member.index = index + 1;
            });
        });

        /************************************************ SAVE *******************************************/
        $scope.validatestring = '';
        $scope.SaveSocialProtection = function () {

            $scope.socialprotections.post($scope.socialprotection).then(function () {
                console.log('socialprotection Saved');
                window.location = '/socialprotection';
            });
        };
        /******************************** UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.Updatesocialprotection = function () {
            document.getElementById('name').style.border = "";
            if ($scope.socialprotection.name == '' || $scope.socialprotection.name == null) {
                $scope.socialprotection.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your socialprotection name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.socialprotections.customPUT($scope.socialprotection).then(function () {
                    console.log('socialprotection Saved');
                    window.location = '/socialprotection';
                });
            }


        };
        /************************************************************* DELETE *******************************************/
        $scope.Delete = function (id) {
            if (confirm("Are you sure want to delete..!") == true) {
                Restangular.one('socialprotections/' + id).remove($scope.socialprotection).then(function () {
                    $route.reload();
                });

            } else {

            }

        }

    });