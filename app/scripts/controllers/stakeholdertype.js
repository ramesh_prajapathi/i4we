'use strict';

angular.module('secondarySalesApp')
	.controller('stakeholdertypesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/stakeholdertypes/create' || $location.path() === '/stakeholdertypes/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/stakeholdertypes/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/stakeholdertypes/create' || $location.path() === '/stakeholdertypes/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/stakeholdertypes/create' || $location.path() === '/stakeholdertypes/' + $routeParams.id;
			return visible;
		};

		/*********************************** Pagination *******************************************/

		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
		}

		if ($window.sessionStorage.prviousLocation != "partials/stakeholdertype") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}
	
		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};


		
		/*********/

		$scope.submitstakeholdertypes = Restangular.all('stakeholdertypes').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'Stakeholder type has been Updated!';
			Restangular.one('stakeholdertypes', $routeParams.id).get().then(function (stakeholdertype) {
				$scope.original = stakeholdertype;
				$scope.stakeholdertype = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Stakeholder type has been Created!';
		}
		$scope.searchstakeholdertype = $scope.name;

		/***************************** INDEX *******************************************/
		$scope.zn = Restangular.all('stakeholdertypes?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.stakeholdertypes = zn;
			angular.forEach($scope.stakeholdertypes, function (member, index) {
				member.index = index + 1;
			});
		});

		$scope.stakeholdertype = {
			name: '',
			deleteflag: false
		};
		/************************************ SAVE *******************************************/
		$scope.validatestring = '';
		$scope.Savestakeholdertype = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.stakeholdertype.name == '' || $scope.stakeholdertype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.stakeholdertype.hnname == '' || $scope.stakeholdertype.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.stakeholdertype.knname == '' || $scope.stakeholdertype.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.stakeholdertype.taname == '' || $scope.stakeholdertype.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.stakeholdertype.tename == '' || $scope.stakeholdertype.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.stakeholdertype.mrname == '' || $scope.stakeholdertype.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.submitstakeholdertypes.post($scope.stakeholdertype).then(function () {
					console.log('stakeholdertype Saved');
					window.location = '/stakeholdertypes';
				});
			}
		};
		/********************************* UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Updatestakeholdertype = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.stakeholdertype.name == '' || $scope.stakeholdertype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.stakeholdertype.hnname == '' || $scope.stakeholdertype.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.stakeholdertype.knname == '' || $scope.stakeholdertype.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.stakeholdertype.taname == '' || $scope.stakeholdertype.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.stakeholdertype.tename == '' || $scope.stakeholdertype.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.stakeholdertype.mrname == '' || $scope.stakeholdertype.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter stakeholder type name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.submitstakeholdertypes.customPUT($scope.stakeholdertype).then(function () {
					console.log('stakeholdertype Saved');
					window.location = '/stakeholdertypes';
				});
			}
		};
	
		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/********************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('stakeholdertype/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

	});
