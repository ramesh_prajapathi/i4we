'use strict';

angular.module('secondarySalesApp')
    .controller('SchemeMastersEditCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $modal) {

        $scope.DisableLang = true;
        $scope.disabledState = true;

        $scope.showForm = function () {
            var visible = $location.path() === '/scheme/create' || $location.path() === '/scheme/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/scheme/create';
                return visible;
            }
        };

        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/scheme/create' || $location.path() === '/scheme/edit/' + $routeParams.id;
            return visible;
        };

        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/scheme/create' || $location.path() === '/scheme/edit/' + $routeParams.id;
            return visible;
        };

        /***********************************************************************************/
        $scope.ShowAllDetails = false;
        $scope.ShowAllDetailsDiv = false;

        /*.................................... INDEX ................................................*/
        $scope.st = Restangular.all('states?filter[where][deleteFlag]=false').getList().then(function (znRes) {
            $scope.states = znRes;
            $scope.stateId = $window.sessionStorage.myRoute1;
        });

        $scope.schememaster = {};

        $scope.showenglishLang = false;
        $scope.disabledFields = false;

        Restangular.all('schemes?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (schme) {
            $scope.englishschemes = schme;
        });

        $scope.$watch('schememaster.language', function (newValue, oldValue) {

            Restangular.all('genders?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (gdr) {
                $scope.genders = gdr;
                $scope.schememaster.gender = $scope.schememaster.gender;
            });

            Restangular.all('educations?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (edu) {
                $scope.educations = edu;
                $scope.schememaster.educationstatus = $scope.schememaster.educationstatus;
            });

            Restangular.all('schemecategories?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (schm) {
                $scope.schemecategories = schm;
                $scope.schememaster.category = $scope.schememaster.category;
            });

            Restangular.all('agegroups?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (age) {
                $scope.agegroups = age;
                $scope.schememaster.agegroup = $scope.schememaster.agegroup;
            });

            $scope.healthstatuses = Restangular.all('healthstatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (hlth) {
                $scope.healthstatuses = hlth;
                $scope.schememaster.healthstatus = $scope.schememaster.healthstatus;
            });

            Restangular.all('minoritystatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (minor) {
                $scope.minoritystatuses = minor;
                $scope.schememaster.minoritystatus = $scope.schememaster.minoritystatus;
            });

            Restangular.all('socialstatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (social) {
                $scope.socialstatuses = social;
                $scope.schememaster.socialstatus = $scope.schememaster.socialstatus;
            });

            Restangular.all('occupationstatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (occ) {
                $scope.occupationstatuses = occ;
                $scope.schememaster.occupationstatus = $scope.schememaster.occupationstatus;
            });

            Restangular.all('incomestatuses?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (income) {
                $scope.incomestatuses = income;
                $scope.schememaster.incomestatus = $scope.schememaster.incomestatus;
            });

            Restangular.all('locationtypes?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (loc) {
                $scope.locationtypes = loc;
                $scope.schememaster.locationtype = $scope.schememaster.locationtype;
            });

            if (newValue == '' || newValue == null || newValue == oldValue) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = true;
                    $scope.disabledFields = true;
                } else {
                    $scope.showenglishLang = false;
                    $scope.disabledFields = false;
                }
            }
        });

        $scope.$watch('schememaster.parentId', function (newValue, oldValue) {
            if (newValue === '' || newValue === null || newValue === oldValue) {
                return;
            } else {
                Restangular.one('schemes', newValue).get().then(function (sch) {
                    // console.log();
                    // $scope.schememaster.schemeState = sch.schemeState;
                    $scope.schememaster.category = sch.category.split(",");
                    $scope.schememaster.objectives = sch.objectives;
                    $scope.schememaster.department = sch.department;
                    $scope.schememaster.agegroup = sch.agegroup.split(",");
                    $scope.schememaster.gender = sch.gender;
                    $scope.schememaster.educationstatus = sch.educationstatus;
                    $scope.schememaster.healthstatus = sch.healthstatus;
                    $scope.schememaster.minoritystatus = sch.minoritystatus;
                    $scope.schememaster.socialstatus = sch.socialstatus;
                    $scope.schememaster.occupationstatus = sch.occupationstatus;
                    $scope.schememaster.locationtype = sch.locationtype;
                    $scope.schememaster.incomestatus = sch.incomestatus;
                    $scope.schememaster.minimumincome = sch.minimumincome;
                    $scope.schememaster.maximumincome = sch.maximumincome;
                    $scope.schememaster.anyothercriteria = sch.anyothercriteria;
                    // console.log('$scope.schememaster', $scope.schememaster);
                });
            }
        });

        /******************************* INDEX *****************************************/

        $scope.getGender = function (genderId) {
            return Restangular.one('genders', genderId).get().$object;
        };
        $scope.showDetails = function () {
            $scope.ShowAllDetails = !$scope.ShowAllDetails;
        };

        $scope.rowArray = [];

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablang) {
            $scope.tablanguages = tablang;
            $scope.tablanguages.splice(0, 1);
            $scope.currValue = 0;

            angular.forEach($scope.tablanguages, function (member, index) {
                member.index = index;

                if (!$routeParams.id) {
                    $scope.rowArray.push({
                        language: member.id,
                        name: ''
                    });
                }

                if (member.index == 0) {
                    member.active = true;
                    member.visible = true;
                } else {
                    member.active = false;
                    member.visible = false;
                }
            });
        });

        $scope.tabClick = function (row) {
            $scope.tablanguages[row.index].visible = true;
            if (row.index != 0) {
                $scope.tablanguages[0].visible = false;
            }
            $scope.currValue = row.index;
        };


        if ($routeParams.id) {
            $scope.ShowAllDetailsDiv = true;
            $scope.message = 'Scheme has been Updated!';
            Restangular.one('schemes', $routeParams.id).get().then(function (scheme) {
                $scope.original = scheme;

                $scope.schememaster = Restangular.copy($scope.original);
                $scope.schememaster.category = scheme.category.split(",");
                $scope.schememaster.agegroup = scheme.agegroup.split(",");
                //    console.log($scope.schememaster);

                if ($scope.schememaster.topscheme == 0) {
                    $scope.schememaster.topscheme = "";
                }

                Restangular.all('allschemes?filter[where][scheme_id]=' + $scope.schememaster.id).getList().then(function (allscheme) {
                    $scope.alloriginal = allscheme[0];
                    if (allscheme.length > 0) {
                        $scope.schememasterall = Restangular.copy($scope.alloriginal);
                    }
                });
            });

            Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (tablangs) {
                $scope.tablanguages1 = tablangs;
                $scope.tablanguages1.splice(0, 1);
                $scope.currValue = 0;

                Restangular.all('schemes?filter[where][deleteFlag]=false&filter[where][parentId]=' + $routeParams.id + '&filter[where][parentFlag]=false').getList().then(function (intvls1) {
                    $scope.mtlangs1 = intvls1;
                    console.log($scope.mtlangs1);

                    angular.forEach($scope.tablanguages1, function (member, index) {
                        member.index = index;

                        var data = $scope.mtlangs1.filter(function (arr) {
                            return arr.language == member.id
                        })[0];
                        
                        console.log(data);

                        if (data == undefined) {
                            $scope.rowArray.push({
                                language: member.id,
                                id: 0
                            });
                        } else {
                            $scope.rowArray.push(data);
                        }

                    });
                });
            });

        } else {
            $scope.message = 'Scheme has been Created!';
        }

        $scope.validatestring = '';

        $scope.Update = function () {

            if ($scope.schememaster.name == '' || $scope.schememaster.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name';
            } else if ($scope.schememaster.schemeState == '' || $scope.schememaster.schemeState == null) {
                $scope.validatestring = $scope.validatestring + 'Please select a State or Union Territory';
            } else if ($scope.schememaster.name == '' || $scope.schememaster.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme name';
            } else if ($scope.schememaster.localname == '' || $scope.schememaster.localname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter scheme local name';
            } else if ($scope.schememaster.category == '' || $scope.schememaster.category == null) {
                $scope.validatestring = $scope.validatestring + 'Please select category';
            } else if ($scope.schememaster.agegroup == '' || $scope.schememaster.agegroup == null) {
                $scope.validatestring = $scope.validatestring + 'Please select age group';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.toggleLoading();

                $scope.schememaster.lastModifiedBy = $window.sessionStorage.userId;
                $scope.schememaster.lastModifiedDate = new Date();
                $scope.schememaster.lastModifiedByRole = $window.sessionStorage.roleId;

                Restangular.one('schemes', $routeParams.id).customPUT($scope.schememaster).then(function (response) {

                    if ($scope.alterTopScheme == true) {
                        $scope.schemeToAlter.topscheme = null;
                        $scope.submitschememasters.customPUT($scope.schemeToAlter).then(function () {
                            $scope.langUpdateFunc(response);
                        });
                    } else {
                        $scope.langUpdateFunc(response);
                    }
                });
            }
        };

        $scope.langUpdatecount = 0;

        $scope.langUpdateFunc = function (respObject) {

            if ($scope.langUpdatecount < $scope.rowArray.length) {

                if ($scope.rowArray[$scope.langUpdatecount].id == 0 || $scope.rowArray[$scope.langUpdatecount].id == "0") {
                    $scope.rowArray[$scope.langUpdatecount].parentId = respObject.id;
                    $scope.rowArray[$scope.langUpdatecount].schemeState = respObject.schemeState;
                    $scope.rowArray[$scope.langUpdatecount].category = respObject.category;
                    $scope.rowArray[$scope.langUpdatecount].objectives = respObject.objectives;
                    $scope.rowArray[$scope.langUpdatecount].department = respObject.department;
                    $scope.rowArray[$scope.langUpdatecount].agegroup = respObject.agegroup;
                    $scope.rowArray[$scope.langUpdatecount].gender = respObject.gender;
                    $scope.rowArray[$scope.langUpdatecount].educationstatus = respObject.educationstatus;
                    $scope.rowArray[$scope.langUpdatecount].healthstatus = respObject.healthstatus;
                    $scope.rowArray[$scope.langUpdatecount].minoritystatus = respObject.minoritystatus;
                    $scope.rowArray[$scope.langUpdatecount].socialstatus = respObject.socialstatus;
                    $scope.rowArray[$scope.langUpdatecount].occupationstatus = respObject.occupationstatus;
                    $scope.rowArray[$scope.langUpdatecount].locationtype = respObject.locationtype;
                    $scope.rowArray[$scope.langUpdatecount].incomestatus = respObject.incomestatus;
                    $scope.rowArray[$scope.langUpdatecount].minimumincome = respObject.minimumincome;
                    $scope.rowArray[$scope.langUpdatecount].maximumincome = respObject.maximumincome;
                    $scope.rowArray[$scope.langUpdatecount].anyothercriteria = respObject.anyothercriteria;
                    $scope.rowArray[$scope.langUpdatecount].parentFlag = false;
                    $scope.rowArray[$scope.langUpdatecount].deleteFlag = false;
                    $scope.rowArray[$scope.langUpdatecount].createdDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].createdByRole = $window.sessionStorage.roleId;
                    $scope.rowArray[$scope.langUpdatecount].createdBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedByRole = $window.sessionStorage.roleId;
                    delete $scope.rowArray[$scope.langUpdatecount]['id'];

                    Restangular.all('schemes').post($scope.rowArray[$scope.langUpdatecount]).then(function () {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(respObject);
                    });

                } else {

                    $scope.rowArray[$scope.langUpdatecount].schemeState = respObject.schemeState;
                    $scope.rowArray[$scope.langUpdatecount].category = respObject.category;
                    $scope.rowArray[$scope.langUpdatecount].objectives = respObject.objectives;
                    $scope.rowArray[$scope.langUpdatecount].department = respObject.department;
                    $scope.rowArray[$scope.langUpdatecount].agegroup = respObject.agegroup;
                    $scope.rowArray[$scope.langUpdatecount].gender = respObject.gender;
                    $scope.rowArray[$scope.langUpdatecount].educationstatus = respObject.educationstatus;
                    $scope.rowArray[$scope.langUpdatecount].healthstatus = respObject.healthstatus;
                    $scope.rowArray[$scope.langUpdatecount].minoritystatus = respObject.minoritystatus;
                    $scope.rowArray[$scope.langUpdatecount].socialstatus = respObject.socialstatus;
                    $scope.rowArray[$scope.langUpdatecount].occupationstatus = respObject.occupationstatus;
                    $scope.rowArray[$scope.langUpdatecount].locationtype = respObject.locationtype;
                    $scope.rowArray[$scope.langUpdatecount].incomestatus = respObject.incomestatus;
                    $scope.rowArray[$scope.langUpdatecount].minimumincome = respObject.minimumincome;
                    $scope.rowArray[$scope.langUpdatecount].maximumincome = respObject.maximumincome;
                    $scope.rowArray[$scope.langUpdatecount].anyothercriteria = respObject.anyothercriteria;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedDate = new Date();
                    $scope.rowArray[$scope.langUpdatecount].lastModifiedByRole = $window.sessionStorage.roleId;

                    Restangular.one('schemes', $scope.rowArray[$scope.langUpdatecount].id).customPUT($scope.rowArray[$scope.langUpdatecount]).then(function (resp1) {
                        $scope.langUpdatecount++;
                        $scope.langUpdateFunc(respObject);
                    });
                }

            } else {
                $scope.modalInstanceLoad.close();
                $scope.schememasterdataModal = !$scope.schememasterdataModal;
                console.log('reloading...');

                setInterval(function () {
                    window.location = '/schemes-list';
                }, 350);
            }
        };


        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.$watch('schememaster.topscheme', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {
                $scope.alterTopScheme = false;
                if ($scope.schememaster.schemeState == undefined || $scope.schememaster.schemeState == '') {
                    alert('Select State');
                    $scope.schememaster.topscheme = '';
                } else {
                    Restangular.all('schemes?filter[where][schemeState]=' + $scope.schememaster.schemeState + '&filter[where][topscheme]=' + newValue).getList().then(function (topscheme) {
                        console.log('topscheme', topscheme);
                        if (topscheme.length > 0 && topscheme[0].id != $scope.schememaster.id) {
                            $scope.openSp();
                            $scope.schememastertopscheme = oldValue;
                            $scope.schemeToAlter = topscheme[0];
                        }
                    });
                }
            }
        });
    });
