'use strict';

angular.module('secondarySalesApp')
	.controller('MaryCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/maritalstatus/create' || $location.path() === '/maritalstatus/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/maritalstatus/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/maritalstatus/create' || $location.path() === '/maritalstatus/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/maritalstatus/create' || $location.path() === '/maritalstatus/' + $routeParams.id;
			return visible;
		};
		/*********/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			//console.log('$scope.countryId From Landing', $scope.pageSize);
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			console.log('mypage', mypage);
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};

		console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
		if ($window.sessionStorage.prviousLocation != "partials/maritalstatus") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
			$scope.currentpage = 1;
			$scope.pageSize = 25;
		}

		/**********************************/
		if ($routeParams.id) {
			$scope.message = 'Marital Status has been Updated!';
			Restangular.one('maritalstatuses', $routeParams.id).get().then(function (maritalstatus) {
				$scope.original = maritalstatus;
				$scope.maritalstatus = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Marital Status has been Updated!';
		}
		$scope.Search = $scope.name;

		/************************ INDEX *******************************************/
		$scope.zn = Restangular.all('maritalstatuses?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.maritalstatuses = zn;
			angular.forEach($scope.maritalstatuses, function (member, index) {
				member.index = index + 1;
			});
		});

		/************ SAVE *******************************************/
		$scope.maritalstatus = {
			"name": '',
			"deleteflag": false
		};

		$scope.submitDisable = false;
		$scope.validatestring = '';
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.maritalstatus.name == '' || $scope.maritalstatus.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.maritalstatus.hnname == '' || $scope.maritalstatus.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.maritalstatus.knname == '' || $scope.maritalstatus.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.maritalstatus.taname == '' || $scope.maritalstatus.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.maritalstatus.tename == '' || $scope.maritalstatus.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.maritalstatus.mrname == '' || $scope.maritalstatus.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.maritalstatuses.post($scope.maritalstatus).then(function () {
					console.log('maritalstatus Saved');
					window.location = '/maritalstatus';
				});
			};
		};
		/************************************** UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.maritalstatus.name == '' || $scope.maritalstatus.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.maritalstatus.hnname == '' || $scope.maritalstatus.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.maritalstatus.knname == '' || $scope.maritalstatus.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.maritalstatus.taname == '' || $scope.maritalstatus.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.maritalstatus.tename == '' || $scope.maritalstatus.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name in melugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.maritalstatus.mrname == '' || $scope.maritalstatus.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter marital status name in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.maritalstatuses.customPUT($scope.maritalstatus).then(function () {
					console.log('maritalstatus Saved');
					window.location = '/maritalstatus';
				});
			}
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

		/************************************ DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('maritalstatuses/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

	});
