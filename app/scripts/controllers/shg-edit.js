'use strict';

angular.module('secondarySalesApp')
    .controller('ShgEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/

        $scope.SHGLanguage = {};

        Restangular.one('shglanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.SHGLanguage = langResponse[0];
            $scope.SHGheading = $scope.SHGLanguage.shgEdit;
            // $scope.modalTitle = $scope.SHGLanguage.thankYou;
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        //  $scope.FocusMe = true;

        $scope.showForm = function () {
            var visible = $location.path() === '/shg/create' || $location.path() === '/shg/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/shg/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/shg/create';
            return visible;
        };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/shg") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            //  console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*$scope.distributionArea = {
                zoneId: ''
            };*/

        /*  $scope.mybool = 'false';
          console.log('$scope.mybool', $scope.mybool);*/

        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};

        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };

        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };

        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };

        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];
        //Datepicker settings end

        /***************************** Pagination ***********************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.pillarid = $window.sessionStorage.myRoute;
            //console.log('$scope.pillarid', $scope.pillarid);
        }

        $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        // console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        //if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage-View") {
        if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage-View" && $window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $window.sessionStorage.myRoute_currentPage = 1;
            //$scope.currentpage = 1;
            //$scope.pageSize = 5;
        }

        /*************************************************************************************************/

        $scope.YesPopup = true;
        $scope.NoPopup = true;
        $scope.YesIncrement = true;
        $scope.NoIncrement = true;
        $scope.YesProvideInfo = true;
        $scope.NoProvideInfo = true;

        /*********/

        //  $scope.submitsurveyquestns = Restangular.all('surveyquestions').getList().$object;

        Restangular.all('shgs?filter[where][deleteFlag]=false').getList().then(function (shg) {
            $scope.shgs = shg;
            angular.forEach($scope.shgs, function (member, index) {
                member.index = index + 1;
                member.membersInGroup = member.membersInGroup.split(',').length;

                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
            });
        });

        Restangular.all('households?filter[where][deleteFlag]=false').getList().then(function (hh) {
            $scope.households = hh;
            $scope.pillarid = $window.sessionStorage.myRoute;
        });

        Restangular.all('meetingIntervals?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (meets) {
            $scope.meetingIntervals = meets;
        });

        $scope.associatedbanks = Restangular.all('associatedbanks?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        if ($window.sessionStorage.roleId + "" === "3") {

            $scope.disableAssigned = true;

            $scope.siteMgrDisabled = false;
            $scope.hidedelete = true;

            Restangular.all('users?filter[where][deleteFlag]=false' + '&filter[where][roleId]=' + 3 + '&filter[where][id]=' + $window.sessionStorage.userId).getList().then(function (urs) {
                $scope.users = urs;
                $scope.shg.associatedHF = $window.sessionStorage.userId;

            });
        } else {

            $scope.disableAssigned = false;

            $scope.siteMgrDisabled = true;
            $scope.hidedelete = false;

            Restangular.all('users?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"roleId":{"inq":[3]}}]}}').getList().then(function (urs) {
                $scope.users = urs;
                $scope.shg.associatedHF = $scope.shg.associatedHF;

            });

        }

        $scope.displaymembers = [];

        $scope.grpmembers = [];

        function filterByName(patients, typedValue) {
            var matches_name = -1;
            var matches_individualid = -1;
            var matches_phone = -1;
            return patients.filter(function (patient) {
                if (patient.name != null) {
                    matches_name = patient.name.toLowerCase().indexOf(typedValue.toLowerCase()) != -1;
                }
                if (patient.individualId != null) {
                    matches_individualid = patient.individualId.toLowerCase().indexOf(typedValue.toLowerCase()) != -1;
                }
                if (patient.mobile != null) {
                    matches_phone = patient.mobile.toLowerCase().indexOf(typedValue.toLowerCase()) != -1;
                }

                return matches_name || matches_individualid || matches_phone;
            });
        }
        $scope.filterByName = filterByName;

        if ($window.sessionStorage.previous == '/shg/household/create') {
            $scope.shg = JSON.parse($window.sessionStorage.ShgData);
            $scope.displaymembers = JSON.parse($window.sessionStorage.ShgMemberData);
            //  $scope.grpmembers = JSON.parse($window.sessionStorage.ShgGroupMemData);

            Restangular.one('members', $window.sessionStorage.shgMemberId).get().then(function (customer) {
                $scope.idvalue = customer.id;
                $scope.displaymembers.push({
                    name: customer.name,
                    individualId: customer.individualId,
                    age: customer.age,
                    mobile: customer.mobile,
                    id: customer.id,
                    parentId: customer.parentId,
                    disableDelete: false
                });
                //  $scope.grpmembers.push(customer.id);
                //  console.log(' $scope.grpmembers', $scope.grpmembers);
                $scope.selected = '';
            });
        } else {
            $window.sessionStorage.shgMemberId = null;
            $window.sessionStorage.ShgData = null;
            $window.sessionStorage.ShgMemberData = null;
            $window.sessionStorage.ShgGroupMemData = null;
            $window.sessionStorage.previousRouteparamsId = null;
            $scope.shg = {
                countryId: $window.sessionStorage.countryId,
                stateId: $window.sessionStorage.stateId,
                districtId: $window.sessionStorage.districtId,
                siteId: $window.sessionStorage.siteId.split(",")[0],
                deleteFlag: false,
                formationDate: new Date()
            };
        }

        $scope.myArray = [];

        $scope.myTmpFlg = false;

        $scope.onSelect = function ($item, $model, $label) {
            console.log('$model', $model);

            Restangular.one('members/findOne?filter[where][id]=' + $model.id + '&filter[where][age][gt]=17').get().then(function (customer) {
                $scope.idvalue = customer.id;
                $scope.displaymembers.push({
                    name: customer.name,
                    individualId: customer.individualId,
                    age: customer.age,
                    mobile: customer.mobile,
                    id: customer.id,
                    parentId: customer.parentId,
                    disableDelete: false
                });

                var a = $scope.displaymembers;

                var b = _.uniq(a, function (v) {
                    return v.id;
                })

                $scope.displaymembers = b;

                $scope.selected = '';
            });
        };


        $scope.$watch('selected', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '') {
                return;
            } else {

                //                Restangular.one('members/findOne?filter[where][name]=' + newValue + '&filter[where][age][gt]=17').get().then(function (customer) {
                //
                //                    for (var d = 0; d < $scope.displaymembers.length; d++) {
                //                        if ($scope.displaymembers[d].id == customer.id) {
                //                            // console.log($scope.displaymembers[d]);
                //                            //  alert('Existing member');
                //                            $scope.myTmpFlg = true;
                //                            $scope.selected = '';
                //                            break;
                //                        } else if ($scope.displaymembers[d].parentId == customer.parentId) {
                //                            //  console.log($scope.displaymembers[d]);
                //                            //  alert('Existing Household member');
                //                            $scope.myTmpFlg = true;
                //                            $scope.selected = '';
                //                            break;
                //                        } else if ($scope.myTmpFlg != false) {
                //                            $scope.idvalue = customer.id;
                //                            $scope.displaymembers.push({
                //                                name: customer.name,
                //                                individualId: customer.individualId,
                //                                age: customer.age,
                //                                mobile: customer.mobile,
                //                                id: customer.id,
                //                                parentId: customer.parentId
                //                            });
                //                        } else {
                //                            return;
                //                        }
                //                    }
                //                    $scope.selected = '';
                //                });
            }

        });

        $scope.StoreData = function () {
            // console.log('$routeParams.id', $routeParams.id);
            $window.sessionStorage.shgMemberId = null;
            $window.sessionStorage.ShgData = angular.toJson($scope.shg);
            $window.sessionStorage.ShgMemberData = angular.toJson($scope.displaymembers);
            //  $window.sessionStorage.ShgGroupMemData = angular.toJson($scope.grpmembers);
            $window.sessionStorage.previousRouteparamsId = $routeParams.id;
        };

        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.memberFilterCall = 'members?filter[where][deleteFlag]=false' + '&filter[where][age][gt]=17' + '&filter[where][associatedHF]=' + $window.sessionStorage.userId;
        } else {
            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"age":{"gt":17}}]}}';
        }

        //        if ($window.sessionStorage.roleId + "" === "3") {
        //            $scope.memberFilterCall = 'members?filter[where][deleteFlag]=false' + '&filter[where][gender]=2' + '&filter[where][age][gt]=17' + '&filter[where][associatedHF]=' + $window.sessionStorage.userId;
        //        } else {
        //            $scope.memberFilterCall = 'members?filter={"where":{"and":[{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}},{"gender":{"inq":[2]}},{"age":{"gt":17}}]}}';
        //        }

        Restangular.all($scope.memberFilterCall).getList().then(function (mems) {
            //  $scope.members = mems;

            $scope.members = [];

            angular.forEach(mems, function (member, index) {
                member.index = index;

                if (member.shgId == 0 || member.shgId == '0' || member.shgId == null || member.shgId == '') {
                    $scope.members.push(member);
                } else {
                    return;
                }
            });
        });

        var uniqueNames = [];

        $scope.value = '';
        var timeout, delay = 1000;

        //        $scope.$watch('shg.amountToSave', function (newValue, oldValue) {
        //            if (timeout) {
        //                $timeout.cancel(timeout);
        //            }
        //            timeout = $timeout(function () {
        //                    if (newValue === oldValue || newValue == '') {
        //                        return;
        //                    } else {
        //                        if (newValue < 50) {
        //                           // alert($scope.SHGLanguage.amountShouldBe);
        //                           // $scope.shg.amountToSave = '';
        //                        } else {
        //                            return;
        //                        }
        //                    }
        //                },
        //                delay);
        //        });

        $(document).on('keyup', '#interest', function (event) {

            var input = event.currentTarget.value;

            if (input.search(/^0/) != -1) {
                $scope.shg.interestRate = '';
            }
        });

        if ($window.sessionStorage.roleId + "" === "3") {
            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"associatedHF":{"inq":[' + $window.sessionStorage.userId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        } else {
            $scope.shgFilterCall = 'shgs?filter={"where":{"and":[{"membersInGroup":{"nin":[""]}},{"siteId":{"inq":[' + $window.sessionStorage.siteId + ']}},{"deleteFlag":{"inq":[false]}}]}}';
        }

        $scope.duplicateSHG == false;

        $scope.Validate = function ($event, value) {

            value = value.toLowerCase();

            function search(nameKey, myArray) {
                for (var i = 0; i < myArray.length; i++) {
                    myArray[i].groupName = myArray[i].groupName.toLowerCase();
                    if (myArray[i].groupName === nameKey) {
                        return myArray[i];
                    }
                }
            }

            if (value == '' || value == null) {
                return;
            } else {
                Restangular.all($scope.shgFilterCall).getList().then(function (shgList) {
                    var resultObject = search(value, shgList);
                    if (resultObject == undefined || $scope.shgFullName == value) {
                        document.getElementById('name').style.borderColor = "";
                        $scope.duplicateSHG == false;
                    } else {
                        $scope.toggleValidation();
                        $scope.validatestring1 = $scope.SHGLanguage.existingShgNamePleaseChange;
                        $scope.shg.groupName = '';
                        document.getElementById('name').style.borderColor = "#FF0000";
                        $scope.duplicateSHG == true;
                    }
                });
            }
        };


        $('#name').keypress(function (evt) {
            if (/^[a-zA-Z 0-9 _]*$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $('#accno').keypress(function (evt) {
            if (/^[0-9]+$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $('#amount').keypress(function (evt) {
            if (/((\d+)((\.\d{1,2})?))$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        $('#interest').keypress(function (evt) {
            if (/^[0-9.]+$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        /************* Removing member from shg group ***************/
        $scope.Remove = function (index) {
            console.log('$scope.displaymembers', $scope.displaymembers[index]);
            $scope.displaymembers[index].disableDelete = true;

            //$scope.MemberData = $scope.displaymembers[index].id;
            console.log('$scope.displaymembers', $scope.displaymembers);

            /* $scope.memeberremove = {
                 id: $scope.displaymembers[index].id,
                 shgId: null
             };*/

            //$scope.displaymembers.splice(index, 1);
            // $scope.grpmembers.splice(index, 1)
        };


        $scope.memeberremove = {
            shgId: null
        };


        $scope.shg = {
            countryId: $window.sessionStorage.countryId,
            stateId: $window.sessionStorage.stateId,
            districtId: $window.sessionStorage.districtId,
            siteId: $window.sessionStorage.siteId.split(",")[0],
            deleteFlag: false,
            formationDate: new Date()
        };

        $scope.validatestring = '';

        $scope.shjObj = {};
        $scope.confirmSaveSHG = false;
        /***************************** UPDATE *******************************************/

        $scope.Update = function (clicked) {


            //$scope.shg.amountToSave = $scope.shg.amountToSave;
            // console.log('$scope.shg', $scope.shg);


            for (var i = 0; i < $scope.displaymembers.length; i++) {
                $scope.grpmembers.push($scope.displaymembers[i].id);
            }

            var c = $scope.grpmembers;

            var d = _.uniq(c, function (w) {
                return w;
            })

            $scope.grpmembers = d;

            $scope.shg.membersInGroup = $scope.grpmembers.toString();
            //  console.log('$scope.shg.membersInGroup', $scope.shg.membersInGroup);

            document.getElementById('name').style.border = "";
            //  document.getElementById('date').style.border = "";
            document.getElementById('amount').style.border = "";
            document.getElementById('interest').style.border = "";

            if ($scope.shg.groupName == '' || $scope.shg.groupName == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.enterNameOfGroup;
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.duplicateSHG == true) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.existingShgNamePleaseChange;
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.shg.formationDate == '' || $scope.shg.formationDate == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.selectDateOfFormation;
                //  document.getElementById('date').style.borderColor = "#FF0000";

            } else if ($scope.shg.associatedHF == '' || $scope.shg.associatedHF == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.selectAssociateHF;

            } else if ($scope.shg.meetingInterval == '' || $scope.shg.meetingInterval == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.selectMeetingInterval;
            } else if ($scope.shg.amountToSave === 0) {
                //$scope.validatestring = $scope.validatestring + $scope.SHGLanguage.enterAmount;
                //document.getElementById('amount').style.borderColor = "#FF0000";
                //console.log('$scope.shg.amountToSave', $scope.shg.amountToSave);

            } else if ($scope.shg.amountToSave == '' || $scope.shg.amountToSave == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.enterAmount;
                document.getElementById('amount').style.borderColor = "#FF0000";
                console.log('$scope.shg.amountToSave', $scope.shg.amountToSave);
            } else if ($scope.shg.interestRate == '' || $scope.shg.interestRate == null) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.enterInterestRate;
                document.getElementById('interest').style.borderColor = "#FF0000";

            }
            /*else if ($scope.displaymembers.length == 0) {
                $scope.validatestring = $scope.validatestring + $scope.SHGLanguage.atleastOneMember;
            }*/
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.shjObj.displayDateOfFormation = $filter('date')($scope.shg.formationDate, 'dd-MMM-yyyy');
                $scope.shjObj.displayNameOfGroup = $scope.shg.groupName;
                $scope.shjObj.displayAmountToBeSave = $scope.shg.amountToSave;
                $scope.shjObj.displayInterest = $scope.shg.interestRate;
                $scope.confirmSaveSHG = true;
                if ($window.sessionStorage.language == 1) {

                    Restangular.one('users', $scope.shg.associatedHF).get().then(function (udr) {
                        Restangular.one('meetingintervals', $scope.shg.meetingInterval).get().then(function (vdr) {

                            $scope.shjObj.displayAssociateHF = udr.name;
                            $scope.shjObj.displayMeetingInterval = vdr.name;
                        });
                    });


                } else {

                    Restangular.one('users', $scope.shg.associatedHF).get().then(function (udr) {
                        Restangular.one('meetingintervals/findOne?filter[where][parentId]=' + $scope.shg.meetingInterval + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (vdr) {

                            $scope.shjObj.displayAssociateHF = udr.name;
                            $scope.shjObj.displayMeetingInterval = vdr.name;
                        });
                    });
                }

            }
        };


        /*********************** Final Save After confrimation ***************/
        $scope.SaveShgConfirm = function () {


            $scope.toggleLoading();

            if ($routeParams.id) {
                $scope.message = $scope.SHGLanguage.thankYouUpdated;
            } else {
                $scope.message = $scope.SHGLanguage.thankYouCreated;
            }
            // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            //$scope.submitDisable = true;

            $scope.shg.lastModifiedDate = new Date();
            $scope.shg.lastModifiedBy = $window.sessionStorage.userId;
            $scope.shg.lastModifiedByRole = $window.sessionStorage.roleId;

            //toaster.pop('success', "State has been created", null, null, 'trustedHtml');
            Restangular.one('shgs', $routeParams.id).customPUT($scope.shg).then(function (znResponse) {
                //  console.log('znResponse', znResponse);
                $scope.updateHouseholdData(znResponse.id);
            });
        };


        /*********************** Final Save After confrimation ***************/

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.UserLanguage = $window.sessionStorage.language;

        $scope.memeberupdate = {};

        $scope.memberCount = 0;

        $scope.updateHouseholdData = function (shgId) {

            if ($scope.memberCount < $scope.displaymembers.length) {
                $scope.memeberupdate.shgId = shgId;

                Restangular.one('members', $scope.displaymembers[$scope.memberCount].id).customPUT($scope.memeberupdate).then(function (memResp) {

                    Restangular.one('households', memResp.parentId).get().then(function (hsehld) {

                        $scope.shgbelongs = hsehld.ssgThisHHBelongs;

                        if ($scope.shgbelongs === null || $scope.shgbelongs === 'null') {
                            $scope.shgbelongs = "";
                        }

                        $scope.householdsData = {
                            id: hsehld.id,
                            ssgThisHHBelongs: ""
                        }

                        if ($scope.shgbelongs === "") {
                            $scope.householdsData.ssgThisHHBelongs = shgId;
                        } else {
                            $scope.householdsData.ssgThisHHBelongs = $scope.shgbelongs + "," + shgId;

                            var array = $scope.householdsData.ssgThisHHBelongs.split(",");

                            array.sort(function (a, b) {
                                return a - b
                            });

                            var uniqueArray = [array[0]];

                            for (var i = 1; i < array.length; i++) {
                                if (array[i - 1] !== array[i])
                                    uniqueArray.push(array[i]);
                            }

                            $scope.householdsData.ssgThisHHBelongs = uniqueArray.toString();
                        }

                        Restangular.one('households', hsehld.id).customPUT($scope.householdsData).then(function (memResp) {
                            $scope.memberCount++
                                if ($scope.memberCount < $scope.displaymembers.length) {
                                    $scope.updateHouseholdData(shgId);
                                } else {

                                    /*** $scope.modalInstanceLoad.close();
                                     $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                                     console.log('reloading...');

                                     setInterval(function () {
                                         window.location = '/shg-list';
                                     }, 350);***/

                                    $scope.parentSave();
                                }
                        });
                    });
                });
            } else {

                /*** $scope.modalInstanceLoad.close();
                 $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                 console.log('reloading...');

                 setInterval(function () {
                     window.location = '/shg-list';
                 }, 350); ***/

                $scope.parentSave();
            }
        };

        /********************************* New Changes ********************************/
        $scope.parentSaveCount = 0;

        $scope.parentSave = function () {

            if ($scope.parentSaveCount < $scope.displaymembers.length) {

                if ($scope.displaymembers[$scope.parentSaveCount].disableDelete == true) {

                    //  console.log('$scope.parentSaveCount', $scope.parentSaveCount);
                    $scope.count = 0;
                    Restangular.all('members?filter[where][parentId]=' + $scope.displaymembers[$scope.parentSaveCount].parentId + '&filter[where][shgId][nlike]=null%').getList().then(function (memberResponse) {

                        //  console.log('memberResponse', memberResponse);

                        angular.forEach(memberResponse, function (member, index) {
                            if ($routeParams.id == member.shgId) {
                                $scope.count++;
                            }

                        });
                        // console.log('MemberGetId',$scope.displaymembers[$scope.parentSaveCount].id);

                        Restangular.one('members', $scope.displaymembers[$scope.parentSaveCount].id).customPUT($scope.memeberremove).then(function (memResp) {
                            //    console.log('memResp', memResp);

                            Restangular.one('households', $scope.displaymembers[$scope.parentSaveCount].parentId).get().then(function (hsehld) {
                                $scope.ssgThisHHBelongsArray = hsehld.ssgThisHHBelongs.split(",");
                                // console.log($scope.ssgThisHHBelongsArray);

                                for (var i = 0; i < $scope.ssgThisHHBelongsArray.length; i++) {
                                    if ($scope.ssgThisHHBelongsArray[i] + '' === $routeParams.id + '') {
                                        if ($scope.count == 1) {
                                            $scope.ssgThisHHBelongsArray.splice(i, 1);
                                            // console.log($scope.ssgThisHHBelongsArray);
                                        }
                                    }
                                }

                                hsehld.ssgThisHHBelongs = $scope.ssgThisHHBelongsArray.toString();
                                // console.log(hsehld);

                                Restangular.one('households', hsehld.id).customPUT(hsehld).then(function (hhResp) {

                                    Restangular.one('shgs', $routeParams.id).get().then(function (shg) {
                                        // console.log('shgResponse', shg);

                                        $scope.membersInGroupsArray = shg.membersInGroup.split(",");
                                        for (var i = 0; i < $scope.membersInGroupsArray.length; i++) {
                                            if ($scope.membersInGroupsArray[i] + '' === memResp.id + '') {
                                                $scope.membersInGroupsArray.splice(i, 1);
                                                //  console.log(' $scope.membersInGroupsArray',  $scope.membersInGroupsArray);
                                            }
                                        }

                                        shg.membersInGroup = $scope.membersInGroupsArray.toString();
                                        //console.log('shg.membersInGroup', shg.membersInGroup);

                                        Restangular.one('shgs', $routeParams.id).customPUT(shg).then(function (ShGResp) {

                                            //  console.log('ShGResp', ShGResp);

                                            $scope.parentSaveCount++;
                                            $scope.parentSave();

                                        });
                                    });

                                });

                            });

                        });
                    });

                } else {
                    $scope.parentSaveCount++;
                    $scope.parentSave();
                }

            } else {


                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setInterval(function () {
                    window.location = '/shg-list';
                }, 1500);

            }

        };

        /********************************* New Changes ********************************/

        var memberslists = [];

        if ($routeParams.id) {
            $scope.message = $scope.SHGLanguage.thankYouUpdated;
            Restangular.one('shgs', $routeParams.id).get().then(function (shg) {
                if (shg.amountToSave === 0) {
                    console.log('hi checking amount');
                    var xyz = shg.amountToSave;
                    $scope.shg.amountToSave = 0;
                }
                $scope.original = shg;
                $scope.shg = Restangular.copy($scope.original);
                console.log('$scope.shg', $scope.shg);
                $scope.shgFullName = shg.groupName;


                if ($window.sessionStorage.previous == '/shg-list') {

                    var memberslists = $scope.shg.membersInGroup.split(",");

                    memberslists.sort(function (a, b) {
                        return a - b
                    });

                    var uniqueArray = [memberslists[0]];

                    for (var i = 1; i < memberslists.length; i++) {
                        if (memberslists[i - 1] !== memberslists[i])
                            uniqueArray.push(memberslists[i]);
                    }

                    for (var i = 0; i < uniqueArray.length; i++) {
                        Restangular.one('members/findOne?filter[where][id]=' + uniqueArray[i]).get().then(function (membr) {
                            $scope.idvalue = membr.id;
                            $scope.displaymembers.push({
                                name: membr.name,
                                individualId: membr.individualId,
                                age: membr.age,
                                mobile: membr.mobile,
                                id: membr.id,
                                parentId: membr.parentId,
                                disableDelete: false
                            });

                            console.log('$scope.displaymembers', $scope.displaymembers);

                            //  $scope.grpmembers.push(membr.id);
                            $scope.selected = '';
                        });
                    }
                }
            });
        } else {
            $scope.message = $scope.SHGLanguage.thankYouCreated;
        }

        $scope.CancleButoon = function () {

            $scope.displaymembers
            window.location = '/shg-list';
        };

    });
