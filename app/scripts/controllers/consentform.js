'use strict';

angular.module('secondarySalesApp')
    .controller('ConsentFormCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/consentform/create' || $location.path() === '/consentform/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/consentform/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/consentform/create' || $location.path() === '/consentform/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/consentform/create' || $location.path() === '/consentform/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/consentform") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Consent has been Updated!';
            Restangular.one('consentforms', $routeParams.id).get().then(function (gender) {
                $scope.original = gender;
                $scope.gender = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Facilitytype has been Created!';
        }
        $scope.Search = $scope.name;

        /******************************** INDEX *******************************************/
        $scope.zn = Restangular.all('consentforms?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.consentforms = zn;
            angular.forEach($scope.consentforms, function (member, index) {
                member.index = index + 1;
            });
        });

        /********************************************* SAVE *******************************************/
        $scope.consentform = {
            "name": '',
            "deleteflag": false
        };
        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";

            if ($scope.consentform.name == '' || $scope.consentform.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.consentform.hnname == '' || $scope.consentform.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.consentform.knname == '' || $scope.consentform.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.consentform.taname == '' || $scope.consentform.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.consentform.tename == '' || $scope.consentform.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.consentform.mrname == '' || $scope.consentform.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.consentforms.post($scope.consentform).then(function () {
                    window.location = '/consentform';
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.consentform.name == '' || $scope.consentform.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.consentform.hnname == '' || $scope.consentform.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.consentform.knname == '' || $scope.consentform.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.consentform.taname == '' || $scope.consentform.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.consentform.tename == '' || $scope.consentform.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.consentform.mrname == '' || $scope.consentform.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Consent Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                Restangular.one('consentforms', $routeParams.id).customPUT($scope.consentform).then(function () {
                    console.log('consentform Saved');
                    window.location = '/consentform';
                });
            }
        };
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('consentform/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
