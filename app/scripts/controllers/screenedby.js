'use strict';

angular.module('secondarySalesApp')
    .controller('ScreenedByCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {

        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 9) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/screenedby/create' || $location.path() === '/screenedby/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/screenedby/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/screenedby/create' || $location.path() === '/screenedby/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/screenedby/create' || $location.path() === '/screenedby/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/screenedby") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.$watch('screenedby.conditionId', function (newValue, oldValue) {
            if (newValue == '' || newValue == undefined) {
                return;
            } else {
                Restangular.all('screenedbys?filter[where][deleteFlag]=false' + '&filter[where][default]=yes' + '&filter[where][conditionId]=' + newValue).getList().then(function (screens) {
                    $scope.updatingobj = screens;
                });
            }
        });

        Restangular.all('conditions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + 1).getList().then(function (cns) {
            $scope.conditions = cns;
        });

        if ($routeParams.id) {
            $scope.message = 'ScreenedBy has been Updated!';
            Restangular.one('screenedbys', $routeParams.id).get().then(function (screenedby) {
                $scope.original = screenedby;
                $scope.screenedby = Restangular.copy($scope.original);
                $scope.currvalue = $scope.screenedby.default;
            });

        } else {
            $scope.message = 'ScreenedBy has been Created!';
        }

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];

        /******************************** INDEX *******************************************/

        Restangular.all('screenedbys?filter[where][deleteFlag]=false').getList().then(function (sfs) {
            $scope.screenedbys = sfs;

            Restangular.all('conditions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + 1).getList().then(function (cnslist) {

                angular.forEach($scope.screenedbys, function (member, index) {
                    member.index = index + 1;

                    var data = cnslist.filter(function (arr) {
                        return arr.id == member.conditionId
                    })[0];

                    if (data != undefined) {
                        member.conditionName = data.name;
                    }
                });
            });
        });

        /********************************************* SAVE *******************************************/

        $scope.screenedby = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteFlag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.creatingFlag = false;
        $scope.updatingFlag == false

        $scope.Save = function () {

            document.getElementById('name').style.border = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.updatingobj.length > 0 && $scope.screenedby.default == 'yes') {
                $scope.creatingFlag = true;
            }

            if ($scope.screenedby.conditionId == '' || $scope.screenedby.conditionId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Condition';

            } else if ($scope.screenedby.name == '' || $scope.screenedby.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                document.getElementById('name').style.border = "#FF0000";

            } else if ($scope.screenedby.default == '' || $scope.screenedby.default == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Default';

            } else if ($scope.screenedby.orderNo == '' || $scope.screenedby.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter order';
                document.getElementById('order').style.border = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.submitDisable = true;

                $scope.screenedbys.post($scope.screenedby).then(function () {

                    if ($scope.creatingFlag == true) {

                        $scope.updateRow = {
                            default: 'no'
                        };

                        Restangular.one('screenedbys', $scope.updatingobj[0].id).customPUT($scope.updateRow).then(function (resp) {
                            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                            window.location = '/screenedby-list';
                        });

                    } else {
                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        window.location = '/screenedby-list';
                    }
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/

        $scope.Update = function () {

            document.getElementById('name').style.border = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.updatingobj.length > 0 && $scope.screenedby.default == 'yes' && $scope.currvalue != $scope.screenedby.default) {
                $scope.updatingFlag = true;
            }

            if ($scope.screenedby.conditionId == '' || $scope.screenedby.conditionId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Condition';

            } else if ($scope.screenedby.name == '' || $scope.screenedby.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                document.getElementById('name').style.border = "#FF0000";

            } else if ($scope.screenedby.default == '' || $scope.screenedby.default == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Default';

            } else if ($scope.screenedby.orderNo == '' || $scope.screenedby.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter order';
                document.getElementById('order').style.border = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.submitDisable = true;

                Restangular.one('screenedbys', $routeParams.id).customPUT($scope.screenedby).then(function () {

                    if ($scope.updatingFlag == true) {

                        $scope.updateRow = {
                            default: 'no'
                        };

                        Restangular.one('screenedbys', $scope.updatingobj[0].id).customPUT($scope.updateRow).then(function (resp) {
                            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                            window.location = '/screenedby-list';
                        });

                    } else {
                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        window.location = '/screenedby-list';
                    }
                });
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        
/******************************************************** DELETE *******************************************/
$scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('screenedbys/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

    });
