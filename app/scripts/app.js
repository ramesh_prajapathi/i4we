'use strict';

angular.module('secondarySalesApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'restangular',
    'ui.select2',
    'ui.bootstrap',
    'angularFileUpload',
    'angularUtils.directives.dirPagination',
    'ngIdle',
    'toggle-switch',
    'angucomplete',
    'services.breadcrumbs',
    'ngPageTitle',
    'confirmButton',
    'confirmButton2',
    'countTo'
  ])
    .config(function ($routeProvider, $locationProvider, RestangularProvider, $idleProvider, $keepaliveProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'partials/main1',
                controller: 'MainCtrl',
                label: 'Home',
                data: {
                    pageTitle: 'Home'
                }
            })
            .when('/login', {
                templateUrl: 'partials/login',
                controller: 'LoginCtrl',
                label: 'Login',
                data: {
                    pageTitle: 'Login'
                }
            })
            .when('/roles', {
                templateUrl: 'partials/roles',
                controller: 'RolesCtrl',
                label: 'Roles List',
                data: {
                    pageTitle: 'Roles List'
                }
            })
            .when('/roles/create', {
                templateUrl: 'partials/roles',
                controller: 'RolesCtrl',
                label: 'Roles Create',
                data: {
                    pageTitle: 'Roles Create'
                }
            })
            .when('/roles/:id', {
                templateUrl: 'partials/roles',
                controller: 'RolesCtrl',
                label: 'Roles Edit',
                data: {
                    pageTitle: 'Roles Edit'
                }
            })
            .when('/user-list', {
                templateUrl: 'partials/users',
                controller: 'UsersCtrl',
                label: 'User List',
                data: {
                    pageTitle: 'User List'
                }
            })
            .when('/user/create', {
                templateUrl: 'partials/users-form',
                controller: 'UsersCreateCtrl',
                label: 'User Create',
                data: {
                    pageTitle: 'User Create'
                }
            })
            .when('/user/edit/:id', {
                templateUrl: 'partials/users-form',
                controller: 'UsersEditCtrl',
                label: 'User Edit',
                data: {
                    pageTitle: 'User Edit'
                }
            })
            .when('/groups', {
                templateUrl: 'partials/groups',
                controller: 'GroupsCtrl',
                label: 'Groups List',
                data: {
                    pageTitle: 'Groups List'
                }
            })
            .when('/groups/create', {
                templateUrl: 'partials/groups',
                controller: 'GroupsCtrl',
                label: 'Group Create',
                data: {
                    pageTitle: 'Group Create'
                }
            })
            .when('/groups/:id', {
                templateUrl: 'partials/groups',
                controller: 'GroupsCtrl',
                label: 'Group Edit',
                data: {
                    pageTitle: 'Group Edit'
                }
            })
            .when('/country-list', {
                templateUrl: 'partials/countries',
                controller: 'CountriesCtrl',
                label: 'Country List',
                data: {
                    pageTitle: 'Country List'
                }
            })
            .when('/country/create', {
                templateUrl: 'partials/countries',
                controller: 'CountriesCtrl',
                label: 'Country Create',
                data: {
                    pageTitle: 'Country Create'
                }
            })
            .when('/country/edit/:id', {
                templateUrl: 'partials/countries',
                controller: 'CountriesCtrl',
                label: 'Country Edit',
                data: {
                    pageTitle: 'Country Edit'
                }
            })
            .when('/uploadcountry', {
                templateUrl: 'partials/uploadcountries',
                controller: 'CountryReadXlsCtrl',
                label: 'Country Upload',
                data: {
                    pageTitle: 'Country Upload'
                }
            })
            .when('/state-list', {
                templateUrl: 'partials/states',
                controller: 'StatesCtrl',
                label: 'State List',
                data: {
                    pageTitle: 'State List'
                }
            })
            .when('/state/create', {
                templateUrl: 'partials/states',
                controller: 'StatesCtrl',
                label: 'State Create',
                data: {
                    pageTitle: 'State Create'
                }
            })
            .when('/state/edit/:id', {
                templateUrl: 'partials/states',
                controller: 'StatesCtrl',
                label: 'State Edit',
                data: {
                    pageTitle: 'State Edit'
                }
            })
            .when('/uploadstate', {
                templateUrl: 'partials/uploadstates',
                controller: 'StateReadXlsCtrl',
                label: 'State Upload',
                data: {
                    pageTitle: 'State Upload'
                }
            })
            .when('/district-list', {
                templateUrl: 'partials/districts',
                controller: 'DistrictsCtrl',
                label: 'District List',
                data: {
                    pageTitle: 'District List'
                }
            })
            .when('/district/create', {
                templateUrl: 'partials/districts',
                controller: 'DistrictsCtrl',
                label: 'District Create',
                data: {
                    pageTitle: 'District Create'
                }
            })
            .when('/district/edit/:id', {
                templateUrl: 'partials/districts',
                controller: 'DistrictsCtrl',
                label: 'District Edit',
                data: {
                    pageTitle: 'District Edit'
                }
            })
            .when('/uploaddistrict', {
                templateUrl: 'partials/uploaddistricts',
                controller: 'DistReadXlsCtrl',
                label: 'District Upload',
                data: {
                    pageTitle: 'District Upload'
                }
            })
            .when('/site-list', {
                templateUrl: 'partials/sites',
                controller: 'SitesCtrl',
                label: 'Site List',
                data: {
                    pageTitle: 'Site List'
                }
            })
            .when('/site/create', {
                templateUrl: 'partials/sites',
                controller: 'SitesCtrl',
                label: 'Site Create',
                data: {
                    pageTitle: 'Site Create'
                }
            })
            .when('/site/edit/:id', {
                templateUrl: 'partials/sites',
                controller: 'SitesCtrl',
                label: 'Site Edit',
                data: {
                    pageTitle: 'Site Edit'
                }
            })
            .when('/uploadsite', {
                templateUrl: 'partials/uploadsites',
                controller: 'SiteReadXlsCtrl',
                label: 'Upload Site',
                data: {
                    pageTitle: 'Site Upload'
                }
            })
            .when('/area-list', {
                templateUrl: 'partials/areas',
                controller: 'AreaCtrl',
                label: 'Area',
                data: {
                    pageTitle: 'Area'
                }
            })
            .when('/area/create', {
                templateUrl: 'partials/areas',
                controller: 'AreaCtrl',
                label: 'Area Create',
                data: {
                    pageTitle: 'Area Create'
                }
            })
            .when('/area/edit/:id', {
                templateUrl: 'partials/areas',
                controller: 'AreaCtrl',
                label: 'Area Edit',
                data: {
                    pageTitle: 'Area Edit'
                }
            })
            .when('/employees', {
                templateUrl: 'partials/employees1',
                controller: 'EmployeesCtrl'
            })
            .when('/employees/create', {
                templateUrl: 'partials/employees-form',
                controller: 'EmployeesCreateCtrl'
            })
            .when('/employees/:id', {
                templateUrl: 'partials/employees-form',
                controller: 'EmployeesEditCtrl'
            })
            /************New Added*********/
            .when('/facilitytype', {
                templateUrl: 'partials/facilitytype',
                controller: 'FacilityTypeCtrl'
            })
            .when('/facilitytype/create', {
                templateUrl: 'partials/facilitytype',
                controller: 'FacilityTypeCtrl'
            })
            .when('/facilitytype/:id', {
                templateUrl: 'partials/facilitytype',
                controller: 'FacilityTypeCtrl'
            })
            .when('/areaofoperation', {
                templateUrl: 'partials/areaofoperation',
                controller: 'AreaofOpeartionCtrl'
            })
            .when('/areaofoperation/create', {
                templateUrl: 'partials/areaofoperation',
                controller: 'AreaofOpeartionCtrl'
            })
            .when('/areaofoperation/:id', {
                templateUrl: 'partials/areaofoperation',
                controller: 'AreaofOpeartionCtrl'
            })
            .when('/12astatus', {
                templateUrl: 'partials/12astatus',
                controller: '12AStatusCtrl'
            })
            .when('/12astatus/create', {
                templateUrl: 'partials/12astatus',
                controller: '12AStatusCtrl'
            })
            .when('/12astatus/:id', {
                templateUrl: 'partials/12astatus',
                controller: '12AStatusCtrl'
            })
            .when('/tirating', {
                templateUrl: 'partials/tirating',
                controller: 'TiRatingCtrl'
            })
            .when('/tirating/create', {
                templateUrl: 'partials/tirating',
                controller: 'TiRatingCtrl'
            })
            .when('/tirating/:id', {
                templateUrl: 'partials/tirating',
                controller: 'TiRatingCtrl'
            })
            /* .when('/80gstatus', {
                 templateUrl: 'partials/80gstatus',
                 controller: '80gStatusCtrl'
             })
             .when('/80gstatus/create', {
                 templateUrl: 'partials/80gstatus',
                 controller: '80gStatusCtrl'
             })
             .when('/80gstatus/:id', {
                 templateUrl: 'partials/80gstatus',
                 controller: '80gStatusCtrl'
             })
             .when('/fcrastaus', {
                 templateUrl: 'partials/fcrastaus',
                 controller: 'FcraStausCtrl'
             })
             .when('/fcrastaus/create', {
                 templateUrl: 'partials/fcrastaus',
                 controller: 'FcraStausCtrl'
             })
             .when('/fcrastaus/:id', {
                 templateUrl: 'partials/fcrastaus',
                 controller: 'FcraStausCtrl'
             })*/
            /*****************************/
            .when('/co-organisation', {
                templateUrl: 'partials/co-org',
                controller: 'COgrCtrl'
            })
            .when('/co-organisation/create', {
                templateUrl: 'partials/co-org-form',
                controller: 'COCreateCtrl'
            })
            .when('/co-organisation/:id', {
                templateUrl: 'partials/co-org-form',
                controller: 'COEditCtrl'
            })
            .when('/upload-co-organisation', {
                templateUrl: 'partials/upload-co-org',
                controller: 'CoOrgReadXlsCtrl'
            })
            .when('/healthfacilitator-list', {
                templateUrl: 'partials/f-work',
                controller: 'FWCtrl',
                label: 'Field Work List',
                data: {
                    pageTitle: 'Field Work'
                }
            })
            .when('/healthfacilitator/create', {
                templateUrl: 'partials/f-work-form',
                controller: 'FWCreateCtrl',
                label: 'Field Work Create',
                data: {
                    pageTitle: 'Field Work'
                }
            })
            .when('/healthfacilitator/:id', {
                templateUrl: 'partials/f-work-form',
                controller: 'FWEditCtrl',
                label: 'Field Work Edit',
                data: {
                    pageTitle: 'Field Work'
                }
            })
            .when('/household-list', {
                templateUrl: 'partials/households',
                controller: 'HouseholdCtrl',
                label: 'Households List',
                data: {
                    pageTitle: 'Household List'
                }
            })
            .when('/household/create', {
                templateUrl: 'partials/households-form',
                controller: 'HouseholdCreateCtrl',
                label: 'Household Create',
                data: {
                    pageTitle: 'Household Create'
                }
            })
            .when('/shg/household/create', {
                templateUrl: 'partials/households-form',
                controller: 'HouseholdCreateCtrl',
                label: 'SHG - Household Create',
                data: {
                    pageTitle: 'SHG - Household Create'
                }
            })
            .when('/household/edit/:id', {
                templateUrl: 'partials/households-form',
                controller: 'HouseholdEditCtrl',
                label: 'Household Edit',
                data: {
                    pageTitle: 'Household Edit'
                }
            })
            //            .when('/members/:id', {
            //                templateUrl: 'partials/beneficiaries-form',
            //                controller: 'BnEditCtrl',
            //                label: 'Member Edit',
            //                data: {
            //                    pageTitle: 'Members'
            //                }
            //            })
            /* .when('/coorgedit', {
                     templateUrl: 'partials/coorgedit',
                     controller: 'COOrgEditCtrl',
                     label: 'Facility Profile',
                     data: {
                         pageTitle: 'Facility Profile'
                     }
                 })*/
            .when('/coorgedit/:id', {
                templateUrl: 'partials/coorgedit',
                controller: 'COOrgEditCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }

            })
            .when('/routes', {
                templateUrl: 'partials/routes',
                controller: 'RoutesCtrl'
            })
            .when('/routes/create', {
                templateUrl: 'partials/routes-form',
                controller: 'RoutesCreateCtrl'
            })
            .when('/routes/:id', {
                templateUrl: 'partials/routes-form',
                controller: 'RoutesEditCtrl'
            })
            .when('/uploadroutes', {
                templateUrl: 'partials/uploadroutes',
                controller: 'RouteReadXlsCtrl'
            })
            .when('/sites', {
                templateUrl: 'partials/routesviewlist',
                controller: 'RoutesViewListCtrl',
                label: 'Site List',
                data: {
                    pageTitle: 'Site View'
                }

            })
            .when('/routesview/:id', {
                templateUrl: 'partials/routesview',
                controller: 'RoutesViewCtrl',
                label: 'Site Detials'
            })
            .when('/routelinks', {
                templateUrl: 'partials/routelinks',
                controller: 'RoutelinksCtrl'
            })
            .when('/routelinks/create', {
                templateUrl: 'partials/routelinks',
                controller: 'RoutelinksCtrl'
            })
            .when('/routelinks/:id', {
                templateUrl: 'partials/routelinks',
                controller: 'RoutelinksCtrl'
            })
            .when('/route_assignment', {
                templateUrl: 'partials/route_assignmentdisplays',
                controller: 'RouteassignmentDisplayCtrl'
            })
            .when('/route_assignment/create', {
                templateUrl: 'partials/route_assignment',
                controller: 'RouteassignmentCtrl'
            })
            .when('/route_assignment/:id', {
                templateUrl: 'partials/route_assignmentupdate',
                controller: 'RouteassignmentUpdateCtrl'
            })
            .when('/maps', {
                templateUrl: 'partials/maps',
                controller: 'TestCtrl'
            })
            .when('/maps/create', {
                templateUrl: 'partials/maps',
                controller: 'TestCtrl'
            })
            .when('/maps/:id', {
                templateUrl: 'partials/maps',
                controller: 'TestCtrl'
            })
            .when('/tourtypes', {
                templateUrl: 'partials/tourtypes',
                controller: 'TourtypeCtrl'
            })
            .when('/tourtypes/create', {
                templateUrl: 'partials/tourtypes',
                controller: 'TourtypeCtrl'
            })
            .when('/tourtypes/:id', {
                templateUrl: 'partials/tourtypes',
                controller: 'TourtypeCtrl'
            })
            .when('/assignfwtosite', {
                templateUrl: 'partials/agentroutelinks',
                controller: 'AgentRoutelinksCtrl',
                label: 'Assign FW To Site',
                data: {
                    pageTitle: 'Assign FW To Site'
                }
            })
            .when('/assignfwtosite/create', {
                templateUrl: 'partials/agentroutelinks',
                controller: 'AgentRoutelinksCtrl',
                label: 'Assign FW To Site',
                data: {
                    pageTitle: 'Assign FW To Site'
                }
            })
            .when('/assignfwtosite/:id', {
                templateUrl: 'partials/agentroutelinks',
                controller: 'AgentRoutelinksCtrl',
                label: 'Assign FW To Site',
                data: {
                    pageTitle: 'Assign FW To Site'
                }
            })
            /*.when('/imageupload', {
			        templateUrl: 'partials/imageupload',
			        controller: 'ImageUploadCtrl'
			    })
			    .when('/imageupload/create', {
			        templateUrl: 'partials/imageupload',
			        controller: 'ImageUploadCtrl'
			    })
			    .when('/imageupload/:id', {
			        templateUrl: 'partials/imageupload',
			        controller: 'ImageUploadCtrl'
			    })*/
            .when('/sdocumenttypes', {
                templateUrl: 'partials/sdoctypes',
                controller: 'SDocTypCtrl'
            })
            .when('/sdocumenttypes/create', {
                templateUrl: 'partials/sdoctypes',
                controller: 'SDocTypCtrl'
            })
            .when('/sdocumenttypes/:id', {
                templateUrl: 'partials/sdoctypes',
                controller: 'SDocTypCtrl'
            })
            .when('/fdocumenttypes', {
                templateUrl: 'partials/fdoctypes',
                controller: 'FDocTypCtrl'
            })
            .when('/fdocumenttypes/create', {
                templateUrl: 'partials/fdoctypes',
                controller: 'FDocTypCtrl'
            })
            .when('/fdocumenttypes/:id', {
                templateUrl: 'partials/fdoctypes',
                controller: 'FDocTypCtrl'
            })
            .when('/schemes', {
                templateUrl: 'partials/scheme',
                controller: 'SchCtrl',
                label: 'Schemes List',
                data: {
                    pageTitle: 'Schemes List'
                }
            })
            .when('/schemes/create', {
                templateUrl: 'partials/scheme',
                controller: 'SchCtrl',
                label: 'Scheme Create',
                data: {
                    pageTitle: 'Scheme Create'
                }
            })
            .when('/schemes/:id', {
                templateUrl: 'partials/scheme',
                controller: 'SchCtrl',
                label: 'Scheme Edit',
                data: {
                    pageTitle: 'Scheme Edit'
                }
            })
            .when('/genders-list', {
                templateUrl: 'partials/gender',
                controller: 'GenCtrl',
                label: 'Genders List',
                data: {
                    pageTitle: 'Genders List'
                }
            })
            .when('/gender/create', {
                templateUrl: 'partials/gender',
                controller: 'GenCtrl',
                label: 'Gender Create',
                data: {
                    pageTitle: 'Gender Create'
                }
            })
            .when('/gender/edit/:id', {
                templateUrl: 'partials/gender',
                controller: 'GenCtrl',
                label: 'Gender Edit',
                data: {
                    pageTitle: 'Gender Edit'
                }
            })
            .when('/occupations-list', {
                templateUrl: 'partials/occupation',
                controller: 'OccupationCtrl',
                label: 'Occupations List',
                data: {
                    pageTitle: 'Occupations List'
                }
            })
            .when('/occupation/create', {
                templateUrl: 'partials/occupation',
                controller: 'OccupationCtrl',
                label: 'Occupation Create',
                data: {
                    pageTitle: 'Occupation Create'
                }
            })
            .when('/occupation/edit/:id', {
                templateUrl: 'partials/occupation',
                controller: 'OccupationCtrl',
                label: 'Occupation Edit',
                data: {
                    pageTitle: 'Occupation Edit'
                }
            })
            .when('/castes-list', {
                templateUrl: 'partials/caste',
                controller: 'CasteCtrl',
                label: 'Castes List',
                data: {
                    pageTitle: 'Castes List'
                }
            })
            .when('/caste/create', {
                templateUrl: 'partials/caste',
                controller: 'CasteCtrl',
                label: 'Caste Create',
                data: {
                    pageTitle: 'Caste Create'
                }
            })
            .when('/caste/edit/:id', {
                templateUrl: 'partials/caste',
                controller: 'CasteCtrl',
                label: 'Caste Edit',
                data: {
                    pageTitle: 'Caste Edit'
                }
            })
            .when('/religions-list', {
                templateUrl: 'partials/religion',
                controller: 'ReligionCtrl',
                label: 'Religions List',
                data: {
                    pageTitle: 'Religions List'
                }
            })
            .when('/religion/create', {
                templateUrl: 'partials/religion',
                controller: 'ReligionCtrl',
                label: 'Religion Create',
                data: {
                    pageTitle: 'Religion Create'
                }
            })
            .when('/religion/edit/:id', {
                templateUrl: 'partials/religion',
                controller: 'ReligionCtrl',
                label: 'Religion Edit',
                data: {
                    pageTitle: 'Religion Edit'
                }
            })
            .when('/migrants-list', {
                templateUrl: 'partials/migrant',
                controller: 'MigrantCtrl',
                label: 'Migrants List',
                data: {
                    pageTitle: 'Migrants List'
                }
            })
            .when('/migrant/create', {
                templateUrl: 'partials/migrant',
                controller: 'MigrantCtrl',
                label: 'Migrant Create',
                data: {
                    pageTitle: 'Migrant Create'
                }
            })
            .when('/migrant/edit/:id', {
                templateUrl: 'partials/migrant',
                controller: 'MigrantCtrl',
                label: 'Migrant Edit',
                data: {
                    pageTitle: 'Migrant Edit'
                }
            })
            .when('/bplstatus-list', {
                templateUrl: 'partials/bplstatus',
                controller: 'BplStatusCtrl',
                label: 'BPL Status List',
                data: {
                    pageTitle: 'BPL Status List'
                }
            })
            .when('/bplstatus/create', {
                templateUrl: 'partials/bplstatus',
                controller: 'BplStatusCtrl',
                label: 'BPL Status Create',
                data: {
                    pageTitle: 'BPL Status Create'
                }
            })
            .when('/bplstatus/edit/:id', {
                templateUrl: 'partials/bplstatus',
                controller: 'BplStatusCtrl',
                label: 'BPL Status Edit',
                data: {
                    pageTitle: 'BPL Status Edit'
                }
            })
            .when('/relationwithhh-list', {
                templateUrl: 'partials/relationwithhh',
                controller: 'RelationWithHHCtrl',
                label: 'Relation With HH List',
                data: {
                    pageTitle: 'Relation With HH List'
                }
            })
            .when('/relationwithhh/create', {
                templateUrl: 'partials/relationwithhh',
                controller: 'RelationWithHHCtrl',
                label: 'Relation With HH Create',
                data: {
                    pageTitle: 'Relation With HH Create'
                }
            })
            .when('/relationwithhh/edit/:id', {
                templateUrl: 'partials/relationwithhh',
                controller: 'RelationWithHHCtrl',
                label: 'Relation With HH Edit',
                data: {
                    pageTitle: 'Relation With HH Edit'
                }
            })
            .when('/literacylevel-list', {
                templateUrl: 'partials/literacylevel',
                controller: 'LiteracyLevelCtrl',
                label: 'Literacy Level List',
                data: {
                    pageTitle: 'Literacy Level List'
                }
            })
            .when('/literacylevel/create', {
                templateUrl: 'partials/literacylevel',
                controller: 'LiteracyLevelCtrl',
                label: 'Literacy Level Create',
                data: {
                    pageTitle: 'Literacy Level Create'
                }
            })
            .when('/literacylevel/edit/:id', {
                templateUrl: 'partials/literacylevel',
                controller: 'LiteracyLevelCtrl',
                label: 'Literacy Level Edit',
                data: {
                    pageTitle: 'Literacy Level Edit'
                }
            })
            .when('/nameofschools-list', {
                templateUrl: 'partials/nameofschool',
                controller: 'NameofSchoolCtrl',
                label: 'Name of Schools List',
                data: {
                    pageTitle: 'Name of Schools List'
                }
            })
            .when('/nameofschool/create', {
                templateUrl: 'partials/nameofschool',
                controller: 'NameofSchoolCtrl',
                label: 'Name of School Create',
                data: {
                    pageTitle: 'Name of School Create'
                }
            })
            .when('/nameofschool/edit/:id', {
                templateUrl: 'partials/nameofschool',
                controller: 'NameofSchoolCtrl',
                label: 'Name of School Edit',
                data: {
                    pageTitle: 'Name of Factory Edit'
                }
            })
            .when('/nameoffactories-list', {
                templateUrl: 'partials/nameoffactory',
                controller: 'NamofFactoryCtrl',
                label: 'Name of Factory List',
                data: {
                    pageTitle: 'Name of Factory List'
                }
            })
            .when('/nameoffactory/create', {
                templateUrl: 'partials/nameoffactory',
                controller: 'NamofFactoryCtrl',
                label: 'Name of Factory Create',
                data: {
                    pageTitle: 'Name of Factory Create'
                }
            })
            .when('/nameoffactory/edit/:id', {
                templateUrl: 'partials/nameoffactory',
                controller: 'NamofFactoryCtrl',
                label: 'Name of Factory Edit',
                data: {
                    pageTitle: 'Name of Factory Edit'
                }
            })
            .when('/meetinginterval-list', {
                templateUrl: 'partials/meetinginterval',
                controller: 'MeetingIntervalCtrl',
                label: 'Meeting Interval List',
                data: {
                    pageTitle: 'Meeting Interval List'
                }
            })
            .when('/meetinginterval/create', {
                templateUrl: 'partials/meetinginterval',
                controller: 'MeetingIntervalCtrl',
                label: 'Meeting Interval Create',
                data: {
                    pageTitle: 'Meeting Interval Create'
                }
            })
            .when('/meetinginterval/edit/:id', {
                templateUrl: 'partials/meetinginterval',
                controller: 'MeetingIntervalCtrl',
                label: 'Meeting Interval Edit',
                data: {
                    pageTitle: 'Meeting Interval Edit'
                }
            })
            .when('/associatedbanks-list', {
                templateUrl: 'partials/associatedbank',
                controller: 'AssociatedBankCtrl',
                label: 'Associated Banks List',
                data: {
                    pageTitle: 'Associated Banks List'
                }
            })
            .when('/associatedbank/create', {
                templateUrl: 'partials/associatedbank',
                controller: 'AssociatedBankCtrl',
                label: 'Associated Bank Create',
                data: {
                    pageTitle: 'Associated Bank Create'
                }
            })
            .when('/associatedbank/edit/:id', {
                templateUrl: 'partials/associatedbank',
                controller: 'AssociatedBankCtrl',
                label: 'Associated Bank Edit',
                data: {
                    pageTitle: 'Associated Bank Edit'
                }
            })
            .when('/typologies', {
                templateUrl: 'partials/typology',
                controller: 'TypCtrl'
            })
            .when('/typologies/create', {
                templateUrl: 'partials/typology',
                controller: 'TypCtrl'
            })
            .when('/typologies/:id', {
                templateUrl: 'partials/typology',
                controller: 'TypCtrl'
            })
            .when('/education-list', {
                templateUrl: 'partials/education',
                controller: 'EduCtrl',
                label: 'Educations List',
                data: {
                    pageTitle: 'Educations List'
                }
            })
            .when('/education/create', {
                templateUrl: 'partials/education',
                controller: 'EduCtrl',
                label: 'Education Create',
                data: {
                    pageTitle: 'Home'
                }
            })
            .when('/education/edit/:id', {
                templateUrl: 'partials/education',
                controller: 'EduCtrl',
                label: 'Education Edit',
                data: {
                    pageTitle: 'Education Edit'
                }
            })
            .when('/maritalstatus', {
                templateUrl: 'partials/maritalstatus',
                controller: 'MaryCtrl',
                label: 'Marital Statuses List',
                data: {
                    pageTitle: 'Marital Statuses List'
                }
            })
            .when('/maritalstatus/create', {
                templateUrl: 'partials/maritalstatus',
                controller: 'MaryCtrl',
                label: 'Marital Status Create',
                data: {
                    pageTitle: 'Marital Status Create'
                }
            })
            .when('/maritalstatus/:id', {
                templateUrl: 'partials/maritalstatus',
                controller: 'MaryCtrl',
                label: 'Marital Status Edit',
                data: {
                    pageTitle: 'Marital Status Edit'
                }
            })
            .when('/employmentstatuses', {
                templateUrl: 'partials/employmentstatus',
                controller: 'EmpstCtrl',
                label: 'Employment Statuses List',
                data: {
                    pageTitle: 'Employment Statuses List'
                }
            })
            .when('/employmentstatuses/create', {
                templateUrl: 'partials/employmentstatus',
                controller: 'EmpstCtrl',
                label: 'Employment Status Create',
                data: {
                    pageTitle: 'Employment Status Create'
                }
            })
            .when('/employmentstatuses/:id', {
                templateUrl: 'partials/employmentstatus',
                controller: 'EmpstCtrl',
                label: 'Employment Status Edit',
                data: {
                    pageTitle: 'Employment Status Edit'
                }
            })
            .when('/physicalappearances', {
                templateUrl: 'partials/physicalappearance',
                controller: 'PhyApCtrl'
            })
            .when('/physicalappearances/create', {
                templateUrl: 'partials/physicalappearance',
                controller: 'PhyApCtrl'
            })
            .when('/physicalappearances/:id', {
                templateUrl: 'partials/physicalappearance',
                controller: 'PhyApCtrl'
            })
            .when('/emotionalstates', {
                templateUrl: 'partials/emotionalstate',
                controller: 'EmotionCtrl'
            })
            .when('/emotionalstates/create', {
                templateUrl: 'partials/emotionalstate',
                controller: 'EmotionCtrl'
            })
            .when('/emotionalstates/:id', {
                templateUrl: 'partials/emotionalstate',
                controller: 'EmotionCtrl'
            })
            .when('/cocategories', {
                templateUrl: 'partials/cocategories',
                controller: 'COCategoriesCtrl'
            })
            .when('/cocategories/create', {
                templateUrl: 'partials/cocategories',
                controller: 'COCategoriesCtrl'
            })
            .when('/cocategories/:id', {
                templateUrl: 'partials/cocategories',
                controller: 'COCategoriesCtrl'
            })
            .when('/reportincident-list', {
                templateUrl: 'partials/reportincident-view',
                controller: 'ReportIncidentListCtrl',
                label: 'Report Incident List',
                data: {
                    pageTitle: 'Report Incident List'
                }
            })
            .when('/reportincident/create', {
                templateUrl: 'partials/reportincident',
                controller: 'ReportIncidentCreateCtrl',
                label: 'Add Incident',
                data: {
                    pageTitle: 'Add Incident'
                }
            })
            .when('/reportincident/edit/:id', {
                templateUrl: 'partials/reportincident',
                controller: 'ReportIncidentEditCtrl',
                label: 'Edit Incident',
                data: {
                    pageTitle: 'Edit Incident'
                }
            })
            .when('/socialprotection', {
                templateUrl: 'partials/socialprotection',
                controller: 'SocialProtectionCtrl'
            })
            .when('/health', {
                templateUrl: 'partials/health',
                controller: 'HealthCtrl'
            })
            .when('/outreachactivity', {
                templateUrl: 'partials/outreachactivity',
                controller: 'OutReachActivityCtrl'
            })
            .when('/codashboard', {
                templateUrl: 'partials/codashboard',
                controller: 'CODashboardCtrl',
                label: 'CO Dashboard',
                data: {
                    pageTitle: 'CO Dashboard'
                }
            })
            .when('/applicationstage', {
                templateUrl: 'partials/applicationstages',
                controller: 'applicationstageCtrl'
            })
            .when('/applicationstage/create', {
                templateUrl: 'partials/applicationstages',
                controller: 'applicationstageCtrl'
            })
            .when('/applicationstage/:id', {
                templateUrl: 'partials/applicationstages',
                controller: 'applicationstageCtrl'
            })
            .when('/schemestage-list', {
                templateUrl: 'partials/schemestage',
                controller: 'schemestageCtrl',
                label: 'Scheme Stages List',
                data: {
                    pageTitle: 'Scheme Stages List'
                }
            })
            .when('/schemestage/create', {
                templateUrl: 'partials/schemestage',
                controller: 'schemestageCtrl',
                label: 'Scheme Stage Create',
                data: {
                    pageTitle: 'Scheme Stage Create'
                }
            })
            .when('/schemestage/edit/:id', {
                templateUrl: 'partials/schemestage',
                controller: 'schemestageCtrl',
                label: 'Scheme Stage Edit',
                data: {
                    pageTitle: 'Scheme Stage Edit'
                }
            })
            .when('/coactivity', {
                templateUrl: 'partials/coactivity',
                controller: 'BnCreateCtrl'
            })
            .when('/resetpassword', {
                templateUrl: 'partials/resetpassword',
                controller: 'ResetPswdCtrl',
                label: 'Reset Password'
            })
            .when('/schemes-list', {
                templateUrl: 'partials/schememasters',
                controller: 'SchemeMastersCtrl',
                label: 'Schemes List',
                data: {
                    pageTitle: 'Schemes List'
                }
            })
            .when('/scheme/create', {
                templateUrl: 'partials/schememasters',
                controller: 'SchemeMastersCtrl',
                label: 'Scheme Create',
                data: {
                    pageTitle: 'Scheme Create'
                }
            })
            .when('/scheme/edit/:id', {
                templateUrl: 'partials/schememasters',
                controller: 'SchemeMastersEditCtrl',
                label: 'Scheme Edit',
                data: {
                    pageTitle: 'Scheme Edit'
                }
            })
            .when('/fwarchieve', {
                templateUrl: 'partials/fieldworkerarchieve',
                controller: 'FWArchieveCtrl',
                label: 'Archived Members',
                data: {
                    pageTitle: 'Archived Members'
                }
            })
            .when('/coarchieve', {
                templateUrl: 'partials/coarchieve',
                controller: 'COArchieveCtrl',
                label: 'Archived Member',
                data: {
                    pageTitle: 'Archived Members'
                }
            })
            .when('/mysites', {
                templateUrl: 'partials/fieldworkersites',
                controller: 'FWSitesCtrl',
                label: 'Site List',
                data: {
                    pageTitle: 'Sites'
                }
            })
            .when('/groupmeetings', {
                templateUrl: 'partials/groupmeetings',
                controller: 'GroupMeetingsCtrl',
                label: 'Group Meeting List',
                data: {
                    pageTitle: 'Group Meetings'
                }
            })
            .when('/groupmeetings/create', {
                templateUrl: 'partials/groupmeetings',
                controller: 'GroupMeetingsCtrl',
                label: 'Group Meeting Create',
                data: {
                    pageTitle: 'Group Meetings'
                }
            })
            .when('/groupmeetings/:id', {
                templateUrl: 'partials/groupmeetings',
                controller: 'EditGroupMeetingsCtrl',
                label: 'Group Meeting Edit',
                data: {
                    pageTitle: 'Group Meetings'
                }
            })
            .when('/events', {
                templateUrl: 'partials/events',
                controller: 'EventsCtrl',
                label: 'Event List',
                data: {
                    pageTitle: 'Events'
                }
            })
            .when('/events/create', {
                templateUrl: 'partials/events',
                controller: 'EventsCtrl',
                label: 'Event Create',
                data: {
                    pageTitle: 'Events'
                }
            })
            .when('/events/:id', {
                templateUrl: 'partials/events',
                controller: 'EditEventsCtrl',
                label: 'Event Edit',
                data: {
                    pageTitle: 'Events'
                }
            })
            .when('/applyforschemes', {
                templateUrl: 'partials/applyforschemehome',
                controller: 'ApplyForSchemeHomeCtrl',
                label: 'Apply For Schemes',
                data: {
                    pageTitle: 'Apply For Schemes'
                }
            })
            .when('/applyforscheme/create', {
                templateUrl: 'partials/applyforscheme',
                controller: 'ApplyForSchemeCtrl',
                label: 'Apply For Scheme Create',
                data: {
                    pageTitle: 'Apply For Scheme Create'
                }
            })
            .when('/applyforscheme/edit/:id', {
                templateUrl: 'partials/applyforscheme',
                controller: 'EditSchemeCtrl',
                label: 'Apply For Scheme Edit',
                data: {
                    pageTitle: 'Apply For Scheme Edit'
                }
            })
            .when('/applyfordocuments', {
                templateUrl: 'partials/applyfordocumenthome',
                controller: 'ApplyForDocumentHomeCtrl',
                label: 'Apply For Documents',
                data: {
                    pageTitle: 'Apply For Documents'
                }
            })
            .when('/applyfordocument/create', {
                templateUrl: 'partials/applyfordocument',
                controller: 'ApplyForDocumentCtrl',
                label: 'Apply For Document Create',
                data: {
                    pageTitle: 'Apply For Document Create'
                }
            })
            .when('/applyfordocument/edit/:id', {
                templateUrl: 'partials/applyfordocument',
                controller: 'EditDocumentCtrl',
                label: 'Apply For Document Edit',
                data: {
                    pageTitle: 'Apply For Document Edit'
                }
            })
            .when('/onetoone/:id', {
                templateUrl: 'partials/onetoone1',
                controller: 'OneToOneCtrl',
                label: 'OnetoOne',
                data: {
                    pageTitle: 'One To One'
                }
            })
            /*  .when('/surveyquestions', {
                  templateUrl: 'partials/surveyquestions',
                  controller: 'SurveyQuestionsCtrl'
              })
              .when('/surveyquestions/create', {
                  templateUrl: 'partials/surveyquestions',
                  controller: 'SurveyQuestionsCtrl'
              })
              .when('/surveyquestions/:id', {
                  templateUrl: 'partials/surveyquestions',
                  controller: 'SurveyQuestionsCtrl'
              })*/
            .when('/pillars', {
                templateUrl: 'partials/pillars',
                controller: 'PillarsCtrl'
            })
            .when('/pillars/create', {
                templateUrl: 'partials/pillars',
                controller: 'PillarsCtrl'
            })
            .when('/pillars/:id', {
                templateUrl: 'partials/pillars',
                controller: 'PillarsCtrl'
            })
            .when('/groupmeetingtopics', {
                templateUrl: 'partials/groupmeetingtopic',
                controller: 'groupmeetingtopicCtrl'
            })
            .when('/groupmeetingtopics/create', {
                templateUrl: 'partials/groupmeetingtopic',
                controller: 'groupmeetingtopicCtrl'
            })
            .when('/groupmeetingtopics/:id', {
                templateUrl: 'partials/groupmeetingtopic',
                controller: 'groupmeetingtopicCtrl'
            })
            .when('/groupmeetingevents', {
                templateUrl: 'partials/groupmeetingevent',
                controller: 'groupmeetingeventCtrl'
            })
            .when('/groupmeetingevents/create', {
                templateUrl: 'partials/groupmeetingevent',
                controller: 'groupmeetingeventCtrl'
            })
            .when('/groupmeetingevents/:id', {
                templateUrl: 'partials/groupmeetingevent',
                controller: 'groupmeetingeventCtrl'
            })
            .when('/stakeholdermeetings', {
                templateUrl: 'partials/stakeholder',
                controller: 'StakeHolderCtrl',
                label: 'Stakeholder Meeting List',
                data: {
                    pageTitle: 'Stakeholder Meetings'
                }
            })
            .when('/stakeholdermeetings/create', {
                templateUrl: 'partials/stakeholder',
                controller: 'StakeHolderCtrl',
                label: 'Stakeholder Meeting Create',
                data: {
                    pageTitle: 'Stakeholder Meetings'
                }
            })
            .when('/stakeholdermeetings/:id', {
                templateUrl: 'partials/stakeholder',
                controller: 'EditStakeHolderCtrl',
                label: 'Stakeholder Meeting Edit',
                data: {
                    pageTitle: 'Stakeholder Meetings'
                }
            })
            .when('/eventtypes', {
                templateUrl: 'partials/eventtypes',
                controller: 'EventTypesCtrl'
            })
            .when('/eventtypes/create', {
                templateUrl: 'partials/eventtypes',
                controller: 'EventTypesCtrl'
            })
            .when('/eventtypes/:id', {
                templateUrl: 'partials/eventtypes',
                controller: 'EventTypesCtrl'
            })
            .when('/followuptypes', {
                templateUrl: 'partials/followuptypes',
                controller: 'FollowUpTypesCtrl'
            })
            .when('/followuptypes/create', {
                templateUrl: 'partials/followuptypes',
                controller: 'FollowUpTypesCtrl'
            })
            .when('/followuptypes/:id', {
                templateUrl: 'partials/followuptypes',
                controller: 'FollowUpTypesCtrl'
            })
            .when('/bulkupdates', {
                templateUrl: 'partials/bulkupdate',
                controller: 'BulkUpCtrl',
                label: 'Bulkupdate List',
                data: {
                    pageTitle: 'Bulk Updates'
                }
            })
            .when('/bulkupdates/create', {
                templateUrl: 'partials/bulkupdate',
                controller: 'BulkUpCtrl',
                label: 'Bulkupdate Create',
                data: {
                    pageTitle: 'Bulk Updates'
                }
            })
            .when('/bulkupdates/:id', {
                templateUrl: 'partials/bulkupdate',
                controller: 'BulkUPEditCtrl',
                label: 'Bulkupdate Edit',
                data: {
                    pageTitle: 'Bulk Updates'
                }
            })
            .when('/todotypes', {
                templateUrl: 'partials/todotype',
                controller: 'ToDoTypesCtrl'
            })
            .when('/todotypes/create', {
                templateUrl: 'partials/todotype',
                controller: 'ToDoTypesCtrl'
            })
            .when('/todotypes/:id', {
                templateUrl: 'partials/todotype',
                controller: 'ToDoTypesCtrl'
            })
            .when('/todostatuses', {
                templateUrl: 'partials/todostatuses',
                controller: 'ToDoStatusesCtrl'
            })
            .when('/todostatuses/create', {
                templateUrl: 'partials/todostatuses',
                controller: 'ToDoStatusesCtrl'
            })
            .when('/todostatuses/:id', {
                templateUrl: 'partials/todostatuses',
                controller: 'ToDoStatusesCtrl'
            })
            .when('/todofilters', {
                templateUrl: 'partials/mytodostatuses',
                controller: 'MyToDoStatusesCtrl'
            })
            .when('/todofilters/create', {
                templateUrl: 'partials/mytodostatuses',
                controller: 'MyToDoStatusesCtrl'
            })
            .when('/todofilters/:id', {
                templateUrl: 'partials/mytodostatuses',
                controller: 'MyToDoStatusesCtrl'
            })
            .when('/workflows', {
                templateUrl: 'partials/workflow',
                controller: 'WorkFlowCtrl'
            })
            .when('/workflows/create', {
                templateUrl: 'partials/workflow',
                controller: 'WorkFlowCtrl'
            })
            .when('/workflows/:id', {
                templateUrl: 'partials/workflow',
                controller: 'WorkFlowCtrl'
            })
            .when('/trackapplications', {
                templateUrl: 'partials/trackapplications',
                controller: 'TrackApplicationCtrl',
                label: 'Applications',
                data: {
                    pageTitle: 'Applications'
                }
            })
            .when('/stakeholdertypes', {
                templateUrl: 'partials/stakeholdertype',
                controller: 'stakeholdertypesCtrl'
            })
            .when('/stakeholdertypes/create', {
                templateUrl: 'partials/stakeholdertype',
                controller: 'stakeholdertypesCtrl'
            })
            .when('/stakeholdertypes/:id', {
                templateUrl: 'partials/stakeholdertype',
                controller: 'stakeholdertypesCtrl'
            })
            /********new added*******/
            .when('/financialgoal', {
                templateUrl: 'partials/financialgoal',
                controller: 'FinancialGoalCtrl'
            })
            .when('/financialgoal/create', {
                templateUrl: 'partials/financialgoal',
                controller: 'FinancialGoalCtrl'
            })
            .when('/financialgoal/:id', {
                templateUrl: 'partials/financialgoal',
                controller: 'FinancialGoalCtrl'
            })
            .when('/timeinyear', {
                templateUrl: 'partials/timeinyear',
                controller: 'TimeinYearCtrl'
            })
            .when('/timeinyear/create', {
                templateUrl: 'partials/timeinyear',
                controller: 'TimeinYearCtrl'
            })
            .when('/timeinyear/:id', {
                templateUrl: 'partials/timeinyear',
                controller: 'TimeinYearCtrl'
            })
            .when('/budgetyear', {
                templateUrl: 'partials/budgetyear',
                controller: 'BudgetYearCtrl'
            })
            .when('/budgetyear/create', {
                templateUrl: 'partials/budget',
                controller: 'BudgetYearCtrl'
            })
            .when('/budgetyear/:id', {
                templateUrl: 'partials/budgetyear',
                controller: 'BudgetYearCtrl'
            })
            .when('/loantracking', {
                templateUrl: 'partials/loantracking',
                controller: 'LoanTrackingCtrl'
            })
            .when('/loantracking/create', {
                templateUrl: 'partials/loantracking',
                controller: 'LoanTrackingCtrl'
            })
            .when('/loantracking/:id', {
                templateUrl: 'partials/loantracking',
                controller: 'LoanTrackingCtrl'
            })
            /***********************/
            .when('/followupstacks', {
                templateUrl: 'partials/followupstack',
                controller: 'followupstacksCtrl'
            })
            .when('/followupstacks/create', {
                templateUrl: 'partials/followupstack',
                controller: 'followupstacksCtrl'
            })
            .when('/followupstacks/:id', {
                templateUrl: 'partials/followupstack',
                controller: 'followupstacksCtrl'
            })
            .when('/followupbulkupdates', {
                templateUrl: 'partials/followupbulkupdate',
                controller: 'followupbulkupdatesCtrl'
            })
            .when('/followupbulkupdates/create', {
                templateUrl: 'partials/followupbulkupdate',
                controller: 'followupbulkupdatesCtrl'
            })
            .when('/followupbulkupdates/:id', {
                templateUrl: 'partials/followupbulkupdate',
                controller: 'followupbulkupdatesCtrl'
            })
            .when('/purposeofmeetings', {
                templateUrl: 'partials/purposeofmeeting',
                controller: 'purposeofmeetingsCtrl'
            })
            .when('/purposeofmeetings/create', {
                templateUrl: 'partials/purposeofmeeting',
                controller: 'purposeofmeetingsCtrl'
            })
            .when('/purposeofmeetings/:id', {
                templateUrl: 'partials/purposeofmeeting',
                controller: 'purposeofmeetingsCtrl'
            })
            .when('/followupgroups', {
                templateUrl: 'partials/followupgroup',
                controller: 'followupgroupsCtrl'
            })
            .when('/followupgroups/create', {
                templateUrl: 'partials/followupgroup',
                controller: 'followupgroupsCtrl'
            })
            .when('/followupgroups/:id', {
                templateUrl: 'partials/followupgroup',
                controller: 'followupgroupsCtrl'
            })
            .when('/fwdashboard', {
                templateUrl: 'partials/fwdashboard',
                controller: 'CODashboardCtrl',
                label: 'FW Dashboard',
                data: {
                    pageTitle: 'FW Dashboard'
                }
            })
            .when('/memberdatamigration', {
                templateUrl: 'partials/memberdatamigration',
                controller: 'memberdatamigrationCtrl'
            })
            .when('/uploadfacility', {
                templateUrl: 'partials/uploadfacility',
                controller: 'FacilityReadXlsCtrl',
                label: 'Facility',
                data: {
                    pageTitle: 'Facility'
                }
            })
            .when('/uploadscheme', {
                templateUrl: 'partials/uploadscheme',
                controller: 'SchemeReadXlsCtrl',
                label: 'Scheme',
                data: {
                    pageTitle: 'Scheme'
                }
            })
            .when('/fieldworkers/:id', {
                templateUrl: 'partials/f-work-form',
                controller: 'FWEditCtrl',
                label: 'Field Work Profile Edit',
                data: {
                    pageTitle: 'Field Work Profile'
                }
            })
            .when('/facilityprofileedit', {
                templateUrl: 'partials/facilityprofileedit',
                controller: 'FacilityProfileEditCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/nodashboard', {
                templateUrl: 'partials/nodashboard',
                controller: 'CODashboardCtrl',
                label: 'NO Dashboard',
                data: {
                    pageTitle: 'NO Dashboard'
                }
            })
            .when('/rodashboard', {
                templateUrl: 'partials/rodashboard',
                controller: 'CODashboardCtrl',
                label: 'RO Dashboard',
                data: {
                    pageTitle: 'RO Dashboard'
                }
            })
            .when('/spmdashboard', {
                templateUrl: 'partials/spmdashboard',
                controller: 'CODashboardCtrl',
                label: 'SPM Dashboard',
                data: {
                    pageTitle: 'SPM Dashboard'
                }
            })
            .when('/fsmentordashboard', {
                templateUrl: 'partials/fsmentordashboard',
                controller: 'CODashboardCtrl',
                label: 'FS Mentor Dashboard',
                data: {
                    pageTitle: 'FS Mentor Dashboard'
                }
            })
            .when('/mentor', {
                templateUrl: 'partials/mentor',
                controller: 'MentorCtrl'
            })
            .when('/mentor/create', {
                templateUrl: 'partials/mentor-form',
                controller: 'MentorCreateCtrl'
            })
            .when('/mentor/:id', {
                templateUrl: 'partials/mentor-form',
                controller: 'MentorCreateCtrl'
            })
            .when('/mentorassign', {
                templateUrl: 'partials/mentorassign',
                controller: 'MentorAssignCtrl'
            })
            .when('/mentorassign/create', {
                templateUrl: 'partials/mentorassign',
                controller: 'MentorAssignCtrl'
            })
            .when('/schemedepartment', {
                templateUrl: 'partials/schemedepartments',
                controller: 'SchemeDeptCtrl'
            })
            .when('/schemedepartment/create', {
                templateUrl: 'partials/schemedepartments',
                controller: 'SchemeDeptCtrl'
            })
            .when('/schemedepartment/:id', {
                templateUrl: 'partials/schemedepartments',
                controller: 'SchemeDeptCtrl'
            })
            .when('/agegroup-list', {
                templateUrl: 'partials/agegroups',
                controller: 'AgeGroupCtrl',
                label: 'Age Groups List',
                data: {
                    pageTitle: 'Age Groups List'
                }
            })
            .when('/agegroup/create', {
                templateUrl: 'partials/agegroups',
                controller: 'AgeGroupCtrl',
                label: 'Age Group Create',
                data: {
                    pageTitle: 'Age Group Create'
                }
            })
            .when('/agegroup/edit/:id', {
                templateUrl: 'partials/agegroups',
                controller: 'AgeGroupCtrl',
                label: 'Age Group Edit',
                data: {
                    pageTitle: 'Age Group Edit'
                }
            })
            .when('/healthstatus-list', {
                templateUrl: 'partials/healthstatuses',
                controller: 'HealthStatusCtrl',
                label: 'Health Statuses List',
                data: {
                    pageTitle: 'Health Statuses List'
                }
            })
            .when('/healthstatus/create', {
                templateUrl: 'partials/healthstatuses',
                controller: 'HealthStatusCtrl',
                label: 'Health Status Create',
                data: {
                    pageTitle: 'Health Status Create'
                }
            })
            .when('/healthstatus/edit/:id', {
                templateUrl: 'partials/healthstatuses',
                controller: 'HealthStatusCtrl',
                label: 'Health Status Edit',
                data: {
                    pageTitle: 'Health Status Edit'
                }
            })
            .when('/minoritystatus-list', {
                templateUrl: 'partials/minoritystatuses',
                controller: 'MinorityStatusCtrl',
                label: 'Minority Statuses List',
                data: {
                    pageTitle: 'Minority Statuses List'
                }
            })
            .when('/minoritystatus/create', {
                templateUrl: 'partials/minoritystatuses',
                controller: 'MinorityStatusCtrl',
                label: 'Minority Status Create',
                data: {
                    pageTitle: 'Minority Status Create'
                }
            })
            .when('/minoritystatus/edit/:id', {
                templateUrl: 'partials/minoritystatuses',
                controller: 'MinorityStatusCtrl',
                label: 'Minority Status Edit',
                data: {
                    pageTitle: 'Monority Status Edit'
                }
            })
            .when('/socialstatus-list', {
                templateUrl: 'partials/socialstatuses',
                controller: 'SocialStatusCtrl',
                label: 'Social Status List',
                data: {
                    pageTitle: 'Social Status List'
                }
            })
            .when('/socialstatus/create', {
                templateUrl: 'partials/socialstatuses',
                controller: 'SocialStatusCtrl',
                label: 'Social Status Create',
                data: {
                    pageTitle: 'Social Status Create'
                }
            })
            .when('/socialstatus/edit/:id', {
                templateUrl: 'partials/socialstatuses',
                controller: 'SocialStatusCtrl',
                label: 'Social Status Edit',
                data: {
                    pageTitle: 'Social Status Edit'
                }
            })
            .when('/occupationstatus-list', {
                templateUrl: 'partials/occupationstatuses',
                controller: 'OccupationStatusCtrl',
                label: 'Occupation Statuses List',
                data: {
                    pageTitle: 'Occupation Statuses List'
                }
            })
            .when('/occupationstatus/create', {
                templateUrl: 'partials/occupationstatuses',
                controller: 'OccupationStatusCtrl',
                label: 'Occupation Status Create',
                data: {
                    pageTitle: 'Occupation Status Create'
                }
            })
            .when('/occupationstatus/edit/:id', {
                templateUrl: 'partials/occupationstatuses',
                controller: 'OccupationStatusCtrl',
                label: 'Occupation Status Edit',
                data: {
                    pageTitle: 'Occupation Status Edit'
                }
            })
            .when('/locationtype-list', {
                templateUrl: 'partials/locationtypes',
                controller: 'LocationTypeCtrl',
                label: 'Location Types List',
                data: {
                    pageTitle: 'Location Types List'
                }
            })
            .when('/locationtype/create', {
                templateUrl: 'partials/locationtypes',
                controller: 'LocationTypeCtrl',
                label: 'Location Type Create',
                data: {
                    pageTitle: 'Location Type Create'
                }
            })
            .when('/locationtype/edit/:id', {
                templateUrl: 'partials/locationtypes',
                controller: 'LocationTypeCtrl',
                label: 'Location Type Edit',
                data: {
                    pageTitle: 'Location Type edit'
                }
            })
            .when('/incomestatus-list', {
                templateUrl: 'partials/incomestatuses',
                controller: 'IncomeStatusCtrl',
                label: 'Income Statuses List',
                data: {
                    pageTitle: 'Income Statuses List'
                }
            })
            .when('/incomestatus/create', {
                templateUrl: 'partials/incomestatuses',
                controller: 'IncomeStatusCtrl',
                label: 'Income Status Create',
                data: {
                    pageTitle: 'Income Status Create'
                }
            })
            .when('/incomestatus/edit/:id', {
                templateUrl: 'partials/incomestatuses',
                controller: 'IncomeStatusCtrl',
                label: 'Income Status Edit',
                data: {
                    pageTitle: 'Income Status Edit'
                }
            })
            .when('/schemecategory-list', {
                templateUrl: 'partials/schemecategories',
                controller: 'SchemeCategCtrl',
                label: 'Scheme Categories List',
                data: {
                    pageTitle: 'Scheme Categories List'
                }
            })
            .when('/schemecategory/create', {
                templateUrl: 'partials/schemecategories',
                controller: 'SchemeCategCtrl',
                label: 'Scheme Category Create',
                data: {
                    pageTitle: 'Scheme Category Create'
                }
            })
            .when('/schemecategory/edit/:id', {
                templateUrl: 'partials/schemecategories',
                controller: 'SchemeCategCtrl',
                label: 'Scheme Category Edit',
                data: {
                    pageTitle: 'Scheme Category Edit'
                }
            })
            .when('/schemetype-list', {
                templateUrl: 'partials/schemetype',
                controller: 'SchemeTypeCtrl',
                label: 'Scheme Types List',
                data: {
                    pageTitle: 'Scheme Types List'
                }
            })
            .when('/schemetype/create', {
                templateUrl: 'partials/schemetype',
                controller: 'SchemeTypeCtrl',
                label: 'Scheme Type Create',
                data: {
                    pageTitle: 'Scheme Type Create'
                }
            })
            .when('/schemetype/edit/:id', {
                templateUrl: 'partials/schemetype',
                controller: 'SchemeTypeCtrl',
                label: 'Scheme Type Edit',
                data: {
                    pageTitle: 'Scheme Type Edit'
                }
            })
            .when('/mentor', {
                templateUrl: 'partials/mentor',
                controller: 'MentorCtrl'
            })
            .when('/mentor/create', {
                templateUrl: 'partials/mentor-form',
                controller: 'MentorCreateCtrl'
            })
            .when('/mentor/:id', {
                templateUrl: 'partials/mentor-form',
                controller: 'MentorCreateCtrl'
            })
            .when('/mentorassign', {
                templateUrl: 'partials/mentorassign',
                controller: 'MentorAssignCtrl'
            })
            .when('/mentorassign/create', {
                templateUrl: 'partials/mentorassign',
                controller: 'MentorAssignCtrl'
            })
            .when('/upload-co-organisation', {
                templateUrl: 'partials/upload-co-org',
                controller: 'CoOrgReadXlsCtrl'
            })
            .when('/followupreportincident', {
                templateUrl: 'partials/followupreportincident',
                controller: 'followupreportincidentCtrl'
            })
            .when('/followupreportincident/create', {
                templateUrl: 'partials/followupreportincident',
                controller: 'followupreportincidentCtrl'
            })
            .when('/followupreportincident/:id', {
                templateUrl: 'partials/followupreportincident',
                controller: 'followupreportincidentCtrl'
            })
            .when('/reasonforrejection-list', {
                templateUrl: 'partials/reasonforrejection',
                controller: 'ReasonForRejectionCtrl',
                abel: 'Reason For Rejection List',
                data: {
                    pageTitle: 'Reason For Rejection List'
                }
            })
            .when('/reasonforrejection/edit/:id', {
                templateUrl: 'partials/reasonforrejection',
                controller: 'ReasonForRejectionCtrl',
                label: 'Reason For Rejection Edit',
                data: {
                    pageTitle: 'Reason For Rejection Edit'
                }
            })
            .when('/reasonforrejection/create', {
                templateUrl: 'partials/reasonforrejection',
                controller: 'ReasonForRejectionCtrl',
                label: 'Reason For Rejection Create',
                data: {
                    pageTitle: 'Reason For Rejection Create'
                }
            })
            .when('/reasonfordelay-list', {
                templateUrl: 'partials/reasonfordelay',
                controller: 'ReasonForDelayCtrl',
                abel: 'Reason For Delay List',
                data: {
                    pageTitle: 'Reason For Delay List'
                }
            })
            .when('/reasonfordelay/edit/:id', {
                templateUrl: 'partials/reasonfordelay',
                controller: 'ReasonForDelayCtrl',
                label: 'Reason For Delay Edit',
                data: {
                    pageTitle: 'Reason For Delay Edit'
                }
            })
            .when('/reasonfordelay/create', {
                templateUrl: 'partials/reasonfordelay',
                controller: 'ReasonForDelayCtrl',
                label: 'Reason For Delay Create',
                data: {
                    pageTitle: 'Reason For Delay Create'
                }
            })
            .when('/severityofincident-list', {
                templateUrl: 'partials/servityofincident',
                controller: 'SeverityOfIncidentCtrl',
                abel: 'Severity of Incident List',
                data: {
                    pageTitle: 'Severity of Incident List'
                }
            })
            .when('/severityofincident/edit/:id', {
                templateUrl: 'partials/servityofincident',
                controller: 'SeverityOfIncidentCtrl',
                label: 'Severity of Incident Edit',
                data: {
                    pageTitle: 'Severity of Incident Edit'
                }
            })
            .when('/severityofincident/create', {
                templateUrl: 'partials/servityofincident',
                controller: 'SeverityOfIncidentCtrl',
                label: 'Severity of Incident Create',
                data: {
                    pageTitle: 'Severity of Incident Create'
                }
            })
            .when('/currentstatusofcase-list', {
                templateUrl: 'partials/currentstatusofcase',
                controller: 'CurrentStatusOfCaseCtrl',
                abel: 'Current status of the case List',
                data: {
                    pageTitle: 'Current status of the case List'
                }
            })
            .when('/currentstatusofcase/edit/:id', {
                templateUrl: 'partials/currentstatusofcase',
                controller: 'CurrentStatusOfCaseCtrl',
                label: 'Current status of the case Edit',
                data: {
                    pageTitle: 'Current status of the case Edit'
                }
            })
            .when('/currentstatusofcase/create', {
                templateUrl: 'partials/currentstatusofcase',
                controller: 'CurrentStatusOfCaseCtrl',
                label: 'Current status of the case Create',
                data: {
                    pageTitle: 'Current status of the case Create'
                }
            })
            .when('/reportincidentfollowup-list', {
                templateUrl: 'partials/reportincidentfollowup',
                controller: 'ReportIncidentFollowupCtrl',
                label: 'Report Incident Followup List',
                data: {
                    pageTitle: 'Report Incident Followup List'
                }
            })
            .when('/reportincidentfollowup/create', {
                templateUrl: 'partials/reportincidentfollowup',
                controller: 'ReportIncidentFollowupCtrl',
                label: 'Report Incident Followup Create',
                data: {
                    pageTitle: 'Report Incident Followup Create'
                }
            })
            .when('/reportincidentfollowup/edit/:id', {
                templateUrl: 'partials/reportincidentfollowup',
                controller: 'ReportIncidentFollowupCtrl',
                label: 'Report Incident Followup Edit',
                data: {
                    pageTitle: 'Report Incident Followup Edit'
                }
            })
            .when('/typeofincident-list', {
                templateUrl: 'partials/typeofincident',
                controller: 'TypeOfIncidentCtrl',
                label: 'Type Of Incident List',
                data: {
                    pageTitle: 'Type Of Incident List'
                }
            })
            .when('/typeofincident/create', {
                templateUrl: 'partials/typeofincident',
                controller: 'TypeOfIncidentCtrl',
                label: 'Type Of Incident Create',
                data: {
                    pageTitle: 'Type Of Incident Create'
                }
            })
            .when('/typeofincident/edit/:id', {
                templateUrl: 'partials/typeofincident',
                controller: 'TypeOfIncidentCtrl',
                label: 'Type Of Incident Edit',
                data: {
                    pageTitle: 'Type Of Incident Edit'
                }
            })
            .when('/isnew-list', {
                templateUrl: 'partials/isnew',
                controller: 'NewCtrl',
                label: 'Is New List',
                data: {
                    pageTitle: 'Is New List'
                }
            })
            .when('/isnew/create', {
                templateUrl: 'partials/isnew',
                controller: 'NewCtrl',
                label: 'Is New Create',
                data: {
                    pageTitle: 'Is New Create'
                }
            })
            .when('/isnew/edit/:id', {
                templateUrl: 'partials/isnew',
                controller: 'NewCtrl',
                label: 'Is New Edit',
                data: {
                    pageTitle: 'Is New Edit'
                }
            })
            .when('/organisebystakeholder', {
                templateUrl: 'partials/organisebystakeholder',
                controller: 'organisebystakeholderCtrl',
                abel: 'Organised by Stakeholder',
                data: {
                    pageTitle: 'Organised by Stakeholder'
                }
            })
            .when('/organisebystakeholder/:id', {
                templateUrl: 'partials/organisebystakeholder',
                controller: 'organisebystakeholderCtrl',
                label: 'Organised by Stakeholder',
                data: {
                    pageTitle: 'Organised by Stakeholder'
                }
            })
            .when('/organisebystakeholder/create', {
                templateUrl: 'partials/organisebystakeholder',
                controller: 'organisebystakeholderCtrl',
                label: 'Organised by Stakeholder',
                data: {
                    pageTitle: 'Organised by Stakeholder'
                }
            })
            .when('/responsereceive-list', {
                templateUrl: 'partials/responsereceive',
                controller: 'ResponseReceiveCtrl',
                label: 'Response Receive List',
                data: {
                    pageTitle: 'Response Receive List'
                }
            })
            .when('/responsereceive/edit/:id', {
                templateUrl: 'partials/responsereceive',
                controller: 'ResponseReceiveCtrl',
                label: 'Response Receive Edit',
                data: {
                    pageTitle: 'Response Receive Edit'
                }
            })
            .when('/responsereceive/create', {
                templateUrl: 'partials/responsereceive',
                controller: 'ResponseReceiveCtrl',
                label: 'Response Receive Create',
                data: {
                    pageTitle: 'Response Receive Create'
                }
            })
            .when('/documenttype-list', {
                templateUrl: 'partials/documenttype',
                controller: 'DocumentTypeCtrl',
                label: 'Document Types',
                data: {
                    pageTitle: 'Document Types'
                }
            })
            .when('/documenttype/create', {
                templateUrl: 'partials/documenttype',
                controller: 'DocumentTypeCtrl',
                label: 'Document Type Create',
                data: {
                    pageTitle: 'Document Type Create'
                }
            })
            .when('/documenttype/edit/:id', {
                templateUrl: 'partials/documenttype',
                controller: 'DocumentTypeCtrl',
                label: 'Document Type Edit',
                data: {
                    pageTitle: 'Document Type edit'
                }
            })
            .when('/english', {
                templateUrl: 'partials/language-english',
                controller: 'EnglishCtrl'
            })
            .when('/hindi', {
                templateUrl: 'partials/language-hindi',
                controller: 'HindiCtrl'
            })
            .when('/kannada', {
                templateUrl: 'partials/language-kannda',
                controller: 'kanndaCtrl'
            })
            .when('/tamil', {
                templateUrl: 'partials/language-tamil',
                controller: 'TamilCtrl'
            })
            .when('/telugu', {
                templateUrl: 'partials/language-telagu',
                controller: 'TelaguCtrl'
            })
            .when('/malyalam', {
                templateUrl: 'partials/language-malyalam',
                controller: 'MalyalamCtrl'
            })
            .when('/marathi', {
                templateUrl: 'partials/language-marathi',
                controller: 'marathiCtrl'
            })
            .when('/shg-list', {
                templateUrl: 'partials/shg-view',
                controller: 'ShgCtrl',
                label: 'SHG List',
                data: {
                    pageTitle: 'SHG List'
                }
            })
            .when('/shg/create', {
                templateUrl: 'partials/shg-form',
                controller: 'ShgCtrl',
                label: 'SHG Create',
                data: {
                    pageTitle: 'SHG Create'
                }
            })
            .when('/shg/edit/:id', {
                templateUrl: 'partials/shg-form',
                controller: 'ShgEditCtrl',
                label: 'SHG Edit',
                data: {
                    pageTitle: 'SHG Edit'
                }
            })
            /******************************
        
            .when('/consentform', {
                    templateUrl: 'partials/consentform',
                    controller: 'ConsentFormCtrl'
                })
                .when('/consentform/create', {
                    templateUrl: 'partials/consentform',
                    controller: 'ConsentFormCtrl'
                })
                .when('/consentform/:id', {
                    templateUrl: 'partials/consentform',
                    controller: 'ConsentFormCtrl'
                })
            *****************************/
            .when('/syncoutdata/:id', {
                templateUrl: 'partials/syncoutdata2',
                controller: 'SyncOutDataCtrl',
                label: 'Last Pushed Data',
                data: {
                    pageTitle: 'Last Pushed Data'
                }
            })
            .when('/phonetypes', {
                templateUrl: 'partials/phonetypes',
                controller: 'PhoneCtrl',
                label: 'Phone Type',
                data: {
                    pageTitle: 'Phone Type'
                }
            })
            .when('/phonetypes/create', {
                templateUrl: 'partials/phonetypes',
                controller: 'PhoneCtrl',
                label: 'Phone Type',
                data: {
                    pageTitle: 'Phone Type'
                }
            })
            .when('/phonetypes/:id', {
                templateUrl: 'partials/phonetypes',
                controller: 'PhoneCtrl',
                label: 'Phone Type',
                data: {
                    pageTitle: 'Phone Type'
                }
            })
            .when('/noofterms', {
                templateUrl: 'partials/noofterms',
                controller: 'TermCtrl',
                label: 'No of Terms',
                data: {
                    pageTitle: 'No of Terms'
                }
            })
            .when('/noofterms/create', {
                templateUrl: 'partials/noofterms',
                controller: 'TermCtrl',
                label: 'No of Terms',
                data: {
                    pageTitle: 'No of Terms'
                }
            })
            .when('/noofterms/:id', {
                templateUrl: 'partials/noofterms',
                controller: 'TermCtrl',
                label: 'No of Terms',
                data: {
                    pageTitle: 'No of Terms'
                }
            })
            /*********/
            .when('/archivemember', {
                templateUrl: 'partials/archivemember',
                controller: 'ArchiveCtrl',
                label: 'Archive members',
                data: {
                    pageTitle: 'Archive members'
                }
            })
            .when('/archivemember/create', {
                templateUrl: 'partials/archivemember',
                controller: 'ArchiveCtrl',
                label: 'Archive members',
                data: {
                    pageTitle: 'Archive member'
                }
            })
            .when('/archivemember/:id', {
                templateUrl: 'partials/archivemember',
                controller: 'ArchiveCtrl',
                label: 'Archive members',
                data: {
                    pageTitle: 'Archive members'
                }
            })
            /************/
            .when('/financialPlanning', {
                templateUrl: 'partials/financialPlaning',
                controller: 'fPlanCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/financialPlanning/create', {
                templateUrl: 'partials/financialPlaning',
                controller: 'fPlanCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/financialPlanning:/id', {
                templateUrl: 'partials/financialPlaning',
                controller: 'fPlanCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            /*.when('/cobudgets', {
                templateUrl: 'partials/cobudgetsView',
                controller: 'cobudgetsViewCtrl',
                label: 'CO Budgets',
                data: {
                    pageTitle: 'CO Budgets'
                }
            })
            .when('/cobudgets/create', {
                templateUrl: 'partials/cobudgetsView',
                controller: 'cobudgetsViewCtrl',
                label: 'CO Budgets',
                data: {
                    pageTitle: 'CO Budgets'
                }
            })
            .when('/cobudgets:/id', {
                templateUrl: 'partials/cobudgetsView',
                controller: 'cobudgetsViewCtrl',
                label: 'CO Budgets',
                data: {
                    pageTitle: 'CO Budgets'
                }
            })*/
            .when('/spmbudgets', {
                templateUrl: 'partials/spmbudgetsView',
                controller: 'spmbudgetsViewCtrl',
                label: 'SPM Budgets',
                data: {
                    pageTitle: 'SPM Budgets'
                }
            })
            .when('/spmbudgets/create', {
                templateUrl: 'partials/spmbudgets',
                controller: 'spmbudgetsCtrl',
                label: 'SPM Budgets',
                data: {
                    pageTitle: 'SPM Budgets'
                }
            })
            .when('/spmbudgets/:id', {
                templateUrl: 'partials/spmbudgets',
                controller: 'spmbudgetsCtrl',
                label: 'SPM Budgets',
                data: {
                    pageTitle: 'SPM Budgets'
                }
            })
            .when('/months', {
                templateUrl: 'partials/months',
                controller: 'MonthCtrl'
            })
            .when('/months/create', {
                templateUrl: 'partials/months',
                controller: 'MonthCtrl'
            })
            .when('/months/:id', {
                templateUrl: 'partials/months',
                controller: 'MonthCtrl'
            })
            .when('/cobudgets', {
                templateUrl: 'partials/cobudgetsView1',
                controller: 'cobudgetsViewNew2Ctrl',
                label: 'CO Budgets',
                data: {
                    pageTitle: 'CO Budgets'
                }
            })
            .when('/cobudgets/create', {
                templateUrl: 'partials/cobudgetsView1',
                controller: 'cobudgetsViewNew2Ctrl',
                label: 'CO Budgets',
                data: {
                    pageTitle: 'CO Budgets'
                }
            })
            .when('/cobudgets:/id', {
                templateUrl: 'partials/cobudgetsView1',
                controller: 'cobudgetsViewNew2Ctrl',
                label: 'CO Budgets',
                data: {
                    pageTitle: 'CO Budgets'
                }
            })
            .when('/nobudgets', {
                templateUrl: 'partials/nobudgets',
                controller: 'nobudgetsCtrl',
                label: 'NO Budgets',
                data: {
                    pageTitle: 'NO Budgets'
                }
            })
            .when('/learning', {
                templateUrl: 'partials/learning',
                controller: 'learningCtrl',
                label: 'Learning',
                data: {
                    pageTitle: 'Learning'
                }
            })
            .when('/learning/create', {
                templateUrl: 'partials/learning',
                controller: 'learningCtrl',
                label: 'Learning',
                data: {
                    pageTitle: 'Learning'
                }
            })
            .when('/learning/:id', {
                templateUrl: 'partials/learning',
                controller: 'learningCtrl',
                label: 'Learning',
                data: {
                    pageTitle: 'Learning'
                }
            })
            .when('/learningtype', {
                templateUrl: 'partials/learningtype',
                controller: 'learningtypeCtrl',
                label: 'Learning',
                data: {
                    pageTitle: 'Learning Type'
                }
            })
            .when('/learningtype/create', {
                templateUrl: 'partials/learningtype',
                controller: 'learningtypeCtrl',
                label: 'Learning',
                data: {
                    pageTitle: 'Learning Type'
                }
            })
            .when('/learningtype/:id', {
                templateUrl: 'partials/learningtype',
                controller: 'learningtypeCtrl',
                label: 'Learning',
                data: {
                    pageTitle: 'Learning Type'
                }
            })
            .when('/phctabs', {
                templateUrl: 'partials/phctabs',
                controller: 'PHCTABSCtrl',

            })
            .when('/phctabs/create', {
                templateUrl: 'partials/phctabscreate',
                controller: 'PHCTABSCREATECtrl',
            })
            .when('/LangShgList', {
                templateUrl: 'partials/LangshgList',
                controller: 'LangShgListCtrl',
                label: 'SHG Language List',
                data: {
                    pageTitle: 'SHG Language List'
                }
            })
            .when('/LangShg', {
                templateUrl: 'partials/LangShg',
                controller: 'LangShgCtrl',
                label: 'SHG Language Create',
                data: {
                    pageTitle: 'SHG Language Create'
                }
            })
            .when('/LangShg/:id', {
                templateUrl: 'partials/LangShg',
                controller: 'LangShgCtrl',
                label: 'SHG Language Edit',
                data: {
                    pageTitle: 'SHG Language Edit'
                }
            })
            .when('/LangHhList', {
                templateUrl: 'partials/LanghhList',
                controller: 'LangHHListCtrl',
                label: 'HH Language List',
                data: {
                    pageTitle: 'HH Language List'
                }
            })
            .when('/LangHousehold', {
                templateUrl: 'partials/LangHousehold',
                controller: 'LangHouseholdCtrl',
                label: 'HH Language Create',
                data: {
                    pageTitle: 'HH Language Create'
                }
            })
            .when('/LangHousehold/:id', {
                templateUrl: 'partials/LangHousehold',
                controller: 'LangHouseholdCtrl',
                label: 'HH Language Edit',
                data: {
                    pageTitle: 'HH Language Edit'
                }
            })
            .when('/Langapplyforschmlist', {
                templateUrl: 'partials/Langapplyforschmlist',
                controller: 'LangapplyforschmlistCtrl',
                label: 'Scheme Language List',
                data: {
                    pageTitle: 'Scheme Language List'
                }
            })
            .when('/LangAFS', {
                templateUrl: 'partials/LangAFScheme',
                controller: 'LangAFSchemeCtrl',
                label: 'Scheme Language Create',
                data: {
                    pageTitle: 'Scheme Language Create'
                }
            })
            .when('/LangAFS/:id', {
                templateUrl: 'partials/LangAFScheme',
                controller: 'LangAFSchemeCtrl',
                label: 'Scheme Language Edit',
                data: {
                    pageTitle: 'Scheme Language Edit'
                }
            })
            .when('/Langapplyfordoclist', {
                templateUrl: 'partials/Langapplyfordoclist',
                controller: 'LangapplyfordoclistCtrl',
                label: 'Document Language List',
                data: {
                    pageTitle: 'Document Language List'
                }
            })
            .when('/LangAFD', {
                templateUrl: 'partials/LangAFDocument',
                controller: 'LangAFDo1Ctrl',
                label: 'Document Language Create',
                data: {
                    pageTitle: 'Document Language Create'
                }
            })
            .when('/LangAFD/:id', {
                templateUrl: 'partials/LangAFDocument',
                controller: 'LangAFDo1Ctrl',
                label: 'Document Language Edit',
                data: {
                    pageTitle: 'Document Language Edit'
                }
            })
            .when('/LangReportincidentlist', {
                templateUrl: 'partials/LangReportincidentlist',
                controller: 'LangReportincidentlistCtrl',
                label: 'Incident Language List',
                data: {
                    pageTitle: 'Incident Language List'
                }
            })
            .when('/LangReport', {
                templateUrl: 'partials/LangReportIncident',
                controller: 'LangRICtrl',
                label: 'Incident Language Create',
                data: {
                    pageTitle: 'Incident Language Create'
                }
            })
            .when('/LangReport/:id', {
                templateUrl: 'partials/LangReportIncident',
                controller: 'LangRICtrl',
                label: 'Incident Language Edit',
                data: {
                    pageTitle: 'Incident Language Edit'
                }
            })
            .when('/LangLHSlist', {
                templateUrl: 'partials/LangLHSList',
                controller: 'LangLHSListCtrl',
                label: 'LHS Language List',
                data: {
                    pageTitle: 'LHS Language List'
                }
            })
            .when('/LangLHS', {
                templateUrl: 'partials/LangLHS',
                controller: 'LangLHSCtrl',
                label: 'LHS Language Create',
                data: {
                    pageTitle: 'LHS Language Create'
                }
            })
            .when('/LangLHS/:id', {
                templateUrl: 'partials/LangLHS',
                controller: 'LangLHSCtrl',
                label: 'LHS Language Edit',
                data: {
                    pageTitle: 'LHS Language Edit'
                }
            })
            .when('/LangScreenTreat-list', {
                templateUrl: 'partials/Lang-condition-list',
                controller: 'LangConditionsListCtrl',
                label: 'Screen and Treat Language List',
                data: {
                    pageTitle: 'Screen and Treat Language List'
                }
            })
            .when('/LangScreenTreat/create', {
                templateUrl: 'partials/Lang-condition',
                controller: 'LangConditionCtrl',
                label: 'Screen and Treat Language Create',
                data: {
                    pageTitle: 'Screen and Treat Language Create'
                }
            })
            .when('/LangScreenTreat/edit/:id', {
                templateUrl: 'partials/Lang-condition',
                controller: 'LangConditionCtrl',
                label: 'Screen and Treat Language Edit',
                data: {
                    pageTitle: 'Screen and Treat Language Edit'
                }
            })
            .when('/LangShgmeetings-list', {
                templateUrl: 'partials/Lang-Shgmeeting-list',
                controller: 'LangShgMeetingsListCtrl',
                label: 'SHG Meeting Language List',
                data: {
                    pageTitle: 'SHG Meeting Language List'
                }
            })
            .when('/LangShgmeeting/create', {
                templateUrl: 'partials/Lang-Shgmeeting',
                controller: 'LangShgMeetingsCtrl',
                label: 'SHG Meeting Language Create',
                data: {
                    pageTitle: 'SHG Meeting Language Create'
                }
            })
            .when('/LangShgmeeting/edit/:id', {
                templateUrl: 'partials/Lang-Shgmeeting',
                controller: 'LangShgMeetingsCtrl',
                label: 'SHG Meeting Language Edit',
                data: {
                    pageTitle: 'SHG Meeting Language Edit'
                }
            })
            .when('/symptoms-list', {
                templateUrl: 'partials/symptoms',
                controller: 'SymptomsCtrl',
                label: 'Symptoms List',
                data: {
                    pageTitle: 'Symptoms List'
                }
            })
            .when('/symptom/create', {
                templateUrl: 'partials/symptoms',
                controller: 'SymptomsCtrl',
                label: 'Symptom Create',
                data: {
                    pageTitle: 'Symptom Create'
                }
            })
            .when('/symptom/edit/:id', {
                templateUrl: 'partials/symptoms',
                controller: 'SymptomsCtrl',
                label: 'Symptom Edit',
                data: {
                    pageTitle: 'Symptom Edit'
                }
            })
            .when('/illness-list', {
                templateUrl: 'partials/illness',
                controller: 'IllnessCtrl',
                label: 'Illness List',
                data: {
                    pageTitle: 'Illness List'
                }
            })
            .when('/illness/create', {
                templateUrl: 'partials/illness',
                controller: 'IllnessCtrl',
                label: 'Illness Create',
                data: {
                    pageTitle: 'Illness Create'
                }
            })
            .when('/illness/edit/:id', {
                templateUrl: 'partials/illness',
                controller: 'IllnessCtrl',
                label: 'Illness Edit',
                data: {
                    pageTitle: 'Illness Edit'
                }
            })
            .when('/reasonforloan-list', {
                templateUrl: 'partials/reasonforloan',
                controller: 'ReasonForLoanCtrl',
                label: 'Reason for Loan List',
                data: {
                    pageTitle: 'Reason for Loan List'
                }
            })
            .when('/reasonforloan/create', {
                templateUrl: 'partials/reasonforloan',
                controller: 'ReasonForLoanCtrl',
                label: 'Reason for Loan Create',
                data: {
                    pageTitle: 'Reason for Loan Create'
                }
            })
            .when('/reasonforloan/edit/:id', {
                templateUrl: 'partials/reasonforloan',
                controller: 'ReasonForLoanCtrl',
                label: 'Reason for Loan Edit',
                data: {
                    pageTitle: 'Reason for Loan Edit'
                }
            })
            .when('/screentreatstatus-list', {
                templateUrl: 'partials/conditionstatus',
                controller: 'ConditionStatusCtrl',
                label: 'Screen and Treat Status List',
                data: {
                    pageTitle: 'Screen and Treat Status List'
                }
            })
            .when('/screentreatstatus/create', {
                templateUrl: 'partials/conditionstatus',
                controller: 'ConditionStatusCtrl',
                label: 'Screen and Treat Status Create',
                data: {
                    pageTitle: 'Screen and Treat Status Create'
                }
            })
            .when('/screentreatstatus/edit/:id', {
                templateUrl: 'partials/conditionstatus',
                controller: 'ConditionStatusCtrl',
                label: 'Screen and Treat Status Edit',
                data: {
                    pageTitle: 'Screen and Treat Status Edit'
                }
            })
            .when('/screentreatstep-list', {
                templateUrl: 'partials/conditionstep',
                controller: 'ConditionStepCtrl',
                label: 'Screen and Treat Step List',
                data: {
                    pageTitle: 'Screen and Treat Step List'
                }
            })
            .when('/screentreatstep/create', {
                templateUrl: 'partials/conditionstep',
                controller: 'ConditionStepCtrl',
                label: 'Screen and Treat Step Create',
                data: {
                    pageTitle: 'Screen and Treat Step Create'
                }
            })
            .when('/screentreatstep/edit/:id', {
                templateUrl: 'partials/conditionstep',
                controller: 'ConditionStepCtrl',
                label: 'Screen and Treat Step Edit',
                data: {
                    pageTitle: 'Screen and Treat Step Edit'
                }
            })
            .when('/screentreatdiagnosis-list', {
                templateUrl: 'partials/conditiondiagnosis',
                controller: 'ConditionDiagnosisCtrl',
                label: 'Screen and Treat Diagnosis List',
                data: {
                    pageTitle: 'Screen and Treat Diagnosis List'
                }
            })
            .when('/screentreatdiagnosis/create', {
                templateUrl: 'partials/conditiondiagnosis',
                controller: 'ConditionDiagnosisCtrl',
                label: 'Screen and Treat Diagnosis Create',
                data: {
                    pageTitle: 'Screen and Treat Diagnosis Create'
                }
            })
            .when('/screentreatdiagnosis/edit/:id', {
                templateUrl: 'partials/conditiondiagnosis',
                controller: 'ConditionDiagnosisCtrl',
                label: 'Screen and Treat Diagnosis Edit',
                data: {
                    pageTitle: 'Screen and Treat Diagnosis Edit'
                }
            })
            .when('/screenedby-list', {
                templateUrl: 'partials/screenedby',
                controller: 'ScreenedByCtrl',
                label: 'Screened By List',
                data: {
                    pageTitle: 'Screened By List'
                }
            })
            .when('/screenedby/create', {
                templateUrl: 'partials/screenedby',
                controller: 'ScreenedByCtrl',
                label: 'Screened By Create',
                data: {
                    pageTitle: 'Screened By Create'
                }
            })
            .when('/screenedby/edit/:id', {
                templateUrl: 'partials/screenedby',
                controller: 'ScreenedByCtrl',
                label: 'Screened By Edit',
                data: {
                    pageTitle: 'Screened By Edit'
                }
            })
            .when('/screentreat-list', {
                templateUrl: 'partials/condition',
                controller: 'ConditionCtrl',
                label: 'Screen and Treat List',
                data: {
                    pageTitle: 'Screen and Treat List'
                }
            })
            .when('/screentreatlist/create', {
                templateUrl: 'partials/condition',
                controller: 'ConditionCtrl',
                label: 'Screen and Treat Create',
                data: {
                    pageTitle: 'Screen and Treat Create'
                }
            })
            .when('/screentreatlist/edit/:id', {
                templateUrl: 'partials/condition',
                controller: 'ConditionCtrl',
                label: 'Screen and Treat Edit',
                data: {
                    pageTitle: 'Screen and Treat Edit'
                }
            })
            .when('/screentreatfollowup-list', {
                templateUrl: 'partials/conditionfollowup',
                controller: 'ConditionFollowupCtrl',
                label: 'Screen and Treat Followup List',
                data: {
                    pageTitle: 'Screen and Treat Followup List'
                }
            })
            .when('/screentreatfollowup/create', {
                templateUrl: 'partials/conditionfollowup',
                controller: 'ConditionFollowupCtrl',
                label: 'Screen and Treat Followup Create',
                data: {
                    pageTitle: 'Screen and Treat Followup Create'
                }
            })
            .when('/screentreatfollowup/edit/:id', {
                templateUrl: 'partials/conditionfollowup',
                controller: 'ConditionFollowupCtrl',
                label: 'Screen and Treat Followup Edit',
                data: {
                    pageTitle: 'Screen and Treat Followup Edit'
                }
            })
            .when('/screentreatchecklist-list', {
                templateUrl: 'partials/conditionchecklist',
                controller: 'ConditionCheckListCtrl',
                label: 'Screen and Treat Checklist List',
                data: {
                    pageTitle: 'Screen and Treat Checklist List'
                }
            })
            .when('/screentreatchecklist/create', {
                templateUrl: 'partials/conditionchecklist',
                controller: 'ConditionCheckListCtrl',
                label: 'Screen and Treat Checklist Create',
                data: {
                    pageTitle: 'Screen and Treat Checklist Create'
                }
            })
            .when('/screentreatchecklist/edit/:id', {
                templateUrl: 'partials/conditionchecklist',
                controller: 'ConditionCheckListCtrl',
                label: 'Screen and Treat Checklist Edit',
                data: {
                    pageTitle: 'Screen and Treat Checklist Edit'
                }
            })
            .when('/reasonforcancelling-list', {
                templateUrl: 'partials/reasonforcancelling',
                controller: 'ReasonForCancellingCtrl',
                label: 'Reason For Cancelling List',
                data: {
                    pageTitle: 'Reason For Cancelling List'
                }
            })
            .when('/reasonforcancelling/create', {
                templateUrl: 'partials/reasonforcancelling',
                controller: 'ReasonForCancellingCtrl',
                label: 'Reason For Cancelling Create',
                data: {
                    pageTitle: 'Reason For Cancelling Create'
                }
            })
            .when('/reasonforcancelling/edit/:id', {
                templateUrl: 'partials/reasonforcancelling',
                controller: 'ReasonForCancellingCtrl',
                label: 'Reason For Cancelling  Edit',
                data: {
                    pageTitle: 'Reason For Cancelling  Edit'
                }
            })
            .when('/language-list', {
                templateUrl: 'partials/languages',
                controller: 'LanguagesCtrl',
                label: 'Language List',
                data: {
                    pageTitle: 'Language List'
                }
            })
            .when('/language/create', {
                templateUrl: 'partials/languages',
                controller: 'LanguagesCtrl',
                label: 'Language Create',
                data: {
                    pageTitle: 'Language Create'
                }
            })
            .when('/language/edit/:id', {
                templateUrl: 'partials/languages',
                controller: 'LanguagesCtrl',
                label: 'Language Edit',
                data: {
                    pageTitle: 'Language Edit'
                }
            })

            .when('/occupationstatus', {
                templateUrl: 'partials/occupationstatus',
                controller: 'occupationstatusCtrl',
                label: 'occupationstatus',
                data: {
                    pageTitle: 'occupationstatus'
                }
            })
            .when('/documenttype', {
                templateUrl: 'partials/documenttype',
                controller: 'documenttypeCtrl',
                label: 'documenttype',
                data: {
                    pageTitle: 'documenttype'
                }
            })
            .when('/audit-trail', {
                templateUrl: 'partials/audit-trail',
                controller: 'AuditTrailCtrl',
                label: 'Audit Trail',
                data: {
                    pageTitle: 'Audit Trail'
                }
            })
            .when('/shgmeetings-list', {
                templateUrl: 'partials/shgmeetinglist',
                controller: 'SHGMeetingsListCtrl',
                label: 'SHG Meetings List',
                data: {
                    pageTitle: 'SHG Meetings List'
                }
            })
            .when('/shgmeeting/create', {
                templateUrl: 'partials/shgmeetings',
                controller: 'SHGMeetingsCtrl',
                label: 'SHG Meeting Create',
                data: {
                    pageTitle: 'SHG Meeting Create'
                }
            })
            .when('/shgmeeting/view/:id', {
                templateUrl: 'partials/shgmeetings',
                controller: 'SHGMeetingsCtrl',
                label: 'SHG Meeting View',
                data: {
                    pageTitle: 'SHG Meeting View'
                }
            })
            .when('/shgmeeting/edit/:id', {
                templateUrl: 'partials/shgmeetings',
                controller: 'SHGMeetingsEditCtrl',
                label: 'SHG Meeting Edit',
                data: {
                    pageTitle: 'SHG Meeting Edit'
                }
            })
            .when('/screentreats-list', {
                templateUrl: 'partials/conditionslist',
                controller: 'ConditionListCtrl',
                label: 'Screen and Treat List',
                data: {
                    pageTitle: 'Screen and Treat List'
                }
            })
            .when('/screentreat/create', {
                templateUrl: 'partials/condition-form',
                controller: 'ConditionCreateCtrl',
                label: 'Screen and Treat Create',
                data: {
                    pageTitle: 'Screen and Treat Create'
                }
            })
            .when('/screentreat/edit/:id', {
                templateUrl: 'partials/condition-form',
                controller: 'ConditionEditCtrl',
                label: 'Screen and Treat Edit',
                data: {
                    pageTitle: 'Screen and Treat Edit'
                }
            })
            .when('/interventions-list', {
                templateUrl: 'partials/interventionslist',
                controller: 'InterventionsListCtrl',
                label: 'Interventions List',
                data: {
                    pageTitle: 'Interventions List'
                }
            })
            .when('/intervention/create', {
                templateUrl: 'partials/intervention-form',
                controller: 'InterventionsCreateCtrl',
                label: 'Intervention Create',
                data: {
                    pageTitle: 'Intervention Create'
                }
            })
            .when('/intervention/edit/:id', {
                templateUrl: 'partials/intervention-form',
                controller: 'InterventionsEditCtrl',
                label: 'Intervention Edit',
                data: {
                    pageTitle: 'Intervention Edit'
                }
            })
            .when('/LangInterventions-list', {
                templateUrl: 'partials/Lang-interventions-list',
                controller: 'LangInterventionsListCtrl',
                label: 'Interventions Language List',
                data: {
                    pageTitle: 'Interventions Language List'
                }
            })
            .when('/LangInterventions/create', {
                templateUrl: 'partials/Lang-interventions',
                controller: 'LangInterventionsCtrl',
                label: 'Interventions Language Create',
                data: {
                    pageTitle: 'Interventions Language Create'
                }
            })
            .when('/LangInterventions/edit/:id', {
                templateUrl: 'partials/Lang-interventions',
                controller: 'LangInterventionsCtrl',
                label: 'Interventions Language Edit',
                data: {
                    pageTitle: 'Interventions Language Edit'
                }
            })
            .when('/interventions-venue-list', {
                templateUrl: 'partials/interventions-venue',
                controller: 'InterventionsVenueCtrl',
                label: 'Interventions Venue List',
                data: {
                    pageTitle: 'Interventions Venue List'
                }
            })
            .when('/interventions-venue/create', {
                templateUrl: 'partials/interventions-venue',
                controller: 'InterventionsVenueCtrl',
                label: 'Interventions Venue Create',
                data: {
                    pageTitle: 'Interventions Venue Create'
                }
            })
            .when('/interventions-venue/edit/:id', {
                templateUrl: 'partials/interventions-venue',
                controller: 'InterventionsVenueCtrl',
                label: 'Interventions Venue Edit',
                data: {
                    pageTitle: 'Interventions Venue Edit'
                }
            })
            .when('/interventions-type-list', {
                templateUrl: 'partials/interventions-type',
                controller: 'InterventionsTypeCtrl',
                label: 'Interventions Type List',
                data: {
                    pageTitle: 'Interventions Type List'
                }
            })
            .when('/interventions-type/create', {
                templateUrl: 'partials/interventions-type',
                controller: 'InterventionsTypeCtrl',
                label: 'Interventions Type Create',
                data: {
                    pageTitle: 'Interventions Type Create'
                }
            })
            .when('/interventions-type/edit/:id', {
                templateUrl: 'partials/interventions-type',
                controller: 'InterventionsTypeCtrl',
                label: 'Interventions Type Edit',
                data: {
                    pageTitle: 'Interventions Type Edit'
                }
            })
            .when('/interventions-status-list', {
                templateUrl: 'partials/interventions-status',
                controller: 'InterventionsStatusCtrl',
                label: 'Interventions Status List',
                data: {
                    pageTitle: 'Interventions Status List'
                }
            })
            .when('/interventions-status/create', {
                templateUrl: 'partials/interventions-status',
                controller: 'InterventionsStatusCtrl',
                label: 'Interventions Status Create',
                data: {
                    pageTitle: 'Interventions Status Create'
                }
            })
            .when('/interventions-status/edit/:id', {
                templateUrl: 'partials/interventions-status',
                controller: 'InterventionsStatusCtrl',
                label: 'Interventions Status Edit',
                data: {
                    pageTitle: 'Interventions Status Edit'
                }
            })
            .when('/analytics', {
                templateUrl: 'partials/analytics',
                controller: 'AnalyticsCtrl',
                label: 'Analytics',
                data: {
                    pageTitle: 'Analytics'
                }
            })
            .when('/import', {
                templateUrl: 'partials/import',
                controller: 'ImportHouseHoldCtrl',
                label: 'Import Household',
                data: {
                    pageTitle: 'Import Household'
                }
            })
            .when('/patientrecord-list', {
                templateUrl: 'partials/patientrecord-view',
                controller: 'PatientRecordListCtrl',
                label: 'Patient Record List',
                data: {
                    pageTitle: 'Patient Record List'
                }
            })
            .when('/patientrecord/create', {
                templateUrl: 'partials/patientrecord',
                controller: 'PatientRecordCreateCtrl',
                label: 'Add Patient Record',
                data: {
                    pageTitle: 'Add Patient Record'
                }
            })
            .when('/patientrecord/edit/:id', {
                templateUrl: 'partials/patientrecord',
                controller: 'PatientRecordEditCtrl',
                label: 'Edit Patient Record',
                data: {
                    pageTitle: 'Edit Patient Record'
                }
            })
            .when('/medicine-list', {
                templateUrl: 'partials/medicines',
                controller: 'MedicineCtrl',
                label: 'Medicine List',
                data: {
                    pageTitle: 'Medicine List'
                }
            })
            .when('/medicine/edit/:id', {
                templateUrl: 'partials/medicines',
                controller: 'MedicineCtrl',
                label: 'Medicine Edit',
                data: {
                    pageTitle: 'Medicine Edit'
                }
            })
            .when('/medicine/create', {
                templateUrl: 'partials/medicines',
                controller: 'MedicineCtrl',
                label: 'Medicine Create',
                data: {
                    pageTitle: 'Medicine Create'
                }
            })
            .when('/seenby-list', {
                templateUrl: 'partials/seenby',
                controller: 'seenByCtrl',
                label: 'SeenBy List',
                data: {
                    pageTitle: 'SeenBy List'
                }
            })
            .when('/seenby/edit/:id', {
                templateUrl: 'partials/seenby',
                controller: 'seenByCtrl',
                label: 'SeenBy Edit',
                data: {
                    pageTitle: 'SeenBy Edit'
                }
            })
            .when('/seenby/create', {
                templateUrl: 'partials/seenby',
                controller: 'seenByCtrl',
                label: 'SeenBy Create',
                data: {
                    pageTitle: 'SeenBy Create'
                }
            })

            .when('/referto-list', {
                templateUrl: 'partials/referto',
                controller: 'ReferToCtrl',
                label: 'Refer To List',
                data: {
                    pageTitle: 'Refer To List'
                }
            })
            .when('/referto/edit/:id', {
                templateUrl: 'partials/referto',
                controller: 'ReferToCtrl',
                label: 'Refer To Edit',
                data: {
                    pageTitle: 'Refer To Edit'
                }
            })
            .when('/referto/create', {
                templateUrl: 'partials/referto',
                controller: 'ReferToCtrl',
                label: 'Refer To Create',
                data: {
                    pageTitle: 'Refer To Create'
                }
            })
            .when('/testname-list', {
                templateUrl: 'partials/testname',
                controller: 'TestNameCtrl',
                label: 'Test Name List',
                data: {
                    pageTitle: 'Test Name List'
                }
            })
            .when('/testname/edit/:id', {
                templateUrl: 'partials/testname',
                controller: 'TestNameCtrl',
                label: 'Test Name Edit',
                data: {
                    pageTitle: 'Test Name Edit'
                }
            })
            .when('/testname/create', {
                templateUrl: 'partials/testname',
                controller: 'TestNameCtrl',
                label: 'Test Name Create',
                data: {
                    pageTitle: 'Test Name Create'
                }
            })

            .when('/LangPatientRecordlist', {
                templateUrl: 'partials/LangPatientrecordlist',
                controller: 'LangPatientRecordlist',
                label: 'Patient Record Language List',
                data: {
                    pageTitle: 'Patient Record Language List'
                }
            })
            .when('/LangPatientRecord', {
                templateUrl: 'partials/LangPatientRecord',
                controller: 'LangPatientRecordCtrl',
                label: 'Patient Record LanguageCreate',
                data: {
                    pageTitle: 'Patient Record Language Create'
                }
            })
            .when('/LangPatientRecord/:id', {
                templateUrl: 'partials/LangPatientRecord',
                controller: 'LangPatientRecordCtrl',
                label: 'Patient Record Language Edit',
                data: {
                    pageTitle: 'Patient Record Language Edit'
                }
            })
            .when('/vitalrecord-list', {
                templateUrl: 'partials/vitalrecord',
                controller: 'VitalRecordCtrl',
                label: 'Vital Record List',
                data: {
                    pageTitle: 'Vital Record List'
                }
            })
            .when('/vitalrecord/edit/:id', {
                templateUrl: 'partials/vitalrecord',
                controller: 'VitalRecordCtrl',
                label: 'Vital Record Edit',
                data: {
                    pageTitle: 'Vital Record Edit'
                }
            })
            .when('/vitalrecord/create', {
                templateUrl: 'partials/vitalrecord',
                controller: 'VitalRecordCtrl',
                label: 'Vital Record Create',
                data: {
                    pageTitle: 'Vital Record Create'
                }
            })
            .when('/nonsystematicrecords-list', {
                templateUrl: 'partials/nonsystematicrecord',
                controller: 'NonSystematicRecordCtrl',
                label: 'Non Systematic Record List',
                data: {
                    pageTitle: 'Non Systematic Record List'
                }
            })
            .when('/nonsystematicrecord/create', {
                templateUrl: 'partials/nonsystematicrecord',
                controller: 'NonSystematicRecordCtrl',
                label: 'Non Systematic Record Create',
                data: {
                    pageTitle: 'Non Systematic Record Create'
                }
            })
            .when('/nonsystematicrecord/edit/:id', {
                templateUrl: 'partials/nonsystematicrecord',
                controller: 'NonSystematicRecordCtrl',
                label: 'Non Systematic Record Edit',
                data: {
                    pageTitle: 'Non Systematic Record Edit'
                }
            })
            .when('/LangWash-list', {
                templateUrl: 'partials/LangWashList',
                controller: 'LangWashListCtrl',
                label: 'Wash Language List',
                data: {
                    pageTitle: 'Wash Language List'
                }
            })
            .when('/LangWash/create', {
                templateUrl: 'partials/Lang-wash',
                controller: 'LangWashCtrl',
                label: 'Wash Language Create',
                data: {
                    pageTitle: 'Wash Language Create'
                }
            })
            .when('/LangWash/edit/:id', {
                templateUrl: 'partials/Lang-wash',
                controller: 'LangWashCtrl',
                label: 'Wash Language Edit',
                data: {
                    pageTitle: 'Wash Language Edit'
                }
            })
            .when('/LangHf-list', {
                templateUrl: 'partials/LangHf-List',
                controller: 'LangHfListCtrl',
                label: 'HF Language List',
                data: {
                    pageTitle: 'HF Language List'
                }
            })
            .when('/LangHf/create', {
                templateUrl: 'partials/LangHf',
                controller: 'LangHfCtrl',
                label: 'HF Language Create',
                data: {
                    pageTitle: 'HF Language Create'
                }
            })
            .when('/LangHf/edit/:id', {
                templateUrl: 'partials/LangHf',
                controller: 'LangHfCtrl',
                label: 'HF Language Edit',
                data: {
                    pageTitle: 'HF Language Edit'
                }
            })
            .when('/typeofinfrastructure-list', {
                templateUrl: 'partials/typeofinfrastructures',
                controller: 'TypeOfInfrastructureCtrl',
                label: 'Type of Infrastructure List',
                data: {
                    pageTitle: 'Type of Infrastructure List'
                }
            })
            .when('/typeofinfrastructure/create', {
                templateUrl: 'partials/typeofinfrastructures',
                controller: 'TypeOfInfrastructureCtrl',
                label: 'Type of Infrastructure Create',
                data: {
                    pageTitle: 'Type of Infrastructure Create'
                }
            })
            .when('/typeofinfrastructure/edit/:id', {
                templateUrl: 'partials/typeofinfrastructures',
                controller: 'TypeOfInfrastructureCtrl',
                label: 'Type of Infrastructure Edit',
                data: {
                    pageTitle: 'Type of Infrastructure Edit'
                }
            })
            .when('/washinfrastructure-list', {
                templateUrl: 'partials/washinfrastructure-list',
                controller: 'WashInfraListCtrl',
                label: 'Wash Infrastructure List',
                data: {
                    pageTitle: 'Wash Infrastructure List'
                }
            })
            .when('/washinfrastructure/create', {
                templateUrl: 'partials/washinfrastructure',
                controller: 'WashInfrastructureCtrl',
                label: 'Wash Infrastructure Create',
                data: {
                    pageTitle: 'Wash Infrastructure Create'
                }
            })
            .when('/washinfrastructure/edit/:id', {
                templateUrl: 'partials/washinfrastructure',
                controller: 'WashInfrastructureEditCtrl',
                label: 'Wash Infrastructure Edit',
                data: {
                    pageTitle: 'Wash Infrastructure Edit'
                }
            })
            .when('/school-list', {
                templateUrl: 'partials/schoollist',
                controller: 'SchoolListCtrl',
                label: 'School List',
                data: {
                    pageTitle: 'School List'
                }
            })
            .when('/school/create', {
                templateUrl: 'partials/school-form',
                controller: 'SchoolCreateCtrl',
                label: 'Add School ',
                data: {
                    pageTitle: 'Add School'
                }
            })
            .when('/school/edit/:id', {
                templateUrl: 'partials/school-form',
                controller: 'SchoolEditCtrl',
                label: 'Edit School',
                data: {
                    pageTitle: 'Edit School'
                }
            })
            .when('/typeofschool-list', {
                templateUrl: 'partials/typeofschool',
                controller: 'SchoolTypeCtrl',
                label: 'School Type List',
                data: {
                    pageTitle: 'School Type List'
                }
            })
            .when('/typeofschool/edit/:id', {
                templateUrl: 'partials/typeofschool',
                controller: 'SchoolTypeCtrl',
                label: 'School Type Edit',
                data: {
                    pageTitle: 'School Type Edit'
                }
            })
            .when('/typeofschool/create', {
                templateUrl: 'partials/typeofschool',
                controller: 'SchoolTypeCtrl',
                label: 'School Type Create',
                data: {
                    pageTitle: 'School Type Create'
                }
            })
            .when('/Langschool-list', {
                templateUrl: 'partials/LangSchool-list',
                controller: 'LangSchoolListCtrl',
                label: 'School Language List',
                data: {
                    pageTitle: 'School Language List'
                }
            })
            .when('/Langschool/create', {
                templateUrl: 'partials/LangSchool',
                controller: 'LangSchoolCtrl',
                label: 'School Language Create',
                data: {
                    pageTitle: 'School Language Create'
                }
            })
            .when('/Langschool/edit/:id', {
                templateUrl: 'partials/LangSchool',
                controller: 'LangSchoolCtrl',
                label: 'School Language Edit',
                data: {
                    pageTitle: 'School Language Edit'
                }
            })
            .when('/dynamicschoolchecklist', {
                templateUrl: 'partials/dynamicChecklist',
                controller: 'DynamicSchoolCheckListCtrl',
                label: 'Dynamic school CheckList',
                data: {
                    pageTitle: 'Dynamic school CheckList'
                }
            })
            .when('/dynamicschoolchecklist/create', {
                templateUrl: 'partials/dynamicChecklist',
                controller: 'DynamicSchoolCheckListCtrl',
                label: 'Dynamic school CheckList Create',
                data: {
                    pageTitle: 'Dynamic school CheckList Create'
                }
            })
            .when('/dynamicschoolchecklist/edit/:id', {
                templateUrl: 'partials/dynamicChecklist',
                controller: 'DynamicSchoolCheckListCtrl',
                label: 'Dynamic school CheckList Edit',
                data: {
                    pageTitle: 'Dynamic school CheckList Edit'
                }
            })
            .when('/schoolchecklist-list', {
                templateUrl: 'partials/schoolChecklist',
                controller: 'SchoolCheckListCtrl',
                label: 'School CheckList',
                data: {
                    pageTitle: 'School CheckList'
                }
            })
            .when('/schoolchecklist/create', {
                templateUrl: 'partials/schoolChecklist-form',
                controller: 'SchoolCheckListCreateCtrl',
                label: 'School CheckList Create',
                data: {
                    pageTitle: 'School CheckList Create'
                }
            })
            .when('/schoolchecklist/edit/:id', {
                templateUrl: 'partials/schoolChecklist-form',
                controller: 'SchoolCheckListEditCtrl',
                label: 'School CheckList Edit',
                data: {
                    pageTitle: 'School CheckList Edit'
                }
            })
            .when('/trendcharts', {
                templateUrl: 'partials/trendcharts',
                controller: 'TrendChartsCtrl',
                label: 'Trend Charts',
                data: {
                    pageTitle: 'Trend Charts'
                }
            })
    
    .when('/shg-migration', {
                templateUrl: 'partials/shgmigration',
                controller: 'ShgMigrationCtrl',
                label: 'Shg Migration',
                data: {
                    pageTitle: 'Shg Migration'
                }
            })
            .otherwise({
                redirectTo: '/'
            });

        $locationProvider.html5Mode(true);
        RestangularProvider.setBaseUrl('//i4we-api.herokuapp.com/api/v1/');
        RestangularProvider.addElementTransformer('users', true, function (user) {
            // This will add a method called login that will do a POST to the path login
            // signature is (name, operation, path, params, headers, elementToPost)

            user.addRestangularMethod('login', 'post', 'login');

            return user;
        });
        $idleProvider.idleDuration(60 * 30);
        $idleProvider.warningDuration(5);
        $keepaliveProvider.interval(10);
    })

    .factory('AnalyticsRestangular', function (Restangular) {
        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl('//swastianalyticstest.herokuapp.com/api/v1/');
        });
    })
    .run(function ($rootScope, $window, $location, Restangular, $idle, $modal, AnalyticsRestangular) {
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            if (current != undefined) {
                $window.sessionStorage.prviousLocation = current.loadedTemplateUrl;
                $window.sessionStorage.current = next.originalPath;
                $window.sessionStorage.previous = current.originalPath;
                //                console.log("event",event);
                //                console.log("next",next);
                //                console.log("current",current);
                // console.log('$window.sessionStorage.prviousLocation when current != undefined',$window.sessionStorage.prviousLocation);
            } else {
                $window.sessionStorage.prviousLocation = null;
                //console.log('$window.sessionStorage.prviousLocation when else',$window.sessionStorage.prviousLocation);
            }
            //  console.log('$window.sessionStorage.userId', $window.sessionStorage.userId);
            console.log('$location.path()', $location.path());
            if (!$window.sessionStorage.userId && $location.path() !== '/login') {
                $location.path('/login');
            }
            if ($location.path() == '/login') {
                $rootScope.showMe = true;
            } else {
                $rootScope.showMe = false;
            }

            if ($location.host() !== "localhost" && $location.protocol() == 'http') {
                //$location.protocol = $location.protocol().replace("http", "https");
                window.location = $location.absUrl().replace("http", "https");
                // console.log('http', 'http');
            } else {
                // console.log('https', 'https')
            }

            Restangular.setDefaultHeaders({
                'authorization': $window.sessionStorage.accessToken
            });

            AnalyticsRestangular.setDefaultHeaders({
                'authorization': $window.sessionStorage.analyticsAccessToken
            });
        });

        $rootScope.started = false;
        $rootScope.pageSize = 25;

        function closeModals() {
            if ($rootScope.warning) {
                $rootScope.warning.close();
                $rootScope.warning = null;
            }

            if ($rootScope.timedout) {
                $rootScope.timedout.close();
                $rootScope.timedout = null;
            }
        }

        function logout() {
            // alert('alert');
            //$location.path("/login");

            //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
            Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
                $window.sessionStorage.userId = '';
                console.log('Logout');
            }).then(function (redirect) {
                $window.location.href = '/login';
                $window.location.reload();
                $idle.unwatch();
            });
        }

        $rootScope.$on('$idleStart', function () {
            closeModals();

            $rootScope.warning = $modal.open({
                templateUrl: 'partials/warning-dialog.html',
                windowClass: 'modal-danger'
            });
        });

        $rootScope.$on('$idleEnd', function () {
            closeModals();
        });

        $rootScope.$on('$idleTimeout', function () {
            closeModals();
            logout();
            $rootScope.timedout = $modal.open({
                templateUrl: 'partials/timedout-dialog.html',
                windowClass: 'modal-danger'
            });
        });

        $rootScope.start = function () {
            closeModals();
            $idle.watch();
            $rootScope.started = true;
        };
        if (!$window.sessionStorage.userId == '') {
            $rootScope.start();
        }

        $rootScope.stop = function () {
            closeModals();
            $idle.unwatch();
            $rootScope.started = false;

        };
        $rootScope.style = {
            "font-size": 14
        }
    })

    .directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    // this next if is necessary for when using ng-required on your input. 
                    // In such cases, when a letter is typed first, this parser will be called
                    // again, and the 2nd time, the value will be undefined
                    if (inputValue == undefined) return ''
                    var transformedInput = inputValue.replace(/[^0-9.]/g, '');
                    if (transformedInput != inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }

                    return transformedInput;
                });
            }
        };
    })

    .controller('crumbCtrl', ['$scope', '$location', '$route', 'breadcrumbs',
  function ($scope, $location, $route, breadcrumbs) {
            $scope.location = $location;
            $scope.breadcrumbs = breadcrumbs;

}])
